﻿using UnityEngine;
using System.Collections;

public class RowController : MonoBehaviour {

	public Material BlackHintColor;
	public Material WhiteHintColor;
	public int rowLength;

	bool active;
	Material[] rowCode;
	GameObject[] hints;

	void Awake() {
		rowCode = new Material[rowLength];
		hints = new GameObject[rowLength];

		hints [0] = transform.FindChild ("Hint 1").gameObject;
		hints [1] = transform.FindChild ("Hint 2").gameObject;
		hints [2] = transform.FindChild ("Hint 3").gameObject;
		hints [3] = transform.FindChild ("Hint 4").gameObject;
		if (rowLength > 4) {
			hints [4] = transform.FindChild ("Hint 5").gameObject;
			hints [5] = transform.FindChild ("Hint 6").gameObject;
		}
	}
	
	public void SetActive(bool flag) {
		active = flag;
	}

	public bool IsActive() {
		return active;
	}

	public void SetColor(Material color, int index) {
		rowCode [index] = color;
	}

	public bool IsComplete() {
		bool isComplete = true;
		foreach (Material code in rowCode) {
			isComplete = isComplete && (code != null);
		}
		return isComplete;
	}

	public bool CheckCode(Material[] code) {
		bool correct = true;
		for (int i = 0; i < rowLength; i++) {
			GameObject go = hints[i];
			if (code[i] == rowCode[i]) {
				go.SetActive(true);
				go.GetComponent<Renderer> ().sharedMaterial = BlackHintColor;
			} else {
				if (contains(rowCode[i], code)) {
					go.SetActive(true);
					go.GetComponent<Renderer> ().sharedMaterial = WhiteHintColor;
				}
				correct = false;
			}
		}

		return correct;
	}

	public bool CheckCodeEasy(Material[] code) {
		bool correct = true;
		for (int i = 0; i < rowLength; i++) {
			GameObject go = hints[i];
			if (contains(rowCode[i], code)) {
				go.SetActive(true);
				go.GetComponent<Renderer> ().sharedMaterial = BlackHintColor;
			} else {
				correct = false;
			}
		}
		
		return correct;
	}

	bool contains(Material color, Material[] code) {
		bool contains = false;
		for (int j = 0; j < rowLength; j++) {
			contains = contains || (color == code[j]);
		}
		return contains;
	}

//	static void RandomizeArray(GameObject[] array) {
//		for (int i = 0; i < 4; i++) {
//			int r = Random.Range(0,i);
//			GameObject tmp = array[i];
//			array[i] = array[r];
//			array[r] = tmp;
//		}
//	}
}
