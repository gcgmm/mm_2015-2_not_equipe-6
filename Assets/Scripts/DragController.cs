﻿using UnityEngine;
using System.Collections;

public class DragController : MonoBehaviour {

	public Material color;
	public float camRayLength = 100f;

	Vector3 originalPosition;
	LayerMask dragLayerMask;
	Collider dock = null;
	
	void Awake() {
		dragLayerMask = LayerMask.GetMask("DragSurface");
		originalPosition = transform.position;
		GetComponent<Renderer> ().sharedMaterial = color;
	}

	void OnMouseUp() {
		if (dock != null) {
			dock.gameObject.GetComponent<DockController> ().setDockedColor(color);
			dock = null;
		}
		transform.position = originalPosition;
	}
	
	void OnMouseDrag() {
		Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit floorHit;

		if (Physics.Raycast (camRay, out floorHit, camRayLength, dragLayerMask)) {
			if (dock == null || !dock.bounds.Contains(floorHit.point)) {
				transform.position = new Vector3(floorHit.point.x, transform.position.y, floorHit.point.z);
			} else {
				Vector3 newPos = dock.transform.position;
				newPos.y = transform.position.y;
				transform.position = newPos;
			}
		}
	}
	
	void OnTriggerEnter (Collider other) {
		if (other.gameObject.CompareTag ("Dock")) {
			dock = other;
			dock.gameObject.GetComponent<DockController> ().isOver = true;
		}
	}
	
	void OnTriggerExit(Collider other) {
		if (other.gameObject.CompareTag ("Dock")) {
			other.gameObject.GetComponent<DockController> ().isOver = false;
		}
	}
}
