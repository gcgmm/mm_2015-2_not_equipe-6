﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class BoardController : MonoBehaviour {
	
	public RowController[] rows;
	public Material[] materials;
	public Animator anim;
	public int codeLength;
	public GameController game;
	
	int rowIndex = 0;
	Material[] code;
	
	void Awake() {
		StartNewGame ();
	}
	
	void StartNewGame () {
		code = new Material[codeLength];
		List<Material> list = new List<Material> (materials);
		for (int i = 0; i < codeLength; i++) {
			int index = Random.Range(0, list.Count-1);
			code[i] = list[index];
			list.RemoveAt(index);
		}
		
		foreach (RowController row in rows) {
			row.SetActive(false);
		}
		
		rowIndex = 0;
		rows [rowIndex].SetActive (true);
	}
	
	public void Check(int difficulty) {
		if (rows[rowIndex].IsComplete()) {
			bool result;
			if (difficulty == GameController.EASY) {
				result = rows[rowIndex].CheckCodeEasy(code);
			} else {
				result = rows[rowIndex].CheckCode(code);
			}

			if (result) {
				game.OnGameWin();
			} else {
				rows[rowIndex].SetActive(false);
				rowIndex++;
				if (rowIndex >= 10) {
					game.onGameLoose();
				} else {
					rows[rowIndex].SetActive(true);
				}
			}
		} else {
			anim.SetTrigger("ShowMessage");
		}
	}

	public int GetIndex() {
		return rowIndex;
	}
	
	//	public void Update() {
	//		rows [rowIndex].gameObject.SetActive (true);
	//		rows [rowIndex].gameObject.transform.Rotate (new Vector3 (15, 30, 15) * Time.deltaTime);
	//	}
	
}
