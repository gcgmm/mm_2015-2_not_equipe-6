﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public const int EASY = 1;
	public const int MILD = 2;
	public const int HARD = 3;

	public Image initMenu;
	public Image WinMenu;
	public Image LooseMenu;
	public Text rowCount;
	public GameObject board4;
	public GameObject board6;

	int difficulty;

	GameObject activeBoard;

	public void startEasy() {
		hideInitMenu ();
		board4.SetActive (true);
		activeBoard = board4;
		difficulty = EASY;
	}

	public void startMild() {
		hideInitMenu ();
		board4.SetActive (true);
		activeBoard = board4;
		difficulty = MILD;
	}

	public void startHard() {
		hideInitMenu ();
		board6.SetActive (true);
		activeBoard = board6;
		difficulty = HARD;
	}

	public void CheckCode() {
		activeBoard.GetComponent<BoardController> ().Check (difficulty);
	}

	public void OnGameWin() {
		WinMenu.gameObject.SetActive (true);
		rowCount.text = (activeBoard.GetComponent<BoardController> ().GetIndex () + 1).ToString();
	}

	public void onGameLoose() {
		LooseMenu.gameObject.SetActive (false);
	}

	public void Restart() {
		Application.LoadLevel (Application.loadedLevel);
	}

	void hideInitMenu() {
		initMenu.gameObject.SetActive (false);
	}
}
