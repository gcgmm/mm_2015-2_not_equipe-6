﻿using UnityEngine;
using System.Collections;

public class DockController : MonoBehaviour {

	public int DockIndex;
	public bool isOver = false;

	Material dockedColor = null;
	GameObject dockHolder;
	RowController row;

	void Awake() {
		dockHolder = transform.GetChild (0).gameObject;
		row = transform.parent.gameObject.GetComponent<RowController> ();
	}

	void Update () {
		if (row.IsActive ()) {
			//transform.parent.gameObject.SetActive (true);
			//transform.parent.Rotate (new Vector3 (15, 30, 15) * Time.deltaTime);
		}
		if (dockedColor != null && !isOver) {
			dockHolder.SetActive (true);
			Renderer rend = dockHolder.GetComponent<Renderer> ();
			rend.sharedMaterial = dockedColor;
		} else {
			dockHolder.SetActive(false);
		}
	}

	public void setDockedColor(Material color) {
		if (row.IsActive ()) {
			dockedColor = color;
			row.SetColor(color, DockIndex);
		}
	}

}
