﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// <PrivateImplementationDetails>{C4A9B53A-936F-421A-9398-89108BD15C01}/__StaticArrayInitTypeSize=24
#pragma pack(push, tp, 1)
struct  __StaticArrayInitTypeSizeU3D24_t920 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t920__padding[24];
	};
};
#pragma pack(pop, tp)
