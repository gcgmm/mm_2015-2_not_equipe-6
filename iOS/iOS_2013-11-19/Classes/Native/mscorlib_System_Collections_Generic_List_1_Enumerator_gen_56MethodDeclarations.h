﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t569;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_56.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m18371_gshared (Enumerator_t2593 * __this, List_1_t569 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m18371(__this, ___l, method) (( void (*) (Enumerator_t2593 *, List_1_t569 *, const MethodInfo*))Enumerator__ctor_m18371_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m18372_gshared (Enumerator_t2593 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m18372(__this, method) (( void (*) (Enumerator_t2593 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m18372_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18373_gshared (Enumerator_t2593 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18373(__this, method) (( Object_t * (*) (Enumerator_t2593 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18373_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C" void Enumerator_Dispose_m18374_gshared (Enumerator_t2593 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m18374(__this, method) (( void (*) (Enumerator_t2593 *, const MethodInfo*))Enumerator_Dispose_m18374_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m18375_gshared (Enumerator_t2593 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m18375(__this, method) (( void (*) (Enumerator_t2593 *, const MethodInfo*))Enumerator_VerifyState_m18375_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m18376_gshared (Enumerator_t2593 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m18376(__this, method) (( bool (*) (Enumerator_t2593 *, const MethodInfo*))Enumerator_MoveNext_m18376_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C" UILineInfo_t428  Enumerator_get_Current_m18377_gshared (Enumerator_t2593 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m18377(__this, method) (( UILineInfo_t428  (*) (Enumerator_t2593 *, const MethodInfo*))Enumerator_get_Current_m18377_gshared)(__this, method)
