﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// Vuforia.SmartTerrainInitializationInfo
struct  SmartTerrainInitializationInfo_t760 
{
	union
	{
		struct
		{
		};
		uint8_t SmartTerrainInitializationInfo_t760__padding[1];
	};
};
