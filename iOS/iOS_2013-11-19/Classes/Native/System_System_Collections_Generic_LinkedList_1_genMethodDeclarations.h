﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t827;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t687;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3132;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t978;
// System.Int32[]
struct Int32U5BU5D_t122;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor()
extern "C" void LinkedList_1__ctor_m5328_gshared (LinkedList_1_t827 * __this, const MethodInfo* method);
#define LinkedList_1__ctor_m5328(__this, method) (( void (*) (LinkedList_1_t827 *, const MethodInfo*))LinkedList_1__ctor_m5328_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1__ctor_m20914_gshared (LinkedList_1_t827 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define LinkedList_1__ctor_m20914(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t827 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))LinkedList_1__ctor_m20914_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20915_gshared (LinkedList_1_t827 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20915(__this, ___value, method) (( void (*) (LinkedList_1_t827 *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20915_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m20916_gshared (LinkedList_1_t827 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m20916(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t827 *, Array_t *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m20916_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20917_gshared (LinkedList_1_t827 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20917(__this, method) (( Object_t* (*) (LinkedList_1_t827 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20917_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m20918_gshared (LinkedList_1_t827 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m20918(__this, method) (( Object_t * (*) (LinkedList_1_t827 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m20918_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20919_gshared (LinkedList_1_t827 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20919(__this, method) (( bool (*) (LinkedList_1_t827 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20919_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m20920_gshared (LinkedList_1_t827 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m20920(__this, method) (( bool (*) (LinkedList_1_t827 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m20920_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m20921_gshared (LinkedList_1_t827 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m20921(__this, method) (( Object_t * (*) (LinkedList_1_t827 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m20921_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_VerifyReferencedNode_m20922_gshared (LinkedList_1_t827 * __this, LinkedListNode_1_t978 * ___node, const MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m20922(__this, ___node, method) (( void (*) (LinkedList_1_t827 *, LinkedListNode_1_t978 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m20922_gshared)(__this, ___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::AddLast(T)
extern "C" LinkedListNode_1_t978 * LinkedList_1_AddLast_m5329_gshared (LinkedList_1_t827 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_AddLast_m5329(__this, ___value, method) (( LinkedListNode_1_t978 * (*) (LinkedList_1_t827 *, int32_t, const MethodInfo*))LinkedList_1_AddLast_m5329_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Clear()
extern "C" void LinkedList_1_Clear_m20923_gshared (LinkedList_1_t827 * __this, const MethodInfo* method);
#define LinkedList_1_Clear_m20923(__this, method) (( void (*) (LinkedList_1_t827 *, const MethodInfo*))LinkedList_1_Clear_m20923_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(T)
extern "C" bool LinkedList_1_Contains_m20924_gshared (LinkedList_1_t827 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Contains_m20924(__this, ___value, method) (( bool (*) (LinkedList_1_t827 *, int32_t, const MethodInfo*))LinkedList_1_Contains_m20924_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void LinkedList_1_CopyTo_m20925_gshared (LinkedList_1_t827 * __this, Int32U5BU5D_t122* ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_CopyTo_m20925(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t827 *, Int32U5BU5D_t122*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m20925_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::Find(T)
extern "C" LinkedListNode_1_t978 * LinkedList_1_Find_m20926_gshared (LinkedList_1_t827 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Find_m20926(__this, ___value, method) (( LinkedListNode_1_t978 * (*) (LinkedList_1_t827 *, int32_t, const MethodInfo*))LinkedList_1_Find_m20926_gshared)(__this, ___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t2772  LinkedList_1_GetEnumerator_m20927_gshared (LinkedList_1_t827 * __this, const MethodInfo* method);
#define LinkedList_1_GetEnumerator_m20927(__this, method) (( Enumerator_t2772  (*) (LinkedList_1_t827 *, const MethodInfo*))LinkedList_1_GetEnumerator_m20927_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1_GetObjectData_m20928_gshared (LinkedList_1_t827 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define LinkedList_1_GetObjectData_m20928(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t827 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))LinkedList_1_GetObjectData_m20928_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::OnDeserialization(System.Object)
extern "C" void LinkedList_1_OnDeserialization_m20929_gshared (LinkedList_1_t827 * __this, Object_t * ___sender, const MethodInfo* method);
#define LinkedList_1_OnDeserialization_m20929(__this, ___sender, method) (( void (*) (LinkedList_1_t827 *, Object_t *, const MethodInfo*))LinkedList_1_OnDeserialization_m20929_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(T)
extern "C" bool LinkedList_1_Remove_m20930_gshared (LinkedList_1_t827 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Remove_m20930(__this, ___value, method) (( bool (*) (LinkedList_1_t827 *, int32_t, const MethodInfo*))LinkedList_1_Remove_m20930_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_Remove_m5483_gshared (LinkedList_1_t827 * __this, LinkedListNode_1_t978 * ___node, const MethodInfo* method);
#define LinkedList_1_Remove_m5483(__this, ___node, method) (( void (*) (LinkedList_1_t827 *, LinkedListNode_1_t978 *, const MethodInfo*))LinkedList_1_Remove_m5483_gshared)(__this, ___node, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::RemoveLast()
extern "C" void LinkedList_1_RemoveLast_m20931_gshared (LinkedList_1_t827 * __this, const MethodInfo* method);
#define LinkedList_1_RemoveLast_m20931(__this, method) (( void (*) (LinkedList_1_t827 *, const MethodInfo*))LinkedList_1_RemoveLast_m20931_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Int32>::get_Count()
extern "C" int32_t LinkedList_1_get_Count_m20932_gshared (LinkedList_1_t827 * __this, const MethodInfo* method);
#define LinkedList_1_get_Count_m20932(__this, method) (( int32_t (*) (LinkedList_1_t827 *, const MethodInfo*))LinkedList_1_get_Count_m20932_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::get_First()
extern "C" LinkedListNode_1_t978 * LinkedList_1_get_First_m5337_gshared (LinkedList_1_t827 * __this, const MethodInfo* method);
#define LinkedList_1_get_First_m5337(__this, method) (( LinkedListNode_1_t978 * (*) (LinkedList_1_t827 *, const MethodInfo*))LinkedList_1_get_First_m5337_gshared)(__this, method)
