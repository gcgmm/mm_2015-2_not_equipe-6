﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_80.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24294_gshared (InternalEnumerator_1_t2984 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m24294(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2984 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m24294_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24295_gshared (InternalEnumerator_1_t2984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24295(__this, method) (( void (*) (InternalEnumerator_1_t2984 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24295_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24296_gshared (InternalEnumerator_1_t2984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24296(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2984 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24296_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24297_gshared (InternalEnumerator_1_t2984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24297(__this, method) (( void (*) (InternalEnumerator_1_t2984 *, const MethodInfo*))InternalEnumerator_1_Dispose_m24297_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24298_gshared (InternalEnumerator_1_t2984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24298(__this, method) (( bool (*) (InternalEnumerator_1_t2984 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m24298_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern "C" Link_t2983  InternalEnumerator_1_get_Current_m24299_gshared (InternalEnumerator_1_t2984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24299(__this, method) (( Link_t2983  (*) (InternalEnumerator_1_t2984 *, const MethodInfo*))InternalEnumerator_1_get_Current_m24299_gshared)(__this, method)
