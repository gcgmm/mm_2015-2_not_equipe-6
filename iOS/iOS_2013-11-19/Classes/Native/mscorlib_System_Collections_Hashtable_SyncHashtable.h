﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1164;

#include "mscorlib_System_Collections_Hashtable.h"

// System.Collections.Hashtable/SyncHashtable
struct  SyncHashtable_t1563  : public Hashtable_t1164
{
	// System.Collections.Hashtable System.Collections.Hashtable/SyncHashtable::host
	Hashtable_t1164 * ___host_14;
};
