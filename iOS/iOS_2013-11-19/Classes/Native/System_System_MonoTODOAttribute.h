﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Attribute.h"

// System.MonoTODOAttribute
struct  MonoTODOAttribute_t1274  : public Attribute_t104
{
	// System.String System.MonoTODOAttribute::comment
	String_t* ___comment_0;
};
