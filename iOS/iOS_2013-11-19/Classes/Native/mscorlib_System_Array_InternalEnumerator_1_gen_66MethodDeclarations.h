﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_66.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__10.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m20958_gshared (InternalEnumerator_1_t2775 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m20958(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2775 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m20958_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20959_gshared (InternalEnumerator_1_t2775 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20959(__this, method) (( void (*) (InternalEnumerator_1_t2775 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20959_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20960_gshared (InternalEnumerator_1_t2775 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20960(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2775 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20960_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m20961_gshared (InternalEnumerator_1_t2775 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m20961(__this, method) (( void (*) (InternalEnumerator_1_t2775 *, const MethodInfo*))InternalEnumerator_1_Dispose_m20961_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m20962_gshared (InternalEnumerator_1_t2775 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m20962(__this, method) (( bool (*) (InternalEnumerator_1_t2775 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m20962_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::get_Current()
extern "C" PropData_t820  InternalEnumerator_1_get_Current_m20963_gshared (InternalEnumerator_1_t2775 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m20963(__this, method) (( PropData_t820  (*) (InternalEnumerator_1_t2775 *, const MethodInfo*))InternalEnumerator_1_get_Current_m20963_gshared)(__this, method)
