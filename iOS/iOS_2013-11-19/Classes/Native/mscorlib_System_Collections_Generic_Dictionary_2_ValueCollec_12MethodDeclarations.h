﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_30MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m21958(__this, ___host, method) (( void (*) (Enumerator_t989 *, Dictionary_2_t859 *, const MethodInfo*))Enumerator__ctor_m15354_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21959(__this, method) (( Object_t * (*) (Enumerator_t989 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15355_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m21960(__this, method) (( void (*) (Enumerator_t989 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15356_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Dispose()
#define Enumerator_Dispose_m5371(__this, method) (( void (*) (Enumerator_t989 *, const MethodInfo*))Enumerator_Dispose_m15357_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::MoveNext()
#define Enumerator_MoveNext_m5370(__this, method) (( bool (*) (Enumerator_t989 *, const MethodInfo*))Enumerator_MoveNext_m15358_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Current()
#define Enumerator_get_Current_m5368(__this, method) (( List_1_t858 * (*) (Enumerator_t989 *, const MethodInfo*))Enumerator_get_Current_m15359_gshared)(__this, method)
