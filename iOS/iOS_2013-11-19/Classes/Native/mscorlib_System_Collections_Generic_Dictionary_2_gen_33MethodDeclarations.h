﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t2706;
// System.Collections.Generic.IEqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>
struct IEqualityComparer_1_t2705;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t687;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>[]
struct KeyValuePair_2U5BU5D_t3201;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>
struct IEnumerator_1_t3202;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1238;
// System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct KeyCollection_t2711;
// System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct ValueCollection_t2715;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_17.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m19848_gshared (Dictionary_2_t2706 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m19848(__this, method) (( void (*) (Dictionary_2_t2706 *, const MethodInfo*))Dictionary_2__ctor_m19848_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m19850_gshared (Dictionary_2_t2706 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m19850(__this, ___comparer, method) (( void (*) (Dictionary_2_t2706 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m19850_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m19852_gshared (Dictionary_2_t2706 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m19852(__this, ___capacity, method) (( void (*) (Dictionary_2_t2706 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m19852_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m19854_gshared (Dictionary_2_t2706 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m19854(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2706 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2__ctor_m19854_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m19856_gshared (Dictionary_2_t2706 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m19856(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2706 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m19856_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m19858_gshared (Dictionary_2_t2706 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m19858(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2706 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m19858_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m19860_gshared (Dictionary_2_t2706 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m19860(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2706 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m19860_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m19862_gshared (Dictionary_2_t2706 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m19862(__this, ___key, method) (( bool (*) (Dictionary_2_t2706 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m19862_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m19864_gshared (Dictionary_2_t2706 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m19864(__this, ___key, method) (( void (*) (Dictionary_2_t2706 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m19864_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m19866_gshared (Dictionary_2_t2706 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m19866(__this, method) (( bool (*) (Dictionary_2_t2706 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m19866_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m19868_gshared (Dictionary_2_t2706 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m19868(__this, method) (( Object_t * (*) (Dictionary_2_t2706 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m19868_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m19870_gshared (Dictionary_2_t2706 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m19870(__this, method) (( bool (*) (Dictionary_2_t2706 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m19870_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m19872_gshared (Dictionary_2_t2706 * __this, KeyValuePair_2_t2708  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m19872(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2706 *, KeyValuePair_2_t2708 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m19872_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19874_gshared (Dictionary_2_t2706 * __this, KeyValuePair_2_t2708  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19874(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2706 *, KeyValuePair_2_t2708 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19874_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m19876_gshared (Dictionary_2_t2706 * __this, KeyValuePair_2U5BU5D_t3201* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m19876(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2706 *, KeyValuePair_2U5BU5D_t3201*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m19876_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m19878_gshared (Dictionary_2_t2706 * __this, KeyValuePair_2_t2708  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m19878(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2706 *, KeyValuePair_2_t2708 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m19878_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m19880_gshared (Dictionary_2_t2706 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m19880(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2706 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m19880_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19882_gshared (Dictionary_2_t2706 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19882(__this, method) (( Object_t * (*) (Dictionary_2_t2706 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19882_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m19884_gshared (Dictionary_2_t2706 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m19884(__this, method) (( Object_t* (*) (Dictionary_2_t2706 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m19884_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m19886_gshared (Dictionary_2_t2706 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m19886(__this, method) (( Object_t * (*) (Dictionary_2_t2706 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m19886_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m19888_gshared (Dictionary_2_t2706 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m19888(__this, method) (( int32_t (*) (Dictionary_2_t2706 *, const MethodInfo*))Dictionary_2_get_Count_m19888_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m19890_gshared (Dictionary_2_t2706 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m19890(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2706 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m19890_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m19892_gshared (Dictionary_2_t2706 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m19892(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2706 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m19892_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m19894_gshared (Dictionary_2_t2706 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m19894(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2706 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m19894_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m19896_gshared (Dictionary_2_t2706 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m19896(__this, ___size, method) (( void (*) (Dictionary_2_t2706 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m19896_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m19898_gshared (Dictionary_2_t2706 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m19898(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2706 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m19898_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2708  Dictionary_2_make_pair_m19900_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m19900(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2708  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m19900_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m19902_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m19902(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m19902_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m19904_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m19904(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m19904_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m19906_gshared (Dictionary_2_t2706 * __this, KeyValuePair_2U5BU5D_t3201* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m19906(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2706 *, KeyValuePair_2U5BU5D_t3201*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m19906_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m19908_gshared (Dictionary_2_t2706 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m19908(__this, method) (( void (*) (Dictionary_2_t2706 *, const MethodInfo*))Dictionary_2_Resize_m19908_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m19910_gshared (Dictionary_2_t2706 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m19910(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2706 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m19910_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m19912_gshared (Dictionary_2_t2706 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m19912(__this, method) (( void (*) (Dictionary_2_t2706 *, const MethodInfo*))Dictionary_2_Clear_m19912_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m19914_gshared (Dictionary_2_t2706 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m19914(__this, ___key, method) (( bool (*) (Dictionary_2_t2706 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m19914_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m19916_gshared (Dictionary_2_t2706 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m19916(__this, ___value, method) (( bool (*) (Dictionary_2_t2706 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m19916_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m19918_gshared (Dictionary_2_t2706 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m19918(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2706 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2_GetObjectData_m19918_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m19920_gshared (Dictionary_2_t2706 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m19920(__this, ___sender, method) (( void (*) (Dictionary_2_t2706 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m19920_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m19922_gshared (Dictionary_2_t2706 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m19922(__this, ___key, method) (( bool (*) (Dictionary_2_t2706 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m19922_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m19924_gshared (Dictionary_2_t2706 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m19924(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2706 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m19924_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Keys()
extern "C" KeyCollection_t2711 * Dictionary_2_get_Keys_m19926_gshared (Dictionary_2_t2706 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m19926(__this, method) (( KeyCollection_t2711 * (*) (Dictionary_2_t2706 *, const MethodInfo*))Dictionary_2_get_Keys_m19926_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Values()
extern "C" ValueCollection_t2715 * Dictionary_2_get_Values_m19927_gshared (Dictionary_2_t2706 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m19927(__this, method) (( ValueCollection_t2715 * (*) (Dictionary_2_t2706 *, const MethodInfo*))Dictionary_2_get_Values_m19927_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m19929_gshared (Dictionary_2_t2706 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m19929(__this, ___key, method) (( int32_t (*) (Dictionary_2_t2706 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m19929_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m19931_gshared (Dictionary_2_t2706 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m19931(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t2706 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m19931_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m19933_gshared (Dictionary_2_t2706 * __this, KeyValuePair_2_t2708  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m19933(__this, ___pair, method) (( bool (*) (Dictionary_2_t2706 *, KeyValuePair_2_t2708 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m19933_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::GetEnumerator()
extern "C" Enumerator_t2713  Dictionary_2_GetEnumerator_m19935_gshared (Dictionary_2_t2706 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m19935(__this, method) (( Enumerator_t2713  (*) (Dictionary_2_t2706 *, const MethodInfo*))Dictionary_2_GetEnumerator_m19935_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1423  Dictionary_2_U3CCopyToU3Em__0_m19937_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m19937(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1423  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m19937_gshared)(__this /* static, unused */, ___key, ___value, method)
