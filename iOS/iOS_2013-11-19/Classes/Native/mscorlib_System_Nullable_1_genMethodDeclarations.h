﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C" void Nullable_1__ctor_m12775_gshared (Nullable_1_t2120 * __this, TimeSpan_t975  ___value, const MethodInfo* method);
#define Nullable_1__ctor_m12775(__this, ___value, method) (( void (*) (Nullable_1_t2120 *, TimeSpan_t975 , const MethodInfo*))Nullable_1__ctor_m12775_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m12776_gshared (Nullable_1_t2120 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m12776(__this, method) (( bool (*) (Nullable_1_t2120 *, const MethodInfo*))Nullable_1_get_HasValue_m12776_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C" TimeSpan_t975  Nullable_1_get_Value_m12777_gshared (Nullable_1_t2120 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m12777(__this, method) (( TimeSpan_t975  (*) (Nullable_1_t2120 *, const MethodInfo*))Nullable_1_get_Value_m12777_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m25506_gshared (Nullable_1_t2120 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m25506(__this, ___other, method) (( bool (*) (Nullable_1_t2120 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m25506_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m25507_gshared (Nullable_1_t2120 * __this, Nullable_1_t2120  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m25507(__this, ___other, method) (( bool (*) (Nullable_1_t2120 *, Nullable_1_t2120 , const MethodInfo*))Nullable_1_Equals_m25507_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m25508_gshared (Nullable_1_t2120 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m25508(__this, method) (( int32_t (*) (Nullable_1_t2120 *, const MethodInfo*))Nullable_1_GetHashCode_m25508_gshared)(__this, method)
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C" String_t* Nullable_1_ToString_m25509_gshared (Nullable_1_t2120 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m25509(__this, method) (( String_t* (*) (Nullable_1_t2120 *, const MethodInfo*))Nullable_1_ToString_m25509_gshared)(__this, method)
