﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_26MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m22609(__this, ___host, method) (( void (*) (Enumerator_t1009 *, Dictionary_2_t875 *, const MethodInfo*))Enumerator__ctor_m14335_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22610(__this, method) (( Object_t * (*) (Enumerator_t1009 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14336_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m22611(__this, method) (( void (*) (Enumerator_t1009 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14337_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m5433(__this, method) (( void (*) (Enumerator_t1009 *, const MethodInfo*))Enumerator_Dispose_m14338_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m5432(__this, method) (( bool (*) (Enumerator_t1009 *, const MethodInfo*))Enumerator_MoveNext_m14339_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m5431(__this, method) (( PropAbstractBehaviour_t52 * (*) (Enumerator_t1009 *, const MethodInfo*))Enumerator_get_Current_m14340_gshared)(__this, method)
