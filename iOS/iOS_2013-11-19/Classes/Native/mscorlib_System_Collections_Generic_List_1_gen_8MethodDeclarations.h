﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_63MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::.ctor()
#define List_1__ctor_m1768(__this, method) (( void (*) (List_1_t181 *, const MethodInfo*))List_1__ctor_m12803_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m14427(__this, ___collection, method) (( void (*) (List_1_t181 *, Object_t*, const MethodInfo*))List_1__ctor_m12804_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::.ctor(System.Int32)
#define List_1__ctor_m14428(__this, ___capacity, method) (( void (*) (List_1_t181 *, int32_t, const MethodInfo*))List_1__ctor_m12806_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::.cctor()
#define List_1__cctor_m14429(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m12808_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14430(__this, method) (( Object_t* (*) (List_1_t181 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12810_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m14431(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t181 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m12812_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m14432(__this, method) (( Object_t * (*) (List_1_t181 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m12814_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m14433(__this, ___item, method) (( int32_t (*) (List_1_t181 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m12816_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m14434(__this, ___item, method) (( bool (*) (List_1_t181 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m12818_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m14435(__this, ___item, method) (( int32_t (*) (List_1_t181 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m12820_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m14436(__this, ___index, ___item, method) (( void (*) (List_1_t181 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m12822_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m14437(__this, ___item, method) (( void (*) (List_1_t181 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m12824_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14438(__this, method) (( bool (*) (List_1_t181 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m14439(__this, method) (( bool (*) (List_1_t181 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m12828_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m14440(__this, method) (( Object_t * (*) (List_1_t181 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m12830_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m14441(__this, method) (( bool (*) (List_1_t181 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m12832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m14442(__this, method) (( bool (*) (List_1_t181 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m12834_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m14443(__this, ___index, method) (( Object_t * (*) (List_1_t181 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m12836_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m14444(__this, ___index, ___value, method) (( void (*) (List_1_t181 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12838_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Add(T)
#define List_1_Add_m14445(__this, ___item, method) (( void (*) (List_1_t181 *, ButtonState_t178 *, const MethodInfo*))List_1_Add_m12840_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m14446(__this, ___newCount, method) (( void (*) (List_1_t181 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m12842_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m14447(__this, ___collection, method) (( void (*) (List_1_t181 *, Object_t*, const MethodInfo*))List_1_AddCollection_m12844_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m14448(__this, ___enumerable, method) (( void (*) (List_1_t181 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m12846_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m14449(__this, ___collection, method) (( void (*) (List_1_t181 *, Object_t*, const MethodInfo*))List_1_AddRange_m12848_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::AsReadOnly()
#define List_1_AsReadOnly_m14450(__this, method) (( ReadOnlyCollection_1_t2298 * (*) (List_1_t181 *, const MethodInfo*))List_1_AsReadOnly_m12850_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Clear()
#define List_1_Clear_m14451(__this, method) (( void (*) (List_1_t181 *, const MethodInfo*))List_1_Clear_m12852_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Contains(T)
#define List_1_Contains_m14452(__this, ___item, method) (( bool (*) (List_1_t181 *, ButtonState_t178 *, const MethodInfo*))List_1_Contains_m12854_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m14453(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t181 *, ButtonStateU5BU5D_t2297*, int32_t, const MethodInfo*))List_1_CopyTo_m12856_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Find(System.Predicate`1<T>)
#define List_1_Find_m14454(__this, ___match, method) (( ButtonState_t178 * (*) (List_1_t181 *, Predicate_1_t2300 *, const MethodInfo*))List_1_Find_m12858_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m14455(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2300 *, const MethodInfo*))List_1_CheckMatch_m12860_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m14456(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t181 *, int32_t, int32_t, Predicate_1_t2300 *, const MethodInfo*))List_1_GetIndex_m12862_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::GetEnumerator()
#define List_1_GetEnumerator_m14457(__this, method) (( Enumerator_t2301  (*) (List_1_t181 *, const MethodInfo*))List_1_GetEnumerator_m12864_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::IndexOf(T)
#define List_1_IndexOf_m14458(__this, ___item, method) (( int32_t (*) (List_1_t181 *, ButtonState_t178 *, const MethodInfo*))List_1_IndexOf_m12866_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m14459(__this, ___start, ___delta, method) (( void (*) (List_1_t181 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m12868_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m14460(__this, ___index, method) (( void (*) (List_1_t181 *, int32_t, const MethodInfo*))List_1_CheckIndex_m12870_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Insert(System.Int32,T)
#define List_1_Insert_m14461(__this, ___index, ___item, method) (( void (*) (List_1_t181 *, int32_t, ButtonState_t178 *, const MethodInfo*))List_1_Insert_m12872_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m14462(__this, ___collection, method) (( void (*) (List_1_t181 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m12874_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Remove(T)
#define List_1_Remove_m14463(__this, ___item, method) (( bool (*) (List_1_t181 *, ButtonState_t178 *, const MethodInfo*))List_1_Remove_m12876_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m14464(__this, ___match, method) (( int32_t (*) (List_1_t181 *, Predicate_1_t2300 *, const MethodInfo*))List_1_RemoveAll_m12878_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m14465(__this, ___index, method) (( void (*) (List_1_t181 *, int32_t, const MethodInfo*))List_1_RemoveAt_m12880_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Reverse()
#define List_1_Reverse_m14466(__this, method) (( void (*) (List_1_t181 *, const MethodInfo*))List_1_Reverse_m12882_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Sort()
#define List_1_Sort_m14467(__this, method) (( void (*) (List_1_t181 *, const MethodInfo*))List_1_Sort_m12884_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m14468(__this, ___comparison, method) (( void (*) (List_1_t181 *, Comparison_1_t2302 *, const MethodInfo*))List_1_Sort_m12886_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::ToArray()
#define List_1_ToArray_m14469(__this, method) (( ButtonStateU5BU5D_t2297* (*) (List_1_t181 *, const MethodInfo*))List_1_ToArray_m12888_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::TrimExcess()
#define List_1_TrimExcess_m14470(__this, method) (( void (*) (List_1_t181 *, const MethodInfo*))List_1_TrimExcess_m12890_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::get_Capacity()
#define List_1_get_Capacity_m14471(__this, method) (( int32_t (*) (List_1_t181 *, const MethodInfo*))List_1_get_Capacity_m12892_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m14472(__this, ___value, method) (( void (*) (List_1_t181 *, int32_t, const MethodInfo*))List_1_set_Capacity_m12894_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::get_Count()
#define List_1_get_Count_m14473(__this, method) (( int32_t (*) (List_1_t181 *, const MethodInfo*))List_1_get_Count_m12896_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::get_Item(System.Int32)
#define List_1_get_Item_m14474(__this, ___index, method) (( ButtonState_t178 * (*) (List_1_t181 *, int32_t, const MethodInfo*))List_1_get_Item_m12898_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::set_Item(System.Int32,T)
#define List_1_set_Item_m14475(__this, ___index, ___value, method) (( void (*) (List_1_t181 *, int32_t, ButtonState_t178 *, const MethodInfo*))List_1_set_Item_m12900_gshared)(__this, ___index, ___value, method)
