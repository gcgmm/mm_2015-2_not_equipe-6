﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUIStyle>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m18660(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2614 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m12785_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUIStyle>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18661(__this, method) (( void (*) (InternalEnumerator_1_t2614 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12787_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18662(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2614 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12789_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUIStyle>::Dispose()
#define InternalEnumerator_1_Dispose_m18663(__this, method) (( void (*) (InternalEnumerator_1_t2614 *, const MethodInfo*))InternalEnumerator_1_Dispose_m12791_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GUIStyle>::MoveNext()
#define InternalEnumerator_1_MoveNext_m18664(__this, method) (( bool (*) (InternalEnumerator_1_t2614 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m12793_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.GUIStyle>::get_Current()
#define InternalEnumerator_1_get_Current_m18665(__this, method) (( GUIStyle_t584 * (*) (InternalEnumerator_1_t2614 *, const MethodInfo*))InternalEnumerator_1_get_Current_m12795_gshared)(__this, method)
