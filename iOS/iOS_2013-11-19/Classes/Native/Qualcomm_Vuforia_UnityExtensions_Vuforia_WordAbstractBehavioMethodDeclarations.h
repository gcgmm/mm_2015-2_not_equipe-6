﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t80;
// Vuforia.Word
struct Word_t862;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t84;
// UnityEngine.GameObject
struct GameObject_t9;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"

// System.Void Vuforia.WordAbstractBehaviour::InternalUnregisterTrackable()
extern "C" void WordAbstractBehaviour_InternalUnregisterTrackable_m5215 (WordAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Word Vuforia.WordAbstractBehaviour::get_Word()
extern "C" Object_t * WordAbstractBehaviour_get_Word_m5216 (WordAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_SpecificWord()
extern "C" String_t* WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m5217 (WordAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetSpecificWord(System.String)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m5218 (WordAbstractBehaviour_t80 * __this, String_t* ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_Mode()
extern "C" int32_t WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m5219 (WordAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsTemplateMode()
extern "C" bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m5220 (WordAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode()
extern "C" bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m5221 (WordAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetMode(Vuforia.WordTemplateMode)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m5222 (WordAbstractBehaviour_t80 * __this, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.InitializeWord(Vuforia.Word)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m5223 (WordAbstractBehaviour_t80 * __this, Object_t * ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
extern "C" void WordAbstractBehaviour__ctor_m302 (WordAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m5224 (WordAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m5225 (WordAbstractBehaviour_t80 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t84 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m5226 (WordAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t9 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m5227 (WordAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
