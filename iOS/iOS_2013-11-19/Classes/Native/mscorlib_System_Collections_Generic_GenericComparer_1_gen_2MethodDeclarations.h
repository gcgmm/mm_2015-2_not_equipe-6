﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct GenericComparer_1_t2168;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
extern "C" void GenericComparer_1__ctor_m12781_gshared (GenericComparer_1_t2168 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m12781(__this, method) (( void (*) (GenericComparer_1_t2168 *, const MethodInfo*))GenericComparer_1__ctor_m12781_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m25563_gshared (GenericComparer_1_t2168 * __this, TimeSpan_t975  ___x, TimeSpan_t975  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m25563(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2168 *, TimeSpan_t975 , TimeSpan_t975 , const MethodInfo*))GenericComparer_1_Compare_m25563_gshared)(__this, ___x, ___y, method)
