﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t348;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector4>
struct IEnumerable_1_t3173;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector4>
struct IEnumerator_1_t3174;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector4>
struct ICollection_1_t3175;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>
struct ReadOnlyCollection_1_t2521;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t457;
// System.Predicate`1<UnityEngine.Vector4>
struct Predicate_1_t2526;
// System.Comparison`1<UnityEngine.Vector4>
struct Comparison_1_t2529;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_53.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor()
extern "C" void List_1__ctor_m17570_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1__ctor_m17570(__this, method) (( void (*) (List_1_t348 *, const MethodInfo*))List_1__ctor_m17570_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m17571_gshared (List_1_t348 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m17571(__this, ___collection, method) (( void (*) (List_1_t348 *, Object_t*, const MethodInfo*))List_1__ctor_m17571_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor(System.Int32)
extern "C" void List_1__ctor_m17572_gshared (List_1_t348 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m17572(__this, ___capacity, method) (( void (*) (List_1_t348 *, int32_t, const MethodInfo*))List_1__ctor_m17572_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.cctor()
extern "C" void List_1__cctor_m17573_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m17573(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m17573_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17574_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17574(__this, method) (( Object_t* (*) (List_1_t348 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17574_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17575_gshared (List_1_t348 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m17575(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t348 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m17575_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m17576_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17576(__this, method) (( Object_t * (*) (List_1_t348 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m17576_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m17577_gshared (List_1_t348 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m17577(__this, ___item, method) (( int32_t (*) (List_1_t348 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m17577_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m17578_gshared (List_1_t348 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m17578(__this, ___item, method) (( bool (*) (List_1_t348 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m17578_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m17579_gshared (List_1_t348 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m17579(__this, ___item, method) (( int32_t (*) (List_1_t348 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m17579_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m17580_gshared (List_1_t348 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m17580(__this, ___index, ___item, method) (( void (*) (List_1_t348 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m17580_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m17581_gshared (List_1_t348 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m17581(__this, ___item, method) (( void (*) (List_1_t348 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m17581_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17582_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17582(__this, method) (( bool (*) (List_1_t348 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17582_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m17583_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17583(__this, method) (( bool (*) (List_1_t348 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m17583_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m17584_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m17584(__this, method) (( Object_t * (*) (List_1_t348 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m17584_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m17585_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m17585(__this, method) (( bool (*) (List_1_t348 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m17585_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m17586_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m17586(__this, method) (( bool (*) (List_1_t348 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m17586_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m17587_gshared (List_1_t348 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m17587(__this, ___index, method) (( Object_t * (*) (List_1_t348 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m17587_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m17588_gshared (List_1_t348 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m17588(__this, ___index, ___value, method) (( void (*) (List_1_t348 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m17588_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Add(T)
extern "C" void List_1_Add_m17589_gshared (List_1_t348 * __this, Vector4_t350  ___item, const MethodInfo* method);
#define List_1_Add_m17589(__this, ___item, method) (( void (*) (List_1_t348 *, Vector4_t350 , const MethodInfo*))List_1_Add_m17589_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m17590_gshared (List_1_t348 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m17590(__this, ___newCount, method) (( void (*) (List_1_t348 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m17590_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m17591_gshared (List_1_t348 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m17591(__this, ___collection, method) (( void (*) (List_1_t348 *, Object_t*, const MethodInfo*))List_1_AddCollection_m17591_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m17592_gshared (List_1_t348 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m17592(__this, ___enumerable, method) (( void (*) (List_1_t348 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m17592_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m2306_gshared (List_1_t348 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m2306(__this, ___collection, method) (( void (*) (List_1_t348 *, Object_t*, const MethodInfo*))List_1_AddRange_m2306_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2521 * List_1_AsReadOnly_m17593_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m17593(__this, method) (( ReadOnlyCollection_1_t2521 * (*) (List_1_t348 *, const MethodInfo*))List_1_AsReadOnly_m17593_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Clear()
extern "C" void List_1_Clear_m17594_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_Clear_m17594(__this, method) (( void (*) (List_1_t348 *, const MethodInfo*))List_1_Clear_m17594_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::Contains(T)
extern "C" bool List_1_Contains_m17595_gshared (List_1_t348 * __this, Vector4_t350  ___item, const MethodInfo* method);
#define List_1_Contains_m17595(__this, ___item, method) (( bool (*) (List_1_t348 *, Vector4_t350 , const MethodInfo*))List_1_Contains_m17595_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m17596_gshared (List_1_t348 * __this, Vector4U5BU5D_t457* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m17596(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t348 *, Vector4U5BU5D_t457*, int32_t, const MethodInfo*))List_1_CopyTo_m17596_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector4>::Find(System.Predicate`1<T>)
extern "C" Vector4_t350  List_1_Find_m17597_gshared (List_1_t348 * __this, Predicate_1_t2526 * ___match, const MethodInfo* method);
#define List_1_Find_m17597(__this, ___match, method) (( Vector4_t350  (*) (List_1_t348 *, Predicate_1_t2526 *, const MethodInfo*))List_1_Find_m17597_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m17598_gshared (Object_t * __this /* static, unused */, Predicate_1_t2526 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m17598(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2526 *, const MethodInfo*))List_1_CheckMatch_m17598_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m17599_gshared (List_1_t348 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2526 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m17599(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t348 *, int32_t, int32_t, Predicate_1_t2526 *, const MethodInfo*))List_1_GetIndex_m17599_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::GetEnumerator()
extern "C" Enumerator_t2520  List_1_GetEnumerator_m17600_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m17600(__this, method) (( Enumerator_t2520  (*) (List_1_t348 *, const MethodInfo*))List_1_GetEnumerator_m17600_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m17601_gshared (List_1_t348 * __this, Vector4_t350  ___item, const MethodInfo* method);
#define List_1_IndexOf_m17601(__this, ___item, method) (( int32_t (*) (List_1_t348 *, Vector4_t350 , const MethodInfo*))List_1_IndexOf_m17601_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m17602_gshared (List_1_t348 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m17602(__this, ___start, ___delta, method) (( void (*) (List_1_t348 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m17602_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m17603_gshared (List_1_t348 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m17603(__this, ___index, method) (( void (*) (List_1_t348 *, int32_t, const MethodInfo*))List_1_CheckIndex_m17603_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m17604_gshared (List_1_t348 * __this, int32_t ___index, Vector4_t350  ___item, const MethodInfo* method);
#define List_1_Insert_m17604(__this, ___index, ___item, method) (( void (*) (List_1_t348 *, int32_t, Vector4_t350 , const MethodInfo*))List_1_Insert_m17604_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m17605_gshared (List_1_t348 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m17605(__this, ___collection, method) (( void (*) (List_1_t348 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m17605_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::Remove(T)
extern "C" bool List_1_Remove_m17606_gshared (List_1_t348 * __this, Vector4_t350  ___item, const MethodInfo* method);
#define List_1_Remove_m17606(__this, ___item, method) (( bool (*) (List_1_t348 *, Vector4_t350 , const MethodInfo*))List_1_Remove_m17606_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m17607_gshared (List_1_t348 * __this, Predicate_1_t2526 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m17607(__this, ___match, method) (( int32_t (*) (List_1_t348 *, Predicate_1_t2526 *, const MethodInfo*))List_1_RemoveAll_m17607_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m17608_gshared (List_1_t348 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m17608(__this, ___index, method) (( void (*) (List_1_t348 *, int32_t, const MethodInfo*))List_1_RemoveAt_m17608_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Reverse()
extern "C" void List_1_Reverse_m17609_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_Reverse_m17609(__this, method) (( void (*) (List_1_t348 *, const MethodInfo*))List_1_Reverse_m17609_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Sort()
extern "C" void List_1_Sort_m17610_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_Sort_m17610(__this, method) (( void (*) (List_1_t348 *, const MethodInfo*))List_1_Sort_m17610_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m17611_gshared (List_1_t348 * __this, Comparison_1_t2529 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m17611(__this, ___comparison, method) (( void (*) (List_1_t348 *, Comparison_1_t2529 *, const MethodInfo*))List_1_Sort_m17611_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector4>::ToArray()
extern "C" Vector4U5BU5D_t457* List_1_ToArray_m17612_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_ToArray_m17612(__this, method) (( Vector4U5BU5D_t457* (*) (List_1_t348 *, const MethodInfo*))List_1_ToArray_m17612_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::TrimExcess()
extern "C" void List_1_TrimExcess_m17613_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m17613(__this, method) (( void (*) (List_1_t348 *, const MethodInfo*))List_1_TrimExcess_m17613_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m17614_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m17614(__this, method) (( int32_t (*) (List_1_t348 *, const MethodInfo*))List_1_get_Capacity_m17614_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m17615_gshared (List_1_t348 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m17615(__this, ___value, method) (( void (*) (List_1_t348 *, int32_t, const MethodInfo*))List_1_set_Capacity_m17615_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Count()
extern "C" int32_t List_1_get_Count_m17616_gshared (List_1_t348 * __this, const MethodInfo* method);
#define List_1_get_Count_m17616(__this, method) (( int32_t (*) (List_1_t348 *, const MethodInfo*))List_1_get_Count_m17616_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Item(System.Int32)
extern "C" Vector4_t350  List_1_get_Item_m17617_gshared (List_1_t348 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m17617(__this, ___index, method) (( Vector4_t350  (*) (List_1_t348 *, int32_t, const MethodInfo*))List_1_get_Item_m17617_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m17618_gshared (List_1_t348 * __this, int32_t ___index, Vector4_t350  ___value, const MethodInfo* method);
#define List_1_set_Item_m17618(__this, ___index, ___value, method) (( void (*) (List_1_t348 *, int32_t, Vector4_t350 , const MethodInfo*))List_1_set_Item_m17618_gshared)(__this, ___index, ___value, method)
