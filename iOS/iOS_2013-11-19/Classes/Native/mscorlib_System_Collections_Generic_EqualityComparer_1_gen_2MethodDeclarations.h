﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>
struct EqualityComparer_1_t2384;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::.ctor()
extern "C" void EqualityComparer_1__ctor_m15611_gshared (EqualityComparer_1_t2384 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m15611(__this, method) (( void (*) (EqualityComparer_1_t2384 *, const MethodInfo*))EqualityComparer_1__ctor_m15611_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::.cctor()
extern "C" void EqualityComparer_1__cctor_m15612_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m15612(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m15612_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15613_gshared (EqualityComparer_1_t2384 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15613(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t2384 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15613_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15614_gshared (EqualityComparer_1_t2384 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15614(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t2384 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15614_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::get_Default()
extern "C" EqualityComparer_1_t2384 * EqualityComparer_1_get_Default_m15615_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m15615(__this /* static, unused */, method) (( EqualityComparer_1_t2384 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m15615_gshared)(__this /* static, unused */, method)
