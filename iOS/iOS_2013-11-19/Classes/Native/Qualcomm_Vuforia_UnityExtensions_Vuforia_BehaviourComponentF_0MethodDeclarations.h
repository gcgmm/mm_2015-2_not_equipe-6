﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory
struct NullBehaviourComponentFactory_t769;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t71;
// UnityEngine.GameObject
struct GameObject_t9;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t37;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t49;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t23;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t80;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t51;

#include "codegen/il2cpp-codegen.h"

// Vuforia.VirtualButtonAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C" VirtualButtonAbstractBehaviour_t71 * NullBehaviourComponentFactory_AddVirtualButtonBehaviour_m3980 (NullBehaviourComponentFactory_t769 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C" ImageTargetAbstractBehaviour_t37 * NullBehaviourComponentFactory_AddImageTargetBehaviour_m3981 (NullBehaviourComponentFactory_t769 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C" MultiTargetAbstractBehaviour_t49 * NullBehaviourComponentFactory_AddMultiTargetBehaviour_m3982 (NullBehaviourComponentFactory_t769 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C" CylinderTargetAbstractBehaviour_t23 * NullBehaviourComponentFactory_AddCylinderTargetBehaviour_m3983 (NullBehaviourComponentFactory_t769 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C" WordAbstractBehaviour_t80 * NullBehaviourComponentFactory_AddWordBehaviour_m3984 (NullBehaviourComponentFactory_t769 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C" ObjectTargetAbstractBehaviour_t51 * NullBehaviourComponentFactory_AddObjectTargetBehaviour_m3985 (NullBehaviourComponentFactory_t769 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::.ctor()
extern "C" void NullBehaviourComponentFactory__ctor_m3986 (NullBehaviourComponentFactory_t769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
