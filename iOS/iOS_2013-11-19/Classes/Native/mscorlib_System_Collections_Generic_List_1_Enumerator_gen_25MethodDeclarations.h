﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m24397(__this, ___l, method) (( void (*) (Enumerator_t1056 *, List_1_t918 *, const MethodInfo*))Enumerator__ctor_m12901_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m24398(__this, method) (( void (*) (Enumerator_t1056 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m12902_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24399(__this, method) (( Object_t * (*) (Enumerator_t1056 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12903_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::Dispose()
#define Enumerator_Dispose_m5556(__this, method) (( void (*) (Enumerator_t1056 *, const MethodInfo*))Enumerator_Dispose_m12904_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::VerifyState()
#define Enumerator_VerifyState_m24400(__this, method) (( void (*) (Enumerator_t1056 *, const MethodInfo*))Enumerator_VerifyState_m12905_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::MoveNext()
#define Enumerator_MoveNext_m5555(__this, method) (( bool (*) (Enumerator_t1056 *, const MethodInfo*))Enumerator_MoveNext_m12906_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::get_Current()
#define Enumerator_get_Current_m5554(__this, method) (( Object_t * (*) (Enumerator_t1056 *, const MethodInfo*))Enumerator_get_Current_m12907_gshared)(__this, method)
