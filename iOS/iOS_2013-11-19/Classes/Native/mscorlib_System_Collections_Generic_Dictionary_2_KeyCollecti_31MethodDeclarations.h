﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_29MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m21369(__this, ___dictionary, method) (( void (*) (KeyCollection_t2808 *, Dictionary_2_t848 *, const MethodInfo*))KeyCollection__ctor_m21271_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21370(__this, ___item, method) (( void (*) (KeyCollection_t2808 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21272_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21371(__this, method) (( void (*) (KeyCollection_t2808 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21273_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21372(__this, ___item, method) (( bool (*) (KeyCollection_t2808 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21274_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21373(__this, ___item, method) (( bool (*) (KeyCollection_t2808 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21275_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21374(__this, method) (( Object_t* (*) (KeyCollection_t2808 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21276_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m21375(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2808 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m21277_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21376(__this, method) (( Object_t * (*) (KeyCollection_t2808 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21278_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21377(__this, method) (( bool (*) (KeyCollection_t2808 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21378(__this, method) (( bool (*) (KeyCollection_t2808 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21280_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m21379(__this, method) (( Object_t * (*) (KeyCollection_t2808 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m21281_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m21380(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2808 *, TypeU5BU5D_t679*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m21282_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::GetEnumerator()
#define KeyCollection_GetEnumerator_m21381(__this, method) (( Enumerator_t3218  (*) (KeyCollection_t2808 *, const MethodInfo*))KeyCollection_GetEnumerator_m21283_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::get_Count()
#define KeyCollection_get_Count_m21382(__this, method) (( int32_t (*) (KeyCollection_t2808 *, const MethodInfo*))KeyCollection_get_Count_m21284_gshared)(__this, method)
