﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.EventSystems.IPointerEnterHandler
struct IPointerEnterHandler_t357;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t137;
// UnityEngine.EventSystems.IPointerExitHandler
struct IPointerExitHandler_t358;
// UnityEngine.EventSystems.IPointerDownHandler
struct IPointerDownHandler_t359;
// UnityEngine.EventSystems.IPointerUpHandler
struct IPointerUpHandler_t360;
// UnityEngine.EventSystems.IPointerClickHandler
struct IPointerClickHandler_t361;
// UnityEngine.EventSystems.IInitializePotentialDragHandler
struct IInitializePotentialDragHandler_t362;
// UnityEngine.EventSystems.IBeginDragHandler
struct IBeginDragHandler_t363;
// UnityEngine.EventSystems.IDragHandler
struct IDragHandler_t364;
// UnityEngine.EventSystems.IEndDragHandler
struct IEndDragHandler_t365;
// UnityEngine.EventSystems.IDropHandler
struct IDropHandler_t366;
// UnityEngine.EventSystems.IScrollHandler
struct IScrollHandler_t367;
// UnityEngine.EventSystems.IUpdateSelectedHandler
struct IUpdateSelectedHandler_t368;
// UnityEngine.EventSystems.ISelectHandler
struct ISelectHandler_t369;
// UnityEngine.EventSystems.IDeselectHandler
struct IDeselectHandler_t370;
// UnityEngine.EventSystems.IMoveHandler
struct IMoveHandler_t371;
// UnityEngine.EventSystems.ISubmitHandler
struct ISubmitHandler_t372;
// UnityEngine.EventSystems.ICancelHandler
struct ICancelHandler_t373;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler>
struct EventFunction_1_t146;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct EventFunction_1_t147;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct EventFunction_1_t148;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_t149;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct EventFunction_1_t150;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct EventFunction_1_t151;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
struct EventFunction_1_t152;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
struct EventFunction_1_t153;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct EventFunction_1_t154;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct EventFunction_1_t155;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct EventFunction_1_t156;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
struct EventFunction_1_t157;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>
struct EventFunction_1_t158;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
struct EventFunction_1_t159;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
struct EventFunction_1_t160;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
struct EventFunction_1_t161;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct EventFunction_1_t162;
// UnityEngine.GameObject
struct GameObject_t9;
// System.Collections.Generic.IList`1<UnityEngine.Transform>
struct IList_1_t374;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct List_1_t356;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.EventSystems.ExecuteEvents::.cctor()
extern "C" void ExecuteEvents__cctor_m361 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerEnterHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m362 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerExitHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m363 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerDownHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m364 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerUpHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m365 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerClickHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m366 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IInitializePotentialDragHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m367 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IBeginDragHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m368 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IDragHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m369 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IEndDragHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m370 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IDropHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m371 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IScrollHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m372 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IUpdateSelectedHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m373 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.ISelectHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m374 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IDeselectHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m375 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IMoveHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m376 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.ISubmitHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m377 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.ICancelHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m378 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerEnterHandler()
extern "C" EventFunction_1_t146 * ExecuteEvents_get_pointerEnterHandler_m379 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerExitHandler()
extern "C" EventFunction_1_t147 * ExecuteEvents_get_pointerExitHandler_m380 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerDownHandler()
extern "C" EventFunction_1_t148 * ExecuteEvents_get_pointerDownHandler_m381 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerUpHandler()
extern "C" EventFunction_1_t149 * ExecuteEvents_get_pointerUpHandler_m382 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerClickHandler()
extern "C" EventFunction_1_t150 * ExecuteEvents_get_pointerClickHandler_m383 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_initializePotentialDrag()
extern "C" EventFunction_1_t151 * ExecuteEvents_get_initializePotentialDrag_m384 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_beginDragHandler()
extern "C" EventFunction_1_t152 * ExecuteEvents_get_beginDragHandler_m385 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_dragHandler()
extern "C" EventFunction_1_t153 * ExecuteEvents_get_dragHandler_m386 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_endDragHandler()
extern "C" EventFunction_1_t154 * ExecuteEvents_get_endDragHandler_m387 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::get_dropHandler()
extern "C" EventFunction_1_t155 * ExecuteEvents_get_dropHandler_m388 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::get_scrollHandler()
extern "C" EventFunction_1_t156 * ExecuteEvents_get_scrollHandler_m389 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::get_updateSelectedHandler()
extern "C" EventFunction_1_t157 * ExecuteEvents_get_updateSelectedHandler_m390 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::get_selectHandler()
extern "C" EventFunction_1_t158 * ExecuteEvents_get_selectHandler_m391 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::get_deselectHandler()
extern "C" EventFunction_1_t159 * ExecuteEvents_get_deselectHandler_m392 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::get_moveHandler()
extern "C" EventFunction_1_t160 * ExecuteEvents_get_moveHandler_m393 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::get_submitHandler()
extern "C" EventFunction_1_t161 * ExecuteEvents_get_submitHandler_m394 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::get_cancelHandler()
extern "C" EventFunction_1_t162 * ExecuteEvents_get_cancelHandler_m395 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::GetEventChain(UnityEngine.GameObject,System.Collections.Generic.IList`1<UnityEngine.Transform>)
extern "C" void ExecuteEvents_GetEventChain_m396 (Object_t * __this /* static, unused */, GameObject_t9 * ___root, Object_t* ___eventChain, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::<s_HandlerListPool>m__0(System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>)
extern "C" void ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m397 (Object_t * __this /* static, unused */, List_1_t356 * ___l, const MethodInfo* method) IL2CPP_METHOD_ATTR;
