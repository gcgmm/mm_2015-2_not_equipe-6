﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Version
struct Version_t1304;

#include "mscorlib_System_Attribute.h"

// System.Resources.SatelliteContractVersionAttribute
struct  SatelliteContractVersionAttribute_t1731  : public Attribute_t104
{
	// System.Version System.Resources.SatelliteContractVersionAttribute::ver
	Version_t1304 * ___ver_0;
};
