﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"

// System.Void System.Predicate`1<Vuforia.Word>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m21674(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2823 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m12988_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.Word>::Invoke(T)
#define Predicate_1_Invoke_m21675(__this, ___obj, method) (( bool (*) (Predicate_1_t2823 *, Object_t *, const MethodInfo*))Predicate_1_Invoke_m12989_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.Word>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m21676(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2823 *, Object_t *, AsyncCallback_t261 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m12990_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.Word>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m21677(__this, ___result, method) (( bool (*) (Predicate_1_t2823 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m12991_gshared)(__this, ___result, method)
