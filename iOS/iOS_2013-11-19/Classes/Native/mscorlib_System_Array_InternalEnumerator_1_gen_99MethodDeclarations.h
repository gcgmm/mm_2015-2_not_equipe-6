﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m24815(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3037 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m12785_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24816(__this, method) (( void (*) (InternalEnumerator_1_t3037 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12787_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24817(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3037 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12789_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m24818(__this, method) (( void (*) (InternalEnumerator_1_t3037 *, const MethodInfo*))InternalEnumerator_1_Dispose_m12791_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m24819(__this, method) (( bool (*) (InternalEnumerator_1_t3037 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m12793_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m24820(__this, method) (( FieldInfo_t * (*) (InternalEnumerator_1_t3037 *, const MethodInfo*))InternalEnumerator_1_get_Current_m12795_gshared)(__this, method)
