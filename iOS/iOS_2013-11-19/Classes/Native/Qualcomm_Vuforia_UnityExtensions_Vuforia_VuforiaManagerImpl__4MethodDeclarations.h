﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void WordResultData_t814_marshal(const WordResultData_t814& unmarshaled, WordResultData_t814_marshaled& marshaled);
extern "C" void WordResultData_t814_marshal_back(const WordResultData_t814_marshaled& marshaled, WordResultData_t814& unmarshaled);
extern "C" void WordResultData_t814_marshal_cleanup(WordResultData_t814_marshaled& marshaled);
