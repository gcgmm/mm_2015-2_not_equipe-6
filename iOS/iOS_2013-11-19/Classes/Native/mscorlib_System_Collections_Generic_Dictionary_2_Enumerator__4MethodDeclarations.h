﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2309;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m14667_gshared (Enumerator_t2316 * __this, Dictionary_2_t2309 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m14667(__this, ___dictionary, method) (( void (*) (Enumerator_t2316 *, Dictionary_2_t2309 *, const MethodInfo*))Enumerator__ctor_m14667_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14668_gshared (Enumerator_t2316 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14668(__this, method) (( Object_t * (*) (Enumerator_t2316 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14668_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m14669_gshared (Enumerator_t2316 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m14669(__this, method) (( void (*) (Enumerator_t2316 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14669_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1423  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14670_gshared (Enumerator_t2316 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14670(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t2316 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14670_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14671_gshared (Enumerator_t2316 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14671(__this, method) (( Object_t * (*) (Enumerator_t2316 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14671_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14672_gshared (Enumerator_t2316 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14672(__this, method) (( Object_t * (*) (Enumerator_t2316 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14672_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14673_gshared (Enumerator_t2316 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14673(__this, method) (( bool (*) (Enumerator_t2316 *, const MethodInfo*))Enumerator_MoveNext_m14673_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t2312  Enumerator_get_Current_m14674_gshared (Enumerator_t2316 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14674(__this, method) (( KeyValuePair_2_t2312  (*) (Enumerator_t2316 *, const MethodInfo*))Enumerator_get_Current_m14674_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m14675_gshared (Enumerator_t2316 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m14675(__this, method) (( Object_t * (*) (Enumerator_t2316 *, const MethodInfo*))Enumerator_get_CurrentKey_m14675_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m14676_gshared (Enumerator_t2316 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m14676(__this, method) (( int32_t (*) (Enumerator_t2316 *, const MethodInfo*))Enumerator_get_CurrentValue_m14676_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Reset()
extern "C" void Enumerator_Reset_m14677_gshared (Enumerator_t2316 * __this, const MethodInfo* method);
#define Enumerator_Reset_m14677(__this, method) (( void (*) (Enumerator_t2316 *, const MethodInfo*))Enumerator_Reset_m14677_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m14678_gshared (Enumerator_t2316 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m14678(__this, method) (( void (*) (Enumerator_t2316 *, const MethodInfo*))Enumerator_VerifyState_m14678_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m14679_gshared (Enumerator_t2316 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m14679(__this, method) (( void (*) (Enumerator_t2316 *, const MethodInfo*))Enumerator_VerifyCurrent_m14679_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m14680_gshared (Enumerator_t2316 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14680(__this, method) (( void (*) (Enumerator_t2316 *, const MethodInfo*))Enumerator_Dispose_m14680_gshared)(__this, method)
