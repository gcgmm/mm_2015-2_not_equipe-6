﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t346;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_51.h"
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m17331_gshared (Enumerator_t2499 * __this, List_1_t346 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m17331(__this, ___l, method) (( void (*) (Enumerator_t2499 *, List_1_t346 *, const MethodInfo*))Enumerator__ctor_m17331_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m17332_gshared (Enumerator_t2499 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m17332(__this, method) (( void (*) (Enumerator_t2499 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m17332_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17333_gshared (Enumerator_t2499 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17333(__this, method) (( Object_t * (*) (Enumerator_t2499 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17333_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
extern "C" void Enumerator_Dispose_m17334_gshared (Enumerator_t2499 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17334(__this, method) (( void (*) (Enumerator_t2499 *, const MethodInfo*))Enumerator_Dispose_m17334_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
extern "C" void Enumerator_VerifyState_m17335_gshared (Enumerator_t2499 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17335(__this, method) (( void (*) (Enumerator_t2499 *, const MethodInfo*))Enumerator_VerifyState_m17335_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17336_gshared (Enumerator_t2499 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17336(__this, method) (( bool (*) (Enumerator_t2499 *, const MethodInfo*))Enumerator_MoveNext_m17336_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
extern "C" Color32_t382  Enumerator_get_Current_m17337_gshared (Enumerator_t2499 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17337(__this, method) (( Color32_t382  (*) (Enumerator_t2499 *, const MethodInfo*))Enumerator_get_Current_m17337_gshared)(__this, method)
