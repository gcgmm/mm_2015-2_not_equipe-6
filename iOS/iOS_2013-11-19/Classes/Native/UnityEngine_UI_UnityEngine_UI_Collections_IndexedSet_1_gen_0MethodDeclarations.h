﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_2MethodDeclarations.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
#define IndexedSet_1__ctor_m2011(__this, method) (( void (*) (IndexedSet_1_t422 *, const MethodInfo*))IndexedSet_1__ctor_m14557_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m15820(__this, method) (( Object_t * (*) (IndexedSet_1_t422 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m14559_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m15821(__this, ___item, method) (( void (*) (IndexedSet_1_t422 *, Graphic_t228 *, const MethodInfo*))IndexedSet_1_Add_m14561_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m15822(__this, ___item, method) (( bool (*) (IndexedSet_1_t422 *, Graphic_t228 *, const MethodInfo*))IndexedSet_1_Remove_m14563_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m15823(__this, method) (( Object_t* (*) (IndexedSet_1_t422 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m14565_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m15824(__this, method) (( void (*) (IndexedSet_1_t422 *, const MethodInfo*))IndexedSet_1_Clear_m14567_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m15825(__this, ___item, method) (( bool (*) (IndexedSet_1_t422 *, Graphic_t228 *, const MethodInfo*))IndexedSet_1_Contains_m14569_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m15826(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t422 *, GraphicU5BU5D_t2390*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m14571_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m15827(__this, method) (( int32_t (*) (IndexedSet_1_t422 *, const MethodInfo*))IndexedSet_1_get_Count_m14573_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m15828(__this, method) (( bool (*) (IndexedSet_1_t422 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m14575_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m15829(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t422 *, Graphic_t228 *, const MethodInfo*))IndexedSet_1_IndexOf_m14577_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m15830(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t422 *, int32_t, Graphic_t228 *, const MethodInfo*))IndexedSet_1_Insert_m14579_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m15831(__this, ___index, method) (( void (*) (IndexedSet_1_t422 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m14581_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m15832(__this, ___index, method) (( Graphic_t228 * (*) (IndexedSet_1_t422 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m14583_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m15833(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t422 *, int32_t, Graphic_t228 *, const MethodInfo*))IndexedSet_1_set_Item_m14585_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m15834(__this, ___match, method) (( void (*) (IndexedSet_1_t422 *, Predicate_1_t2392 *, const MethodInfo*))IndexedSet_1_RemoveAll_m14587_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m15835(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t422 *, Comparison_1_t238 *, const MethodInfo*))IndexedSet_1_Sort_m14588_gshared)(__this, ___sortLayoutFunction, method)
