﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>
struct DefaultComparer_t2731;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C" void DefaultComparer__ctor_m20222_gshared (DefaultComparer_t2731 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m20222(__this, method) (( void (*) (DefaultComparer_t2731 *, const MethodInfo*))DefaultComparer__ctor_m20222_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m20223_gshared (DefaultComparer_t2731 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m20223(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2731 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m20223_gshared)(__this, ___x, ___y, method)
