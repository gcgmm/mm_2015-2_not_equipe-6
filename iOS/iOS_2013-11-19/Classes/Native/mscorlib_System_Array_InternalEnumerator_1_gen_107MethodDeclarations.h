﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_107.h"
#include "mscorlib_System_Collections_SortedList_Slot.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24868_gshared (InternalEnumerator_1_t3049 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m24868(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3049 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m24868_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24869_gshared (InternalEnumerator_1_t3049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24869(__this, method) (( void (*) (InternalEnumerator_1_t3049 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24869_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24870_gshared (InternalEnumerator_1_t3049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24870(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3049 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24870_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24871_gshared (InternalEnumerator_1_t3049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24871(__this, method) (( void (*) (InternalEnumerator_1_t3049 *, const MethodInfo*))InternalEnumerator_1_Dispose_m24871_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24872_gshared (InternalEnumerator_1_t3049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24872(__this, method) (( bool (*) (InternalEnumerator_1_t3049 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m24872_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern "C" Slot_t1565  InternalEnumerator_1_get_Current_m24873_gshared (InternalEnumerator_1_t3049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24873(__this, method) (( Slot_t1565  (*) (InternalEnumerator_1_t3049 *, const MethodInfo*))InternalEnumerator_1_get_Current_m24873_gshared)(__this, method)
