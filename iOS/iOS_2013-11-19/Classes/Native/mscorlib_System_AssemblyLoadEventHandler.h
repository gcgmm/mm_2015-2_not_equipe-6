﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.AssemblyLoadEventArgs
struct AssemblyLoadEventArgs_t2007;
// System.IAsyncResult
struct IAsyncResult_t260;
// System.AsyncCallback
struct AsyncCallback_t261;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"

// System.AssemblyLoadEventHandler
struct  AssemblyLoadEventHandler_t2001  : public MulticastDelegate_t259
{
};
