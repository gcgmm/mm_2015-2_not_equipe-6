﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void TargetFinderState_t885_marshal(const TargetFinderState_t885& unmarshaled, TargetFinderState_t885_marshaled& marshaled);
extern "C" void TargetFinderState_t885_marshal_back(const TargetFinderState_t885_marshaled& marshaled, TargetFinderState_t885& unmarshaled);
extern "C" void TargetFinderState_t885_marshal_cleanup(TargetFinderState_t885_marshaled& marshaled);
