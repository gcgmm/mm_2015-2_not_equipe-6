﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t3036;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m24809_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3036 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m24809(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3036 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m24809_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m24810_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3036 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m24810(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t3036 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m24810_gshared)(__this, method)
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m24811_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3036 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m24811(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t3036 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m24811_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m24812_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3036 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m24812(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t3036 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m24812_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m24813_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3036 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m24813(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3036 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m24813_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Reset_m24814_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3036 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Reset_m24814(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3036 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Reset_m24814_gshared)(__this, method)
