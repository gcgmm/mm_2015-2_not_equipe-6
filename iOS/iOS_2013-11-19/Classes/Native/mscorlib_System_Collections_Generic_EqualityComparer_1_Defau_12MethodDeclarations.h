﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct DefaultComparer_t2893;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor()
extern "C" void DefaultComparer__ctor_m23064_gshared (DefaultComparer_t2893 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m23064(__this, method) (( void (*) (DefaultComparer_t2893 *, const MethodInfo*))DefaultComparer__ctor_m23064_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m23065_gshared (DefaultComparer_t2893 * __this, TrackableResultData_t810  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m23065(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2893 *, TrackableResultData_t810 , const MethodInfo*))DefaultComparer_GetHashCode_m23065_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m23066_gshared (DefaultComparer_t2893 * __this, TrackableResultData_t810  ___x, TrackableResultData_t810  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m23066(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2893 *, TrackableResultData_t810 , TrackableResultData_t810 , const MethodInfo*))DefaultComparer_Equals_m23066_gshared)(__this, ___x, ___y, method)
