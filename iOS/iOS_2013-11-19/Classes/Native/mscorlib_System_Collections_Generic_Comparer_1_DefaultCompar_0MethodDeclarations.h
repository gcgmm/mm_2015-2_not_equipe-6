﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t2248;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void DefaultComparer__ctor_m13733_gshared (DefaultComparer_t2248 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m13733(__this, method) (( void (*) (DefaultComparer_t2248 *, const MethodInfo*))DefaultComparer__ctor_m13733_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m13734_gshared (DefaultComparer_t2248 * __this, RaycastResult_t169  ___x, RaycastResult_t169  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m13734(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2248 *, RaycastResult_t169 , RaycastResult_t169 , const MethodInfo*))DefaultComparer_Compare_m13734_gshared)(__this, ___x, ___y, method)
