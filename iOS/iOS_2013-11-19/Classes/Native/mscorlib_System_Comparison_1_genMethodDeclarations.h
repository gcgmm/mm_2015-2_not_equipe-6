﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t138;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t260;
// System.AsyncCallback
struct AsyncCallback_t261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m1714_gshared (Comparison_1_t138 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m1714(__this, ___object, ___method, method) (( void (*) (Comparison_1_t138 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m1714_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m13302_gshared (Comparison_1_t138 * __this, RaycastResult_t169  ___x, RaycastResult_t169  ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m13302(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t138 *, RaycastResult_t169 , RaycastResult_t169 , const MethodInfo*))Comparison_1_Invoke_m13302_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m13303_gshared (Comparison_1_t138 * __this, RaycastResult_t169  ___x, RaycastResult_t169  ___y, AsyncCallback_t261 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m13303(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t138 *, RaycastResult_t169 , RaycastResult_t169 , AsyncCallback_t261 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m13303_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m13304_gshared (Comparison_1_t138 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m13304(__this, ___result, method) (( int32_t (*) (Comparison_1_t138 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m13304_gshared)(__this, ___result, method)
