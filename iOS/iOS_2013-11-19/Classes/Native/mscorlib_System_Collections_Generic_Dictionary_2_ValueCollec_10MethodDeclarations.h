﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_42MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m20970(__this, ___host, method) (( void (*) (Enumerator_t982 *, Dictionary_2_t778 *, const MethodInfo*))Enumerator__ctor_m20008_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20971(__this, method) (( Object_t * (*) (Enumerator_t982 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20009_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m20972(__this, method) (( void (*) (Enumerator_t982 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m20010_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Dispose()
#define Enumerator_Dispose_m5344(__this, method) (( void (*) (Enumerator_t982 *, const MethodInfo*))Enumerator_Dispose_m20011_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::MoveNext()
#define Enumerator_MoveNext_m5343(__this, method) (( bool (*) (Enumerator_t982 *, const MethodInfo*))Enumerator_MoveNext_m20012_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Current()
#define Enumerator_get_Current_m5342(__this, method) (( Image_t784 * (*) (Enumerator_t982 *, const MethodInfo*))Enumerator_get_Current_m20013_gshared)(__this, method)
