﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t197;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.AnimationTriggers::.ctor()
extern "C" void AnimationTriggers__ctor_m618 (AnimationTriggers_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_normalTrigger()
extern "C" String_t* AnimationTriggers_get_normalTrigger_m619 (AnimationTriggers_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_normalTrigger(System.String)
extern "C" void AnimationTriggers_set_normalTrigger_m620 (AnimationTriggers_t197 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_highlightedTrigger()
extern "C" String_t* AnimationTriggers_get_highlightedTrigger_m621 (AnimationTriggers_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_highlightedTrigger(System.String)
extern "C" void AnimationTriggers_set_highlightedTrigger_m622 (AnimationTriggers_t197 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_pressedTrigger()
extern "C" String_t* AnimationTriggers_get_pressedTrigger_m623 (AnimationTriggers_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_pressedTrigger(System.String)
extern "C" void AnimationTriggers_set_pressedTrigger_m624 (AnimationTriggers_t197 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_disabledTrigger()
extern "C" String_t* AnimationTriggers_get_disabledTrigger_m625 (AnimationTriggers_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_disabledTrigger(System.String)
extern "C" void AnimationTriggers_set_disabledTrigger_m626 (AnimationTriggers_t197 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
