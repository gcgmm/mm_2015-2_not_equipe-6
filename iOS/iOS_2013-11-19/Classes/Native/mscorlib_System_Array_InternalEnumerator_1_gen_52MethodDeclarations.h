﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_52.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18894_gshared (InternalEnumerator_1_t2628 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18894(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2628 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18894_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18895_gshared (InternalEnumerator_1_t2628 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18895(__this, method) (( void (*) (InternalEnumerator_1_t2628 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18895_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18896_gshared (InternalEnumerator_1_t2628 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18896(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2628 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18896_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18897_gshared (InternalEnumerator_1_t2628 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18897(__this, method) (( void (*) (InternalEnumerator_1_t2628 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18897_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18898_gshared (InternalEnumerator_1_t2628 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18898(__this, method) (( bool (*) (InternalEnumerator_1_t2628 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18898_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern "C" HitInfo_t633  InternalEnumerator_1_get_Current_m18899_gshared (InternalEnumerator_1_t2628 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18899(__this, method) (( HitInfo_t633  (*) (InternalEnumerator_1_t2628 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18899_gshared)(__this, method)
