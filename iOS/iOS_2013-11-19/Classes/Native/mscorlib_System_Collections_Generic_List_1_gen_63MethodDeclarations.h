﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t128;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t129;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2460;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3123;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2170;
// System.Object[]
struct ObjectU5BU5D_t105;
// System.Predicate`1<System.Object>
struct Predicate_1_t2180;
// System.Comparison`1<System.Object>
struct Comparison_1_t2186;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m12803_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1__ctor_m12803(__this, method) (( void (*) (List_1_t128 *, const MethodInfo*))List_1__ctor_m12803_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m12804_gshared (List_1_t128 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m12804(__this, ___collection, method) (( void (*) (List_1_t128 *, Object_t*, const MethodInfo*))List_1__ctor_m12804_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" void List_1__ctor_m12806_gshared (List_1_t128 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m12806(__this, ___capacity, method) (( void (*) (List_1_t128 *, int32_t, const MethodInfo*))List_1__ctor_m12806_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m12808_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m12808(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m12808_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12810_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12810(__this, method) (( Object_t* (*) (List_1_t128 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12810_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m12812_gshared (List_1_t128 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m12812(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t128 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m12812_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m12814_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m12814(__this, method) (( Object_t * (*) (List_1_t128 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m12814_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m12816_gshared (List_1_t128 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m12816(__this, ___item, method) (( int32_t (*) (List_1_t128 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m12816_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m12818_gshared (List_1_t128 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m12818(__this, ___item, method) (( bool (*) (List_1_t128 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m12818_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m12820_gshared (List_1_t128 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m12820(__this, ___item, method) (( int32_t (*) (List_1_t128 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m12820_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m12822_gshared (List_1_t128 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m12822(__this, ___index, ___item, method) (( void (*) (List_1_t128 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m12822_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m12824_gshared (List_1_t128 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m12824(__this, ___item, method) (( void (*) (List_1_t128 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m12824_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12826_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12826(__this, method) (( bool (*) (List_1_t128 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m12828_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m12828(__this, method) (( bool (*) (List_1_t128 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m12828_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m12830_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m12830(__this, method) (( Object_t * (*) (List_1_t128 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m12830_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m12832_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m12832(__this, method) (( bool (*) (List_1_t128 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m12832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m12834_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m12834(__this, method) (( bool (*) (List_1_t128 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m12834_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m12836_gshared (List_1_t128 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m12836(__this, ___index, method) (( Object_t * (*) (List_1_t128 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m12836_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m12838_gshared (List_1_t128 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m12838(__this, ___index, ___value, method) (( void (*) (List_1_t128 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12838_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C" void List_1_Add_m12840_gshared (List_1_t128 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Add_m12840(__this, ___item, method) (( void (*) (List_1_t128 *, Object_t *, const MethodInfo*))List_1_Add_m12840_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m12842_gshared (List_1_t128 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m12842(__this, ___newCount, method) (( void (*) (List_1_t128 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m12842_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m12844_gshared (List_1_t128 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m12844(__this, ___collection, method) (( void (*) (List_1_t128 *, Object_t*, const MethodInfo*))List_1_AddCollection_m12844_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m12846_gshared (List_1_t128 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m12846(__this, ___enumerable, method) (( void (*) (List_1_t128 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m12846_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m12848_gshared (List_1_t128 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m12848(__this, ___collection, method) (( void (*) (List_1_t128 *, Object_t*, const MethodInfo*))List_1_AddRange_m12848_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2170 * List_1_AsReadOnly_m12850_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m12850(__this, method) (( ReadOnlyCollection_1_t2170 * (*) (List_1_t128 *, const MethodInfo*))List_1_AsReadOnly_m12850_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m12852_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_Clear_m12852(__this, method) (( void (*) (List_1_t128 *, const MethodInfo*))List_1_Clear_m12852_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m12854_gshared (List_1_t128 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Contains_m12854(__this, ___item, method) (( bool (*) (List_1_t128 *, Object_t *, const MethodInfo*))List_1_Contains_m12854_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m12856_gshared (List_1_t128 * __this, ObjectU5BU5D_t105* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m12856(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t128 *, ObjectU5BU5D_t105*, int32_t, const MethodInfo*))List_1_CopyTo_m12856_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
extern "C" Object_t * List_1_Find_m12858_gshared (List_1_t128 * __this, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define List_1_Find_m12858(__this, ___match, method) (( Object_t * (*) (List_1_t128 *, Predicate_1_t2180 *, const MethodInfo*))List_1_Find_m12858_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m12860_gshared (Object_t * __this /* static, unused */, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m12860(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2180 *, const MethodInfo*))List_1_CheckMatch_m12860_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m12862_gshared (List_1_t128 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m12862(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t128 *, int32_t, int32_t, Predicate_1_t2180 *, const MethodInfo*))List_1_GetIndex_m12862_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2174  List_1_GetEnumerator_m12864_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m12864(__this, method) (( Enumerator_t2174  (*) (List_1_t128 *, const MethodInfo*))List_1_GetEnumerator_m12864_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m12866_gshared (List_1_t128 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_IndexOf_m12866(__this, ___item, method) (( int32_t (*) (List_1_t128 *, Object_t *, const MethodInfo*))List_1_IndexOf_m12866_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m12868_gshared (List_1_t128 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m12868(__this, ___start, ___delta, method) (( void (*) (List_1_t128 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m12868_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m12870_gshared (List_1_t128 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m12870(__this, ___index, method) (( void (*) (List_1_t128 *, int32_t, const MethodInfo*))List_1_CheckIndex_m12870_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m12872_gshared (List_1_t128 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_Insert_m12872(__this, ___index, ___item, method) (( void (*) (List_1_t128 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m12872_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m12874_gshared (List_1_t128 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m12874(__this, ___collection, method) (( void (*) (List_1_t128 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m12874_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C" bool List_1_Remove_m12876_gshared (List_1_t128 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Remove_m12876(__this, ___item, method) (( bool (*) (List_1_t128 *, Object_t *, const MethodInfo*))List_1_Remove_m12876_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m12878_gshared (List_1_t128 * __this, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m12878(__this, ___match, method) (( int32_t (*) (List_1_t128 *, Predicate_1_t2180 *, const MethodInfo*))List_1_RemoveAll_m12878_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m12880_gshared (List_1_t128 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m12880(__this, ___index, method) (( void (*) (List_1_t128 *, int32_t, const MethodInfo*))List_1_RemoveAt_m12880_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse()
extern "C" void List_1_Reverse_m12882_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_Reverse_m12882(__this, method) (( void (*) (List_1_t128 *, const MethodInfo*))List_1_Reverse_m12882_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort()
extern "C" void List_1_Sort_m12884_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_Sort_m12884(__this, method) (( void (*) (List_1_t128 *, const MethodInfo*))List_1_Sort_m12884_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m12886_gshared (List_1_t128 * __this, Comparison_1_t2186 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m12886(__this, ___comparison, method) (( void (*) (List_1_t128 *, Comparison_1_t2186 *, const MethodInfo*))List_1_Sort_m12886_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t105* List_1_ToArray_m12888_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_ToArray_m12888(__this, method) (( ObjectU5BU5D_t105* (*) (List_1_t128 *, const MethodInfo*))List_1_ToArray_m12888_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::TrimExcess()
extern "C" void List_1_TrimExcess_m12890_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m12890(__this, method) (( void (*) (List_1_t128 *, const MethodInfo*))List_1_TrimExcess_m12890_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m12892_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m12892(__this, method) (( int32_t (*) (List_1_t128 *, const MethodInfo*))List_1_get_Capacity_m12892_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m12894_gshared (List_1_t128 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m12894(__this, ___value, method) (( void (*) (List_1_t128 *, int32_t, const MethodInfo*))List_1_set_Capacity_m12894_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m12896_gshared (List_1_t128 * __this, const MethodInfo* method);
#define List_1_get_Count_m12896(__this, method) (( int32_t (*) (List_1_t128 *, const MethodInfo*))List_1_get_Count_m12896_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m12898_gshared (List_1_t128 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m12898(__this, ___index, method) (( Object_t * (*) (List_1_t128 *, int32_t, const MethodInfo*))List_1_get_Item_m12898_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m12900_gshared (List_1_t128 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_set_Item_m12900(__this, ___index, ___value, method) (( void (*) (List_1_t128 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m12900_gshared)(__this, ___index, ___value, method)
