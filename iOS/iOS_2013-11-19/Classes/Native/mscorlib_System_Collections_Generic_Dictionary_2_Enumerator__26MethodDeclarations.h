﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m22411(__this, ___dictionary, method) (( void (*) (Enumerator_t2856 *, Dictionary_2_t873 *, const MethodInfo*))Enumerator__ctor_m14303_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22412(__this, method) (( Object_t * (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14304_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m22413(__this, method) (( void (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14305_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22414(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14306_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22415(__this, method) (( Object_t * (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14307_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22416(__this, method) (( Object_t * (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14308_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m22417(__this, method) (( bool (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_MoveNext_m14309_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m22418(__this, method) (( KeyValuePair_2_t2854  (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_get_Current_m14310_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m22419(__this, method) (( int32_t (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_get_CurrentKey_m14311_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m22420(__this, method) (( SurfaceAbstractBehaviour_t58 * (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_get_CurrentValue_m14312_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::Reset()
#define Enumerator_Reset_m22421(__this, method) (( void (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_Reset_m14313_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m22422(__this, method) (( void (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_VerifyState_m14314_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m22423(__this, method) (( void (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_VerifyCurrent_m14315_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m22424(__this, method) (( void (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_Dispose_m14316_gshared)(__this, method)
