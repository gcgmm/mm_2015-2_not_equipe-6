﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.UserDefinedTargetBuildingBehaviour
struct UserDefinedTargetBuildingBehaviour_t64;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
extern "C" void UserDefinedTargetBuildingBehaviour__ctor_m122 (UserDefinedTargetBuildingBehaviour_t64 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
