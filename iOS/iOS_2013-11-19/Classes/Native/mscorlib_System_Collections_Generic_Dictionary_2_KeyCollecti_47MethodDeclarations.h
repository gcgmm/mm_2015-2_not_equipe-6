﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3003;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_47.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m24603_gshared (Enumerator_t3009 * __this, Dictionary_2_t3003 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m24603(__this, ___host, method) (( void (*) (Enumerator_t3009 *, Dictionary_2_t3003 *, const MethodInfo*))Enumerator__ctor_m24603_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m24604_gshared (Enumerator_t3009 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m24604(__this, method) (( Object_t * (*) (Enumerator_t3009 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24604_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m24605_gshared (Enumerator_t3009 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m24605(__this, method) (( void (*) (Enumerator_t3009 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m24605_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m24606_gshared (Enumerator_t3009 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m24606(__this, method) (( void (*) (Enumerator_t3009 *, const MethodInfo*))Enumerator_Dispose_m24606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m24607_gshared (Enumerator_t3009 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m24607(__this, method) (( bool (*) (Enumerator_t3009 *, const MethodInfo*))Enumerator_MoveNext_m24607_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m24608_gshared (Enumerator_t3009 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m24608(__this, method) (( Object_t * (*) (Enumerator_t3009 *, const MethodInfo*))Enumerator_get_Current_m24608_gshared)(__this, method)
