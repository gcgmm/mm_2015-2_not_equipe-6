﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m15974(__this, ___dictionary, method) (( void (*) (Enumerator_t2410 *, Dictionary_2_t421 *, const MethodInfo*))Enumerator__ctor_m14667_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15975(__this, method) (( Object_t * (*) (Enumerator_t2410 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14668_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m15976(__this, method) (( void (*) (Enumerator_t2410 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14669_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15977(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t2410 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14670_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15978(__this, method) (( Object_t * (*) (Enumerator_t2410 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14671_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15979(__this, method) (( Object_t * (*) (Enumerator_t2410 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14672_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m15980(__this, method) (( bool (*) (Enumerator_t2410 *, const MethodInfo*))Enumerator_MoveNext_m14673_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_Current()
#define Enumerator_get_Current_m15981(__this, method) (( KeyValuePair_2_t2407  (*) (Enumerator_t2410 *, const MethodInfo*))Enumerator_get_Current_m14674_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15982(__this, method) (( Object_t * (*) (Enumerator_t2410 *, const MethodInfo*))Enumerator_get_CurrentKey_m14675_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15983(__this, method) (( int32_t (*) (Enumerator_t2410 *, const MethodInfo*))Enumerator_get_CurrentValue_m14676_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::Reset()
#define Enumerator_Reset_m15984(__this, method) (( void (*) (Enumerator_t2410 *, const MethodInfo*))Enumerator_Reset_m14677_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m15985(__this, method) (( void (*) (Enumerator_t2410 *, const MethodInfo*))Enumerator_VerifyState_m14678_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15986(__this, method) (( void (*) (Enumerator_t2410 *, const MethodInfo*))Enumerator_VerifyCurrent_m14679_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::Dispose()
#define Enumerator_Dispose_m15987(__this, method) (( void (*) (Enumerator_t2410 *, const MethodInfo*))Enumerator_Dispose_m14680_gshared)(__this, method)
