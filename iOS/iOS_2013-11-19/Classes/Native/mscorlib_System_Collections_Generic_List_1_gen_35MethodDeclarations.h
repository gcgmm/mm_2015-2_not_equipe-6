﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_63MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor()
#define List_1__ctor_m3643(__this, method) (( void (*) (List_1_t662 *, const MethodInfo*))List_1__ctor_m12803_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m19275(__this, ___collection, method) (( void (*) (List_1_t662 *, Object_t*, const MethodInfo*))List_1__ctor_m12804_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Int32)
#define List_1__ctor_m19276(__this, ___capacity, method) (( void (*) (List_1_t662 *, int32_t, const MethodInfo*))List_1__ctor_m12806_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.cctor()
#define List_1__cctor_m19277(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m12808_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19278(__this, method) (( Object_t* (*) (List_1_t662 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12810_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19279(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t662 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m12812_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19280(__this, method) (( Object_t * (*) (List_1_t662 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m12814_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19281(__this, ___item, method) (( int32_t (*) (List_1_t662 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m12816_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19282(__this, ___item, method) (( bool (*) (List_1_t662 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m12818_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19283(__this, ___item, method) (( int32_t (*) (List_1_t662 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m12820_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19284(__this, ___index, ___item, method) (( void (*) (List_1_t662 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m12822_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19285(__this, ___item, method) (( void (*) (List_1_t662 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m12824_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19286(__this, method) (( bool (*) (List_1_t662 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19287(__this, method) (( bool (*) (List_1_t662 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m12828_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19288(__this, method) (( Object_t * (*) (List_1_t662 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m12830_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19289(__this, method) (( bool (*) (List_1_t662 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m12832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19290(__this, method) (( bool (*) (List_1_t662 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m12834_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19291(__this, ___index, method) (( Object_t * (*) (List_1_t662 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m12836_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19292(__this, ___index, ___value, method) (( void (*) (List_1_t662 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12838_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(T)
#define List_1_Add_m19293(__this, ___item, method) (( void (*) (List_1_t662 *, BaseInvokableCall_t655 *, const MethodInfo*))List_1_Add_m12840_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19294(__this, ___newCount, method) (( void (*) (List_1_t662 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m12842_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19295(__this, ___collection, method) (( void (*) (List_1_t662 *, Object_t*, const MethodInfo*))List_1_AddCollection_m12844_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19296(__this, ___enumerable, method) (( void (*) (List_1_t662 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m12846_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3646(__this, ___collection, method) (( void (*) (List_1_t662 *, Object_t*, const MethodInfo*))List_1_AddRange_m12848_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AsReadOnly()
#define List_1_AsReadOnly_m19297(__this, method) (( ReadOnlyCollection_1_t2668 * (*) (List_1_t662 *, const MethodInfo*))List_1_AsReadOnly_m12850_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear()
#define List_1_Clear_m19298(__this, method) (( void (*) (List_1_t662 *, const MethodInfo*))List_1_Clear_m12852_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Contains(T)
#define List_1_Contains_m19299(__this, ___item, method) (( bool (*) (List_1_t662 *, BaseInvokableCall_t655 *, const MethodInfo*))List_1_Contains_m12854_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19300(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t662 *, BaseInvokableCallU5BU5D_t2667*, int32_t, const MethodInfo*))List_1_CopyTo_m12856_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Find(System.Predicate`1<T>)
#define List_1_Find_m19301(__this, ___match, method) (( BaseInvokableCall_t655 * (*) (List_1_t662 *, Predicate_1_t726 *, const MethodInfo*))List_1_Find_m12858_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19302(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t726 *, const MethodInfo*))List_1_CheckMatch_m12860_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19303(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t662 *, int32_t, int32_t, Predicate_1_t726 *, const MethodInfo*))List_1_GetIndex_m12862_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GetEnumerator()
#define List_1_GetEnumerator_m19304(__this, method) (( Enumerator_t2670  (*) (List_1_t662 *, const MethodInfo*))List_1_GetEnumerator_m12864_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::IndexOf(T)
#define List_1_IndexOf_m19305(__this, ___item, method) (( int32_t (*) (List_1_t662 *, BaseInvokableCall_t655 *, const MethodInfo*))List_1_IndexOf_m12866_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19306(__this, ___start, ___delta, method) (( void (*) (List_1_t662 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m12868_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19307(__this, ___index, method) (( void (*) (List_1_t662 *, int32_t, const MethodInfo*))List_1_CheckIndex_m12870_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Insert(System.Int32,T)
#define List_1_Insert_m19308(__this, ___index, ___item, method) (( void (*) (List_1_t662 *, int32_t, BaseInvokableCall_t655 *, const MethodInfo*))List_1_Insert_m12872_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19309(__this, ___collection, method) (( void (*) (List_1_t662 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m12874_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Remove(T)
#define List_1_Remove_m19310(__this, ___item, method) (( bool (*) (List_1_t662 *, BaseInvokableCall_t655 *, const MethodInfo*))List_1_Remove_m12876_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3645(__this, ___match, method) (( int32_t (*) (List_1_t662 *, Predicate_1_t726 *, const MethodInfo*))List_1_RemoveAll_m12878_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19311(__this, ___index, method) (( void (*) (List_1_t662 *, int32_t, const MethodInfo*))List_1_RemoveAt_m12880_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Reverse()
#define List_1_Reverse_m19312(__this, method) (( void (*) (List_1_t662 *, const MethodInfo*))List_1_Reverse_m12882_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Sort()
#define List_1_Sort_m19313(__this, method) (( void (*) (List_1_t662 *, const MethodInfo*))List_1_Sort_m12884_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19314(__this, ___comparison, method) (( void (*) (List_1_t662 *, Comparison_1_t2671 *, const MethodInfo*))List_1_Sort_m12886_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::ToArray()
#define List_1_ToArray_m19315(__this, method) (( BaseInvokableCallU5BU5D_t2667* (*) (List_1_t662 *, const MethodInfo*))List_1_ToArray_m12888_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::TrimExcess()
#define List_1_TrimExcess_m19316(__this, method) (( void (*) (List_1_t662 *, const MethodInfo*))List_1_TrimExcess_m12890_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Capacity()
#define List_1_get_Capacity_m19317(__this, method) (( int32_t (*) (List_1_t662 *, const MethodInfo*))List_1_get_Capacity_m12892_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19318(__this, ___value, method) (( void (*) (List_1_t662 *, int32_t, const MethodInfo*))List_1_set_Capacity_m12894_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Count()
#define List_1_get_Count_m19319(__this, method) (( int32_t (*) (List_1_t662 *, const MethodInfo*))List_1_get_Count_m12896_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32)
#define List_1_get_Item_m19320(__this, ___index, method) (( BaseInvokableCall_t655 * (*) (List_1_t662 *, int32_t, const MethodInfo*))List_1_get_Item_m12898_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::set_Item(System.Int32,T)
#define List_1_set_Item_m19321(__this, ___index, ___value, method) (( void (*) (List_1_t662 *, int32_t, BaseInvokableCall_t655 *, const MethodInfo*))List_1_set_Item_m12900_gshared)(__this, ___index, ___value, method)
