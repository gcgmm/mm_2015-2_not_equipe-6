﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_17MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m20045(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2722 *, int32_t, Image_t784 *, const MethodInfo*))KeyValuePair_2__ctor_m19944_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Key()
#define KeyValuePair_2_get_Key_m20046(__this, method) (( int32_t (*) (KeyValuePair_2_t2722 *, const MethodInfo*))KeyValuePair_2_get_Key_m19945_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20047(__this, ___value, method) (( void (*) (KeyValuePair_2_t2722 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m19946_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Value()
#define KeyValuePair_2_get_Value_m20048(__this, method) (( Image_t784 * (*) (KeyValuePair_2_t2722 *, const MethodInfo*))KeyValuePair_2_get_Value_m19947_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20049(__this, ___value, method) (( void (*) (KeyValuePair_2_t2722 *, Image_t784 *, const MethodInfo*))KeyValuePair_2_set_Value_m19948_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ToString()
#define KeyValuePair_2_ToString_m20050(__this, method) (( String_t* (*) (KeyValuePair_2_t2722 *, const MethodInfo*))KeyValuePair_2_ToString_m19949_gshared)(__this, method)
