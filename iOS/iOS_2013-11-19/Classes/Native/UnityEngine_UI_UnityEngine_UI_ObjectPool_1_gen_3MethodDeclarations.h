﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_1MethodDeclarations.h"

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m13576(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t2229 *, UnityAction_1_t2230 *, UnityAction_1_t2230 *, const MethodInfo*))ObjectPool_1__ctor_m13407_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countAll()
#define ObjectPool_1_get_countAll_m13577(__this, method) (( int32_t (*) (ObjectPool_1_t2229 *, const MethodInfo*))ObjectPool_1_get_countAll_m13409_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m13578(__this, ___value, method) (( void (*) (ObjectPool_1_t2229 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m13411_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countActive()
#define ObjectPool_1_get_countActive_m13579(__this, method) (( int32_t (*) (ObjectPool_1_t2229 *, const MethodInfo*))ObjectPool_1_get_countActive_m13413_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m13580(__this, method) (( int32_t (*) (ObjectPool_1_t2229 *, const MethodInfo*))ObjectPool_1_get_countInactive_m13415_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Get()
#define ObjectPool_1_Get_m13581(__this, method) (( List_1_t388 * (*) (ObjectPool_1_t2229 *, const MethodInfo*))ObjectPool_1_Get_m13417_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Release(T)
#define ObjectPool_1_Release_m13582(__this, ___element, method) (( void (*) (ObjectPool_1_t2229 *, List_1_t388 *, const MethodInfo*))ObjectPool_1_Release_m13419_gshared)(__this, ___element, method)
