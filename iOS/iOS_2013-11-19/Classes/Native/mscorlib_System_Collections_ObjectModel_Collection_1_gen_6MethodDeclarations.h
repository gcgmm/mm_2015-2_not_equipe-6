﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>
struct Collection_1_t2523;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t457;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector4>
struct IEnumerator_1_t3174;
// System.Collections.Generic.IList`1<UnityEngine.Vector4>
struct IList_1_t2522;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::.ctor()
extern "C" void Collection_1__ctor_m17662_gshared (Collection_1_t2523 * __this, const MethodInfo* method);
#define Collection_1__ctor_m17662(__this, method) (( void (*) (Collection_1_t2523 *, const MethodInfo*))Collection_1__ctor_m17662_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17663_gshared (Collection_1_t2523 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17663(__this, method) (( bool (*) (Collection_1_t2523 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17663_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m17664_gshared (Collection_1_t2523 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m17664(__this, ___array, ___index, method) (( void (*) (Collection_1_t2523 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m17664_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m17665_gshared (Collection_1_t2523 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m17665(__this, method) (( Object_t * (*) (Collection_1_t2523 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m17665_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m17666_gshared (Collection_1_t2523 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m17666(__this, ___value, method) (( int32_t (*) (Collection_1_t2523 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m17666_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m17667_gshared (Collection_1_t2523 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m17667(__this, ___value, method) (( bool (*) (Collection_1_t2523 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m17667_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m17668_gshared (Collection_1_t2523 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m17668(__this, ___value, method) (( int32_t (*) (Collection_1_t2523 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m17668_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m17669_gshared (Collection_1_t2523 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m17669(__this, ___index, ___value, method) (( void (*) (Collection_1_t2523 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m17669_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m17670_gshared (Collection_1_t2523 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m17670(__this, ___value, method) (( void (*) (Collection_1_t2523 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m17670_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m17671_gshared (Collection_1_t2523 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m17671(__this, method) (( bool (*) (Collection_1_t2523 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m17671_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m17672_gshared (Collection_1_t2523 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m17672(__this, method) (( Object_t * (*) (Collection_1_t2523 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m17672_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m17673_gshared (Collection_1_t2523 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m17673(__this, method) (( bool (*) (Collection_1_t2523 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m17673_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m17674_gshared (Collection_1_t2523 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m17674(__this, method) (( bool (*) (Collection_1_t2523 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m17674_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m17675_gshared (Collection_1_t2523 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m17675(__this, ___index, method) (( Object_t * (*) (Collection_1_t2523 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m17675_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m17676_gshared (Collection_1_t2523 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m17676(__this, ___index, ___value, method) (( void (*) (Collection_1_t2523 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m17676_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Add(T)
extern "C" void Collection_1_Add_m17677_gshared (Collection_1_t2523 * __this, Vector4_t350  ___item, const MethodInfo* method);
#define Collection_1_Add_m17677(__this, ___item, method) (( void (*) (Collection_1_t2523 *, Vector4_t350 , const MethodInfo*))Collection_1_Add_m17677_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Clear()
extern "C" void Collection_1_Clear_m17678_gshared (Collection_1_t2523 * __this, const MethodInfo* method);
#define Collection_1_Clear_m17678(__this, method) (( void (*) (Collection_1_t2523 *, const MethodInfo*))Collection_1_Clear_m17678_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::ClearItems()
extern "C" void Collection_1_ClearItems_m17679_gshared (Collection_1_t2523 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m17679(__this, method) (( void (*) (Collection_1_t2523 *, const MethodInfo*))Collection_1_ClearItems_m17679_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Contains(T)
extern "C" bool Collection_1_Contains_m17680_gshared (Collection_1_t2523 * __this, Vector4_t350  ___item, const MethodInfo* method);
#define Collection_1_Contains_m17680(__this, ___item, method) (( bool (*) (Collection_1_t2523 *, Vector4_t350 , const MethodInfo*))Collection_1_Contains_m17680_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m17681_gshared (Collection_1_t2523 * __this, Vector4U5BU5D_t457* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m17681(__this, ___array, ___index, method) (( void (*) (Collection_1_t2523 *, Vector4U5BU5D_t457*, int32_t, const MethodInfo*))Collection_1_CopyTo_m17681_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m17682_gshared (Collection_1_t2523 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m17682(__this, method) (( Object_t* (*) (Collection_1_t2523 *, const MethodInfo*))Collection_1_GetEnumerator_m17682_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m17683_gshared (Collection_1_t2523 * __this, Vector4_t350  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m17683(__this, ___item, method) (( int32_t (*) (Collection_1_t2523 *, Vector4_t350 , const MethodInfo*))Collection_1_IndexOf_m17683_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m17684_gshared (Collection_1_t2523 * __this, int32_t ___index, Vector4_t350  ___item, const MethodInfo* method);
#define Collection_1_Insert_m17684(__this, ___index, ___item, method) (( void (*) (Collection_1_t2523 *, int32_t, Vector4_t350 , const MethodInfo*))Collection_1_Insert_m17684_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m17685_gshared (Collection_1_t2523 * __this, int32_t ___index, Vector4_t350  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m17685(__this, ___index, ___item, method) (( void (*) (Collection_1_t2523 *, int32_t, Vector4_t350 , const MethodInfo*))Collection_1_InsertItem_m17685_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Remove(T)
extern "C" bool Collection_1_Remove_m17686_gshared (Collection_1_t2523 * __this, Vector4_t350  ___item, const MethodInfo* method);
#define Collection_1_Remove_m17686(__this, ___item, method) (( bool (*) (Collection_1_t2523 *, Vector4_t350 , const MethodInfo*))Collection_1_Remove_m17686_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m17687_gshared (Collection_1_t2523 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m17687(__this, ___index, method) (( void (*) (Collection_1_t2523 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m17687_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m17688_gshared (Collection_1_t2523 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m17688(__this, ___index, method) (( void (*) (Collection_1_t2523 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m17688_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::get_Count()
extern "C" int32_t Collection_1_get_Count_m17689_gshared (Collection_1_t2523 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m17689(__this, method) (( int32_t (*) (Collection_1_t2523 *, const MethodInfo*))Collection_1_get_Count_m17689_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::get_Item(System.Int32)
extern "C" Vector4_t350  Collection_1_get_Item_m17690_gshared (Collection_1_t2523 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m17690(__this, ___index, method) (( Vector4_t350  (*) (Collection_1_t2523 *, int32_t, const MethodInfo*))Collection_1_get_Item_m17690_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m17691_gshared (Collection_1_t2523 * __this, int32_t ___index, Vector4_t350  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m17691(__this, ___index, ___value, method) (( void (*) (Collection_1_t2523 *, int32_t, Vector4_t350 , const MethodInfo*))Collection_1_set_Item_m17691_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m17692_gshared (Collection_1_t2523 * __this, int32_t ___index, Vector4_t350  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m17692(__this, ___index, ___item, method) (( void (*) (Collection_1_t2523 *, int32_t, Vector4_t350 , const MethodInfo*))Collection_1_SetItem_m17692_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m17693_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m17693(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m17693_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::ConvertItem(System.Object)
extern "C" Vector4_t350  Collection_1_ConvertItem_m17694_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m17694(__this /* static, unused */, ___item, method) (( Vector4_t350  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m17694_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m17695_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m17695(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m17695_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m17696_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m17696(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m17696_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m17697_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m17697(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m17697_gshared)(__this /* static, unused */, ___list, method)
