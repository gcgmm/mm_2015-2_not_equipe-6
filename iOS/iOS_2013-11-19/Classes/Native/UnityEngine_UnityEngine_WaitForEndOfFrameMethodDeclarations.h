﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t434;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C" void WaitForEndOfFrame__ctor_m2162 (WaitForEndOfFrame_t434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
