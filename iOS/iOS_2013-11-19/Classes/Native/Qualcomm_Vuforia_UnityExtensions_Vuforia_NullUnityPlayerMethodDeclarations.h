﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.NullUnityPlayer
struct NullUnityPlayer_t115;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"

// System.Void Vuforia.NullUnityPlayer::LoadNativeLibraries()
extern "C" void NullUnityPlayer_LoadNativeLibraries_m3855 (NullUnityPlayer_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::InitializePlatform()
extern "C" void NullUnityPlayer_InitializePlatform_m3856 (NullUnityPlayer_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaUnity/InitError Vuforia.NullUnityPlayer::Start(System.String)
extern "C" int32_t NullUnityPlayer_Start_m3857 (NullUnityPlayer_t115 * __this, String_t* ___licenseKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::Update()
extern "C" void NullUnityPlayer_Update_m3858 (NullUnityPlayer_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::Dispose()
extern "C" void NullUnityPlayer_Dispose_m3859 (NullUnityPlayer_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnPause()
extern "C" void NullUnityPlayer_OnPause_m3860 (NullUnityPlayer_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnResume()
extern "C" void NullUnityPlayer_OnResume_m3861 (NullUnityPlayer_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnDestroy()
extern "C" void NullUnityPlayer_OnDestroy_m3862 (NullUnityPlayer_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::.ctor()
extern "C" void NullUnityPlayer__ctor_m265 (NullUnityPlayer_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
