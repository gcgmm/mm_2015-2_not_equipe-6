﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t91;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MonoCameraConfigura.h"
#include "UnityEngine_UnityEngine_Vector4.h"

// Vuforia.StereoCameraConfiguration
struct  StereoCameraConfiguration_t732  : public MonoCameraConfiguration_t730
{
	// UnityEngine.Camera Vuforia.StereoCameraConfiguration::mSecondaryCamera
	Camera_t91 * ___mSecondaryCamera_16;
	// System.Boolean Vuforia.StereoCameraConfiguration::mSkewFrustum
	bool ___mSkewFrustum_17;
	// System.Boolean Vuforia.StereoCameraConfiguration::mNeedToCheckStereo
	bool ___mNeedToCheckStereo_18;
	// System.Single Vuforia.StereoCameraConfiguration::mLastAppliedNearClipPlane
	float ___mLastAppliedNearClipPlane_19;
	// System.Single Vuforia.StereoCameraConfiguration::mLastAppliedFarClipPlane
	float ___mLastAppliedFarClipPlane_20;
	// System.Single Vuforia.StereoCameraConfiguration::mLastAppliedVirtualFoV
	float ___mLastAppliedVirtualFoV_21;
	// System.Single Vuforia.StereoCameraConfiguration::mNewNearClipPlane
	float ___mNewNearClipPlane_22;
	// System.Single Vuforia.StereoCameraConfiguration::mNewFarClipPlane
	float ___mNewFarClipPlane_23;
	// System.Single Vuforia.StereoCameraConfiguration::mNewVirtualFoV
	float ___mNewVirtualFoV_24;
	// System.Single Vuforia.StereoCameraConfiguration::mCameraOffset
	float ___mCameraOffset_25;
	// System.Int32 Vuforia.StereoCameraConfiguration::mEyewearUserCalibrationProfileId
	int32_t ___mEyewearUserCalibrationProfileId_26;
};
struct StereoCameraConfiguration_t732_StaticFields{
	// UnityEngine.Vector4 Vuforia.StereoCameraConfiguration::MIN_CENTER
	Vector4_t350  ___MIN_CENTER_12;
	// UnityEngine.Vector4 Vuforia.StereoCameraConfiguration::MAX_CENTER
	Vector4_t350  ___MAX_CENTER_13;
	// UnityEngine.Vector4 Vuforia.StereoCameraConfiguration::MAX_BOTTOM
	Vector4_t350  ___MAX_BOTTOM_14;
	// UnityEngine.Vector4 Vuforia.StereoCameraConfiguration::MAX_TOP
	Vector4_t350  ___MAX_TOP_15;
};
