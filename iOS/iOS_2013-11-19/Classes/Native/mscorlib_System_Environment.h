﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.OperatingSystem
struct OperatingSystem_t2030;

#include "mscorlib_System_Object.h"

// System.Environment
struct  Environment_t2029  : public Object_t
{
};
struct Environment_t2029_StaticFields{
	// System.OperatingSystem System.Environment::os
	OperatingSystem_t2030 * ___os_0;
};
