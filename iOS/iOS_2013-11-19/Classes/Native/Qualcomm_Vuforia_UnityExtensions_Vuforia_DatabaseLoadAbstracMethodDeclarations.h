﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.DatabaseLoadAbstractBehaviour
struct DatabaseLoadAbstractBehaviour_t25;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.DatabaseLoadAbstractBehaviour::LoadDatasets()
extern "C" void DatabaseLoadAbstractBehaviour_LoadDatasets_m3962 (DatabaseLoadAbstractBehaviour_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadAbstractBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C" void DatabaseLoadAbstractBehaviour_AddOSSpecificExternalDatasetSearchDirs_m3963 (DatabaseLoadAbstractBehaviour_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadAbstractBehaviour::AddExternalDatasetSearchDir(System.String)
extern "C" void DatabaseLoadAbstractBehaviour_AddExternalDatasetSearchDir_m3964 (DatabaseLoadAbstractBehaviour_t25 * __this, String_t* ___searchDir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadAbstractBehaviour::Start()
extern "C" void DatabaseLoadAbstractBehaviour_Start_m3965 (DatabaseLoadAbstractBehaviour_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadAbstractBehaviour::.ctor()
extern "C" void DatabaseLoadAbstractBehaviour__ctor_m179 (DatabaseLoadAbstractBehaviour_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
