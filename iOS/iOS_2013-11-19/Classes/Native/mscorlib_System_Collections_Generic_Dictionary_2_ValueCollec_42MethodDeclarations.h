﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t2706;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_42.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m20008_gshared (Enumerator_t2716 * __this, Dictionary_2_t2706 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m20008(__this, ___host, method) (( void (*) (Enumerator_t2716 *, Dictionary_2_t2706 *, const MethodInfo*))Enumerator__ctor_m20008_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20009_gshared (Enumerator_t2716 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20009(__this, method) (( Object_t * (*) (Enumerator_t2716 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20009_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m20010_gshared (Enumerator_t2716 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m20010(__this, method) (( void (*) (Enumerator_t2716 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m20010_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m20011_gshared (Enumerator_t2716 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20011(__this, method) (( void (*) (Enumerator_t2716 *, const MethodInfo*))Enumerator_Dispose_m20011_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20012_gshared (Enumerator_t2716 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20012(__this, method) (( bool (*) (Enumerator_t2716 *, const MethodInfo*))Enumerator_MoveNext_m20012_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m20013_gshared (Enumerator_t2716 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20013(__this, method) (( Object_t * (*) (Enumerator_t2716 *, const MethodInfo*))Enumerator_get_Current_m20013_gshared)(__this, method)
