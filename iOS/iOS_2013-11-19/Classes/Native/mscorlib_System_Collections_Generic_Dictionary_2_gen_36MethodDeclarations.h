﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3003;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2311;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t687;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t3257;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct IEnumerator_1_t3258;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1238;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>
struct KeyCollection_t3008;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>
struct ValueCollection_t3012;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_35.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__35.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
extern "C" void Dictionary_2__ctor_m24481_gshared (Dictionary_2_t3003 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m24481(__this, method) (( void (*) (Dictionary_2_t3003 *, const MethodInfo*))Dictionary_2__ctor_m24481_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m24482_gshared (Dictionary_2_t3003 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m24482(__this, ___comparer, method) (( void (*) (Dictionary_2_t3003 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m24482_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m24484_gshared (Dictionary_2_t3003 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m24484(__this, ___capacity, method) (( void (*) (Dictionary_2_t3003 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m24484_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m24486_gshared (Dictionary_2_t3003 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m24486(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3003 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2__ctor_m24486_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m24488_gshared (Dictionary_2_t3003 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m24488(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3003 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m24488_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m24490_gshared (Dictionary_2_t3003 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m24490(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3003 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m24490_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m24492_gshared (Dictionary_2_t3003 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m24492(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3003 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m24492_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m24494_gshared (Dictionary_2_t3003 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m24494(__this, ___key, method) (( bool (*) (Dictionary_2_t3003 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m24494_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m24496_gshared (Dictionary_2_t3003 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m24496(__this, ___key, method) (( void (*) (Dictionary_2_t3003 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m24496_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m24498_gshared (Dictionary_2_t3003 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m24498(__this, method) (( bool (*) (Dictionary_2_t3003 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m24498_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m24500_gshared (Dictionary_2_t3003 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m24500(__this, method) (( Object_t * (*) (Dictionary_2_t3003 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m24500_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m24502_gshared (Dictionary_2_t3003 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m24502(__this, method) (( bool (*) (Dictionary_2_t3003 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m24502_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m24504_gshared (Dictionary_2_t3003 * __this, KeyValuePair_2_t3005  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m24504(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3003 *, KeyValuePair_2_t3005 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m24504_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m24506_gshared (Dictionary_2_t3003 * __this, KeyValuePair_2_t3005  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m24506(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3003 *, KeyValuePair_2_t3005 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m24506_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m24508_gshared (Dictionary_2_t3003 * __this, KeyValuePair_2U5BU5D_t3257* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m24508(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3003 *, KeyValuePair_2U5BU5D_t3257*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m24508_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m24510_gshared (Dictionary_2_t3003 * __this, KeyValuePair_2_t3005  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m24510(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3003 *, KeyValuePair_2_t3005 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m24510_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m24512_gshared (Dictionary_2_t3003 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m24512(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3003 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m24512_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m24514_gshared (Dictionary_2_t3003 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m24514(__this, method) (( Object_t * (*) (Dictionary_2_t3003 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m24514_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m24516_gshared (Dictionary_2_t3003 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m24516(__this, method) (( Object_t* (*) (Dictionary_2_t3003 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m24516_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m24518_gshared (Dictionary_2_t3003 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m24518(__this, method) (( Object_t * (*) (Dictionary_2_t3003 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m24518_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m24520_gshared (Dictionary_2_t3003 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m24520(__this, method) (( int32_t (*) (Dictionary_2_t3003 *, const MethodInfo*))Dictionary_2_get_Count_m24520_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Item(TKey)
extern "C" bool Dictionary_2_get_Item_m24522_gshared (Dictionary_2_t3003 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m24522(__this, ___key, method) (( bool (*) (Dictionary_2_t3003 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m24522_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m24524_gshared (Dictionary_2_t3003 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m24524(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3003 *, Object_t *, bool, const MethodInfo*))Dictionary_2_set_Item_m24524_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m24526_gshared (Dictionary_2_t3003 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m24526(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3003 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m24526_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m24528_gshared (Dictionary_2_t3003 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m24528(__this, ___size, method) (( void (*) (Dictionary_2_t3003 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m24528_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m24530_gshared (Dictionary_2_t3003 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m24530(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3003 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m24530_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3005  Dictionary_2_make_pair_m24532_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m24532(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3005  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_make_pair_m24532_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m24534_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m24534(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_pick_key_m24534_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::pick_value(TKey,TValue)
extern "C" bool Dictionary_2_pick_value_m24536_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m24536(__this /* static, unused */, ___key, ___value, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_pick_value_m24536_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m24538_gshared (Dictionary_2_t3003 * __this, KeyValuePair_2U5BU5D_t3257* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m24538(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3003 *, KeyValuePair_2U5BU5D_t3257*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m24538_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Resize()
extern "C" void Dictionary_2_Resize_m24540_gshared (Dictionary_2_t3003 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m24540(__this, method) (( void (*) (Dictionary_2_t3003 *, const MethodInfo*))Dictionary_2_Resize_m24540_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m24542_gshared (Dictionary_2_t3003 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_Add_m24542(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3003 *, Object_t *, bool, const MethodInfo*))Dictionary_2_Add_m24542_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Clear()
extern "C" void Dictionary_2_Clear_m24544_gshared (Dictionary_2_t3003 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m24544(__this, method) (( void (*) (Dictionary_2_t3003 *, const MethodInfo*))Dictionary_2_Clear_m24544_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m24546_gshared (Dictionary_2_t3003 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m24546(__this, ___key, method) (( bool (*) (Dictionary_2_t3003 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m24546_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m24548_gshared (Dictionary_2_t3003 * __this, bool ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m24548(__this, ___value, method) (( bool (*) (Dictionary_2_t3003 *, bool, const MethodInfo*))Dictionary_2_ContainsValue_m24548_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m24550_gshared (Dictionary_2_t3003 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m24550(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3003 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2_GetObjectData_m24550_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m24552_gshared (Dictionary_2_t3003 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m24552(__this, ___sender, method) (( void (*) (Dictionary_2_t3003 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m24552_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m24554_gshared (Dictionary_2_t3003 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m24554(__this, ___key, method) (( bool (*) (Dictionary_2_t3003 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m24554_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m24556_gshared (Dictionary_2_t3003 * __this, Object_t * ___key, bool* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m24556(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3003 *, Object_t *, bool*, const MethodInfo*))Dictionary_2_TryGetValue_m24556_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Keys()
extern "C" KeyCollection_t3008 * Dictionary_2_get_Keys_m24558_gshared (Dictionary_2_t3003 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m24558(__this, method) (( KeyCollection_t3008 * (*) (Dictionary_2_t3003 *, const MethodInfo*))Dictionary_2_get_Keys_m24558_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Values()
extern "C" ValueCollection_t3012 * Dictionary_2_get_Values_m24560_gshared (Dictionary_2_t3003 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m24560(__this, method) (( ValueCollection_t3012 * (*) (Dictionary_2_t3003 *, const MethodInfo*))Dictionary_2_get_Values_m24560_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m24562_gshared (Dictionary_2_t3003 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m24562(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3003 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m24562_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTValue(System.Object)
extern "C" bool Dictionary_2_ToTValue_m24564_gshared (Dictionary_2_t3003 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m24564(__this, ___value, method) (( bool (*) (Dictionary_2_t3003 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m24564_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m24566_gshared (Dictionary_2_t3003 * __this, KeyValuePair_2_t3005  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m24566(__this, ___pair, method) (( bool (*) (Dictionary_2_t3003 *, KeyValuePair_2_t3005 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m24566_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetEnumerator()
extern "C" Enumerator_t3010  Dictionary_2_GetEnumerator_m24568_gshared (Dictionary_2_t3003 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m24568(__this, method) (( Enumerator_t3010  (*) (Dictionary_2_t3003 *, const MethodInfo*))Dictionary_2_GetEnumerator_m24568_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1423  Dictionary_2_U3CCopyToU3Em__0_m24570_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m24570(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1423  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m24570_gshared)(__this /* static, unused */, ___key, ___value, method)
