﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_genMethodDeclarations.h"

// System.Void UnityEngine.Events.InvokableCall`1<System.String>::.ctor(System.Object,System.Reflection.MethodInfo)
#define InvokableCall_1__ctor_m19179(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t2661 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m13935_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.String>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
#define InvokableCall_1__ctor_m19180(__this, ___action, method) (( void (*) (InvokableCall_1_t2661 *, UnityAction_1_t2421 *, const MethodInfo*))InvokableCall_1__ctor_m13936_gshared)(__this, ___action, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.String>::Invoke(System.Object[])
#define InvokableCall_1_Invoke_m19181(__this, ___args, method) (( void (*) (InvokableCall_1_t2661 *, ObjectU5BU5D_t105*, const MethodInfo*))InvokableCall_1_Invoke_m13937_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.String>::Find(System.Object,System.Reflection.MethodInfo)
#define InvokableCall_1_Find_m19182(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t2661 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m13938_gshared)(__this, ___targetObj, ___method, method)
