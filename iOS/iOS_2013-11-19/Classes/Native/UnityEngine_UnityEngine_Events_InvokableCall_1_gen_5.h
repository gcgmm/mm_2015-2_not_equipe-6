﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityAction`1<System.String>
struct UnityAction_1_t2421;

#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"

// UnityEngine.Events.InvokableCall`1<System.String>
struct  InvokableCall_1_t2661  : public BaseInvokableCall_t655
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<System.String>::Delegate
	UnityAction_1_t2421 * ___Delegate_0;
};
