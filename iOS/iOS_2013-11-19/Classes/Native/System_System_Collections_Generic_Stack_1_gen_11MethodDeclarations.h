﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor()
#define Stack_1__ctor_m17849(__this, method) (( void (*) (Stack_1_t2552 *, const MethodInfo*))Stack_1__ctor_m13420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m17850(__this, method) (( bool (*) (Stack_1_t2552 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m13421_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m17851(__this, method) (( Object_t * (*) (Stack_1_t2552 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m13422_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m17852(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t2552 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m13423_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17853(__this, method) (( Object_t* (*) (Stack_1_t2552 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13424_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m17854(__this, method) (( Object_t * (*) (Stack_1_t2552 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m13425_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Peek()
#define Stack_1_Peek_m17855(__this, method) (( List_1_t380 * (*) (Stack_1_t2552 *, const MethodInfo*))Stack_1_Peek_m13426_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Pop()
#define Stack_1_Pop_m17856(__this, method) (( List_1_t380 * (*) (Stack_1_t2552 *, const MethodInfo*))Stack_1_Pop_m13427_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Push(T)
#define Stack_1_Push_m17857(__this, ___t, method) (( void (*) (Stack_1_t2552 *, List_1_t380 *, const MethodInfo*))Stack_1_Push_m13428_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_Count()
#define Stack_1_get_Count_m17858(__this, method) (( int32_t (*) (Stack_1_t2552 *, const MethodInfo*))Stack_1_get_Count_m13429_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::GetEnumerator()
#define Stack_1_GetEnumerator_m17859(__this, method) (( Enumerator_t3181  (*) (Stack_1_t2552 *, const MethodInfo*))Stack_1_GetEnumerator_m13430_gshared)(__this, method)
