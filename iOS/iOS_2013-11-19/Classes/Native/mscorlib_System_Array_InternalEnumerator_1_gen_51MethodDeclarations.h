﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_51.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18888_gshared (InternalEnumerator_1_t2627 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18888(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2627 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18888_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18889_gshared (InternalEnumerator_1_t2627 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18889(__this, method) (( void (*) (InternalEnumerator_1_t2627 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18889_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18890_gshared (InternalEnumerator_1_t2627 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18890(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2627 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18890_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18891_gshared (InternalEnumerator_1_t2627 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18891(__this, method) (( void (*) (InternalEnumerator_1_t2627 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18891_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18892_gshared (InternalEnumerator_1_t2627 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18892(__this, method) (( bool (*) (InternalEnumerator_1_t2627 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18892_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern "C" ParameterModifier_t1710  InternalEnumerator_1_get_Current_m18893_gshared (InternalEnumerator_1_t2627 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18893(__this, method) (( ParameterModifier_t1710  (*) (InternalEnumerator_1_t2627 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18893_gshared)(__this, method)
