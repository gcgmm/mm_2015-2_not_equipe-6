﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t1037;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_48.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m23034_gshared (Enumerator_t2888 * __this, Dictionary_2_t1037 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m23034(__this, ___host, method) (( void (*) (Enumerator_t2888 *, Dictionary_2_t1037 *, const MethodInfo*))Enumerator__ctor_m23034_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23035_gshared (Enumerator_t2888 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m23035(__this, method) (( Object_t * (*) (Enumerator_t2888 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23035_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m23036_gshared (Enumerator_t2888 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m23036(__this, method) (( void (*) (Enumerator_t2888 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m23036_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C" void Enumerator_Dispose_m23037_gshared (Enumerator_t2888 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m23037(__this, method) (( void (*) (Enumerator_t2888 *, const MethodInfo*))Enumerator_Dispose_m23037_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m23038_gshared (Enumerator_t2888 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m23038(__this, method) (( bool (*) (Enumerator_t2888 *, const MethodInfo*))Enumerator_MoveNext_m23038_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C" TrackableResultData_t810  Enumerator_get_Current_m23039_gshared (Enumerator_t2888 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m23039(__this, method) (( TrackableResultData_t810  (*) (Enumerator_t2888 *, const MethodInfo*))Enumerator_get_Current_m23039_gshared)(__this, method)
