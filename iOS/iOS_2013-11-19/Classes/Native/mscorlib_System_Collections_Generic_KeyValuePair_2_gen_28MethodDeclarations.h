﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m22860(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2875 *, int32_t, TrackableBehaviour_t32 *, const MethodInfo*))KeyValuePair_2__ctor_m14271_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m22861(__this, method) (( int32_t (*) (KeyValuePair_2_t2875 *, const MethodInfo*))KeyValuePair_2_get_Key_m14272_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m22862(__this, ___value, method) (( void (*) (KeyValuePair_2_t2875 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m14273_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m22863(__this, method) (( TrackableBehaviour_t32 * (*) (KeyValuePair_2_t2875 *, const MethodInfo*))KeyValuePair_2_get_Value_m14274_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m22864(__this, ___value, method) (( void (*) (KeyValuePair_2_t2875 *, TrackableBehaviour_t32 *, const MethodInfo*))KeyValuePair_2_set_Value_m14275_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::ToString()
#define KeyValuePair_2_ToString_m22865(__this, method) (( String_t* (*) (KeyValuePair_2_t2875 *, const MethodInfo*))KeyValuePair_2_ToString_m14276_gshared)(__this, method)
