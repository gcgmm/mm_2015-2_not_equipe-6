﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_63MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::.ctor()
#define List_1__ctor_m2194(__this, method) (( void (*) (List_1_t301 *, const MethodInfo*))List_1__ctor_m12803_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m16319(__this, ___collection, method) (( void (*) (List_1_t301 *, Object_t*, const MethodInfo*))List_1__ctor_m12804_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::.ctor(System.Int32)
#define List_1__ctor_m16320(__this, ___capacity, method) (( void (*) (List_1_t301 *, int32_t, const MethodInfo*))List_1__ctor_m12806_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::.cctor()
#define List_1__cctor_m16321(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m12808_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16322(__this, method) (( Object_t* (*) (List_1_t301 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12810_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m16323(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t301 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m12812_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16324(__this, method) (( Object_t * (*) (List_1_t301 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m12814_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m16325(__this, ___item, method) (( int32_t (*) (List_1_t301 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m12816_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m16326(__this, ___item, method) (( bool (*) (List_1_t301 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m12818_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m16327(__this, ___item, method) (( int32_t (*) (List_1_t301 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m12820_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m16328(__this, ___index, ___item, method) (( void (*) (List_1_t301 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m12822_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m16329(__this, ___item, method) (( void (*) (List_1_t301 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m12824_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16330(__this, method) (( bool (*) (List_1_t301 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16331(__this, method) (( bool (*) (List_1_t301 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m12828_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m16332(__this, method) (( Object_t * (*) (List_1_t301 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m12830_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m16333(__this, method) (( bool (*) (List_1_t301 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m12832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m16334(__this, method) (( bool (*) (List_1_t301 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m12834_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m16335(__this, ___index, method) (( Object_t * (*) (List_1_t301 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m12836_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m16336(__this, ___index, ___value, method) (( void (*) (List_1_t301 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12838_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Add(T)
#define List_1_Add_m16337(__this, ___item, method) (( void (*) (List_1_t301 *, Selectable_t202 *, const MethodInfo*))List_1_Add_m12840_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m16338(__this, ___newCount, method) (( void (*) (List_1_t301 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m12842_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m16339(__this, ___collection, method) (( void (*) (List_1_t301 *, Object_t*, const MethodInfo*))List_1_AddCollection_m12844_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m16340(__this, ___enumerable, method) (( void (*) (List_1_t301 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m12846_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m16341(__this, ___collection, method) (( void (*) (List_1_t301 *, Object_t*, const MethodInfo*))List_1_AddRange_m12848_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::AsReadOnly()
#define List_1_AsReadOnly_m16342(__this, method) (( ReadOnlyCollection_1_t2437 * (*) (List_1_t301 *, const MethodInfo*))List_1_AsReadOnly_m12850_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Clear()
#define List_1_Clear_m16343(__this, method) (( void (*) (List_1_t301 *, const MethodInfo*))List_1_Clear_m12852_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Contains(T)
#define List_1_Contains_m16344(__this, ___item, method) (( bool (*) (List_1_t301 *, Selectable_t202 *, const MethodInfo*))List_1_Contains_m12854_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m16345(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t301 *, SelectableU5BU5D_t2436*, int32_t, const MethodInfo*))List_1_CopyTo_m12856_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Find(System.Predicate`1<T>)
#define List_1_Find_m16346(__this, ___match, method) (( Selectable_t202 * (*) (List_1_t301 *, Predicate_1_t2439 *, const MethodInfo*))List_1_Find_m12858_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m16347(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2439 *, const MethodInfo*))List_1_CheckMatch_m12860_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m16348(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t301 *, int32_t, int32_t, Predicate_1_t2439 *, const MethodInfo*))List_1_GetIndex_m12862_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::GetEnumerator()
#define List_1_GetEnumerator_m16349(__this, method) (( Enumerator_t2440  (*) (List_1_t301 *, const MethodInfo*))List_1_GetEnumerator_m12864_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::IndexOf(T)
#define List_1_IndexOf_m16350(__this, ___item, method) (( int32_t (*) (List_1_t301 *, Selectable_t202 *, const MethodInfo*))List_1_IndexOf_m12866_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m16351(__this, ___start, ___delta, method) (( void (*) (List_1_t301 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m12868_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m16352(__this, ___index, method) (( void (*) (List_1_t301 *, int32_t, const MethodInfo*))List_1_CheckIndex_m12870_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Insert(System.Int32,T)
#define List_1_Insert_m16353(__this, ___index, ___item, method) (( void (*) (List_1_t301 *, int32_t, Selectable_t202 *, const MethodInfo*))List_1_Insert_m12872_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m16354(__this, ___collection, method) (( void (*) (List_1_t301 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m12874_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Remove(T)
#define List_1_Remove_m16355(__this, ___item, method) (( bool (*) (List_1_t301 *, Selectable_t202 *, const MethodInfo*))List_1_Remove_m12876_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m16356(__this, ___match, method) (( int32_t (*) (List_1_t301 *, Predicate_1_t2439 *, const MethodInfo*))List_1_RemoveAll_m12878_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m16357(__this, ___index, method) (( void (*) (List_1_t301 *, int32_t, const MethodInfo*))List_1_RemoveAt_m12880_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Reverse()
#define List_1_Reverse_m16358(__this, method) (( void (*) (List_1_t301 *, const MethodInfo*))List_1_Reverse_m12882_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Sort()
#define List_1_Sort_m16359(__this, method) (( void (*) (List_1_t301 *, const MethodInfo*))List_1_Sort_m12884_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m16360(__this, ___comparison, method) (( void (*) (List_1_t301 *, Comparison_1_t2441 *, const MethodInfo*))List_1_Sort_m12886_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::ToArray()
#define List_1_ToArray_m16361(__this, method) (( SelectableU5BU5D_t2436* (*) (List_1_t301 *, const MethodInfo*))List_1_ToArray_m12888_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::TrimExcess()
#define List_1_TrimExcess_m16362(__this, method) (( void (*) (List_1_t301 *, const MethodInfo*))List_1_TrimExcess_m12890_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::get_Capacity()
#define List_1_get_Capacity_m16363(__this, method) (( int32_t (*) (List_1_t301 *, const MethodInfo*))List_1_get_Capacity_m12892_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m16364(__this, ___value, method) (( void (*) (List_1_t301 *, int32_t, const MethodInfo*))List_1_set_Capacity_m12894_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::get_Count()
#define List_1_get_Count_m16365(__this, method) (( int32_t (*) (List_1_t301 *, const MethodInfo*))List_1_get_Count_m12896_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::get_Item(System.Int32)
#define List_1_get_Item_m16366(__this, ___index, method) (( Selectable_t202 * (*) (List_1_t301 *, int32_t, const MethodInfo*))List_1_get_Item_m12898_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::set_Item(System.Int32,T)
#define List_1_set_Item_m16367(__this, ___index, ___value, method) (( void (*) (List_1_t301 *, int32_t, Selectable_t202 *, const MethodInfo*))List_1_set_Item_m12900_gshared)(__this, ___index, ___value, method)
