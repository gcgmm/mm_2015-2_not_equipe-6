﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ShimEnumerator_t2647;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2634;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m19086_gshared (ShimEnumerator_t2647 * __this, Dictionary_2_t2634 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m19086(__this, ___host, method) (( void (*) (ShimEnumerator_t2647 *, Dictionary_2_t2634 *, const MethodInfo*))ShimEnumerator__ctor_m19086_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m19087_gshared (ShimEnumerator_t2647 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m19087(__this, method) (( bool (*) (ShimEnumerator_t2647 *, const MethodInfo*))ShimEnumerator_MoveNext_m19087_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Entry()
extern "C" DictionaryEntry_t1423  ShimEnumerator_get_Entry_m19088_gshared (ShimEnumerator_t2647 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m19088(__this, method) (( DictionaryEntry_t1423  (*) (ShimEnumerator_t2647 *, const MethodInfo*))ShimEnumerator_get_Entry_m19088_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m19089_gshared (ShimEnumerator_t2647 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m19089(__this, method) (( Object_t * (*) (ShimEnumerator_t2647 *, const MethodInfo*))ShimEnumerator_get_Key_m19089_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m19090_gshared (ShimEnumerator_t2647 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m19090(__this, method) (( Object_t * (*) (ShimEnumerator_t2647 *, const MethodInfo*))ShimEnumerator_get_Value_m19090_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m19091_gshared (ShimEnumerator_t2647 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m19091(__this, method) (( Object_t * (*) (ShimEnumerator_t2647 *, const MethodInfo*))ShimEnumerator_get_Current_m19091_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C" void ShimEnumerator_Reset_m19092_gshared (ShimEnumerator_t2647 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m19092(__this, method) (( void (*) (ShimEnumerator_t2647 *, const MethodInfo*))ShimEnumerator_Reset_m19092_gshared)(__this, method)
