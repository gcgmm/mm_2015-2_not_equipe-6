﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>
struct DefaultComparer_t2507;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>::.ctor()
extern "C" void DefaultComparer__ctor_m17420_gshared (DefaultComparer_t2507 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m17420(__this, method) (( void (*) (DefaultComparer_t2507 *, const MethodInfo*))DefaultComparer__ctor_m17420_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m17421_gshared (DefaultComparer_t2507 * __this, Color32_t382  ___x, Color32_t382  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m17421(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2507 *, Color32_t382 , Color32_t382 , const MethodInfo*))DefaultComparer_Compare_m17421_gshared)(__this, ___x, ___y, method)
