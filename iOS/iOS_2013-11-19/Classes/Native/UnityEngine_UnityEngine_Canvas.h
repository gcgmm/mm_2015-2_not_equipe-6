﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t406;

#include "UnityEngine_UnityEngine_Behaviour.h"

// UnityEngine.Canvas
struct  Canvas_t230  : public Behaviour_t450
{
};
struct Canvas_t230_StaticFields{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t406 * ___willRenderCanvases_2;
};
