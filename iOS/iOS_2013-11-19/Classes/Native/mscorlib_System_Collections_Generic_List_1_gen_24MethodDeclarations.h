﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t346;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Color32>
struct IEnumerable_1_t3167;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color32>
struct IEnumerator_1_t3168;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Color32>
struct ICollection_1_t3169;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>
struct ReadOnlyCollection_1_t2500;
// UnityEngine.Color32[]
struct Color32U5BU5D_t456;
// System.Predicate`1<UnityEngine.Color32>
struct Predicate_1_t2505;
// System.Comparison`1<UnityEngine.Color32>
struct Comparison_1_t2508;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_51.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor()
extern "C" void List_1__ctor_m17276_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1__ctor_m17276(__this, method) (( void (*) (List_1_t346 *, const MethodInfo*))List_1__ctor_m17276_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m17277_gshared (List_1_t346 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m17277(__this, ___collection, method) (( void (*) (List_1_t346 *, Object_t*, const MethodInfo*))List_1__ctor_m17277_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor(System.Int32)
extern "C" void List_1__ctor_m17278_gshared (List_1_t346 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m17278(__this, ___capacity, method) (( void (*) (List_1_t346 *, int32_t, const MethodInfo*))List_1__ctor_m17278_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.cctor()
extern "C" void List_1__cctor_m17279_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m17279(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m17279_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17280_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17280(__this, method) (( Object_t* (*) (List_1_t346 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17280_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17281_gshared (List_1_t346 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m17281(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t346 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m17281_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m17282_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17282(__this, method) (( Object_t * (*) (List_1_t346 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m17282_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m17283_gshared (List_1_t346 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m17283(__this, ___item, method) (( int32_t (*) (List_1_t346 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m17283_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m17284_gshared (List_1_t346 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m17284(__this, ___item, method) (( bool (*) (List_1_t346 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m17284_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m17285_gshared (List_1_t346 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m17285(__this, ___item, method) (( int32_t (*) (List_1_t346 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m17285_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m17286_gshared (List_1_t346 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m17286(__this, ___index, ___item, method) (( void (*) (List_1_t346 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m17286_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m17287_gshared (List_1_t346 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m17287(__this, ___item, method) (( void (*) (List_1_t346 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m17287_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17288_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17288(__this, method) (( bool (*) (List_1_t346 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m17289_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17289(__this, method) (( bool (*) (List_1_t346 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m17289_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m17290_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m17290(__this, method) (( Object_t * (*) (List_1_t346 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m17290_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m17291_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m17291(__this, method) (( bool (*) (List_1_t346 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m17291_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m17292_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m17292(__this, method) (( bool (*) (List_1_t346 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m17292_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m17293_gshared (List_1_t346 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m17293(__this, ___index, method) (( Object_t * (*) (List_1_t346 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m17293_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m17294_gshared (List_1_t346 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m17294(__this, ___index, ___value, method) (( void (*) (List_1_t346 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m17294_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Add(T)
extern "C" void List_1_Add_m17295_gshared (List_1_t346 * __this, Color32_t382  ___item, const MethodInfo* method);
#define List_1_Add_m17295(__this, ___item, method) (( void (*) (List_1_t346 *, Color32_t382 , const MethodInfo*))List_1_Add_m17295_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m17296_gshared (List_1_t346 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m17296(__this, ___newCount, method) (( void (*) (List_1_t346 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m17296_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m17297_gshared (List_1_t346 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m17297(__this, ___collection, method) (( void (*) (List_1_t346 *, Object_t*, const MethodInfo*))List_1_AddCollection_m17297_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m17298_gshared (List_1_t346 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m17298(__this, ___enumerable, method) (( void (*) (List_1_t346 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m17298_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m2300_gshared (List_1_t346 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m2300(__this, ___collection, method) (( void (*) (List_1_t346 *, Object_t*, const MethodInfo*))List_1_AddRange_m2300_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Color32>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2500 * List_1_AsReadOnly_m17299_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m17299(__this, method) (( ReadOnlyCollection_1_t2500 * (*) (List_1_t346 *, const MethodInfo*))List_1_AsReadOnly_m17299_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Clear()
extern "C" void List_1_Clear_m17300_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_Clear_m17300(__this, method) (( void (*) (List_1_t346 *, const MethodInfo*))List_1_Clear_m17300_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::Contains(T)
extern "C" bool List_1_Contains_m17301_gshared (List_1_t346 * __this, Color32_t382  ___item, const MethodInfo* method);
#define List_1_Contains_m17301(__this, ___item, method) (( bool (*) (List_1_t346 *, Color32_t382 , const MethodInfo*))List_1_Contains_m17301_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m17302_gshared (List_1_t346 * __this, Color32U5BU5D_t456* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m17302(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t346 *, Color32U5BU5D_t456*, int32_t, const MethodInfo*))List_1_CopyTo_m17302_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Color32>::Find(System.Predicate`1<T>)
extern "C" Color32_t382  List_1_Find_m17303_gshared (List_1_t346 * __this, Predicate_1_t2505 * ___match, const MethodInfo* method);
#define List_1_Find_m17303(__this, ___match, method) (( Color32_t382  (*) (List_1_t346 *, Predicate_1_t2505 *, const MethodInfo*))List_1_Find_m17303_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m17304_gshared (Object_t * __this /* static, unused */, Predicate_1_t2505 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m17304(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2505 *, const MethodInfo*))List_1_CheckMatch_m17304_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m17305_gshared (List_1_t346 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2505 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m17305(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t346 *, int32_t, int32_t, Predicate_1_t2505 *, const MethodInfo*))List_1_GetIndex_m17305_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Color32>::GetEnumerator()
extern "C" Enumerator_t2499  List_1_GetEnumerator_m17306_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m17306(__this, method) (( Enumerator_t2499  (*) (List_1_t346 *, const MethodInfo*))List_1_GetEnumerator_m17306_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m17307_gshared (List_1_t346 * __this, Color32_t382  ___item, const MethodInfo* method);
#define List_1_IndexOf_m17307(__this, ___item, method) (( int32_t (*) (List_1_t346 *, Color32_t382 , const MethodInfo*))List_1_IndexOf_m17307_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m17308_gshared (List_1_t346 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m17308(__this, ___start, ___delta, method) (( void (*) (List_1_t346 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m17308_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m17309_gshared (List_1_t346 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m17309(__this, ___index, method) (( void (*) (List_1_t346 *, int32_t, const MethodInfo*))List_1_CheckIndex_m17309_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m17310_gshared (List_1_t346 * __this, int32_t ___index, Color32_t382  ___item, const MethodInfo* method);
#define List_1_Insert_m17310(__this, ___index, ___item, method) (( void (*) (List_1_t346 *, int32_t, Color32_t382 , const MethodInfo*))List_1_Insert_m17310_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m17311_gshared (List_1_t346 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m17311(__this, ___collection, method) (( void (*) (List_1_t346 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m17311_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::Remove(T)
extern "C" bool List_1_Remove_m17312_gshared (List_1_t346 * __this, Color32_t382  ___item, const MethodInfo* method);
#define List_1_Remove_m17312(__this, ___item, method) (( bool (*) (List_1_t346 *, Color32_t382 , const MethodInfo*))List_1_Remove_m17312_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m17313_gshared (List_1_t346 * __this, Predicate_1_t2505 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m17313(__this, ___match, method) (( int32_t (*) (List_1_t346 *, Predicate_1_t2505 *, const MethodInfo*))List_1_RemoveAll_m17313_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m17314_gshared (List_1_t346 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m17314(__this, ___index, method) (( void (*) (List_1_t346 *, int32_t, const MethodInfo*))List_1_RemoveAt_m17314_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Reverse()
extern "C" void List_1_Reverse_m17315_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_Reverse_m17315(__this, method) (( void (*) (List_1_t346 *, const MethodInfo*))List_1_Reverse_m17315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Sort()
extern "C" void List_1_Sort_m17316_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_Sort_m17316(__this, method) (( void (*) (List_1_t346 *, const MethodInfo*))List_1_Sort_m17316_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m17317_gshared (List_1_t346 * __this, Comparison_1_t2508 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m17317(__this, ___comparison, method) (( void (*) (List_1_t346 *, Comparison_1_t2508 *, const MethodInfo*))List_1_Sort_m17317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Color32>::ToArray()
extern "C" Color32U5BU5D_t456* List_1_ToArray_m17318_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_ToArray_m17318(__this, method) (( Color32U5BU5D_t456* (*) (List_1_t346 *, const MethodInfo*))List_1_ToArray_m17318_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::TrimExcess()
extern "C" void List_1_TrimExcess_m17319_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m17319(__this, method) (( void (*) (List_1_t346 *, const MethodInfo*))List_1_TrimExcess_m17319_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m17320_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m17320(__this, method) (( int32_t (*) (List_1_t346 *, const MethodInfo*))List_1_get_Capacity_m17320_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m17321_gshared (List_1_t346 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m17321(__this, ___value, method) (( void (*) (List_1_t346 *, int32_t, const MethodInfo*))List_1_set_Capacity_m17321_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::get_Count()
extern "C" int32_t List_1_get_Count_m17322_gshared (List_1_t346 * __this, const MethodInfo* method);
#define List_1_get_Count_m17322(__this, method) (( int32_t (*) (List_1_t346 *, const MethodInfo*))List_1_get_Count_m17322_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Color32>::get_Item(System.Int32)
extern "C" Color32_t382  List_1_get_Item_m17323_gshared (List_1_t346 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m17323(__this, ___index, method) (( Color32_t382  (*) (List_1_t346 *, int32_t, const MethodInfo*))List_1_get_Item_m17323_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m17324_gshared (List_1_t346 * __this, int32_t ___index, Color32_t382  ___value, const MethodInfo* method);
#define List_1_set_Item_m17324(__this, ___index, ___value, method) (( void (*) (List_1_t346 *, int32_t, Color32_t382 , const MethodInfo*))List_1_set_Item_m17324_gshared)(__this, ___index, ___value, method)
