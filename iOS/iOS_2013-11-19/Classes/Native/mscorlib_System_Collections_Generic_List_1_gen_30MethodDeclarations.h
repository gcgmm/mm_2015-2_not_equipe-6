﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t568;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UICharInfo>
struct IEnumerable_1_t3182;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t3183;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>
struct ICollection_1_t3184;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>
struct ReadOnlyCollection_1_t2585;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t683;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t2589;
// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t2592;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_55.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void List_1__ctor_m18178_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1__ctor_m18178(__this, method) (( void (*) (List_1_t568 *, const MethodInfo*))List_1__ctor_m18178_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m18179_gshared (List_1_t568 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m18179(__this, ___collection, method) (( void (*) (List_1_t568 *, Object_t*, const MethodInfo*))List_1__ctor_m18179_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m3586_gshared (List_1_t568 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m3586(__this, ___capacity, method) (( void (*) (List_1_t568 *, int32_t, const MethodInfo*))List_1__ctor_m3586_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.cctor()
extern "C" void List_1__cctor_m18180_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m18180(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m18180_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18181_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18181(__this, method) (( Object_t* (*) (List_1_t568 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18181_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m18182_gshared (List_1_t568 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m18182(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t568 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m18182_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m18183_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18183(__this, method) (( Object_t * (*) (List_1_t568 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m18183_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m18184_gshared (List_1_t568 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m18184(__this, ___item, method) (( int32_t (*) (List_1_t568 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m18184_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m18185_gshared (List_1_t568 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m18185(__this, ___item, method) (( bool (*) (List_1_t568 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m18185_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m18186_gshared (List_1_t568 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m18186(__this, ___item, method) (( int32_t (*) (List_1_t568 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m18186_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m18187_gshared (List_1_t568 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m18187(__this, ___index, ___item, method) (( void (*) (List_1_t568 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m18187_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m18188_gshared (List_1_t568 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m18188(__this, ___item, method) (( void (*) (List_1_t568 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m18188_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18189_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18189(__this, method) (( bool (*) (List_1_t568 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18189_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m18190_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18190(__this, method) (( bool (*) (List_1_t568 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m18190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m18191_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m18191(__this, method) (( Object_t * (*) (List_1_t568 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m18191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m18192_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m18192(__this, method) (( bool (*) (List_1_t568 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m18192_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m18193_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m18193(__this, method) (( bool (*) (List_1_t568 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m18193_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m18194_gshared (List_1_t568 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m18194(__this, ___index, method) (( Object_t * (*) (List_1_t568 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m18194_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m18195_gshared (List_1_t568 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m18195(__this, ___index, ___value, method) (( void (*) (List_1_t568 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m18195_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void List_1_Add_m18196_gshared (List_1_t568 * __this, UICharInfo_t430  ___item, const MethodInfo* method);
#define List_1_Add_m18196(__this, ___item, method) (( void (*) (List_1_t568 *, UICharInfo_t430 , const MethodInfo*))List_1_Add_m18196_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m18197_gshared (List_1_t568 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m18197(__this, ___newCount, method) (( void (*) (List_1_t568 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m18197_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m18198_gshared (List_1_t568 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m18198(__this, ___collection, method) (( void (*) (List_1_t568 *, Object_t*, const MethodInfo*))List_1_AddCollection_m18198_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m18199_gshared (List_1_t568 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m18199(__this, ___enumerable, method) (( void (*) (List_1_t568 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m18199_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m18200_gshared (List_1_t568 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m18200(__this, ___collection, method) (( void (*) (List_1_t568 *, Object_t*, const MethodInfo*))List_1_AddRange_m18200_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2585 * List_1_AsReadOnly_m18201_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m18201(__this, method) (( ReadOnlyCollection_1_t2585 * (*) (List_1_t568 *, const MethodInfo*))List_1_AsReadOnly_m18201_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Clear()
extern "C" void List_1_Clear_m18202_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_Clear_m18202(__this, method) (( void (*) (List_1_t568 *, const MethodInfo*))List_1_Clear_m18202_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool List_1_Contains_m18203_gshared (List_1_t568 * __this, UICharInfo_t430  ___item, const MethodInfo* method);
#define List_1_Contains_m18203(__this, ___item, method) (( bool (*) (List_1_t568 *, UICharInfo_t430 , const MethodInfo*))List_1_Contains_m18203_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m18204_gshared (List_1_t568 * __this, UICharInfoU5BU5D_t683* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m18204(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t568 *, UICharInfoU5BU5D_t683*, int32_t, const MethodInfo*))List_1_CopyTo_m18204_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Find(System.Predicate`1<T>)
extern "C" UICharInfo_t430  List_1_Find_m18205_gshared (List_1_t568 * __this, Predicate_1_t2589 * ___match, const MethodInfo* method);
#define List_1_Find_m18205(__this, ___match, method) (( UICharInfo_t430  (*) (List_1_t568 *, Predicate_1_t2589 *, const MethodInfo*))List_1_Find_m18205_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m18206_gshared (Object_t * __this /* static, unused */, Predicate_1_t2589 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m18206(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2589 *, const MethodInfo*))List_1_CheckMatch_m18206_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m18207_gshared (List_1_t568 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2589 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m18207(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t568 *, int32_t, int32_t, Predicate_1_t2589 *, const MethodInfo*))List_1_GetIndex_m18207_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Enumerator_t2584  List_1_GetEnumerator_m18208_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m18208(__this, method) (( Enumerator_t2584  (*) (List_1_t568 *, const MethodInfo*))List_1_GetEnumerator_m18208_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m18209_gshared (List_1_t568 * __this, UICharInfo_t430  ___item, const MethodInfo* method);
#define List_1_IndexOf_m18209(__this, ___item, method) (( int32_t (*) (List_1_t568 *, UICharInfo_t430 , const MethodInfo*))List_1_IndexOf_m18209_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m18210_gshared (List_1_t568 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m18210(__this, ___start, ___delta, method) (( void (*) (List_1_t568 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m18210_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m18211_gshared (List_1_t568 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m18211(__this, ___index, method) (( void (*) (List_1_t568 *, int32_t, const MethodInfo*))List_1_CheckIndex_m18211_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m18212_gshared (List_1_t568 * __this, int32_t ___index, UICharInfo_t430  ___item, const MethodInfo* method);
#define List_1_Insert_m18212(__this, ___index, ___item, method) (( void (*) (List_1_t568 *, int32_t, UICharInfo_t430 , const MethodInfo*))List_1_Insert_m18212_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m18213_gshared (List_1_t568 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m18213(__this, ___collection, method) (( void (*) (List_1_t568 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m18213_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool List_1_Remove_m18214_gshared (List_1_t568 * __this, UICharInfo_t430  ___item, const MethodInfo* method);
#define List_1_Remove_m18214(__this, ___item, method) (( bool (*) (List_1_t568 *, UICharInfo_t430 , const MethodInfo*))List_1_Remove_m18214_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m18215_gshared (List_1_t568 * __this, Predicate_1_t2589 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m18215(__this, ___match, method) (( int32_t (*) (List_1_t568 *, Predicate_1_t2589 *, const MethodInfo*))List_1_RemoveAll_m18215_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m18216_gshared (List_1_t568 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m18216(__this, ___index, method) (( void (*) (List_1_t568 *, int32_t, const MethodInfo*))List_1_RemoveAt_m18216_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Reverse()
extern "C" void List_1_Reverse_m18217_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_Reverse_m18217(__this, method) (( void (*) (List_1_t568 *, const MethodInfo*))List_1_Reverse_m18217_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort()
extern "C" void List_1_Sort_m18218_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_Sort_m18218(__this, method) (( void (*) (List_1_t568 *, const MethodInfo*))List_1_Sort_m18218_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m18219_gshared (List_1_t568 * __this, Comparison_1_t2592 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m18219(__this, ___comparison, method) (( void (*) (List_1_t568 *, Comparison_1_t2592 *, const MethodInfo*))List_1_Sort_m18219_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UICharInfo>::ToArray()
extern "C" UICharInfoU5BU5D_t683* List_1_ToArray_m18220_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_ToArray_m18220(__this, method) (( UICharInfoU5BU5D_t683* (*) (List_1_t568 *, const MethodInfo*))List_1_ToArray_m18220_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m18221_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m18221(__this, method) (( void (*) (List_1_t568 *, const MethodInfo*))List_1_TrimExcess_m18221_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m18222_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m18222(__this, method) (( int32_t (*) (List_1_t568 *, const MethodInfo*))List_1_get_Capacity_m18222_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m18223_gshared (List_1_t568 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m18223(__this, ___value, method) (( void (*) (List_1_t568 *, int32_t, const MethodInfo*))List_1_set_Capacity_m18223_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m18224_gshared (List_1_t568 * __this, const MethodInfo* method);
#define List_1_get_Count_m18224(__this, method) (( int32_t (*) (List_1_t568 *, const MethodInfo*))List_1_get_Count_m18224_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t430  List_1_get_Item_m18225_gshared (List_1_t568 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m18225(__this, ___index, method) (( UICharInfo_t430  (*) (List_1_t568 *, int32_t, const MethodInfo*))List_1_get_Item_m18225_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m18226_gshared (List_1_t568 * __this, int32_t ___index, UICharInfo_t430  ___value, const MethodInfo* method);
#define List_1_set_Item_m18226(__this, ___index, ___value, method) (( void (*) (List_1_t568 *, int32_t, UICharInfo_t430 , const MethodInfo*))List_1_set_Item_m18226_gshared)(__this, ___index, ___value, method)
