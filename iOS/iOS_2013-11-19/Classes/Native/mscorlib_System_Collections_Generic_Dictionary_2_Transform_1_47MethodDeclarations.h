﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Transform_1_t2889;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t260;
// System.AsyncCallback
struct AsyncCallback_t261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m23040_gshared (Transform_1_t2889 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m23040(__this, ___object, ___method, method) (( void (*) (Transform_1_t2889 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m23040_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::Invoke(TKey,TValue)
extern "C" TrackableResultData_t810  Transform_1_Invoke_m23041_gshared (Transform_1_t2889 * __this, int32_t ___key, TrackableResultData_t810  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m23041(__this, ___key, ___value, method) (( TrackableResultData_t810  (*) (Transform_1_t2889 *, int32_t, TrackableResultData_t810 , const MethodInfo*))Transform_1_Invoke_m23041_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m23042_gshared (Transform_1_t2889 * __this, int32_t ___key, TrackableResultData_t810  ___value, AsyncCallback_t261 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m23042(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2889 *, int32_t, TrackableResultData_t810 , AsyncCallback_t261 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m23042_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::EndInvoke(System.IAsyncResult)
extern "C" TrackableResultData_t810  Transform_1_EndInvoke_m23043_gshared (Transform_1_t2889 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m23043(__this, ___result, method) (( TrackableResultData_t810  (*) (Transform_1_t2889 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m23043_gshared)(__this, ___result, method)
