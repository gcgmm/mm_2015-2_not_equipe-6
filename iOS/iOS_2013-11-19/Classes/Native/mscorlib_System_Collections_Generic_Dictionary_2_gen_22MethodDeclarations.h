﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t1037;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2277;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t687;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>[]
struct KeyValuePair_2U5BU5D_t3237;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>
struct IEnumerator_1_t3238;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1238;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct KeyCollection_t2883;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct ValueCollection_t2887;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__29.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor()
extern "C" void Dictionary_2__ctor_m5484_gshared (Dictionary_2_t1037 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m5484(__this, method) (( void (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2__ctor_m5484_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m22925_gshared (Dictionary_2_t1037 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m22925(__this, ___comparer, method) (( void (*) (Dictionary_2_t1037 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22925_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m22926_gshared (Dictionary_2_t1037 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m22926(__this, ___capacity, method) (( void (*) (Dictionary_2_t1037 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m22926_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m22927_gshared (Dictionary_2_t1037 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m22927(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1037 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2__ctor_m22927_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m22928_gshared (Dictionary_2_t1037 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m22928(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1037 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m22928_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22929_gshared (Dictionary_2_t1037 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m22929(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1037 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m22929_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22930_gshared (Dictionary_2_t1037 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m22930(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1037 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m22930_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m22931_gshared (Dictionary_2_t1037 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m22931(__this, ___key, method) (( bool (*) (Dictionary_2_t1037 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m22931_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22932_gshared (Dictionary_2_t1037 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m22932(__this, ___key, method) (( void (*) (Dictionary_2_t1037 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m22932_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22933_gshared (Dictionary_2_t1037 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22933(__this, method) (( bool (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22933_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22934_gshared (Dictionary_2_t1037 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22934(__this, method) (( Object_t * (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22934_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22935_gshared (Dictionary_2_t1037 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22935(__this, method) (( bool (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22935_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22936_gshared (Dictionary_2_t1037 * __this, KeyValuePair_2_t2881  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22936(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1037 *, KeyValuePair_2_t2881 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22936_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22937_gshared (Dictionary_2_t1037 * __this, KeyValuePair_2_t2881  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22937(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1037 *, KeyValuePair_2_t2881 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22937_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22938_gshared (Dictionary_2_t1037 * __this, KeyValuePair_2U5BU5D_t3237* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22938(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1037 *, KeyValuePair_2U5BU5D_t3237*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22938_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22939_gshared (Dictionary_2_t1037 * __this, KeyValuePair_2_t2881  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22939(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1037 *, KeyValuePair_2_t2881 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22939_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22940_gshared (Dictionary_2_t1037 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m22940(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1037 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m22940_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22941_gshared (Dictionary_2_t1037 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22941(__this, method) (( Object_t * (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22941_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22942_gshared (Dictionary_2_t1037 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22942(__this, method) (( Object_t* (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22942_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22943_gshared (Dictionary_2_t1037 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22943(__this, method) (( Object_t * (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22943_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m22944_gshared (Dictionary_2_t1037 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m22944(__this, method) (( int32_t (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_get_Count_m22944_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Item(TKey)
extern "C" TrackableResultData_t810  Dictionary_2_get_Item_m22945_gshared (Dictionary_2_t1037 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m22945(__this, ___key, method) (( TrackableResultData_t810  (*) (Dictionary_2_t1037 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m22945_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m22946_gshared (Dictionary_2_t1037 * __this, int32_t ___key, TrackableResultData_t810  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m22946(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1037 *, int32_t, TrackableResultData_t810 , const MethodInfo*))Dictionary_2_set_Item_m22946_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m22947_gshared (Dictionary_2_t1037 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m22947(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1037 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m22947_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m22948_gshared (Dictionary_2_t1037 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m22948(__this, ___size, method) (( void (*) (Dictionary_2_t1037 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m22948_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m22949_gshared (Dictionary_2_t1037 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m22949(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1037 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m22949_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2881  Dictionary_2_make_pair_m22950_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t810  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m22950(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2881  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t810 , const MethodInfo*))Dictionary_2_make_pair_m22950_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m22951_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t810  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m22951(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t810 , const MethodInfo*))Dictionary_2_pick_key_m22951_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::pick_value(TKey,TValue)
extern "C" TrackableResultData_t810  Dictionary_2_pick_value_m22952_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t810  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m22952(__this /* static, unused */, ___key, ___value, method) (( TrackableResultData_t810  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t810 , const MethodInfo*))Dictionary_2_pick_value_m22952_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m22953_gshared (Dictionary_2_t1037 * __this, KeyValuePair_2U5BU5D_t3237* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m22953(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1037 *, KeyValuePair_2U5BU5D_t3237*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m22953_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Resize()
extern "C" void Dictionary_2_Resize_m22954_gshared (Dictionary_2_t1037 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m22954(__this, method) (( void (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_Resize_m22954_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m22955_gshared (Dictionary_2_t1037 * __this, int32_t ___key, TrackableResultData_t810  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m22955(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1037 *, int32_t, TrackableResultData_t810 , const MethodInfo*))Dictionary_2_Add_m22955_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Clear()
extern "C" void Dictionary_2_Clear_m22956_gshared (Dictionary_2_t1037 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m22956(__this, method) (( void (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_Clear_m22956_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m22957_gshared (Dictionary_2_t1037 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m22957(__this, ___key, method) (( bool (*) (Dictionary_2_t1037 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m22957_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m22958_gshared (Dictionary_2_t1037 * __this, TrackableResultData_t810  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m22958(__this, ___value, method) (( bool (*) (Dictionary_2_t1037 *, TrackableResultData_t810 , const MethodInfo*))Dictionary_2_ContainsValue_m22958_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m22959_gshared (Dictionary_2_t1037 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m22959(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1037 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2_GetObjectData_m22959_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m22960_gshared (Dictionary_2_t1037 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m22960(__this, ___sender, method) (( void (*) (Dictionary_2_t1037 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m22960_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m22961_gshared (Dictionary_2_t1037 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m22961(__this, ___key, method) (( bool (*) (Dictionary_2_t1037 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m22961_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m22962_gshared (Dictionary_2_t1037 * __this, int32_t ___key, TrackableResultData_t810 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m22962(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1037 *, int32_t, TrackableResultData_t810 *, const MethodInfo*))Dictionary_2_TryGetValue_m22962_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Keys()
extern "C" KeyCollection_t2883 * Dictionary_2_get_Keys_m22963_gshared (Dictionary_2_t1037 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m22963(__this, method) (( KeyCollection_t2883 * (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_get_Keys_m22963_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Values()
extern "C" ValueCollection_t2887 * Dictionary_2_get_Values_m22964_gshared (Dictionary_2_t1037 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m22964(__this, method) (( ValueCollection_t2887 * (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_get_Values_m22964_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m22965_gshared (Dictionary_2_t1037 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m22965(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1037 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m22965_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToTValue(System.Object)
extern "C" TrackableResultData_t810  Dictionary_2_ToTValue_m22966_gshared (Dictionary_2_t1037 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m22966(__this, ___value, method) (( TrackableResultData_t810  (*) (Dictionary_2_t1037 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m22966_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m22967_gshared (Dictionary_2_t1037 * __this, KeyValuePair_2_t2881  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m22967(__this, ___pair, method) (( bool (*) (Dictionary_2_t1037 *, KeyValuePair_2_t2881 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m22967_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::GetEnumerator()
extern "C" Enumerator_t2885  Dictionary_2_GetEnumerator_m22968_gshared (Dictionary_2_t1037 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m22968(__this, method) (( Enumerator_t2885  (*) (Dictionary_2_t1037 *, const MethodInfo*))Dictionary_2_GetEnumerator_m22968_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1423  Dictionary_2_U3CCopyToU3Em__0_m22969_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t810  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m22969(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1423  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t810 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m22969_gshared)(__this /* static, unused */, ___key, ___value, method)
