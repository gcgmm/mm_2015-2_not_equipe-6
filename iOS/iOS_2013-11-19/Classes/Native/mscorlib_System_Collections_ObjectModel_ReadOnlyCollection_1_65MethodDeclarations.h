﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_2MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m24363(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2988 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m12908_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24364(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2988 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m12909_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24365(__this, method) (( void (*) (ReadOnlyCollection_1_t2988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m12910_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24366(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2988 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m12911_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24367(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2988 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m12912_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24368(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m12913_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24369(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m12914_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24370(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2988 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m12915_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24371(__this, method) (( bool (*) (ReadOnlyCollection_1_t2988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12916_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24372(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2988 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m12917_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24373(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m12918_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m24374(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2988 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m12919_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m24375(__this, method) (( void (*) (ReadOnlyCollection_1_t2988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m12920_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m24376(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2988 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m12921_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24377(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2988 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m12922_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m24378(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2988 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m12923_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m24379(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2988 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m12924_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24380(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m12925_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24381(__this, method) (( bool (*) (ReadOnlyCollection_1_t2988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m12926_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24382(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m12927_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24383(__this, method) (( bool (*) (ReadOnlyCollection_1_t2988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m12928_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24384(__this, method) (( bool (*) (ReadOnlyCollection_1_t2988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m12929_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m24385(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m12930_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m24386(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2988 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m12931_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::Contains(T)
#define ReadOnlyCollection_1_Contains_m24387(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2988 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_Contains_m12932_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m24388(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2988 *, IVirtualButtonEventHandlerU5BU5D_t2987*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m12933_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m24389(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2988 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m12934_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m24390(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2988 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m12935_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::get_Count()
#define ReadOnlyCollection_1_get_Count_m24391(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2988 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m12936_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m24392(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m12937_gshared)(__this, ___index, method)
