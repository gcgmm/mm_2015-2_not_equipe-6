﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t2790;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2311;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t687;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t3215;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>
struct IEnumerator_1_t3216;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1238;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt16>
struct KeyCollection_t2795;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt16>
struct ValueCollection_t2799;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor()
extern "C" void Dictionary_2__ctor_m21162_gshared (Dictionary_2_t2790 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m21162(__this, method) (( void (*) (Dictionary_2_t2790 *, const MethodInfo*))Dictionary_2__ctor_m21162_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m21164_gshared (Dictionary_2_t2790 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m21164(__this, ___comparer, method) (( void (*) (Dictionary_2_t2790 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21164_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m21166_gshared (Dictionary_2_t2790 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m21166(__this, ___capacity, method) (( void (*) (Dictionary_2_t2790 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m21166_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m21168_gshared (Dictionary_2_t2790 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m21168(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2790 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2__ctor_m21168_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m21170_gshared (Dictionary_2_t2790 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m21170(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2790 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m21170_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m21172_gshared (Dictionary_2_t2790 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m21172(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2790 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m21172_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m21174_gshared (Dictionary_2_t2790 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m21174(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2790 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m21174_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m21176_gshared (Dictionary_2_t2790 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m21176(__this, ___key, method) (( bool (*) (Dictionary_2_t2790 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m21176_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m21178_gshared (Dictionary_2_t2790 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m21178(__this, ___key, method) (( void (*) (Dictionary_2_t2790 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m21178_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21180_gshared (Dictionary_2_t2790 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21180(__this, method) (( bool (*) (Dictionary_2_t2790 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21180_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21182_gshared (Dictionary_2_t2790 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21182(__this, method) (( Object_t * (*) (Dictionary_2_t2790 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21182_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21184_gshared (Dictionary_2_t2790 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21184(__this, method) (( bool (*) (Dictionary_2_t2790 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21184_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21186_gshared (Dictionary_2_t2790 * __this, KeyValuePair_2_t2792  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21186(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2790 *, KeyValuePair_2_t2792 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21186_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21188_gshared (Dictionary_2_t2790 * __this, KeyValuePair_2_t2792  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21188(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2790 *, KeyValuePair_2_t2792 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21188_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21190_gshared (Dictionary_2_t2790 * __this, KeyValuePair_2U5BU5D_t3215* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21190(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2790 *, KeyValuePair_2U5BU5D_t3215*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21190_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21192_gshared (Dictionary_2_t2790 * __this, KeyValuePair_2_t2792  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21192(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2790 *, KeyValuePair_2_t2792 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21192_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m21194_gshared (Dictionary_2_t2790 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m21194(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2790 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m21194_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21196_gshared (Dictionary_2_t2790 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21196(__this, method) (( Object_t * (*) (Dictionary_2_t2790 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21196_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21198_gshared (Dictionary_2_t2790 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21198(__this, method) (( Object_t* (*) (Dictionary_2_t2790 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21198_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21200_gshared (Dictionary_2_t2790 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21200(__this, method) (( Object_t * (*) (Dictionary_2_t2790 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21200_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m21202_gshared (Dictionary_2_t2790 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m21202(__this, method) (( int32_t (*) (Dictionary_2_t2790 *, const MethodInfo*))Dictionary_2_get_Count_m21202_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Item(TKey)
extern "C" uint16_t Dictionary_2_get_Item_m21204_gshared (Dictionary_2_t2790 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m21204(__this, ___key, method) (( uint16_t (*) (Dictionary_2_t2790 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m21204_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m21206_gshared (Dictionary_2_t2790 * __this, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m21206(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2790 *, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_set_Item_m21206_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m21208_gshared (Dictionary_2_t2790 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m21208(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2790 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m21208_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m21210_gshared (Dictionary_2_t2790 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m21210(__this, ___size, method) (( void (*) (Dictionary_2_t2790 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m21210_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m21212_gshared (Dictionary_2_t2790 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m21212(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2790 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m21212_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2792  Dictionary_2_make_pair_m21214_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m21214(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2792  (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_make_pair_m21214_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m21216_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m21216(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_pick_key_m21216_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::pick_value(TKey,TValue)
extern "C" uint16_t Dictionary_2_pick_value_m21218_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m21218(__this /* static, unused */, ___key, ___value, method) (( uint16_t (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_pick_value_m21218_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m21220_gshared (Dictionary_2_t2790 * __this, KeyValuePair_2U5BU5D_t3215* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m21220(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2790 *, KeyValuePair_2U5BU5D_t3215*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m21220_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Resize()
extern "C" void Dictionary_2_Resize_m21222_gshared (Dictionary_2_t2790 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m21222(__this, method) (( void (*) (Dictionary_2_t2790 *, const MethodInfo*))Dictionary_2_Resize_m21222_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m21224_gshared (Dictionary_2_t2790 * __this, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m21224(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2790 *, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_Add_m21224_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Clear()
extern "C" void Dictionary_2_Clear_m21226_gshared (Dictionary_2_t2790 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m21226(__this, method) (( void (*) (Dictionary_2_t2790 *, const MethodInfo*))Dictionary_2_Clear_m21226_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m21228_gshared (Dictionary_2_t2790 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m21228(__this, ___key, method) (( bool (*) (Dictionary_2_t2790 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m21228_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m21230_gshared (Dictionary_2_t2790 * __this, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m21230(__this, ___value, method) (( bool (*) (Dictionary_2_t2790 *, uint16_t, const MethodInfo*))Dictionary_2_ContainsValue_m21230_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m21232_gshared (Dictionary_2_t2790 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m21232(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2790 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2_GetObjectData_m21232_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m21234_gshared (Dictionary_2_t2790 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m21234(__this, ___sender, method) (( void (*) (Dictionary_2_t2790 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m21234_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m21236_gshared (Dictionary_2_t2790 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m21236(__this, ___key, method) (( bool (*) (Dictionary_2_t2790 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m21236_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m21238_gshared (Dictionary_2_t2790 * __this, Object_t * ___key, uint16_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m21238(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2790 *, Object_t *, uint16_t*, const MethodInfo*))Dictionary_2_TryGetValue_m21238_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Keys()
extern "C" KeyCollection_t2795 * Dictionary_2_get_Keys_m21240_gshared (Dictionary_2_t2790 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m21240(__this, method) (( KeyCollection_t2795 * (*) (Dictionary_2_t2790 *, const MethodInfo*))Dictionary_2_get_Keys_m21240_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Values()
extern "C" ValueCollection_t2799 * Dictionary_2_get_Values_m21242_gshared (Dictionary_2_t2790 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m21242(__this, method) (( ValueCollection_t2799 * (*) (Dictionary_2_t2790 *, const MethodInfo*))Dictionary_2_get_Values_m21242_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m21244_gshared (Dictionary_2_t2790 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m21244(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2790 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m21244_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ToTValue(System.Object)
extern "C" uint16_t Dictionary_2_ToTValue_m21246_gshared (Dictionary_2_t2790 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m21246(__this, ___value, method) (( uint16_t (*) (Dictionary_2_t2790 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m21246_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m21248_gshared (Dictionary_2_t2790 * __this, KeyValuePair_2_t2792  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m21248(__this, ___pair, method) (( bool (*) (Dictionary_2_t2790 *, KeyValuePair_2_t2792 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m21248_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::GetEnumerator()
extern "C" Enumerator_t2797  Dictionary_2_GetEnumerator_m21250_gshared (Dictionary_2_t2790 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m21250(__this, method) (( Enumerator_t2797  (*) (Dictionary_2_t2790 *, const MethodInfo*))Dictionary_2_GetEnumerator_m21250_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1423  Dictionary_2_U3CCopyToU3Em__0_m21252_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m21252(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1423  (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m21252_gshared)(__this /* static, unused */, ___key, ___value, method)
