﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void VideoBGCfgData_t830_marshal(const VideoBGCfgData_t830& unmarshaled, VideoBGCfgData_t830_marshaled& marshaled);
extern "C" void VideoBGCfgData_t830_marshal_back(const VideoBGCfgData_t830_marshaled& marshaled, VideoBGCfgData_t830& unmarshaled);
extern "C" void VideoBGCfgData_t830_marshal_cleanup(VideoBGCfgData_t830_marshaled& marshaled);
