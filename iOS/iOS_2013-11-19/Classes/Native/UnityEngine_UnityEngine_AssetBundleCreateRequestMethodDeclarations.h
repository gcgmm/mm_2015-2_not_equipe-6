﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t461;
// UnityEngine.AssetBundle
struct AssetBundle_t464;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C" void AssetBundleCreateRequest__ctor_m2332 (AssetBundleCreateRequest_t461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C" AssetBundle_t464 * AssetBundleCreateRequest_get_assetBundle_m2333 (AssetBundleCreateRequest_t461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m2334 (AssetBundleCreateRequest_t461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
