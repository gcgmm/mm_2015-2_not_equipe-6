﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t15;
// UnityEngine.UI.Text
struct Text_t16;
// UnityEngine.GameObject
struct GameObject_t9;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// GameController
struct  GameController_t6  : public MonoBehaviour_t2
{
	// UnityEngine.UI.Image GameController::initMenu
	Image_t15 * ___initMenu_5;
	// UnityEngine.UI.Image GameController::WinMenu
	Image_t15 * ___WinMenu_6;
	// UnityEngine.UI.Image GameController::LooseMenu
	Image_t15 * ___LooseMenu_7;
	// UnityEngine.UI.Text GameController::rowCount
	Text_t16 * ___rowCount_8;
	// UnityEngine.GameObject GameController::board4
	GameObject_t9 * ___board4_9;
	// UnityEngine.GameObject GameController::board6
	GameObject_t9 * ___board6_10;
	// System.Int32 GameController::difficulty
	int32_t ___difficulty_11;
	// UnityEngine.GameObject GameController::activeBoard
	GameObject_t9 * ___activeBoard_12;
};
