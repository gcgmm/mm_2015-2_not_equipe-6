﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// BoardController
struct BoardController_t1;
// DockController
struct DockController_t7;
// RowController
struct RowController_t10;
// System.Object
struct Object_t;
// UnityEngine.Renderer
struct Renderer_t85;
// UnityEngine.Material
struct Material_t8;
// DragController
struct DragController_t11;
// UnityEngine.Collider
struct Collider_t14;
// GameController
struct GameController_t6;
// UnityEngine.Material[]
struct MaterialU5BU5D_t4;
// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t18;
// Vuforia.CloudRecoBehaviour
struct CloudRecoBehaviour_t20;
// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_t22;
// Vuforia.DatabaseLoadBehaviour
struct DatabaseLoadBehaviour_t24;
// Vuforia.DefaultInitializationErrorHandler
struct DefaultInitializationErrorHandler_t26;
// Vuforia.DefaultSmartTerrainEventHandler
struct DefaultSmartTerrainEventHandler_t27;
// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t28;
// Vuforia.Prop
struct Prop_t81;
// Vuforia.Surface
struct Surface_t82;
// Vuforia.DefaultTrackableEventHandler
struct DefaultTrackableEventHandler_t31;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t32;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t99;
// System.Object[]
struct ObjectU5BU5D_t105;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t100;
// Vuforia.GLErrorHandler
struct GLErrorHandler_t33;
// System.String
struct String_t;
// Vuforia.HideExcessAreaBehaviour
struct HideExcessAreaBehaviour_t34;
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t36;
// Vuforia.AndroidUnityPlayer
struct AndroidUnityPlayer_t38;
// Vuforia.ComponentFactoryStarterBehaviour
struct ComponentFactoryStarterBehaviour_t39;
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
struct List_1_t102;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
struct IEnumerable_1_t127;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t128;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t129;
// Vuforia.IOSUnityPlayer
struct IOSUnityPlayer_t40;
// Vuforia.VuforiaBehaviourComponentFactory
struct VuforiaBehaviourComponentFactory_t41;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t47;
// UnityEngine.GameObject
struct GameObject_t9;
// Vuforia.MaskOutBehaviour
struct MaskOutBehaviour_t46;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t71;
// Vuforia.VirtualButtonBehaviour
struct VirtualButtonBehaviour_t70;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t62;
// Vuforia.TurnOffBehaviour
struct TurnOffBehaviour_t61;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t37;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t45;
// Vuforia.MarkerBehaviour
struct MarkerBehaviour_t44;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t49;
// Vuforia.MultiTargetBehaviour
struct MultiTargetBehaviour_t48;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t23;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t80;
// Vuforia.WordBehaviour
struct WordBehaviour_t79;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t60;
// Vuforia.TextRecoBehaviour
struct TextRecoBehaviour_t59;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t51;
// Vuforia.ObjectTargetBehaviour
struct ObjectTargetBehaviour_t50;
// Vuforia.KeepAliveBehaviour
struct KeepAliveBehaviour_t42;
// Vuforia.PropBehaviour
struct PropBehaviour_t29;
// Vuforia.ReconstructionFromTargetBehaviour
struct ReconstructionFromTargetBehaviour_t54;
// Vuforia.SmartTerrainTrackerBehaviour
struct SmartTerrainTrackerBehaviour_t56;
// Vuforia.SurfaceBehaviour
struct SurfaceBehaviour_t30;
// UnityEngine.MeshRenderer
struct MeshRenderer_t113;
// UnityEngine.MeshFilter
struct MeshFilter_t114;
// Vuforia.TurnOffWordBehaviour
struct TurnOffWordBehaviour_t63;
// Vuforia.UserDefinedTargetBuildingBehaviour
struct UserDefinedTargetBuildingBehaviour_t64;
// Vuforia.VideoBackgroundBehaviour
struct VideoBackgroundBehaviour_t66;
// Vuforia.VideoTextureRenderer
struct VideoTextureRenderer_t68;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t72;
// Vuforia.WebCamBehaviour
struct WebCamBehaviour_t74;
// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t76;
// UnityEngine.Camera[]
struct CameraU5BU5D_t119;
// Vuforia.WireframeTrackableEventHandler
struct WireframeTrackableEventHandler_t78;
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t126;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"
#include "AssemblyU2DCSharp_BoardController.h"
#include "AssemblyU2DCSharp_BoardControllerMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
#include "AssemblyU2DCSharp_RowControllerMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
#include "mscorlib_System_Int32.h"
#include "AssemblyU2DCSharp_RowController.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Material.h"
#include "mscorlib_System_Boolean.h"
#include "AssemblyU2DCSharp_GameControllerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
#include "AssemblyU2DCSharp_GameController.h"
#include "UnityEngine_UnityEngine_Animator.h"
#include "mscorlib_System_String.h"
#include "AssemblyU2DCSharp_DockController.h"
#include "AssemblyU2DCSharp_DockControllerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "AssemblyU2DCSharp_DragController.h"
#include "AssemblyU2DCSharp_DragControllerMethodDeclarations.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_LayerMask.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Collider.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"
#include "UnityEngine_UnityEngine_Bounds.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image.h"
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbstMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbst.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBeMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBe.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstrMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr.h"
#include "AssemblyU2DCSharp_Vuforia_DatabaseLoadBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_DatabaseLoadBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DatabaseLoadAbstracMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DatabaseLoadAbstrac.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandler.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandlerMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_Action_1_genMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBehaMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeha.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"
#include "mscorlib_System_Action_1_gen.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandler.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandlerMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_0MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstrMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour.h"
#include "mscorlib_System_Action_1_gen_0.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstr.h"
#include "mscorlib_System_Action_1_gen_1.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavio.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeha.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandler.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandlerMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandlerMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstrMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstr.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstractMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstract.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayerMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilitiesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnityMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviourMethodDeclarations.h"
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_genMethodDeclarations.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "System_Core_System_ActionMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_0.h"
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"
#include "mscorlib_System_Attribute.h"
#include "System_Core_System_Action.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "System_Core_System_Linq_Enumerable.h"
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetter.h"
#include "mscorlib_System_Delegate.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactoryMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentFMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactory.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayerMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeha.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehav.h"
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstract.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstrac.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBeMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBe.h"
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehavMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBehaMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtiliMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstractMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstracMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavioMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviourMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromTMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT.h"
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackerMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBehaMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBehMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBehaMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshRenderer.h"
#include "UnityEngine_UnityEngine_MeshFilter.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviourMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBuMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBu.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbstMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst.h"
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer.h"
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRendererMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendereMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendere.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstraMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayerMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayerMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayer.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer.h"
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehavMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehav.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HideFlags.h"
#include "UnityEngine_UnityEngine_Shader.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManager.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandler.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandlerMethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviourMethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavioMethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m303_gshared (GameObject_t9 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m303(__this, method) (( Object_t * (*) (GameObject_t9 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m303_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<RowController>()
#define GameObject_GetComponent_TisRowController_t10_m149(__this, method) (( RowController_t10 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m303_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t85_m152(__this, method) (( Renderer_t85 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m303_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m304_gshared (Component_t130 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m304(__this, method) (( Object_t * (*) (Component_t130 *, const MethodInfo*))Component_GetComponent_TisObject_t_m304_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t85_m157(__this, method) (( Renderer_t85 * (*) (Component_t130 *, const MethodInfo*))Component_GetComponent_TisObject_t_m304_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<DockController>()
#define GameObject_GetComponent_TisDockController_t7_m158(__this, method) (( DockController_t7 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m303_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<BoardController>()
#define GameObject_GetComponent_TisBoardController_t1_m171(__this, method) (( BoardController_t1 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m303_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionBehaviour>()
#define Component_GetComponent_TisReconstructionBehaviour_t28_m196(__this, method) (( ReconstructionBehaviour_t28 * (*) (Component_t130 *, const MethodInfo*))Component_GetComponent_TisObject_t_m304_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
#define Component_GetComponent_TisTrackableBehaviour_t32_m205(__this, method) (( TrackableBehaviour_t32 * (*) (Component_t130 *, const MethodInfo*))Component_GetComponent_TisObject_t_m304_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C" ObjectU5BU5D_t105* Component_GetComponentsInChildren_TisObject_t_m305_gshared (Component_t130 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m305(__this, p0, method) (( ObjectU5BU5D_t105* (*) (Component_t130 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m305_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
#define Component_GetComponentsInChildren_TisRenderer_t85_m207(__this, p0, method) (( RendererU5BU5D_t99* (*) (Component_t130 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m305_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
#define Component_GetComponentsInChildren_TisCollider_t14_m208(__this, p0, method) (( ColliderU5BU5D_t100* (*) (Component_t130 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m305_gshared)(__this, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" List_1_t128 * Enumerable_ToList_TisObject_t_m306_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_ToList_TisObject_t_m306(__this /* static, unused */, p0, method) (( List_1_t128 * (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToList_TisObject_t_m306_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisMethodInfo_t_m224(__this /* static, unused */, p0, method) (( List_1_t102 * (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToList_TisObject_t_m306_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m307_gshared (GameObject_t9 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m307(__this, method) (( Object_t * (*) (GameObject_t9 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m307_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MaskOutBehaviour>()
#define GameObject_AddComponent_TisMaskOutBehaviour_t46_m232(__this, method) (( MaskOutBehaviour_t46 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m307_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.VirtualButtonBehaviour>()
#define GameObject_AddComponent_TisVirtualButtonBehaviour_t70_m233(__this, method) (( VirtualButtonBehaviour_t70 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m307_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TurnOffBehaviour>()
#define GameObject_AddComponent_TisTurnOffBehaviour_t61_m234(__this, method) (( TurnOffBehaviour_t61 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m307_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ImageTargetBehaviour>()
#define GameObject_AddComponent_TisImageTargetBehaviour_t36_m235(__this, method) (( ImageTargetBehaviour_t36 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m307_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MarkerBehaviour>()
#define GameObject_AddComponent_TisMarkerBehaviour_t44_m236(__this, method) (( MarkerBehaviour_t44 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m307_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MultiTargetBehaviour>()
#define GameObject_AddComponent_TisMultiTargetBehaviour_t48_m237(__this, method) (( MultiTargetBehaviour_t48 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m307_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.CylinderTargetBehaviour>()
#define GameObject_AddComponent_TisCylinderTargetBehaviour_t22_m238(__this, method) (( CylinderTargetBehaviour_t22 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m307_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.WordBehaviour>()
#define GameObject_AddComponent_TisWordBehaviour_t79_m239(__this, method) (( WordBehaviour_t79 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m307_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TextRecoBehaviour>()
#define GameObject_AddComponent_TisTextRecoBehaviour_t59_m240(__this, method) (( TextRecoBehaviour_t59 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m307_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ObjectTargetBehaviour>()
#define GameObject_AddComponent_TisObjectTargetBehaviour_t50_m241(__this, method) (( ObjectTargetBehaviour_t50 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m307_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t113_m257(__this, method) (( MeshRenderer_t113 * (*) (Component_t130 *, const MethodInfo*))Component_GetComponent_TisObject_t_m304_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t114_m259(__this, method) (( MeshFilter_t114 * (*) (Component_t130 *, const MethodInfo*))Component_GetComponent_TisObject_t_m304_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ComponentFactoryStarterBehaviour>()
#define GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t39_m270(__this, method) (( ComponentFactoryStarterBehaviour_t39 * (*) (GameObject_t9 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m307_gshared)(__this, method)
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C" Object_t * Object_FindObjectOfType_TisObject_t_m308_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisObject_t_m308(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m308_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<Vuforia.VuforiaBehaviour>()
#define Object_FindObjectOfType_TisVuforiaBehaviour_t72_m271(__this /* static, unused */, method) (( VuforiaBehaviour_t72 * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m308_gshared)(__this /* static, unused */, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C" ObjectU5BU5D_t105* GameObject_GetComponentsInChildren_TisObject_t_m309_gshared (GameObject_t9 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisObject_t_m309(__this, method) (( ObjectU5BU5D_t105* (*) (GameObject_t9 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m309_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
#define GameObject_GetComponentsInChildren_TisCamera_t91_m279(__this, method) (( CameraU5BU5D_t119* (*) (GameObject_t9 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m309_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
#define Component_GetComponentsInChildren_TisWireframeBehaviour_t76_m300(__this, p0, method) (( WireframeBehaviourU5BU5D_t126* (*) (Component_t130 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m305_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BoardController::.ctor()
extern "C" void BoardController__ctor_m0 (BoardController_t1 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m141(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BoardController::Awake()
extern "C" void BoardController_Awake_m1 (BoardController_t1 * __this, const MethodInfo* method)
{
	{
		BoardController_StartNewGame_m2(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BoardController::StartNewGame()
extern TypeInfo* MaterialU5BU5D_t4_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t83_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m142_MethodInfo_var;
extern "C" void BoardController_StartNewGame_m2 (BoardController_t1 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MaterialU5BU5D_t4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		List_1_t83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		List_1__ctor_m142_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t83 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	RowController_t10 * V_3 = {0};
	RowControllerU5BU5D_t3* V_4 = {0};
	int32_t V_5 = 0;
	{
		int32_t L_0 = (__this->___codeLength_5);
		__this->___code_8 = ((MaterialU5BU5D_t4*)SZArrayNew(MaterialU5BU5D_t4_il2cpp_TypeInfo_var, L_0));
		MaterialU5BU5D_t4* L_1 = (__this->___materials_3);
		List_1_t83 * L_2 = (List_1_t83 *)il2cpp_codegen_object_new (List_1_t83_il2cpp_TypeInfo_var);
		List_1__ctor_m142(L_2, (Object_t*)(Object_t*)L_1, /*hidden argument*/List_1__ctor_m142_MethodInfo_var);
		V_0 = L_2;
		V_1 = 0;
		goto IL_004d;
	}

IL_0024:
	{
		List_1_t83 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Material>::get_Count() */, L_3);
		int32_t L_5 = Random_Range_m143(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/NULL);
		V_2 = L_5;
		MaterialU5BU5D_t4* L_6 = (__this->___code_8);
		int32_t L_7 = V_1;
		List_1_t83 * L_8 = V_0;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		Material_t8 * L_10 = (Material_t8 *)VirtFuncInvoker1< Material_t8 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Material>::get_Item(System.Int32) */, L_8, L_9);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		ArrayElementTypeCheck (L_6, L_10);
		*((Material_t8 **)(Material_t8 **)SZArrayLdElema(L_6, L_7, sizeof(Material_t8 *))) = (Material_t8 *)L_10;
		List_1_t83 * L_11 = V_0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<UnityEngine.Material>::RemoveAt(System.Int32) */, L_11, L_12);
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_14 = V_1;
		int32_t L_15 = (__this->___codeLength_5);
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0024;
		}
	}
	{
		RowControllerU5BU5D_t3* L_16 = (__this->___rows_2);
		V_4 = L_16;
		V_5 = 0;
		goto IL_007c;
	}

IL_0069:
	{
		RowControllerU5BU5D_t3* L_17 = V_4;
		int32_t L_18 = V_5;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		V_3 = (*(RowController_t10 **)(RowController_t10 **)SZArrayLdElema(L_17, L_19, sizeof(RowController_t10 *)));
		RowController_t10 * L_20 = V_3;
		NullCheck(L_20);
		RowController_SetActive_m26(L_20, 0, /*hidden argument*/NULL);
		int32_t L_21 = V_5;
		V_5 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_007c:
	{
		int32_t L_22 = V_5;
		RowControllerU5BU5D_t3* L_23 = V_4;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_23)->max_length)))))))
		{
			goto IL_0069;
		}
	}
	{
		__this->___rowIndex_7 = 0;
		RowControllerU5BU5D_t3* L_24 = (__this->___rows_2);
		int32_t L_25 = (__this->___rowIndex_7);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		NullCheck((*(RowController_t10 **)(RowController_t10 **)SZArrayLdElema(L_24, L_26, sizeof(RowController_t10 *))));
		RowController_SetActive_m26((*(RowController_t10 **)(RowController_t10 **)SZArrayLdElema(L_24, L_26, sizeof(RowController_t10 *))), 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BoardController::Check(System.Int32)
extern Il2CppCodeGenString* _stringLiteral0;
extern "C" void BoardController_Check_m3 (BoardController_t1 * __this, int32_t ___difficulty, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral0 = il2cpp_codegen_string_literal_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		RowControllerU5BU5D_t3* L_0 = (__this->___rows_2);
		int32_t L_1 = (__this->___rowIndex_7);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(RowController_t10 **)(RowController_t10 **)SZArrayLdElema(L_0, L_2, sizeof(RowController_t10 *))));
		bool L_3 = RowController_IsComplete_m29((*(RowController_t10 **)(RowController_t10 **)SZArrayLdElema(L_0, L_2, sizeof(RowController_t10 *))), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_4 = ___difficulty;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_003c;
		}
	}
	{
		RowControllerU5BU5D_t3* L_5 = (__this->___rows_2);
		int32_t L_6 = (__this->___rowIndex_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		MaterialU5BU5D_t4* L_8 = (__this->___code_8);
		NullCheck((*(RowController_t10 **)(RowController_t10 **)SZArrayLdElema(L_5, L_7, sizeof(RowController_t10 *))));
		bool L_9 = RowController_CheckCodeEasy_m31((*(RowController_t10 **)(RowController_t10 **)SZArrayLdElema(L_5, L_7, sizeof(RowController_t10 *))), L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0055;
	}

IL_003c:
	{
		RowControllerU5BU5D_t3* L_10 = (__this->___rows_2);
		int32_t L_11 = (__this->___rowIndex_7);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		MaterialU5BU5D_t4* L_13 = (__this->___code_8);
		NullCheck((*(RowController_t10 **)(RowController_t10 **)SZArrayLdElema(L_10, L_12, sizeof(RowController_t10 *))));
		bool L_14 = RowController_CheckCode_m30((*(RowController_t10 **)(RowController_t10 **)SZArrayLdElema(L_10, L_12, sizeof(RowController_t10 *))), L_13, /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_0055:
	{
		bool L_15 = V_0;
		if (!L_15)
		{
			goto IL_006b;
		}
	}
	{
		GameController_t6 * L_16 = (__this->___game_6);
		NullCheck(L_16);
		GameController_OnGameWin_m20(L_16, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_006b:
	{
		RowControllerU5BU5D_t3* L_17 = (__this->___rows_2);
		int32_t L_18 = (__this->___rowIndex_7);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		NullCheck((*(RowController_t10 **)(RowController_t10 **)SZArrayLdElema(L_17, L_19, sizeof(RowController_t10 *))));
		RowController_SetActive_m26((*(RowController_t10 **)(RowController_t10 **)SZArrayLdElema(L_17, L_19, sizeof(RowController_t10 *))), 0, /*hidden argument*/NULL);
		int32_t L_20 = (__this->___rowIndex_7);
		__this->___rowIndex_7 = ((int32_t)((int32_t)L_20+(int32_t)1));
		int32_t L_21 = (__this->___rowIndex_7);
		if ((((int32_t)L_21) < ((int32_t)((int32_t)10))))
		{
			goto IL_00a9;
		}
	}
	{
		GameController_t6 * L_22 = (__this->___game_6);
		NullCheck(L_22);
		GameController_onGameLoose_m21(L_22, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_00a9:
	{
		RowControllerU5BU5D_t3* L_23 = (__this->___rows_2);
		int32_t L_24 = (__this->___rowIndex_7);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		NullCheck((*(RowController_t10 **)(RowController_t10 **)SZArrayLdElema(L_23, L_25, sizeof(RowController_t10 *))));
		RowController_SetActive_m26((*(RowController_t10 **)(RowController_t10 **)SZArrayLdElema(L_23, L_25, sizeof(RowController_t10 *))), 1, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		goto IL_00d1;
	}

IL_00c1:
	{
		Animator_t5 * L_26 = (__this->___anim_4);
		NullCheck(L_26);
		Animator_SetTrigger_m144(L_26, _stringLiteral0, /*hidden argument*/NULL);
	}

IL_00d1:
	{
		return;
	}
}
// System.Int32 BoardController::GetIndex()
extern "C" int32_t BoardController_GetIndex_m4 (BoardController_t1 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___rowIndex_7);
		return L_0;
	}
}
// System.Void DockController::.ctor()
extern "C" void DockController__ctor_m5 (DockController_t7 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m141(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DockController::Awake()
extern const MethodInfo* GameObject_GetComponent_TisRowController_t10_m149_MethodInfo_var;
extern "C" void DockController_Awake_m6 (DockController_t7 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisRowController_t10_m149_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t84 * L_0 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t84 * L_1 = Transform_GetChild_m146(L_0, 0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_t9 * L_2 = Component_get_gameObject_m147(L_1, /*hidden argument*/NULL);
		__this->___dockHolder_5 = L_2;
		Transform_t84 * L_3 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t84 * L_4 = Transform_get_parent_m148(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t9 * L_5 = Component_get_gameObject_m147(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		RowController_t10 * L_6 = GameObject_GetComponent_TisRowController_t10_m149(L_5, /*hidden argument*/GameObject_GetComponent_TisRowController_t10_m149_MethodInfo_var);
		__this->___row_6 = L_6;
		return;
	}
}
// System.Void DockController::Update()
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t85_m152_MethodInfo_var;
extern "C" void DockController_Update_m7 (DockController_t7 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisRenderer_t85_m152_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		s_Il2CppMethodIntialized = true;
	}
	Renderer_t85 * V_0 = {0};
	{
		RowController_t10 * L_0 = (__this->___row_6);
		NullCheck(L_0);
		bool L_1 = RowController_IsActive_m27(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0010;
		}
	}

IL_0010:
	{
		Material_t8 * L_2 = (__this->___dockedColor_4);
		bool L_3 = Object_op_Inequality_m150(NULL /*static, unused*/, L_2, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		bool L_4 = (__this->___isOver_3);
		if (L_4)
		{
			goto IL_0055;
		}
	}
	{
		GameObject_t9 * L_5 = (__this->___dockHolder_5);
		NullCheck(L_5);
		GameObject_SetActive_m151(L_5, 1, /*hidden argument*/NULL);
		GameObject_t9 * L_6 = (__this->___dockHolder_5);
		NullCheck(L_6);
		Renderer_t85 * L_7 = GameObject_GetComponent_TisRenderer_t85_m152(L_6, /*hidden argument*/GameObject_GetComponent_TisRenderer_t85_m152_MethodInfo_var);
		V_0 = L_7;
		Renderer_t85 * L_8 = V_0;
		Material_t8 * L_9 = (__this->___dockedColor_4);
		NullCheck(L_8);
		Renderer_set_sharedMaterial_m153(L_8, L_9, /*hidden argument*/NULL);
		goto IL_0061;
	}

IL_0055:
	{
		GameObject_t9 * L_10 = (__this->___dockHolder_5);
		NullCheck(L_10);
		GameObject_SetActive_m151(L_10, 0, /*hidden argument*/NULL);
	}

IL_0061:
	{
		return;
	}
}
// System.Void DockController::setDockedColor(UnityEngine.Material)
extern "C" void DockController_setDockedColor_m8 (DockController_t7 * __this, Material_t8 * ___color, const MethodInfo* method)
{
	{
		RowController_t10 * L_0 = (__this->___row_6);
		NullCheck(L_0);
		bool L_1 = RowController_IsActive_m27(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		Material_t8 * L_2 = ___color;
		__this->___dockedColor_4 = L_2;
		RowController_t10 * L_3 = (__this->___row_6);
		Material_t8 * L_4 = ___color;
		int32_t L_5 = (__this->___DockIndex_2);
		NullCheck(L_3);
		RowController_SetColor_m28(L_3, L_4, L_5, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void DragController::.ctor()
extern "C" void DragController__ctor_m9 (DragController_t11 * __this, const MethodInfo* method)
{
	{
		__this->___camRayLength_3 = (100.0f);
		MonoBehaviour__ctor_m141(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DragController::Awake()
extern TypeInfo* StringU5BU5D_t87_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t85_m157_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1;
extern "C" void DragController_Awake_m10 (DragController_t11 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t87_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		Component_GetComponent_TisRenderer_t85_m157_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t87* L_0 = ((StringU5BU5D_t87*)SZArrayNew(StringU5BU5D_t87_il2cpp_TypeInfo_var, 1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral1);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral1;
		int32_t L_1 = LayerMask_GetMask_m154(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		LayerMask_t13  L_2 = LayerMask_op_Implicit_m155(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->___dragLayerMask_5 = L_2;
		Transform_t84 * L_3 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t12  L_4 = Transform_get_position_m156(L_3, /*hidden argument*/NULL);
		__this->___originalPosition_4 = L_4;
		Renderer_t85 * L_5 = Component_GetComponent_TisRenderer_t85_m157(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t85_m157_MethodInfo_var);
		Material_t8 * L_6 = (__this->___color_2);
		NullCheck(L_5);
		Renderer_set_sharedMaterial_m153(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DragController::OnMouseUp()
extern const MethodInfo* GameObject_GetComponent_TisDockController_t7_m158_MethodInfo_var;
extern "C" void DragController_OnMouseUp_m11 (DragController_t11 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisDockController_t7_m158_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t14 * L_0 = (__this->___dock_6);
		bool L_1 = Object_op_Inequality_m150(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		Collider_t14 * L_2 = (__this->___dock_6);
		NullCheck(L_2);
		GameObject_t9 * L_3 = Component_get_gameObject_m147(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		DockController_t7 * L_4 = GameObject_GetComponent_TisDockController_t7_m158(L_3, /*hidden argument*/GameObject_GetComponent_TisDockController_t7_m158_MethodInfo_var);
		Material_t8 * L_5 = (__this->___color_2);
		NullCheck(L_4);
		DockController_setDockedColor_m8(L_4, L_5, /*hidden argument*/NULL);
		__this->___dock_6 = (Collider_t14 *)NULL;
	}

IL_0033:
	{
		Transform_t84 * L_6 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		Vector3_t12  L_7 = (__this->___originalPosition_4);
		NullCheck(L_6);
		Transform_set_position_m159(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DragController::OnMouseDrag()
extern TypeInfo* Input_t92_il2cpp_TypeInfo_var;
extern "C" void DragController_OnMouseDrag_m12 (DragController_t11 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t92_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t88  V_0 = {0};
	RaycastHit_t89  V_1 = {0};
	Vector3_t12  V_2 = {0};
	Bounds_t90  V_3 = {0};
	Vector3_t12  V_4 = {0};
	Vector3_t12  V_5 = {0};
	Vector3_t12  V_6 = {0};
	Vector3_t12  V_7 = {0};
	{
		Camera_t91 * L_0 = Camera_get_main_m160(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t92_il2cpp_TypeInfo_var);
		Vector3_t12  L_1 = Input_get_mousePosition_m161(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Ray_t88  L_2 = Camera_ScreenPointToRay_m162(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Ray_t88  L_3 = V_0;
		float L_4 = (__this->___camRayLength_3);
		LayerMask_t13  L_5 = (__this->___dragLayerMask_5);
		int32_t L_6 = LayerMask_op_Implicit_m163(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		bool L_7 = Physics_Raycast_m164(NULL /*static, unused*/, L_3, (&V_1), L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00df;
		}
	}
	{
		Collider_t14 * L_8 = (__this->___dock_6);
		bool L_9 = Object_op_Equality_m165(NULL /*static, unused*/, L_8, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_005e;
		}
	}
	{
		Collider_t14 * L_10 = (__this->___dock_6);
		NullCheck(L_10);
		Bounds_t90  L_11 = Collider_get_bounds_m166(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		Vector3_t12  L_12 = RaycastHit_get_point_m167((&V_1), /*hidden argument*/NULL);
		bool L_13 = Bounds_Contains_m168((&V_3), L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_00a7;
		}
	}

IL_005e:
	{
		Transform_t84 * L_14 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		Vector3_t12  L_15 = RaycastHit_get_point_m167((&V_1), /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = ((&V_4)->___x_1);
		Transform_t84 * L_17 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t12  L_18 = Transform_get_position_m156(L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		float L_19 = ((&V_5)->___y_2);
		Vector3_t12  L_20 = RaycastHit_get_point_m167((&V_1), /*hidden argument*/NULL);
		V_6 = L_20;
		float L_21 = ((&V_6)->___z_3);
		Vector3_t12  L_22 = {0};
		Vector3__ctor_m169(&L_22, L_16, L_19, L_21, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_position_m159(L_14, L_22, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_00a7:
	{
		Collider_t14 * L_23 = (__this->___dock_6);
		NullCheck(L_23);
		Transform_t84 * L_24 = Component_get_transform_m145(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t12  L_25 = Transform_get_position_m156(L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		Transform_t84 * L_26 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		Vector3_t12  L_27 = Transform_get_position_m156(L_26, /*hidden argument*/NULL);
		V_7 = L_27;
		float L_28 = ((&V_7)->___y_2);
		(&V_2)->___y_2 = L_28;
		Transform_t84 * L_29 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		Vector3_t12  L_30 = V_2;
		NullCheck(L_29);
		Transform_set_position_m159(L_29, L_30, /*hidden argument*/NULL);
	}

IL_00df:
	{
		return;
	}
}
// System.Void DragController::OnTriggerEnter(UnityEngine.Collider)
extern const MethodInfo* GameObject_GetComponent_TisDockController_t7_m158_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2;
extern "C" void DragController_OnTriggerEnter_m13 (DragController_t11 * __this, Collider_t14 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisDockController_t7_m158_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t14 * L_0 = ___other;
		NullCheck(L_0);
		GameObject_t9 * L_1 = Component_get_gameObject_m147(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = GameObject_CompareTag_m170(L_1, _stringLiteral2, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		Collider_t14 * L_3 = ___other;
		__this->___dock_6 = L_3;
		Collider_t14 * L_4 = (__this->___dock_6);
		NullCheck(L_4);
		GameObject_t9 * L_5 = Component_get_gameObject_m147(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		DockController_t7 * L_6 = GameObject_GetComponent_TisDockController_t7_m158(L_5, /*hidden argument*/GameObject_GetComponent_TisDockController_t7_m158_MethodInfo_var);
		NullCheck(L_6);
		L_6->___isOver_3 = 1;
	}

IL_0032:
	{
		return;
	}
}
// System.Void DragController::OnTriggerExit(UnityEngine.Collider)
extern const MethodInfo* GameObject_GetComponent_TisDockController_t7_m158_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2;
extern "C" void DragController_OnTriggerExit_m14 (DragController_t11 * __this, Collider_t14 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisDockController_t7_m158_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t14 * L_0 = ___other;
		NullCheck(L_0);
		GameObject_t9 * L_1 = Component_get_gameObject_m147(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = GameObject_CompareTag_m170(L_1, _stringLiteral2, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		Collider_t14 * L_3 = ___other;
		NullCheck(L_3);
		GameObject_t9 * L_4 = Component_get_gameObject_m147(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		DockController_t7 * L_5 = GameObject_GetComponent_TisDockController_t7_m158(L_4, /*hidden argument*/GameObject_GetComponent_TisDockController_t7_m158_MethodInfo_var);
		NullCheck(L_5);
		L_5->___isOver_3 = 0;
	}

IL_0026:
	{
		return;
	}
}
// System.Void GameController::.ctor()
extern "C" void GameController__ctor_m15 (GameController_t6 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m141(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::startEasy()
extern "C" void GameController_startEasy_m16 (GameController_t6 * __this, const MethodInfo* method)
{
	{
		GameController_hideInitMenu_m23(__this, /*hidden argument*/NULL);
		GameObject_t9 * L_0 = (__this->___board4_9);
		NullCheck(L_0);
		GameObject_SetActive_m151(L_0, 1, /*hidden argument*/NULL);
		GameObject_t9 * L_1 = (__this->___board4_9);
		__this->___activeBoard_12 = L_1;
		__this->___difficulty_11 = 1;
		return;
	}
}
// System.Void GameController::startMild()
extern "C" void GameController_startMild_m17 (GameController_t6 * __this, const MethodInfo* method)
{
	{
		GameController_hideInitMenu_m23(__this, /*hidden argument*/NULL);
		GameObject_t9 * L_0 = (__this->___board4_9);
		NullCheck(L_0);
		GameObject_SetActive_m151(L_0, 1, /*hidden argument*/NULL);
		GameObject_t9 * L_1 = (__this->___board4_9);
		__this->___activeBoard_12 = L_1;
		__this->___difficulty_11 = 2;
		return;
	}
}
// System.Void GameController::startHard()
extern "C" void GameController_startHard_m18 (GameController_t6 * __this, const MethodInfo* method)
{
	{
		GameController_hideInitMenu_m23(__this, /*hidden argument*/NULL);
		GameObject_t9 * L_0 = (__this->___board6_10);
		NullCheck(L_0);
		GameObject_SetActive_m151(L_0, 1, /*hidden argument*/NULL);
		GameObject_t9 * L_1 = (__this->___board6_10);
		__this->___activeBoard_12 = L_1;
		__this->___difficulty_11 = 3;
		return;
	}
}
// System.Void GameController::CheckCode()
extern const MethodInfo* GameObject_GetComponent_TisBoardController_t1_m171_MethodInfo_var;
extern "C" void GameController_CheckCode_m19 (GameController_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisBoardController_t1_m171_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t9 * L_0 = (__this->___activeBoard_12);
		NullCheck(L_0);
		BoardController_t1 * L_1 = GameObject_GetComponent_TisBoardController_t1_m171(L_0, /*hidden argument*/GameObject_GetComponent_TisBoardController_t1_m171_MethodInfo_var);
		int32_t L_2 = (__this->___difficulty_11);
		NullCheck(L_1);
		BoardController_Check_m3(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::OnGameWin()
extern const MethodInfo* GameObject_GetComponent_TisBoardController_t1_m171_MethodInfo_var;
extern "C" void GameController_OnGameWin_m20 (GameController_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisBoardController_t1_m171_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Image_t15 * L_0 = (__this->___WinMenu_6);
		NullCheck(L_0);
		GameObject_t9 * L_1 = Component_get_gameObject_m147(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m151(L_1, 1, /*hidden argument*/NULL);
		Text_t16 * L_2 = (__this->___rowCount_8);
		GameObject_t9 * L_3 = (__this->___activeBoard_12);
		NullCheck(L_3);
		BoardController_t1 * L_4 = GameObject_GetComponent_TisBoardController_t1_m171(L_3, /*hidden argument*/GameObject_GetComponent_TisBoardController_t1_m171_MethodInfo_var);
		NullCheck(L_4);
		int32_t L_5 = BoardController_GetIndex_m4(L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
		String_t* L_6 = Int32_ToString_m172((&V_0), /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_6);
		return;
	}
}
// System.Void GameController::onGameLoose()
extern "C" void GameController_onGameLoose_m21 (GameController_t6 * __this, const MethodInfo* method)
{
	{
		Image_t15 * L_0 = (__this->___LooseMenu_7);
		NullCheck(L_0);
		GameObject_t9 * L_1 = Component_get_gameObject_m147(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m151(L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Restart()
extern "C" void GameController_Restart_m22 (GameController_t6 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_loadedLevel_m173(NULL /*static, unused*/, /*hidden argument*/NULL);
		Application_LoadLevel_m174(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::hideInitMenu()
extern "C" void GameController_hideInitMenu_m23 (GameController_t6 * __this, const MethodInfo* method)
{
	{
		Image_t15 * L_0 = (__this->___initMenu_5);
		NullCheck(L_0);
		GameObject_t9 * L_1 = Component_get_gameObject_m147(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m151(L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RowController::.ctor()
extern "C" void RowController__ctor_m24 (RowController_t10 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m141(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RowController::Awake()
extern TypeInfo* MaterialU5BU5D_t4_il2cpp_TypeInfo_var;
extern TypeInfo* GameObjectU5BU5D_t17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral4;
extern Il2CppCodeGenString* _stringLiteral5;
extern Il2CppCodeGenString* _stringLiteral6;
extern Il2CppCodeGenString* _stringLiteral7;
extern Il2CppCodeGenString* _stringLiteral8;
extern "C" void RowController_Awake_m25 (RowController_t10 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MaterialU5BU5D_t4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		GameObjectU5BU5D_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		_stringLiteral5 = il2cpp_codegen_string_literal_from_index(5);
		_stringLiteral6 = il2cpp_codegen_string_literal_from_index(6);
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		_stringLiteral8 = il2cpp_codegen_string_literal_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___rowLength_4);
		__this->___rowCode_6 = ((MaterialU5BU5D_t4*)SZArrayNew(MaterialU5BU5D_t4_il2cpp_TypeInfo_var, L_0));
		int32_t L_1 = (__this->___rowLength_4);
		__this->___hints_7 = ((GameObjectU5BU5D_t17*)SZArrayNew(GameObjectU5BU5D_t17_il2cpp_TypeInfo_var, L_1));
		GameObjectU5BU5D_t17* L_2 = (__this->___hints_7);
		Transform_t84 * L_3 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t84 * L_4 = Transform_FindChild_m175(L_3, _stringLiteral3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t9 * L_5 = Component_get_gameObject_m147(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		*((GameObject_t9 **)(GameObject_t9 **)SZArrayLdElema(L_2, 0, sizeof(GameObject_t9 *))) = (GameObject_t9 *)L_5;
		GameObjectU5BU5D_t17* L_6 = (__this->___hints_7);
		Transform_t84 * L_7 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t84 * L_8 = Transform_FindChild_m175(L_7, _stringLiteral4, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_t9 * L_9 = Component_get_gameObject_m147(L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_9);
		*((GameObject_t9 **)(GameObject_t9 **)SZArrayLdElema(L_6, 1, sizeof(GameObject_t9 *))) = (GameObject_t9 *)L_9;
		GameObjectU5BU5D_t17* L_10 = (__this->___hints_7);
		Transform_t84 * L_11 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t84 * L_12 = Transform_FindChild_m175(L_11, _stringLiteral5, /*hidden argument*/NULL);
		NullCheck(L_12);
		GameObject_t9 * L_13 = Component_get_gameObject_m147(L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, L_13);
		*((GameObject_t9 **)(GameObject_t9 **)SZArrayLdElema(L_10, 2, sizeof(GameObject_t9 *))) = (GameObject_t9 *)L_13;
		GameObjectU5BU5D_t17* L_14 = (__this->___hints_7);
		Transform_t84 * L_15 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t84 * L_16 = Transform_FindChild_m175(L_15, _stringLiteral6, /*hidden argument*/NULL);
		NullCheck(L_16);
		GameObject_t9 * L_17 = Component_get_gameObject_m147(L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
		ArrayElementTypeCheck (L_14, L_17);
		*((GameObject_t9 **)(GameObject_t9 **)SZArrayLdElema(L_14, 3, sizeof(GameObject_t9 *))) = (GameObject_t9 *)L_17;
		int32_t L_18 = (__this->___rowLength_4);
		if ((((int32_t)L_18) <= ((int32_t)4)))
		{
			goto IL_00dc;
		}
	}
	{
		GameObjectU5BU5D_t17* L_19 = (__this->___hints_7);
		Transform_t84 * L_20 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t84 * L_21 = Transform_FindChild_m175(L_20, _stringLiteral7, /*hidden argument*/NULL);
		NullCheck(L_21);
		GameObject_t9 * L_22 = Component_get_gameObject_m147(L_21, /*hidden argument*/NULL);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 4);
		ArrayElementTypeCheck (L_19, L_22);
		*((GameObject_t9 **)(GameObject_t9 **)SZArrayLdElema(L_19, 4, sizeof(GameObject_t9 *))) = (GameObject_t9 *)L_22;
		GameObjectU5BU5D_t17* L_23 = (__this->___hints_7);
		Transform_t84 * L_24 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t84 * L_25 = Transform_FindChild_m175(L_24, _stringLiteral8, /*hidden argument*/NULL);
		NullCheck(L_25);
		GameObject_t9 * L_26 = Component_get_gameObject_m147(L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 5);
		ArrayElementTypeCheck (L_23, L_26);
		*((GameObject_t9 **)(GameObject_t9 **)SZArrayLdElema(L_23, 5, sizeof(GameObject_t9 *))) = (GameObject_t9 *)L_26;
	}

IL_00dc:
	{
		return;
	}
}
// System.Void RowController::SetActive(System.Boolean)
extern "C" void RowController_SetActive_m26 (RowController_t10 * __this, bool ___flag, const MethodInfo* method)
{
	{
		bool L_0 = ___flag;
		__this->___active_5 = L_0;
		return;
	}
}
// System.Boolean RowController::IsActive()
extern "C" bool RowController_IsActive_m27 (RowController_t10 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___active_5);
		return L_0;
	}
}
// System.Void RowController::SetColor(UnityEngine.Material,System.Int32)
extern "C" void RowController_SetColor_m28 (RowController_t10 * __this, Material_t8 * ___color, int32_t ___index, const MethodInfo* method)
{
	{
		MaterialU5BU5D_t4* L_0 = (__this->___rowCode_6);
		int32_t L_1 = ___index;
		Material_t8 * L_2 = ___color;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		ArrayElementTypeCheck (L_0, L_2);
		*((Material_t8 **)(Material_t8 **)SZArrayLdElema(L_0, L_1, sizeof(Material_t8 *))) = (Material_t8 *)L_2;
		return;
	}
}
// System.Boolean RowController::IsComplete()
extern "C" bool RowController_IsComplete_m29 (RowController_t10 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	Material_t8 * V_1 = {0};
	MaterialU5BU5D_t4* V_2 = {0};
	int32_t V_3 = 0;
	int32_t G_B4_0 = 0;
	{
		V_0 = 1;
		MaterialU5BU5D_t4* L_0 = (__this->___rowCode_6);
		V_2 = L_0;
		V_3 = 0;
		goto IL_0029;
	}

IL_0010:
	{
		MaterialU5BU5D_t4* L_1 = V_2;
		int32_t L_2 = V_3;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_1 = (*(Material_t8 **)(Material_t8 **)SZArrayLdElema(L_1, L_3, sizeof(Material_t8 *)));
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		Material_t8 * L_5 = V_1;
		bool L_6 = Object_op_Inequality_m150(NULL /*static, unused*/, L_5, (Object_t86 *)NULL, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_6));
		goto IL_0024;
	}

IL_0023:
	{
		G_B4_0 = 0;
	}

IL_0024:
	{
		V_0 = G_B4_0;
		int32_t L_7 = V_3;
		V_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_8 = V_3;
		MaterialU5BU5D_t4* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Boolean RowController::CheckCode(UnityEngine.Material[])
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t85_m152_MethodInfo_var;
extern "C" bool RowController_CheckCode_m30 (RowController_t10 * __this, MaterialU5BU5D_t4* ___code, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisRenderer_t85_m152_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	GameObject_t9 * V_2 = {0};
	{
		V_0 = 1;
		V_1 = 0;
		goto IL_0076;
	}

IL_0009:
	{
		GameObjectU5BU5D_t17* L_0 = (__this->___hints_7);
		int32_t L_1 = V_1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_2 = (*(GameObject_t9 **)(GameObject_t9 **)SZArrayLdElema(L_0, L_2, sizeof(GameObject_t9 *)));
		MaterialU5BU5D_t4* L_3 = ___code;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		MaterialU5BU5D_t4* L_6 = (__this->___rowCode_6);
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		bool L_9 = Object_op_Equality_m165(NULL /*static, unused*/, (*(Material_t8 **)(Material_t8 **)SZArrayLdElema(L_3, L_5, sizeof(Material_t8 *))), (*(Material_t8 **)(Material_t8 **)SZArrayLdElema(L_6, L_8, sizeof(Material_t8 *))), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0044;
		}
	}
	{
		GameObject_t9 * L_10 = V_2;
		NullCheck(L_10);
		GameObject_SetActive_m151(L_10, 1, /*hidden argument*/NULL);
		GameObject_t9 * L_11 = V_2;
		NullCheck(L_11);
		Renderer_t85 * L_12 = GameObject_GetComponent_TisRenderer_t85_m152(L_11, /*hidden argument*/GameObject_GetComponent_TisRenderer_t85_m152_MethodInfo_var);
		Material_t8 * L_13 = (__this->___BlackHintColor_2);
		NullCheck(L_12);
		Renderer_set_sharedMaterial_m153(L_12, L_13, /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_0044:
	{
		MaterialU5BU5D_t4* L_14 = (__this->___rowCode_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		MaterialU5BU5D_t4* L_17 = ___code;
		bool L_18 = RowController_contains_m32(__this, (*(Material_t8 **)(Material_t8 **)SZArrayLdElema(L_14, L_16, sizeof(Material_t8 *))), L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0070;
		}
	}
	{
		GameObject_t9 * L_19 = V_2;
		NullCheck(L_19);
		GameObject_SetActive_m151(L_19, 1, /*hidden argument*/NULL);
		GameObject_t9 * L_20 = V_2;
		NullCheck(L_20);
		Renderer_t85 * L_21 = GameObject_GetComponent_TisRenderer_t85_m152(L_20, /*hidden argument*/GameObject_GetComponent_TisRenderer_t85_m152_MethodInfo_var);
		Material_t8 * L_22 = (__this->___WhiteHintColor_3);
		NullCheck(L_21);
		Renderer_set_sharedMaterial_m153(L_21, L_22, /*hidden argument*/NULL);
	}

IL_0070:
	{
		V_0 = 0;
	}

IL_0072:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0076:
	{
		int32_t L_24 = V_1;
		int32_t L_25 = (__this->___rowLength_4);
		if ((((int32_t)L_24) < ((int32_t)L_25)))
		{
			goto IL_0009;
		}
	}
	{
		bool L_26 = V_0;
		return L_26;
	}
}
// System.Boolean RowController::CheckCodeEasy(UnityEngine.Material[])
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t85_m152_MethodInfo_var;
extern "C" bool RowController_CheckCodeEasy_m31 (RowController_t10 * __this, MaterialU5BU5D_t4* ___code, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisRenderer_t85_m152_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	GameObject_t9 * V_2 = {0};
	{
		V_0 = 1;
		V_1 = 0;
		goto IL_0049;
	}

IL_0009:
	{
		GameObjectU5BU5D_t17* L_0 = (__this->___hints_7);
		int32_t L_1 = V_1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_2 = (*(GameObject_t9 **)(GameObject_t9 **)SZArrayLdElema(L_0, L_2, sizeof(GameObject_t9 *)));
		MaterialU5BU5D_t4* L_3 = (__this->___rowCode_6);
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		MaterialU5BU5D_t4* L_6 = ___code;
		bool L_7 = RowController_contains_m32(__this, (*(Material_t8 **)(Material_t8 **)SZArrayLdElema(L_3, L_5, sizeof(Material_t8 *))), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0043;
		}
	}
	{
		GameObject_t9 * L_8 = V_2;
		NullCheck(L_8);
		GameObject_SetActive_m151(L_8, 1, /*hidden argument*/NULL);
		GameObject_t9 * L_9 = V_2;
		NullCheck(L_9);
		Renderer_t85 * L_10 = GameObject_GetComponent_TisRenderer_t85_m152(L_9, /*hidden argument*/GameObject_GetComponent_TisRenderer_t85_m152_MethodInfo_var);
		Material_t8 * L_11 = (__this->___BlackHintColor_2);
		NullCheck(L_10);
		Renderer_set_sharedMaterial_m153(L_10, L_11, /*hidden argument*/NULL);
		goto IL_0045;
	}

IL_0043:
	{
		V_0 = 0;
	}

IL_0045:
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_13 = V_1;
		int32_t L_14 = (__this->___rowLength_4);
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0009;
		}
	}
	{
		bool L_15 = V_0;
		return L_15;
	}
}
// System.Boolean RowController::contains(UnityEngine.Material,UnityEngine.Material[])
extern "C" bool RowController_contains_m32 (RowController_t10 * __this, Material_t8 * ___color, MaterialU5BU5D_t4* ___code, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t G_B4_0 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_0020;
	}

IL_0009:
	{
		bool L_0 = V_0;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		Material_t8 * L_1 = ___color;
		MaterialU5BU5D_t4* L_2 = ___code;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		bool L_5 = Object_op_Equality_m165(NULL /*static, unused*/, L_1, (*(Material_t8 **)(Material_t8 **)SZArrayLdElema(L_2, L_4, sizeof(Material_t8 *))), /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_5));
		goto IL_001b;
	}

IL_001a:
	{
		G_B4_0 = 1;
	}

IL_001b:
	{
		V_0 = G_B4_0;
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = (__this->___rowLength_4);
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0009;
		}
	}
	{
		bool L_9 = V_0;
		return L_9;
	}
}
// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
extern TypeInfo* BackgroundPlaneAbstractBehaviour_t19_il2cpp_TypeInfo_var;
extern "C" void BackgroundPlaneBehaviour__ctor_m33 (BackgroundPlaneBehaviour_t18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BackgroundPlaneAbstractBehaviour_t19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BackgroundPlaneAbstractBehaviour_t19_il2cpp_TypeInfo_var);
		BackgroundPlaneAbstractBehaviour__ctor_m176(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CloudRecoBehaviour::.ctor()
extern "C" void CloudRecoBehaviour__ctor_m34 (CloudRecoBehaviour_t20 * __this, const MethodInfo* method)
{
	{
		CloudRecoAbstractBehaviour__ctor_m177(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
extern "C" void CylinderTargetBehaviour__ctor_m35 (CylinderTargetBehaviour_t22 * __this, const MethodInfo* method)
{
	{
		CylinderTargetAbstractBehaviour__ctor_m178(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DatabaseLoadBehaviour::.ctor()
extern "C" void DatabaseLoadBehaviour__ctor_m36 (DatabaseLoadBehaviour_t24 * __this, const MethodInfo* method)
{
	{
		DatabaseLoadAbstractBehaviour__ctor_m179(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DatabaseLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C" void DatabaseLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m37 (DatabaseLoadBehaviour_t24 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DefaultInitializationErrorHandler__ctor_m38 (DefaultInitializationErrorHandler_t26 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___mErrorText_3 = L_0;
		MonoBehaviour__ctor_m141(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern const Il2CppType* VuforiaAbstractBehaviour_t73_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* VuforiaAbstractBehaviour_t73_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t93_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_OnVuforiaInitializationError_m45_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m183_MethodInfo_var;
extern "C" void DefaultInitializationErrorHandler_Awake_m39 (DefaultInitializationErrorHandler_t26 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaAbstractBehaviour_t73_0_0_0_var = il2cpp_codegen_type_from_index(14);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		VuforiaAbstractBehaviour_t73_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Action_1_t93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		DefaultInitializationErrorHandler_OnVuforiaInitializationError_m45_MethodInfo_var = il2cpp_codegen_method_info_from_index(7);
		Action_1__ctor_m183_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		s_Il2CppMethodIntialized = true;
	}
	VuforiaAbstractBehaviour_t73 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m180(NULL /*static, unused*/, LoadTypeToken(VuforiaAbstractBehaviour_t73_0_0_0_var), /*hidden argument*/NULL);
		Object_t86 * L_1 = Object_FindObjectOfType_m181(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((VuforiaAbstractBehaviour_t73 *)CastclassClass(L_1, VuforiaAbstractBehaviour_t73_il2cpp_TypeInfo_var));
		VuforiaAbstractBehaviour_t73 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m182(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		VuforiaAbstractBehaviour_t73 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)DefaultInitializationErrorHandler_OnVuforiaInitializationError_m45_MethodInfo_var };
		Action_1_t93 * L_6 = (Action_1_t93 *)il2cpp_codegen_object_new (Action_1_t93_il2cpp_TypeInfo_var);
		Action_1__ctor_m183(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m183_MethodInfo_var);
		NullCheck(L_4);
		VuforiaAbstractBehaviour_RegisterVuforiaInitErrorCallback_m184(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern TypeInfo* WindowFunction_t95_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t96_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_DrawWindowContent_m42_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral9;
extern "C" void DefaultInitializationErrorHandler_OnGUI_m40 (DefaultInitializationErrorHandler_t26 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WindowFunction_t95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		GUI_t96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		DefaultInitializationErrorHandler_DrawWindowContent_m42_MethodInfo_var = il2cpp_codegen_method_info_from_index(9);
		_stringLiteral9 = il2cpp_codegen_string_literal_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___mErrorOccurred_4);
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m185(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m186(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t94  L_3 = {0};
		Rect__ctor_m187(&L_3, (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		IntPtr_t L_4 = { (void*)DefaultInitializationErrorHandler_DrawWindowContent_m42_MethodInfo_var };
		WindowFunction_t95 * L_5 = (WindowFunction_t95 *)il2cpp_codegen_object_new (WindowFunction_t95_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m188(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t96_il2cpp_TypeInfo_var);
		GUI_Window_m189(NULL /*static, unused*/, 0, L_3, L_5, _stringLiteral9, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern const Il2CppType* VuforiaAbstractBehaviour_t73_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* VuforiaAbstractBehaviour_t73_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t93_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_OnVuforiaInitializationError_m45_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m183_MethodInfo_var;
extern "C" void DefaultInitializationErrorHandler_OnDestroy_m41 (DefaultInitializationErrorHandler_t26 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaAbstractBehaviour_t73_0_0_0_var = il2cpp_codegen_type_from_index(14);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		VuforiaAbstractBehaviour_t73_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Action_1_t93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		DefaultInitializationErrorHandler_OnVuforiaInitializationError_m45_MethodInfo_var = il2cpp_codegen_method_info_from_index(7);
		Action_1__ctor_m183_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		s_Il2CppMethodIntialized = true;
	}
	VuforiaAbstractBehaviour_t73 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m180(NULL /*static, unused*/, LoadTypeToken(VuforiaAbstractBehaviour_t73_0_0_0_var), /*hidden argument*/NULL);
		Object_t86 * L_1 = Object_FindObjectOfType_m181(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((VuforiaAbstractBehaviour_t73 *)CastclassClass(L_1, VuforiaAbstractBehaviour_t73_il2cpp_TypeInfo_var));
		VuforiaAbstractBehaviour_t73 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m182(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		VuforiaAbstractBehaviour_t73 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)DefaultInitializationErrorHandler_OnVuforiaInitializationError_m45_MethodInfo_var };
		Action_1_t93 * L_6 = (Action_1_t93 *)il2cpp_codegen_object_new (Action_1_t93_il2cpp_TypeInfo_var);
		Action_1__ctor_m183(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m183_MethodInfo_var);
		NullCheck(L_4);
		VuforiaAbstractBehaviour_UnregisterVuforiaInitErrorCallback_m190(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern TypeInfo* GUI_t96_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral10;
extern "C" void DefaultInitializationErrorHandler_DrawWindowContent_m42 (DefaultInitializationErrorHandler_t26 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral10 = il2cpp_codegen_string_literal_from_index(10);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m185(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m186(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t94  L_2 = {0};
		Rect__ctor_m187(&L_2, (10.0f), (25.0f), (((float)((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20)))))), (((float)((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95)))))), /*hidden argument*/NULL);
		String_t* L_3 = (__this->___mErrorText_3);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t96_il2cpp_TypeInfo_var);
		GUI_Label_m191(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m185(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m186(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t94  L_6 = {0};
		Rect__ctor_m187(&L_6, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75)))))), (((float)((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60)))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m192(NULL /*static, unused*/, L_6, _stringLiteral10, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0063;
		}
	}
	{
		Application_Quit_m193(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0063:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.VuforiaUnity/InitError)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral11;
extern Il2CppCodeGenString* _stringLiteral12;
extern Il2CppCodeGenString* _stringLiteral13;
extern Il2CppCodeGenString* _stringLiteral14;
extern Il2CppCodeGenString* _stringLiteral15;
extern Il2CppCodeGenString* _stringLiteral16;
extern Il2CppCodeGenString* _stringLiteral17;
extern Il2CppCodeGenString* _stringLiteral18;
extern Il2CppCodeGenString* _stringLiteral19;
extern Il2CppCodeGenString* _stringLiteral20;
extern Il2CppCodeGenString* _stringLiteral21;
extern "C" void DefaultInitializationErrorHandler_SetErrorCode_m43 (DefaultInitializationErrorHandler_t26 * __this, int32_t ___errorCode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		_stringLiteral12 = il2cpp_codegen_string_literal_from_index(12);
		_stringLiteral13 = il2cpp_codegen_string_literal_from_index(13);
		_stringLiteral14 = il2cpp_codegen_string_literal_from_index(14);
		_stringLiteral15 = il2cpp_codegen_string_literal_from_index(15);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		_stringLiteral17 = il2cpp_codegen_string_literal_from_index(17);
		_stringLiteral18 = il2cpp_codegen_string_literal_from_index(18);
		_stringLiteral19 = il2cpp_codegen_string_literal_from_index(19);
		_stringLiteral20 = il2cpp_codegen_string_literal_from_index(20);
		_stringLiteral21 = il2cpp_codegen_string_literal_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		String_t* L_0 = (__this->___mErrorText_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m194(NULL /*static, unused*/, _stringLiteral11, L_0, /*hidden argument*/NULL);
		Debug_LogError_m195(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___errorCode;
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 0)
		{
			goto IL_004d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 1)
		{
			goto IL_00ad;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 2)
		{
			goto IL_009d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 3)
		{
			goto IL_007d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 4)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 5)
		{
			goto IL_006d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 6)
		{
			goto IL_005d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 7)
		{
			goto IL_00bd;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 8)
		{
			goto IL_00cd;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 9)
		{
			goto IL_00dd;
		}
	}
	{
		goto IL_00ed;
	}

IL_004d:
	{
		__this->___mErrorText_3 = _stringLiteral12;
		goto IL_00ed;
	}

IL_005d:
	{
		__this->___mErrorText_3 = _stringLiteral13;
		goto IL_00ed;
	}

IL_006d:
	{
		__this->___mErrorText_3 = _stringLiteral14;
		goto IL_00ed;
	}

IL_007d:
	{
		__this->___mErrorText_3 = _stringLiteral15;
		goto IL_00ed;
	}

IL_008d:
	{
		__this->___mErrorText_3 = _stringLiteral16;
		goto IL_00ed;
	}

IL_009d:
	{
		__this->___mErrorText_3 = _stringLiteral17;
		goto IL_00ed;
	}

IL_00ad:
	{
		__this->___mErrorText_3 = _stringLiteral18;
		goto IL_00ed;
	}

IL_00bd:
	{
		__this->___mErrorText_3 = _stringLiteral19;
		goto IL_00ed;
	}

IL_00cd:
	{
		__this->___mErrorText_3 = _stringLiteral20;
		goto IL_00ed;
	}

IL_00dd:
	{
		__this->___mErrorText_3 = _stringLiteral21;
		goto IL_00ed;
	}

IL_00ed:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C" void DefaultInitializationErrorHandler_SetErrorOccurred_m44 (DefaultInitializationErrorHandler_t26 * __this, bool ___errorOccurred, const MethodInfo* method)
{
	{
		bool L_0 = ___errorOccurred;
		__this->___mErrorOccurred_4 = L_0;
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnVuforiaInitializationError(Vuforia.VuforiaUnity/InitError)
extern "C" void DefaultInitializationErrorHandler_OnVuforiaInitializationError_m45 (DefaultInitializationErrorHandler_t26 * __this, int32_t ___initError, const MethodInfo* method)
{
	{
		int32_t L_0 = ___initError;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___initError;
		DefaultInitializationErrorHandler_SetErrorCode_m43(__this, L_1, /*hidden argument*/NULL);
		DefaultInitializationErrorHandler_SetErrorOccurred_m44(__this, 1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
extern "C" void DefaultSmartTerrainEventHandler__ctor_m46 (DefaultSmartTerrainEventHandler_t27 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m141(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
extern TypeInfo* Action_1_t97_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t98_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisReconstructionBehaviour_t28_m196_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnPropCreated_m49_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m197_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnSurfaceCreated_m50_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m199_MethodInfo_var;
extern "C" void DefaultSmartTerrainEventHandler_Start_m47 (DefaultSmartTerrainEventHandler_t27 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t97_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Action_1_t98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		Component_GetComponent_TisReconstructionBehaviour_t28_m196_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		DefaultSmartTerrainEventHandler_OnPropCreated_m49_MethodInfo_var = il2cpp_codegen_method_info_from_index(11);
		Action_1__ctor_m197_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483660);
		DefaultSmartTerrainEventHandler_OnSurfaceCreated_m50_MethodInfo_var = il2cpp_codegen_method_info_from_index(13);
		Action_1__ctor_m199_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReconstructionBehaviour_t28 * L_0 = Component_GetComponent_TisReconstructionBehaviour_t28_m196(__this, /*hidden argument*/Component_GetComponent_TisReconstructionBehaviour_t28_m196_MethodInfo_var);
		__this->___mReconstructionBehaviour_2 = L_0;
		ReconstructionBehaviour_t28 * L_1 = (__this->___mReconstructionBehaviour_2);
		bool L_2 = Object_op_Implicit_m182(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		ReconstructionBehaviour_t28 * L_3 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t L_4 = { (void*)DefaultSmartTerrainEventHandler_OnPropCreated_m49_MethodInfo_var };
		Action_1_t97 * L_5 = (Action_1_t97 *)il2cpp_codegen_object_new (Action_1_t97_il2cpp_TypeInfo_var);
		Action_1__ctor_m197(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m197_MethodInfo_var);
		NullCheck(L_3);
		ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m198(L_3, L_5, /*hidden argument*/NULL);
		ReconstructionBehaviour_t28 * L_6 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t L_7 = { (void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m50_MethodInfo_var };
		Action_1_t98 * L_8 = (Action_1_t98 *)il2cpp_codegen_object_new (Action_1_t98_il2cpp_TypeInfo_var);
		Action_1__ctor_m199(L_8, __this, L_7, /*hidden argument*/Action_1__ctor_m199_MethodInfo_var);
		NullCheck(L_6);
		ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m200(L_6, L_8, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
extern TypeInfo* Action_1_t97_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t98_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnPropCreated_m49_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m197_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnSurfaceCreated_m50_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m199_MethodInfo_var;
extern "C" void DefaultSmartTerrainEventHandler_OnDestroy_m48 (DefaultSmartTerrainEventHandler_t27 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t97_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Action_1_t98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		DefaultSmartTerrainEventHandler_OnPropCreated_m49_MethodInfo_var = il2cpp_codegen_method_info_from_index(11);
		Action_1__ctor_m197_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483660);
		DefaultSmartTerrainEventHandler_OnSurfaceCreated_m50_MethodInfo_var = il2cpp_codegen_method_info_from_index(13);
		Action_1__ctor_m199_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReconstructionBehaviour_t28 * L_0 = (__this->___mReconstructionBehaviour_2);
		bool L_1 = Object_op_Implicit_m182(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		ReconstructionBehaviour_t28 * L_2 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t L_3 = { (void*)DefaultSmartTerrainEventHandler_OnPropCreated_m49_MethodInfo_var };
		Action_1_t97 * L_4 = (Action_1_t97 *)il2cpp_codegen_object_new (Action_1_t97_il2cpp_TypeInfo_var);
		Action_1__ctor_m197(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m197_MethodInfo_var);
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m201(L_2, L_4, /*hidden argument*/NULL);
		ReconstructionBehaviour_t28 * L_5 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t L_6 = { (void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m50_MethodInfo_var };
		Action_1_t98 * L_7 = (Action_1_t98 *)il2cpp_codegen_object_new (Action_1_t98_il2cpp_TypeInfo_var);
		Action_1__ctor_m199(L_7, __this, L_6, /*hidden argument*/Action_1__ctor_m199_MethodInfo_var);
		NullCheck(L_5);
		ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m202(L_5, L_7, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
extern "C" void DefaultSmartTerrainEventHandler_OnPropCreated_m49 (DefaultSmartTerrainEventHandler_t27 * __this, Object_t * ___prop, const MethodInfo* method)
{
	{
		ReconstructionBehaviour_t28 * L_0 = (__this->___mReconstructionBehaviour_2);
		bool L_1 = Object_op_Implicit_m182(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t28 * L_2 = (__this->___mReconstructionBehaviour_2);
		PropBehaviour_t29 * L_3 = (__this->___PropTemplate_3);
		Object_t * L_4 = ___prop;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateProp_m203(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
extern "C" void DefaultSmartTerrainEventHandler_OnSurfaceCreated_m50 (DefaultSmartTerrainEventHandler_t27 * __this, Object_t * ___surface, const MethodInfo* method)
{
	{
		ReconstructionBehaviour_t28 * L_0 = (__this->___mReconstructionBehaviour_2);
		bool L_1 = Object_op_Implicit_m182(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t28 * L_2 = (__this->___mReconstructionBehaviour_2);
		SurfaceBehaviour_t30 * L_3 = (__this->___SurfaceTemplate_4);
		Object_t * L_4 = ___surface;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateSurface_m204(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::.ctor()
extern "C" void DefaultTrackableEventHandler__ctor_m51 (DefaultTrackableEventHandler_t31 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m141(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::Start()
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t32_m205_MethodInfo_var;
extern "C" void DefaultTrackableEventHandler_Start_m52 (DefaultTrackableEventHandler_t31 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisTrackableBehaviour_t32_m205_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483663);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableBehaviour_t32 * L_0 = Component_GetComponent_TisTrackableBehaviour_t32_m205(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t32_m205_MethodInfo_var);
		__this->___mTrackableBehaviour_2 = L_0;
		TrackableBehaviour_t32 * L_1 = (__this->___mTrackableBehaviour_2);
		bool L_2 = Object_op_Implicit_m182(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t32 * L_3 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m206(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C" void DefaultTrackableEventHandler_OnTrackableStateChanged_m53 (DefaultTrackableEventHandler_t31 * __this, int32_t ___previousStatus, int32_t ___newStatus, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = ___newStatus;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = ___newStatus;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		DefaultTrackableEventHandler_OnTrackingFound_m54(__this, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		DefaultTrackableEventHandler_OnTrackingLost_m55(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t85_m207_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t14_m208_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern "C" void DefaultTrackableEventHandler_OnTrackingFound_m54 (DefaultTrackableEventHandler_t31 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Component_GetComponentsInChildren_TisRenderer_t85_m207_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		Component_GetComponentsInChildren_TisCollider_t14_m208_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t99* V_0 = {0};
	ColliderU5BU5D_t100* V_1 = {0};
	Renderer_t85 * V_2 = {0};
	RendererU5BU5D_t99* V_3 = {0};
	int32_t V_4 = 0;
	Collider_t14 * V_5 = {0};
	ColliderU5BU5D_t100* V_6 = {0};
	int32_t V_7 = 0;
	{
		RendererU5BU5D_t99* L_0 = Component_GetComponentsInChildren_TisRenderer_t85_m207(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t85_m207_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t100* L_1 = Component_GetComponentsInChildren_TisCollider_t14_m208(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t14_m208_MethodInfo_var);
		V_1 = L_1;
		RendererU5BU5D_t99* L_2 = V_0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		RendererU5BU5D_t99* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_2 = (*(Renderer_t85 **)(Renderer_t85 **)SZArrayLdElema(L_3, L_5, sizeof(Renderer_t85 *)));
		Renderer_t85 * L_6 = V_2;
		NullCheck(L_6);
		Renderer_set_enabled_m209(L_6, 1, /*hidden argument*/NULL);
		int32_t L_7 = V_4;
		V_4 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_8 = V_4;
		RendererU5BU5D_t99* L_9 = V_3;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		ColliderU5BU5D_t100* L_10 = V_1;
		V_6 = L_10;
		V_7 = 0;
		goto IL_0056;
	}

IL_0041:
	{
		ColliderU5BU5D_t100* L_11 = V_6;
		int32_t L_12 = V_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		V_5 = (*(Collider_t14 **)(Collider_t14 **)SZArrayLdElema(L_11, L_13, sizeof(Collider_t14 *)));
		Collider_t14 * L_14 = V_5;
		NullCheck(L_14);
		Collider_set_enabled_m210(L_14, 1, /*hidden argument*/NULL);
		int32_t L_15 = V_7;
		V_7 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0056:
	{
		int32_t L_16 = V_7;
		ColliderU5BU5D_t100* L_17 = V_6;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_17)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		TrackableBehaviour_t32 * L_18 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_18);
		String_t* L_19 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m211(NULL /*static, unused*/, _stringLiteral22, L_19, _stringLiteral23, /*hidden argument*/NULL);
		Debug_Log_m212(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t85_m207_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t14_m208_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral24;
extern "C" void DefaultTrackableEventHandler_OnTrackingLost_m55 (DefaultTrackableEventHandler_t31 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Component_GetComponentsInChildren_TisRenderer_t85_m207_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		Component_GetComponentsInChildren_TisCollider_t14_m208_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral24 = il2cpp_codegen_string_literal_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t99* V_0 = {0};
	ColliderU5BU5D_t100* V_1 = {0};
	Renderer_t85 * V_2 = {0};
	RendererU5BU5D_t99* V_3 = {0};
	int32_t V_4 = 0;
	Collider_t14 * V_5 = {0};
	ColliderU5BU5D_t100* V_6 = {0};
	int32_t V_7 = 0;
	{
		RendererU5BU5D_t99* L_0 = Component_GetComponentsInChildren_TisRenderer_t85_m207(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t85_m207_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t100* L_1 = Component_GetComponentsInChildren_TisCollider_t14_m208(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t14_m208_MethodInfo_var);
		V_1 = L_1;
		RendererU5BU5D_t99* L_2 = V_0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		RendererU5BU5D_t99* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_2 = (*(Renderer_t85 **)(Renderer_t85 **)SZArrayLdElema(L_3, L_5, sizeof(Renderer_t85 *)));
		Renderer_t85 * L_6 = V_2;
		NullCheck(L_6);
		Renderer_set_enabled_m209(L_6, 0, /*hidden argument*/NULL);
		int32_t L_7 = V_4;
		V_4 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_8 = V_4;
		RendererU5BU5D_t99* L_9 = V_3;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		ColliderU5BU5D_t100* L_10 = V_1;
		V_6 = L_10;
		V_7 = 0;
		goto IL_0056;
	}

IL_0041:
	{
		ColliderU5BU5D_t100* L_11 = V_6;
		int32_t L_12 = V_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		V_5 = (*(Collider_t14 **)(Collider_t14 **)SZArrayLdElema(L_11, L_13, sizeof(Collider_t14 *)));
		Collider_t14 * L_14 = V_5;
		NullCheck(L_14);
		Collider_set_enabled_m210(L_14, 0, /*hidden argument*/NULL);
		int32_t L_15 = V_7;
		V_7 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0056:
	{
		int32_t L_16 = V_7;
		ColliderU5BU5D_t100* L_17 = V_6;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_17)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		TrackableBehaviour_t32 * L_18 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_18);
		String_t* L_19 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m211(NULL /*static, unused*/, _stringLiteral22, L_19, _stringLiteral24, /*hidden argument*/NULL);
		Debug_Log_m212(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::.ctor()
extern "C" void GLErrorHandler__ctor_m56 (GLErrorHandler_t33 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m141(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GLErrorHandler_t33_il2cpp_TypeInfo_var;
extern "C" void GLErrorHandler__cctor_m57 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		GLErrorHandler_t33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		((GLErrorHandler_t33_StaticFields*)GLErrorHandler_t33_il2cpp_TypeInfo_var->static_fields)->___mErrorText_3 = L_0;
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::SetError(System.String)
extern TypeInfo* GLErrorHandler_t33_il2cpp_TypeInfo_var;
extern "C" void GLErrorHandler_SetError_m58 (Object_t * __this /* static, unused */, String_t* ___errorText, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GLErrorHandler_t33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___errorText;
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t33_il2cpp_TypeInfo_var);
		((GLErrorHandler_t33_StaticFields*)GLErrorHandler_t33_il2cpp_TypeInfo_var->static_fields)->___mErrorText_3 = L_0;
		((GLErrorHandler_t33_StaticFields*)GLErrorHandler_t33_il2cpp_TypeInfo_var->static_fields)->___mErrorOccurred_4 = 1;
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::OnGUI()
extern TypeInfo* GLErrorHandler_t33_il2cpp_TypeInfo_var;
extern TypeInfo* WindowFunction_t95_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t96_il2cpp_TypeInfo_var;
extern const MethodInfo* GLErrorHandler_DrawWindowContent_m60_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral25;
extern "C" void GLErrorHandler_OnGUI_m59 (GLErrorHandler_t33 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GLErrorHandler_t33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		WindowFunction_t95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		GUI_t96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		GLErrorHandler_DrawWindowContent_m60_MethodInfo_var = il2cpp_codegen_method_info_from_index(18);
		_stringLiteral25 = il2cpp_codegen_string_literal_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t33_il2cpp_TypeInfo_var);
		bool L_0 = ((GLErrorHandler_t33_StaticFields*)GLErrorHandler_t33_il2cpp_TypeInfo_var->static_fields)->___mErrorOccurred_4;
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m185(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m186(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t94  L_3 = {0};
		Rect__ctor_m187(&L_3, (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		IntPtr_t L_4 = { (void*)GLErrorHandler_DrawWindowContent_m60_MethodInfo_var };
		WindowFunction_t95 * L_5 = (WindowFunction_t95 *)il2cpp_codegen_object_new (WindowFunction_t95_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m188(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t96_il2cpp_TypeInfo_var);
		GUI_Window_m189(NULL /*static, unused*/, 0, L_3, L_5, _stringLiteral25, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::DrawWindowContent(System.Int32)
extern TypeInfo* GLErrorHandler_t33_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t96_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral10;
extern "C" void GLErrorHandler_DrawWindowContent_m60 (GLErrorHandler_t33 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GLErrorHandler_t33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		GUI_t96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral10 = il2cpp_codegen_string_literal_from_index(10);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m185(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m186(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t94  L_2 = {0};
		Rect__ctor_m187(&L_2, (10.0f), (25.0f), (((float)((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20)))))), (((float)((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95)))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t33_il2cpp_TypeInfo_var);
		String_t* L_3 = ((GLErrorHandler_t33_StaticFields*)GLErrorHandler_t33_il2cpp_TypeInfo_var->static_fields)->___mErrorText_3;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t96_il2cpp_TypeInfo_var);
		GUI_Label_m191(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m185(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m186(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t94  L_6 = {0};
		Rect__ctor_m187(&L_6, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75)))))), (((float)((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60)))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m192(NULL /*static, unused*/, L_6, _stringLiteral10, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0062;
		}
	}
	{
		Application_Quit_m193(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern "C" void HideExcessAreaBehaviour__ctor_m61 (HideExcessAreaBehaviour_t34 * __this, const MethodInfo* method)
{
	{
		HideExcessAreaAbstractBehaviour__ctor_m213(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern "C" void ImageTargetBehaviour__ctor_m62 (ImageTargetBehaviour_t36 * __this, const MethodInfo* method)
{
	{
		ImageTargetAbstractBehaviour__ctor_m214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::.ctor()
extern "C" void AndroidUnityPlayer__ctor_m63 (AndroidUnityPlayer_t38 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m215(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibraries()
extern "C" void AndroidUnityPlayer_LoadNativeLibraries_m64 (AndroidUnityPlayer_t38 * __this, const MethodInfo* method)
{
	{
		AndroidUnityPlayer_LoadNativeLibrariesFromJava_m72(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializePlatform()
extern "C" void AndroidUnityPlayer_InitializePlatform_m65 (AndroidUnityPlayer_t38 * __this, const MethodInfo* method)
{
	{
		AndroidUnityPlayer_InitAndroidPlatform_m73(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaUnity/InitError Vuforia.AndroidUnityPlayer::Start(System.String)
extern "C" int32_t AndroidUnityPlayer_Start_m66 (AndroidUnityPlayer_t38 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___licenseKey;
		int32_t L_1 = AndroidUnityPlayer_InitVuforia_m74(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m75(__this, /*hidden argument*/NULL);
	}

IL_0015:
	{
		int32_t L_3 = V_0;
		return (int32_t)(L_3);
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Update()
extern TypeInfo* SurfaceUtilities_t101_il2cpp_TypeInfo_var;
extern "C" void AndroidUnityPlayer_Update_m67 (AndroidUnityPlayer_t38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t101_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m216(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m75(__this, /*hidden argument*/NULL);
		goto IL_0031;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m217(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___mScreenOrientation_2);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		AndroidUnityPlayer_ResetUnityScreenOrientation_m76(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		AndroidUnityPlayer_CheckOrientation_m77(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		int32_t L_3 = (__this->___mFramesSinceLastOrientationReset_4);
		__this->___mFramesSinceLastOrientationReset_4 = ((int32_t)((int32_t)L_3+(int32_t)1));
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnPause()
extern "C" void AndroidUnityPlayer_OnPause_m68 (AndroidUnityPlayer_t38 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_OnPause_m218(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnResume()
extern "C" void AndroidUnityPlayer_OnResume_m69 (AndroidUnityPlayer_t38 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_OnResume_m219(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnDestroy()
extern "C" void AndroidUnityPlayer_OnDestroy_m70 (AndroidUnityPlayer_t38 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_Deinit_m220(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Dispose()
extern "C" void AndroidUnityPlayer_Dispose_m71 (AndroidUnityPlayer_t38 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibrariesFromJava()
extern "C" void AndroidUnityPlayer_LoadNativeLibrariesFromJava_m72 (AndroidUnityPlayer_t38 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitAndroidPlatform()
extern "C" void AndroidUnityPlayer_InitAndroidPlatform_m73 (AndroidUnityPlayer_t38 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 Vuforia.AndroidUnityPlayer::InitVuforia(System.String)
extern "C" int32_t AndroidUnityPlayer_InitVuforia_m74 (AndroidUnityPlayer_t38 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializeSurface()
extern TypeInfo* SurfaceUtilities_t101_il2cpp_TypeInfo_var;
extern "C" void AndroidUnityPlayer_InitializeSurface_m75 (AndroidUnityPlayer_t38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t101_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m221(NULL /*static, unused*/, /*hidden argument*/NULL);
		AndroidUnityPlayer_ResetUnityScreenOrientation_m76(__this, /*hidden argument*/NULL);
		AndroidUnityPlayer_CheckOrientation_m77(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::ResetUnityScreenOrientation()
extern "C" void AndroidUnityPlayer_ResetUnityScreenOrientation_m76 (AndroidUnityPlayer_t38 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_orientation_m217(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mScreenOrientation_2 = L_0;
		__this->___mFramesSinceLastOrientationReset_4 = 0;
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::CheckOrientation()
extern TypeInfo* SurfaceUtilities_t101_il2cpp_TypeInfo_var;
extern "C" void AndroidUnityPlayer_CheckOrientation_m77 (AndroidUnityPlayer_t38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = (__this->___mFramesSinceLastOrientationReset_4);
		V_0 = ((((int32_t)L_0) < ((int32_t)((int32_t)25)))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = (__this->___mFramesSinceLastJavaOrientationCheck_5);
		V_0 = ((((int32_t)L_2) > ((int32_t)((int32_t)60)))? 1 : 0);
	}

IL_001c:
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_4 = (__this->___mScreenOrientation_2);
		V_1 = L_4;
		int32_t L_5 = V_1;
		V_2 = L_5;
		int32_t L_6 = V_2;
		int32_t L_7 = (__this->___mJavaScreenOrientation_3);
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_8 = V_2;
		__this->___mJavaScreenOrientation_3 = L_8;
		int32_t L_9 = (__this->___mJavaScreenOrientation_3);
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t101_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m222(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0049:
	{
		__this->___mFramesSinceLastJavaOrientationCheck_5 = 0;
		goto IL_0063;
	}

IL_0055:
	{
		int32_t L_10 = (__this->___mFramesSinceLastJavaOrientationCheck_5);
		__this->___mFramesSinceLastJavaOrientationCheck_5 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0063:
	{
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
extern "C" void ComponentFactoryStarterBehaviour__ctor_m78 (ComponentFactoryStarterBehaviour_t39 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m141(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
extern const Il2CppType* Action_t106_0_0_0_var;
extern TypeInfo* Attribute_t104_il2cpp_TypeInfo_var;
extern TypeInfo* FactorySetter_t109_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t106_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t103_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t111_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ToList_TisMethodInfo_t_m224_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m225_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m226_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m227_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m230_MethodInfo_var;
extern "C" void ComponentFactoryStarterBehaviour_Awake_m79 (ComponentFactoryStarterBehaviour_t39 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t106_0_0_0_var = il2cpp_codegen_type_from_index(30);
		Attribute_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		FactorySetter_t109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(32);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		Action_t106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		Enumerator_t103_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		IDisposable_t111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Enumerable_ToList_TisMethodInfo_t_m224_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483667);
		List_1_AddRange_m225_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483668);
		List_1_GetEnumerator_m226_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483669);
		Enumerator_get_Current_m227_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483670);
		Enumerator_MoveNext_m230_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483671);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t102 * V_0 = {0};
	MethodInfo_t * V_1 = {0};
	Enumerator_t103  V_2 = {0};
	Attribute_t104 * V_3 = {0};
	ObjectU5BU5D_t105* V_4 = {0};
	int32_t V_5 = 0;
	Action_t106 * V_6 = {0};
	Exception_t107 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t107 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = Object_GetType_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		MethodInfoU5BU5D_t108* L_1 = (MethodInfoU5BU5D_t108*)VirtFuncInvoker1< MethodInfoU5BU5D_t108*, int32_t >::Invoke(51 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_0, ((int32_t)38));
		List_1_t102 * L_2 = Enumerable_ToList_TisMethodInfo_t_m224(NULL /*static, unused*/, (Object_t*)(Object_t*)L_1, /*hidden argument*/Enumerable_ToList_TisMethodInfo_t_m224_MethodInfo_var);
		V_0 = L_2;
		List_1_t102 * L_3 = V_0;
		Type_t * L_4 = Object_GetType_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		MethodInfoU5BU5D_t108* L_5 = (MethodInfoU5BU5D_t108*)VirtFuncInvoker1< MethodInfoU5BU5D_t108*, int32_t >::Invoke(51 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_4, ((int32_t)22));
		NullCheck(L_3);
		List_1_AddRange_m225(L_3, (Object_t*)(Object_t*)L_5, /*hidden argument*/List_1_AddRange_m225_MethodInfo_var);
		List_1_t102 * L_6 = V_0;
		NullCheck(L_6);
		Enumerator_t103  L_7 = List_1_GetEnumerator_m226(L_6, /*hidden argument*/List_1_GetEnumerator_m226_MethodInfo_var);
		V_2 = L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0098;
		}

IL_0032:
		{
			MethodInfo_t * L_8 = Enumerator_get_Current_m227((&V_2), /*hidden argument*/Enumerator_get_Current_m227_MethodInfo_var);
			V_1 = L_8;
			MethodInfo_t * L_9 = V_1;
			NullCheck(L_9);
			ObjectU5BU5D_t105* L_10 = (ObjectU5BU5D_t105*)VirtFuncInvoker1< ObjectU5BU5D_t105*, bool >::Invoke(12 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_9, 1);
			V_4 = L_10;
			V_5 = 0;
			goto IL_008d;
		}

IL_004b:
		{
			ObjectU5BU5D_t105* L_11 = V_4;
			int32_t L_12 = V_5;
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
			int32_t L_13 = L_12;
			V_3 = ((Attribute_t104 *)CastclassClass((*(Object_t **)(Object_t **)SZArrayLdElema(L_11, L_13, sizeof(Object_t *))), Attribute_t104_il2cpp_TypeInfo_var));
			Attribute_t104 * L_14 = V_3;
			if (!((FactorySetter_t109 *)IsInstClass(L_14, FactorySetter_t109_il2cpp_TypeInfo_var)))
			{
				goto IL_0087;
			}
		}

IL_0061:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_15 = Type_GetTypeFromHandle_m180(NULL /*static, unused*/, LoadTypeToken(Action_t106_0_0_0_var), /*hidden argument*/NULL);
			MethodInfo_t * L_16 = V_1;
			Delegate_t110 * L_17 = Delegate_CreateDelegate_m228(NULL /*static, unused*/, L_15, __this, L_16, /*hidden argument*/NULL);
			V_6 = ((Action_t106 *)IsInstSealed(L_17, Action_t106_il2cpp_TypeInfo_var));
			Action_t106 * L_18 = V_6;
			if (!L_18)
			{
				goto IL_0087;
			}
		}

IL_0080:
		{
			Action_t106 * L_19 = V_6;
			NullCheck(L_19);
			Action_Invoke_m229(L_19, /*hidden argument*/NULL);
		}

IL_0087:
		{
			int32_t L_20 = V_5;
			V_5 = ((int32_t)((int32_t)L_20+(int32_t)1));
		}

IL_008d:
		{
			int32_t L_21 = V_5;
			ObjectU5BU5D_t105* L_22 = V_4;
			NullCheck(L_22);
			if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_22)->max_length)))))))
			{
				goto IL_004b;
			}
		}

IL_0098:
		{
			bool L_23 = Enumerator_MoveNext_m230((&V_2), /*hidden argument*/Enumerator_MoveNext_m230_MethodInfo_var);
			if (L_23)
			{
				goto IL_0032;
			}
		}

IL_00a4:
		{
			IL2CPP_LEAVE(0xB5, FINALLY_00a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t107 *)e.ex;
		goto FINALLY_00a9;
	}

FINALLY_00a9:
	{ // begin finally (depth: 1)
		Enumerator_t103  L_24 = V_2;
		Enumerator_t103  L_25 = L_24;
		Object_t * L_26 = Box(Enumerator_t103_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_26);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t111_il2cpp_TypeInfo_var, L_26);
		IL2CPP_END_FINALLY(169)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(169)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t107 *)
	}

IL_00b5:
	{
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
extern TypeInfo* VuforiaBehaviourComponentFactory_t41_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral26;
extern "C" void ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m80 (ComponentFactoryStarterBehaviour_t39 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaBehaviourComponentFactory_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		_stringLiteral26 = il2cpp_codegen_string_literal_from_index(26);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m212(NULL /*static, unused*/, _stringLiteral26, /*hidden argument*/NULL);
		VuforiaBehaviourComponentFactory_t41 * L_0 = (VuforiaBehaviourComponentFactory_t41 *)il2cpp_codegen_object_new (VuforiaBehaviourComponentFactory_t41_il2cpp_TypeInfo_var);
		VuforiaBehaviourComponentFactory__ctor_m95(L_0, /*hidden argument*/NULL);
		BehaviourComponentFactory_set_Instance_m231(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::.ctor()
extern "C" void IOSUnityPlayer__ctor_m81 (IOSUnityPlayer_t40 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m215(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::LoadNativeLibraries()
extern "C" void IOSUnityPlayer_LoadNativeLibraries_m82 (IOSUnityPlayer_t40 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializePlatform()
extern "C" void IOSUnityPlayer_InitializePlatform_m83 (IOSUnityPlayer_t40 * __this, const MethodInfo* method)
{
	{
		IOSUnityPlayer_setPlatFormNative_m92(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaUnity/InitError Vuforia.IOSUnityPlayer::Start(System.String)
extern "C" int32_t IOSUnityPlayer_Start_m84 (IOSUnityPlayer_t40 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Screen_get_orientation_m217(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___licenseKey;
		int32_t L_2 = IOSUnityPlayer_initQCARiOS_m93(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m90(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		return (int32_t)(L_4);
	}
}
// System.Void Vuforia.IOSUnityPlayer::Update()
extern TypeInfo* SurfaceUtilities_t101_il2cpp_TypeInfo_var;
extern "C" void IOSUnityPlayer_Update_m85 (IOSUnityPlayer_t40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t101_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m216(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m90(__this, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m217(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___mScreenOrientation_0);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		IOSUnityPlayer_SetUnityScreenOrientation_m91(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::Dispose()
extern "C" void IOSUnityPlayer_Dispose_m86 (IOSUnityPlayer_t40 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnPause()
extern "C" void IOSUnityPlayer_OnPause_m87 (IOSUnityPlayer_t40 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_OnPause_m218(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnResume()
extern "C" void IOSUnityPlayer_OnResume_m88 (IOSUnityPlayer_t40 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_OnResume_m219(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnDestroy()
extern "C" void IOSUnityPlayer_OnDestroy_m89 (IOSUnityPlayer_t40 * __this, const MethodInfo* method)
{
	{
		VuforiaUnity_Deinit_m220(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializeSurface()
extern TypeInfo* SurfaceUtilities_t101_il2cpp_TypeInfo_var;
extern "C" void IOSUnityPlayer_InitializeSurface_m90 (IOSUnityPlayer_t40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t101_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m221(NULL /*static, unused*/, /*hidden argument*/NULL);
		IOSUnityPlayer_SetUnityScreenOrientation_m91(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::SetUnityScreenOrientation()
extern TypeInfo* SurfaceUtilities_t101_il2cpp_TypeInfo_var;
extern "C" void IOSUnityPlayer_SetUnityScreenOrientation_m91 (IOSUnityPlayer_t40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_orientation_m217(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mScreenOrientation_0 = L_0;
		int32_t L_1 = (__this->___mScreenOrientation_0);
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t101_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m222(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___mScreenOrientation_0);
		IOSUnityPlayer_setSurfaceOrientationiOS_m94(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::setPlatFormNative()
extern "C" {void DEFAULT_CALL setPlatFormNative();}
extern "C" void IOSUnityPlayer_setPlatFormNative_m92 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setPlatFormNative;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setPlatFormNative'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Int32 Vuforia.IOSUnityPlayer::initQCARiOS(System.Int32,System.String)
extern "C" {int32_t DEFAULT_CALL initQCARiOS(int32_t, char*);}
extern "C" int32_t IOSUnityPlayer_initQCARiOS_m93 (Object_t * __this /* static, unused */, int32_t ___screenOrientation, String_t* ___licenseKey, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)initQCARiOS;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'initQCARiOS'"));
		}
	}

	// Marshaling of parameter '___screenOrientation' to native representation

	// Marshaling of parameter '___licenseKey' to native representation
	char* ____licenseKey_marshaled = { 0 };
	____licenseKey_marshaled = il2cpp_codegen_marshal_string(___licenseKey);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(___screenOrientation, ____licenseKey_marshaled);

	// Marshaling cleanup of parameter '___screenOrientation' native representation

	// Marshaling cleanup of parameter '___licenseKey' native representation
	il2cpp_codegen_marshal_free(____licenseKey_marshaled);
	____licenseKey_marshaled = NULL;

	return _return_value;
}
// System.Void Vuforia.IOSUnityPlayer::setSurfaceOrientationiOS(System.Int32)
extern "C" {void DEFAULT_CALL setSurfaceOrientationiOS(int32_t);}
extern "C" void IOSUnityPlayer_setSurfaceOrientationiOS_m94 (Object_t * __this /* static, unused */, int32_t ___screenOrientation, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setSurfaceOrientationiOS;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setSurfaceOrientationiOS'"));
		}
	}

	// Marshaling of parameter '___screenOrientation' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___screenOrientation);

	// Marshaling cleanup of parameter '___screenOrientation' native representation

}
// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C" void VuforiaBehaviourComponentFactory__ctor_m95 (VuforiaBehaviourComponentFactory_t41 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m215(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMaskOutBehaviour_t46_m232_MethodInfo_var;
extern "C" MaskOutAbstractBehaviour_t47 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m96 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisMaskOutBehaviour_t46_m232_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483672);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t9 * L_0 = ___gameObject;
		NullCheck(L_0);
		MaskOutBehaviour_t46 * L_1 = GameObject_AddComponent_TisMaskOutBehaviour_t46_m232(L_0, /*hidden argument*/GameObject_AddComponent_TisMaskOutBehaviour_t46_m232_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisVirtualButtonBehaviour_t70_m233_MethodInfo_var;
extern "C" VirtualButtonAbstractBehaviour_t71 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m97 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisVirtualButtonBehaviour_t70_m233_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483673);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t9 * L_0 = ___gameObject;
		NullCheck(L_0);
		VirtualButtonBehaviour_t70 * L_1 = GameObject_AddComponent_TisVirtualButtonBehaviour_t70_m233(L_0, /*hidden argument*/GameObject_AddComponent_TisVirtualButtonBehaviour_t70_m233_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisTurnOffBehaviour_t61_m234_MethodInfo_var;
extern "C" TurnOffAbstractBehaviour_t62 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m98 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisTurnOffBehaviour_t61_m234_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483674);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t9 * L_0 = ___gameObject;
		NullCheck(L_0);
		TurnOffBehaviour_t61 * L_1 = GameObject_AddComponent_TisTurnOffBehaviour_t61_m234(L_0, /*hidden argument*/GameObject_AddComponent_TisTurnOffBehaviour_t61_m234_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisImageTargetBehaviour_t36_m235_MethodInfo_var;
extern "C" ImageTargetAbstractBehaviour_t37 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m99 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisImageTargetBehaviour_t36_m235_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483675);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t9 * L_0 = ___gameObject;
		NullCheck(L_0);
		ImageTargetBehaviour_t36 * L_1 = GameObject_AddComponent_TisImageTargetBehaviour_t36_m235(L_0, /*hidden argument*/GameObject_AddComponent_TisImageTargetBehaviour_t36_m235_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMarkerBehaviour_t44_m236_MethodInfo_var;
extern "C" MarkerAbstractBehaviour_t45 * VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m100 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisMarkerBehaviour_t44_m236_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483676);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t9 * L_0 = ___gameObject;
		NullCheck(L_0);
		MarkerBehaviour_t44 * L_1 = GameObject_AddComponent_TisMarkerBehaviour_t44_m236(L_0, /*hidden argument*/GameObject_AddComponent_TisMarkerBehaviour_t44_m236_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMultiTargetBehaviour_t48_m237_MethodInfo_var;
extern "C" MultiTargetAbstractBehaviour_t49 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m101 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisMultiTargetBehaviour_t48_m237_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483677);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t9 * L_0 = ___gameObject;
		NullCheck(L_0);
		MultiTargetBehaviour_t48 * L_1 = GameObject_AddComponent_TisMultiTargetBehaviour_t48_m237(L_0, /*hidden argument*/GameObject_AddComponent_TisMultiTargetBehaviour_t48_m237_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisCylinderTargetBehaviour_t22_m238_MethodInfo_var;
extern "C" CylinderTargetAbstractBehaviour_t23 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m102 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisCylinderTargetBehaviour_t22_m238_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483678);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t9 * L_0 = ___gameObject;
		NullCheck(L_0);
		CylinderTargetBehaviour_t22 * L_1 = GameObject_AddComponent_TisCylinderTargetBehaviour_t22_m238(L_0, /*hidden argument*/GameObject_AddComponent_TisCylinderTargetBehaviour_t22_m238_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisWordBehaviour_t79_m239_MethodInfo_var;
extern "C" WordAbstractBehaviour_t80 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m103 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisWordBehaviour_t79_m239_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483679);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t9 * L_0 = ___gameObject;
		NullCheck(L_0);
		WordBehaviour_t79 * L_1 = GameObject_AddComponent_TisWordBehaviour_t79_m239(L_0, /*hidden argument*/GameObject_AddComponent_TisWordBehaviour_t79_m239_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisTextRecoBehaviour_t59_m240_MethodInfo_var;
extern "C" TextRecoAbstractBehaviour_t60 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m104 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisTextRecoBehaviour_t59_m240_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483680);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t9 * L_0 = ___gameObject;
		NullCheck(L_0);
		TextRecoBehaviour_t59 * L_1 = GameObject_AddComponent_TisTextRecoBehaviour_t59_m240(L_0, /*hidden argument*/GameObject_AddComponent_TisTextRecoBehaviour_t59_m240_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisObjectTargetBehaviour_t50_m241_MethodInfo_var;
extern "C" ObjectTargetAbstractBehaviour_t51 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m105 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisObjectTargetBehaviour_t50_m241_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483681);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t9 * L_0 = ___gameObject;
		NullCheck(L_0);
		ObjectTargetBehaviour_t50 * L_1 = GameObject_AddComponent_TisObjectTargetBehaviour_t50_m241(L_0, /*hidden argument*/GameObject_AddComponent_TisObjectTargetBehaviour_t50_m241_MethodInfo_var);
		return L_1;
	}
}
// System.Void Vuforia.KeepAliveBehaviour::.ctor()
extern "C" void KeepAliveBehaviour__ctor_m106 (KeepAliveBehaviour_t42 * __this, const MethodInfo* method)
{
	{
		KeepAliveAbstractBehaviour__ctor_m242(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.MarkerBehaviour::.ctor()
extern "C" void MarkerBehaviour__ctor_m107 (MarkerBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		MarkerAbstractBehaviour__ctor_m243(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.MaskOutBehaviour::.ctor()
extern "C" void MaskOutBehaviour__ctor_m108 (MaskOutBehaviour_t46 * __this, const MethodInfo* method)
{
	{
		MaskOutAbstractBehaviour__ctor_m244(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.MaskOutBehaviour::Start()
extern TypeInfo* VuforiaRuntimeUtilities_t112_il2cpp_TypeInfo_var;
extern TypeInfo* MaterialU5BU5D_t4_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t85_m157_MethodInfo_var;
extern "C" void MaskOutBehaviour_Start_m109 (MaskOutBehaviour_t46 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaRuntimeUtilities_t112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		MaterialU5BU5D_t4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		Component_GetComponent_TisRenderer_t85_m157_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		s_Il2CppMethodIntialized = true;
	}
	Renderer_t85 * V_0 = {0};
	int32_t V_1 = 0;
	MaterialU5BU5D_t4* V_2 = {0};
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t112_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m245(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		Renderer_t85 * L_1 = Component_GetComponent_TisRenderer_t85_m157(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t85_m157_MethodInfo_var);
		V_0 = L_1;
		Renderer_t85 * L_2 = V_0;
		NullCheck(L_2);
		MaterialU5BU5D_t4* L_3 = Renderer_get_materials_m246(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((Array_t *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0032;
		}
	}
	{
		Renderer_t85 * L_5 = V_0;
		Material_t8 * L_6 = (((MaskOutAbstractBehaviour_t47 *)__this)->___maskMaterial_2);
		NullCheck(L_5);
		Renderer_set_sharedMaterial_m153(L_5, L_6, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_0032:
	{
		int32_t L_7 = V_1;
		V_2 = ((MaterialU5BU5D_t4*)SZArrayNew(MaterialU5BU5D_t4_il2cpp_TypeInfo_var, L_7));
		V_3 = 0;
		goto IL_004d;
	}

IL_0040:
	{
		MaterialU5BU5D_t4* L_8 = V_2;
		int32_t L_9 = V_3;
		Material_t8 * L_10 = (((MaskOutAbstractBehaviour_t47 *)__this)->___maskMaterial_2);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, L_10);
		*((Material_t8 **)(Material_t8 **)SZArrayLdElema(L_8, L_9, sizeof(Material_t8 *))) = (Material_t8 *)L_10;
		int32_t L_11 = V_3;
		V_3 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_12 = V_3;
		int32_t L_13 = V_1;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t85 * L_14 = V_0;
		MaterialU5BU5D_t4* L_15 = V_2;
		NullCheck(L_14);
		Renderer_set_sharedMaterials_m247(L_14, L_15, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void Vuforia.MultiTargetBehaviour::.ctor()
extern "C" void MultiTargetBehaviour__ctor_m110 (MultiTargetBehaviour_t48 * __this, const MethodInfo* method)
{
	{
		MultiTargetAbstractBehaviour__ctor_m248(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
extern "C" void ObjectTargetBehaviour__ctor_m111 (ObjectTargetBehaviour_t50 * __this, const MethodInfo* method)
{
	{
		ObjectTargetAbstractBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.PropBehaviour::.ctor()
extern "C" void PropBehaviour__ctor_m112 (PropBehaviour_t29 * __this, const MethodInfo* method)
{
	{
		PropAbstractBehaviour__ctor_m250(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ReconstructionBehaviour::.ctor()
extern "C" void ReconstructionBehaviour__ctor_m113 (ReconstructionBehaviour_t28 * __this, const MethodInfo* method)
{
	{
		ReconstructionAbstractBehaviour__ctor_m251(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
extern "C" void ReconstructionFromTargetBehaviour__ctor_m114 (ReconstructionFromTargetBehaviour_t54 * __this, const MethodInfo* method)
{
	{
		ReconstructionFromTargetAbstractBehaviour__ctor_m252(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerBehaviour::.ctor()
extern "C" void SmartTerrainTrackerBehaviour__ctor_m115 (SmartTerrainTrackerBehaviour_t56 * __this, const MethodInfo* method)
{
	{
		SmartTerrainTrackerAbstractBehaviour__ctor_m253(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SurfaceBehaviour::.ctor()
extern "C" void SurfaceBehaviour__ctor_m116 (SurfaceBehaviour_t30 * __this, const MethodInfo* method)
{
	{
		SurfaceAbstractBehaviour__ctor_m254(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TextRecoBehaviour::.ctor()
extern "C" void TextRecoBehaviour__ctor_m117 (TextRecoBehaviour_t59 * __this, const MethodInfo* method)
{
	{
		TextRecoAbstractBehaviour__ctor_m255(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffBehaviour::.ctor()
extern "C" void TurnOffBehaviour__ctor_m118 (TurnOffBehaviour_t61 * __this, const MethodInfo* method)
{
	{
		TurnOffAbstractBehaviour__ctor_m256(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffBehaviour::Awake()
extern TypeInfo* VuforiaRuntimeUtilities_t112_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t113_m257_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t114_m259_MethodInfo_var;
extern "C" void TurnOffBehaviour_Awake_m119 (TurnOffBehaviour_t61 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaRuntimeUtilities_t112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Component_GetComponent_TisMeshRenderer_t113_m257_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483682);
		Component_GetComponent_TisMeshFilter_t114_m259_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483683);
		s_Il2CppMethodIntialized = true;
	}
	MeshRenderer_t113 * V_0 = {0};
	MeshFilter_t114 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t112_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m245(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		MeshRenderer_t113 * L_1 = Component_GetComponent_TisMeshRenderer_t113_m257(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t113_m257_MethodInfo_var);
		V_0 = L_1;
		MeshRenderer_t113 * L_2 = V_0;
		Object_Destroy_m258(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		MeshFilter_t114 * L_3 = Component_GetComponent_TisMeshFilter_t114_m259(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t114_m259_MethodInfo_var);
		V_1 = L_3;
		MeshFilter_t114 * L_4 = V_1;
		Object_Destroy_m258(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
extern "C" void TurnOffWordBehaviour__ctor_m120 (TurnOffWordBehaviour_t63 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m141(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
extern TypeInfo* VuforiaRuntimeUtilities_t112_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t113_m257_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral27;
extern "C" void TurnOffWordBehaviour_Awake_m121 (TurnOffWordBehaviour_t63 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaRuntimeUtilities_t112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Component_GetComponent_TisMeshRenderer_t113_m257_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483682);
		_stringLiteral27 = il2cpp_codegen_string_literal_from_index(27);
		s_Il2CppMethodIntialized = true;
	}
	MeshRenderer_t113 * V_0 = {0};
	Transform_t84 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t112_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m245(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		MeshRenderer_t113 * L_1 = Component_GetComponent_TisMeshRenderer_t113_m257(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t113_m257_MethodInfo_var);
		V_0 = L_1;
		MeshRenderer_t113 * L_2 = V_0;
		Object_Destroy_m258(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Transform_t84 * L_3 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t84 * L_4 = Transform_FindChild_m175(L_3, _stringLiteral27, /*hidden argument*/NULL);
		V_1 = L_4;
		Transform_t84 * L_5 = V_1;
		bool L_6 = Object_op_Inequality_m150(NULL /*static, unused*/, L_5, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		Transform_t84 * L_7 = V_1;
		NullCheck(L_7);
		GameObject_t9 * L_8 = Component_get_gameObject_m147(L_7, /*hidden argument*/NULL);
		Object_Destroy_m258(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
extern "C" void UserDefinedTargetBuildingBehaviour__ctor_m122 (UserDefinedTargetBuildingBehaviour_t64 * __this, const MethodInfo* method)
{
	{
		UserDefinedTargetBuildingAbstractBehaviour__ctor_m260(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
extern "C" void VideoBackgroundBehaviour__ctor_m123 (VideoBackgroundBehaviour_t66 * __this, const MethodInfo* method)
{
	{
		VideoBackgroundAbstractBehaviour__ctor_m261(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VideoTextureRenderer::.ctor()
extern "C" void VideoTextureRenderer__ctor_m124 (VideoTextureRenderer_t68 * __this, const MethodInfo* method)
{
	{
		VideoTextureRendererAbstractBehaviour__ctor_m262(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
extern "C" void VirtualButtonBehaviour__ctor_m125 (VirtualButtonBehaviour_t70 * __this, const MethodInfo* method)
{
	{
		VirtualButtonAbstractBehaviour__ctor_m263(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.ctor()
extern "C" void VuforiaBehaviour__ctor_m126 (VuforiaBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		VuforiaAbstractBehaviour__ctor_m264(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.cctor()
extern "C" void VuforiaBehaviour__cctor_m127 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::Awake()
extern TypeInfo* NullUnityPlayer_t115_il2cpp_TypeInfo_var;
extern TypeInfo* AndroidUnityPlayer_t38_il2cpp_TypeInfo_var;
extern TypeInfo* IOSUnityPlayer_t40_il2cpp_TypeInfo_var;
extern TypeInfo* VuforiaRuntimeUtilities_t112_il2cpp_TypeInfo_var;
extern TypeInfo* PlayModeUnityPlayer_t116_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t39_m270_MethodInfo_var;
extern "C" void VuforiaBehaviour_Awake_m128 (VuforiaBehaviour_t72 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullUnityPlayer_t115_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(50);
		AndroidUnityPlayer_t38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		IOSUnityPlayer_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(52);
		VuforiaRuntimeUtilities_t112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		PlayModeUnityPlayer_t116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t39_m270_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483684);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		NullUnityPlayer_t115 * L_0 = (NullUnityPlayer_t115 *)il2cpp_codegen_object_new (NullUnityPlayer_t115_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m265(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m266(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001d;
		}
	}
	{
		AndroidUnityPlayer_t38 * L_2 = (AndroidUnityPlayer_t38 *)il2cpp_codegen_object_new (AndroidUnityPlayer_t38_il2cpp_TypeInfo_var);
		AndroidUnityPlayer__ctor_m63(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0043;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m266(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0033;
		}
	}
	{
		IOSUnityPlayer_t40 * L_4 = (IOSUnityPlayer_t40 *)il2cpp_codegen_object_new (IOSUnityPlayer_t40_il2cpp_TypeInfo_var);
		IOSUnityPlayer__ctor_m81(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0043;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t112_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m267(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		PlayModeUnityPlayer_t116 * L_6 = (PlayModeUnityPlayer_t116 *)il2cpp_codegen_object_new (PlayModeUnityPlayer_t116_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m268(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0043:
	{
		Object_t * L_7 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m269(__this, L_7, /*hidden argument*/NULL);
		GameObject_t9 * L_8 = Component_get_gameObject_m147(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t39_m270(L_8, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t39_m270_MethodInfo_var);
		return;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::get_Instance()
extern TypeInfo* VuforiaBehaviour_t72_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisVuforiaBehaviour_t72_m271_MethodInfo_var;
extern "C" VuforiaBehaviour_t72 * VuforiaBehaviour_get_Instance_m129 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaBehaviour_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		Object_FindObjectOfType_TisVuforiaBehaviour_t72_m271_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483685);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t72_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t72 * L_0 = ((VuforiaBehaviour_t72_StaticFields*)VuforiaBehaviour_t72_il2cpp_TypeInfo_var->static_fields)->___mVuforiaBehaviour_48;
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		VuforiaBehaviour_t72 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t72_m271(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t72_m271_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t72_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t72_StaticFields*)VuforiaBehaviour_t72_il2cpp_TypeInfo_var->static_fields)->___mVuforiaBehaviour_48 = L_2;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t72_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t72 * L_3 = ((VuforiaBehaviour_t72_StaticFields*)VuforiaBehaviour_t72_il2cpp_TypeInfo_var->static_fields)->___mVuforiaBehaviour_48;
		return L_3;
	}
}
// System.Void Vuforia.WebCamBehaviour::.ctor()
extern "C" void WebCamBehaviour__ctor_m130 (WebCamBehaviour_t74 * __this, const MethodInfo* method)
{
	{
		WebCamAbstractBehaviour__ctor_m272(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::.ctor()
extern "C" void WireframeBehaviour__ctor_m131 (WireframeBehaviour_t76 * __this, const MethodInfo* method)
{
	{
		__this->___ShowLines_3 = 1;
		Color_t77  L_0 = Color_get_green_m273(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___LineColor_4 = L_0;
		MonoBehaviour__ctor_m141(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t117_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Material_t8_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral28;
extern Il2CppCodeGenString* _stringLiteral29;
extern Il2CppCodeGenString* _stringLiteral30;
extern "C" void WireframeBehaviour_CreateLineMaterial_m132 (WireframeBehaviour_t76 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Single_t117_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(56);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Material_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		_stringLiteral28 = il2cpp_codegen_string_literal_from_index(28);
		_stringLiteral29 = il2cpp_codegen_string_literal_from_index(29);
		_stringLiteral30 = il2cpp_codegen_string_literal_from_index(30);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ((ObjectU5BU5D_t105*)SZArrayNew(ObjectU5BU5D_t105_il2cpp_TypeInfo_var, ((int32_t)9)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral28);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral28;
		ObjectU5BU5D_t105* L_1 = L_0;
		Color_t77 * L_2 = &(__this->___LineColor_4);
		float L_3 = (L_2->___r_0);
		float L_4 = L_3;
		Object_t * L_5 = Box(Single_t117_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t105* L_6 = L_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral29;
		ObjectU5BU5D_t105* L_7 = L_6;
		Color_t77 * L_8 = &(__this->___LineColor_4);
		float L_9 = (L_8->___g_1);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t117_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t105* L_12 = L_7;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral29;
		ObjectU5BU5D_t105* L_13 = L_12;
		Color_t77 * L_14 = &(__this->___LineColor_4);
		float L_15 = (L_14->___b_2);
		float L_16 = L_15;
		Object_t * L_17 = Box(Single_t117_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 5, sizeof(Object_t *))) = (Object_t *)L_17;
		ObjectU5BU5D_t105* L_18 = L_13;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 6);
		ArrayElementTypeCheck (L_18, _stringLiteral29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral29;
		ObjectU5BU5D_t105* L_19 = L_18;
		Color_t77 * L_20 = &(__this->___LineColor_4);
		float L_21 = (L_20->___a_3);
		float L_22 = L_21;
		Object_t * L_23 = Box(Single_t117_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		ArrayElementTypeCheck (L_19, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 7, sizeof(Object_t *))) = (Object_t *)L_23;
		ObjectU5BU5D_t105* L_24 = L_19;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 8);
		ArrayElementTypeCheck (L_24, _stringLiteral30);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 8, sizeof(Object_t *))) = (Object_t *)_stringLiteral30;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m274(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Material_t8 * L_26 = (Material_t8 *)il2cpp_codegen_object_new (Material_t8_il2cpp_TypeInfo_var);
		Material__ctor_m275(L_26, L_25, /*hidden argument*/NULL);
		__this->___mLineMaterial_2 = L_26;
		Material_t8 * L_27 = (__this->___mLineMaterial_2);
		NullCheck(L_27);
		Object_set_hideFlags_m276(L_27, ((int32_t)61), /*hidden argument*/NULL);
		Material_t8 * L_28 = (__this->___mLineMaterial_2);
		NullCheck(L_28);
		Shader_t118 * L_29 = Material_get_shader_m277(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Object_set_hideFlags_m276(L_29, ((int32_t)61), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern TypeInfo* VuforiaManager_t123_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisCamera_t91_m279_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t114_m259_MethodInfo_var;
extern "C" void WireframeBehaviour_OnRenderObject_m133 (WireframeBehaviour_t76 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaManager_t123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		GameObject_GetComponentsInChildren_TisCamera_t91_m279_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483686);
		Component_GetComponent_TisMeshFilter_t114_m259_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483683);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t9 * V_0 = {0};
	CameraU5BU5D_t119* V_1 = {0};
	bool V_2 = false;
	Camera_t91 * V_3 = {0};
	CameraU5BU5D_t119* V_4 = {0};
	int32_t V_5 = 0;
	MeshFilter_t114 * V_6 = {0};
	Mesh_t120 * V_7 = {0};
	Vector3U5BU5D_t121* V_8 = {0};
	Int32U5BU5D_t122* V_9 = {0};
	int32_t V_10 = 0;
	Vector3_t12  V_11 = {0};
	Vector3_t12  V_12 = {0};
	Vector3_t12  V_13 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t123_il2cpp_TypeInfo_var);
		VuforiaManager_t123 * L_0 = VuforiaManager_get_Instance_m278(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t84 * L_1 = (Transform_t84 *)VirtFuncInvoker0< Transform_t84 * >::Invoke(8 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t9 * L_2 = Component_get_gameObject_m147(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t9 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t119* L_4 = GameObject_GetComponentsInChildren_TisCamera_t91_m279(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t91_m279_MethodInfo_var);
		V_1 = L_4;
		V_2 = 0;
		CameraU5BU5D_t119* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t119* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_3 = (*(Camera_t91 **)(Camera_t91 **)SZArrayLdElema(L_6, L_8, sizeof(Camera_t91 *)));
		Camera_t91 * L_9 = Camera_get_current_m280(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t91 * L_10 = V_3;
		bool L_11 = Object_op_Equality_m165(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = 1;
	}

IL_003c:
	{
		int32_t L_12 = V_5;
		V_5 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_13 = V_5;
		CameraU5BU5D_t119* L_14 = V_4;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_14)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_15 = V_2;
		if (L_15)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_16 = (__this->___ShowLines_3);
		if (L_16)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t114 * L_17 = Component_GetComponent_TisMeshFilter_t114_m259(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t114_m259_MethodInfo_var);
		V_6 = L_17;
		MeshFilter_t114 * L_18 = V_6;
		bool L_19 = Object_op_Implicit_m182(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t8 * L_20 = (__this->___mLineMaterial_2);
		bool L_21 = Object_op_Equality_m165(NULL /*static, unused*/, L_20, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_008c;
		}
	}
	{
		WireframeBehaviour_CreateLineMaterial_m132(__this, /*hidden argument*/NULL);
	}

IL_008c:
	{
		MeshFilter_t114 * L_22 = V_6;
		NullCheck(L_22);
		Mesh_t120 * L_23 = MeshFilter_get_sharedMesh_m281(L_22, /*hidden argument*/NULL);
		V_7 = L_23;
		Mesh_t120 * L_24 = V_7;
		NullCheck(L_24);
		Vector3U5BU5D_t121* L_25 = Mesh_get_vertices_m282(L_24, /*hidden argument*/NULL);
		V_8 = L_25;
		Mesh_t120 * L_26 = V_7;
		NullCheck(L_26);
		Int32U5BU5D_t122* L_27 = Mesh_get_triangles_m283(L_26, /*hidden argument*/NULL);
		V_9 = L_27;
		GL_PushMatrix_m284(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t84 * L_28 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Matrix4x4_t124  L_29 = Transform_get_localToWorldMatrix_m285(L_28, /*hidden argument*/NULL);
		GL_MultMatrix_m286(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		Material_t8 * L_30 = (__this->___mLineMaterial_2);
		NullCheck(L_30);
		Material_SetPass_m287(L_30, 0, /*hidden argument*/NULL);
		GL_Begin_m288(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_0144;
	}

IL_00d7:
	{
		Vector3U5BU5D_t121* L_31 = V_8;
		Int32U5BU5D_t122* L_32 = V_9;
		int32_t L_33 = V_10;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
		int32_t L_34 = L_33;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, (*(int32_t*)(int32_t*)SZArrayLdElema(L_32, L_34, sizeof(int32_t))));
		V_11 = (*(Vector3_t12 *)((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_31, (*(int32_t*)(int32_t*)SZArrayLdElema(L_32, L_34, sizeof(int32_t))), sizeof(Vector3_t12 ))));
		Vector3U5BU5D_t121* L_35 = V_8;
		Int32U5BU5D_t122* L_36 = V_9;
		int32_t L_37 = V_10;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)((int32_t)L_37+(int32_t)1)));
		int32_t L_38 = ((int32_t)((int32_t)L_37+(int32_t)1));
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, (*(int32_t*)(int32_t*)SZArrayLdElema(L_36, L_38, sizeof(int32_t))));
		V_12 = (*(Vector3_t12 *)((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_35, (*(int32_t*)(int32_t*)SZArrayLdElema(L_36, L_38, sizeof(int32_t))), sizeof(Vector3_t12 ))));
		Vector3U5BU5D_t121* L_39 = V_8;
		Int32U5BU5D_t122* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)((int32_t)L_41+(int32_t)2)));
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)2));
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, (*(int32_t*)(int32_t*)SZArrayLdElema(L_40, L_42, sizeof(int32_t))));
		V_13 = (*(Vector3_t12 *)((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_39, (*(int32_t*)(int32_t*)SZArrayLdElema(L_40, L_42, sizeof(int32_t))), sizeof(Vector3_t12 ))));
		Vector3_t12  L_43 = V_11;
		GL_Vertex_m289(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		Vector3_t12  L_44 = V_12;
		GL_Vertex_m289(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		Vector3_t12  L_45 = V_12;
		GL_Vertex_m289(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		Vector3_t12  L_46 = V_13;
		GL_Vertex_m289(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		Vector3_t12  L_47 = V_13;
		GL_Vertex_m289(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		Vector3_t12  L_48 = V_11;
		GL_Vertex_m289(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		int32_t L_49 = V_10;
		V_10 = ((int32_t)((int32_t)L_49+(int32_t)3));
	}

IL_0144:
	{
		int32_t L_50 = V_10;
		Int32U5BU5D_t122* L_51 = V_9;
		NullCheck(L_51);
		if ((((int32_t)L_50) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_51)->max_length)))))))
		{
			goto IL_00d7;
		}
	}
	{
		GL_End_m290(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m291(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t114_m259_MethodInfo_var;
extern "C" void WireframeBehaviour_OnDrawGizmos_m134 (WireframeBehaviour_t76 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisMeshFilter_t114_m259_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483683);
		s_Il2CppMethodIntialized = true;
	}
	MeshFilter_t114 * V_0 = {0};
	Mesh_t120 * V_1 = {0};
	Vector3U5BU5D_t121* V_2 = {0};
	Int32U5BU5D_t122* V_3 = {0};
	int32_t V_4 = 0;
	Vector3_t12  V_5 = {0};
	Vector3_t12  V_6 = {0};
	Vector3_t12  V_7 = {0};
	{
		bool L_0 = (__this->___ShowLines_3);
		if (!L_0)
		{
			goto IL_00ed;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m292(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00ed;
		}
	}
	{
		MeshFilter_t114 * L_2 = Component_GetComponent_TisMeshFilter_t114_m259(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t114_m259_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t114 * L_3 = V_0;
		bool L_4 = Object_op_Implicit_m182(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t9 * L_5 = Component_get_gameObject_m147(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t84 * L_6 = GameObject_get_transform_m293(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t12  L_7 = Transform_get_position_m156(L_6, /*hidden argument*/NULL);
		GameObject_t9 * L_8 = Component_get_gameObject_m147(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t84 * L_9 = GameObject_get_transform_m293(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t125  L_10 = Transform_get_rotation_m294(L_9, /*hidden argument*/NULL);
		GameObject_t9 * L_11 = Component_get_gameObject_m147(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t84 * L_12 = GameObject_get_transform_m293(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t12  L_13 = Transform_get_lossyScale_m295(L_12, /*hidden argument*/NULL);
		Matrix4x4_t124  L_14 = Matrix4x4_TRS_m296(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m297(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t77  L_15 = (__this->___LineColor_4);
		Gizmos_set_color_m298(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t114 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t120 * L_17 = MeshFilter_get_sharedMesh_m281(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t120 * L_18 = V_1;
		NullCheck(L_18);
		Vector3U5BU5D_t121* L_19 = Mesh_get_vertices_m282(L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		Mesh_t120 * L_20 = V_1;
		NullCheck(L_20);
		Int32U5BU5D_t122* L_21 = Mesh_get_triangles_m283(L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		V_4 = 0;
		goto IL_00e3;
	}

IL_008b:
	{
		Vector3U5BU5D_t121* L_22 = V_2;
		Int32U5BU5D_t122* L_23 = V_3;
		int32_t L_24 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, (*(int32_t*)(int32_t*)SZArrayLdElema(L_23, L_25, sizeof(int32_t))));
		V_5 = (*(Vector3_t12 *)((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_22, (*(int32_t*)(int32_t*)SZArrayLdElema(L_23, L_25, sizeof(int32_t))), sizeof(Vector3_t12 ))));
		Vector3U5BU5D_t121* L_26 = V_2;
		Int32U5BU5D_t122* L_27 = V_3;
		int32_t L_28 = V_4;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)L_28+(int32_t)1)));
		int32_t L_29 = ((int32_t)((int32_t)L_28+(int32_t)1));
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, (*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_29, sizeof(int32_t))));
		V_6 = (*(Vector3_t12 *)((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_26, (*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_29, sizeof(int32_t))), sizeof(Vector3_t12 ))));
		Vector3U5BU5D_t121* L_30 = V_2;
		Int32U5BU5D_t122* L_31 = V_3;
		int32_t L_32 = V_4;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)((int32_t)L_32+(int32_t)2)));
		int32_t L_33 = ((int32_t)((int32_t)L_32+(int32_t)2));
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, (*(int32_t*)(int32_t*)SZArrayLdElema(L_31, L_33, sizeof(int32_t))));
		V_7 = (*(Vector3_t12 *)((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_30, (*(int32_t*)(int32_t*)SZArrayLdElema(L_31, L_33, sizeof(int32_t))), sizeof(Vector3_t12 ))));
		Vector3_t12  L_34 = V_5;
		Vector3_t12  L_35 = V_6;
		Gizmos_DrawLine_m299(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		Vector3_t12  L_36 = V_6;
		Vector3_t12  L_37 = V_7;
		Gizmos_DrawLine_m299(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		Vector3_t12  L_38 = V_7;
		Vector3_t12  L_39 = V_5;
		Gizmos_DrawLine_m299(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		int32_t L_40 = V_4;
		V_4 = ((int32_t)((int32_t)L_40+(int32_t)3));
	}

IL_00e3:
	{
		int32_t L_41 = V_4;
		Int32U5BU5D_t122* L_42 = V_3;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_42)->max_length)))))))
		{
			goto IL_008b;
		}
	}

IL_00ed:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
extern "C" void WireframeTrackableEventHandler__ctor_m135 (WireframeTrackableEventHandler_t78 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m141(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t32_m205_MethodInfo_var;
extern "C" void WireframeTrackableEventHandler_Start_m136 (WireframeTrackableEventHandler_t78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisTrackableBehaviour_t32_m205_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483663);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableBehaviour_t32 * L_0 = Component_GetComponent_TisTrackableBehaviour_t32_m205(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t32_m205_MethodInfo_var);
		__this->___mTrackableBehaviour_2 = L_0;
		TrackableBehaviour_t32 * L_1 = (__this->___mTrackableBehaviour_2);
		bool L_2 = Object_op_Implicit_m182(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t32 * L_3 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m206(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C" void WireframeTrackableEventHandler_OnTrackableStateChanged_m137 (WireframeTrackableEventHandler_t78 * __this, int32_t ___previousStatus, int32_t ___newStatus, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___newStatus;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_OnTrackingFound_m138(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_OnTrackingLost_m139(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t85_m207_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t14_m208_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t76_m300_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern "C" void WireframeTrackableEventHandler_OnTrackingFound_m138 (WireframeTrackableEventHandler_t78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Component_GetComponentsInChildren_TisRenderer_t85_m207_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		Component_GetComponentsInChildren_TisCollider_t14_m208_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		Component_GetComponentsInChildren_TisWireframeBehaviour_t76_m300_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483687);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t99* V_0 = {0};
	ColliderU5BU5D_t100* V_1 = {0};
	WireframeBehaviourU5BU5D_t126* V_2 = {0};
	Renderer_t85 * V_3 = {0};
	RendererU5BU5D_t99* V_4 = {0};
	int32_t V_5 = 0;
	Collider_t14 * V_6 = {0};
	ColliderU5BU5D_t100* V_7 = {0};
	int32_t V_8 = 0;
	WireframeBehaviour_t76 * V_9 = {0};
	WireframeBehaviourU5BU5D_t126* V_10 = {0};
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t99* L_0 = Component_GetComponentsInChildren_TisRenderer_t85_m207(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t85_m207_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t100* L_1 = Component_GetComponentsInChildren_TisCollider_t14_m208(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t14_m208_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t126* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t76_m300(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t76_m300_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t99* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t99* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_3 = (*(Renderer_t85 **)(Renderer_t85 **)SZArrayLdElema(L_4, L_6, sizeof(Renderer_t85 *)));
		Renderer_t85 * L_7 = V_3;
		NullCheck(L_7);
		Renderer_set_enabled_m209(L_7, 1, /*hidden argument*/NULL);
		int32_t L_8 = V_5;
		V_5 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_9 = V_5;
		RendererU5BU5D_t99* L_10 = V_4;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t100* L_11 = V_1;
		V_7 = L_11;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t100* L_12 = V_7;
		int32_t L_13 = V_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_6 = (*(Collider_t14 **)(Collider_t14 **)SZArrayLdElema(L_12, L_14, sizeof(Collider_t14 *)));
		Collider_t14 * L_15 = V_6;
		NullCheck(L_15);
		Collider_set_enabled_m210(L_15, 1, /*hidden argument*/NULL);
		int32_t L_16 = V_8;
		V_8 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_17 = V_8;
		ColliderU5BU5D_t100* L_18 = V_7;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_18)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t126* L_19 = V_2;
		V_10 = L_19;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t126* L_20 = V_10;
		int32_t L_21 = V_11;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_9 = (*(WireframeBehaviour_t76 **)(WireframeBehaviour_t76 **)SZArrayLdElema(L_20, L_22, sizeof(WireframeBehaviour_t76 *)));
		WireframeBehaviour_t76 * L_23 = V_9;
		NullCheck(L_23);
		Behaviour_set_enabled_m301(L_23, 1, /*hidden argument*/NULL);
		int32_t L_24 = V_11;
		V_11 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_25 = V_11;
		WireframeBehaviourU5BU5D_t126* L_26 = V_10;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_26)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t32 * L_27 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_27);
		String_t* L_28 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m211(NULL /*static, unused*/, _stringLiteral22, L_28, _stringLiteral23, /*hidden argument*/NULL);
		Debug_Log_m212(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t85_m207_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t14_m208_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t76_m300_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral24;
extern "C" void WireframeTrackableEventHandler_OnTrackingLost_m139 (WireframeTrackableEventHandler_t78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Component_GetComponentsInChildren_TisRenderer_t85_m207_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		Component_GetComponentsInChildren_TisCollider_t14_m208_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		Component_GetComponentsInChildren_TisWireframeBehaviour_t76_m300_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483687);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral24 = il2cpp_codegen_string_literal_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t99* V_0 = {0};
	ColliderU5BU5D_t100* V_1 = {0};
	WireframeBehaviourU5BU5D_t126* V_2 = {0};
	Renderer_t85 * V_3 = {0};
	RendererU5BU5D_t99* V_4 = {0};
	int32_t V_5 = 0;
	Collider_t14 * V_6 = {0};
	ColliderU5BU5D_t100* V_7 = {0};
	int32_t V_8 = 0;
	WireframeBehaviour_t76 * V_9 = {0};
	WireframeBehaviourU5BU5D_t126* V_10 = {0};
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t99* L_0 = Component_GetComponentsInChildren_TisRenderer_t85_m207(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t85_m207_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t100* L_1 = Component_GetComponentsInChildren_TisCollider_t14_m208(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t14_m208_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t126* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t76_m300(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t76_m300_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t99* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t99* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_3 = (*(Renderer_t85 **)(Renderer_t85 **)SZArrayLdElema(L_4, L_6, sizeof(Renderer_t85 *)));
		Renderer_t85 * L_7 = V_3;
		NullCheck(L_7);
		Renderer_set_enabled_m209(L_7, 0, /*hidden argument*/NULL);
		int32_t L_8 = V_5;
		V_5 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_9 = V_5;
		RendererU5BU5D_t99* L_10 = V_4;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t100* L_11 = V_1;
		V_7 = L_11;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t100* L_12 = V_7;
		int32_t L_13 = V_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_6 = (*(Collider_t14 **)(Collider_t14 **)SZArrayLdElema(L_12, L_14, sizeof(Collider_t14 *)));
		Collider_t14 * L_15 = V_6;
		NullCheck(L_15);
		Collider_set_enabled_m210(L_15, 0, /*hidden argument*/NULL);
		int32_t L_16 = V_8;
		V_8 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_17 = V_8;
		ColliderU5BU5D_t100* L_18 = V_7;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_18)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t126* L_19 = V_2;
		V_10 = L_19;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t126* L_20 = V_10;
		int32_t L_21 = V_11;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_9 = (*(WireframeBehaviour_t76 **)(WireframeBehaviour_t76 **)SZArrayLdElema(L_20, L_22, sizeof(WireframeBehaviour_t76 *)));
		WireframeBehaviour_t76 * L_23 = V_9;
		NullCheck(L_23);
		Behaviour_set_enabled_m301(L_23, 0, /*hidden argument*/NULL);
		int32_t L_24 = V_11;
		V_11 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_25 = V_11;
		WireframeBehaviourU5BU5D_t126* L_26 = V_10;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_26)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t32 * L_27 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_27);
		String_t* L_28 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m211(NULL /*static, unused*/, _stringLiteral22, L_28, _stringLiteral24, /*hidden argument*/NULL);
		Debug_Log_m212(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordBehaviour::.ctor()
extern "C" void WordBehaviour__ctor_m140 (WordBehaviour_t79 * __this, const MethodInfo* method)
{
	{
		WordAbstractBehaviour__ctor_m302(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
