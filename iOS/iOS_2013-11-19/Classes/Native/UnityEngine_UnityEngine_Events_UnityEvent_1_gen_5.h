﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t105;

#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct  UnityEvent_1_t295  : public UnityEventBase_t663
{
	// System.Object[] UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::m_InvokeArray
	ObjectU5BU5D_t105* ___m_InvokeArray_4;
};
