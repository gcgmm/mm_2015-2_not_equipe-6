﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"

// Vuforia.IOSCamRecoveringHelper
struct  IOSCamRecoveringHelper_t729  : public Object_t
{
};
struct IOSCamRecoveringHelper_t729_StaticFields{
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sHasJustResumed
	bool ___sHasJustResumed_0;
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sCheckFailedFrameAfterResume
	bool ___sCheckFailedFrameAfterResume_1;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sCheckedFailedFrameCounter
	int32_t ___sCheckedFailedFrameCounter_2;
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sWaitToRecoverCameraRestart
	bool ___sWaitToRecoverCameraRestart_3;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sWaitedFrameRecoverCounter
	int32_t ___sWaitedFrameRecoverCounter_4;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sRecoveryAttemptCounter
	int32_t ___sRecoveryAttemptCounter_5;
};
