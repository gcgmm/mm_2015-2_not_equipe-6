﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>
struct Collection_1_t2383;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t266;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t3145;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t441;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Collection_1__ctor_m15575_gshared (Collection_1_t2383 * __this, const MethodInfo* method);
#define Collection_1__ctor_m15575(__this, method) (( void (*) (Collection_1_t2383 *, const MethodInfo*))Collection_1__ctor_m15575_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15576_gshared (Collection_1_t2383 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15576(__this, method) (( bool (*) (Collection_1_t2383 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15576_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15577_gshared (Collection_1_t2383 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m15577(__this, ___array, ___index, method) (( void (*) (Collection_1_t2383 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m15577_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m15578_gshared (Collection_1_t2383 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m15578(__this, method) (( Object_t * (*) (Collection_1_t2383 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m15578_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m15579_gshared (Collection_1_t2383 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m15579(__this, ___value, method) (( int32_t (*) (Collection_1_t2383 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m15579_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m15580_gshared (Collection_1_t2383 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m15580(__this, ___value, method) (( bool (*) (Collection_1_t2383 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m15580_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m15581_gshared (Collection_1_t2383 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m15581(__this, ___value, method) (( int32_t (*) (Collection_1_t2383 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m15581_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m15582_gshared (Collection_1_t2383 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m15582(__this, ___index, ___value, method) (( void (*) (Collection_1_t2383 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m15582_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m15583_gshared (Collection_1_t2383 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m15583(__this, ___value, method) (( void (*) (Collection_1_t2383 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m15583_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m15584_gshared (Collection_1_t2383 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m15584(__this, method) (( bool (*) (Collection_1_t2383 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m15584_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m15585_gshared (Collection_1_t2383 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m15585(__this, method) (( Object_t * (*) (Collection_1_t2383 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m15585_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m15586_gshared (Collection_1_t2383 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m15586(__this, method) (( bool (*) (Collection_1_t2383 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m15586_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m15587_gshared (Collection_1_t2383 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m15587(__this, method) (( bool (*) (Collection_1_t2383 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m15587_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m15588_gshared (Collection_1_t2383 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m15588(__this, ___index, method) (( Object_t * (*) (Collection_1_t2383 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m15588_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m15589_gshared (Collection_1_t2383 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m15589(__this, ___index, ___value, method) (( void (*) (Collection_1_t2383 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m15589_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Add(T)
extern "C" void Collection_1_Add_m15590_gshared (Collection_1_t2383 * __this, UIVertex_t272  ___item, const MethodInfo* method);
#define Collection_1_Add_m15590(__this, ___item, method) (( void (*) (Collection_1_t2383 *, UIVertex_t272 , const MethodInfo*))Collection_1_Add_m15590_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Clear()
extern "C" void Collection_1_Clear_m15591_gshared (Collection_1_t2383 * __this, const MethodInfo* method);
#define Collection_1_Clear_m15591(__this, method) (( void (*) (Collection_1_t2383 *, const MethodInfo*))Collection_1_Clear_m15591_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ClearItems()
extern "C" void Collection_1_ClearItems_m15592_gshared (Collection_1_t2383 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m15592(__this, method) (( void (*) (Collection_1_t2383 *, const MethodInfo*))Collection_1_ClearItems_m15592_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool Collection_1_Contains_m15593_gshared (Collection_1_t2383 * __this, UIVertex_t272  ___item, const MethodInfo* method);
#define Collection_1_Contains_m15593(__this, ___item, method) (( bool (*) (Collection_1_t2383 *, UIVertex_t272 , const MethodInfo*))Collection_1_Contains_m15593_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m15594_gshared (Collection_1_t2383 * __this, UIVertexU5BU5D_t266* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m15594(__this, ___array, ___index, method) (( void (*) (Collection_1_t2383 *, UIVertexU5BU5D_t266*, int32_t, const MethodInfo*))Collection_1_CopyTo_m15594_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m15595_gshared (Collection_1_t2383 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m15595(__this, method) (( Object_t* (*) (Collection_1_t2383 *, const MethodInfo*))Collection_1_GetEnumerator_m15595_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m15596_gshared (Collection_1_t2383 * __this, UIVertex_t272  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m15596(__this, ___item, method) (( int32_t (*) (Collection_1_t2383 *, UIVertex_t272 , const MethodInfo*))Collection_1_IndexOf_m15596_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m15597_gshared (Collection_1_t2383 * __this, int32_t ___index, UIVertex_t272  ___item, const MethodInfo* method);
#define Collection_1_Insert_m15597(__this, ___index, ___item, method) (( void (*) (Collection_1_t2383 *, int32_t, UIVertex_t272 , const MethodInfo*))Collection_1_Insert_m15597_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m15598_gshared (Collection_1_t2383 * __this, int32_t ___index, UIVertex_t272  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m15598(__this, ___index, ___item, method) (( void (*) (Collection_1_t2383 *, int32_t, UIVertex_t272 , const MethodInfo*))Collection_1_InsertItem_m15598_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool Collection_1_Remove_m15599_gshared (Collection_1_t2383 * __this, UIVertex_t272  ___item, const MethodInfo* method);
#define Collection_1_Remove_m15599(__this, ___item, method) (( bool (*) (Collection_1_t2383 *, UIVertex_t272 , const MethodInfo*))Collection_1_Remove_m15599_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m15600_gshared (Collection_1_t2383 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m15600(__this, ___index, method) (( void (*) (Collection_1_t2383 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m15600_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m15601_gshared (Collection_1_t2383 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m15601(__this, ___index, method) (( void (*) (Collection_1_t2383 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m15601_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t Collection_1_get_Count_m15602_gshared (Collection_1_t2383 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m15602(__this, method) (( int32_t (*) (Collection_1_t2383 *, const MethodInfo*))Collection_1_get_Count_m15602_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t272  Collection_1_get_Item_m15603_gshared (Collection_1_t2383 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m15603(__this, ___index, method) (( UIVertex_t272  (*) (Collection_1_t2383 *, int32_t, const MethodInfo*))Collection_1_get_Item_m15603_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m15604_gshared (Collection_1_t2383 * __this, int32_t ___index, UIVertex_t272  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m15604(__this, ___index, ___value, method) (( void (*) (Collection_1_t2383 *, int32_t, UIVertex_t272 , const MethodInfo*))Collection_1_set_Item_m15604_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m15605_gshared (Collection_1_t2383 * __this, int32_t ___index, UIVertex_t272  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m15605(__this, ___index, ___item, method) (( void (*) (Collection_1_t2383 *, int32_t, UIVertex_t272 , const MethodInfo*))Collection_1_SetItem_m15605_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m15606_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m15606(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m15606_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ConvertItem(System.Object)
extern "C" UIVertex_t272  Collection_1_ConvertItem_m15607_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m15607(__this /* static, unused */, ___item, method) (( UIVertex_t272  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m15607_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m15608_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m15608(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m15608_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m15609_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m15609(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m15609_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m15610_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m15610(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m15610_gshared)(__this /* static, unused */, ___list, method)
