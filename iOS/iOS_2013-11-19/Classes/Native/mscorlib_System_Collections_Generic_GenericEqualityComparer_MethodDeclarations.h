﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>
struct GenericEqualityComparer_1_t2162;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m12772_gshared (GenericEqualityComparer_1_t2162 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m12772(__this, method) (( void (*) (GenericEqualityComparer_1_t2162 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m12772_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m25479_gshared (GenericEqualityComparer_1_t2162 * __this, DateTime_t577  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m25479(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2162 *, DateTime_t577 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m25479_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m25480_gshared (GenericEqualityComparer_1_t2162 * __this, DateTime_t577  ___x, DateTime_t577  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m25480(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2162 *, DateTime_t577 , DateTime_t577 , const MethodInfo*))GenericEqualityComparer_1_Equals_m25480_gshared)(__this, ___x, ___y, method)
