﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct DefaultComparer_t2909;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor()
extern "C" void DefaultComparer__ctor_m23212_gshared (DefaultComparer_t2909 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m23212(__this, method) (( void (*) (DefaultComparer_t2909 *, const MethodInfo*))DefaultComparer__ctor_m23212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/VirtualButtonData>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m23213_gshared (DefaultComparer_t2909 * __this, VirtualButtonData_t811  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m23213(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2909 *, VirtualButtonData_t811 , const MethodInfo*))DefaultComparer_GetHashCode_m23213_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/VirtualButtonData>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m23214_gshared (DefaultComparer_t2909 * __this, VirtualButtonData_t811  ___x, VirtualButtonData_t811  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m23214(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2909 *, VirtualButtonData_t811 , VirtualButtonData_t811 , const MethodInfo*))DefaultComparer_Equals_m23214_gshared)(__this, ___x, ___y, method)
