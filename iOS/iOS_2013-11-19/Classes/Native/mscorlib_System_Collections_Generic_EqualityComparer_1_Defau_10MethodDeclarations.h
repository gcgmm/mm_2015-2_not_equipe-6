﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>
struct DefaultComparer_t2721;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C" void DefaultComparer__ctor_m20038_gshared (DefaultComparer_t2721 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m20038(__this, method) (( void (*) (DefaultComparer_t2721 *, const MethodInfo*))DefaultComparer__ctor_m20038_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m20039_gshared (DefaultComparer_t2721 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m20039(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2721 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m20039_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m20040_gshared (DefaultComparer_t2721 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m20040(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2721 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m20040_gshared)(__this, ___x, ___y, method)
