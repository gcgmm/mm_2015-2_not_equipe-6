﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t755;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t53;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct  ReconstructionFromTargetAbstractBehaviour_t55  : public MonoBehaviour_t2
{
	// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::mReconstructionFromTarget
	Object_t * ___mReconstructionFromTarget_2;
	// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::mReconstructionBehaviour
	ReconstructionAbstractBehaviour_t53 * ___mReconstructionBehaviour_3;
};
