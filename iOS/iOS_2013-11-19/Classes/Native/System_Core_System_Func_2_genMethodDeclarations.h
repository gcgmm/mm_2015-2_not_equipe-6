﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen_1MethodDeclarations.h"

// System.Void System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2249(__this, ___object, ___method, method) (( void (*) (Func_2_t315 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m16693_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m16694(__this, ___arg1, method) (( bool (*) (Func_2_t315 *, Toggle_t213 *, const MethodInfo*))Func_2_Invoke_m16695_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m16696(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t315 *, Toggle_t213 *, AsyncCallback_t261 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m16697_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m16698(__this, ___result, method) (( bool (*) (Func_2_t315 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m16699_gshared)(__this, ___result, method)
