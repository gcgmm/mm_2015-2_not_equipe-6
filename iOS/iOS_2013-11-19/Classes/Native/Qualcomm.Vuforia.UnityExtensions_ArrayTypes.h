﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// Vuforia.VideoBackgroundAbstractBehaviour[]
// Vuforia.VideoBackgroundAbstractBehaviour[]
struct VideoBackgroundAbstractBehaviourU5BU5D_t2676  : public Array_t { };
// Vuforia.ITrackableEventHandler[]
// Vuforia.ITrackableEventHandler[]
struct ITrackableEventHandlerU5BU5D_t2682  : public Array_t { };
// Vuforia.ICloudRecoEventHandler[]
// Vuforia.ICloudRecoEventHandler[]
struct ICloudRecoEventHandlerU5BU5D_t2687  : public Array_t { };
// Vuforia.TargetFinder/TargetSearchResult[]
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t2914  : public Array_t { };
// Vuforia.HideExcessAreaAbstractBehaviour[]
// Vuforia.HideExcessAreaAbstractBehaviour[]
struct HideExcessAreaAbstractBehaviourU5BU5D_t747  : public Array_t { };
// Vuforia.Image/PIXEL_FORMAT[]
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t2702  : public Array_t { };
// Vuforia.Image[]
// Vuforia.Image[]
struct ImageU5BU5D_t2703  : public Array_t { };
// Vuforia.Trackable[]
// Vuforia.Trackable[]
struct TrackableU5BU5D_t2733  : public Array_t { };
// Vuforia.VirtualButton[]
// Vuforia.VirtualButton[]
struct VirtualButtonU5BU5D_t2743  : public Array_t { };
// Vuforia.DataSetImpl[]
// Vuforia.DataSetImpl[]
struct DataSetImplU5BU5D_t2748  : public Array_t { };
// Vuforia.DataSet[]
// Vuforia.DataSet[]
struct DataSetU5BU5D_t2753  : public Array_t { };
// Vuforia.Marker[]
// Vuforia.Marker[]
struct MarkerU5BU5D_t2760  : public Array_t { };
// Vuforia.VuforiaManagerImpl/TrackableResultData[]
// Vuforia.VuforiaManagerImpl/TrackableResultData[]
struct TrackableResultDataU5BU5D_t824  : public Array_t { };
// Vuforia.VuforiaManagerImpl/WordData[]
// Vuforia.VuforiaManagerImpl/WordData[]
struct WordDataU5BU5D_t825  : public Array_t { };
// Vuforia.VuforiaManagerImpl/WordResultData[]
// Vuforia.VuforiaManagerImpl/WordResultData[]
struct WordResultDataU5BU5D_t826  : public Array_t { };
// Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData[]
// Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData[]
struct SmartTerrainRevisionDataU5BU5D_t927  : public Array_t { };
// Vuforia.VuforiaManagerImpl/SurfaceData[]
// Vuforia.VuforiaManagerImpl/SurfaceData[]
struct SurfaceDataU5BU5D_t928  : public Array_t { };
// Vuforia.VuforiaManagerImpl/PropData[]
// Vuforia.VuforiaManagerImpl/PropData[]
struct PropDataU5BU5D_t929  : public Array_t { };
// Vuforia.TrackableBehaviour[]
// Vuforia.TrackableBehaviour[]
struct TrackableBehaviourU5BU5D_t979  : public Array_t { };
// Vuforia.IEditorTrackableBehaviour[]
// Vuforia.IEditorTrackableBehaviour[]
struct IEditorTrackableBehaviourU5BU5D_t3333  : public Array_t { };
// Vuforia.SmartTerrainTrackable[]
// Vuforia.SmartTerrainTrackable[]
struct SmartTerrainTrackableU5BU5D_t2777  : public Array_t { };
// Vuforia.ReconstructionAbstractBehaviour[]
// Vuforia.ReconstructionAbstractBehaviour[]
struct ReconstructionAbstractBehaviourU5BU5D_t984  : public Array_t { };
// Vuforia.SmartTerrainTrackableBehaviour[]
// Vuforia.SmartTerrainTrackableBehaviour[]
struct SmartTerrainTrackableBehaviourU5BU5D_t985  : public Array_t { };
// Vuforia.WordResult[]
// Vuforia.WordResult[]
struct WordResultU5BU5D_t2811  : public Array_t { };
// Vuforia.Word[]
// Vuforia.Word[]
struct WordU5BU5D_t2820  : public Array_t { };
// Vuforia.WordAbstractBehaviour[]
// Vuforia.WordAbstractBehaviour[]
struct WordAbstractBehaviourU5BU5D_t991  : public Array_t { };
// Vuforia.ILoadLevelEventHandler[]
// Vuforia.ILoadLevelEventHandler[]
struct ILoadLevelEventHandlerU5BU5D_t2835  : public Array_t { };
// Vuforia.ISmartTerrainEventHandler[]
// Vuforia.ISmartTerrainEventHandler[]
struct ISmartTerrainEventHandlerU5BU5D_t2845  : public Array_t { };
// Vuforia.Surface[]
// Vuforia.Surface[]
struct SurfaceU5BU5D_t2850  : public Array_t { };
// Vuforia.SurfaceAbstractBehaviour[]
// Vuforia.SurfaceAbstractBehaviour[]
struct SurfaceAbstractBehaviourU5BU5D_t2852  : public Array_t { };
// Vuforia.Prop[]
// Vuforia.Prop[]
struct PropU5BU5D_t2857  : public Array_t { };
// Vuforia.PropAbstractBehaviour[]
// Vuforia.PropAbstractBehaviour[]
struct PropAbstractBehaviourU5BU5D_t1027  : public Array_t { };
// Vuforia.MarkerAbstractBehaviour[]
// Vuforia.MarkerAbstractBehaviour[]
struct MarkerAbstractBehaviourU5BU5D_t1029  : public Array_t { };
// Vuforia.IEditorMarkerBehaviour[]
// Vuforia.IEditorMarkerBehaviour[]
struct IEditorMarkerBehaviourU5BU5D_t3334  : public Array_t { };
// Vuforia.WorldCenterTrackableBehaviour[]
// Vuforia.WorldCenterTrackableBehaviour[]
struct WorldCenterTrackableBehaviourU5BU5D_t3335  : public Array_t { };
// Vuforia.DataSetTrackableBehaviour[]
// Vuforia.DataSetTrackableBehaviour[]
struct DataSetTrackableBehaviourU5BU5D_t1031  : public Array_t { };
// Vuforia.IEditorDataSetTrackableBehaviour[]
// Vuforia.IEditorDataSetTrackableBehaviour[]
struct IEditorDataSetTrackableBehaviourU5BU5D_t3336  : public Array_t { };
// Vuforia.VirtualButtonAbstractBehaviour[]
// Vuforia.VirtualButtonAbstractBehaviour[]
struct VirtualButtonAbstractBehaviourU5BU5D_t942  : public Array_t { };
// Vuforia.IEditorVirtualButtonBehaviour[]
// Vuforia.IEditorVirtualButtonBehaviour[]
struct IEditorVirtualButtonBehaviourU5BU5D_t3337  : public Array_t { };
// Vuforia.VuforiaManagerImpl/VirtualButtonData[]
// Vuforia.VuforiaManagerImpl/VirtualButtonData[]
struct VirtualButtonDataU5BU5D_t2894  : public Array_t { };
// Vuforia.ImageTarget[]
// Vuforia.ImageTarget[]
struct ImageTargetU5BU5D_t2925  : public Array_t { };
// Vuforia.WebCamProfile/ProfileData[]
// Vuforia.WebCamProfile/ProfileData[]
struct ProfileDataU5BU5D_t2931  : public Array_t { };
// Vuforia.ITrackerEventHandler[]
// Vuforia.ITrackerEventHandler[]
struct ITrackerEventHandlerU5BU5D_t2957  : public Array_t { };
// Vuforia.IVideoBackgroundEventHandler[]
// Vuforia.IVideoBackgroundEventHandler[]
struct IVideoBackgroundEventHandlerU5BU5D_t2962  : public Array_t { };
// Vuforia.BackgroundPlaneAbstractBehaviour[]
// Vuforia.BackgroundPlaneAbstractBehaviour[]
struct BackgroundPlaneAbstractBehaviourU5BU5D_t1047  : public Array_t { };
// Vuforia.ITextRecoEventHandler[]
// Vuforia.ITextRecoEventHandler[]
struct ITextRecoEventHandlerU5BU5D_t2967  : public Array_t { };
// Vuforia.IUserDefinedTargetEventHandler[]
// Vuforia.IUserDefinedTargetEventHandler[]
struct IUserDefinedTargetEventHandlerU5BU5D_t2972  : public Array_t { };
// Vuforia.IVirtualButtonEventHandler[]
// Vuforia.IVirtualButtonEventHandler[]
struct IVirtualButtonEventHandlerU5BU5D_t2987  : public Array_t { };
