﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m20078(__this, ___dictionary, method) (( void (*) (Enumerator_t2724 *, Dictionary_2_t778 *, const MethodInfo*))Enumerator__ctor_m19976_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20079(__this, method) (( Object_t * (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19977_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m20080(__this, method) (( void (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m19978_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20081(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19979_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20082(__this, method) (( Object_t * (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19980_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20083(__this, method) (( Object_t * (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19981_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::MoveNext()
#define Enumerator_MoveNext_m20084(__this, method) (( bool (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_MoveNext_m19982_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Current()
#define Enumerator_get_Current_m20085(__this, method) (( KeyValuePair_2_t2722  (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_get_Current_m19983_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m20086(__this, method) (( int32_t (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_get_CurrentKey_m19984_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m20087(__this, method) (( Image_t784 * (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_get_CurrentValue_m19985_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Reset()
#define Enumerator_Reset_m20088(__this, method) (( void (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_Reset_m19986_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::VerifyState()
#define Enumerator_VerifyState_m20089(__this, method) (( void (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_VerifyState_m19987_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m20090(__this, method) (( void (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_VerifyCurrent_m19988_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Dispose()
#define Enumerator_Dispose_m20091(__this, method) (( void (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_Dispose_m19989_gshared)(__this, method)
