﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_78.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_32.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m23640_gshared (InternalEnumerator_1_t2936 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m23640(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2936 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m23640_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23641_gshared (InternalEnumerator_1_t2936 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23641(__this, method) (( void (*) (InternalEnumerator_1_t2936 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m23641_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23642_gshared (InternalEnumerator_1_t2936 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23642(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2936 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23642_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m23643_gshared (InternalEnumerator_1_t2936 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m23643(__this, method) (( void (*) (InternalEnumerator_1_t2936 *, const MethodInfo*))InternalEnumerator_1_Dispose_m23643_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m23644_gshared (InternalEnumerator_1_t2936 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m23644(__this, method) (( bool (*) (InternalEnumerator_1_t2936 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m23644_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::get_Current()
extern "C" KeyValuePair_2_t2935  InternalEnumerator_1_get_Current_m23645_gshared (InternalEnumerator_1_t2936 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m23645(__this, method) (( KeyValuePair_2_t2935  (*) (InternalEnumerator_1_t2936 *, const MethodInfo*))InternalEnumerator_1_get_Current_m23645_gshared)(__this, method)
