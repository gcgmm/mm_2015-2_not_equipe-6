﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t105;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.ChannelInfo
struct  ChannelInfo_t1765  : public Object_t
{
	// System.Object[] System.Runtime.Remoting.ChannelInfo::channelData
	ObjectU5BU5D_t105* ___channelData_0;
};
