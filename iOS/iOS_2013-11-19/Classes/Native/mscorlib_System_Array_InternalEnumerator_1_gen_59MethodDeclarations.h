﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_59.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19950_gshared (InternalEnumerator_1_t2710 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19950(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2710 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19950_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19951_gshared (InternalEnumerator_1_t2710 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19951(__this, method) (( void (*) (InternalEnumerator_1_t2710 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19951_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19952_gshared (InternalEnumerator_1_t2710 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19952(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2710 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19952_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19953_gshared (InternalEnumerator_1_t2710 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19953(__this, method) (( void (*) (InternalEnumerator_1_t2710 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19953_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19954_gshared (InternalEnumerator_1_t2710 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19954(__this, method) (( bool (*) (InternalEnumerator_1_t2710 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19954_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m19955_gshared (InternalEnumerator_1_t2710 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19955(__this, method) (( int32_t (*) (InternalEnumerator_1_t2710 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19955_gshared)(__this, method)
