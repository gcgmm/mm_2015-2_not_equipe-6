﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.ITrackableEventHandler[]
struct ITrackableEventHandlerU5BU5D_t2682;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct  List_1_t736  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::_items
	ITrackableEventHandlerU5BU5D_t2682* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t736_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::EmptyArray
	ITrackableEventHandlerU5BU5D_t2682* ___EmptyArray_4;
};
