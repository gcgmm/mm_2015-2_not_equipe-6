﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void CacheIndex_t511_marshal(const CacheIndex_t511& unmarshaled, CacheIndex_t511_marshaled& marshaled);
extern "C" void CacheIndex_t511_marshal_back(const CacheIndex_t511_marshaled& marshaled, CacheIndex_t511& unmarshaled);
extern "C" void CacheIndex_t511_marshal_cleanup(CacheIndex_t511_marshaled& marshaled);
