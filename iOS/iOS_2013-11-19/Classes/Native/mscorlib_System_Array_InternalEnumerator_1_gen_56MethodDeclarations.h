﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_56.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19657_gshared (InternalEnumerator_1_t2692 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19657(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2692 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19657_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19658_gshared (InternalEnumerator_1_t2692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19658(__this, method) (( void (*) (InternalEnumerator_1_t2692 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19658_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19659_gshared (InternalEnumerator_1_t2692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19659(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2692 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19659_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19660_gshared (InternalEnumerator_1_t2692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19660(__this, method) (( void (*) (InternalEnumerator_1_t2692 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19660_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19661_gshared (InternalEnumerator_1_t2692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19661(__this, method) (( bool (*) (InternalEnumerator_1_t2692 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19661_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern "C" TargetSearchResult_t884  InternalEnumerator_1_get_Current_m19662_gshared (InternalEnumerator_1_t2692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19662(__this, method) (( TargetSearchResult_t884  (*) (InternalEnumerator_1_t2692 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19662_gshared)(__this, method)
