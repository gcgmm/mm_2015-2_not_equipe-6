﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.BackgroundPlaneAbstractBehaviour
struct BackgroundPlaneAbstractBehaviour_t19;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::get_NumDivisions()
extern "C" int32_t BackgroundPlaneAbstractBehaviour_get_NumDivisions_m3652 (BackgroundPlaneAbstractBehaviour_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::set_NumDivisions(System.Int32)
extern "C" void BackgroundPlaneAbstractBehaviour_set_NumDivisions_m3653 (BackgroundPlaneAbstractBehaviour_t19 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetEditorValues(System.Int32)
extern "C" void BackgroundPlaneAbstractBehaviour_SetEditorValues_m3654 (BackgroundPlaneAbstractBehaviour_t19 * __this, int32_t ___numDivisions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::CheckNumDivisions()
extern "C" bool BackgroundPlaneAbstractBehaviour_CheckNumDivisions_m3655 (BackgroundPlaneAbstractBehaviour_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetStereoDepth(System.Single)
extern "C" void BackgroundPlaneAbstractBehaviour_SetStereoDepth_m3656 (BackgroundPlaneAbstractBehaviour_t19 * __this, float ___depth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Start()
extern "C" void BackgroundPlaneAbstractBehaviour_Start_m3657 (BackgroundPlaneAbstractBehaviour_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Update()
extern "C" void BackgroundPlaneAbstractBehaviour_Update_m3658 (BackgroundPlaneAbstractBehaviour_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Vuforia.BackgroundPlaneAbstractBehaviour::get_DefaultRotationTowardsCamera()
extern "C" Quaternion_t125  BackgroundPlaneAbstractBehaviour_get_DefaultRotationTowardsCamera_m3659 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::CreateAndSetVideoMesh()
extern "C" void BackgroundPlaneAbstractBehaviour_CreateAndSetVideoMesh_m3660 (BackgroundPlaneAbstractBehaviour_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::PositionVideoMesh()
extern "C" void BackgroundPlaneAbstractBehaviour_PositionVideoMesh_m3661 (BackgroundPlaneAbstractBehaviour_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::ShouldFitWidth()
extern "C" bool BackgroundPlaneAbstractBehaviour_ShouldFitWidth_m3662 (BackgroundPlaneAbstractBehaviour_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::OnBackgroundTextureChanged()
extern "C" void BackgroundPlaneAbstractBehaviour_OnBackgroundTextureChanged_m3663 (BackgroundPlaneAbstractBehaviour_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern "C" void BackgroundPlaneAbstractBehaviour_OnVideoBackgroundConfigChanged_m3664 (BackgroundPlaneAbstractBehaviour_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.ctor()
extern "C" void BackgroundPlaneAbstractBehaviour__ctor_m176 (BackgroundPlaneAbstractBehaviour_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.cctor()
extern "C" void BackgroundPlaneAbstractBehaviour__cctor_m3665 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
