﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.HideExcessAreaBehaviour
struct HideExcessAreaBehaviour_t34;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern "C" void HideExcessAreaBehaviour__ctor_m61 (HideExcessAreaBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
