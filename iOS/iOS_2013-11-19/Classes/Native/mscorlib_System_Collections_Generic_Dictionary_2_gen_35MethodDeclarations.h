﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t2933;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2311;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t687;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t3248;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
struct IEnumerator_1_t3249;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1238;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Vuforia.WebCamProfile/ProfileData>
struct KeyCollection_t2938;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>
struct ValueCollection_t2942;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_32.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__32.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor()
extern "C" void Dictionary_2__ctor_m23549_gshared (Dictionary_2_t2933 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m23549(__this, method) (( void (*) (Dictionary_2_t2933 *, const MethodInfo*))Dictionary_2__ctor_m23549_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m23551_gshared (Dictionary_2_t2933 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m23551(__this, ___comparer, method) (( void (*) (Dictionary_2_t2933 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m23551_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m23553_gshared (Dictionary_2_t2933 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m23553(__this, ___capacity, method) (( void (*) (Dictionary_2_t2933 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m23553_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m23555_gshared (Dictionary_2_t2933 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m23555(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2933 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2__ctor_m23555_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m23557_gshared (Dictionary_2_t2933 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m23557(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2933 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m23557_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m23559_gshared (Dictionary_2_t2933 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m23559(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2933 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m23559_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m23561_gshared (Dictionary_2_t2933 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m23561(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2933 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m23561_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m23563_gshared (Dictionary_2_t2933 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m23563(__this, ___key, method) (( bool (*) (Dictionary_2_t2933 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m23563_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m23565_gshared (Dictionary_2_t2933 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m23565(__this, ___key, method) (( void (*) (Dictionary_2_t2933 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m23565_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m23567_gshared (Dictionary_2_t2933 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m23567(__this, method) (( bool (*) (Dictionary_2_t2933 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m23567_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m23569_gshared (Dictionary_2_t2933 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m23569(__this, method) (( Object_t * (*) (Dictionary_2_t2933 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m23569_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m23571_gshared (Dictionary_2_t2933 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m23571(__this, method) (( bool (*) (Dictionary_2_t2933 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m23571_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m23573_gshared (Dictionary_2_t2933 * __this, KeyValuePair_2_t2935  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m23573(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2933 *, KeyValuePair_2_t2935 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m23573_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m23575_gshared (Dictionary_2_t2933 * __this, KeyValuePair_2_t2935  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m23575(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2933 *, KeyValuePair_2_t2935 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m23575_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m23577_gshared (Dictionary_2_t2933 * __this, KeyValuePair_2U5BU5D_t3248* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m23577(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2933 *, KeyValuePair_2U5BU5D_t3248*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m23577_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m23579_gshared (Dictionary_2_t2933 * __this, KeyValuePair_2_t2935  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m23579(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2933 *, KeyValuePair_2_t2935 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m23579_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m23581_gshared (Dictionary_2_t2933 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m23581(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2933 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m23581_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m23583_gshared (Dictionary_2_t2933 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m23583(__this, method) (( Object_t * (*) (Dictionary_2_t2933 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m23583_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m23585_gshared (Dictionary_2_t2933 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m23585(__this, method) (( Object_t* (*) (Dictionary_2_t2933 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m23585_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m23587_gshared (Dictionary_2_t2933 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m23587(__this, method) (( Object_t * (*) (Dictionary_2_t2933 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m23587_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m23589_gshared (Dictionary_2_t2933 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m23589(__this, method) (( int32_t (*) (Dictionary_2_t2933 *, const MethodInfo*))Dictionary_2_get_Count_m23589_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Item(TKey)
extern "C" ProfileData_t898  Dictionary_2_get_Item_m23591_gshared (Dictionary_2_t2933 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m23591(__this, ___key, method) (( ProfileData_t898  (*) (Dictionary_2_t2933 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m23591_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m23593_gshared (Dictionary_2_t2933 * __this, Object_t * ___key, ProfileData_t898  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m23593(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2933 *, Object_t *, ProfileData_t898 , const MethodInfo*))Dictionary_2_set_Item_m23593_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m23595_gshared (Dictionary_2_t2933 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m23595(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2933 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m23595_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m23597_gshared (Dictionary_2_t2933 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m23597(__this, ___size, method) (( void (*) (Dictionary_2_t2933 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m23597_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m23599_gshared (Dictionary_2_t2933 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m23599(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2933 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m23599_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2935  Dictionary_2_make_pair_m23601_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t898  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m23601(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2935  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t898 , const MethodInfo*))Dictionary_2_make_pair_m23601_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m23603_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t898  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m23603(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t898 , const MethodInfo*))Dictionary_2_pick_key_m23603_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::pick_value(TKey,TValue)
extern "C" ProfileData_t898  Dictionary_2_pick_value_m23605_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t898  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m23605(__this /* static, unused */, ___key, ___value, method) (( ProfileData_t898  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t898 , const MethodInfo*))Dictionary_2_pick_value_m23605_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m23607_gshared (Dictionary_2_t2933 * __this, KeyValuePair_2U5BU5D_t3248* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m23607(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2933 *, KeyValuePair_2U5BU5D_t3248*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m23607_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Resize()
extern "C" void Dictionary_2_Resize_m23609_gshared (Dictionary_2_t2933 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m23609(__this, method) (( void (*) (Dictionary_2_t2933 *, const MethodInfo*))Dictionary_2_Resize_m23609_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m23611_gshared (Dictionary_2_t2933 * __this, Object_t * ___key, ProfileData_t898  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m23611(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2933 *, Object_t *, ProfileData_t898 , const MethodInfo*))Dictionary_2_Add_m23611_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Clear()
extern "C" void Dictionary_2_Clear_m23613_gshared (Dictionary_2_t2933 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m23613(__this, method) (( void (*) (Dictionary_2_t2933 *, const MethodInfo*))Dictionary_2_Clear_m23613_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m23615_gshared (Dictionary_2_t2933 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m23615(__this, ___key, method) (( bool (*) (Dictionary_2_t2933 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m23615_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m23617_gshared (Dictionary_2_t2933 * __this, ProfileData_t898  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m23617(__this, ___value, method) (( bool (*) (Dictionary_2_t2933 *, ProfileData_t898 , const MethodInfo*))Dictionary_2_ContainsValue_m23617_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m23619_gshared (Dictionary_2_t2933 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m23619(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2933 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2_GetObjectData_m23619_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m23621_gshared (Dictionary_2_t2933 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m23621(__this, ___sender, method) (( void (*) (Dictionary_2_t2933 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m23621_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m23623_gshared (Dictionary_2_t2933 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m23623(__this, ___key, method) (( bool (*) (Dictionary_2_t2933 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m23623_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m23625_gshared (Dictionary_2_t2933 * __this, Object_t * ___key, ProfileData_t898 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m23625(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2933 *, Object_t *, ProfileData_t898 *, const MethodInfo*))Dictionary_2_TryGetValue_m23625_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Keys()
extern "C" KeyCollection_t2938 * Dictionary_2_get_Keys_m23627_gshared (Dictionary_2_t2933 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m23627(__this, method) (( KeyCollection_t2938 * (*) (Dictionary_2_t2933 *, const MethodInfo*))Dictionary_2_get_Keys_m23627_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Values()
extern "C" ValueCollection_t2942 * Dictionary_2_get_Values_m23629_gshared (Dictionary_2_t2933 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m23629(__this, method) (( ValueCollection_t2942 * (*) (Dictionary_2_t2933 *, const MethodInfo*))Dictionary_2_get_Values_m23629_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m23631_gshared (Dictionary_2_t2933 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m23631(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2933 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m23631_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToTValue(System.Object)
extern "C" ProfileData_t898  Dictionary_2_ToTValue_m23633_gshared (Dictionary_2_t2933 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m23633(__this, ___value, method) (( ProfileData_t898  (*) (Dictionary_2_t2933 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m23633_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m23635_gshared (Dictionary_2_t2933 * __this, KeyValuePair_2_t2935  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m23635(__this, ___pair, method) (( bool (*) (Dictionary_2_t2933 *, KeyValuePair_2_t2935 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m23635_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::GetEnumerator()
extern "C" Enumerator_t2940  Dictionary_2_GetEnumerator_m23637_gshared (Dictionary_2_t2933 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m23637(__this, method) (( Enumerator_t2940  (*) (Dictionary_2_t2933 *, const MethodInfo*))Dictionary_2_GetEnumerator_m23637_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1423  Dictionary_2_U3CCopyToU3Em__0_m23639_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t898  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m23639(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1423  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t898 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m23639_gshared)(__this /* static, unused */, ___key, ___value, method)
