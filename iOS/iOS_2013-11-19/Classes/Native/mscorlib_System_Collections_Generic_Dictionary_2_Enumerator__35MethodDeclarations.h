﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3003;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__35.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_35.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m24609_gshared (Enumerator_t3010 * __this, Dictionary_2_t3003 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m24609(__this, ___dictionary, method) (( void (*) (Enumerator_t3010 *, Dictionary_2_t3003 *, const MethodInfo*))Enumerator__ctor_m24609_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m24610_gshared (Enumerator_t3010 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m24610(__this, method) (( Object_t * (*) (Enumerator_t3010 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24610_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m24611_gshared (Enumerator_t3010 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m24611(__this, method) (( void (*) (Enumerator_t3010 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m24611_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1423  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24612_gshared (Enumerator_t3010 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24612(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t3010 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24612_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24613_gshared (Enumerator_t3010 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24613(__this, method) (( Object_t * (*) (Enumerator_t3010 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24613_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24614_gshared (Enumerator_t3010 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24614(__this, method) (( Object_t * (*) (Enumerator_t3010 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24614_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m24615_gshared (Enumerator_t3010 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m24615(__this, method) (( bool (*) (Enumerator_t3010 *, const MethodInfo*))Enumerator_MoveNext_m24615_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" KeyValuePair_2_t3005  Enumerator_get_Current_m24616_gshared (Enumerator_t3010 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m24616(__this, method) (( KeyValuePair_2_t3005  (*) (Enumerator_t3010 *, const MethodInfo*))Enumerator_get_Current_m24616_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m24617_gshared (Enumerator_t3010 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m24617(__this, method) (( Object_t * (*) (Enumerator_t3010 *, const MethodInfo*))Enumerator_get_CurrentKey_m24617_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C" bool Enumerator_get_CurrentValue_m24618_gshared (Enumerator_t3010 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m24618(__this, method) (( bool (*) (Enumerator_t3010 *, const MethodInfo*))Enumerator_get_CurrentValue_m24618_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Reset()
extern "C" void Enumerator_Reset_m24619_gshared (Enumerator_t3010 * __this, const MethodInfo* method);
#define Enumerator_Reset_m24619(__this, method) (( void (*) (Enumerator_t3010 *, const MethodInfo*))Enumerator_Reset_m24619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern "C" void Enumerator_VerifyState_m24620_gshared (Enumerator_t3010 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m24620(__this, method) (( void (*) (Enumerator_t3010 *, const MethodInfo*))Enumerator_VerifyState_m24620_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m24621_gshared (Enumerator_t3010 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m24621(__this, method) (( void (*) (Enumerator_t3010 *, const MethodInfo*))Enumerator_VerifyCurrent_m24621_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m24622_gshared (Enumerator_t3010 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m24622(__this, method) (( void (*) (Enumerator_t3010 *, const MethodInfo*))Enumerator_Dispose_m24622_gshared)(__this, method)
