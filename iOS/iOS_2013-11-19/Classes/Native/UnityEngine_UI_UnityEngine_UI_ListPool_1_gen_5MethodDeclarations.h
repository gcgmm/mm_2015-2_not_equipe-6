﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t349;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.ListPool`1<System.Int32>::.cctor()
extern "C" void ListPool_1__cctor_m17816_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m17816(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m17816_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Int32>::Get()
extern "C" List_1_t349 * ListPool_1_Get_m2297_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Get_m2297(__this /* static, unused */, method) (( List_1_t349 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m2297_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m2321_gshared (Object_t * __this /* static, unused */, List_1_t349 * ___toRelease, const MethodInfo* method);
#define ListPool_1_Release_m2321(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t349 *, const MethodInfo*))ListPool_1_Release_m2321_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__14_m17817_gshared (Object_t * __this /* static, unused */, List_1_t349 * ___l, const MethodInfo* method);
#define ListPool_1_U3Cs_ListPoolU3Em__14_m17817(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t349 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__14_m17817_gshared)(__this /* static, unused */, ___l, method)
