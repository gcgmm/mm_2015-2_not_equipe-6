﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.LayoutRebuilder
struct LayoutRebuilder_t337;
// System.IAsyncResult
struct IAsyncResult_t260;
// System.AsyncCallback
struct AsyncCallback_t261;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"

// UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutRebuilder>
struct  UnityAction_1_t339  : public MulticastDelegate_t259
{
};
