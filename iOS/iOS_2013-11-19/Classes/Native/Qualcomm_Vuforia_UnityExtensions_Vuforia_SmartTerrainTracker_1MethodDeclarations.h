﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.SmartTerrainTrackerImpl
struct SmartTerrainTrackerImpl_t843;
// Vuforia.SmartTerrainBuilder
struct SmartTerrainBuilder_t756;

#include "codegen/il2cpp-codegen.h"

// System.Boolean Vuforia.SmartTerrainTrackerImpl::SetScaleToMillimeter(System.Single)
extern "C" bool SmartTerrainTrackerImpl_SetScaleToMillimeter_m4198 (SmartTerrainTrackerImpl_t843 * __this, float ___scaleFactor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTrackerImpl::get_SmartTerrainBuilder()
extern "C" SmartTerrainBuilder_t756 * SmartTerrainTrackerImpl_get_SmartTerrainBuilder_m4199 (SmartTerrainTrackerImpl_t843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerImpl::Start()
extern "C" bool SmartTerrainTrackerImpl_Start_m4200 (SmartTerrainTrackerImpl_t843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::Stop()
extern "C" void SmartTerrainTrackerImpl_Stop_m4201 (SmartTerrainTrackerImpl_t843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::.ctor()
extern "C" void SmartTerrainTrackerImpl__ctor_m4202 (SmartTerrainTrackerImpl_t843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
