﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>
struct GenericEqualityComparer_1_t3018;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m24671_gshared (GenericEqualityComparer_1_t3018 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m24671(__this, method) (( void (*) (GenericEqualityComparer_1_t3018 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m24671_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m24672_gshared (GenericEqualityComparer_1_t3018 * __this, bool ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m24672(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t3018 *, bool, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m24672_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m24673_gshared (GenericEqualityComparer_1_t3018 * __this, bool ___x, bool ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m24673(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t3018 *, bool, bool, const MethodInfo*))GenericEqualityComparer_1_Equals_m24673_gshared)(__this, ___x, ___y, method)
