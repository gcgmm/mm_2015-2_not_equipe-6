﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_133.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25366_gshared (InternalEnumerator_1_t3099 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m25366(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3099 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m25366_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m25367_gshared (InternalEnumerator_1_t3099 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m25367(__this, method) (( void (*) (InternalEnumerator_1_t3099 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m25367_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25368_gshared (InternalEnumerator_1_t3099 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25368(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3099 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25368_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25369_gshared (InternalEnumerator_1_t3099 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25369(__this, method) (( void (*) (InternalEnumerator_1_t3099 *, const MethodInfo*))InternalEnumerator_1_Dispose_m25369_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25370_gshared (InternalEnumerator_1_t3099 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25370(__this, method) (( bool (*) (InternalEnumerator_1_t3099 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m25370_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
extern "C" uint8_t InternalEnumerator_1_get_Current_m25371_gshared (InternalEnumerator_1_t3099 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25371(__this, method) (( uint8_t (*) (InternalEnumerator_1_t3099 *, const MethodInfo*))InternalEnumerator_1_get_Current_m25371_gshared)(__this, method)
