﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t2790;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_30.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21285_gshared (Enumerator_t2796 * __this, Dictionary_2_t2790 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m21285(__this, ___host, method) (( void (*) (Enumerator_t2796 *, Dictionary_2_t2790 *, const MethodInfo*))Enumerator__ctor_m21285_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21286_gshared (Enumerator_t2796 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21286(__this, method) (( Object_t * (*) (Enumerator_t2796 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21286_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m21287_gshared (Enumerator_t2796 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m21287(__this, method) (( void (*) (Enumerator_t2796 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m21287_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C" void Enumerator_Dispose_m21288_gshared (Enumerator_t2796 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21288(__this, method) (( void (*) (Enumerator_t2796 *, const MethodInfo*))Enumerator_Dispose_m21288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21289_gshared (Enumerator_t2796 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21289(__this, method) (( bool (*) (Enumerator_t2796 *, const MethodInfo*))Enumerator_MoveNext_m21289_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m21290_gshared (Enumerator_t2796 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21290(__this, method) (( Object_t * (*) (Enumerator_t2796 *, const MethodInfo*))Enumerator_get_Current_m21290_gshared)(__this, method)
