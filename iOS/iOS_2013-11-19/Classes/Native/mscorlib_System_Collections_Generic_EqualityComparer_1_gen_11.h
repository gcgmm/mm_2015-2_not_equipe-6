﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.EqualityComparer`1<System.UInt16>
struct EqualityComparer_1_t2804;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.EqualityComparer`1<System.UInt16>
struct  EqualityComparer_1_t2804  : public Object_t
{
};
struct EqualityComparer_1_t2804_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt16>::_default
	EqualityComparer_1_t2804 * ____default_0;
};
