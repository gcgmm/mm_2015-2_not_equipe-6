﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2363;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_30.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15354_gshared (Enumerator_t2372 * __this, Dictionary_2_t2363 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m15354(__this, ___host, method) (( void (*) (Enumerator_t2372 *, Dictionary_2_t2363 *, const MethodInfo*))Enumerator__ctor_m15354_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15355_gshared (Enumerator_t2372 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15355(__this, method) (( Object_t * (*) (Enumerator_t2372 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15355_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m15356_gshared (Enumerator_t2372 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m15356(__this, method) (( void (*) (Enumerator_t2372 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15356_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15357_gshared (Enumerator_t2372 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15357(__this, method) (( void (*) (Enumerator_t2372 *, const MethodInfo*))Enumerator_Dispose_m15357_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15358_gshared (Enumerator_t2372 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15358(__this, method) (( bool (*) (Enumerator_t2372 *, const MethodInfo*))Enumerator_MoveNext_m15358_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m15359_gshared (Enumerator_t2372 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15359(__this, method) (( Object_t * (*) (Enumerator_t2372 *, const MethodInfo*))Enumerator_get_Current_m15359_gshared)(__this, method)
