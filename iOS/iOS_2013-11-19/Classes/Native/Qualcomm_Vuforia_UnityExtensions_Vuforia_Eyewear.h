﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.Eyewear
struct Eyewear_t758;

#include "mscorlib_System_Object.h"

// Vuforia.Eyewear
struct  Eyewear_t758  : public Object_t
{
};
struct Eyewear_t758_StaticFields{
	// Vuforia.Eyewear Vuforia.Eyewear::mInstance
	Eyewear_t758 * ___mInstance_0;
};
