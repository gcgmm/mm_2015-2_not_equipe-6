﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// UnityEngine.EventSystems.BaseInputModule[]
// UnityEngine.EventSystems.BaseInputModule[]
struct BaseInputModuleU5BU5D_t2205  : public Array_t { };
// UnityEngine.EventSystems.IEventSystemHandler[]
// UnityEngine.EventSystems.IEventSystemHandler[]
struct IEventSystemHandlerU5BU5D_t2211  : public Array_t { };
// UnityEngine.EventSystems.RaycastResult[]
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2238  : public Array_t { };
// UnityEngine.EventSystems.BaseRaycaster[]
// UnityEngine.EventSystems.BaseRaycaster[]
struct BaseRaycasterU5BU5D_t2249  : public Array_t { };
// UnityEngine.EventSystems.EventTrigger/Entry[]
// UnityEngine.EventSystems.EventTrigger/Entry[]
struct EntryU5BU5D_t2255  : public Array_t { };
// UnityEngine.EventSystems.PointerEventData[]
// UnityEngine.EventSystems.PointerEventData[]
struct PointerEventDataU5BU5D_t2275  : public Array_t { };
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
struct ButtonStateU5BU5D_t2297  : public Array_t { };
// UnityEngine.UI.ICanvasElement[]
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t2323  : public Array_t { };
// UnityEngine.UI.Dropdown/OptionData[]
// UnityEngine.UI.Dropdown/OptionData[]
struct OptionDataU5BU5D_t2327  : public Array_t { };
// UnityEngine.UI.Dropdown/DropdownItem[]
// UnityEngine.UI.Dropdown/DropdownItem[]
struct DropdownItemU5BU5D_t2335  : public Array_t { };
// UnityEngine.UI.Text[]
// UnityEngine.UI.Text[]
struct TextU5BU5D_t2357  : public Array_t { };
// UnityEngine.UI.Graphic[]
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t2390  : public Array_t { };
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
struct IndexedSet_1U5BU5D_t2394  : public Array_t { };
// UnityEngine.UI.InputField/ContentType[]
// UnityEngine.UI.InputField/ContentType[]
struct ContentTypeU5BU5D_t383  : public Array_t { };
// UnityEngine.UI.RectMask2D[]
// UnityEngine.UI.RectMask2D[]
struct RectMask2DU5BU5D_t2422  : public Array_t { };
// UnityEngine.UI.IClippable[]
// UnityEngine.UI.IClippable[]
struct IClippableU5BU5D_t2428  : public Array_t { };
// UnityEngine.UI.Selectable[]
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t2436  : public Array_t { };
// UnityEngine.UI.StencilMaterial/MatEntry[]
// UnityEngine.UI.StencilMaterial/MatEntry[]
struct MatEntryU5BU5D_t2448  : public Array_t { };
// UnityEngine.UI.Toggle[]
// UnityEngine.UI.Toggle[]
struct ToggleU5BU5D_t2454  : public Array_t { };
// UnityEngine.UI.IClipper[]
// UnityEngine.UI.IClipper[]
struct IClipperU5BU5D_t2462  : public Array_t { };
// UnityEngine.UI.LayoutRebuilder[]
// UnityEngine.UI.LayoutRebuilder[]
struct LayoutRebuilderU5BU5D_t2478  : public Array_t { };
