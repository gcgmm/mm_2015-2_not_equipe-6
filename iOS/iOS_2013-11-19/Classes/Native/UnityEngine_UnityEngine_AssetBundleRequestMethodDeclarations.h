﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t463;
// UnityEngine.Object
struct Object_t86;
struct Object_t86_marshaled;
// UnityEngine.Object[]
struct ObjectU5BU5D_t671;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m2335 (AssetBundleRequest_t463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t86 * AssetBundleRequest_get_asset_m2336 (AssetBundleRequest_t463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C" ObjectU5BU5D_t671* AssetBundleRequest_get_allAssets_m2337 (AssetBundleRequest_t463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
