﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2363;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15322_gshared (Enumerator_t2369 * __this, Dictionary_2_t2363 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m15322(__this, ___dictionary, method) (( void (*) (Enumerator_t2369 *, Dictionary_2_t2363 *, const MethodInfo*))Enumerator__ctor_m15322_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15323_gshared (Enumerator_t2369 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15323(__this, method) (( Object_t * (*) (Enumerator_t2369 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m15324_gshared (Enumerator_t2369 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m15324(__this, method) (( void (*) (Enumerator_t2369 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15324_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1423  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15325_gshared (Enumerator_t2369 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15325(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t2369 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15325_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15326_gshared (Enumerator_t2369 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15326(__this, method) (( Object_t * (*) (Enumerator_t2369 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15326_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15327_gshared (Enumerator_t2369 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15327(__this, method) (( Object_t * (*) (Enumerator_t2369 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15327_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15328_gshared (Enumerator_t2369 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15328(__this, method) (( bool (*) (Enumerator_t2369 *, const MethodInfo*))Enumerator_MoveNext_m15328_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2365  Enumerator_get_Current_m15329_gshared (Enumerator_t2369 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15329(__this, method) (( KeyValuePair_2_t2365  (*) (Enumerator_t2369 *, const MethodInfo*))Enumerator_get_Current_m15329_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m15330_gshared (Enumerator_t2369 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m15330(__this, method) (( Object_t * (*) (Enumerator_t2369 *, const MethodInfo*))Enumerator_get_CurrentKey_m15330_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m15331_gshared (Enumerator_t2369 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m15331(__this, method) (( Object_t * (*) (Enumerator_t2369 *, const MethodInfo*))Enumerator_get_CurrentValue_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Reset()
extern "C" void Enumerator_Reset_m15332_gshared (Enumerator_t2369 * __this, const MethodInfo* method);
#define Enumerator_Reset_m15332(__this, method) (( void (*) (Enumerator_t2369 *, const MethodInfo*))Enumerator_Reset_m15332_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m15333_gshared (Enumerator_t2369 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15333(__this, method) (( void (*) (Enumerator_t2369 *, const MethodInfo*))Enumerator_VerifyState_m15333_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m15334_gshared (Enumerator_t2369 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m15334(__this, method) (( void (*) (Enumerator_t2369 *, const MethodInfo*))Enumerator_VerifyCurrent_m15334_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15335_gshared (Enumerator_t2369 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15335(__this, method) (( void (*) (Enumerator_t2369 *, const MethodInfo*))Enumerator_Dispose_m15335_gshared)(__this, method)
