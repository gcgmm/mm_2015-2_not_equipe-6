﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_17.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m19944_gshared (KeyValuePair_2_t2708 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m19944(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2708 *, int32_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m19944_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m19945_gshared (KeyValuePair_2_t2708 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m19945(__this, method) (( int32_t (*) (KeyValuePair_2_t2708 *, const MethodInfo*))KeyValuePair_2_get_Key_m19945_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m19946_gshared (KeyValuePair_2_t2708 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m19946(__this, ___value, method) (( void (*) (KeyValuePair_2_t2708 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m19946_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m19947_gshared (KeyValuePair_2_t2708 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m19947(__this, method) (( Object_t * (*) (KeyValuePair_2_t2708 *, const MethodInfo*))KeyValuePair_2_get_Value_m19947_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m19948_gshared (KeyValuePair_2_t2708 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m19948(__this, ___value, method) (( void (*) (KeyValuePair_2_t2708 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m19948_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m19949_gshared (KeyValuePair_2_t2708 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m19949(__this, method) (( String_t* (*) (KeyValuePair_2_t2708 *, const MethodInfo*))KeyValuePair_2_ToString_m19949_gshared)(__this, method)
