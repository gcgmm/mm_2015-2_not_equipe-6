﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct ShimEnumerator_t2907;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t1038;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m23200_gshared (ShimEnumerator_t2907 * __this, Dictionary_2_t1038 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m23200(__this, ___host, method) (( void (*) (ShimEnumerator_t2907 *, Dictionary_2_t1038 *, const MethodInfo*))ShimEnumerator__ctor_m23200_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m23201_gshared (ShimEnumerator_t2907 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m23201(__this, method) (( bool (*) (ShimEnumerator_t2907 *, const MethodInfo*))ShimEnumerator_MoveNext_m23201_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Entry()
extern "C" DictionaryEntry_t1423  ShimEnumerator_get_Entry_m23202_gshared (ShimEnumerator_t2907 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m23202(__this, method) (( DictionaryEntry_t1423  (*) (ShimEnumerator_t2907 *, const MethodInfo*))ShimEnumerator_get_Entry_m23202_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m23203_gshared (ShimEnumerator_t2907 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m23203(__this, method) (( Object_t * (*) (ShimEnumerator_t2907 *, const MethodInfo*))ShimEnumerator_get_Key_m23203_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m23204_gshared (ShimEnumerator_t2907 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m23204(__this, method) (( Object_t * (*) (ShimEnumerator_t2907 *, const MethodInfo*))ShimEnumerator_get_Value_m23204_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m23205_gshared (ShimEnumerator_t2907 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m23205(__this, method) (( Object_t * (*) (ShimEnumerator_t2907 *, const MethodInfo*))ShimEnumerator_get_Current_m23205_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Reset()
extern "C" void ShimEnumerator_Reset_m23206_gshared (ShimEnumerator_t2907 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m23206(__this, method) (( void (*) (ShimEnumerator_t2907 *, const MethodInfo*))ShimEnumerator_Reset_m23206_gshared)(__this, method)
