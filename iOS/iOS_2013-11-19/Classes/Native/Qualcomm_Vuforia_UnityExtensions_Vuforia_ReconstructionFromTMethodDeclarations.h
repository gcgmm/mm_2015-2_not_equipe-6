﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t55;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t53;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t755;

#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionBehaviour()
extern "C" ReconstructionAbstractBehaviour_t53 * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m3884 (ReconstructionFromTargetAbstractBehaviour_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionFromTarget()
extern "C" Object_t * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m3885 (ReconstructionFromTargetAbstractBehaviour_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Awake()
extern "C" void ReconstructionFromTargetAbstractBehaviour_Awake_m3886 (ReconstructionFromTargetAbstractBehaviour_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnDestroy()
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnDestroy_m3887 (ReconstructionFromTargetAbstractBehaviour_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Initialize()
extern "C" void ReconstructionFromTargetAbstractBehaviour_Initialize_m3888 (ReconstructionFromTargetAbstractBehaviour_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnTrackerStarted()
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m3889 (ReconstructionFromTargetAbstractBehaviour_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::.ctor()
extern "C" void ReconstructionFromTargetAbstractBehaviour__ctor_m252 (ReconstructionFromTargetAbstractBehaviour_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
