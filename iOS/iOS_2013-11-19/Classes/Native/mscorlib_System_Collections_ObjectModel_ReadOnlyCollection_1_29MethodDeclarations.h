﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>
struct ReadOnlyCollection_1_t2500;
// System.Collections.Generic.IList`1<UnityEngine.Color32>
struct IList_1_t2501;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// UnityEngine.Color32[]
struct Color32U5BU5D_t456;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color32>
struct IEnumerator_1_t3168;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m17338_gshared (ReadOnlyCollection_1_t2500 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m17338(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2500 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m17338_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17339_gshared (ReadOnlyCollection_1_t2500 * __this, Color32_t382  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17339(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2500 *, Color32_t382 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17339_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17340_gshared (ReadOnlyCollection_1_t2500 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17340(__this, method) (( void (*) (ReadOnlyCollection_1_t2500 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17340_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17341_gshared (ReadOnlyCollection_1_t2500 * __this, int32_t ___index, Color32_t382  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17341(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2500 *, int32_t, Color32_t382 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17341_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17342_gshared (ReadOnlyCollection_1_t2500 * __this, Color32_t382  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17342(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2500 *, Color32_t382 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17342_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17343_gshared (ReadOnlyCollection_1_t2500 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17343(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2500 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17343_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" Color32_t382  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17344_gshared (ReadOnlyCollection_1_t2500 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17344(__this, ___index, method) (( Color32_t382  (*) (ReadOnlyCollection_1_t2500 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17344_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17345_gshared (ReadOnlyCollection_1_t2500 * __this, int32_t ___index, Color32_t382  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17345(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2500 *, int32_t, Color32_t382 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17345_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17346_gshared (ReadOnlyCollection_1_t2500 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17346(__this, method) (( bool (*) (ReadOnlyCollection_1_t2500 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17346_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17347_gshared (ReadOnlyCollection_1_t2500 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17347(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2500 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17347_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17348_gshared (ReadOnlyCollection_1_t2500 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17348(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2500 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17348_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m17349_gshared (ReadOnlyCollection_1_t2500 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m17349(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2500 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m17349_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m17350_gshared (ReadOnlyCollection_1_t2500 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m17350(__this, method) (( void (*) (ReadOnlyCollection_1_t2500 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m17350_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m17351_gshared (ReadOnlyCollection_1_t2500 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m17351(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2500 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m17351_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17352_gshared (ReadOnlyCollection_1_t2500 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17352(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2500 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17352_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m17353_gshared (ReadOnlyCollection_1_t2500 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m17353(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2500 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m17353_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m17354_gshared (ReadOnlyCollection_1_t2500 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m17354(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2500 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m17354_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17355_gshared (ReadOnlyCollection_1_t2500 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17355(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2500 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17355_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17356_gshared (ReadOnlyCollection_1_t2500 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17356(__this, method) (( bool (*) (ReadOnlyCollection_1_t2500 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17356_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17357_gshared (ReadOnlyCollection_1_t2500 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17357(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2500 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17357_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17358_gshared (ReadOnlyCollection_1_t2500 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17358(__this, method) (( bool (*) (ReadOnlyCollection_1_t2500 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17358_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17359_gshared (ReadOnlyCollection_1_t2500 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17359(__this, method) (( bool (*) (ReadOnlyCollection_1_t2500 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17359_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m17360_gshared (ReadOnlyCollection_1_t2500 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m17360(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2500 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m17360_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m17361_gshared (ReadOnlyCollection_1_t2500 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m17361(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2500 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m17361_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m17362_gshared (ReadOnlyCollection_1_t2500 * __this, Color32_t382  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m17362(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2500 *, Color32_t382 , const MethodInfo*))ReadOnlyCollection_1_Contains_m17362_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m17363_gshared (ReadOnlyCollection_1_t2500 * __this, Color32U5BU5D_t456* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m17363(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2500 *, Color32U5BU5D_t456*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m17363_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m17364_gshared (ReadOnlyCollection_1_t2500 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m17364(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2500 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m17364_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m17365_gshared (ReadOnlyCollection_1_t2500 * __this, Color32_t382  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m17365(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2500 *, Color32_t382 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m17365_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m17366_gshared (ReadOnlyCollection_1_t2500 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m17366(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2500 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m17366_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::get_Item(System.Int32)
extern "C" Color32_t382  ReadOnlyCollection_1_get_Item_m17367_gshared (ReadOnlyCollection_1_t2500 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m17367(__this, ___index, method) (( Color32_t382  (*) (ReadOnlyCollection_1_t2500 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m17367_gshared)(__this, ___index, method)
