﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_97.h"

// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24777_gshared (InternalEnumerator_1_t3032 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m24777(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3032 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m24777_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24778_gshared (InternalEnumerator_1_t3032 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24778(__this, method) (( void (*) (InternalEnumerator_1_t3032 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24778_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24779_gshared (InternalEnumerator_1_t3032 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24779(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3032 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24779_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24780_gshared (InternalEnumerator_1_t3032 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24780(__this, method) (( void (*) (InternalEnumerator_1_t3032 *, const MethodInfo*))InternalEnumerator_1_Dispose_m24780_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24781_gshared (InternalEnumerator_1_t3032 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24781(__this, method) (( bool (*) (InternalEnumerator_1_t3032 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m24781_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern "C" int8_t InternalEnumerator_1_get_Current_m24782_gshared (InternalEnumerator_1_t3032 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24782(__this, method) (( int8_t (*) (InternalEnumerator_1_t3032 *, const MethodInfo*))InternalEnumerator_1_get_Current_m24782_gshared)(__this, method)
