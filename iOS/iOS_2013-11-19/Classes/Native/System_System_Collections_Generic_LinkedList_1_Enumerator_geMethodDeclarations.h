﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t827;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C" void Enumerator__ctor_m20937_gshared (Enumerator_t2772 * __this, LinkedList_1_t827 * ___parent, const MethodInfo* method);
#define Enumerator__ctor_m20937(__this, ___parent, method) (( void (*) (Enumerator_t2772 *, LinkedList_1_t827 *, const MethodInfo*))Enumerator__ctor_m20937_gshared)(__this, ___parent, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20938_gshared (Enumerator_t2772 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20938(__this, method) (( Object_t * (*) (Enumerator_t2772 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20938_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m20939_gshared (Enumerator_t2772 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m20939(__this, method) (( void (*) (Enumerator_t2772 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m20939_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m20940_gshared (Enumerator_t2772 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20940(__this, method) (( int32_t (*) (Enumerator_t2772 *, const MethodInfo*))Enumerator_get_Current_m20940_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20941_gshared (Enumerator_t2772 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20941(__this, method) (( bool (*) (Enumerator_t2772 *, const MethodInfo*))Enumerator_MoveNext_m20941_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m20942_gshared (Enumerator_t2772 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20942(__this, method) (( void (*) (Enumerator_t2772 *, const MethodInfo*))Enumerator_Dispose_m20942_gshared)(__this, method)
