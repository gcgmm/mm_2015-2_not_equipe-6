﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_92.h"
#include "System_System_Text_RegularExpressions_Mark.h"

// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24747_gshared (InternalEnumerator_1_t3027 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m24747(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3027 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m24747_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24748_gshared (InternalEnumerator_1_t3027 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24748(__this, method) (( void (*) (InternalEnumerator_1_t3027 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24748_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24749_gshared (InternalEnumerator_1_t3027 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24749(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3027 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24749_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24750_gshared (InternalEnumerator_1_t3027 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24750(__this, method) (( void (*) (InternalEnumerator_1_t3027 *, const MethodInfo*))InternalEnumerator_1_Dispose_m24750_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24751_gshared (InternalEnumerator_1_t3027 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24751(__this, method) (( bool (*) (InternalEnumerator_1_t3027 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m24751_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern "C" Mark_t1375  InternalEnumerator_1_get_Current_m24752_gshared (InternalEnumerator_1_t3027 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24752(__this, method) (( Mark_t1375  (*) (InternalEnumerator_1_t3027 *, const MethodInfo*))InternalEnumerator_1_get_Current_m24752_gshared)(__this, method)
