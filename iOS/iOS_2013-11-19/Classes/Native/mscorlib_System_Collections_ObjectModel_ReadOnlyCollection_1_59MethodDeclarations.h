﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_2MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m23262(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2910 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m12908_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23263(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2910 *, VirtualButtonAbstractBehaviour_t71 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m12909_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23264(__this, method) (( void (*) (ReadOnlyCollection_1_t2910 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m12910_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23265(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2910 *, int32_t, VirtualButtonAbstractBehaviour_t71 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m12911_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23266(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2910 *, VirtualButtonAbstractBehaviour_t71 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m12912_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23267(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2910 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m12913_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23268(__this, ___index, method) (( VirtualButtonAbstractBehaviour_t71 * (*) (ReadOnlyCollection_1_t2910 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m12914_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23269(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2910 *, int32_t, VirtualButtonAbstractBehaviour_t71 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m12915_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23270(__this, method) (( bool (*) (ReadOnlyCollection_1_t2910 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12916_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23271(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2910 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m12917_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23272(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2910 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m12918_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m23273(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2910 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m12919_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m23274(__this, method) (( void (*) (ReadOnlyCollection_1_t2910 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m12920_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m23275(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2910 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m12921_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23276(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2910 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m12922_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m23277(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2910 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m12923_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m23278(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2910 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m12924_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23279(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2910 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m12925_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23280(__this, method) (( bool (*) (ReadOnlyCollection_1_t2910 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m12926_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23281(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2910 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m12927_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23282(__this, method) (( bool (*) (ReadOnlyCollection_1_t2910 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m12928_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23283(__this, method) (( bool (*) (ReadOnlyCollection_1_t2910 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m12929_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m23284(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2910 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m12930_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m23285(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2910 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m12931_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Contains(T)
#define ReadOnlyCollection_1_Contains_m23286(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2910 *, VirtualButtonAbstractBehaviour_t71 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m12932_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m23287(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2910 *, VirtualButtonAbstractBehaviourU5BU5D_t942*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m12933_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m23288(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2910 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m12934_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m23289(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2910 *, VirtualButtonAbstractBehaviour_t71 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m12935_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Count()
#define ReadOnlyCollection_1_get_Count_m23290(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2910 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m12936_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m23291(__this, ___index, method) (( VirtualButtonAbstractBehaviour_t71 * (*) (ReadOnlyCollection_1_t2910 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m12937_gshared)(__this, ___index, method)
