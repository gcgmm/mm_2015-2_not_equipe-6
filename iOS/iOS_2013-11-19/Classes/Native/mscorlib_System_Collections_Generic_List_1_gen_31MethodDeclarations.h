﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t569;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UILineInfo>
struct IEnumerable_1_t3185;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t3186;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>
struct ICollection_1_t432;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>
struct ReadOnlyCollection_1_t2594;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t684;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t2598;
// System.Comparison`1<UnityEngine.UILineInfo>
struct Comparison_1_t2601;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_56.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void List_1__ctor_m18322_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1__ctor_m18322(__this, method) (( void (*) (List_1_t569 *, const MethodInfo*))List_1__ctor_m18322_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m18323_gshared (List_1_t569 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m18323(__this, ___collection, method) (( void (*) (List_1_t569 *, Object_t*, const MethodInfo*))List_1__ctor_m18323_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m3587_gshared (List_1_t569 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m3587(__this, ___capacity, method) (( void (*) (List_1_t569 *, int32_t, const MethodInfo*))List_1__ctor_m3587_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.cctor()
extern "C" void List_1__cctor_m18324_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m18324(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m18324_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18325_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18325(__this, method) (( Object_t* (*) (List_1_t569 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18325_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m18326_gshared (List_1_t569 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m18326(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t569 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m18326_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m18327_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18327(__this, method) (( Object_t * (*) (List_1_t569 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m18327_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m18328_gshared (List_1_t569 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m18328(__this, ___item, method) (( int32_t (*) (List_1_t569 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m18328_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m18329_gshared (List_1_t569 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m18329(__this, ___item, method) (( bool (*) (List_1_t569 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m18329_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m18330_gshared (List_1_t569 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m18330(__this, ___item, method) (( int32_t (*) (List_1_t569 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m18330_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m18331_gshared (List_1_t569 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m18331(__this, ___index, ___item, method) (( void (*) (List_1_t569 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m18331_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m18332_gshared (List_1_t569 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m18332(__this, ___item, method) (( void (*) (List_1_t569 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m18332_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18333_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18333(__this, method) (( bool (*) (List_1_t569 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18333_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m18334_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18334(__this, method) (( bool (*) (List_1_t569 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m18334_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m18335_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m18335(__this, method) (( Object_t * (*) (List_1_t569 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m18335_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m18336_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m18336(__this, method) (( bool (*) (List_1_t569 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m18336_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m18337_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m18337(__this, method) (( bool (*) (List_1_t569 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m18337_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m18338_gshared (List_1_t569 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m18338(__this, ___index, method) (( Object_t * (*) (List_1_t569 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m18338_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m18339_gshared (List_1_t569 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m18339(__this, ___index, ___value, method) (( void (*) (List_1_t569 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m18339_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void List_1_Add_m18340_gshared (List_1_t569 * __this, UILineInfo_t428  ___item, const MethodInfo* method);
#define List_1_Add_m18340(__this, ___item, method) (( void (*) (List_1_t569 *, UILineInfo_t428 , const MethodInfo*))List_1_Add_m18340_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m18341_gshared (List_1_t569 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m18341(__this, ___newCount, method) (( void (*) (List_1_t569 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m18341_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m18342_gshared (List_1_t569 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m18342(__this, ___collection, method) (( void (*) (List_1_t569 *, Object_t*, const MethodInfo*))List_1_AddCollection_m18342_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m18343_gshared (List_1_t569 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m18343(__this, ___enumerable, method) (( void (*) (List_1_t569 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m18343_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m18344_gshared (List_1_t569 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m18344(__this, ___collection, method) (( void (*) (List_1_t569 *, Object_t*, const MethodInfo*))List_1_AddRange_m18344_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2594 * List_1_AsReadOnly_m18345_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m18345(__this, method) (( ReadOnlyCollection_1_t2594 * (*) (List_1_t569 *, const MethodInfo*))List_1_AsReadOnly_m18345_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Clear()
extern "C" void List_1_Clear_m18346_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_Clear_m18346(__this, method) (( void (*) (List_1_t569 *, const MethodInfo*))List_1_Clear_m18346_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool List_1_Contains_m18347_gshared (List_1_t569 * __this, UILineInfo_t428  ___item, const MethodInfo* method);
#define List_1_Contains_m18347(__this, ___item, method) (( bool (*) (List_1_t569 *, UILineInfo_t428 , const MethodInfo*))List_1_Contains_m18347_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m18348_gshared (List_1_t569 * __this, UILineInfoU5BU5D_t684* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m18348(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t569 *, UILineInfoU5BU5D_t684*, int32_t, const MethodInfo*))List_1_CopyTo_m18348_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Find(System.Predicate`1<T>)
extern "C" UILineInfo_t428  List_1_Find_m18349_gshared (List_1_t569 * __this, Predicate_1_t2598 * ___match, const MethodInfo* method);
#define List_1_Find_m18349(__this, ___match, method) (( UILineInfo_t428  (*) (List_1_t569 *, Predicate_1_t2598 *, const MethodInfo*))List_1_Find_m18349_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m18350_gshared (Object_t * __this /* static, unused */, Predicate_1_t2598 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m18350(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2598 *, const MethodInfo*))List_1_CheckMatch_m18350_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m18351_gshared (List_1_t569 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2598 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m18351(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t569 *, int32_t, int32_t, Predicate_1_t2598 *, const MethodInfo*))List_1_GetIndex_m18351_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Enumerator_t2593  List_1_GetEnumerator_m18352_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m18352(__this, method) (( Enumerator_t2593  (*) (List_1_t569 *, const MethodInfo*))List_1_GetEnumerator_m18352_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m18353_gshared (List_1_t569 * __this, UILineInfo_t428  ___item, const MethodInfo* method);
#define List_1_IndexOf_m18353(__this, ___item, method) (( int32_t (*) (List_1_t569 *, UILineInfo_t428 , const MethodInfo*))List_1_IndexOf_m18353_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m18354_gshared (List_1_t569 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m18354(__this, ___start, ___delta, method) (( void (*) (List_1_t569 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m18354_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m18355_gshared (List_1_t569 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m18355(__this, ___index, method) (( void (*) (List_1_t569 *, int32_t, const MethodInfo*))List_1_CheckIndex_m18355_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m18356_gshared (List_1_t569 * __this, int32_t ___index, UILineInfo_t428  ___item, const MethodInfo* method);
#define List_1_Insert_m18356(__this, ___index, ___item, method) (( void (*) (List_1_t569 *, int32_t, UILineInfo_t428 , const MethodInfo*))List_1_Insert_m18356_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m18357_gshared (List_1_t569 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m18357(__this, ___collection, method) (( void (*) (List_1_t569 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m18357_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool List_1_Remove_m18358_gshared (List_1_t569 * __this, UILineInfo_t428  ___item, const MethodInfo* method);
#define List_1_Remove_m18358(__this, ___item, method) (( bool (*) (List_1_t569 *, UILineInfo_t428 , const MethodInfo*))List_1_Remove_m18358_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m18359_gshared (List_1_t569 * __this, Predicate_1_t2598 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m18359(__this, ___match, method) (( int32_t (*) (List_1_t569 *, Predicate_1_t2598 *, const MethodInfo*))List_1_RemoveAll_m18359_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m18360_gshared (List_1_t569 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m18360(__this, ___index, method) (( void (*) (List_1_t569 *, int32_t, const MethodInfo*))List_1_RemoveAt_m18360_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Reverse()
extern "C" void List_1_Reverse_m18361_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_Reverse_m18361(__this, method) (( void (*) (List_1_t569 *, const MethodInfo*))List_1_Reverse_m18361_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort()
extern "C" void List_1_Sort_m18362_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_Sort_m18362(__this, method) (( void (*) (List_1_t569 *, const MethodInfo*))List_1_Sort_m18362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m18363_gshared (List_1_t569 * __this, Comparison_1_t2601 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m18363(__this, ___comparison, method) (( void (*) (List_1_t569 *, Comparison_1_t2601 *, const MethodInfo*))List_1_Sort_m18363_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UILineInfo>::ToArray()
extern "C" UILineInfoU5BU5D_t684* List_1_ToArray_m18364_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_ToArray_m18364(__this, method) (( UILineInfoU5BU5D_t684* (*) (List_1_t569 *, const MethodInfo*))List_1_ToArray_m18364_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m18365_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m18365(__this, method) (( void (*) (List_1_t569 *, const MethodInfo*))List_1_TrimExcess_m18365_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m18366_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m18366(__this, method) (( int32_t (*) (List_1_t569 *, const MethodInfo*))List_1_get_Capacity_m18366_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m18367_gshared (List_1_t569 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m18367(__this, ___value, method) (( void (*) (List_1_t569 *, int32_t, const MethodInfo*))List_1_set_Capacity_m18367_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m18368_gshared (List_1_t569 * __this, const MethodInfo* method);
#define List_1_get_Count_m18368(__this, method) (( int32_t (*) (List_1_t569 *, const MethodInfo*))List_1_get_Count_m18368_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t428  List_1_get_Item_m18369_gshared (List_1_t569 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m18369(__this, ___index, method) (( UILineInfo_t428  (*) (List_1_t569 *, int32_t, const MethodInfo*))List_1_get_Item_m18369_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m18370_gshared (List_1_t569 * __this, int32_t ___index, UILineInfo_t428  ___value, const MethodInfo* method);
#define List_1_set_Item_m18370(__this, ___index, ___value, method) (( void (*) (List_1_t569 *, int32_t, UILineInfo_t428 , const MethodInfo*))List_1_set_Item_m18370_gshared)(__this, ___index, ___value, method)
