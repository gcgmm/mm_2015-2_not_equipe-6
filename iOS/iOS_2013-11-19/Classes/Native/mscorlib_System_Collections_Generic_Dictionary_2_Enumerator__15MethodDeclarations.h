﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m19139(__this, ___dictionary, method) (( void (*) (Enumerator_t2653 *, Dictionary_2_t652 *, const MethodInfo*))Enumerator__ctor_m19036_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19140(__this, method) (( Object_t * (*) (Enumerator_t2653 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19037_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m19141(__this, method) (( void (*) (Enumerator_t2653 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m19038_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19142(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t2653 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19039_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19143(__this, method) (( Object_t * (*) (Enumerator_t2653 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19040_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19144(__this, method) (( Object_t * (*) (Enumerator_t2653 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19041_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m19145(__this, method) (( bool (*) (Enumerator_t2653 *, const MethodInfo*))Enumerator_MoveNext_m19042_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m19146(__this, method) (( KeyValuePair_2_t2650  (*) (Enumerator_t2653 *, const MethodInfo*))Enumerator_get_Current_m19043_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m19147(__this, method) (( Event_t269 * (*) (Enumerator_t2653 *, const MethodInfo*))Enumerator_get_CurrentKey_m19044_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m19148(__this, method) (( int32_t (*) (Enumerator_t2653 *, const MethodInfo*))Enumerator_get_CurrentValue_m19045_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Reset()
#define Enumerator_Reset_m19149(__this, method) (( void (*) (Enumerator_t2653 *, const MethodInfo*))Enumerator_Reset_m19046_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m19150(__this, method) (( void (*) (Enumerator_t2653 *, const MethodInfo*))Enumerator_VerifyState_m19047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m19151(__this, method) (( void (*) (Enumerator_t2653 *, const MethodInfo*))Enumerator_VerifyCurrent_m19048_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m19152(__this, method) (( void (*) (Enumerator_t2653 *, const MethodInfo*))Enumerator_Dispose_m19049_gshared)(__this, method)
