﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m21733(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1001 *, int32_t, WordAbstractBehaviour_t80 *, const MethodInfo*))KeyValuePair_2__ctor_m14271_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m5400(__this, method) (( int32_t (*) (KeyValuePair_2_t1001 *, const MethodInfo*))KeyValuePair_2_get_Key_m14272_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m21734(__this, ___value, method) (( void (*) (KeyValuePair_2_t1001 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m14273_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m5401(__this, method) (( WordAbstractBehaviour_t80 * (*) (KeyValuePair_2_t1001 *, const MethodInfo*))KeyValuePair_2_get_Value_m14274_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m21735(__this, ___value, method) (( void (*) (KeyValuePair_2_t1001 *, WordAbstractBehaviour_t80 *, const MethodInfo*))KeyValuePair_2_set_Value_m14275_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::ToString()
#define KeyValuePair_2_ToString_m21736(__this, method) (( String_t* (*) (KeyValuePair_2_t1001 *, const MethodInfo*))KeyValuePair_2_ToString_m14276_gshared)(__this, method)
