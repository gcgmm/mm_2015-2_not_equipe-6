﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.FactorySetter
struct FactorySetter_t109;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.FactorySetter::.ctor()
extern "C" void FactorySetter__ctor_m3651 (FactorySetter_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
