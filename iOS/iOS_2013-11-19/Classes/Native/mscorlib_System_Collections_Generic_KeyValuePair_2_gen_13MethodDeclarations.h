﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m18714(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2616 *, String_t*, GUIStyle_t584 *, const MethodInfo*))KeyValuePair_2__ctor_m15296_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Key()
#define KeyValuePair_2_get_Key_m18715(__this, method) (( String_t* (*) (KeyValuePair_2_t2616 *, const MethodInfo*))KeyValuePair_2_get_Key_m15297_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m18716(__this, ___value, method) (( void (*) (KeyValuePair_2_t2616 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m15298_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Value()
#define KeyValuePair_2_get_Value_m18717(__this, method) (( GUIStyle_t584 * (*) (KeyValuePair_2_t2616 *, const MethodInfo*))KeyValuePair_2_get_Value_m15299_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18718(__this, ___value, method) (( void (*) (KeyValuePair_2_t2616 *, GUIStyle_t584 *, const MethodInfo*))KeyValuePair_2_set_Value_m15300_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::ToString()
#define KeyValuePair_2_ToString_m18719(__this, method) (( String_t* (*) (KeyValuePair_2_t2616 *, const MethodInfo*))KeyValuePair_2_ToString_m15301_gshared)(__this, method)
