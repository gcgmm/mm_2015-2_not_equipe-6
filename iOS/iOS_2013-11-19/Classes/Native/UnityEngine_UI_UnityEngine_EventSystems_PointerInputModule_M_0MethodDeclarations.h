﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct MouseButtonEventData_t179;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::.ctor()
extern "C" void MouseButtonEventData__ctor_m506 (MouseButtonEventData_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::PressedThisFrame()
extern "C" bool MouseButtonEventData_PressedThisFrame_m507 (MouseButtonEventData_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::ReleasedThisFrame()
extern "C" bool MouseButtonEventData_ReleasedThisFrame_m508 (MouseButtonEventData_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
