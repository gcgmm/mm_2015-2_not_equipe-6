﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t3067;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerable_1_t3263;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3262;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ICollection_1_t2133;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ReadOnlyCollection_1_t2131;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2129;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t3071;
// System.Comparison`1<System.Reflection.CustomAttributeTypedArgument>
struct Comparison_1_t3074;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_63.h"

// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void List_1__ctor_m25036_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1__ctor_m25036(__this, method) (( void (*) (List_1_t3067 *, const MethodInfo*))List_1__ctor_m25036_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m25037_gshared (List_1_t3067 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m25037(__this, ___collection, method) (( void (*) (List_1_t3067 *, Object_t*, const MethodInfo*))List_1__ctor_m25037_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Int32)
extern "C" void List_1__ctor_m25038_gshared (List_1_t3067 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m25038(__this, ___capacity, method) (( void (*) (List_1_t3067 *, int32_t, const MethodInfo*))List_1__ctor_m25038_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern "C" void List_1__cctor_m25039_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m25039(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m25039_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25040_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25040(__this, method) (( Object_t* (*) (List_1_t3067 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25040_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m25041_gshared (List_1_t3067 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m25041(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3067 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m25041_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m25042_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m25042(__this, method) (( Object_t * (*) (List_1_t3067 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m25042_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m25043_gshared (List_1_t3067 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m25043(__this, ___item, method) (( int32_t (*) (List_1_t3067 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m25043_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m25044_gshared (List_1_t3067 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m25044(__this, ___item, method) (( bool (*) (List_1_t3067 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m25044_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m25045_gshared (List_1_t3067 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m25045(__this, ___item, method) (( int32_t (*) (List_1_t3067 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m25045_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m25046_gshared (List_1_t3067 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m25046(__this, ___index, ___item, method) (( void (*) (List_1_t3067 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m25046_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m25047_gshared (List_1_t3067 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m25047(__this, ___item, method) (( void (*) (List_1_t3067 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m25047_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25048_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25048(__this, method) (( bool (*) (List_1_t3067 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25048_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m25049_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m25049(__this, method) (( bool (*) (List_1_t3067 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m25049_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m25050_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m25050(__this, method) (( Object_t * (*) (List_1_t3067 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m25050_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m25051_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m25051(__this, method) (( bool (*) (List_1_t3067 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m25051_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m25052_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m25052(__this, method) (( bool (*) (List_1_t3067 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m25052_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m25053_gshared (List_1_t3067 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m25053(__this, ___index, method) (( Object_t * (*) (List_1_t3067 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m25053_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m25054_gshared (List_1_t3067 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m25054(__this, ___index, ___value, method) (( void (*) (List_1_t3067 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m25054_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void List_1_Add_m25055_gshared (List_1_t3067 * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define List_1_Add_m25055(__this, ___item, method) (( void (*) (List_1_t3067 *, CustomAttributeTypedArgument_t1692 , const MethodInfo*))List_1_Add_m25055_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m25056_gshared (List_1_t3067 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m25056(__this, ___newCount, method) (( void (*) (List_1_t3067 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m25056_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m25057_gshared (List_1_t3067 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m25057(__this, ___collection, method) (( void (*) (List_1_t3067 *, Object_t*, const MethodInfo*))List_1_AddCollection_m25057_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m25058_gshared (List_1_t3067 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m25058(__this, ___enumerable, method) (( void (*) (List_1_t3067 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m25058_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m25059_gshared (List_1_t3067 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m25059(__this, ___collection, method) (( void (*) (List_1_t3067 *, Object_t*, const MethodInfo*))List_1_AddRange_m25059_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2131 * List_1_AsReadOnly_m25060_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m25060(__this, method) (( ReadOnlyCollection_1_t2131 * (*) (List_1_t3067 *, const MethodInfo*))List_1_AsReadOnly_m25060_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void List_1_Clear_m25061_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_Clear_m25061(__this, method) (( void (*) (List_1_t3067 *, const MethodInfo*))List_1_Clear_m25061_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool List_1_Contains_m25062_gshared (List_1_t3067 * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define List_1_Contains_m25062(__this, ___item, method) (( bool (*) (List_1_t3067 *, CustomAttributeTypedArgument_t1692 , const MethodInfo*))List_1_Contains_m25062_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m25063_gshared (List_1_t3067 * __this, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m25063(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3067 *, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, const MethodInfo*))List_1_CopyTo_m25063_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Find(System.Predicate`1<T>)
extern "C" CustomAttributeTypedArgument_t1692  List_1_Find_m25064_gshared (List_1_t3067 * __this, Predicate_1_t3071 * ___match, const MethodInfo* method);
#define List_1_Find_m25064(__this, ___match, method) (( CustomAttributeTypedArgument_t1692  (*) (List_1_t3067 *, Predicate_1_t3071 *, const MethodInfo*))List_1_Find_m25064_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m25065_gshared (Object_t * __this /* static, unused */, Predicate_1_t3071 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m25065(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3071 *, const MethodInfo*))List_1_CheckMatch_m25065_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m25066_gshared (List_1_t3067 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3071 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m25066(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t3067 *, int32_t, int32_t, Predicate_1_t3071 *, const MethodInfo*))List_1_GetIndex_m25066_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Enumerator_t3068  List_1_GetEnumerator_m25067_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m25067(__this, method) (( Enumerator_t3068  (*) (List_1_t3067 *, const MethodInfo*))List_1_GetEnumerator_m25067_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m25068_gshared (List_1_t3067 * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define List_1_IndexOf_m25068(__this, ___item, method) (( int32_t (*) (List_1_t3067 *, CustomAttributeTypedArgument_t1692 , const MethodInfo*))List_1_IndexOf_m25068_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m25069_gshared (List_1_t3067 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m25069(__this, ___start, ___delta, method) (( void (*) (List_1_t3067 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m25069_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m25070_gshared (List_1_t3067 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m25070(__this, ___index, method) (( void (*) (List_1_t3067 *, int32_t, const MethodInfo*))List_1_CheckIndex_m25070_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m25071_gshared (List_1_t3067 * __this, int32_t ___index, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define List_1_Insert_m25071(__this, ___index, ___item, method) (( void (*) (List_1_t3067 *, int32_t, CustomAttributeTypedArgument_t1692 , const MethodInfo*))List_1_Insert_m25071_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m25072_gshared (List_1_t3067 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m25072(__this, ___collection, method) (( void (*) (List_1_t3067 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m25072_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool List_1_Remove_m25073_gshared (List_1_t3067 * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define List_1_Remove_m25073(__this, ___item, method) (( bool (*) (List_1_t3067 *, CustomAttributeTypedArgument_t1692 , const MethodInfo*))List_1_Remove_m25073_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m25074_gshared (List_1_t3067 * __this, Predicate_1_t3071 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m25074(__this, ___match, method) (( int32_t (*) (List_1_t3067 *, Predicate_1_t3071 *, const MethodInfo*))List_1_RemoveAll_m25074_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m25075_gshared (List_1_t3067 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m25075(__this, ___index, method) (( void (*) (List_1_t3067 *, int32_t, const MethodInfo*))List_1_RemoveAt_m25075_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Reverse()
extern "C" void List_1_Reverse_m25076_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_Reverse_m25076(__this, method) (( void (*) (List_1_t3067 *, const MethodInfo*))List_1_Reverse_m25076_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort()
extern "C" void List_1_Sort_m25077_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_Sort_m25077(__this, method) (( void (*) (List_1_t3067 *, const MethodInfo*))List_1_Sort_m25077_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m25078_gshared (List_1_t3067 * __this, Comparison_1_t3074 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m25078(__this, ___comparison, method) (( void (*) (List_1_t3067 *, Comparison_1_t3074 *, const MethodInfo*))List_1_Sort_m25078_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::ToArray()
extern "C" CustomAttributeTypedArgumentU5BU5D_t2129* List_1_ToArray_m25079_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_ToArray_m25079(__this, method) (( CustomAttributeTypedArgumentU5BU5D_t2129* (*) (List_1_t3067 *, const MethodInfo*))List_1_ToArray_m25079_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::TrimExcess()
extern "C" void List_1_TrimExcess_m25080_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m25080(__this, method) (( void (*) (List_1_t3067 *, const MethodInfo*))List_1_TrimExcess_m25080_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m25081_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m25081(__this, method) (( int32_t (*) (List_1_t3067 *, const MethodInfo*))List_1_get_Capacity_m25081_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m25082_gshared (List_1_t3067 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m25082(__this, ___value, method) (( void (*) (List_1_t3067 *, int32_t, const MethodInfo*))List_1_set_Capacity_m25082_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t List_1_get_Count_m25083_gshared (List_1_t3067 * __this, const MethodInfo* method);
#define List_1_get_Count_m25083(__this, method) (( int32_t (*) (List_1_t3067 *, const MethodInfo*))List_1_get_Count_m25083_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1692  List_1_get_Item_m25084_gshared (List_1_t3067 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m25084(__this, ___index, method) (( CustomAttributeTypedArgument_t1692  (*) (List_1_t3067 *, int32_t, const MethodInfo*))List_1_get_Item_m25084_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m25085_gshared (List_1_t3067 * __this, int32_t ___index, CustomAttributeTypedArgument_t1692  ___value, const MethodInfo* method);
#define List_1_set_Item_m25085(__this, ___index, ___value, method) (( void (*) (List_1_t3067 *, int32_t, CustomAttributeTypedArgument_t1692 , const MethodInfo*))List_1_set_Item_m25085_gshared)(__this, ___index, ___value, method)
