﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.MulticastDelegate
struct MulticastDelegate_t259;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t687;
// System.Object
struct Object_t;
// System.Delegate[]
struct DelegateU5BU5D_t2102;
// System.Delegate
struct Delegate_t110;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.MulticastDelegate::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MulticastDelegate_GetObjectData_m8192 (MulticastDelegate_t259 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::Equals(System.Object)
extern "C" bool MulticastDelegate_Equals_m8193 (MulticastDelegate_t259 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MulticastDelegate::GetHashCode()
extern "C" int32_t MulticastDelegate_GetHashCode_m8194 (MulticastDelegate_t259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate[] System.MulticastDelegate::GetInvocationList()
extern "C" DelegateU5BU5D_t2102* MulticastDelegate_GetInvocationList_m8195 (MulticastDelegate_t259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::CombineImpl(System.Delegate)
extern "C" Delegate_t110 * MulticastDelegate_CombineImpl_m8196 (MulticastDelegate_t259 * __this, Delegate_t110 * ___follow, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::BaseEquals(System.MulticastDelegate)
extern "C" bool MulticastDelegate_BaseEquals_m8197 (MulticastDelegate_t259 * __this, MulticastDelegate_t259 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.MulticastDelegate System.MulticastDelegate::KPM(System.MulticastDelegate,System.MulticastDelegate,System.MulticastDelegate&)
extern "C" MulticastDelegate_t259 * MulticastDelegate_KPM_m8198 (Object_t * __this /* static, unused */, MulticastDelegate_t259 * ___needle, MulticastDelegate_t259 * ___haystack, MulticastDelegate_t259 ** ___tail, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::RemoveImpl(System.Delegate)
extern "C" Delegate_t110 * MulticastDelegate_RemoveImpl_m8199 (MulticastDelegate_t259 * __this, Delegate_t110 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
