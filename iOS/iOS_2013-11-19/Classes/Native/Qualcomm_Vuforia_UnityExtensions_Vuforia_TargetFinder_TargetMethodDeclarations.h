﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void TargetSearchResult_t884_marshal(const TargetSearchResult_t884& unmarshaled, TargetSearchResult_t884_marshaled& marshaled);
extern "C" void TargetSearchResult_t884_marshal_back(const TargetSearchResult_t884_marshaled& marshaled, TargetSearchResult_t884& unmarshaled);
extern "C" void TargetSearchResult_t884_marshal_cleanup(TargetSearchResult_t884_marshaled& marshaled);
