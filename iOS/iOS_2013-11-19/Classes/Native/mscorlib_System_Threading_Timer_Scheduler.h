﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.Timer/Scheduler
struct Scheduler_t1994;
// System.Collections.SortedList
struct SortedList_t1431;

#include "mscorlib_System_Object.h"

// System.Threading.Timer/Scheduler
struct  Scheduler_t1994  : public Object_t
{
	// System.Collections.SortedList System.Threading.Timer/Scheduler::list
	SortedList_t1431 * ___list_1;
};
struct Scheduler_t1994_StaticFields{
	// System.Threading.Timer/Scheduler System.Threading.Timer/Scheduler::instance
	Scheduler_t1994 * ___instance_0;
};
