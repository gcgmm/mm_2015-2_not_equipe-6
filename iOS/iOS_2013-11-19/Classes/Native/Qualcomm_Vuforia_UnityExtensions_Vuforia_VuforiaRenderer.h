﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.VuforiaRenderer
struct VuforiaRenderer_t832;

#include "mscorlib_System_Object.h"

// Vuforia.VuforiaRenderer
struct  VuforiaRenderer_t832  : public Object_t
{
};
struct VuforiaRenderer_t832_StaticFields{
	// Vuforia.VuforiaRenderer Vuforia.VuforiaRenderer::sInstance
	VuforiaRenderer_t832 * ___sInstance_0;
};
