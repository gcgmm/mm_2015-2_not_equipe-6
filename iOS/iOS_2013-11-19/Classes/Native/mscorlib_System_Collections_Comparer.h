﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Comparer
struct Comparer_t1556;
// System.Globalization.CompareInfo
struct CompareInfo_t1255;

#include "mscorlib_System_Object.h"

// System.Collections.Comparer
struct  Comparer_t1556  : public Object_t
{
	// System.Globalization.CompareInfo System.Collections.Comparer::m_compareInfo
	CompareInfo_t1255 * ___m_compareInfo_2;
};
struct Comparer_t1556_StaticFields{
	// System.Collections.Comparer System.Collections.Comparer::Default
	Comparer_t1556 * ___Default_0;
	// System.Collections.Comparer System.Collections.Comparer::DefaultInvariant
	Comparer_t1556 * ___DefaultInvariant_1;
};
