﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t659;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t661;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t663;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C" void PersistentCallGroup__ctor_m3513 (PersistentCallGroup_t659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C" void PersistentCallGroup_Initialize_m3514 (PersistentCallGroup_t659 * __this, InvokableCallList_t661 * ___invokableList, UnityEventBase_t663 * ___unityEventBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
