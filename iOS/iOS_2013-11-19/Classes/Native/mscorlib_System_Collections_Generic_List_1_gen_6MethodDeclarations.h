﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_63MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor()
#define List_1__ctor_m13988(__this, method) (( void (*) (List_1_t164 *, const MethodInfo*))List_1__ctor_m12803_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m13989(__this, ___collection, method) (( void (*) (List_1_t164 *, Object_t*, const MethodInfo*))List_1__ctor_m12804_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor(System.Int32)
#define List_1__ctor_m1751(__this, ___capacity, method) (( void (*) (List_1_t164 *, int32_t, const MethodInfo*))List_1__ctor_m12806_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.cctor()
#define List_1__cctor_m13990(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m12808_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13991(__this, method) (( Object_t* (*) (List_1_t164 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12810_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m13992(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t164 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m12812_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m13993(__this, method) (( Object_t * (*) (List_1_t164 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m12814_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m13994(__this, ___item, method) (( int32_t (*) (List_1_t164 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m12816_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m13995(__this, ___item, method) (( bool (*) (List_1_t164 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m12818_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m13996(__this, ___item, method) (( int32_t (*) (List_1_t164 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m12820_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m13997(__this, ___index, ___item, method) (( void (*) (List_1_t164 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m12822_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m13998(__this, ___item, method) (( void (*) (List_1_t164 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m12824_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13999(__this, method) (( bool (*) (List_1_t164 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m14000(__this, method) (( bool (*) (List_1_t164 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m12828_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m14001(__this, method) (( Object_t * (*) (List_1_t164 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m12830_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m14002(__this, method) (( bool (*) (List_1_t164 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m12832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m14003(__this, method) (( bool (*) (List_1_t164 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m12834_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m14004(__this, ___index, method) (( Object_t * (*) (List_1_t164 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m12836_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m14005(__this, ___index, ___value, method) (( void (*) (List_1_t164 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12838_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Add(T)
#define List_1_Add_m14006(__this, ___item, method) (( void (*) (List_1_t164 *, Transform_t84 *, const MethodInfo*))List_1_Add_m12840_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m14007(__this, ___newCount, method) (( void (*) (List_1_t164 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m12842_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m14008(__this, ___collection, method) (( void (*) (List_1_t164 *, Object_t*, const MethodInfo*))List_1_AddCollection_m12844_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m14009(__this, ___enumerable, method) (( void (*) (List_1_t164 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m12846_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m14010(__this, ___collection, method) (( void (*) (List_1_t164 *, Object_t*, const MethodInfo*))List_1_AddRange_m12848_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Transform>::AsReadOnly()
#define List_1_AsReadOnly_m14011(__this, method) (( ReadOnlyCollection_1_t2265 * (*) (List_1_t164 *, const MethodInfo*))List_1_AsReadOnly_m12850_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Clear()
#define List_1_Clear_m14012(__this, method) (( void (*) (List_1_t164 *, const MethodInfo*))List_1_Clear_m12852_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::Contains(T)
#define List_1_Contains_m14013(__this, ___item, method) (( bool (*) (List_1_t164 *, Transform_t84 *, const MethodInfo*))List_1_Contains_m12854_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m14014(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t164 *, TransformU5BU5D_t2264*, int32_t, const MethodInfo*))List_1_CopyTo_m12856_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Transform>::Find(System.Predicate`1<T>)
#define List_1_Find_m14015(__this, ___match, method) (( Transform_t84 * (*) (List_1_t164 *, Predicate_1_t2266 *, const MethodInfo*))List_1_Find_m12858_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m14016(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2266 *, const MethodInfo*))List_1_CheckMatch_m12860_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m14017(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t164 *, int32_t, int32_t, Predicate_1_t2266 *, const MethodInfo*))List_1_GetIndex_m12862_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Transform>::GetEnumerator()
#define List_1_GetEnumerator_m14018(__this, method) (( Enumerator_t2267  (*) (List_1_t164 *, const MethodInfo*))List_1_GetEnumerator_m12864_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::IndexOf(T)
#define List_1_IndexOf_m14019(__this, ___item, method) (( int32_t (*) (List_1_t164 *, Transform_t84 *, const MethodInfo*))List_1_IndexOf_m12866_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m14020(__this, ___start, ___delta, method) (( void (*) (List_1_t164 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m12868_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m14021(__this, ___index, method) (( void (*) (List_1_t164 *, int32_t, const MethodInfo*))List_1_CheckIndex_m12870_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Insert(System.Int32,T)
#define List_1_Insert_m14022(__this, ___index, ___item, method) (( void (*) (List_1_t164 *, int32_t, Transform_t84 *, const MethodInfo*))List_1_Insert_m12872_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m14023(__this, ___collection, method) (( void (*) (List_1_t164 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m12874_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::Remove(T)
#define List_1_Remove_m14024(__this, ___item, method) (( bool (*) (List_1_t164 *, Transform_t84 *, const MethodInfo*))List_1_Remove_m12876_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m14025(__this, ___match, method) (( int32_t (*) (List_1_t164 *, Predicate_1_t2266 *, const MethodInfo*))List_1_RemoveAll_m12878_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m14026(__this, ___index, method) (( void (*) (List_1_t164 *, int32_t, const MethodInfo*))List_1_RemoveAt_m12880_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Reverse()
#define List_1_Reverse_m14027(__this, method) (( void (*) (List_1_t164 *, const MethodInfo*))List_1_Reverse_m12882_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Sort()
#define List_1_Sort_m14028(__this, method) (( void (*) (List_1_t164 *, const MethodInfo*))List_1_Sort_m12884_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m14029(__this, ___comparison, method) (( void (*) (List_1_t164 *, Comparison_1_t2268 *, const MethodInfo*))List_1_Sort_m12886_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Transform>::ToArray()
#define List_1_ToArray_m14030(__this, method) (( TransformU5BU5D_t2264* (*) (List_1_t164 *, const MethodInfo*))List_1_ToArray_m12888_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::TrimExcess()
#define List_1_TrimExcess_m14031(__this, method) (( void (*) (List_1_t164 *, const MethodInfo*))List_1_TrimExcess_m12890_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Capacity()
#define List_1_get_Capacity_m14032(__this, method) (( int32_t (*) (List_1_t164 *, const MethodInfo*))List_1_get_Capacity_m12892_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m14033(__this, ___value, method) (( void (*) (List_1_t164 *, int32_t, const MethodInfo*))List_1_set_Capacity_m12894_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count()
#define List_1_get_Count_m14034(__this, method) (( int32_t (*) (List_1_t164 *, const MethodInfo*))List_1_get_Count_m12896_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32)
#define List_1_get_Item_m14035(__this, ___index, method) (( Transform_t84 * (*) (List_1_t164 *, int32_t, const MethodInfo*))List_1_get_Item_m12898_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::set_Item(System.Int32,T)
#define List_1_set_Item_m14036(__this, ___index, ___value, method) (( void (*) (List_1_t164 *, int32_t, Transform_t84 *, const MethodInfo*))List_1_set_Item_m12900_gshared)(__this, ___index, ___value, method)
