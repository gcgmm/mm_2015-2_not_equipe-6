﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_2MethodDeclarations.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::.ctor()
#define IndexedSet_1__ctor_m1851(__this, method) (( void (*) (IndexedSet_1_t205 *, const MethodInfo*))IndexedSet_1__ctor_m14557_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m14558(__this, method) (( Object_t * (*) (IndexedSet_1_t205 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m14559_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define IndexedSet_1_Add_m14560(__this, ___item, method) (( void (*) (IndexedSet_1_t205 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m14561_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define IndexedSet_1_Remove_m14562(__this, ___item, method) (( bool (*) (IndexedSet_1_t205 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m14563_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m14564(__this, method) (( Object_t* (*) (IndexedSet_1_t205 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m14565_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Clear()
#define IndexedSet_1_Clear_m14566(__this, method) (( void (*) (IndexedSet_1_t205 *, const MethodInfo*))IndexedSet_1_Clear_m14567_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define IndexedSet_1_Contains_m14568(__this, ___item, method) (( bool (*) (IndexedSet_1_t205 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m14569_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m14570(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t205 *, ICanvasElementU5BU5D_t2323*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m14571_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define IndexedSet_1_get_Count_m14572(__this, method) (( int32_t (*) (IndexedSet_1_t205 *, const MethodInfo*))IndexedSet_1_get_Count_m14573_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m14574(__this, method) (( bool (*) (IndexedSet_1_t205 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m14575_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define IndexedSet_1_IndexOf_m14576(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t205 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m14577_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m14578(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t205 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m14579_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m14580(__this, ___index, method) (( void (*) (IndexedSet_1_t205 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m14581_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m14582(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t205 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m14583_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m14584(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t205 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m14585_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m14586(__this, ___match, method) (( void (*) (IndexedSet_1_t205 *, Predicate_1_t2326 *, const MethodInfo*))IndexedSet_1_RemoveAll_m14587_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m1855(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t205 *, Comparison_1_t206 *, const MethodInfo*))IndexedSet_1_Sort_m14588_gshared)(__this, ___sortLayoutFunction, method)
