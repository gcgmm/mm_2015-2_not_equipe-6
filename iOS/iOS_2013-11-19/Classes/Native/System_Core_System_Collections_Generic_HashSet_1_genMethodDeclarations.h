﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::.ctor()
#define HashSet_1__ctor_m5551(__this, method) (( void (*) (HashSet_1_t917 *, const MethodInfo*))HashSet_1__ctor_m24251_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1__ctor_m24252(__this, ___info, ___context, method) (( void (*) (HashSet_1_t917 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))HashSet_1__ctor_m24253_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24254(__this, method) (( Object_t* (*) (HashSet_1_t917 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24255_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24256(__this, method) (( bool (*) (HashSet_1_t917 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24257_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m24258(__this, ___array, ___index, method) (( void (*) (HashSet_1_t917 *, MeshRendererU5BU5D_t2978*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m24259_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::System.Collections.Generic.ICollection<T>.Add(T)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24260(__this, ___item, method) (( void (*) (HashSet_1_t917 *, MeshRenderer_t113 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24261_gshared)(__this, ___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::System.Collections.IEnumerable.GetEnumerator()
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m24262(__this, method) (( Object_t * (*) (HashSet_1_t917 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m24263_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::get_Count()
#define HashSet_1_get_Count_m24264(__this, method) (( int32_t (*) (HashSet_1_t917 *, const MethodInfo*))HashSet_1_get_Count_m24265_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1_Init_m24266(__this, ___capacity, ___comparer, method) (( void (*) (HashSet_1_t917 *, int32_t, Object_t*, const MethodInfo*))HashSet_1_Init_m24267_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::InitArrays(System.Int32)
#define HashSet_1_InitArrays_m24268(__this, ___size, method) (( void (*) (HashSet_1_t917 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m24269_gshared)(__this, ___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::SlotsContainsAt(System.Int32,System.Int32,T)
#define HashSet_1_SlotsContainsAt_m24270(__this, ___index, ___hash, ___item, method) (( bool (*) (HashSet_1_t917 *, int32_t, int32_t, MeshRenderer_t113 *, const MethodInfo*))HashSet_1_SlotsContainsAt_m24271_gshared)(__this, ___index, ___hash, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::CopyTo(T[],System.Int32)
#define HashSet_1_CopyTo_m24272(__this, ___array, ___index, method) (( void (*) (HashSet_1_t917 *, MeshRendererU5BU5D_t2978*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m24273_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::CopyTo(T[],System.Int32,System.Int32)
#define HashSet_1_CopyTo_m24274(__this, ___array, ___index, ___count, method) (( void (*) (HashSet_1_t917 *, MeshRendererU5BU5D_t2978*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m24275_gshared)(__this, ___array, ___index, ___count, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::Resize()
#define HashSet_1_Resize_m24276(__this, method) (( void (*) (HashSet_1_t917 *, const MethodInfo*))HashSet_1_Resize_m24277_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::GetLinkHashCode(System.Int32)
#define HashSet_1_GetLinkHashCode_m24278(__this, ___index, method) (( int32_t (*) (HashSet_1_t917 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m24279_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::GetItemHashCode(T)
#define HashSet_1_GetItemHashCode_m24280(__this, ___item, method) (( int32_t (*) (HashSet_1_t917 *, MeshRenderer_t113 *, const MethodInfo*))HashSet_1_GetItemHashCode_m24281_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::Add(T)
#define HashSet_1_Add_m5545(__this, ___item, method) (( bool (*) (HashSet_1_t917 *, MeshRenderer_t113 *, const MethodInfo*))HashSet_1_Add_m24282_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::Clear()
#define HashSet_1_Clear_m24283(__this, method) (( void (*) (HashSet_1_t917 *, const MethodInfo*))HashSet_1_Clear_m24284_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::Contains(T)
#define HashSet_1_Contains_m24285(__this, ___item, method) (( bool (*) (HashSet_1_t917 *, MeshRenderer_t113 *, const MethodInfo*))HashSet_1_Contains_m24286_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::Remove(T)
#define HashSet_1_Remove_m24287(__this, ___item, method) (( bool (*) (HashSet_1_t917 *, MeshRenderer_t113 *, const MethodInfo*))HashSet_1_Remove_m24288_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1_GetObjectData_m24289(__this, ___info, ___context, method) (( void (*) (HashSet_1_t917 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))HashSet_1_GetObjectData_m24290_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::OnDeserialization(System.Object)
#define HashSet_1_OnDeserialization_m24291(__this, ___sender, method) (( void (*) (HashSet_1_t917 *, Object_t *, const MethodInfo*))HashSet_1_OnDeserialization_m24292_gshared)(__this, ___sender, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::GetEnumerator()
#define HashSet_1_GetEnumerator_m5546(__this, method) (( Enumerator_t1055  (*) (HashSet_1_t917 *, const MethodInfo*))HashSet_1_GetEnumerator_m24293_gshared)(__this, method)
