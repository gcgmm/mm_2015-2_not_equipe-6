﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_26MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m21961(__this, ___host, method) (( void (*) (Enumerator_t994 *, Dictionary_2_t857 *, const MethodInfo*))Enumerator__ctor_m14335_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21962(__this, method) (( Object_t * (*) (Enumerator_t994 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14336_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m21963(__this, method) (( void (*) (Enumerator_t994 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14337_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m5384(__this, method) (( void (*) (Enumerator_t994 *, const MethodInfo*))Enumerator_Dispose_m14338_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m5383(__this, method) (( bool (*) (Enumerator_t994 *, const MethodInfo*))Enumerator_MoveNext_m14339_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m5382(__this, method) (( WordAbstractBehaviour_t80 * (*) (Enumerator_t994 *, const MethodInfo*))Enumerator_get_Current_m14340_gshared)(__this, method)
