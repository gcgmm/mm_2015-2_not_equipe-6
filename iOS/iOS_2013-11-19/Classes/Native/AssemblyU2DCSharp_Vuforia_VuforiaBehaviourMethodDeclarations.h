﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t72;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.VuforiaBehaviour::.ctor()
extern "C" void VuforiaBehaviour__ctor_m126 (VuforiaBehaviour_t72 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaBehaviour::.cctor()
extern "C" void VuforiaBehaviour__cctor_m127 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaBehaviour::Awake()
extern "C" void VuforiaBehaviour_Awake_m128 (VuforiaBehaviour_t72 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::get_Instance()
extern "C" VuforiaBehaviour_t72 * VuforiaBehaviour_get_Instance_m129 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
