﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_1MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m22374(__this, ___object, ___method, method) (( void (*) (Transform_1_t2853 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m14345_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m22375(__this, ___key, ___value, method) (( DictionaryEntry_t1423  (*) (Transform_1_t2853 *, int32_t, SurfaceAbstractBehaviour_t58 *, const MethodInfo*))Transform_1_Invoke_m14346_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m22376(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2853 *, int32_t, SurfaceAbstractBehaviour_t58 *, AsyncCallback_t261 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m14347_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m22377(__this, ___result, method) (( DictionaryEntry_t1423  (*) (Transform_1_t2853 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m14348_gshared)(__this, ___result, method)
