﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.Prop
struct Prop_t81;
// UnityEngine.BoxCollider
struct BoxCollider_t537;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"

// Vuforia.PropAbstractBehaviour
struct  PropAbstractBehaviour_t52  : public SmartTerrainTrackableBehaviour_t761
{
	// Vuforia.Prop Vuforia.PropAbstractBehaviour::mProp
	Object_t * ___mProp_13;
	// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::mBoxColliderToUpdate
	BoxCollider_t537 * ___mBoxColliderToUpdate_14;
};
