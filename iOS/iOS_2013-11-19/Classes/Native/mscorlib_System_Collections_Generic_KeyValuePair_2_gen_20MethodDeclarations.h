﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m20467(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2745 *, int32_t, VirtualButton_t895 *, const MethodInfo*))KeyValuePair_2__ctor_m14271_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::get_Key()
#define KeyValuePair_2_get_Key_m20468(__this, method) (( int32_t (*) (KeyValuePair_2_t2745 *, const MethodInfo*))KeyValuePair_2_get_Key_m14272_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20469(__this, ___value, method) (( void (*) (KeyValuePair_2_t2745 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m14273_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::get_Value()
#define KeyValuePair_2_get_Value_m20470(__this, method) (( VirtualButton_t895 * (*) (KeyValuePair_2_t2745 *, const MethodInfo*))KeyValuePair_2_get_Value_m14274_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20471(__this, ___value, method) (( void (*) (KeyValuePair_2_t2745 *, VirtualButton_t895 *, const MethodInfo*))KeyValuePair_2_set_Value_m14275_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::ToString()
#define KeyValuePair_2_ToString_m20472(__this, method) (( String_t* (*) (KeyValuePair_2_t2745 *, const MethodInfo*))KeyValuePair_2_ToString_m14276_gshared)(__this, method)
