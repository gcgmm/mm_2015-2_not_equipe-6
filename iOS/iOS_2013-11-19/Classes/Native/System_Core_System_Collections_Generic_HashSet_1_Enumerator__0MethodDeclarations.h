﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t2981;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C" void Enumerator__ctor_m24300_gshared (Enumerator_t2985 * __this, HashSet_1_t2981 * ___hashset, const MethodInfo* method);
#define Enumerator__ctor_m24300(__this, ___hashset, method) (( void (*) (Enumerator_t2985 *, HashSet_1_t2981 *, const MethodInfo*))Enumerator__ctor_m24300_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m24301_gshared (Enumerator_t2985 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m24301(__this, method) (( Object_t * (*) (Enumerator_t2985 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24301_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m24302_gshared (Enumerator_t2985 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m24302(__this, method) (( void (*) (Enumerator_t2985 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m24302_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m24303_gshared (Enumerator_t2985 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m24303(__this, method) (( bool (*) (Enumerator_t2985 *, const MethodInfo*))Enumerator_MoveNext_m24303_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m24304_gshared (Enumerator_t2985 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m24304(__this, method) (( Object_t * (*) (Enumerator_t2985 *, const MethodInfo*))Enumerator_get_Current_m24304_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m24305_gshared (Enumerator_t2985 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m24305(__this, method) (( void (*) (Enumerator_t2985 *, const MethodInfo*))Enumerator_Dispose_m24305_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::CheckState()
extern "C" void Enumerator_CheckState_m24306_gshared (Enumerator_t2985 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m24306(__this, method) (( void (*) (Enumerator_t2985 *, const MethodInfo*))Enumerator_CheckState_m24306_gshared)(__this, method)
