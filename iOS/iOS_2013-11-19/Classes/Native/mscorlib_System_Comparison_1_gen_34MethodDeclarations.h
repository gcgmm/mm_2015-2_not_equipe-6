﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t2592;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t260;
// System.AsyncCallback
struct AsyncCallback_t261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Comparison`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m18318_gshared (Comparison_1_t2592 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m18318(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2592 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m18318_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.UICharInfo>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m18319_gshared (Comparison_1_t2592 * __this, UICharInfo_t430  ___x, UICharInfo_t430  ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m18319(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2592 *, UICharInfo_t430 , UICharInfo_t430 , const MethodInfo*))Comparison_1_Invoke_m18319_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.UICharInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m18320_gshared (Comparison_1_t2592 * __this, UICharInfo_t430  ___x, UICharInfo_t430  ___y, AsyncCallback_t261 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m18320(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2592 *, UICharInfo_t430 , UICharInfo_t430 , AsyncCallback_t261 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m18320_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m18321_gshared (Comparison_1_t2592 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m18321(__this, ___result, method) (( int32_t (*) (Comparison_1_t2592 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m18321_gshared)(__this, ___result, method)
