﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__32MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m23781(__this, ___dictionary, method) (( void (*) (Enumerator_t2952 *, Dictionary_2_t900 *, const MethodInfo*))Enumerator__ctor_m23678_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23782(__this, method) (( Object_t * (*) (Enumerator_t2952 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23679_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m23783(__this, method) (( void (*) (Enumerator_t2952 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m23680_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m23784(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t2952 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m23681_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m23785(__this, method) (( Object_t * (*) (Enumerator_t2952 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m23682_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m23786(__this, method) (( Object_t * (*) (Enumerator_t2952 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m23683_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::MoveNext()
#define Enumerator_MoveNext_m23787(__this, method) (( bool (*) (Enumerator_t2952 *, const MethodInfo*))Enumerator_MoveNext_m23684_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_Current()
#define Enumerator_get_Current_m23788(__this, method) (( KeyValuePair_2_t2949  (*) (Enumerator_t2952 *, const MethodInfo*))Enumerator_get_Current_m23685_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m23789(__this, method) (( String_t* (*) (Enumerator_t2952 *, const MethodInfo*))Enumerator_get_CurrentKey_m23686_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m23790(__this, method) (( ProfileData_t898  (*) (Enumerator_t2952 *, const MethodInfo*))Enumerator_get_CurrentValue_m23687_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::Reset()
#define Enumerator_Reset_m23791(__this, method) (( void (*) (Enumerator_t2952 *, const MethodInfo*))Enumerator_Reset_m23688_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::VerifyState()
#define Enumerator_VerifyState_m23792(__this, method) (( void (*) (Enumerator_t2952 *, const MethodInfo*))Enumerator_VerifyState_m23689_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m23793(__this, method) (( void (*) (Enumerator_t2952 *, const MethodInfo*))Enumerator_VerifyCurrent_m23690_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::Dispose()
#define Enumerator_Dispose_m23794(__this, method) (( void (*) (Enumerator_t2952 *, const MethodInfo*))Enumerator_Dispose_m23691_gshared)(__this, method)
