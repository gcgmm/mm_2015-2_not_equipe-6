﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.SignatureDescription
struct SignatureDescription_t1925;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.SignatureDescription::.ctor()
extern "C" void SignatureDescription__ctor_m11400 (SignatureDescription_t1925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_DeformatterAlgorithm(System.String)
extern "C" void SignatureDescription_set_DeformatterAlgorithm_m11401 (SignatureDescription_t1925 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_DigestAlgorithm(System.String)
extern "C" void SignatureDescription_set_DigestAlgorithm_m11402 (SignatureDescription_t1925 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_FormatterAlgorithm(System.String)
extern "C" void SignatureDescription_set_FormatterAlgorithm_m11403 (SignatureDescription_t1925 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_KeyAlgorithm(System.String)
extern "C" void SignatureDescription_set_KeyAlgorithm_m11404 (SignatureDescription_t1925 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
