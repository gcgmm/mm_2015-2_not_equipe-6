﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.SmartTerrainTrackable[]
struct SmartTerrainTrackableU5BU5D_t2777;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
struct  List_1_t837  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::_items
	SmartTerrainTrackableU5BU5D_t2777* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::_version
	int32_t ____version_3;
};
struct List_1_t837_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::EmptyArray
	SmartTerrainTrackableU5BU5D_t2777* ___EmptyArray_4;
};
