﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Dropdown/DropdownItem
struct DropdownItem_t211;
// System.IAsyncResult
struct IAsyncResult_t260;
// System.AsyncCallback
struct AsyncCallback_t261;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// System.Comparison`1<UnityEngine.UI.Dropdown/DropdownItem>
struct  Comparison_1_t2340  : public MulticastDelegate_t259
{
};
