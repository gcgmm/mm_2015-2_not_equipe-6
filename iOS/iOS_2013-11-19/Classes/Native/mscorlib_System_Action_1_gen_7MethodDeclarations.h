﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen_9MethodDeclarations.h"

// System.Void System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m17893(__this, ___object, ___method, method) (( void (*) (Action_1_t478 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m13079_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>::Invoke(T)
#define Action_1_Invoke_m3568(__this, ___obj, method) (( void (*) (Action_1_t478 *, IUserProfileU5BU5D_t626*, const MethodInfo*))Action_1_Invoke_m13080_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m17894(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t478 *, IUserProfileU5BU5D_t626*, AsyncCallback_t261 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m13082_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m17895(__this, ___result, method) (( void (*) (Action_1_t478 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m13084_gshared)(__this, ___result, method)
