﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>
struct GenericEqualityComparer_1_t2169;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m12782_gshared (GenericEqualityComparer_1_t2169 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m12782(__this, method) (( void (*) (GenericEqualityComparer_1_t2169 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m12782_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m25570_gshared (GenericEqualityComparer_1_t2169 * __this, TimeSpan_t975  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m25570(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2169 *, TimeSpan_t975 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m25570_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m25571_gshared (GenericEqualityComparer_1_t2169 * __this, TimeSpan_t975  ___x, TimeSpan_t975  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m25571(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2169 *, TimeSpan_t975 , TimeSpan_t975 , const MethodInfo*))GenericEqualityComparer_1_Equals_m25571_gshared)(__this, ___x, ___y, method)
