﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m19427(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2679 *, Camera_t91 *, VideoBackgroundAbstractBehaviour_t67 *, const MethodInfo*))KeyValuePair_2__ctor_m15296_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m19428(__this, method) (( Camera_t91 * (*) (KeyValuePair_2_t2679 *, const MethodInfo*))KeyValuePair_2_get_Key_m15297_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m19429(__this, ___value, method) (( void (*) (KeyValuePair_2_t2679 *, Camera_t91 *, const MethodInfo*))KeyValuePair_2_set_Key_m15298_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m19430(__this, method) (( VideoBackgroundAbstractBehaviour_t67 * (*) (KeyValuePair_2_t2679 *, const MethodInfo*))KeyValuePair_2_get_Value_m15299_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m19431(__this, ___value, method) (( void (*) (KeyValuePair_2_t2679 *, VideoBackgroundAbstractBehaviour_t67 *, const MethodInfo*))KeyValuePair_2_set_Value_m15300_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::ToString()
#define KeyValuePair_2_ToString_m19432(__this, method) (( String_t* (*) (KeyValuePair_2_t2679 *, const MethodInfo*))KeyValuePair_2_ToString_m15301_gshared)(__this, method)
