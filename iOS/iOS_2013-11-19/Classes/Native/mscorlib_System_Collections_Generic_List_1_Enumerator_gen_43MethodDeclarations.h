﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.RectMask2D>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m16201(__this, ___l, method) (( void (*) (Enumerator_t2426 *, List_1_t285 *, const MethodInfo*))Enumerator__ctor_m12901_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.RectMask2D>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m16202(__this, method) (( void (*) (Enumerator_t2426 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m12902_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.RectMask2D>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16203(__this, method) (( Object_t * (*) (Enumerator_t2426 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12903_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.RectMask2D>::Dispose()
#define Enumerator_Dispose_m16204(__this, method) (( void (*) (Enumerator_t2426 *, const MethodInfo*))Enumerator_Dispose_m12904_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.RectMask2D>::VerifyState()
#define Enumerator_VerifyState_m16205(__this, method) (( void (*) (Enumerator_t2426 *, const MethodInfo*))Enumerator_VerifyState_m12905_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.RectMask2D>::MoveNext()
#define Enumerator_MoveNext_m16206(__this, method) (( bool (*) (Enumerator_t2426 *, const MethodInfo*))Enumerator_MoveNext_m12906_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.RectMask2D>::get_Current()
#define Enumerator_get_Current_m16207(__this, method) (( RectMask2D_t276 * (*) (Enumerator_t2426 *, const MethodInfo*))Enumerator_get_Current_m12907_gshared)(__this, method)
