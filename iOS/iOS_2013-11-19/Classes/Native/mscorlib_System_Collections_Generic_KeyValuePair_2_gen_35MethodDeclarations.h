﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_35.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m24577_gshared (KeyValuePair_2_t3005 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m24577(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3005 *, Object_t *, bool, const MethodInfo*))KeyValuePair_2__ctor_m24577_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m24578_gshared (KeyValuePair_2_t3005 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m24578(__this, method) (( Object_t * (*) (KeyValuePair_2_t3005 *, const MethodInfo*))KeyValuePair_2_get_Key_m24578_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m24579_gshared (KeyValuePair_2_t3005 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m24579(__this, ___value, method) (( void (*) (KeyValuePair_2_t3005 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m24579_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C" bool KeyValuePair_2_get_Value_m24580_gshared (KeyValuePair_2_t3005 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m24580(__this, method) (( bool (*) (KeyValuePair_2_t3005 *, const MethodInfo*))KeyValuePair_2_get_Value_m24580_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m24581_gshared (KeyValuePair_2_t3005 * __this, bool ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m24581(__this, ___value, method) (( void (*) (KeyValuePair_2_t3005 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m24581_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m24582_gshared (KeyValuePair_2_t3005 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m24582(__this, method) (( String_t* (*) (KeyValuePair_2_t3005 *, const MethodInfo*))KeyValuePair_2_ToString_m24582_gshared)(__this, method)
