﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DockController
struct DockController_t7;
// UnityEngine.Material
struct Material_t8;

#include "codegen/il2cpp-codegen.h"

// System.Void DockController::.ctor()
extern "C" void DockController__ctor_m5 (DockController_t7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DockController::Awake()
extern "C" void DockController_Awake_m6 (DockController_t7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DockController::Update()
extern "C" void DockController_Update_m7 (DockController_t7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DockController::setDockedColor(UnityEngine.Material)
extern "C" void DockController_setDockedColor_m8 (DockController_t7 * __this, Material_t8 * ___color, const MethodInfo* method) IL2CPP_METHOD_ATTR;
