﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t8;
// UnityEngine.Material[]
struct MaterialU5BU5D_t4;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t17;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// RowController
struct  RowController_t10  : public MonoBehaviour_t2
{
	// UnityEngine.Material RowController::BlackHintColor
	Material_t8 * ___BlackHintColor_2;
	// UnityEngine.Material RowController::WhiteHintColor
	Material_t8 * ___WhiteHintColor_3;
	// System.Int32 RowController::rowLength
	int32_t ___rowLength_4;
	// System.Boolean RowController::active
	bool ___active_5;
	// UnityEngine.Material[] RowController::rowCode
	MaterialU5BU5D_t4* ___rowCode_6;
	// UnityEngine.GameObject[] RowController::hints
	GameObjectU5BU5D_t17* ___hints_7;
};
