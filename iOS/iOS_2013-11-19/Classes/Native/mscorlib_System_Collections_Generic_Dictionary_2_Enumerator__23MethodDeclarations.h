﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m21397(__this, ___dictionary, method) (( void (*) (Enumerator_t2810 *, Dictionary_2_t848 *, const MethodInfo*))Enumerator__ctor_m21291_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21398(__this, method) (( Object_t * (*) (Enumerator_t2810 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21292_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m21399(__this, method) (( void (*) (Enumerator_t2810 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m21293_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21400(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t2810 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21294_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21401(__this, method) (( Object_t * (*) (Enumerator_t2810 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21295_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21402(__this, method) (( Object_t * (*) (Enumerator_t2810 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21296_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::MoveNext()
#define Enumerator_MoveNext_m21403(__this, method) (( bool (*) (Enumerator_t2810 *, const MethodInfo*))Enumerator_MoveNext_m21297_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_Current()
#define Enumerator_get_Current_m21404(__this, method) (( KeyValuePair_2_t2807  (*) (Enumerator_t2810 *, const MethodInfo*))Enumerator_get_Current_m21298_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m21405(__this, method) (( Type_t * (*) (Enumerator_t2810 *, const MethodInfo*))Enumerator_get_CurrentKey_m21299_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m21406(__this, method) (( uint16_t (*) (Enumerator_t2810 *, const MethodInfo*))Enumerator_get_CurrentValue_m21300_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::Reset()
#define Enumerator_Reset_m21407(__this, method) (( void (*) (Enumerator_t2810 *, const MethodInfo*))Enumerator_Reset_m21301_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::VerifyState()
#define Enumerator_VerifyState_m21408(__this, method) (( void (*) (Enumerator_t2810 *, const MethodInfo*))Enumerator_VerifyState_m21302_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m21409(__this, method) (( void (*) (Enumerator_t2810 *, const MethodInfo*))Enumerator_VerifyCurrent_m21303_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::Dispose()
#define Enumerator_Dispose_m21410(__this, method) (( void (*) (Enumerator_t2810 *, const MethodInfo*))Enumerator_Dispose_m21304_gshared)(__this, method)
