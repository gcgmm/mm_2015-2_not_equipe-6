﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievement>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m17878(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2555 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m12785_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievement>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17879(__this, method) (( void (*) (InternalEnumerator_1_t2555 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12787_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievement>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17880(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2555 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12789_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievement>::Dispose()
#define InternalEnumerator_1_Dispose_m17881(__this, method) (( void (*) (InternalEnumerator_1_t2555 *, const MethodInfo*))InternalEnumerator_1_Dispose_m12791_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievement>::MoveNext()
#define InternalEnumerator_1_MoveNext_m17882(__this, method) (( bool (*) (InternalEnumerator_1_t2555 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m12793_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievement>::get_Current()
#define InternalEnumerator_1_get_Current_m17883(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2555 *, const MethodInfo*))InternalEnumerator_1_get_Current_m12795_gshared)(__this, method)
