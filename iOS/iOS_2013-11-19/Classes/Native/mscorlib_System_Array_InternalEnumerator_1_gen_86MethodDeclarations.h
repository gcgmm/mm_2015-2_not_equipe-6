﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_86.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24439_gshared (InternalEnumerator_1_t2998 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m24439(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2998 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m24439_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24440_gshared (InternalEnumerator_1_t2998 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24440(__this, method) (( void (*) (InternalEnumerator_1_t2998 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24440_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24441_gshared (InternalEnumerator_1_t2998 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24441(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2998 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24441_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24442_gshared (InternalEnumerator_1_t2998 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24442(__this, method) (( void (*) (InternalEnumerator_1_t2998 *, const MethodInfo*))InternalEnumerator_1_Dispose_m24442_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24443_gshared (InternalEnumerator_1_t2998 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24443(__this, method) (( bool (*) (InternalEnumerator_1_t2998 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m24443_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m24444_gshared (InternalEnumerator_1_t2998 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24444(__this, method) (( int32_t (*) (InternalEnumerator_1_t2998 *, const MethodInfo*))InternalEnumerator_1_get_Current_m24444_gshared)(__this, method)
