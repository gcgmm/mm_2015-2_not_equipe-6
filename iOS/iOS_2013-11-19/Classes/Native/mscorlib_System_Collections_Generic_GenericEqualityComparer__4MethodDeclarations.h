﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>
struct GenericEqualityComparer_1_t2805;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m21353_gshared (GenericEqualityComparer_1_t2805 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m21353(__this, method) (( void (*) (GenericEqualityComparer_1_t2805 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m21353_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m21354_gshared (GenericEqualityComparer_1_t2805 * __this, uint16_t ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m21354(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2805 *, uint16_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m21354_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m21355_gshared (GenericEqualityComparer_1_t2805 * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m21355(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2805 *, uint16_t, uint16_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m21355_gshared)(__this, ___x, ___y, method)
