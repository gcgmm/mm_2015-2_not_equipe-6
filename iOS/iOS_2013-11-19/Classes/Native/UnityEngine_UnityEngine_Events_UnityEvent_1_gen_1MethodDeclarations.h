﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t195;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t377;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t655;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::.ctor()
extern "C" void UnityEvent_1__ctor_m1842_gshared (UnityEvent_1_t195 * __this, const MethodInfo* method);
#define UnityEvent_1__ctor_m1842(__this, method) (( void (*) (UnityEvent_1_t195 *, const MethodInfo*))UnityEvent_1__ctor_m1842_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_AddListener_m1845_gshared (UnityEvent_1_t195 * __this, UnityAction_1_t377 * ___call, const MethodInfo* method);
#define UnityEvent_1_AddListener_m1845(__this, ___call, method) (( void (*) (UnityEvent_1_t195 *, UnityAction_1_t377 *, const MethodInfo*))UnityEvent_1_AddListener_m1845_gshared)(__this, ___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_RemoveListener_m2171_gshared (UnityEvent_1_t195 * __this, UnityAction_1_t377 * ___call, const MethodInfo* method);
#define UnityEvent_1_RemoveListener_m2171(__this, ___call, method) (( void (*) (UnityEvent_1_t195 *, UnityAction_1_t377 *, const MethodInfo*))UnityEvent_1_RemoveListener_m2171_gshared)(__this, ___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Single>::FindMethod_Impl(System.String,System.Object)
extern "C" MethodInfo_t * UnityEvent_1_FindMethod_Impl_m14547_gshared (UnityEvent_1_t195 * __this, String_t* ___name, Object_t * ___targetObj, const MethodInfo* method);
#define UnityEvent_1_FindMethod_Impl_m14547(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_1_t195 *, String_t*, Object_t *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m14547_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C" BaseInvokableCall_t655 * UnityEvent_1_GetDelegate_m14548_gshared (UnityEvent_1_t195 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m14548(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t655 * (*) (UnityEvent_1_t195 *, Object_t *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m14548_gshared)(__this, ___target, ___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C" BaseInvokableCall_t655 * UnityEvent_1_GetDelegate_m14549_gshared (Object_t * __this /* static, unused */, UnityAction_1_t377 * ___action, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m14549(__this /* static, unused */, ___action, method) (( BaseInvokableCall_t655 * (*) (Object_t * /* static, unused */, UnityAction_1_t377 *, const MethodInfo*))UnityEvent_1_GetDelegate_m14549_gshared)(__this /* static, unused */, ___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::Invoke(T0)
extern "C" void UnityEvent_1_Invoke_m1844_gshared (UnityEvent_1_t195 * __this, float ___arg0, const MethodInfo* method);
#define UnityEvent_1_Invoke_m1844(__this, ___arg0, method) (( void (*) (UnityEvent_1_t195 *, float, const MethodInfo*))UnityEvent_1_Invoke_m1844_gshared)(__this, ___arg0, method)
