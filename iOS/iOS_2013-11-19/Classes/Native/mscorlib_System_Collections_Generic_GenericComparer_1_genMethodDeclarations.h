﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct GenericComparer_1_t2161;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTime>::.ctor()
extern "C" void GenericComparer_1__ctor_m12771_gshared (GenericComparer_1_t2161 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m12771(__this, method) (( void (*) (GenericComparer_1_t2161 *, const MethodInfo*))GenericComparer_1__ctor_m12771_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m25472_gshared (GenericComparer_1_t2161 * __this, DateTime_t577  ___x, DateTime_t577  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m25472(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2161 *, DateTime_t577 , DateTime_t577 , const MethodInfo*))GenericComparer_1_Compare_m25472_gshared)(__this, ___x, ___y, method)
