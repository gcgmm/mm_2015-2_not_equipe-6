﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_64.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__8.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m20946_gshared (InternalEnumerator_1_t2773 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m20946(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2773 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m20946_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20947_gshared (InternalEnumerator_1_t2773 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20947(__this, method) (( void (*) (InternalEnumerator_1_t2773 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20947_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20948_gshared (InternalEnumerator_1_t2773 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20948(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2773 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20948_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m20949_gshared (InternalEnumerator_1_t2773 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m20949(__this, method) (( void (*) (InternalEnumerator_1_t2773 *, const MethodInfo*))InternalEnumerator_1_Dispose_m20949_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m20950_gshared (InternalEnumerator_1_t2773 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m20950(__this, method) (( bool (*) (InternalEnumerator_1_t2773 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m20950_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::get_Current()
extern "C" SmartTerrainRevisionData_t818  InternalEnumerator_1_get_Current_m20951_gshared (InternalEnumerator_1_t2773 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m20951(__this, method) (( SmartTerrainRevisionData_t818  (*) (InternalEnumerator_1_t2773 *, const MethodInfo*))InternalEnumerator_1_get_Current_m20951_gshared)(__this, method)
