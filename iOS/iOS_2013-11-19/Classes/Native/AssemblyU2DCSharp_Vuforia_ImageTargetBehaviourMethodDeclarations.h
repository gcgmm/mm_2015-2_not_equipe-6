﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t36;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern "C" void ImageTargetBehaviour__ctor_m62 (ImageTargetBehaviour_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
