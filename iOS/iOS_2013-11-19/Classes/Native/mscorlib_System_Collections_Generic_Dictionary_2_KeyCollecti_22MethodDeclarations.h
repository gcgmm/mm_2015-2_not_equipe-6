﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_9MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m19433(__this, ___dictionary, method) (( void (*) (KeyCollection_t2680 *, Dictionary_2_t731 *, const MethodInfo*))KeyCollection__ctor_m15302_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m19434(__this, ___item, method) (( void (*) (KeyCollection_t2680 *, Camera_t91 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15303_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m19435(__this, method) (( void (*) (KeyCollection_t2680 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15304_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m19436(__this, ___item, method) (( bool (*) (KeyCollection_t2680 *, Camera_t91 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15305_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m19437(__this, ___item, method) (( bool (*) (KeyCollection_t2680 *, Camera_t91 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15306_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m19438(__this, method) (( Object_t* (*) (KeyCollection_t2680 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15307_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m19439(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2680 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m15308_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m19440(__this, method) (( Object_t * (*) (KeyCollection_t2680 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15309_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m19441(__this, method) (( bool (*) (KeyCollection_t2680 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15310_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m19442(__this, method) (( bool (*) (KeyCollection_t2680 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15311_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m19443(__this, method) (( Object_t * (*) (KeyCollection_t2680 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m15312_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m19444(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2680 *, CameraU5BU5D_t119*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m15313_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::GetEnumerator()
#define KeyCollection_GetEnumerator_m19445(__this, method) (( Enumerator_t3199  (*) (KeyCollection_t2680 *, const MethodInfo*))KeyCollection_GetEnumerator_m15314_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Count()
#define KeyCollection_get_Count_m19446(__this, method) (( int32_t (*) (KeyCollection_t2680 *, const MethodInfo*))KeyCollection_get_Count_m15315_gshared)(__this, method)
