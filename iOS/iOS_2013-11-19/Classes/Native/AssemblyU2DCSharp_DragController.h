﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t8;
// UnityEngine.Collider
struct Collider_t14;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_LayerMask.h"

// DragController
struct  DragController_t11  : public MonoBehaviour_t2
{
	// UnityEngine.Material DragController::color
	Material_t8 * ___color_2;
	// System.Single DragController::camRayLength
	float ___camRayLength_3;
	// UnityEngine.Vector3 DragController::originalPosition
	Vector3_t12  ___originalPosition_4;
	// UnityEngine.LayerMask DragController::dragLayerMask
	LayerMask_t13  ___dragLayerMask_5;
	// UnityEngine.Collider DragController::dock
	Collider_t14 * ___dock_6;
};
