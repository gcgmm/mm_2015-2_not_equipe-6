﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t349;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1058;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3132;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3166;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
struct ReadOnlyCollection_1_t2490;
// System.Int32[]
struct Int32U5BU5D_t122;
// System.Predicate`1<System.Int32>
struct Predicate_1_t2493;
// System.Comparison`1<System.Int32>
struct Comparison_1_t2497;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_10.h"

// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C" void List_1__ctor_m5386_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1__ctor_m5386(__this, method) (( void (*) (List_1_t349 *, const MethodInfo*))List_1__ctor_m5386_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m5330_gshared (List_1_t349 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m5330(__this, ___collection, method) (( void (*) (List_1_t349 *, Object_t*, const MethodInfo*))List_1__ctor_m5330_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
extern "C" void List_1__ctor_m17144_gshared (List_1_t349 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m17144(__this, ___capacity, method) (( void (*) (List_1_t349 *, int32_t, const MethodInfo*))List_1__ctor_m17144_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.cctor()
extern "C" void List_1__cctor_m17145_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m17145(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m17145_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17146_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17146(__this, method) (( Object_t* (*) (List_1_t349 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17146_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17147_gshared (List_1_t349 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m17147(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t349 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m17147_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m17148_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17148(__this, method) (( Object_t * (*) (List_1_t349 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m17148_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m17149_gshared (List_1_t349 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m17149(__this, ___item, method) (( int32_t (*) (List_1_t349 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m17149_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m17150_gshared (List_1_t349 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m17150(__this, ___item, method) (( bool (*) (List_1_t349 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m17150_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m17151_gshared (List_1_t349 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m17151(__this, ___item, method) (( int32_t (*) (List_1_t349 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m17151_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m17152_gshared (List_1_t349 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m17152(__this, ___index, ___item, method) (( void (*) (List_1_t349 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m17152_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m17153_gshared (List_1_t349 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m17153(__this, ___item, method) (( void (*) (List_1_t349 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m17153_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17154_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17154(__this, method) (( bool (*) (List_1_t349 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17154_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m17155_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17155(__this, method) (( bool (*) (List_1_t349 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m17155_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m17156_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m17156(__this, method) (( Object_t * (*) (List_1_t349 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m17156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m17157_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m17157(__this, method) (( bool (*) (List_1_t349 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m17157_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m17158_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m17158(__this, method) (( bool (*) (List_1_t349 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m17158_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m17159_gshared (List_1_t349 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m17159(__this, ___index, method) (( Object_t * (*) (List_1_t349 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m17159_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m17160_gshared (List_1_t349 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m17160(__this, ___index, ___value, method) (( void (*) (List_1_t349 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m17160_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
extern "C" void List_1_Add_m17161_gshared (List_1_t349 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Add_m17161(__this, ___item, method) (( void (*) (List_1_t349 *, int32_t, const MethodInfo*))List_1_Add_m17161_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m17162_gshared (List_1_t349 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m17162(__this, ___newCount, method) (( void (*) (List_1_t349 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m17162_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m17163_gshared (List_1_t349 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m17163(__this, ___collection, method) (( void (*) (List_1_t349 *, Object_t*, const MethodInfo*))List_1_AddCollection_m17163_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m17164_gshared (List_1_t349 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m17164(__this, ___enumerable, method) (( void (*) (List_1_t349 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m17164_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m2308_gshared (List_1_t349 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m2308(__this, ___collection, method) (( void (*) (List_1_t349 *, Object_t*, const MethodInfo*))List_1_AddRange_m2308_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Int32>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2490 * List_1_AsReadOnly_m17165_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m17165(__this, method) (( ReadOnlyCollection_1_t2490 * (*) (List_1_t349 *, const MethodInfo*))List_1_AsReadOnly_m17165_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C" void List_1_Clear_m17166_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_Clear_m17166(__this, method) (( void (*) (List_1_t349 *, const MethodInfo*))List_1_Clear_m17166_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(T)
extern "C" bool List_1_Contains_m17167_gshared (List_1_t349 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Contains_m17167(__this, ___item, method) (( bool (*) (List_1_t349 *, int32_t, const MethodInfo*))List_1_Contains_m17167_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m17168_gshared (List_1_t349 * __this, Int32U5BU5D_t122* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m17168(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t349 *, Int32U5BU5D_t122*, int32_t, const MethodInfo*))List_1_CopyTo_m17168_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Int32>::Find(System.Predicate`1<T>)
extern "C" int32_t List_1_Find_m17169_gshared (List_1_t349 * __this, Predicate_1_t2493 * ___match, const MethodInfo* method);
#define List_1_Find_m17169(__this, ___match, method) (( int32_t (*) (List_1_t349 *, Predicate_1_t2493 *, const MethodInfo*))List_1_Find_m17169_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m17170_gshared (Object_t * __this /* static, unused */, Predicate_1_t2493 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m17170(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2493 *, const MethodInfo*))List_1_CheckMatch_m17170_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m17171_gshared (List_1_t349 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2493 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m17171(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t349 *, int32_t, int32_t, Predicate_1_t2493 *, const MethodInfo*))List_1_GetIndex_m17171_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t977  List_1_GetEnumerator_m5331_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m5331(__this, method) (( Enumerator_t977  (*) (List_1_t349 *, const MethodInfo*))List_1_GetEnumerator_m5331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m17172_gshared (List_1_t349 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m17172(__this, ___item, method) (( int32_t (*) (List_1_t349 *, int32_t, const MethodInfo*))List_1_IndexOf_m17172_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m17173_gshared (List_1_t349 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m17173(__this, ___start, ___delta, method) (( void (*) (List_1_t349 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m17173_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m17174_gshared (List_1_t349 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m17174(__this, ___index, method) (( void (*) (List_1_t349 *, int32_t, const MethodInfo*))List_1_CheckIndex_m17174_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m17175_gshared (List_1_t349 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define List_1_Insert_m17175(__this, ___index, ___item, method) (( void (*) (List_1_t349 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m17175_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m17176_gshared (List_1_t349 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m17176(__this, ___collection, method) (( void (*) (List_1_t349 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m17176_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(T)
extern "C" bool List_1_Remove_m17177_gshared (List_1_t349 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Remove_m17177(__this, ___item, method) (( bool (*) (List_1_t349 *, int32_t, const MethodInfo*))List_1_Remove_m17177_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m17178_gshared (List_1_t349 * __this, Predicate_1_t2493 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m17178(__this, ___match, method) (( int32_t (*) (List_1_t349 *, Predicate_1_t2493 *, const MethodInfo*))List_1_RemoveAll_m17178_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m17179_gshared (List_1_t349 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m17179(__this, ___index, method) (( void (*) (List_1_t349 *, int32_t, const MethodInfo*))List_1_RemoveAt_m17179_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Reverse()
extern "C" void List_1_Reverse_m17180_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_Reverse_m17180(__this, method) (( void (*) (List_1_t349 *, const MethodInfo*))List_1_Reverse_m17180_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort()
extern "C" void List_1_Sort_m17181_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_Sort_m17181(__this, method) (( void (*) (List_1_t349 *, const MethodInfo*))List_1_Sort_m17181_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m17182_gshared (List_1_t349 * __this, Comparison_1_t2497 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m17182(__this, ___comparison, method) (( void (*) (List_1_t349 *, Comparison_1_t2497 *, const MethodInfo*))List_1_Sort_m17182_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C" Int32U5BU5D_t122* List_1_ToArray_m17183_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_ToArray_m17183(__this, method) (( Int32U5BU5D_t122* (*) (List_1_t349 *, const MethodInfo*))List_1_ToArray_m17183_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::TrimExcess()
extern "C" void List_1_TrimExcess_m17184_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m17184(__this, method) (( void (*) (List_1_t349 *, const MethodInfo*))List_1_TrimExcess_m17184_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m17185_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m17185(__this, method) (( int32_t (*) (List_1_t349 *, const MethodInfo*))List_1_get_Capacity_m17185_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m17186_gshared (List_1_t349 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m17186(__this, ___value, method) (( void (*) (List_1_t349 *, int32_t, const MethodInfo*))List_1_set_Capacity_m17186_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C" int32_t List_1_get_Count_m17187_gshared (List_1_t349 * __this, const MethodInfo* method);
#define List_1_get_Count_m17187(__this, method) (( int32_t (*) (List_1_t349 *, const MethodInfo*))List_1_get_Count_m17187_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t List_1_get_Item_m17188_gshared (List_1_t349 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m17188(__this, ___index, method) (( int32_t (*) (List_1_t349 *, int32_t, const MethodInfo*))List_1_get_Item_m17188_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m17189_gshared (List_1_t349 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define List_1_set_Item_m17189(__this, ___index, ___value, method) (( void (*) (List_1_t349 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m17189_gshared)(__this, ___index, ___value, method)
