﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IAsyncResult
struct IAsyncResult_t260;
// System.AsyncCallback
struct AsyncCallback_t261;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>
struct  Comparison_1_t2924  : public MulticastDelegate_t259
{
};
