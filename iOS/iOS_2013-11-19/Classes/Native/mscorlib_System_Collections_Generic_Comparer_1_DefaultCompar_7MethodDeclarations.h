﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t2591;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m18316_gshared (DefaultComparer_t2591 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m18316(__this, method) (( void (*) (DefaultComparer_t2591 *, const MethodInfo*))DefaultComparer__ctor_m18316_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m18317_gshared (DefaultComparer_t2591 * __this, UICharInfo_t430  ___x, UICharInfo_t430  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m18317(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2591 *, UICharInfo_t430 , UICharInfo_t430 , const MethodInfo*))DefaultComparer_Compare_m18317_gshared)(__this, ___x, ___y, method)
