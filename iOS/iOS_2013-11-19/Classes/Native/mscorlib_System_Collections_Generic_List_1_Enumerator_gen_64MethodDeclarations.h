﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t3078;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_64.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m25253_gshared (Enumerator_t3079 * __this, List_1_t3078 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m25253(__this, ___l, method) (( void (*) (Enumerator_t3079 *, List_1_t3078 *, const MethodInfo*))Enumerator__ctor_m25253_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m25254_gshared (Enumerator_t3079 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m25254(__this, method) (( void (*) (Enumerator_t3079 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m25254_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25255_gshared (Enumerator_t3079 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25255(__this, method) (( Object_t * (*) (Enumerator_t3079 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25255_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C" void Enumerator_Dispose_m25256_gshared (Enumerator_t3079 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25256(__this, method) (( void (*) (Enumerator_t3079 *, const MethodInfo*))Enumerator_Dispose_m25256_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern "C" void Enumerator_VerifyState_m25257_gshared (Enumerator_t3079 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m25257(__this, method) (( void (*) (Enumerator_t3079 *, const MethodInfo*))Enumerator_VerifyState_m25257_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25258_gshared (Enumerator_t3079 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25258(__this, method) (( bool (*) (Enumerator_t3079 *, const MethodInfo*))Enumerator_MoveNext_m25258_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C" CustomAttributeNamedArgument_t1691  Enumerator_get_Current_m25259_gshared (Enumerator_t3079 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25259(__this, method) (( CustomAttributeNamedArgument_t1691  (*) (Enumerator_t3079 *, const MethodInfo*))Enumerator_get_Current_m25259_gshared)(__this, method)
