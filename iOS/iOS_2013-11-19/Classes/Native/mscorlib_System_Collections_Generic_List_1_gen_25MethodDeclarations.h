﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t347;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_t3170;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t3171;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector2>
struct ICollection_1_t3172;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>
struct ReadOnlyCollection_1_t2510;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t249;
// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t2515;
// System.Comparison`1<UnityEngine.Vector2>
struct Comparison_1_t2518;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_52.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
extern "C" void List_1__ctor_m17426_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1__ctor_m17426(__this, method) (( void (*) (List_1_t347 *, const MethodInfo*))List_1__ctor_m17426_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m17427_gshared (List_1_t347 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m17427(__this, ___collection, method) (( void (*) (List_1_t347 *, Object_t*, const MethodInfo*))List_1__ctor_m17427_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Int32)
extern "C" void List_1__ctor_m17428_gshared (List_1_t347 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m17428(__this, ___capacity, method) (( void (*) (List_1_t347 *, int32_t, const MethodInfo*))List_1__ctor_m17428_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.cctor()
extern "C" void List_1__cctor_m17429_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m17429(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m17429_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17430_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17430(__this, method) (( Object_t* (*) (List_1_t347 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17430_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17431_gshared (List_1_t347 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m17431(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t347 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m17431_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m17432_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17432(__this, method) (( Object_t * (*) (List_1_t347 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m17432_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m17433_gshared (List_1_t347 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m17433(__this, ___item, method) (( int32_t (*) (List_1_t347 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m17433_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m17434_gshared (List_1_t347 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m17434(__this, ___item, method) (( bool (*) (List_1_t347 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m17434_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m17435_gshared (List_1_t347 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m17435(__this, ___item, method) (( int32_t (*) (List_1_t347 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m17435_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m17436_gshared (List_1_t347 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m17436(__this, ___index, ___item, method) (( void (*) (List_1_t347 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m17436_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m17437_gshared (List_1_t347 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m17437(__this, ___item, method) (( void (*) (List_1_t347 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m17437_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17438_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17438(__this, method) (( bool (*) (List_1_t347 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17438_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m17439_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17439(__this, method) (( bool (*) (List_1_t347 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m17439_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m17440_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m17440(__this, method) (( Object_t * (*) (List_1_t347 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m17440_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m17441_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m17441(__this, method) (( bool (*) (List_1_t347 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m17441_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m17442_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m17442(__this, method) (( bool (*) (List_1_t347 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m17442_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m17443_gshared (List_1_t347 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m17443(__this, ___index, method) (( Object_t * (*) (List_1_t347 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m17443_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m17444_gshared (List_1_t347 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m17444(__this, ___index, ___value, method) (( void (*) (List_1_t347 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m17444_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(T)
extern "C" void List_1_Add_m17445_gshared (List_1_t347 * __this, Vector2_t171  ___item, const MethodInfo* method);
#define List_1_Add_m17445(__this, ___item, method) (( void (*) (List_1_t347 *, Vector2_t171 , const MethodInfo*))List_1_Add_m17445_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m17446_gshared (List_1_t347 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m17446(__this, ___newCount, method) (( void (*) (List_1_t347 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m17446_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m17447_gshared (List_1_t347 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m17447(__this, ___collection, method) (( void (*) (List_1_t347 *, Object_t*, const MethodInfo*))List_1_AddCollection_m17447_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m17448_gshared (List_1_t347 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m17448(__this, ___enumerable, method) (( void (*) (List_1_t347 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m17448_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m2302_gshared (List_1_t347 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m2302(__this, ___collection, method) (( void (*) (List_1_t347 *, Object_t*, const MethodInfo*))List_1_AddRange_m2302_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2510 * List_1_AsReadOnly_m17449_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m17449(__this, method) (( ReadOnlyCollection_1_t2510 * (*) (List_1_t347 *, const MethodInfo*))List_1_AsReadOnly_m17449_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear()
extern "C" void List_1_Clear_m17450_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_Clear_m17450(__this, method) (( void (*) (List_1_t347 *, const MethodInfo*))List_1_Clear_m17450_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::Contains(T)
extern "C" bool List_1_Contains_m17451_gshared (List_1_t347 * __this, Vector2_t171  ___item, const MethodInfo* method);
#define List_1_Contains_m17451(__this, ___item, method) (( bool (*) (List_1_t347 *, Vector2_t171 , const MethodInfo*))List_1_Contains_m17451_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m17452_gshared (List_1_t347 * __this, Vector2U5BU5D_t249* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m17452(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t347 *, Vector2U5BU5D_t249*, int32_t, const MethodInfo*))List_1_CopyTo_m17452_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector2>::Find(System.Predicate`1<T>)
extern "C" Vector2_t171  List_1_Find_m17453_gshared (List_1_t347 * __this, Predicate_1_t2515 * ___match, const MethodInfo* method);
#define List_1_Find_m17453(__this, ___match, method) (( Vector2_t171  (*) (List_1_t347 *, Predicate_1_t2515 *, const MethodInfo*))List_1_Find_m17453_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m17454_gshared (Object_t * __this /* static, unused */, Predicate_1_t2515 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m17454(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2515 *, const MethodInfo*))List_1_CheckMatch_m17454_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m17455_gshared (List_1_t347 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2515 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m17455(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t347 *, int32_t, int32_t, Predicate_1_t2515 *, const MethodInfo*))List_1_GetIndex_m17455_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::GetEnumerator()
extern "C" Enumerator_t2509  List_1_GetEnumerator_m17456_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m17456(__this, method) (( Enumerator_t2509  (*) (List_1_t347 *, const MethodInfo*))List_1_GetEnumerator_m17456_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m17457_gshared (List_1_t347 * __this, Vector2_t171  ___item, const MethodInfo* method);
#define List_1_IndexOf_m17457(__this, ___item, method) (( int32_t (*) (List_1_t347 *, Vector2_t171 , const MethodInfo*))List_1_IndexOf_m17457_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m17458_gshared (List_1_t347 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m17458(__this, ___start, ___delta, method) (( void (*) (List_1_t347 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m17458_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m17459_gshared (List_1_t347 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m17459(__this, ___index, method) (( void (*) (List_1_t347 *, int32_t, const MethodInfo*))List_1_CheckIndex_m17459_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m17460_gshared (List_1_t347 * __this, int32_t ___index, Vector2_t171  ___item, const MethodInfo* method);
#define List_1_Insert_m17460(__this, ___index, ___item, method) (( void (*) (List_1_t347 *, int32_t, Vector2_t171 , const MethodInfo*))List_1_Insert_m17460_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m17461_gshared (List_1_t347 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m17461(__this, ___collection, method) (( void (*) (List_1_t347 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m17461_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::Remove(T)
extern "C" bool List_1_Remove_m17462_gshared (List_1_t347 * __this, Vector2_t171  ___item, const MethodInfo* method);
#define List_1_Remove_m17462(__this, ___item, method) (( bool (*) (List_1_t347 *, Vector2_t171 , const MethodInfo*))List_1_Remove_m17462_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m17463_gshared (List_1_t347 * __this, Predicate_1_t2515 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m17463(__this, ___match, method) (( int32_t (*) (List_1_t347 *, Predicate_1_t2515 *, const MethodInfo*))List_1_RemoveAll_m17463_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m17464_gshared (List_1_t347 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m17464(__this, ___index, method) (( void (*) (List_1_t347 *, int32_t, const MethodInfo*))List_1_RemoveAt_m17464_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Reverse()
extern "C" void List_1_Reverse_m17465_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_Reverse_m17465(__this, method) (( void (*) (List_1_t347 *, const MethodInfo*))List_1_Reverse_m17465_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Sort()
extern "C" void List_1_Sort_m17466_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_Sort_m17466(__this, method) (( void (*) (List_1_t347 *, const MethodInfo*))List_1_Sort_m17466_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m17467_gshared (List_1_t347 * __this, Comparison_1_t2518 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m17467(__this, ___comparison, method) (( void (*) (List_1_t347 *, Comparison_1_t2518 *, const MethodInfo*))List_1_Sort_m17467_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector2>::ToArray()
extern "C" Vector2U5BU5D_t249* List_1_ToArray_m17468_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_ToArray_m17468(__this, method) (( Vector2U5BU5D_t249* (*) (List_1_t347 *, const MethodInfo*))List_1_ToArray_m17468_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::TrimExcess()
extern "C" void List_1_TrimExcess_m17469_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m17469(__this, method) (( void (*) (List_1_t347 *, const MethodInfo*))List_1_TrimExcess_m17469_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m17470_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m17470(__this, method) (( int32_t (*) (List_1_t347 *, const MethodInfo*))List_1_get_Capacity_m17470_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m17471_gshared (List_1_t347 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m17471(__this, ___value, method) (( void (*) (List_1_t347 *, int32_t, const MethodInfo*))List_1_set_Capacity_m17471_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
extern "C" int32_t List_1_get_Count_m17472_gshared (List_1_t347 * __this, const MethodInfo* method);
#define List_1_get_Count_m17472(__this, method) (( int32_t (*) (List_1_t347 *, const MethodInfo*))List_1_get_Count_m17472_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C" Vector2_t171  List_1_get_Item_m17473_gshared (List_1_t347 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m17473(__this, ___index, method) (( Vector2_t171  (*) (List_1_t347 *, int32_t, const MethodInfo*))List_1_get_Item_m17473_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m17474_gshared (List_1_t347 * __this, int32_t ___index, Vector2_t171  ___value, const MethodInfo* method);
#define List_1_set_Item_m17474(__this, ___index, ___value, method) (( void (*) (List_1_t347 *, int32_t, Vector2_t171 , const MethodInfo*))List_1_set_Item_m17474_gshared)(__this, ___index, ___value, method)
