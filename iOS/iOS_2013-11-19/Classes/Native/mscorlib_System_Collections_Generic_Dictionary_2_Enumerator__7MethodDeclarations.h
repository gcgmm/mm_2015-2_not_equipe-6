﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m15870(__this, ___dictionary, method) (( void (*) (Enumerator_t2402 *, Dictionary_2_t240 *, const MethodInfo*))Enumerator__ctor_m15322_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15871(__this, method) (( Object_t * (*) (Enumerator_t2402 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m15872(__this, method) (( void (*) (Enumerator_t2402 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15324_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15873(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t2402 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15325_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15874(__this, method) (( Object_t * (*) (Enumerator_t2402 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15326_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15875(__this, method) (( Object_t * (*) (Enumerator_t2402 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15327_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::MoveNext()
#define Enumerator_MoveNext_m15876(__this, method) (( bool (*) (Enumerator_t2402 *, const MethodInfo*))Enumerator_MoveNext_m15328_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Current()
#define Enumerator_get_Current_m15877(__this, method) (( KeyValuePair_2_t2399  (*) (Enumerator_t2402 *, const MethodInfo*))Enumerator_get_Current_m15329_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15878(__this, method) (( Canvas_t230 * (*) (Enumerator_t2402 *, const MethodInfo*))Enumerator_get_CurrentKey_m15330_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15879(__this, method) (( IndexedSet_1_t422 * (*) (Enumerator_t2402 *, const MethodInfo*))Enumerator_get_CurrentValue_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::Reset()
#define Enumerator_Reset_m15880(__this, method) (( void (*) (Enumerator_t2402 *, const MethodInfo*))Enumerator_Reset_m15332_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::VerifyState()
#define Enumerator_VerifyState_m15881(__this, method) (( void (*) (Enumerator_t2402 *, const MethodInfo*))Enumerator_VerifyState_m15333_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15882(__this, method) (( void (*) (Enumerator_t2402 *, const MethodInfo*))Enumerator_VerifyCurrent_m15334_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::Dispose()
#define Enumerator_Dispose_m15883(__this, method) (( void (*) (Enumerator_t2402 *, const MethodInfo*))Enumerator_Dispose_m15335_gshared)(__this, method)
