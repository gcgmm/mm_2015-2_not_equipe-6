﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_1MethodDeclarations.h"

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m17818(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t2546 *, UnityAction_1_t2547 *, UnityAction_1_t2547 *, const MethodInfo*))ObjectPool_1__ctor_m13407_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::get_countAll()
#define ObjectPool_1_get_countAll_m17819(__this, method) (( int32_t (*) (ObjectPool_1_t2546 *, const MethodInfo*))ObjectPool_1_get_countAll_m13409_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m17820(__this, ___value, method) (( void (*) (ObjectPool_1_t2546 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m13411_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::get_countActive()
#define ObjectPool_1_get_countActive_m17821(__this, method) (( int32_t (*) (ObjectPool_1_t2546 *, const MethodInfo*))ObjectPool_1_get_countActive_m13413_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m17822(__this, method) (( int32_t (*) (ObjectPool_1_t2546 *, const MethodInfo*))ObjectPool_1_get_countInactive_m13415_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::Get()
#define ObjectPool_1_Get_m17823(__this, method) (( List_1_t349 * (*) (ObjectPool_1_t2546 *, const MethodInfo*))ObjectPool_1_Get_m13417_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::Release(T)
#define ObjectPool_1_Release_m17824(__this, ___element, method) (( void (*) (ObjectPool_1_t2546 *, List_1_t349 *, const MethodInfo*))ObjectPool_1_Release_m13419_gshared)(__this, ___element, method)
