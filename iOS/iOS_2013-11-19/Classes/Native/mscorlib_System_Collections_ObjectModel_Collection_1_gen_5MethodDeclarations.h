﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>
struct Collection_1_t2512;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t249;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t3171;
// System.Collections.Generic.IList`1<UnityEngine.Vector2>
struct IList_1_t2511;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::.ctor()
extern "C" void Collection_1__ctor_m17512_gshared (Collection_1_t2512 * __this, const MethodInfo* method);
#define Collection_1__ctor_m17512(__this, method) (( void (*) (Collection_1_t2512 *, const MethodInfo*))Collection_1__ctor_m17512_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17513_gshared (Collection_1_t2512 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17513(__this, method) (( bool (*) (Collection_1_t2512 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17513_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m17514_gshared (Collection_1_t2512 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m17514(__this, ___array, ___index, method) (( void (*) (Collection_1_t2512 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m17514_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m17515_gshared (Collection_1_t2512 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m17515(__this, method) (( Object_t * (*) (Collection_1_t2512 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m17515_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m17516_gshared (Collection_1_t2512 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m17516(__this, ___value, method) (( int32_t (*) (Collection_1_t2512 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m17516_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m17517_gshared (Collection_1_t2512 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m17517(__this, ___value, method) (( bool (*) (Collection_1_t2512 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m17517_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m17518_gshared (Collection_1_t2512 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m17518(__this, ___value, method) (( int32_t (*) (Collection_1_t2512 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m17518_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m17519_gshared (Collection_1_t2512 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m17519(__this, ___index, ___value, method) (( void (*) (Collection_1_t2512 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m17519_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m17520_gshared (Collection_1_t2512 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m17520(__this, ___value, method) (( void (*) (Collection_1_t2512 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m17520_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m17521_gshared (Collection_1_t2512 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m17521(__this, method) (( bool (*) (Collection_1_t2512 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m17521_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m17522_gshared (Collection_1_t2512 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m17522(__this, method) (( Object_t * (*) (Collection_1_t2512 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m17522_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m17523_gshared (Collection_1_t2512 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m17523(__this, method) (( bool (*) (Collection_1_t2512 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m17523_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m17524_gshared (Collection_1_t2512 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m17524(__this, method) (( bool (*) (Collection_1_t2512 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m17524_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m17525_gshared (Collection_1_t2512 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m17525(__this, ___index, method) (( Object_t * (*) (Collection_1_t2512 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m17525_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m17526_gshared (Collection_1_t2512 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m17526(__this, ___index, ___value, method) (( void (*) (Collection_1_t2512 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m17526_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Add(T)
extern "C" void Collection_1_Add_m17527_gshared (Collection_1_t2512 * __this, Vector2_t171  ___item, const MethodInfo* method);
#define Collection_1_Add_m17527(__this, ___item, method) (( void (*) (Collection_1_t2512 *, Vector2_t171 , const MethodInfo*))Collection_1_Add_m17527_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Clear()
extern "C" void Collection_1_Clear_m17528_gshared (Collection_1_t2512 * __this, const MethodInfo* method);
#define Collection_1_Clear_m17528(__this, method) (( void (*) (Collection_1_t2512 *, const MethodInfo*))Collection_1_Clear_m17528_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::ClearItems()
extern "C" void Collection_1_ClearItems_m17529_gshared (Collection_1_t2512 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m17529(__this, method) (( void (*) (Collection_1_t2512 *, const MethodInfo*))Collection_1_ClearItems_m17529_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Contains(T)
extern "C" bool Collection_1_Contains_m17530_gshared (Collection_1_t2512 * __this, Vector2_t171  ___item, const MethodInfo* method);
#define Collection_1_Contains_m17530(__this, ___item, method) (( bool (*) (Collection_1_t2512 *, Vector2_t171 , const MethodInfo*))Collection_1_Contains_m17530_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m17531_gshared (Collection_1_t2512 * __this, Vector2U5BU5D_t249* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m17531(__this, ___array, ___index, method) (( void (*) (Collection_1_t2512 *, Vector2U5BU5D_t249*, int32_t, const MethodInfo*))Collection_1_CopyTo_m17531_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m17532_gshared (Collection_1_t2512 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m17532(__this, method) (( Object_t* (*) (Collection_1_t2512 *, const MethodInfo*))Collection_1_GetEnumerator_m17532_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m17533_gshared (Collection_1_t2512 * __this, Vector2_t171  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m17533(__this, ___item, method) (( int32_t (*) (Collection_1_t2512 *, Vector2_t171 , const MethodInfo*))Collection_1_IndexOf_m17533_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m17534_gshared (Collection_1_t2512 * __this, int32_t ___index, Vector2_t171  ___item, const MethodInfo* method);
#define Collection_1_Insert_m17534(__this, ___index, ___item, method) (( void (*) (Collection_1_t2512 *, int32_t, Vector2_t171 , const MethodInfo*))Collection_1_Insert_m17534_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m17535_gshared (Collection_1_t2512 * __this, int32_t ___index, Vector2_t171  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m17535(__this, ___index, ___item, method) (( void (*) (Collection_1_t2512 *, int32_t, Vector2_t171 , const MethodInfo*))Collection_1_InsertItem_m17535_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Remove(T)
extern "C" bool Collection_1_Remove_m17536_gshared (Collection_1_t2512 * __this, Vector2_t171  ___item, const MethodInfo* method);
#define Collection_1_Remove_m17536(__this, ___item, method) (( bool (*) (Collection_1_t2512 *, Vector2_t171 , const MethodInfo*))Collection_1_Remove_m17536_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m17537_gshared (Collection_1_t2512 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m17537(__this, ___index, method) (( void (*) (Collection_1_t2512 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m17537_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m17538_gshared (Collection_1_t2512 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m17538(__this, ___index, method) (( void (*) (Collection_1_t2512 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m17538_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::get_Count()
extern "C" int32_t Collection_1_get_Count_m17539_gshared (Collection_1_t2512 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m17539(__this, method) (( int32_t (*) (Collection_1_t2512 *, const MethodInfo*))Collection_1_get_Count_m17539_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C" Vector2_t171  Collection_1_get_Item_m17540_gshared (Collection_1_t2512 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m17540(__this, ___index, method) (( Vector2_t171  (*) (Collection_1_t2512 *, int32_t, const MethodInfo*))Collection_1_get_Item_m17540_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m17541_gshared (Collection_1_t2512 * __this, int32_t ___index, Vector2_t171  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m17541(__this, ___index, ___value, method) (( void (*) (Collection_1_t2512 *, int32_t, Vector2_t171 , const MethodInfo*))Collection_1_set_Item_m17541_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m17542_gshared (Collection_1_t2512 * __this, int32_t ___index, Vector2_t171  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m17542(__this, ___index, ___item, method) (( void (*) (Collection_1_t2512 *, int32_t, Vector2_t171 , const MethodInfo*))Collection_1_SetItem_m17542_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m17543_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m17543(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m17543_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::ConvertItem(System.Object)
extern "C" Vector2_t171  Collection_1_ConvertItem_m17544_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m17544(__this /* static, unused */, ___item, method) (( Vector2_t171  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m17544_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m17545_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m17545(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m17545_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m17546_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m17546(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m17546_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m17547_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m17547(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m17547_gshared)(__this /* static, unused */, ___list, method)
