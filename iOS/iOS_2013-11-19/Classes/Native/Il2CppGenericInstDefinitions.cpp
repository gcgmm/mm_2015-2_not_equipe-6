﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Object_t_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Types[] = { &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0 = { 1, GenInst_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 4, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType Material_t8_0_0_0;
static const Il2CppType* GenInst_Material_t8_0_0_0_Types[] = { &Material_t8_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t8_0_0_0 = { 1, GenInst_Material_t8_0_0_0_Types };
extern const Il2CppType RowController_t10_0_0_0;
static const Il2CppType* GenInst_RowController_t10_0_0_0_Types[] = { &RowController_t10_0_0_0 };
extern const Il2CppGenericInst GenInst_RowController_t10_0_0_0 = { 1, GenInst_RowController_t10_0_0_0_Types };
extern const Il2CppType Renderer_t85_0_0_0;
static const Il2CppType* GenInst_Renderer_t85_0_0_0_Types[] = { &Renderer_t85_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t85_0_0_0 = { 1, GenInst_Renderer_t85_0_0_0_Types };
extern const Il2CppType DockController_t7_0_0_0;
static const Il2CppType* GenInst_DockController_t7_0_0_0_Types[] = { &DockController_t7_0_0_0 };
extern const Il2CppGenericInst GenInst_DockController_t7_0_0_0 = { 1, GenInst_DockController_t7_0_0_0_Types };
extern const Il2CppType BoardController_t1_0_0_0;
static const Il2CppType* GenInst_BoardController_t1_0_0_0_Types[] = { &BoardController_t1_0_0_0 };
extern const Il2CppGenericInst GenInst_BoardController_t1_0_0_0 = { 1, GenInst_BoardController_t1_0_0_0_Types };
extern const Il2CppType InitError_t905_0_0_0;
static const Il2CppType* GenInst_InitError_t905_0_0_0_Types[] = { &InitError_t905_0_0_0 };
extern const Il2CppGenericInst GenInst_InitError_t905_0_0_0 = { 1, GenInst_InitError_t905_0_0_0_Types };
extern const Il2CppType ReconstructionBehaviour_t28_0_0_0;
static const Il2CppType* GenInst_ReconstructionBehaviour_t28_0_0_0_Types[] = { &ReconstructionBehaviour_t28_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionBehaviour_t28_0_0_0 = { 1, GenInst_ReconstructionBehaviour_t28_0_0_0_Types };
extern const Il2CppType Prop_t81_0_0_0;
static const Il2CppType* GenInst_Prop_t81_0_0_0_Types[] = { &Prop_t81_0_0_0 };
extern const Il2CppGenericInst GenInst_Prop_t81_0_0_0 = { 1, GenInst_Prop_t81_0_0_0_Types };
extern const Il2CppType Surface_t82_0_0_0;
static const Il2CppType* GenInst_Surface_t82_0_0_0_Types[] = { &Surface_t82_0_0_0 };
extern const Il2CppGenericInst GenInst_Surface_t82_0_0_0 = { 1, GenInst_Surface_t82_0_0_0_Types };
extern const Il2CppType TrackableBehaviour_t32_0_0_0;
static const Il2CppType* GenInst_TrackableBehaviour_t32_0_0_0_Types[] = { &TrackableBehaviour_t32_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableBehaviour_t32_0_0_0 = { 1, GenInst_TrackableBehaviour_t32_0_0_0_Types };
extern const Il2CppType Collider_t14_0_0_0;
static const Il2CppType* GenInst_Collider_t14_0_0_0_Types[] = { &Collider_t14_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t14_0_0_0 = { 1, GenInst_Collider_t14_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType MaskOutBehaviour_t46_0_0_0;
static const Il2CppType* GenInst_MaskOutBehaviour_t46_0_0_0_Types[] = { &MaskOutBehaviour_t46_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskOutBehaviour_t46_0_0_0 = { 1, GenInst_MaskOutBehaviour_t46_0_0_0_Types };
extern const Il2CppType VirtualButtonBehaviour_t70_0_0_0;
static const Il2CppType* GenInst_VirtualButtonBehaviour_t70_0_0_0_Types[] = { &VirtualButtonBehaviour_t70_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonBehaviour_t70_0_0_0 = { 1, GenInst_VirtualButtonBehaviour_t70_0_0_0_Types };
extern const Il2CppType TurnOffBehaviour_t61_0_0_0;
static const Il2CppType* GenInst_TurnOffBehaviour_t61_0_0_0_Types[] = { &TurnOffBehaviour_t61_0_0_0 };
extern const Il2CppGenericInst GenInst_TurnOffBehaviour_t61_0_0_0 = { 1, GenInst_TurnOffBehaviour_t61_0_0_0_Types };
extern const Il2CppType ImageTargetBehaviour_t36_0_0_0;
static const Il2CppType* GenInst_ImageTargetBehaviour_t36_0_0_0_Types[] = { &ImageTargetBehaviour_t36_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetBehaviour_t36_0_0_0 = { 1, GenInst_ImageTargetBehaviour_t36_0_0_0_Types };
extern const Il2CppType MarkerBehaviour_t44_0_0_0;
static const Il2CppType* GenInst_MarkerBehaviour_t44_0_0_0_Types[] = { &MarkerBehaviour_t44_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerBehaviour_t44_0_0_0 = { 1, GenInst_MarkerBehaviour_t44_0_0_0_Types };
extern const Il2CppType MultiTargetBehaviour_t48_0_0_0;
static const Il2CppType* GenInst_MultiTargetBehaviour_t48_0_0_0_Types[] = { &MultiTargetBehaviour_t48_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiTargetBehaviour_t48_0_0_0 = { 1, GenInst_MultiTargetBehaviour_t48_0_0_0_Types };
extern const Il2CppType CylinderTargetBehaviour_t22_0_0_0;
static const Il2CppType* GenInst_CylinderTargetBehaviour_t22_0_0_0_Types[] = { &CylinderTargetBehaviour_t22_0_0_0 };
extern const Il2CppGenericInst GenInst_CylinderTargetBehaviour_t22_0_0_0 = { 1, GenInst_CylinderTargetBehaviour_t22_0_0_0_Types };
extern const Il2CppType WordBehaviour_t79_0_0_0;
static const Il2CppType* GenInst_WordBehaviour_t79_0_0_0_Types[] = { &WordBehaviour_t79_0_0_0 };
extern const Il2CppGenericInst GenInst_WordBehaviour_t79_0_0_0 = { 1, GenInst_WordBehaviour_t79_0_0_0_Types };
extern const Il2CppType TextRecoBehaviour_t59_0_0_0;
static const Il2CppType* GenInst_TextRecoBehaviour_t59_0_0_0_Types[] = { &TextRecoBehaviour_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_TextRecoBehaviour_t59_0_0_0 = { 1, GenInst_TextRecoBehaviour_t59_0_0_0_Types };
extern const Il2CppType ObjectTargetBehaviour_t50_0_0_0;
static const Il2CppType* GenInst_ObjectTargetBehaviour_t50_0_0_0_Types[] = { &ObjectTargetBehaviour_t50_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTargetBehaviour_t50_0_0_0 = { 1, GenInst_ObjectTargetBehaviour_t50_0_0_0_Types };
extern const Il2CppType MeshRenderer_t113_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t113_0_0_0_Types[] = { &MeshRenderer_t113_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t113_0_0_0 = { 1, GenInst_MeshRenderer_t113_0_0_0_Types };
extern const Il2CppType MeshFilter_t114_0_0_0;
static const Il2CppType* GenInst_MeshFilter_t114_0_0_0_Types[] = { &MeshFilter_t114_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshFilter_t114_0_0_0 = { 1, GenInst_MeshFilter_t114_0_0_0_Types };
extern const Il2CppType ComponentFactoryStarterBehaviour_t39_0_0_0;
static const Il2CppType* GenInst_ComponentFactoryStarterBehaviour_t39_0_0_0_Types[] = { &ComponentFactoryStarterBehaviour_t39_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentFactoryStarterBehaviour_t39_0_0_0 = { 1, GenInst_ComponentFactoryStarterBehaviour_t39_0_0_0_Types };
extern const Il2CppType VuforiaBehaviour_t72_0_0_0;
static const Il2CppType* GenInst_VuforiaBehaviour_t72_0_0_0_Types[] = { &VuforiaBehaviour_t72_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaBehaviour_t72_0_0_0 = { 1, GenInst_VuforiaBehaviour_t72_0_0_0_Types };
extern const Il2CppType Camera_t91_0_0_0;
static const Il2CppType* GenInst_Camera_t91_0_0_0_Types[] = { &Camera_t91_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t91_0_0_0 = { 1, GenInst_Camera_t91_0_0_0_Types };
extern const Il2CppType WireframeBehaviour_t76_0_0_0;
static const Il2CppType* GenInst_WireframeBehaviour_t76_0_0_0_Types[] = { &WireframeBehaviour_t76_0_0_0 };
extern const Il2CppGenericInst GenInst_WireframeBehaviour_t76_0_0_0 = { 1, GenInst_WireframeBehaviour_t76_0_0_0_Types };
extern const Il2CppType BaseInputModule_t136_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t136_0_0_0_Types[] = { &BaseInputModule_t136_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t136_0_0_0 = { 1, GenInst_BaseInputModule_t136_0_0_0_Types };
extern const Il2CppType RaycastResult_t169_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t169_0_0_0_Types[] = { &RaycastResult_t169_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t169_0_0_0 = { 1, GenInst_RaycastResult_t169_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t370_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t370_0_0_0_Types[] = { &IDeselectHandler_t370_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t370_0_0_0 = { 1, GenInst_IDeselectHandler_t370_0_0_0_Types };
extern const Il2CppType ISelectHandler_t369_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t369_0_0_0_Types[] = { &ISelectHandler_t369_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t369_0_0_0 = { 1, GenInst_ISelectHandler_t369_0_0_0_Types };
extern const Il2CppType BaseEventData_t137_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t137_0_0_0_Types[] = { &BaseEventData_t137_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t137_0_0_0 = { 1, GenInst_BaseEventData_t137_0_0_0_Types };
extern const Il2CppType Entry_t141_0_0_0;
static const Il2CppType* GenInst_Entry_t141_0_0_0_Types[] = { &Entry_t141_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t141_0_0_0 = { 1, GenInst_Entry_t141_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t357_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t357_0_0_0_Types[] = { &IPointerEnterHandler_t357_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t357_0_0_0 = { 1, GenInst_IPointerEnterHandler_t357_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t358_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t358_0_0_0_Types[] = { &IPointerExitHandler_t358_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t358_0_0_0 = { 1, GenInst_IPointerExitHandler_t358_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t359_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t359_0_0_0_Types[] = { &IPointerDownHandler_t359_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t359_0_0_0 = { 1, GenInst_IPointerDownHandler_t359_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t360_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t360_0_0_0_Types[] = { &IPointerUpHandler_t360_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t360_0_0_0 = { 1, GenInst_IPointerUpHandler_t360_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t361_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t361_0_0_0_Types[] = { &IPointerClickHandler_t361_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t361_0_0_0 = { 1, GenInst_IPointerClickHandler_t361_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t362_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t362_0_0_0_Types[] = { &IInitializePotentialDragHandler_t362_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t362_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t362_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t363_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t363_0_0_0_Types[] = { &IBeginDragHandler_t363_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t363_0_0_0 = { 1, GenInst_IBeginDragHandler_t363_0_0_0_Types };
extern const Il2CppType IDragHandler_t364_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t364_0_0_0_Types[] = { &IDragHandler_t364_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t364_0_0_0 = { 1, GenInst_IDragHandler_t364_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t365_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t365_0_0_0_Types[] = { &IEndDragHandler_t365_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t365_0_0_0 = { 1, GenInst_IEndDragHandler_t365_0_0_0_Types };
extern const Il2CppType IDropHandler_t366_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t366_0_0_0_Types[] = { &IDropHandler_t366_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t366_0_0_0 = { 1, GenInst_IDropHandler_t366_0_0_0_Types };
extern const Il2CppType IScrollHandler_t367_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t367_0_0_0_Types[] = { &IScrollHandler_t367_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t367_0_0_0 = { 1, GenInst_IScrollHandler_t367_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t368_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t368_0_0_0_Types[] = { &IUpdateSelectedHandler_t368_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t368_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t368_0_0_0_Types };
extern const Il2CppType IMoveHandler_t371_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t371_0_0_0_Types[] = { &IMoveHandler_t371_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t371_0_0_0 = { 1, GenInst_IMoveHandler_t371_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t372_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t372_0_0_0_Types[] = { &ISubmitHandler_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t372_0_0_0 = { 1, GenInst_ISubmitHandler_t372_0_0_0_Types };
extern const Il2CppType ICancelHandler_t373_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t373_0_0_0_Types[] = { &ICancelHandler_t373_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t373_0_0_0 = { 1, GenInst_ICancelHandler_t373_0_0_0_Types };
extern const Il2CppType List_1_t356_0_0_0;
static const Il2CppType* GenInst_List_1_t356_0_0_0_Types[] = { &List_1_t356_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t356_0_0_0 = { 1, GenInst_List_1_t356_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t2215_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t2215_0_0_0_Types[] = { &IEventSystemHandler_t2215_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2215_0_0_0 = { 1, GenInst_IEventSystemHandler_t2215_0_0_0_Types };
extern const Il2CppType Transform_t84_0_0_0;
static const Il2CppType* GenInst_Transform_t84_0_0_0_Types[] = { &Transform_t84_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t84_0_0_0 = { 1, GenInst_Transform_t84_0_0_0_Types };
extern const Il2CppType PointerEventData_t175_0_0_0;
static const Il2CppType* GenInst_PointerEventData_t175_0_0_0_Types[] = { &PointerEventData_t175_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t175_0_0_0 = { 1, GenInst_PointerEventData_t175_0_0_0_Types };
extern const Il2CppType AxisEventData_t172_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t172_0_0_0_Types[] = { &AxisEventData_t172_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t172_0_0_0 = { 1, GenInst_AxisEventData_t172_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t170_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t170_0_0_0_Types[] = { &BaseRaycaster_t170_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t170_0_0_0 = { 1, GenInst_BaseRaycaster_t170_0_0_0_Types };
extern const Il2CppType GameObject_t9_0_0_0;
static const Il2CppType* GenInst_GameObject_t9_0_0_0_Types[] = { &GameObject_t9_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t9_0_0_0 = { 1, GenInst_GameObject_t9_0_0_0_Types };
extern const Il2CppType EventSystem_t133_0_0_0;
static const Il2CppType* GenInst_EventSystem_t133_0_0_0_Types[] = { &EventSystem_t133_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t133_0_0_0 = { 1, GenInst_EventSystem_t133_0_0_0_Types };
extern const Il2CppType ButtonState_t178_0_0_0;
static const Il2CppType* GenInst_ButtonState_t178_0_0_0_Types[] = { &ButtonState_t178_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t178_0_0_0 = { 1, GenInst_ButtonState_t178_0_0_0_Types };
extern const Il2CppType Int32_t392_0_0_0;
static const Il2CppType* GenInst_Int32_t392_0_0_0_PointerEventData_t175_0_0_0_Types[] = { &Int32_t392_0_0_0, &PointerEventData_t175_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_PointerEventData_t175_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_PointerEventData_t175_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t400_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t400_0_0_0_Types[] = { &SpriteRenderer_t400_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t400_0_0_0 = { 1, GenInst_SpriteRenderer_t400_0_0_0_Types };
extern const Il2CppType RaycastHit_t89_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t89_0_0_0_Types[] = { &RaycastHit_t89_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t89_0_0_0 = { 1, GenInst_RaycastHit_t89_0_0_0_Types };
extern const Il2CppType Color_t77_0_0_0;
static const Il2CppType* GenInst_Color_t77_0_0_0_Types[] = { &Color_t77_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t77_0_0_0 = { 1, GenInst_Color_t77_0_0_0_Types };
extern const Il2CppType Single_t117_0_0_0;
static const Il2CppType* GenInst_Single_t117_0_0_0_Types[] = { &Single_t117_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t117_0_0_0 = { 1, GenInst_Single_t117_0_0_0_Types };
extern const Il2CppType ICanvasElement_t379_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t379_0_0_0_Types[] = { &ICanvasElement_t379_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t379_0_0_0 = { 1, GenInst_ICanvasElement_t379_0_0_0_Types };
extern const Il2CppType RectTransform_t212_0_0_0;
static const Il2CppType* GenInst_RectTransform_t212_0_0_0_Types[] = { &RectTransform_t212_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t212_0_0_0 = { 1, GenInst_RectTransform_t212_0_0_0_Types };
extern const Il2CppType Image_t15_0_0_0;
static const Il2CppType* GenInst_Image_t15_0_0_0_Types[] = { &Image_t15_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t15_0_0_0 = { 1, GenInst_Image_t15_0_0_0_Types };
extern const Il2CppType Button_t201_0_0_0;
static const Il2CppType* GenInst_Button_t201_0_0_0_Types[] = { &Button_t201_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t201_0_0_0 = { 1, GenInst_Button_t201_0_0_0_Types };
extern const Il2CppType Text_t16_0_0_0;
static const Il2CppType* GenInst_Text_t16_0_0_0_Types[] = { &Text_t16_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t16_0_0_0 = { 1, GenInst_Text_t16_0_0_0_Types };
extern const Il2CppType RawImage_t281_0_0_0;
static const Il2CppType* GenInst_RawImage_t281_0_0_0_Types[] = { &RawImage_t281_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t281_0_0_0 = { 1, GenInst_RawImage_t281_0_0_0_Types };
extern const Il2CppType Slider_t306_0_0_0;
static const Il2CppType* GenInst_Slider_t306_0_0_0_Types[] = { &Slider_t306_0_0_0 };
extern const Il2CppGenericInst GenInst_Slider_t306_0_0_0 = { 1, GenInst_Slider_t306_0_0_0_Types };
extern const Il2CppType Scrollbar_t290_0_0_0;
static const Il2CppType* GenInst_Scrollbar_t290_0_0_0_Types[] = { &Scrollbar_t290_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_t290_0_0_0 = { 1, GenInst_Scrollbar_t290_0_0_0_Types };
extern const Il2CppType Toggle_t213_0_0_0;
static const Il2CppType* GenInst_Toggle_t213_0_0_0_Types[] = { &Toggle_t213_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t213_0_0_0 = { 1, GenInst_Toggle_t213_0_0_0_Types };
extern const Il2CppType InputField_t263_0_0_0;
static const Il2CppType* GenInst_InputField_t263_0_0_0_Types[] = { &InputField_t263_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t263_0_0_0 = { 1, GenInst_InputField_t263_0_0_0_Types };
extern const Il2CppType ScrollRect_t296_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t296_0_0_0_Types[] = { &ScrollRect_t296_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t296_0_0_0 = { 1, GenInst_ScrollRect_t296_0_0_0_Types };
extern const Il2CppType Mask_t273_0_0_0;
static const Il2CppType* GenInst_Mask_t273_0_0_0_Types[] = { &Mask_t273_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t273_0_0_0 = { 1, GenInst_Mask_t273_0_0_0_Types };
extern const Il2CppType Dropdown_t220_0_0_0;
static const Il2CppType* GenInst_Dropdown_t220_0_0_0_Types[] = { &Dropdown_t220_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t220_0_0_0 = { 1, GenInst_Dropdown_t220_0_0_0_Types };
extern const Il2CppType OptionData_t214_0_0_0;
static const Il2CppType* GenInst_OptionData_t214_0_0_0_Types[] = { &OptionData_t214_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t214_0_0_0 = { 1, GenInst_OptionData_t214_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_Types[] = { &Int32_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0 = { 1, GenInst_Int32_t392_0_0_0_Types };
extern const Il2CppType DropdownItem_t211_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t211_0_0_0_Types[] = { &DropdownItem_t211_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t211_0_0_0 = { 1, GenInst_DropdownItem_t211_0_0_0_Types };
extern const Il2CppType FloatTween_t196_0_0_0;
static const Il2CppType* GenInst_FloatTween_t196_0_0_0_Types[] = { &FloatTween_t196_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t196_0_0_0 = { 1, GenInst_FloatTween_t196_0_0_0_Types };
extern const Il2CppType Canvas_t230_0_0_0;
static const Il2CppType* GenInst_Canvas_t230_0_0_0_Types[] = { &Canvas_t230_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t230_0_0_0 = { 1, GenInst_Canvas_t230_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t236_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t236_0_0_0_Types[] = { &GraphicRaycaster_t236_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t236_0_0_0 = { 1, GenInst_GraphicRaycaster_t236_0_0_0_Types };
extern const Il2CppType CanvasGroup_t408_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t408_0_0_0_Types[] = { &CanvasGroup_t408_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t408_0_0_0 = { 1, GenInst_CanvasGroup_t408_0_0_0_Types };
extern const Il2CppType Boolean_t393_0_0_0;
static const Il2CppType* GenInst_Boolean_t393_0_0_0_Types[] = { &Boolean_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t393_0_0_0 = { 1, GenInst_Boolean_t393_0_0_0_Types };
extern const Il2CppType Font_t225_0_0_0;
extern const Il2CppType List_1_t413_0_0_0;
static const Il2CppType* GenInst_Font_t225_0_0_0_List_1_t413_0_0_0_Types[] = { &Font_t225_0_0_0, &List_1_t413_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t225_0_0_0_List_1_t413_0_0_0 = { 2, GenInst_Font_t225_0_0_0_List_1_t413_0_0_0_Types };
static const Il2CppType* GenInst_Font_t225_0_0_0_Types[] = { &Font_t225_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t225_0_0_0 = { 1, GenInst_Font_t225_0_0_0_Types };
extern const Il2CppType ColorTween_t193_0_0_0;
static const Il2CppType* GenInst_ColorTween_t193_0_0_0_Types[] = { &ColorTween_t193_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t193_0_0_0 = { 1, GenInst_ColorTween_t193_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t229_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t229_0_0_0_Types[] = { &CanvasRenderer_t229_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t229_0_0_0 = { 1, GenInst_CanvasRenderer_t229_0_0_0_Types };
extern const Il2CppType Component_t130_0_0_0;
static const Il2CppType* GenInst_Component_t130_0_0_0_Types[] = { &Component_t130_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t130_0_0_0 = { 1, GenInst_Component_t130_0_0_0_Types };
extern const Il2CppType Graphic_t228_0_0_0;
static const Il2CppType* GenInst_Graphic_t228_0_0_0_Types[] = { &Graphic_t228_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t228_0_0_0 = { 1, GenInst_Graphic_t228_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t422_0_0_0;
static const Il2CppType* GenInst_Canvas_t230_0_0_0_IndexedSet_1_t422_0_0_0_Types[] = { &Canvas_t230_0_0_0, &IndexedSet_1_t422_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t230_0_0_0_IndexedSet_1_t422_0_0_0 = { 2, GenInst_Canvas_t230_0_0_0_IndexedSet_1_t422_0_0_0_Types };
extern const Il2CppType Sprite_t209_0_0_0;
static const Il2CppType* GenInst_Sprite_t209_0_0_0_Types[] = { &Sprite_t209_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t209_0_0_0 = { 1, GenInst_Sprite_t209_0_0_0_Types };
extern const Il2CppType Type_t241_0_0_0;
static const Il2CppType* GenInst_Type_t241_0_0_0_Types[] = { &Type_t241_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t241_0_0_0 = { 1, GenInst_Type_t241_0_0_0_Types };
extern const Il2CppType FillMethod_t242_0_0_0;
static const Il2CppType* GenInst_FillMethod_t242_0_0_0_Types[] = { &FillMethod_t242_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t242_0_0_0 = { 1, GenInst_FillMethod_t242_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType SubmitEvent_t254_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t254_0_0_0_Types[] = { &SubmitEvent_t254_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t254_0_0_0 = { 1, GenInst_SubmitEvent_t254_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t256_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t256_0_0_0_Types[] = { &OnChangeEvent_t256_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t256_0_0_0 = { 1, GenInst_OnChangeEvent_t256_0_0_0_Types };
extern const Il2CppType OnValidateInput_t258_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t258_0_0_0_Types[] = { &OnValidateInput_t258_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t258_0_0_0 = { 1, GenInst_OnValidateInput_t258_0_0_0_Types };
extern const Il2CppType ContentType_t250_0_0_0;
static const Il2CppType* GenInst_ContentType_t250_0_0_0_Types[] = { &ContentType_t250_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t250_0_0_0 = { 1, GenInst_ContentType_t250_0_0_0_Types };
extern const Il2CppType LineType_t253_0_0_0;
static const Il2CppType* GenInst_LineType_t253_0_0_0_Types[] = { &LineType_t253_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t253_0_0_0 = { 1, GenInst_LineType_t253_0_0_0_Types };
extern const Il2CppType InputType_t251_0_0_0;
static const Il2CppType* GenInst_InputType_t251_0_0_0_Types[] = { &InputType_t251_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t251_0_0_0 = { 1, GenInst_InputType_t251_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t425_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t425_0_0_0_Types[] = { &TouchScreenKeyboardType_t425_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t425_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t425_0_0_0_Types };
extern const Il2CppType CharacterValidation_t252_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t252_0_0_0_Types[] = { &CharacterValidation_t252_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t252_0_0_0 = { 1, GenInst_CharacterValidation_t252_0_0_0_Types };
extern const Il2CppType Char_t424_0_0_0;
static const Il2CppType* GenInst_Char_t424_0_0_0_Types[] = { &Char_t424_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t424_0_0_0 = { 1, GenInst_Char_t424_0_0_0_Types };
extern const Il2CppType UILineInfo_t428_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t428_0_0_0_Types[] = { &UILineInfo_t428_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t428_0_0_0 = { 1, GenInst_UILineInfo_t428_0_0_0_Types };
extern const Il2CppType UICharInfo_t430_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t430_0_0_0_Types[] = { &UICharInfo_t430_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t430_0_0_0 = { 1, GenInst_UICharInfo_t430_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t392_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t392_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t392_0_0_0_Types };
extern const Il2CppType LayoutElement_t334_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t334_0_0_0_Types[] = { &LayoutElement_t334_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t334_0_0_0 = { 1, GenInst_LayoutElement_t334_0_0_0_Types };
extern const Il2CppType IClippable_t384_0_0_0;
static const Il2CppType* GenInst_IClippable_t384_0_0_0_Types[] = { &IClippable_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t384_0_0_0 = { 1, GenInst_IClippable_t384_0_0_0_Types };
extern const Il2CppType RectMask2D_t276_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t276_0_0_0_Types[] = { &RectMask2D_t276_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t276_0_0_0 = { 1, GenInst_RectMask2D_t276_0_0_0_Types };
extern const Il2CppType Direction_t286_0_0_0;
static const Il2CppType* GenInst_Direction_t286_0_0_0_Types[] = { &Direction_t286_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t286_0_0_0 = { 1, GenInst_Direction_t286_0_0_0_Types };
extern const Il2CppType Vector2_t171_0_0_0;
static const Il2CppType* GenInst_Vector2_t171_0_0_0_Types[] = { &Vector2_t171_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t171_0_0_0 = { 1, GenInst_Vector2_t171_0_0_0_Types };
extern const Il2CppType Selectable_t202_0_0_0;
static const Il2CppType* GenInst_Selectable_t202_0_0_0_Types[] = { &Selectable_t202_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t202_0_0_0 = { 1, GenInst_Selectable_t202_0_0_0_Types };
extern const Il2CppType Navigation_t280_0_0_0;
static const Il2CppType* GenInst_Navigation_t280_0_0_0_Types[] = { &Navigation_t280_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t280_0_0_0 = { 1, GenInst_Navigation_t280_0_0_0_Types };
extern const Il2CppType Transition_t297_0_0_0;
static const Il2CppType* GenInst_Transition_t297_0_0_0_Types[] = { &Transition_t297_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t297_0_0_0 = { 1, GenInst_Transition_t297_0_0_0_Types };
extern const Il2CppType ColorBlock_t207_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t207_0_0_0_Types[] = { &ColorBlock_t207_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t207_0_0_0 = { 1, GenInst_ColorBlock_t207_0_0_0_Types };
extern const Il2CppType SpriteState_t299_0_0_0;
static const Il2CppType* GenInst_SpriteState_t299_0_0_0_Types[] = { &SpriteState_t299_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t299_0_0_0 = { 1, GenInst_SpriteState_t299_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t197_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t197_0_0_0_Types[] = { &AnimationTriggers_t197_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t197_0_0_0 = { 1, GenInst_AnimationTriggers_t197_0_0_0_Types };
extern const Il2CppType Animator_t5_0_0_0;
static const Il2CppType* GenInst_Animator_t5_0_0_0_Types[] = { &Animator_t5_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t5_0_0_0 = { 1, GenInst_Animator_t5_0_0_0_Types };
extern const Il2CppType Direction_t303_0_0_0;
static const Il2CppType* GenInst_Direction_t303_0_0_0_Types[] = { &Direction_t303_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t303_0_0_0 = { 1, GenInst_Direction_t303_0_0_0_Types };
extern const Il2CppType MatEntry_t307_0_0_0;
static const Il2CppType* GenInst_MatEntry_t307_0_0_0_Types[] = { &MatEntry_t307_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t307_0_0_0 = { 1, GenInst_MatEntry_t307_0_0_0_Types };
extern const Il2CppType UIVertex_t272_0_0_0;
static const Il2CppType* GenInst_UIVertex_t272_0_0_0_Types[] = { &UIVertex_t272_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t272_0_0_0 = { 1, GenInst_UIVertex_t272_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t213_0_0_0_Boolean_t393_0_0_0_Types[] = { &Toggle_t213_0_0_0, &Boolean_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t213_0_0_0_Boolean_t393_0_0_0 = { 2, GenInst_Toggle_t213_0_0_0_Boolean_t393_0_0_0_Types };
extern const Il2CppType IClipper_t387_0_0_0;
static const Il2CppType* GenInst_IClipper_t387_0_0_0_Types[] = { &IClipper_t387_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t387_0_0_0 = { 1, GenInst_IClipper_t387_0_0_0_Types };
extern const Il2CppType AspectMode_t319_0_0_0;
static const Il2CppType* GenInst_AspectMode_t319_0_0_0_Types[] = { &AspectMode_t319_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t319_0_0_0 = { 1, GenInst_AspectMode_t319_0_0_0_Types };
extern const Il2CppType FitMode_t325_0_0_0;
static const Il2CppType* GenInst_FitMode_t325_0_0_0_Types[] = { &FitMode_t325_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t325_0_0_0 = { 1, GenInst_FitMode_t325_0_0_0_Types };
extern const Il2CppType Corner_t327_0_0_0;
static const Il2CppType* GenInst_Corner_t327_0_0_0_Types[] = { &Corner_t327_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t327_0_0_0 = { 1, GenInst_Corner_t327_0_0_0_Types };
extern const Il2CppType Axis_t328_0_0_0;
static const Il2CppType* GenInst_Axis_t328_0_0_0_Types[] = { &Axis_t328_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t328_0_0_0 = { 1, GenInst_Axis_t328_0_0_0_Types };
extern const Il2CppType Constraint_t329_0_0_0;
static const Il2CppType* GenInst_Constraint_t329_0_0_0_Types[] = { &Constraint_t329_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t329_0_0_0 = { 1, GenInst_Constraint_t329_0_0_0_Types };
extern const Il2CppType RectOffset_t335_0_0_0;
static const Il2CppType* GenInst_RectOffset_t335_0_0_0_Types[] = { &RectOffset_t335_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t335_0_0_0 = { 1, GenInst_RectOffset_t335_0_0_0_Types };
extern const Il2CppType TextAnchor_t444_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t444_0_0_0_Types[] = { &TextAnchor_t444_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t444_0_0_0 = { 1, GenInst_TextAnchor_t444_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t337_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t337_0_0_0_Types[] = { &LayoutRebuilder_t337_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t337_0_0_0 = { 1, GenInst_LayoutRebuilder_t337_0_0_0_Types };
extern const Il2CppType ILayoutElement_t389_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t389_0_0_0_Single_t117_0_0_0_Types[] = { &ILayoutElement_t389_0_0_0, &Single_t117_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t389_0_0_0_Single_t117_0_0_0 = { 2, GenInst_ILayoutElement_t389_0_0_0_Single_t117_0_0_0_Types };
extern const Il2CppType Vector3_t12_0_0_0;
static const Il2CppType* GenInst_Vector3_t12_0_0_0_Types[] = { &Vector3_t12_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t12_0_0_0 = { 1, GenInst_Vector3_t12_0_0_0_Types };
extern const Il2CppType Color32_t382_0_0_0;
static const Il2CppType* GenInst_Color32_t382_0_0_0_Types[] = { &Color32_t382_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t382_0_0_0 = { 1, GenInst_Color32_t382_0_0_0_Types };
extern const Il2CppType Vector4_t350_0_0_0;
static const Il2CppType* GenInst_Vector4_t350_0_0_0_Types[] = { &Vector4_t350_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t350_0_0_0 = { 1, GenInst_Vector4_t350_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t483_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t483_0_0_0_Types[] = { &GcLeaderboard_t483_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t483_0_0_0 = { 1, GenInst_GcLeaderboard_t483_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t692_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t692_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t692_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t692_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t692_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t694_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t694_0_0_0_Types[] = { &IAchievementU5BU5D_t694_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t694_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t694_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t630_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t630_0_0_0_Types[] = { &IScoreU5BU5D_t630_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t630_0_0_0 = { 1, GenInst_IScoreU5BU5D_t630_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t626_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t626_0_0_0_Types[] = { &IUserProfileU5BU5D_t626_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t626_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t626_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t393_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t393_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t393_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t540_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t540_0_0_0_Types[] = { &Rigidbody2D_t540_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t540_0_0_0 = { 1, GenInst_Rigidbody2D_t540_0_0_0_Types };
extern const Il2CppType LayoutCache_t580_0_0_0;
static const Il2CppType* GenInst_Int32_t392_0_0_0_LayoutCache_t580_0_0_0_Types[] = { &Int32_t392_0_0_0, &LayoutCache_t580_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_LayoutCache_t580_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_LayoutCache_t580_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t585_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t585_0_0_0_Types[] = { &GUILayoutEntry_t585_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t585_0_0_0 = { 1, GenInst_GUILayoutEntry_t585_0_0_0_Types };
extern const Il2CppType GUIStyle_t584_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t584_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t584_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t584_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t584_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType GUILayer_t489_0_0_0;
static const Il2CppType* GenInst_GUILayer_t489_0_0_0_Types[] = { &GUILayer_t489_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t489_0_0_0 = { 1, GenInst_GUILayer_t489_0_0_0_Types };
extern const Il2CppType PersistentCall_t658_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t658_0_0_0_Types[] = { &PersistentCall_t658_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t658_0_0_0 = { 1, GenInst_PersistentCall_t658_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t655_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t655_0_0_0_Types[] = { &BaseInvokableCall_t655_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t655_0_0_0 = { 1, GenInst_BaseInvokableCall_t655_0_0_0_Types };
extern const Il2CppType VideoBackgroundAbstractBehaviour_t67_0_0_0;
static const Il2CppType* GenInst_Camera_t91_0_0_0_VideoBackgroundAbstractBehaviour_t67_0_0_0_Types[] = { &Camera_t91_0_0_0, &VideoBackgroundAbstractBehaviour_t67_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t91_0_0_0_VideoBackgroundAbstractBehaviour_t67_0_0_0 = { 2, GenInst_Camera_t91_0_0_0_VideoBackgroundAbstractBehaviour_t67_0_0_0_Types };
extern const Il2CppType BackgroundPlaneAbstractBehaviour_t19_0_0_0;
static const Il2CppType* GenInst_BackgroundPlaneAbstractBehaviour_t19_0_0_0_Types[] = { &BackgroundPlaneAbstractBehaviour_t19_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundPlaneAbstractBehaviour_t19_0_0_0 = { 1, GenInst_BackgroundPlaneAbstractBehaviour_t19_0_0_0_Types };
static const Il2CppType* GenInst_VideoBackgroundAbstractBehaviour_t67_0_0_0_Types[] = { &VideoBackgroundAbstractBehaviour_t67_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoBackgroundAbstractBehaviour_t67_0_0_0 = { 1, GenInst_VideoBackgroundAbstractBehaviour_t67_0_0_0_Types };
extern const Il2CppType ITrackableEventHandler_t922_0_0_0;
static const Il2CppType* GenInst_ITrackableEventHandler_t922_0_0_0_Types[] = { &ITrackableEventHandler_t922_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackableEventHandler_t922_0_0_0 = { 1, GenInst_ITrackableEventHandler_t922_0_0_0_Types };
extern const Il2CppType SmartTerrainTracker_t842_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTracker_t842_0_0_0_Types[] = { &SmartTerrainTracker_t842_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTracker_t842_0_0_0 = { 1, GenInst_SmartTerrainTracker_t842_0_0_0_Types };
extern const Il2CppType ReconstructionAbstractBehaviour_t53_0_0_0;
static const Il2CppType* GenInst_ReconstructionAbstractBehaviour_t53_0_0_0_Types[] = { &ReconstructionAbstractBehaviour_t53_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionAbstractBehaviour_t53_0_0_0 = { 1, GenInst_ReconstructionAbstractBehaviour_t53_0_0_0_Types };
extern const Il2CppType ICloudRecoEventHandler_t923_0_0_0;
static const Il2CppType* GenInst_ICloudRecoEventHandler_t923_0_0_0_Types[] = { &ICloudRecoEventHandler_t923_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloudRecoEventHandler_t923_0_0_0 = { 1, GenInst_ICloudRecoEventHandler_t923_0_0_0_Types };
extern const Il2CppType TargetSearchResult_t884_0_0_0;
static const Il2CppType* GenInst_TargetSearchResult_t884_0_0_0_Types[] = { &TargetSearchResult_t884_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetSearchResult_t884_0_0_0 = { 1, GenInst_TargetSearchResult_t884_0_0_0_Types };
extern const Il2CppType ObjectTracker_t744_0_0_0;
static const Il2CppType* GenInst_ObjectTracker_t744_0_0_0_Types[] = { &ObjectTracker_t744_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTracker_t744_0_0_0 = { 1, GenInst_ObjectTracker_t744_0_0_0_Types };
extern const Il2CppType HideExcessAreaAbstractBehaviour_t35_0_0_0;
static const Il2CppType* GenInst_HideExcessAreaAbstractBehaviour_t35_0_0_0_Types[] = { &HideExcessAreaAbstractBehaviour_t35_0_0_0 };
extern const Il2CppGenericInst GenInst_HideExcessAreaAbstractBehaviour_t35_0_0_0 = { 1, GenInst_HideExcessAreaAbstractBehaviour_t35_0_0_0_Types };
extern const Il2CppType ReconstructionFromTarget_t755_0_0_0;
static const Il2CppType* GenInst_ReconstructionFromTarget_t755_0_0_0_Types[] = { &ReconstructionFromTarget_t755_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionFromTarget_t755_0_0_0 = { 1, GenInst_ReconstructionFromTarget_t755_0_0_0_Types };
extern const Il2CppType PIXEL_FORMAT_t783_0_0_0;
extern const Il2CppType Image_t784_0_0_0;
static const Il2CppType* GenInst_PIXEL_FORMAT_t783_0_0_0_Image_t784_0_0_0_Types[] = { &PIXEL_FORMAT_t783_0_0_0, &Image_t784_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t783_0_0_0_Image_t784_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t783_0_0_0_Image_t784_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t783_0_0_0_Types[] = { &PIXEL_FORMAT_t783_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t783_0_0_0 = { 1, GenInst_PIXEL_FORMAT_t783_0_0_0_Types };
extern const Il2CppType Trackable_t737_0_0_0;
static const Il2CppType* GenInst_Int32_t392_0_0_0_Trackable_t737_0_0_0_Types[] = { &Int32_t392_0_0_0, &Trackable_t737_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Trackable_t737_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_Trackable_t737_0_0_0_Types };
static const Il2CppType* GenInst_Trackable_t737_0_0_0_Types[] = { &Trackable_t737_0_0_0 };
extern const Il2CppGenericInst GenInst_Trackable_t737_0_0_0 = { 1, GenInst_Trackable_t737_0_0_0_Types };
extern const Il2CppType VirtualButton_t895_0_0_0;
static const Il2CppType* GenInst_Int32_t392_0_0_0_VirtualButton_t895_0_0_0_Types[] = { &Int32_t392_0_0_0, &VirtualButton_t895_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_VirtualButton_t895_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_VirtualButton_t895_0_0_0_Types };
extern const Il2CppType DataSet_t764_0_0_0;
static const Il2CppType* GenInst_DataSet_t764_0_0_0_Types[] = { &DataSet_t764_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSet_t764_0_0_0 = { 1, GenInst_DataSet_t764_0_0_0_Types };
extern const Il2CppType DataSetImpl_t751_0_0_0;
static const Il2CppType* GenInst_DataSetImpl_t751_0_0_0_Types[] = { &DataSetImpl_t751_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetImpl_t751_0_0_0 = { 1, GenInst_DataSetImpl_t751_0_0_0_Types };
extern const Il2CppType Marker_t903_0_0_0;
static const Il2CppType* GenInst_Int32_t392_0_0_0_Marker_t903_0_0_0_Types[] = { &Int32_t392_0_0_0, &Marker_t903_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Marker_t903_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_Marker_t903_0_0_0_Types };
static const Il2CppType* GenInst_Marker_t903_0_0_0_Types[] = { &Marker_t903_0_0_0 };
extern const Il2CppGenericInst GenInst_Marker_t903_0_0_0 = { 1, GenInst_Marker_t903_0_0_0_Types };
extern const Il2CppType TrackableResultData_t810_0_0_0;
static const Il2CppType* GenInst_TrackableResultData_t810_0_0_0_Types[] = { &TrackableResultData_t810_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t810_0_0_0 = { 1, GenInst_TrackableResultData_t810_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackable_t762_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackable_t762_0_0_0_Types[] = { &SmartTerrainTrackable_t762_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackable_t762_0_0_0 = { 1, GenInst_SmartTerrainTrackable_t762_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackableBehaviour_t761_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackableBehaviour_t761_0_0_0_Types[] = { &SmartTerrainTrackableBehaviour_t761_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackableBehaviour_t761_0_0_0 = { 1, GenInst_SmartTerrainTrackableBehaviour_t761_0_0_0_Types };
extern const Il2CppType UInt16_t1430_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t1430_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t1430_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t1430_0_0_0 = { 2, GenInst_Type_t_0_0_0_UInt16_t1430_0_0_0_Types };
extern const Il2CppType WordAbstractBehaviour_t80_0_0_0;
static const Il2CppType* GenInst_WordAbstractBehaviour_t80_0_0_0_Types[] = { &WordAbstractBehaviour_t80_0_0_0 };
extern const Il2CppGenericInst GenInst_WordAbstractBehaviour_t80_0_0_0 = { 1, GenInst_WordAbstractBehaviour_t80_0_0_0_Types };
extern const Il2CppType List_1_t858_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t858_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t858_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t858_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t858_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_WordAbstractBehaviour_t80_0_0_0_Types[] = { &Int32_t392_0_0_0, &WordAbstractBehaviour_t80_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_WordAbstractBehaviour_t80_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_WordAbstractBehaviour_t80_0_0_0_Types };
extern const Il2CppType WordResult_t860_0_0_0;
static const Il2CppType* GenInst_Int32_t392_0_0_0_WordResult_t860_0_0_0_Types[] = { &Int32_t392_0_0_0, &WordResult_t860_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_WordResult_t860_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_WordResult_t860_0_0_0_Types };
extern const Il2CppType WordData_t815_0_0_0;
static const Il2CppType* GenInst_WordData_t815_0_0_0_Types[] = { &WordData_t815_0_0_0 };
extern const Il2CppGenericInst GenInst_WordData_t815_0_0_0 = { 1, GenInst_WordData_t815_0_0_0_Types };
extern const Il2CppType WordResultData_t814_0_0_0;
static const Il2CppType* GenInst_WordResultData_t814_0_0_0_Types[] = { &WordResultData_t814_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResultData_t814_0_0_0 = { 1, GenInst_WordResultData_t814_0_0_0_Types };
extern const Il2CppType Word_t862_0_0_0;
static const Il2CppType* GenInst_Word_t862_0_0_0_Types[] = { &Word_t862_0_0_0 };
extern const Il2CppGenericInst GenInst_Word_t862_0_0_0 = { 1, GenInst_Word_t862_0_0_0_Types };
static const Il2CppType* GenInst_WordResult_t860_0_0_0_Types[] = { &WordResult_t860_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResult_t860_0_0_0 = { 1, GenInst_WordResult_t860_0_0_0_Types };
extern const Il2CppType ILoadLevelEventHandler_t936_0_0_0;
static const Il2CppType* GenInst_ILoadLevelEventHandler_t936_0_0_0_Types[] = { &ILoadLevelEventHandler_t936_0_0_0 };
extern const Il2CppGenericInst GenInst_ILoadLevelEventHandler_t936_0_0_0 = { 1, GenInst_ILoadLevelEventHandler_t936_0_0_0_Types };
extern const Il2CppType SmartTerrainInitializationInfo_t760_0_0_0;
static const Il2CppType* GenInst_SmartTerrainInitializationInfo_t760_0_0_0_Types[] = { &SmartTerrainInitializationInfo_t760_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainInitializationInfo_t760_0_0_0 = { 1, GenInst_SmartTerrainInitializationInfo_t760_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_Prop_t81_0_0_0_Types[] = { &Int32_t392_0_0_0, &Prop_t81_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Prop_t81_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_Prop_t81_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_Surface_t82_0_0_0_Types[] = { &Int32_t392_0_0_0, &Surface_t82_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Surface_t82_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_Surface_t82_0_0_0_Types };
extern const Il2CppType ISmartTerrainEventHandler_t939_0_0_0;
static const Il2CppType* GenInst_ISmartTerrainEventHandler_t939_0_0_0_Types[] = { &ISmartTerrainEventHandler_t939_0_0_0 };
extern const Il2CppGenericInst GenInst_ISmartTerrainEventHandler_t939_0_0_0 = { 1, GenInst_ISmartTerrainEventHandler_t939_0_0_0_Types };
extern const Il2CppType PropAbstractBehaviour_t52_0_0_0;
static const Il2CppType* GenInst_Int32_t392_0_0_0_PropAbstractBehaviour_t52_0_0_0_Types[] = { &Int32_t392_0_0_0, &PropAbstractBehaviour_t52_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_PropAbstractBehaviour_t52_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_PropAbstractBehaviour_t52_0_0_0_Types };
extern const Il2CppType SurfaceAbstractBehaviour_t58_0_0_0;
static const Il2CppType* GenInst_Int32_t392_0_0_0_SurfaceAbstractBehaviour_t58_0_0_0_Types[] = { &Int32_t392_0_0_0, &SurfaceAbstractBehaviour_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_SurfaceAbstractBehaviour_t58_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_SurfaceAbstractBehaviour_t58_0_0_0_Types };
static const Il2CppType* GenInst_PropAbstractBehaviour_t52_0_0_0_Types[] = { &PropAbstractBehaviour_t52_0_0_0 };
extern const Il2CppGenericInst GenInst_PropAbstractBehaviour_t52_0_0_0 = { 1, GenInst_PropAbstractBehaviour_t52_0_0_0_Types };
static const Il2CppType* GenInst_SurfaceAbstractBehaviour_t58_0_0_0_Types[] = { &SurfaceAbstractBehaviour_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceAbstractBehaviour_t58_0_0_0 = { 1, GenInst_SurfaceAbstractBehaviour_t58_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_TrackableBehaviour_t32_0_0_0_Types[] = { &Int32_t392_0_0_0, &TrackableBehaviour_t32_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_TrackableBehaviour_t32_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_TrackableBehaviour_t32_0_0_0_Types };
extern const Il2CppType MarkerTracker_t797_0_0_0;
static const Il2CppType* GenInst_MarkerTracker_t797_0_0_0_Types[] = { &MarkerTracker_t797_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerTracker_t797_0_0_0 = { 1, GenInst_MarkerTracker_t797_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_Types[] = { &Int32_t392_0_0_0, &TrackableResultData_t810_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_Types };
extern const Il2CppType VirtualButtonData_t811_0_0_0;
static const Il2CppType* GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_Types[] = { &Int32_t392_0_0_0, &VirtualButtonData_t811_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_Types };
extern const Il2CppType VirtualButtonAbstractBehaviour_t71_0_0_0;
static const Il2CppType* GenInst_VirtualButtonAbstractBehaviour_t71_0_0_0_Types[] = { &VirtualButtonAbstractBehaviour_t71_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonAbstractBehaviour_t71_0_0_0 = { 1, GenInst_VirtualButtonAbstractBehaviour_t71_0_0_0_Types };
extern const Il2CppType ImageTarget_t897_0_0_0;
static const Il2CppType* GenInst_Int32_t392_0_0_0_ImageTarget_t897_0_0_0_Types[] = { &Int32_t392_0_0_0, &ImageTarget_t897_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_ImageTarget_t897_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_ImageTarget_t897_0_0_0_Types };
extern const Il2CppType ImageTargetAbstractBehaviour_t37_0_0_0;
static const Il2CppType* GenInst_ImageTargetAbstractBehaviour_t37_0_0_0_Types[] = { &ImageTargetAbstractBehaviour_t37_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetAbstractBehaviour_t37_0_0_0 = { 1, GenInst_ImageTargetAbstractBehaviour_t37_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_VirtualButtonAbstractBehaviour_t71_0_0_0_Types[] = { &Int32_t392_0_0_0, &VirtualButtonAbstractBehaviour_t71_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_VirtualButtonAbstractBehaviour_t71_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_VirtualButtonAbstractBehaviour_t71_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButton_t895_0_0_0_Types[] = { &VirtualButton_t895_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButton_t895_0_0_0 = { 1, GenInst_VirtualButton_t895_0_0_0_Types };
extern const Il2CppType ITrackerEventHandler_t946_0_0_0;
static const Il2CppType* GenInst_ITrackerEventHandler_t946_0_0_0_Types[] = { &ITrackerEventHandler_t946_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackerEventHandler_t946_0_0_0 = { 1, GenInst_ITrackerEventHandler_t946_0_0_0_Types };
extern const Il2CppType TextTracker_t844_0_0_0;
static const Il2CppType* GenInst_TextTracker_t844_0_0_0_Types[] = { &TextTracker_t844_0_0_0 };
extern const Il2CppGenericInst GenInst_TextTracker_t844_0_0_0 = { 1, GenInst_TextTracker_t844_0_0_0_Types };
extern const Il2CppType WebCamAbstractBehaviour_t75_0_0_0;
static const Il2CppType* GenInst_WebCamAbstractBehaviour_t75_0_0_0_Types[] = { &WebCamAbstractBehaviour_t75_0_0_0 };
extern const Il2CppGenericInst GenInst_WebCamAbstractBehaviour_t75_0_0_0 = { 1, GenInst_WebCamAbstractBehaviour_t75_0_0_0_Types };
extern const Il2CppType IVideoBackgroundEventHandler_t947_0_0_0;
static const Il2CppType* GenInst_IVideoBackgroundEventHandler_t947_0_0_0_Types[] = { &IVideoBackgroundEventHandler_t947_0_0_0 };
extern const Il2CppGenericInst GenInst_IVideoBackgroundEventHandler_t947_0_0_0 = { 1, GenInst_IVideoBackgroundEventHandler_t947_0_0_0_Types };
extern const Il2CppType ITextRecoEventHandler_t948_0_0_0;
static const Il2CppType* GenInst_ITextRecoEventHandler_t948_0_0_0_Types[] = { &ITextRecoEventHandler_t948_0_0_0 };
extern const Il2CppGenericInst GenInst_ITextRecoEventHandler_t948_0_0_0 = { 1, GenInst_ITextRecoEventHandler_t948_0_0_0_Types };
extern const Il2CppType IUserDefinedTargetEventHandler_t949_0_0_0;
static const Il2CppType* GenInst_IUserDefinedTargetEventHandler_t949_0_0_0_Types[] = { &IUserDefinedTargetEventHandler_t949_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserDefinedTargetEventHandler_t949_0_0_0 = { 1, GenInst_IUserDefinedTargetEventHandler_t949_0_0_0_Types };
extern const Il2CppType VuforiaAbstractBehaviour_t73_0_0_0;
static const Il2CppType* GenInst_VuforiaAbstractBehaviour_t73_0_0_0_Types[] = { &VuforiaAbstractBehaviour_t73_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaAbstractBehaviour_t73_0_0_0 = { 1, GenInst_VuforiaAbstractBehaviour_t73_0_0_0_Types };
extern const Il2CppType IVirtualButtonEventHandler_t950_0_0_0;
static const Il2CppType* GenInst_IVirtualButtonEventHandler_t950_0_0_0_Types[] = { &IVirtualButtonEventHandler_t950_0_0_0 };
extern const Il2CppGenericInst GenInst_IVirtualButtonEventHandler_t950_0_0_0 = { 1, GenInst_IVirtualButtonEventHandler_t950_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t1692_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1692_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1692_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1692_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1692_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t1691_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t1691_0_0_0_Types[] = { &CustomAttributeNamedArgument_t1691_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t1691_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t1691_0_0_0_Types };
extern const Il2CppType StrongName_t1939_0_0_0;
static const Il2CppType* GenInst_StrongName_t1939_0_0_0_Types[] = { &StrongName_t1939_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t1939_0_0_0 = { 1, GenInst_StrongName_t1939_0_0_0_Types };
extern const Il2CppType DateTime_t577_0_0_0;
static const Il2CppType* GenInst_DateTime_t577_0_0_0_Types[] = { &DateTime_t577_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t577_0_0_0 = { 1, GenInst_DateTime_t577_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t2014_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t2014_0_0_0_Types[] = { &DateTimeOffset_t2014_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t2014_0_0_0 = { 1, GenInst_DateTimeOffset_t2014_0_0_0_Types };
extern const Il2CppType TimeSpan_t975_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t975_0_0_0_Types[] = { &TimeSpan_t975_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t975_0_0_0 = { 1, GenInst_TimeSpan_t975_0_0_0_Types };
extern const Il2CppType Guid_t2036_0_0_0;
static const Il2CppType* GenInst_Guid_t2036_0_0_0_Types[] = { &Guid_t2036_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t2036_0_0_0 = { 1, GenInst_Guid_t2036_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t1688_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t1688_0_0_0_Types[] = { &CustomAttributeData_t1688_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t1688_0_0_0 = { 1, GenInst_CustomAttributeData_t1688_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t2_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t2_0_0_0_Types[] = { &MonoBehaviour_t2_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t2_0_0_0 = { 1, GenInst_MonoBehaviour_t2_0_0_0_Types };
extern const Il2CppType Behaviour_t450_0_0_0;
static const Il2CppType* GenInst_Behaviour_t450_0_0_0_Types[] = { &Behaviour_t450_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t450_0_0_0 = { 1, GenInst_Behaviour_t450_0_0_0_Types };
extern const Il2CppType Object_t86_0_0_0;
static const Il2CppType* GenInst_Object_t86_0_0_0_Types[] = { &Object_t86_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t86_0_0_0 = { 1, GenInst_Object_t86_0_0_0_Types };
extern const Il2CppType IReflect_t3455_0_0_0;
static const Il2CppType* GenInst_IReflect_t3455_0_0_0_Types[] = { &IReflect_t3455_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t3455_0_0_0 = { 1, GenInst_IReflect_t3455_0_0_0_Types };
extern const Il2CppType _Type_t3453_0_0_0;
static const Il2CppType* GenInst__Type_t3453_0_0_0_Types[] = { &_Type_t3453_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t3453_0_0_0 = { 1, GenInst__Type_t3453_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t2122_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t2122_0_0_0_Types[] = { &ICustomAttributeProvider_t2122_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t2122_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t2122_0_0_0_Types };
extern const Il2CppType _MemberInfo_t3454_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t3454_0_0_0_Types[] = { &_MemberInfo_t3454_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t3454_0_0_0 = { 1, GenInst__MemberInfo_t3454_0_0_0_Types };
extern const Il2CppType IFormattable_t2124_0_0_0;
static const Il2CppType* GenInst_IFormattable_t2124_0_0_0_Types[] = { &IFormattable_t2124_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t2124_0_0_0 = { 1, GenInst_IFormattable_t2124_0_0_0_Types };
extern const Il2CppType IConvertible_t2127_0_0_0;
static const Il2CppType* GenInst_IConvertible_t2127_0_0_0_Types[] = { &IConvertible_t2127_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t2127_0_0_0 = { 1, GenInst_IConvertible_t2127_0_0_0_Types };
extern const Il2CppType IComparable_t2126_0_0_0;
static const Il2CppType* GenInst_IComparable_t2126_0_0_0_Types[] = { &IComparable_t2126_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t2126_0_0_0 = { 1, GenInst_IComparable_t2126_0_0_0_Types };
extern const Il2CppType IComparable_1_t3690_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3690_0_0_0_Types[] = { &IComparable_1_t3690_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3690_0_0_0 = { 1, GenInst_IComparable_1_t3690_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3695_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3695_0_0_0_Types[] = { &IEquatable_1_t3695_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3695_0_0_0 = { 1, GenInst_IEquatable_1_t3695_0_0_0_Types };
extern const Il2CppType ValueType_t1437_0_0_0;
static const Il2CppType* GenInst_ValueType_t1437_0_0_0_Types[] = { &ValueType_t1437_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t1437_0_0_0 = { 1, GenInst_ValueType_t1437_0_0_0_Types };
extern const Il2CppType Double_t710_0_0_0;
static const Il2CppType* GenInst_Double_t710_0_0_0_Types[] = { &Double_t710_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t710_0_0_0 = { 1, GenInst_Double_t710_0_0_0_Types };
extern const Il2CppType IComparable_1_t3707_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3707_0_0_0_Types[] = { &IComparable_1_t3707_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3707_0_0_0 = { 1, GenInst_IComparable_1_t3707_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3712_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3712_0_0_0_Types[] = { &IEquatable_1_t3712_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3712_0_0_0 = { 1, GenInst_IEquatable_1_t3712_0_0_0_Types };
extern const Il2CppType IComparable_1_t3720_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3720_0_0_0_Types[] = { &IComparable_1_t3720_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3720_0_0_0 = { 1, GenInst_IComparable_1_t3720_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3725_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3725_0_0_0_Types[] = { &IEquatable_1_t3725_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3725_0_0_0 = { 1, GenInst_IEquatable_1_t3725_0_0_0_Types };
extern const Il2CppType IEnumerable_t988_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t988_0_0_0_Types[] = { &IEnumerable_t988_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t988_0_0_0 = { 1, GenInst_IEnumerable_t988_0_0_0_Types };
extern const Il2CppType ICloneable_t2137_0_0_0;
static const Il2CppType* GenInst_ICloneable_t2137_0_0_0_Types[] = { &ICloneable_t2137_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t2137_0_0_0 = { 1, GenInst_ICloneable_t2137_0_0_0_Types };
extern const Il2CppType IComparable_1_t3740_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3740_0_0_0_Types[] = { &IComparable_1_t3740_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3740_0_0_0 = { 1, GenInst_IComparable_1_t3740_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3745_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3745_0_0_0_Types[] = { &IEquatable_1_t3745_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3745_0_0_0 = { 1, GenInst_IEquatable_1_t3745_0_0_0_Types };
extern const Il2CppType List_1_t128_0_0_0;
static const Il2CppType* GenInst_List_1_t128_0_0_0_Types[] = { &List_1_t128_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t128_0_0_0 = { 1, GenInst_List_1_t128_0_0_0_Types };
extern const Il2CppType List_1_t388_0_0_0;
static const Il2CppType* GenInst_List_1_t388_0_0_0_Types[] = { &List_1_t388_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t388_0_0_0 = { 1, GenInst_List_1_t388_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t392_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Object_t_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2280_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2280_0_0_0_Types[] = { &KeyValuePair_2_t2280_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2280_0_0_0 = { 1, GenInst_KeyValuePair_2_t2280_0_0_0_Types };
extern const Il2CppType Link_t1547_0_0_0;
static const Il2CppType* GenInst_Link_t1547_0_0_0_Types[] = { &Link_t1547_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1547_0_0_0 = { 1, GenInst_Link_t1547_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_Object_t_0_0_0_Int32_t392_0_0_0_Types[] = { &Int32_t392_0_0_0, &Object_t_0_0_0, &Int32_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Object_t_0_0_0_Int32_t392_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_Object_t_0_0_0_Int32_t392_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t392_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t1423_0_0_0;
static const Il2CppType* GenInst_Int32_t392_0_0_0_Object_t_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Object_t_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_Object_t_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1423_0_0_0_Types[] = { &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1423_0_0_0 = { 1, GenInst_DictionaryEntry_t1423_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2280_0_0_0_Types[] = { &Int32_t392_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2280_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2280_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2280_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_PointerEventData_t175_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &PointerEventData_t175_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_PointerEventData_t175_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_PointerEventData_t175_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t397_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t397_0_0_0_Types[] = { &KeyValuePair_2_t397_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t397_0_0_0 = { 1, GenInst_KeyValuePair_2_t397_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t402_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t402_0_0_0_Types[] = { &RaycastHit2D_t402_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t402_0_0_0 = { 1, GenInst_RaycastHit2D_t402_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t392_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t392_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int32_t392_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2312_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2312_0_0_0_Types[] = { &KeyValuePair_2_t2312_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2312_0_0_0 = { 1, GenInst_KeyValuePair_2_t2312_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t392_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t392_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t392_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t392_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t392_0_0_0_Int32_t392_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t392_0_0_0, &Int32_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t392_0_0_0_Int32_t392_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t392_0_0_0_Int32_t392_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t392_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t392_0_0_0_KeyValuePair_2_t2312_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t392_0_0_0, &KeyValuePair_2_t2312_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t392_0_0_0_KeyValuePair_2_t2312_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t392_0_0_0_KeyValuePair_2_t2312_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t379_0_0_0_Int32_t392_0_0_0_Types[] = { &ICanvasElement_t379_0_0_0, &Int32_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t379_0_0_0_Int32_t392_0_0_0 = { 2, GenInst_ICanvasElement_t379_0_0_0_Int32_t392_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t379_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &ICanvasElement_t379_0_0_0, &Int32_t392_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t379_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_ICanvasElement_t379_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType List_1_t409_0_0_0;
static const Il2CppType* GenInst_List_1_t409_0_0_0_Types[] = { &List_1_t409_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t409_0_0_0 = { 1, GenInst_List_1_t409_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2365_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2365_0_0_0_Types[] = { &KeyValuePair_2_t2365_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2365_0_0_0 = { 1, GenInst_KeyValuePair_2_t2365_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2365_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2365_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2365_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2365_0_0_0_Types };
static const Il2CppType* GenInst_Font_t225_0_0_0_List_1_t413_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Font_t225_0_0_0, &List_1_t413_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t225_0_0_0_List_1_t413_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Font_t225_0_0_0_List_1_t413_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2375_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2375_0_0_0_Types[] = { &KeyValuePair_2_t2375_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2375_0_0_0 = { 1, GenInst_KeyValuePair_2_t2375_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t228_0_0_0_Int32_t392_0_0_0_Types[] = { &Graphic_t228_0_0_0, &Int32_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t228_0_0_0_Int32_t392_0_0_0 = { 2, GenInst_Graphic_t228_0_0_0_Int32_t392_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t228_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Graphic_t228_0_0_0, &Int32_t392_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t228_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Graphic_t228_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t230_0_0_0_IndexedSet_1_t422_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Canvas_t230_0_0_0, &IndexedSet_1_t422_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t230_0_0_0_IndexedSet_1_t422_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Canvas_t230_0_0_0_IndexedSet_1_t422_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2399_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2399_0_0_0_Types[] = { &KeyValuePair_2_t2399_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2399_0_0_0 = { 1, GenInst_KeyValuePair_2_t2399_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2403_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2403_0_0_0_Types[] = { &KeyValuePair_2_t2403_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2403_0_0_0 = { 1, GenInst_KeyValuePair_2_t2403_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2407_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2407_0_0_0_Types[] = { &KeyValuePair_2_t2407_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2407_0_0_0 = { 1, GenInst_KeyValuePair_2_t2407_0_0_0_Types };
extern const Il2CppType Enum_t700_0_0_0;
static const Il2CppType* GenInst_Enum_t700_0_0_0_Types[] = { &Enum_t700_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t700_0_0_0 = { 1, GenInst_Enum_t700_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t392_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2415_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2415_0_0_0_Types[] = { &KeyValuePair_2_t2415_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2415_0_0_0 = { 1, GenInst_KeyValuePair_2_t2415_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t393_0_0_0 = { 2, GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t387_0_0_0_Int32_t392_0_0_0_Types[] = { &IClipper_t387_0_0_0, &Int32_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t387_0_0_0_Int32_t392_0_0_0 = { 2, GenInst_IClipper_t387_0_0_0_Int32_t392_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t387_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &IClipper_t387_0_0_0, &Int32_t392_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t387_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_IClipper_t387_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2467_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2467_0_0_0_Types[] = { &KeyValuePair_2_t2467_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2467_0_0_0 = { 1, GenInst_KeyValuePair_2_t2467_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t117_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t117_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t117_0_0_0 = { 2, GenInst_Object_t_0_0_0_Single_t117_0_0_0_Types };
extern const Il2CppType List_1_t345_0_0_0;
static const Il2CppType* GenInst_List_1_t345_0_0_0_Types[] = { &List_1_t345_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t345_0_0_0 = { 1, GenInst_List_1_t345_0_0_0_Types };
extern const Il2CppType List_1_t346_0_0_0;
static const Il2CppType* GenInst_List_1_t346_0_0_0_Types[] = { &List_1_t346_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t346_0_0_0 = { 1, GenInst_List_1_t346_0_0_0_Types };
extern const Il2CppType List_1_t347_0_0_0;
static const Il2CppType* GenInst_List_1_t347_0_0_0_Types[] = { &List_1_t347_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t347_0_0_0 = { 1, GenInst_List_1_t347_0_0_0_Types };
extern const Il2CppType List_1_t348_0_0_0;
static const Il2CppType* GenInst_List_1_t348_0_0_0_Types[] = { &List_1_t348_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t348_0_0_0 = { 1, GenInst_List_1_t348_0_0_0_Types };
extern const Il2CppType List_1_t349_0_0_0;
static const Il2CppType* GenInst_List_1_t349_0_0_0_Types[] = { &List_1_t349_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t349_0_0_0 = { 1, GenInst_List_1_t349_0_0_0_Types };
extern const Il2CppType List_1_t380_0_0_0;
static const Il2CppType* GenInst_List_1_t380_0_0_0_Types[] = { &List_1_t380_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t380_0_0_0 = { 1, GenInst_List_1_t380_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t3413_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t3413_0_0_0_Types[] = { &IAchievementDescription_t3413_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3413_0_0_0 = { 1, GenInst_IAchievementDescription_t3413_0_0_0_Types };
extern const Il2CppType IAchievement_t677_0_0_0;
static const Il2CppType* GenInst_IAchievement_t677_0_0_0_Types[] = { &IAchievement_t677_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t677_0_0_0 = { 1, GenInst_IAchievement_t677_0_0_0_Types };
extern const Il2CppType IScore_t632_0_0_0;
static const Il2CppType* GenInst_IScore_t632_0_0_0_Types[] = { &IScore_t632_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t632_0_0_0 = { 1, GenInst_IScore_t632_0_0_0_Types };
extern const Il2CppType IUserProfile_t3412_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t3412_0_0_0_Types[] = { &IUserProfile_t3412_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t3412_0_0_0 = { 1, GenInst_IUserProfile_t3412_0_0_0_Types };
extern const Il2CppType AchievementDescription_t628_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t628_0_0_0_Types[] = { &AchievementDescription_t628_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t628_0_0_0 = { 1, GenInst_AchievementDescription_t628_0_0_0_Types };
extern const Il2CppType UserProfile_t625_0_0_0;
static const Il2CppType* GenInst_UserProfile_t625_0_0_0_Types[] = { &UserProfile_t625_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t625_0_0_0 = { 1, GenInst_UserProfile_t625_0_0_0_Types };
extern const Il2CppType GcAchievementData_t613_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t613_0_0_0_Types[] = { &GcAchievementData_t613_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t613_0_0_0 = { 1, GenInst_GcAchievementData_t613_0_0_0_Types };
extern const Il2CppType Achievement_t627_0_0_0;
static const Il2CppType* GenInst_Achievement_t627_0_0_0_Types[] = { &Achievement_t627_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t627_0_0_0 = { 1, GenInst_Achievement_t627_0_0_0_Types };
extern const Il2CppType GcScoreData_t614_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t614_0_0_0_Types[] = { &GcScoreData_t614_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t614_0_0_0 = { 1, GenInst_GcScoreData_t614_0_0_0_Types };
extern const Il2CppType Score_t629_0_0_0;
static const Il2CppType* GenInst_Score_t629_0_0_0_Types[] = { &Score_t629_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t629_0_0_0 = { 1, GenInst_Score_t629_0_0_0_Types };
extern const Il2CppType IComparable_1_t3924_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3924_0_0_0_Types[] = { &IComparable_1_t3924_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3924_0_0_0 = { 1, GenInst_IComparable_1_t3924_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3929_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3929_0_0_0_Types[] = { &IEquatable_1_t3929_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3929_0_0_0 = { 1, GenInst_IEquatable_1_t3929_0_0_0_Types };
extern const Il2CppType Display_t518_0_0_0;
static const Il2CppType* GenInst_Display_t518_0_0_0_Types[] = { &Display_t518_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t518_0_0_0 = { 1, GenInst_Display_t518_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType ISerializable_t2156_0_0_0;
static const Il2CppType* GenInst_ISerializable_t2156_0_0_0_Types[] = { &ISerializable_t2156_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t2156_0_0_0 = { 1, GenInst_ISerializable_t2156_0_0_0_Types };
extern const Il2CppType ContactPoint_t534_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t534_0_0_0_Types[] = { &ContactPoint_t534_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t534_0_0_0 = { 1, GenInst_ContactPoint_t534_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t541_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t541_0_0_0_Types[] = { &ContactPoint2D_t541_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t541_0_0_0 = { 1, GenInst_ContactPoint2D_t541_0_0_0_Types };
extern const Il2CppType WebCamDevice_t550_0_0_0;
static const Il2CppType* GenInst_WebCamDevice_t550_0_0_0_Types[] = { &WebCamDevice_t550_0_0_0 };
extern const Il2CppGenericInst GenInst_WebCamDevice_t550_0_0_0 = { 1, GenInst_WebCamDevice_t550_0_0_0_Types };
extern const Il2CppType Keyframe_t557_0_0_0;
static const Il2CppType* GenInst_Keyframe_t557_0_0_0_Types[] = { &Keyframe_t557_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t557_0_0_0 = { 1, GenInst_Keyframe_t557_0_0_0_Types };
extern const Il2CppType CharacterInfo_t566_0_0_0;
static const Il2CppType* GenInst_CharacterInfo_t566_0_0_0_Types[] = { &CharacterInfo_t566_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterInfo_t566_0_0_0 = { 1, GenInst_CharacterInfo_t566_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t589_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t589_0_0_0_Types[] = { &GUILayoutOption_t589_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t589_0_0_0 = { 1, GenInst_GUILayoutOption_t589_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_LayoutCache_t580_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &LayoutCache_t580_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_LayoutCache_t580_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_LayoutCache_t580_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2605_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2605_0_0_0_Types[] = { &KeyValuePair_2_t2605_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2605_0_0_0 = { 1, GenInst_KeyValuePair_2_t2605_0_0_0_Types };
static const Il2CppType* GenInst_GUIStyle_t584_0_0_0_Types[] = { &GUIStyle_t584_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t584_0_0_0 = { 1, GenInst_GUIStyle_t584_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t584_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t584_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t584_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t584_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2616_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2616_0_0_0_Types[] = { &KeyValuePair_2_t2616_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2616_0_0_0 = { 1, GenInst_KeyValuePair_2_t2616_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t603_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t603_0_0_0_Types[] = { &DisallowMultipleComponent_t603_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t603_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t603_0_0_0_Types };
extern const Il2CppType Attribute_t104_0_0_0;
static const Il2CppType* GenInst_Attribute_t104_0_0_0_Types[] = { &Attribute_t104_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t104_0_0_0 = { 1, GenInst_Attribute_t104_0_0_0_Types };
extern const Il2CppType _Attribute_t3443_0_0_0;
static const Il2CppType* GenInst__Attribute_t3443_0_0_0_Types[] = { &_Attribute_t3443_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t3443_0_0_0 = { 1, GenInst__Attribute_t3443_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t606_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t606_0_0_0_Types[] = { &ExecuteInEditMode_t606_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t606_0_0_0 = { 1, GenInst_ExecuteInEditMode_t606_0_0_0_Types };
extern const Il2CppType RequireComponent_t604_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t604_0_0_0_Types[] = { &RequireComponent_t604_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t604_0_0_0 = { 1, GenInst_RequireComponent_t604_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1710_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1710_0_0_0_Types[] = { &ParameterModifier_t1710_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1710_0_0_0 = { 1, GenInst_ParameterModifier_t1710_0_0_0_Types };
extern const Il2CppType HitInfo_t633_0_0_0;
static const Il2CppType* GenInst_HitInfo_t633_0_0_0_Types[] = { &HitInfo_t633_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t633_0_0_0 = { 1, GenInst_HitInfo_t633_0_0_0_Types };
extern const Il2CppType ParameterInfo_t716_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t716_0_0_0_Types[] = { &ParameterInfo_t716_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t716_0_0_0 = { 1, GenInst_ParameterInfo_t716_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t3496_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t3496_0_0_0_Types[] = { &_ParameterInfo_t3496_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t3496_0_0_0 = { 1, GenInst__ParameterInfo_t3496_0_0_0_Types };
extern const Il2CppType Event_t269_0_0_0;
extern const Il2CppType TextEditOp_t650_0_0_0;
static const Il2CppType* GenInst_Event_t269_0_0_0_TextEditOp_t650_0_0_0_Types[] = { &Event_t269_0_0_0, &TextEditOp_t650_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t269_0_0_0_TextEditOp_t650_0_0_0 = { 2, GenInst_Event_t269_0_0_0_TextEditOp_t650_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t650_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0 = { 2, GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2636_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2636_0_0_0_Types[] = { &KeyValuePair_2_t2636_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2636_0_0_0 = { 1, GenInst_KeyValuePair_2_t2636_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t650_0_0_0_Types[] = { &TextEditOp_t650_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t650_0_0_0 = { 1, GenInst_TextEditOp_t650_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t650_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_TextEditOp_t650_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t650_0_0_0, &TextEditOp_t650_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_TextEditOp_t650_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_TextEditOp_t650_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t650_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_KeyValuePair_2_t2636_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t650_0_0_0, &KeyValuePair_2_t2636_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_KeyValuePair_2_t2636_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_KeyValuePair_2_t2636_0_0_0_Types };
static const Il2CppType* GenInst_Event_t269_0_0_0_Types[] = { &Event_t269_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t269_0_0_0 = { 1, GenInst_Event_t269_0_0_0_Types };
static const Il2CppType* GenInst_Event_t269_0_0_0_TextEditOp_t650_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Event_t269_0_0_0, &TextEditOp_t650_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t269_0_0_0_TextEditOp_t650_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Event_t269_0_0_0_TextEditOp_t650_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2650_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2650_0_0_0_Types[] = { &KeyValuePair_2_t2650_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2650_0_0_0 = { 1, GenInst_KeyValuePair_2_t2650_0_0_0_Types };
static const Il2CppType* GenInst_Camera_t91_0_0_0_VideoBackgroundAbstractBehaviour_t67_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Camera_t91_0_0_0, &VideoBackgroundAbstractBehaviour_t67_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t91_0_0_0_VideoBackgroundAbstractBehaviour_t67_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Camera_t91_0_0_0_VideoBackgroundAbstractBehaviour_t67_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2679_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2679_0_0_0_Types[] = { &KeyValuePair_2_t2679_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2679_0_0_0 = { 1, GenInst_KeyValuePair_2_t2679_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_Types[] = { &PIXEL_FORMAT_t783_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2708_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2708_0_0_0_Types[] = { &KeyValuePair_2_t2708_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2708_0_0_0 = { 1, GenInst_KeyValuePair_2_t2708_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_PIXEL_FORMAT_t783_0_0_0_Types[] = { &PIXEL_FORMAT_t783_0_0_0, &Object_t_0_0_0, &PIXEL_FORMAT_t783_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_PIXEL_FORMAT_t783_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_PIXEL_FORMAT_t783_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &PIXEL_FORMAT_t783_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &PIXEL_FORMAT_t783_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2708_0_0_0_Types[] = { &PIXEL_FORMAT_t783_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2708_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2708_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2708_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t783_0_0_0_Image_t784_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &PIXEL_FORMAT_t783_0_0_0, &Image_t784_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t783_0_0_0_Image_t784_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t783_0_0_0_Image_t784_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2722_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2722_0_0_0_Types[] = { &KeyValuePair_2_t2722_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2722_0_0_0 = { 1, GenInst_KeyValuePair_2_t2722_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_Trackable_t737_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &Trackable_t737_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Trackable_t737_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_Trackable_t737_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2735_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2735_0_0_0_Types[] = { &KeyValuePair_2_t2735_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2735_0_0_0 = { 1, GenInst_KeyValuePair_2_t2735_0_0_0_Types };
extern const Il2CppType Byte_t699_0_0_0;
static const Il2CppType* GenInst_Byte_t699_0_0_0_Types[] = { &Byte_t699_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t699_0_0_0 = { 1, GenInst_Byte_t699_0_0_0_Types };
extern const Il2CppType IComparable_1_t4055_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4055_0_0_0_Types[] = { &IComparable_1_t4055_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4055_0_0_0 = { 1, GenInst_IComparable_1_t4055_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4060_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4060_0_0_0_Types[] = { &IEquatable_1_t4060_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4060_0_0_0 = { 1, GenInst_IEquatable_1_t4060_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_VirtualButton_t895_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &VirtualButton_t895_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_VirtualButton_t895_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_VirtualButton_t895_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2745_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2745_0_0_0_Types[] = { &KeyValuePair_2_t2745_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2745_0_0_0 = { 1, GenInst_KeyValuePair_2_t2745_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_Marker_t903_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &Marker_t903_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Marker_t903_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_Marker_t903_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2762_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2762_0_0_0_Types[] = { &KeyValuePair_2_t2762_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2762_0_0_0 = { 1, GenInst_KeyValuePair_2_t2762_0_0_0_Types };
extern const Il2CppType SmartTerrainRevisionData_t818_0_0_0;
static const Il2CppType* GenInst_SmartTerrainRevisionData_t818_0_0_0_Types[] = { &SmartTerrainRevisionData_t818_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainRevisionData_t818_0_0_0 = { 1, GenInst_SmartTerrainRevisionData_t818_0_0_0_Types };
extern const Il2CppType SurfaceData_t819_0_0_0;
static const Il2CppType* GenInst_SurfaceData_t819_0_0_0_Types[] = { &SurfaceData_t819_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceData_t819_0_0_0 = { 1, GenInst_SurfaceData_t819_0_0_0_Types };
extern const Il2CppType PropData_t820_0_0_0;
static const Il2CppType* GenInst_PropData_t820_0_0_0_Types[] = { &PropData_t820_0_0_0 };
extern const Il2CppGenericInst GenInst_PropData_t820_0_0_0 = { 1, GenInst_PropData_t820_0_0_0_Types };
extern const Il2CppType IEditorTrackableBehaviour_t981_0_0_0;
static const Il2CppType* GenInst_IEditorTrackableBehaviour_t981_0_0_0_Types[] = { &IEditorTrackableBehaviour_t981_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorTrackableBehaviour_t981_0_0_0 = { 1, GenInst_IEditorTrackableBehaviour_t981_0_0_0_Types };
static const Il2CppType* GenInst_Image_t784_0_0_0_Types[] = { &Image_t784_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t784_0_0_0 = { 1, GenInst_Image_t784_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t1430_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0 = { 2, GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2792_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2792_0_0_0_Types[] = { &KeyValuePair_2_t2792_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2792_0_0_0 = { 1, GenInst_KeyValuePair_2_t2792_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t1430_0_0_0_Types[] = { &UInt16_t1430_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t1430_0_0_0 = { 1, GenInst_UInt16_t1430_0_0_0_Types };
extern const Il2CppType IComparable_1_t4111_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4111_0_0_0_Types[] = { &IComparable_1_t4111_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4111_0_0_0 = { 1, GenInst_IComparable_1_t4111_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4116_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4116_0_0_0_Types[] = { &IEquatable_1_t4116_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4116_0_0_0 = { 1, GenInst_IEquatable_1_t4116_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t1430_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_UInt16_t1430_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t1430_0_0_0, &UInt16_t1430_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_UInt16_t1430_0_0_0 = { 3, GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_UInt16_t1430_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t1430_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_KeyValuePair_2_t2792_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t1430_0_0_0, &KeyValuePair_2_t2792_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_KeyValuePair_2_t2792_0_0_0 = { 3, GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_KeyValuePair_2_t2792_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t1430_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t1430_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t1430_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Type_t_0_0_0_UInt16_t1430_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2807_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2807_0_0_0_Types[] = { &KeyValuePair_2_t2807_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2807_0_0_0 = { 1, GenInst_KeyValuePair_2_t2807_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_WordResult_t860_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &WordResult_t860_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_WordResult_t860_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_WordResult_t860_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2813_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2813_0_0_0_Types[] = { &KeyValuePair_2_t2813_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2813_0_0_0 = { 1, GenInst_KeyValuePair_2_t2813_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_WordAbstractBehaviour_t80_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &WordAbstractBehaviour_t80_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_WordAbstractBehaviour_t80_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_WordAbstractBehaviour_t80_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1001_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1001_0_0_0_Types[] = { &KeyValuePair_2_t1001_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1001_0_0_0 = { 1, GenInst_KeyValuePair_2_t1001_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t858_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t858_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t858_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t858_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2833_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2833_0_0_0_Types[] = { &KeyValuePair_2_t2833_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2833_0_0_0 = { 1, GenInst_KeyValuePair_2_t2833_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t858_0_0_0_Types[] = { &List_1_t858_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t858_0_0_0 = { 1, GenInst_List_1_t858_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_Surface_t82_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &Surface_t82_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Surface_t82_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_Surface_t82_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1014_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1014_0_0_0_Types[] = { &KeyValuePair_2_t1014_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1014_0_0_0 = { 1, GenInst_KeyValuePair_2_t1014_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_SurfaceAbstractBehaviour_t58_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &SurfaceAbstractBehaviour_t58_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_SurfaceAbstractBehaviour_t58_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_SurfaceAbstractBehaviour_t58_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2854_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2854_0_0_0_Types[] = { &KeyValuePair_2_t2854_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2854_0_0_0 = { 1, GenInst_KeyValuePair_2_t2854_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_Prop_t81_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &Prop_t81_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Prop_t81_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_Prop_t81_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1013_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1013_0_0_0_Types[] = { &KeyValuePair_2_t1013_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1013_0_0_0 = { 1, GenInst_KeyValuePair_2_t1013_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_PropAbstractBehaviour_t52_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &PropAbstractBehaviour_t52_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_PropAbstractBehaviour_t52_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_PropAbstractBehaviour_t52_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2860_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2860_0_0_0_Types[] = { &KeyValuePair_2_t2860_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2860_0_0_0 = { 1, GenInst_KeyValuePair_2_t2860_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_TrackableBehaviour_t32_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &TrackableBehaviour_t32_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_TrackableBehaviour_t32_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_TrackableBehaviour_t32_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2875_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2875_0_0_0_Types[] = { &KeyValuePair_2_t2875_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2875_0_0_0 = { 1, GenInst_KeyValuePair_2_t2875_0_0_0_Types };
extern const Il2CppType MarkerAbstractBehaviour_t45_0_0_0;
static const Il2CppType* GenInst_MarkerAbstractBehaviour_t45_0_0_0_Types[] = { &MarkerAbstractBehaviour_t45_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerAbstractBehaviour_t45_0_0_0 = { 1, GenInst_MarkerAbstractBehaviour_t45_0_0_0_Types };
extern const Il2CppType IEditorMarkerBehaviour_t1030_0_0_0;
static const Il2CppType* GenInst_IEditorMarkerBehaviour_t1030_0_0_0_Types[] = { &IEditorMarkerBehaviour_t1030_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorMarkerBehaviour_t1030_0_0_0 = { 1, GenInst_IEditorMarkerBehaviour_t1030_0_0_0_Types };
extern const Il2CppType WorldCenterTrackableBehaviour_t828_0_0_0;
static const Il2CppType* GenInst_WorldCenterTrackableBehaviour_t828_0_0_0_Types[] = { &WorldCenterTrackableBehaviour_t828_0_0_0 };
extern const Il2CppGenericInst GenInst_WorldCenterTrackableBehaviour_t828_0_0_0 = { 1, GenInst_WorldCenterTrackableBehaviour_t828_0_0_0_Types };
extern const Il2CppType DataSetTrackableBehaviour_t738_0_0_0;
static const Il2CppType* GenInst_DataSetTrackableBehaviour_t738_0_0_0_Types[] = { &DataSetTrackableBehaviour_t738_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetTrackableBehaviour_t738_0_0_0 = { 1, GenInst_DataSetTrackableBehaviour_t738_0_0_0_Types };
extern const Il2CppType IEditorDataSetTrackableBehaviour_t1032_0_0_0;
static const Il2CppType* GenInst_IEditorDataSetTrackableBehaviour_t1032_0_0_0_Types[] = { &IEditorDataSetTrackableBehaviour_t1032_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorDataSetTrackableBehaviour_t1032_0_0_0 = { 1, GenInst_IEditorDataSetTrackableBehaviour_t1032_0_0_0_Types };
extern const Il2CppType IEditorVirtualButtonBehaviour_t1044_0_0_0;
static const Il2CppType* GenInst_IEditorVirtualButtonBehaviour_t1044_0_0_0_Types[] = { &IEditorVirtualButtonBehaviour_t1044_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorVirtualButtonBehaviour_t1044_0_0_0 = { 1, GenInst_IEditorVirtualButtonBehaviour_t1044_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2881_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2881_0_0_0_Types[] = { &KeyValuePair_2_t2881_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2881_0_0_0 = { 1, GenInst_KeyValuePair_2_t2881_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_Int32_t392_0_0_0_Types[] = { &Int32_t392_0_0_0, &TrackableResultData_t810_0_0_0, &Int32_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_Int32_t392_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_Int32_t392_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_TrackableResultData_t810_0_0_0_Types[] = { &Int32_t392_0_0_0, &TrackableResultData_t810_0_0_0, &TrackableResultData_t810_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_TrackableResultData_t810_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_TrackableResultData_t810_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &TrackableResultData_t810_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_KeyValuePair_2_t2881_0_0_0_Types[] = { &Int32_t392_0_0_0, &TrackableResultData_t810_0_0_0, &KeyValuePair_2_t2881_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_KeyValuePair_2_t2881_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_KeyValuePair_2_t2881_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2896_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2896_0_0_0_Types[] = { &KeyValuePair_2_t2896_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2896_0_0_0 = { 1, GenInst_KeyValuePair_2_t2896_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t811_0_0_0_Types[] = { &VirtualButtonData_t811_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t811_0_0_0 = { 1, GenInst_VirtualButtonData_t811_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_Int32_t392_0_0_0_Types[] = { &Int32_t392_0_0_0, &VirtualButtonData_t811_0_0_0, &Int32_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_Int32_t392_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_Int32_t392_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_VirtualButtonData_t811_0_0_0_Types[] = { &Int32_t392_0_0_0, &VirtualButtonData_t811_0_0_0, &VirtualButtonData_t811_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_VirtualButtonData_t811_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_VirtualButtonData_t811_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &VirtualButtonData_t811_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_KeyValuePair_2_t2896_0_0_0_Types[] = { &Int32_t392_0_0_0, &VirtualButtonData_t811_0_0_0, &KeyValuePair_2_t2896_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_KeyValuePair_2_t2896_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_KeyValuePair_2_t2896_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_ImageTarget_t897_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &ImageTarget_t897_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_ImageTarget_t897_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_ImageTarget_t897_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2927_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2927_0_0_0_Types[] = { &KeyValuePair_2_t2927_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2927_0_0_0 = { 1, GenInst_KeyValuePair_2_t2927_0_0_0_Types };
extern const Il2CppType ProfileData_t898_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t898_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t898_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t898_0_0_0 = { 2, GenInst_String_t_0_0_0_ProfileData_t898_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t898_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0 = { 2, GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2935_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2935_0_0_0_Types[] = { &KeyValuePair_2_t2935_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2935_0_0_0 = { 1, GenInst_KeyValuePair_2_t2935_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t898_0_0_0_Types[] = { &ProfileData_t898_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t898_0_0_0 = { 1, GenInst_ProfileData_t898_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t898_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_ProfileData_t898_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t898_0_0_0, &ProfileData_t898_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_ProfileData_t898_0_0_0 = { 3, GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_ProfileData_t898_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t898_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_KeyValuePair_2_t2935_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t898_0_0_0, &KeyValuePair_2_t2935_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_KeyValuePair_2_t2935_0_0_0 = { 3, GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_KeyValuePair_2_t2935_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t898_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t898_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t898_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_String_t_0_0_0_ProfileData_t898_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2949_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2949_0_0_0_Types[] = { &KeyValuePair_2_t2949_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2949_0_0_0 = { 1, GenInst_KeyValuePair_2_t2949_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_VirtualButtonAbstractBehaviour_t71_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Int32_t392_0_0_0, &VirtualButtonAbstractBehaviour_t71_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_VirtualButtonAbstractBehaviour_t71_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Int32_t392_0_0_0_VirtualButtonAbstractBehaviour_t71_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2954_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2954_0_0_0_Types[] = { &KeyValuePair_2_t2954_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2954_0_0_0 = { 1, GenInst_KeyValuePair_2_t2954_0_0_0_Types };
extern const Il2CppType Link_t2983_0_0_0;
static const Il2CppType* GenInst_Link_t2983_0_0_0_Types[] = { &Link_t2983_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2983_0_0_0 = { 1, GenInst_Link_t2983_0_0_0_Types };
extern const Il2CppType KeySizes_t1087_0_0_0;
static const Il2CppType* GenInst_KeySizes_t1087_0_0_0_Types[] = { &KeySizes_t1087_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t1087_0_0_0 = { 1, GenInst_KeySizes_t1087_0_0_0_Types };
extern const Il2CppType UInt32_t712_0_0_0;
static const Il2CppType* GenInst_UInt32_t712_0_0_0_Types[] = { &UInt32_t712_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t712_0_0_0 = { 1, GenInst_UInt32_t712_0_0_0_Types };
extern const Il2CppType IComparable_1_t4230_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4230_0_0_0_Types[] = { &IComparable_1_t4230_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4230_0_0_0 = { 1, GenInst_IComparable_1_t4230_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4235_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4235_0_0_0_Types[] = { &IEquatable_1_t4235_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4235_0_0_0 = { 1, GenInst_IEquatable_1_t4235_0_0_0_Types };
extern const Il2CppType BigInteger_t1092_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1092_0_0_0_Types[] = { &BigInteger_t1092_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1092_0_0_0 = { 1, GenInst_BigInteger_t1092_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t786_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t786_0_0_0_Types[] = { &ByteU5BU5D_t786_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t786_0_0_0 = { 1, GenInst_ByteU5BU5D_t786_0_0_0_Types };
extern const Il2CppType Array_t_0_0_0;
static const Il2CppType* GenInst_Array_t_0_0_0_Types[] = { &Array_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_t_0_0_0 = { 1, GenInst_Array_t_0_0_0_Types };
extern const Il2CppType ICollection_t1424_0_0_0;
static const Il2CppType* GenInst_ICollection_t1424_0_0_0_Types[] = { &ICollection_t1424_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t1424_0_0_0 = { 1, GenInst_ICollection_t1424_0_0_0_Types };
extern const Il2CppType IList_t1384_0_0_0;
static const Il2CppType* GenInst_IList_t1384_0_0_0_Types[] = { &IList_t1384_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t1384_0_0_0 = { 1, GenInst_IList_t1384_0_0_0_Types };
extern const Il2CppType X509Certificate_t1199_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t1199_0_0_0_Types[] = { &X509Certificate_t1199_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t1199_0_0_0 = { 1, GenInst_X509Certificate_t1199_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t2159_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t2159_0_0_0_Types[] = { &IDeserializationCallback_t2159_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t2159_0_0_0 = { 1, GenInst_IDeserializationCallback_t2159_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t1203_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t1203_0_0_0_Types[] = { &ClientCertificateType_t1203_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1203_0_0_0 = { 1, GenInst_ClientCertificateType_t1203_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3005_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3005_0_0_0_Types[] = { &KeyValuePair_2_t3005_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3005_0_0_0 = { 1, GenInst_KeyValuePair_2_t3005_0_0_0_Types };
extern const Il2CppType IComparable_1_t4278_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4278_0_0_0_Types[] = { &IComparable_1_t4278_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4278_0_0_0 = { 1, GenInst_IComparable_1_t4278_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4283_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4283_0_0_0_Types[] = { &IEquatable_1_t4283_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4283_0_0_0 = { 1, GenInst_IEquatable_1_t4283_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t393_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_Boolean_t393_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t393_0_0_0, &Boolean_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_Boolean_t393_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_Boolean_t393_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t393_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_KeyValuePair_2_t3005_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t393_0_0_0, &KeyValuePair_2_t3005_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_KeyValuePair_2_t3005_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_KeyValuePair_2_t3005_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t393_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t393_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t393_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t393_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3020_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3020_0_0_0_Types[] = { &KeyValuePair_2_t3020_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3020_0_0_0 = { 1, GenInst_KeyValuePair_2_t3020_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t1333_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t1333_0_0_0_Types[] = { &X509ChainStatus_t1333_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t1333_0_0_0 = { 1, GenInst_X509ChainStatus_t1333_0_0_0_Types };
extern const Il2CppType Capture_t1352_0_0_0;
static const Il2CppType* GenInst_Capture_t1352_0_0_0_Types[] = { &Capture_t1352_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t1352_0_0_0 = { 1, GenInst_Capture_t1352_0_0_0_Types };
extern const Il2CppType Group_t1271_0_0_0;
static const Il2CppType* GenInst_Group_t1271_0_0_0_Types[] = { &Group_t1271_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t1271_0_0_0 = { 1, GenInst_Group_t1271_0_0_0_Types };
extern const Il2CppType Mark_t1375_0_0_0;
static const Il2CppType* GenInst_Mark_t1375_0_0_0_Types[] = { &Mark_t1375_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t1375_0_0_0 = { 1, GenInst_Mark_t1375_0_0_0_Types };
extern const Il2CppType UriScheme_t1412_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1412_0_0_0_Types[] = { &UriScheme_t1412_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1412_0_0_0 = { 1, GenInst_UriScheme_t1412_0_0_0_Types };
extern const Il2CppType Int64_t711_0_0_0;
static const Il2CppType* GenInst_Int64_t711_0_0_0_Types[] = { &Int64_t711_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t711_0_0_0 = { 1, GenInst_Int64_t711_0_0_0_Types };
extern const Il2CppType UInt64_t1442_0_0_0;
static const Il2CppType* GenInst_UInt64_t1442_0_0_0_Types[] = { &UInt64_t1442_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t1442_0_0_0 = { 1, GenInst_UInt64_t1442_0_0_0_Types };
extern const Il2CppType SByte_t1443_0_0_0;
static const Il2CppType* GenInst_SByte_t1443_0_0_0_Types[] = { &SByte_t1443_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t1443_0_0_0 = { 1, GenInst_SByte_t1443_0_0_0_Types };
extern const Il2CppType Int16_t1444_0_0_0;
static const Il2CppType* GenInst_Int16_t1444_0_0_0_Types[] = { &Int16_t1444_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t1444_0_0_0 = { 1, GenInst_Int16_t1444_0_0_0_Types };
extern const Il2CppType Decimal_t1445_0_0_0;
static const Il2CppType* GenInst_Decimal_t1445_0_0_0_Types[] = { &Decimal_t1445_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t1445_0_0_0 = { 1, GenInst_Decimal_t1445_0_0_0_Types };
extern const Il2CppType Delegate_t110_0_0_0;
static const Il2CppType* GenInst_Delegate_t110_0_0_0_Types[] = { &Delegate_t110_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t110_0_0_0 = { 1, GenInst_Delegate_t110_0_0_0_Types };
extern const Il2CppType IComparable_1_t4308_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4308_0_0_0_Types[] = { &IComparable_1_t4308_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4308_0_0_0 = { 1, GenInst_IComparable_1_t4308_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4309_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4309_0_0_0_Types[] = { &IEquatable_1_t4309_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4309_0_0_0 = { 1, GenInst_IEquatable_1_t4309_0_0_0_Types };
extern const Il2CppType IComparable_1_t4312_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4312_0_0_0_Types[] = { &IComparable_1_t4312_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4312_0_0_0 = { 1, GenInst_IComparable_1_t4312_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4313_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4313_0_0_0_Types[] = { &IEquatable_1_t4313_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4313_0_0_0 = { 1, GenInst_IEquatable_1_t4313_0_0_0_Types };
extern const Il2CppType IComparable_1_t4310_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4310_0_0_0_Types[] = { &IComparable_1_t4310_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4310_0_0_0 = { 1, GenInst_IComparable_1_t4310_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4311_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4311_0_0_0_Types[] = { &IEquatable_1_t4311_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4311_0_0_0 = { 1, GenInst_IEquatable_1_t4311_0_0_0_Types };
extern const Il2CppType IComparable_1_t4306_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4306_0_0_0_Types[] = { &IComparable_1_t4306_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4306_0_0_0 = { 1, GenInst_IComparable_1_t4306_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4307_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4307_0_0_0_Types[] = { &IEquatable_1_t4307_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4307_0_0_0 = { 1, GenInst_IEquatable_1_t4307_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t3492_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t3492_0_0_0_Types[] = { &_FieldInfo_t3492_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t3492_0_0_0 = { 1, GenInst__FieldInfo_t3492_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t723_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t723_0_0_0_Types[] = { &ConstructorInfo_t723_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t723_0_0_0 = { 1, GenInst_ConstructorInfo_t723_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3490_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3490_0_0_0_Types[] = { &_ConstructorInfo_t3490_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3490_0_0_0 = { 1, GenInst__ConstructorInfo_t3490_0_0_0_Types };
extern const Il2CppType MethodBase_t714_0_0_0;
static const Il2CppType* GenInst_MethodBase_t714_0_0_0_Types[] = { &MethodBase_t714_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t714_0_0_0 = { 1, GenInst_MethodBase_t714_0_0_0_Types };
extern const Il2CppType _MethodBase_t3493_0_0_0;
static const Il2CppType* GenInst__MethodBase_t3493_0_0_0_Types[] = { &_MethodBase_t3493_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t3493_0_0_0 = { 1, GenInst__MethodBase_t3493_0_0_0_Types };
extern const Il2CppType TableRange_t1480_0_0_0;
static const Il2CppType* GenInst_TableRange_t1480_0_0_0_Types[] = { &TableRange_t1480_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t1480_0_0_0 = { 1, GenInst_TableRange_t1480_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1483_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1483_0_0_0_Types[] = { &TailoringInfo_t1483_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1483_0_0_0 = { 1, GenInst_TailoringInfo_t1483_0_0_0_Types };
extern const Il2CppType Contraction_t1484_0_0_0;
static const Il2CppType* GenInst_Contraction_t1484_0_0_0_Types[] = { &Contraction_t1484_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1484_0_0_0 = { 1, GenInst_Contraction_t1484_0_0_0_Types };
extern const Il2CppType Level2Map_t1486_0_0_0;
static const Il2CppType* GenInst_Level2Map_t1486_0_0_0_Types[] = { &Level2Map_t1486_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t1486_0_0_0 = { 1, GenInst_Level2Map_t1486_0_0_0_Types };
extern const Il2CppType BigInteger_t1507_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1507_0_0_0_Types[] = { &BigInteger_t1507_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1507_0_0_0 = { 1, GenInst_BigInteger_t1507_0_0_0_Types };
extern const Il2CppType Slot_t1557_0_0_0;
static const Il2CppType* GenInst_Slot_t1557_0_0_0_Types[] = { &Slot_t1557_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1557_0_0_0 = { 1, GenInst_Slot_t1557_0_0_0_Types };
extern const Il2CppType Slot_t1565_0_0_0;
static const Il2CppType* GenInst_Slot_t1565_0_0_0_Types[] = { &Slot_t1565_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1565_0_0_0 = { 1, GenInst_Slot_t1565_0_0_0_Types };
extern const Il2CppType StackFrame_t713_0_0_0;
static const Il2CppType* GenInst_StackFrame_t713_0_0_0_Types[] = { &StackFrame_t713_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t713_0_0_0 = { 1, GenInst_StackFrame_t713_0_0_0_Types };
extern const Il2CppType Calendar_t1578_0_0_0;
static const Il2CppType* GenInst_Calendar_t1578_0_0_0_Types[] = { &Calendar_t1578_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t1578_0_0_0 = { 1, GenInst_Calendar_t1578_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t1655_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t1655_0_0_0_Types[] = { &ModuleBuilder_t1655_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t1655_0_0_0 = { 1, GenInst_ModuleBuilder_t1655_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t3485_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t3485_0_0_0_Types[] = { &_ModuleBuilder_t3485_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t3485_0_0_0 = { 1, GenInst__ModuleBuilder_t3485_0_0_0_Types };
extern const Il2CppType Module_t1651_0_0_0;
static const Il2CppType* GenInst_Module_t1651_0_0_0_Types[] = { &Module_t1651_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t1651_0_0_0 = { 1, GenInst_Module_t1651_0_0_0_Types };
extern const Il2CppType _Module_t3495_0_0_0;
static const Il2CppType* GenInst__Module_t3495_0_0_0_Types[] = { &_Module_t3495_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t3495_0_0_0 = { 1, GenInst__Module_t3495_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t1661_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t1661_0_0_0_Types[] = { &ParameterBuilder_t1661_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1661_0_0_0 = { 1, GenInst_ParameterBuilder_t1661_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t3486_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t3486_0_0_0_Types[] = { &_ParameterBuilder_t3486_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t3486_0_0_0 = { 1, GenInst__ParameterBuilder_t3486_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t679_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t679_0_0_0_Types[] = { &TypeU5BU5D_t679_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t679_0_0_0 = { 1, GenInst_TypeU5BU5D_t679_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t1645_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t1645_0_0_0_Types[] = { &ILTokenInfo_t1645_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1645_0_0_0 = { 1, GenInst_ILTokenInfo_t1645_0_0_0_Types };
extern const Il2CppType LabelData_t1647_0_0_0;
static const Il2CppType* GenInst_LabelData_t1647_0_0_0_Types[] = { &LabelData_t1647_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t1647_0_0_0 = { 1, GenInst_LabelData_t1647_0_0_0_Types };
extern const Il2CppType LabelFixup_t1646_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t1646_0_0_0_Types[] = { &LabelFixup_t1646_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t1646_0_0_0 = { 1, GenInst_LabelFixup_t1646_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1643_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1643_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1643_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1643_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1643_0_0_0_Types };
extern const Il2CppType TypeBuilder_t1637_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t1637_0_0_0_Types[] = { &TypeBuilder_t1637_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1637_0_0_0 = { 1, GenInst_TypeBuilder_t1637_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t3487_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t3487_0_0_0_Types[] = { &_TypeBuilder_t3487_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t3487_0_0_0 = { 1, GenInst__TypeBuilder_t3487_0_0_0_Types };
extern const Il2CppType MethodBuilder_t1644_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t1644_0_0_0_Types[] = { &MethodBuilder_t1644_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t1644_0_0_0 = { 1, GenInst_MethodBuilder_t1644_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t3484_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t3484_0_0_0_Types[] = { &_MethodBuilder_t3484_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3484_0_0_0 = { 1, GenInst__MethodBuilder_t3484_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3494_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3494_0_0_0_Types[] = { &_MethodInfo_t3494_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3494_0_0_0 = { 1, GenInst__MethodInfo_t3494_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t1635_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t1635_0_0_0_Types[] = { &ConstructorBuilder_t1635_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t1635_0_0_0 = { 1, GenInst_ConstructorBuilder_t1635_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t3480_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t3480_0_0_0_Types[] = { &_ConstructorBuilder_t3480_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t3480_0_0_0 = { 1, GenInst__ConstructorBuilder_t3480_0_0_0_Types };
extern const Il2CppType FieldBuilder_t1641_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t1641_0_0_0_Types[] = { &FieldBuilder_t1641_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t1641_0_0_0 = { 1, GenInst_FieldBuilder_t1641_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t3482_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t3482_0_0_0_Types[] = { &_FieldBuilder_t3482_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t3482_0_0_0 = { 1, GenInst__FieldBuilder_t3482_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t3497_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t3497_0_0_0_Types[] = { &_PropertyInfo_t3497_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t3497_0_0_0 = { 1, GenInst__PropertyInfo_t3497_0_0_0_Types };
extern const Il2CppType ResourceInfo_t1721_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t1721_0_0_0_Types[] = { &ResourceInfo_t1721_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t1721_0_0_0 = { 1, GenInst_ResourceInfo_t1721_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t1722_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t1722_0_0_0_Types[] = { &ResourceCacheItem_t1722_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t1722_0_0_0 = { 1, GenInst_ResourceCacheItem_t1722_0_0_0_Types };
extern const Il2CppType IContextProperty_t2116_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t2116_0_0_0_Types[] = { &IContextProperty_t2116_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t2116_0_0_0 = { 1, GenInst_IContextProperty_t2116_0_0_0_Types };
extern const Il2CppType Header_t1802_0_0_0;
static const Il2CppType* GenInst_Header_t1802_0_0_0_Types[] = { &Header_t1802_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t1802_0_0_0 = { 1, GenInst_Header_t1802_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2151_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2151_0_0_0_Types[] = { &ITrackingHandler_t2151_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2151_0_0_0 = { 1, GenInst_ITrackingHandler_t2151_0_0_0_Types };
extern const Il2CppType IContextAttribute_t2138_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t2138_0_0_0_Types[] = { &IContextAttribute_t2138_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2138_0_0_0 = { 1, GenInst_IContextAttribute_t2138_0_0_0_Types };
extern const Il2CppType IComparable_1_t4538_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4538_0_0_0_Types[] = { &IComparable_1_t4538_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4538_0_0_0 = { 1, GenInst_IComparable_1_t4538_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4543_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4543_0_0_0_Types[] = { &IEquatable_1_t4543_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4543_0_0_0 = { 1, GenInst_IEquatable_1_t4543_0_0_0_Types };
extern const Il2CppType IComparable_1_t4314_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4314_0_0_0_Types[] = { &IComparable_1_t4314_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4314_0_0_0 = { 1, GenInst_IComparable_1_t4314_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4315_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4315_0_0_0_Types[] = { &IEquatable_1_t4315_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4315_0_0_0 = { 1, GenInst_IEquatable_1_t4315_0_0_0_Types };
extern const Il2CppType IComparable_1_t4562_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4562_0_0_0_Types[] = { &IComparable_1_t4562_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4562_0_0_0 = { 1, GenInst_IComparable_1_t4562_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4567_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4567_0_0_0_Types[] = { &IEquatable_1_t4567_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4567_0_0_0 = { 1, GenInst_IEquatable_1_t4567_0_0_0_Types };
extern const Il2CppType TypeTag_t1858_0_0_0;
static const Il2CppType* GenInst_TypeTag_t1858_0_0_0_Types[] = { &TypeTag_t1858_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t1858_0_0_0 = { 1, GenInst_TypeTag_t1858_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType Version_t1304_0_0_0;
static const Il2CppType* GenInst_Version_t1304_0_0_0_Types[] = { &Version_t1304_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1304_0_0_0 = { 1, GenInst_Version_t1304_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t169_0_0_0_RaycastResult_t169_0_0_0_Types[] = { &RaycastResult_t169_0_0_0, &RaycastResult_t169_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t169_0_0_0_RaycastResult_t169_0_0_0 = { 2, GenInst_RaycastResult_t169_0_0_0_RaycastResult_t169_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t392_0_0_0_Int32_t392_0_0_0_Types[] = { &Int32_t392_0_0_0, &Int32_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t392_0_0_0_Int32_t392_0_0_0 = { 2, GenInst_Int32_t392_0_0_0_Int32_t392_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1423_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &DictionaryEntry_t1423_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1423_0_0_0_DictionaryEntry_t1423_0_0_0 = { 2, GenInst_DictionaryEntry_t1423_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2280_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2280_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2280_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2280_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2280_0_0_0_KeyValuePair_2_t2280_0_0_0_Types[] = { &KeyValuePair_2_t2280_0_0_0, &KeyValuePair_2_t2280_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2280_0_0_0_KeyValuePair_2_t2280_0_0_0 = { 2, GenInst_KeyValuePair_2_t2280_0_0_0_KeyValuePair_2_t2280_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2312_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2312_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2312_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2312_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2312_0_0_0_KeyValuePair_2_t2312_0_0_0_Types[] = { &KeyValuePair_2_t2312_0_0_0, &KeyValuePair_2_t2312_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2312_0_0_0_KeyValuePair_2_t2312_0_0_0 = { 2, GenInst_KeyValuePair_2_t2312_0_0_0_KeyValuePair_2_t2312_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2365_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2365_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2365_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2365_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2365_0_0_0_KeyValuePair_2_t2365_0_0_0_Types[] = { &KeyValuePair_2_t2365_0_0_0, &KeyValuePair_2_t2365_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2365_0_0_0_KeyValuePair_2_t2365_0_0_0 = { 2, GenInst_KeyValuePair_2_t2365_0_0_0_KeyValuePair_2_t2365_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t272_0_0_0_UIVertex_t272_0_0_0_Types[] = { &UIVertex_t272_0_0_0, &UIVertex_t272_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t272_0_0_0_UIVertex_t272_0_0_0 = { 2, GenInst_UIVertex_t272_0_0_0_UIVertex_t272_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t12_0_0_0_Vector3_t12_0_0_0_Types[] = { &Vector3_t12_0_0_0, &Vector3_t12_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t12_0_0_0_Vector3_t12_0_0_0 = { 2, GenInst_Vector3_t12_0_0_0_Vector3_t12_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t382_0_0_0_Color32_t382_0_0_0_Types[] = { &Color32_t382_0_0_0, &Color32_t382_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t382_0_0_0_Color32_t382_0_0_0 = { 2, GenInst_Color32_t382_0_0_0_Color32_t382_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t171_0_0_0_Vector2_t171_0_0_0_Types[] = { &Vector2_t171_0_0_0, &Vector2_t171_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t171_0_0_0_Vector2_t171_0_0_0 = { 2, GenInst_Vector2_t171_0_0_0_Vector2_t171_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t350_0_0_0_Vector4_t350_0_0_0_Types[] = { &Vector4_t350_0_0_0, &Vector4_t350_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t350_0_0_0_Vector4_t350_0_0_0 = { 2, GenInst_Vector4_t350_0_0_0_Vector4_t350_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t430_0_0_0_UICharInfo_t430_0_0_0_Types[] = { &UICharInfo_t430_0_0_0, &UICharInfo_t430_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t430_0_0_0_UICharInfo_t430_0_0_0 = { 2, GenInst_UICharInfo_t430_0_0_0_UICharInfo_t430_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t428_0_0_0_UILineInfo_t428_0_0_0_Types[] = { &UILineInfo_t428_0_0_0, &UILineInfo_t428_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t428_0_0_0_UILineInfo_t428_0_0_0 = { 2, GenInst_UILineInfo_t428_0_0_0_UILineInfo_t428_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t650_0_0_0_Object_t_0_0_0_Types[] = { &TextEditOp_t650_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t650_0_0_0_Object_t_0_0_0 = { 2, GenInst_TextEditOp_t650_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t650_0_0_0_TextEditOp_t650_0_0_0_Types[] = { &TextEditOp_t650_0_0_0, &TextEditOp_t650_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t650_0_0_0_TextEditOp_t650_0_0_0 = { 2, GenInst_TextEditOp_t650_0_0_0_TextEditOp_t650_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2636_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2636_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2636_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2636_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2636_0_0_0_KeyValuePair_2_t2636_0_0_0_Types[] = { &KeyValuePair_2_t2636_0_0_0, &KeyValuePair_2_t2636_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2636_0_0_0_KeyValuePair_2_t2636_0_0_0 = { 2, GenInst_KeyValuePair_2_t2636_0_0_0_KeyValuePair_2_t2636_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t783_0_0_0_PIXEL_FORMAT_t783_0_0_0_Types[] = { &PIXEL_FORMAT_t783_0_0_0, &PIXEL_FORMAT_t783_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t783_0_0_0_PIXEL_FORMAT_t783_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t783_0_0_0_PIXEL_FORMAT_t783_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2708_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2708_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2708_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2708_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2708_0_0_0_KeyValuePair_2_t2708_0_0_0_Types[] = { &KeyValuePair_2_t2708_0_0_0, &KeyValuePair_2_t2708_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2708_0_0_0_KeyValuePair_2_t2708_0_0_0 = { 2, GenInst_KeyValuePair_2_t2708_0_0_0_KeyValuePair_2_t2708_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t1430_0_0_0_Object_t_0_0_0_Types[] = { &UInt16_t1430_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t1430_0_0_0_Object_t_0_0_0 = { 2, GenInst_UInt16_t1430_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t1430_0_0_0_UInt16_t1430_0_0_0_Types[] = { &UInt16_t1430_0_0_0, &UInt16_t1430_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t1430_0_0_0_UInt16_t1430_0_0_0 = { 2, GenInst_UInt16_t1430_0_0_0_UInt16_t1430_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2792_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2792_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2792_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2792_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2792_0_0_0_KeyValuePair_2_t2792_0_0_0_Types[] = { &KeyValuePair_2_t2792_0_0_0, &KeyValuePair_2_t2792_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2792_0_0_0_KeyValuePair_2_t2792_0_0_0 = { 2, GenInst_KeyValuePair_2_t2792_0_0_0_KeyValuePair_2_t2792_0_0_0_Types };
static const Il2CppType* GenInst_TrackableResultData_t810_0_0_0_Object_t_0_0_0_Types[] = { &TrackableResultData_t810_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t810_0_0_0_Object_t_0_0_0 = { 2, GenInst_TrackableResultData_t810_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TrackableResultData_t810_0_0_0_TrackableResultData_t810_0_0_0_Types[] = { &TrackableResultData_t810_0_0_0, &TrackableResultData_t810_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t810_0_0_0_TrackableResultData_t810_0_0_0 = { 2, GenInst_TrackableResultData_t810_0_0_0_TrackableResultData_t810_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2881_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2881_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2881_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2881_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2881_0_0_0_KeyValuePair_2_t2881_0_0_0_Types[] = { &KeyValuePair_2_t2881_0_0_0, &KeyValuePair_2_t2881_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2881_0_0_0_KeyValuePair_2_t2881_0_0_0 = { 2, GenInst_KeyValuePair_2_t2881_0_0_0_KeyValuePair_2_t2881_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t811_0_0_0_Object_t_0_0_0_Types[] = { &VirtualButtonData_t811_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t811_0_0_0_Object_t_0_0_0 = { 2, GenInst_VirtualButtonData_t811_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t811_0_0_0_VirtualButtonData_t811_0_0_0_Types[] = { &VirtualButtonData_t811_0_0_0, &VirtualButtonData_t811_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t811_0_0_0_VirtualButtonData_t811_0_0_0 = { 2, GenInst_VirtualButtonData_t811_0_0_0_VirtualButtonData_t811_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2896_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2896_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2896_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2896_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2896_0_0_0_KeyValuePair_2_t2896_0_0_0_Types[] = { &KeyValuePair_2_t2896_0_0_0, &KeyValuePair_2_t2896_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2896_0_0_0_KeyValuePair_2_t2896_0_0_0 = { 2, GenInst_KeyValuePair_2_t2896_0_0_0_KeyValuePair_2_t2896_0_0_0_Types };
static const Il2CppType* GenInst_TargetSearchResult_t884_0_0_0_TargetSearchResult_t884_0_0_0_Types[] = { &TargetSearchResult_t884_0_0_0, &TargetSearchResult_t884_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetSearchResult_t884_0_0_0_TargetSearchResult_t884_0_0_0 = { 2, GenInst_TargetSearchResult_t884_0_0_0_TargetSearchResult_t884_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t898_0_0_0_Object_t_0_0_0_Types[] = { &ProfileData_t898_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t898_0_0_0_Object_t_0_0_0 = { 2, GenInst_ProfileData_t898_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t898_0_0_0_ProfileData_t898_0_0_0_Types[] = { &ProfileData_t898_0_0_0, &ProfileData_t898_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t898_0_0_0_ProfileData_t898_0_0_0 = { 2, GenInst_ProfileData_t898_0_0_0_ProfileData_t898_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2935_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2935_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2935_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2935_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2935_0_0_0_KeyValuePair_2_t2935_0_0_0_Types[] = { &KeyValuePair_2_t2935_0_0_0, &KeyValuePair_2_t2935_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2935_0_0_0_KeyValuePair_2_t2935_0_0_0 = { 2, GenInst_KeyValuePair_2_t2935_0_0_0_KeyValuePair_2_t2935_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t393_0_0_0_Object_t_0_0_0_Types[] = { &Boolean_t393_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t393_0_0_0_Object_t_0_0_0 = { 2, GenInst_Boolean_t393_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t393_0_0_0_Boolean_t393_0_0_0_Types[] = { &Boolean_t393_0_0_0, &Boolean_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t393_0_0_0_Boolean_t393_0_0_0 = { 2, GenInst_Boolean_t393_0_0_0_Boolean_t393_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3005_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3005_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3005_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3005_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3005_0_0_0_KeyValuePair_2_t3005_0_0_0_Types[] = { &KeyValuePair_2_t3005_0_0_0, &KeyValuePair_2_t3005_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3005_0_0_0_KeyValuePair_2_t3005_0_0_0 = { 2, GenInst_KeyValuePair_2_t3005_0_0_0_KeyValuePair_2_t3005_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1692_0_0_0_CustomAttributeTypedArgument_t1692_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1692_0_0_0, &CustomAttributeTypedArgument_t1692_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1692_0_0_0_CustomAttributeTypedArgument_t1692_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1692_0_0_0_CustomAttributeTypedArgument_t1692_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t1691_0_0_0_CustomAttributeNamedArgument_t1691_0_0_0_Types[] = { &CustomAttributeNamedArgument_t1691_0_0_0, &CustomAttributeNamedArgument_t1691_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t1691_0_0_0_CustomAttributeNamedArgument_t1691_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t1691_0_0_0_CustomAttributeNamedArgument_t1691_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m26752_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m26752_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m26752_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m26752_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m26752_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m26753_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m26753_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m26753_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m26753_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m26753_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m26755_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m26755_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m26755_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m26755_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m26755_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m26756_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m26756_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m26756_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m26756_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m26756_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m26757_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m26757_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m26757_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m26757_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m26757_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t3400_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t3400_gp_0_0_0_0_Types[] = { &TweenRunner_1_t3400_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t3400_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t3400_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m26784_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m26784_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m26784_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m26784_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m26784_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t3404_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t3404_gp_0_0_0_0_Types[] = { &IndexedSet_1_t3404_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t3404_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t3404_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t3404_gp_0_0_0_0_Int32_t392_0_0_0_Types[] = { &IndexedSet_1_t3404_gp_0_0_0_0, &Int32_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t3404_gp_0_0_0_0_Int32_t392_0_0_0 = { 2, GenInst_IndexedSet_1_t3404_gp_0_0_0_0_Int32_t392_0_0_0_Types };
extern const Il2CppType ListPool_1_t3405_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t3405_gp_0_0_0_0_Types[] = { &ListPool_1_t3405_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t3405_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t3405_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t4614_0_0_0;
static const Il2CppType* GenInst_List_1_t4614_0_0_0_Types[] = { &List_1_t4614_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t4614_0_0_0 = { 1, GenInst_List_1_t4614_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t3406_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t3406_gp_0_0_0_0_Types[] = { &ObjectPool_1_t3406_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t3406_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t3406_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m26851_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m26851_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m26851_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m26851_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m26851_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m26855_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m26855_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m26855_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m26855_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m26855_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m26856_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m26856_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m26856_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m26856_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m26856_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m26857_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m26857_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m26857_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m26857_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m26857_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m26858_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m26858_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m26858_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m26858_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m26858_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m26860_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m26860_gp_0_0_0_0_Types[] = { &Component_GetComponents_m26860_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m26860_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m26860_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m26863_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m26863_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m26863_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m26863_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m26863_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m26865_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m26865_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m26865_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m26865_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m26865_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m26866_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m26866_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m26866_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m26866_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m26866_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m26867_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m26867_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m26867_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m26867_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m26867_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t3414_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t3414_gp_0_0_0_0_Types[] = { &InvokableCall_1_t3414_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t3414_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t3414_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t3415_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t3415_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t3415_gp_0_0_0_0_InvokableCall_2_t3415_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3415_gp_0_0_0_0, &InvokableCall_2_t3415_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3415_gp_0_0_0_0_InvokableCall_2_t3415_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t3415_gp_0_0_0_0_InvokableCall_2_t3415_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3415_gp_0_0_0_0_Types[] = { &InvokableCall_2_t3415_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3415_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t3415_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3415_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3415_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3415_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t3415_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3416_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3416_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3416_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3416_gp_0_0_0_0_InvokableCall_3_t3416_gp_1_0_0_0_InvokableCall_3_t3416_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3416_gp_0_0_0_0, &InvokableCall_3_t3416_gp_1_0_0_0, &InvokableCall_3_t3416_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3416_gp_0_0_0_0_InvokableCall_3_t3416_gp_1_0_0_0_InvokableCall_3_t3416_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3416_gp_0_0_0_0_InvokableCall_3_t3416_gp_1_0_0_0_InvokableCall_3_t3416_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3416_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3416_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3416_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3416_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3416_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3416_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3416_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3416_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3416_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3416_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3416_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3416_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t3417_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t3417_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t3417_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t3417_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t3417_gp_0_0_0_0_InvokableCall_4_t3417_gp_1_0_0_0_InvokableCall_4_t3417_gp_2_0_0_0_InvokableCall_4_t3417_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3417_gp_0_0_0_0, &InvokableCall_4_t3417_gp_1_0_0_0, &InvokableCall_4_t3417_gp_2_0_0_0, &InvokableCall_4_t3417_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3417_gp_0_0_0_0_InvokableCall_4_t3417_gp_1_0_0_0_InvokableCall_4_t3417_gp_2_0_0_0_InvokableCall_4_t3417_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t3417_gp_0_0_0_0_InvokableCall_4_t3417_gp_1_0_0_0_InvokableCall_4_t3417_gp_2_0_0_0_InvokableCall_4_t3417_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3417_gp_0_0_0_0_Types[] = { &InvokableCall_4_t3417_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3417_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t3417_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3417_gp_1_0_0_0_Types[] = { &InvokableCall_4_t3417_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3417_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t3417_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3417_gp_2_0_0_0_Types[] = { &InvokableCall_4_t3417_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3417_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t3417_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3417_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3417_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3417_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t3417_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t724_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t724_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t724_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t724_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t724_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t3418_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t3418_gp_0_0_0_0_Types[] = { &UnityEvent_1_t3418_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t3418_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t3418_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t3419_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t3419_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t3419_gp_0_0_0_0_UnityEvent_2_t3419_gp_1_0_0_0_Types[] = { &UnityEvent_2_t3419_gp_0_0_0_0, &UnityEvent_2_t3419_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t3419_gp_0_0_0_0_UnityEvent_2_t3419_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t3419_gp_0_0_0_0_UnityEvent_2_t3419_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t3420_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t3420_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t3420_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t3420_gp_0_0_0_0_UnityEvent_3_t3420_gp_1_0_0_0_UnityEvent_3_t3420_gp_2_0_0_0_Types[] = { &UnityEvent_3_t3420_gp_0_0_0_0, &UnityEvent_3_t3420_gp_1_0_0_0, &UnityEvent_3_t3420_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t3420_gp_0_0_0_0_UnityEvent_3_t3420_gp_1_0_0_0_UnityEvent_3_t3420_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t3420_gp_0_0_0_0_UnityEvent_3_t3420_gp_1_0_0_0_UnityEvent_3_t3420_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t3421_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t3421_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t3421_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t3421_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t3421_gp_0_0_0_0_UnityEvent_4_t3421_gp_1_0_0_0_UnityEvent_4_t3421_gp_2_0_0_0_UnityEvent_4_t3421_gp_3_0_0_0_Types[] = { &UnityEvent_4_t3421_gp_0_0_0_0, &UnityEvent_4_t3421_gp_1_0_0_0, &UnityEvent_4_t3421_gp_2_0_0_0, &UnityEvent_4_t3421_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t3421_gp_0_0_0_0_UnityEvent_4_t3421_gp_1_0_0_0_UnityEvent_4_t3421_gp_2_0_0_0_UnityEvent_4_t3421_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t3421_gp_0_0_0_0_UnityEvent_4_t3421_gp_1_0_0_0_UnityEvent_4_t3421_gp_2_0_0_0_UnityEvent_4_t3421_gp_3_0_0_0_Types };
extern const Il2CppType SmartTerrainBuilderImpl_CreateReconstruction_m27239_gp_0_0_0_0;
static const Il2CppType* GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m27239_gp_0_0_0_0_Types[] = { &SmartTerrainBuilderImpl_CreateReconstruction_m27239_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m27239_gp_0_0_0_0 = { 1, GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m27239_gp_0_0_0_0_Types };
extern const Il2CppType HashSet_1_t3430_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t3430_gp_0_0_0_0_Types[] = { &HashSet_1_t3430_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t3430_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t3430_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3432_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3432_gp_0_0_0_0_Types[] = { &Enumerator_t3432_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3432_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3432_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3433_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3433_gp_0_0_0_0_Types[] = { &PrimeHelper_t3433_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3433_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3433_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m27449_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m27449_gp_0_0_0_0_Types[] = { &Enumerable_Any_m27449_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m27449_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m27449_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Cast_m27450_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Cast_m27450_gp_0_0_0_0_Types[] = { &Enumerable_Cast_m27450_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Cast_m27450_gp_0_0_0_0 = { 1, GenInst_Enumerable_Cast_m27450_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateCastIterator_m27451_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateCastIterator_m27451_gp_0_0_0_0_Types[] = { &Enumerable_CreateCastIterator_m27451_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateCastIterator_m27451_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateCastIterator_m27451_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m27452_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m27452_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m27452_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m27452_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m27452_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m27453_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m27453_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m27453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m27453_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m27453_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m27454_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m27454_gp_0_0_0_0_Types[] = { &Enumerable_First_m27454_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m27454_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m27454_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m27455_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m27455_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m27455_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m27455_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m27455_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m27456_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m27456_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m27456_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m27456_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m27456_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m27457_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m27457_gp_0_0_0_0_Types[] = { &Enumerable_Where_m27457_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m27457_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m27457_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m27457_gp_0_0_0_0_Boolean_t393_0_0_0_Types[] = { &Enumerable_Where_m27457_gp_0_0_0_0, &Boolean_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m27457_gp_0_0_0_0_Boolean_t393_0_0_0 = { 2, GenInst_Enumerable_Where_m27457_gp_0_0_0_0_Boolean_t393_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m27458_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m27458_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m27458_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m27458_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m27458_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m27458_gp_0_0_0_0_Boolean_t393_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m27458_gp_0_0_0_0, &Boolean_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m27458_gp_0_0_0_0_Boolean_t393_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m27458_gp_0_0_0_0_Boolean_t393_0_0_0_Types };
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t3434_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t3434_gp_0_0_0_0_Types[] = { &U3CCreateCastIteratorU3Ec__Iterator0_1_t3434_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t3434_gp_0_0_0_0 = { 1, GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t3434_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3435_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3435_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3435_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3435_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3435_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3435_gp_0_0_0_0_Boolean_t393_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3435_gp_0_0_0_0, &Boolean_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3435_gp_0_0_0_0_Boolean_t393_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3435_gp_0_0_0_0_Boolean_t393_0_0_0_Types };
extern const Il2CppType LinkedList_1_t3437_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t3437_gp_0_0_0_0_Types[] = { &LinkedList_1_t3437_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t3437_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t3437_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3438_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3438_gp_0_0_0_0_Types[] = { &Enumerator_t3438_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3438_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3438_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t3439_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t3439_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t3439_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3439_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t3439_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t3440_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t3440_gp_0_0_0_0_Types[] = { &Stack_1_t3440_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t3440_gp_0_0_0_0 = { 1, GenInst_Stack_1_t3440_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3441_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3441_gp_0_0_0_0_Types[] = { &Enumerator_t3441_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3441_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3441_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3447_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3447_gp_0_0_0_0_Types[] = { &IEnumerable_1_t3447_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3447_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t3447_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m27624_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m27624_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m27624_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m27624_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m27624_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m27636_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m27636_gp_0_0_0_0_Array_Sort_m27636_gp_0_0_0_0_Types[] = { &Array_Sort_m27636_gp_0_0_0_0, &Array_Sort_m27636_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27636_gp_0_0_0_0_Array_Sort_m27636_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m27636_gp_0_0_0_0_Array_Sort_m27636_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m27637_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m27637_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m27637_gp_0_0_0_0_Array_Sort_m27637_gp_1_0_0_0_Types[] = { &Array_Sort_m27637_gp_0_0_0_0, &Array_Sort_m27637_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27637_gp_0_0_0_0_Array_Sort_m27637_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m27637_gp_0_0_0_0_Array_Sort_m27637_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m27638_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m27638_gp_0_0_0_0_Types[] = { &Array_Sort_m27638_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27638_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m27638_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m27638_gp_0_0_0_0_Array_Sort_m27638_gp_0_0_0_0_Types[] = { &Array_Sort_m27638_gp_0_0_0_0, &Array_Sort_m27638_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27638_gp_0_0_0_0_Array_Sort_m27638_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m27638_gp_0_0_0_0_Array_Sort_m27638_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m27639_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m27639_gp_0_0_0_0_Types[] = { &Array_Sort_m27639_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27639_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m27639_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m27639_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m27639_gp_0_0_0_0_Array_Sort_m27639_gp_1_0_0_0_Types[] = { &Array_Sort_m27639_gp_0_0_0_0, &Array_Sort_m27639_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27639_gp_0_0_0_0_Array_Sort_m27639_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m27639_gp_0_0_0_0_Array_Sort_m27639_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m27640_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m27640_gp_0_0_0_0_Array_Sort_m27640_gp_0_0_0_0_Types[] = { &Array_Sort_m27640_gp_0_0_0_0, &Array_Sort_m27640_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27640_gp_0_0_0_0_Array_Sort_m27640_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m27640_gp_0_0_0_0_Array_Sort_m27640_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m27641_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m27641_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m27641_gp_0_0_0_0_Array_Sort_m27641_gp_1_0_0_0_Types[] = { &Array_Sort_m27641_gp_0_0_0_0, &Array_Sort_m27641_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27641_gp_0_0_0_0_Array_Sort_m27641_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m27641_gp_0_0_0_0_Array_Sort_m27641_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m27642_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m27642_gp_0_0_0_0_Types[] = { &Array_Sort_m27642_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27642_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m27642_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m27642_gp_0_0_0_0_Array_Sort_m27642_gp_0_0_0_0_Types[] = { &Array_Sort_m27642_gp_0_0_0_0, &Array_Sort_m27642_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27642_gp_0_0_0_0_Array_Sort_m27642_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m27642_gp_0_0_0_0_Array_Sort_m27642_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m27643_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m27643_gp_0_0_0_0_Types[] = { &Array_Sort_m27643_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27643_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m27643_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m27643_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m27643_gp_1_0_0_0_Types[] = { &Array_Sort_m27643_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27643_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m27643_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m27643_gp_0_0_0_0_Array_Sort_m27643_gp_1_0_0_0_Types[] = { &Array_Sort_m27643_gp_0_0_0_0, &Array_Sort_m27643_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27643_gp_0_0_0_0_Array_Sort_m27643_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m27643_gp_0_0_0_0_Array_Sort_m27643_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m27644_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m27644_gp_0_0_0_0_Types[] = { &Array_Sort_m27644_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27644_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m27644_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m27645_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m27645_gp_0_0_0_0_Types[] = { &Array_Sort_m27645_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m27645_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m27645_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m27646_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m27646_gp_0_0_0_0_Types[] = { &Array_qsort_m27646_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m27646_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m27646_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m27646_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m27646_gp_0_0_0_0_Array_qsort_m27646_gp_1_0_0_0_Types[] = { &Array_qsort_m27646_gp_0_0_0_0, &Array_qsort_m27646_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m27646_gp_0_0_0_0_Array_qsort_m27646_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m27646_gp_0_0_0_0_Array_qsort_m27646_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m27647_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m27647_gp_0_0_0_0_Types[] = { &Array_compare_m27647_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m27647_gp_0_0_0_0 = { 1, GenInst_Array_compare_m27647_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m27648_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m27648_gp_0_0_0_0_Types[] = { &Array_qsort_m27648_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m27648_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m27648_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m27651_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m27651_gp_0_0_0_0_Types[] = { &Array_Resize_m27651_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m27651_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m27651_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m27653_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m27653_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m27653_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m27653_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m27653_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m27654_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m27654_gp_0_0_0_0_Types[] = { &Array_ForEach_m27654_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m27654_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m27654_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m27655_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m27655_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m27655_gp_0_0_0_0_Array_ConvertAll_m27655_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m27655_gp_0_0_0_0, &Array_ConvertAll_m27655_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m27655_gp_0_0_0_0_Array_ConvertAll_m27655_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m27655_gp_0_0_0_0_Array_ConvertAll_m27655_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m27656_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m27656_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m27656_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m27656_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m27656_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m27657_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m27657_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m27657_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m27657_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m27657_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m27658_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m27658_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m27658_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m27658_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m27658_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m27659_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m27659_gp_0_0_0_0_Types[] = { &Array_FindIndex_m27659_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m27659_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m27659_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m27660_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m27660_gp_0_0_0_0_Types[] = { &Array_FindIndex_m27660_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m27660_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m27660_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m27661_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m27661_gp_0_0_0_0_Types[] = { &Array_FindIndex_m27661_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m27661_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m27661_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m27662_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m27662_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m27662_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m27662_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m27662_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m27663_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m27663_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m27663_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m27663_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m27663_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m27664_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m27664_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m27664_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m27664_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m27664_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m27665_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m27665_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m27665_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m27665_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m27665_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m27666_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m27666_gp_0_0_0_0_Types[] = { &Array_IndexOf_m27666_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m27666_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m27666_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m27667_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m27667_gp_0_0_0_0_Types[] = { &Array_IndexOf_m27667_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m27667_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m27667_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m27668_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m27668_gp_0_0_0_0_Types[] = { &Array_IndexOf_m27668_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m27668_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m27668_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m27669_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m27669_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m27669_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m27669_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m27669_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m27670_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m27670_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m27670_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m27670_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m27670_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m27671_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m27671_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m27671_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m27671_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m27671_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m27672_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m27672_gp_0_0_0_0_Types[] = { &Array_FindAll_m27672_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m27672_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m27672_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m27673_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m27673_gp_0_0_0_0_Types[] = { &Array_Exists_m27673_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m27673_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m27673_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m27674_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m27674_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m27674_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m27674_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m27674_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m27675_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m27675_gp_0_0_0_0_Types[] = { &Array_Find_m27675_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m27675_gp_0_0_0_0 = { 1, GenInst_Array_Find_m27675_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m27676_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m27676_gp_0_0_0_0_Types[] = { &Array_FindLast_m27676_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m27676_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m27676_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t3448_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t3448_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t3448_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3448_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3448_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t3449_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t3449_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t3449_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3449_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3449_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t3450_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3450_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t3450_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3450_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3450_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t3451_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t3451_gp_0_0_0_0_Types[] = { &IList_1_t3451_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3451_gp_0_0_0_0 = { 1, GenInst_IList_1_t3451_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t3452_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3452_gp_0_0_0_0_Types[] = { &ICollection_1_t3452_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3452_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t3452_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t2128_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t2128_gp_0_0_0_0_Types[] = { &Nullable_1_t2128_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t2128_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t2128_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t3459_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t3459_gp_0_0_0_0_Types[] = { &Comparer_1_t3459_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t3459_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t3459_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3460_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3460_gp_0_0_0_0_Types[] = { &DefaultComparer_t3460_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3460_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3460_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t3397_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t3397_gp_0_0_0_0_Types[] = { &GenericComparer_1_t3397_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t3397_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t3397_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3461_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3461_gp_0_0_0_0_Types[] = { &Dictionary_2_t3461_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3461_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t3461_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3461_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_Types[] = { &Dictionary_2_t3461_gp_0_0_0_0, &Dictionary_2_t3461_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4752_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4752_0_0_0_Types[] = { &KeyValuePair_2_t4752_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4752_0_0_0 = { 1, GenInst_KeyValuePair_2_t4752_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m27825_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m27825_gp_0_0_0_0_Types[] = { &Dictionary_2_t3461_gp_0_0_0_0, &Dictionary_2_t3461_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m27825_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m27825_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m27825_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m27830_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m27830_gp_0_0_0_0_Types[] = { &Dictionary_2_t3461_gp_0_0_0_0, &Dictionary_2_t3461_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m27830_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m27830_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m27830_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m27830_gp_0_0_0_0_Object_t_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m27830_gp_0_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m27830_gp_0_0_0_0_Object_t_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m27830_gp_0_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_DictionaryEntry_t1423_0_0_0_Types[] = { &Dictionary_2_t3461_gp_0_0_0_0, &Dictionary_2_t3461_gp_1_0_0_0, &DictionaryEntry_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_DictionaryEntry_t1423_0_0_0 = { 3, GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_DictionaryEntry_t1423_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3462_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3462_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3462_gp_0_0_0_0_ShimEnumerator_t3462_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3462_gp_0_0_0_0, &ShimEnumerator_t3462_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3462_gp_0_0_0_0_ShimEnumerator_t3462_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3462_gp_0_0_0_0_ShimEnumerator_t3462_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3463_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3463_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3463_gp_0_0_0_0_Enumerator_t3463_gp_1_0_0_0_Types[] = { &Enumerator_t3463_gp_0_0_0_0, &Enumerator_t3463_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3463_gp_0_0_0_0_Enumerator_t3463_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3463_gp_0_0_0_0_Enumerator_t3463_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4766_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4766_0_0_0_Types[] = { &KeyValuePair_2_t4766_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4766_0_0_0 = { 1, GenInst_KeyValuePair_2_t4766_0_0_0_Types };
extern const Il2CppType KeyCollection_t3464_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t3464_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t3464_gp_0_0_0_0_KeyCollection_t3464_gp_1_0_0_0_Types[] = { &KeyCollection_t3464_gp_0_0_0_0, &KeyCollection_t3464_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3464_gp_0_0_0_0_KeyCollection_t3464_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t3464_gp_0_0_0_0_KeyCollection_t3464_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t3464_gp_0_0_0_0_Types[] = { &KeyCollection_t3464_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3464_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t3464_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3465_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3465_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3465_gp_0_0_0_0_Enumerator_t3465_gp_1_0_0_0_Types[] = { &Enumerator_t3465_gp_0_0_0_0, &Enumerator_t3465_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3465_gp_0_0_0_0_Enumerator_t3465_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3465_gp_0_0_0_0_Enumerator_t3465_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3465_gp_0_0_0_0_Types[] = { &Enumerator_t3465_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3465_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3465_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t3464_gp_0_0_0_0_KeyCollection_t3464_gp_1_0_0_0_KeyCollection_t3464_gp_0_0_0_0_Types[] = { &KeyCollection_t3464_gp_0_0_0_0, &KeyCollection_t3464_gp_1_0_0_0, &KeyCollection_t3464_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3464_gp_0_0_0_0_KeyCollection_t3464_gp_1_0_0_0_KeyCollection_t3464_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t3464_gp_0_0_0_0_KeyCollection_t3464_gp_1_0_0_0_KeyCollection_t3464_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t3464_gp_0_0_0_0_KeyCollection_t3464_gp_0_0_0_0_Types[] = { &KeyCollection_t3464_gp_0_0_0_0, &KeyCollection_t3464_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3464_gp_0_0_0_0_KeyCollection_t3464_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t3464_gp_0_0_0_0_KeyCollection_t3464_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t3466_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t3466_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t3466_gp_0_0_0_0_ValueCollection_t3466_gp_1_0_0_0_Types[] = { &ValueCollection_t3466_gp_0_0_0_0, &ValueCollection_t3466_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3466_gp_0_0_0_0_ValueCollection_t3466_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3466_gp_0_0_0_0_ValueCollection_t3466_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3466_gp_1_0_0_0_Types[] = { &ValueCollection_t3466_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3466_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t3466_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3467_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3467_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3467_gp_0_0_0_0_Enumerator_t3467_gp_1_0_0_0_Types[] = { &Enumerator_t3467_gp_0_0_0_0, &Enumerator_t3467_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3467_gp_0_0_0_0_Enumerator_t3467_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3467_gp_0_0_0_0_Enumerator_t3467_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3467_gp_1_0_0_0_Types[] = { &Enumerator_t3467_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3467_gp_1_0_0_0 = { 1, GenInst_Enumerator_t3467_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3466_gp_0_0_0_0_ValueCollection_t3466_gp_1_0_0_0_ValueCollection_t3466_gp_1_0_0_0_Types[] = { &ValueCollection_t3466_gp_0_0_0_0, &ValueCollection_t3466_gp_1_0_0_0, &ValueCollection_t3466_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3466_gp_0_0_0_0_ValueCollection_t3466_gp_1_0_0_0_ValueCollection_t3466_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t3466_gp_0_0_0_0_ValueCollection_t3466_gp_1_0_0_0_ValueCollection_t3466_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3466_gp_1_0_0_0_ValueCollection_t3466_gp_1_0_0_0_Types[] = { &ValueCollection_t3466_gp_1_0_0_0, &ValueCollection_t3466_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3466_gp_1_0_0_0_ValueCollection_t3466_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3466_gp_1_0_0_0_ValueCollection_t3466_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_KeyValuePair_2_t4752_0_0_0_Types[] = { &Dictionary_2_t3461_gp_0_0_0_0, &Dictionary_2_t3461_gp_1_0_0_0, &KeyValuePair_2_t4752_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_KeyValuePair_2_t4752_0_0_0 = { 3, GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_KeyValuePair_2_t4752_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4752_0_0_0_KeyValuePair_2_t4752_0_0_0_Types[] = { &KeyValuePair_2_t4752_0_0_0, &KeyValuePair_2_t4752_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4752_0_0_0_KeyValuePair_2_t4752_0_0_0 = { 2, GenInst_KeyValuePair_2_t4752_0_0_0_KeyValuePair_2_t4752_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3461_gp_1_0_0_0_Types[] = { &Dictionary_2_t3461_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3461_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t3461_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t3469_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t3469_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t3469_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t3469_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t3469_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3470_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3470_gp_0_0_0_0_Types[] = { &DefaultComparer_t3470_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3470_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3470_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t3396_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t3396_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t3396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t3396_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t3396_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4802_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4802_0_0_0_Types[] = { &KeyValuePair_2_t4802_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4802_0_0_0 = { 1, GenInst_KeyValuePair_2_t4802_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3472_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t3472_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3472_gp_0_0_0_0_IDictionary_2_t3472_gp_1_0_0_0_Types[] = { &IDictionary_2_t3472_gp_0_0_0_0, &IDictionary_2_t3472_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3472_gp_0_0_0_0_IDictionary_2_t3472_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3472_gp_0_0_0_0_IDictionary_2_t3472_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3474_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t3474_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3474_gp_0_0_0_0_KeyValuePair_2_t3474_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t3474_gp_0_0_0_0, &KeyValuePair_2_t3474_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3474_gp_0_0_0_0_KeyValuePair_2_t3474_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t3474_gp_0_0_0_0_KeyValuePair_2_t3474_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t3475_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t3475_gp_0_0_0_0_Types[] = { &List_1_t3475_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3475_gp_0_0_0_0 = { 1, GenInst_List_1_t3475_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3476_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3476_gp_0_0_0_0_Types[] = { &Enumerator_t3476_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3476_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3476_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t3477_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t3477_gp_0_0_0_0_Types[] = { &Collection_1_t3477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t3477_gp_0_0_0_0 = { 1, GenInst_Collection_1_t3477_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t3478_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t3478_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t3478_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t3478_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t3478_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m28108_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m28108_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m28108_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m28108_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m28108_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m28108_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m28108_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m28108_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m28108_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m28108_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m28109_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m28109_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m28109_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m28109_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m28109_gp_0_0_0_0_Types };
extern const Il2CppGenericInst* g_Il2CppGenericInstTable[714] = 
{
	&GenInst_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Material_t8_0_0_0,
	&GenInst_RowController_t10_0_0_0,
	&GenInst_Renderer_t85_0_0_0,
	&GenInst_DockController_t7_0_0_0,
	&GenInst_BoardController_t1_0_0_0,
	&GenInst_InitError_t905_0_0_0,
	&GenInst_ReconstructionBehaviour_t28_0_0_0,
	&GenInst_Prop_t81_0_0_0,
	&GenInst_Surface_t82_0_0_0,
	&GenInst_TrackableBehaviour_t32_0_0_0,
	&GenInst_Collider_t14_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst_MaskOutBehaviour_t46_0_0_0,
	&GenInst_VirtualButtonBehaviour_t70_0_0_0,
	&GenInst_TurnOffBehaviour_t61_0_0_0,
	&GenInst_ImageTargetBehaviour_t36_0_0_0,
	&GenInst_MarkerBehaviour_t44_0_0_0,
	&GenInst_MultiTargetBehaviour_t48_0_0_0,
	&GenInst_CylinderTargetBehaviour_t22_0_0_0,
	&GenInst_WordBehaviour_t79_0_0_0,
	&GenInst_TextRecoBehaviour_t59_0_0_0,
	&GenInst_ObjectTargetBehaviour_t50_0_0_0,
	&GenInst_MeshRenderer_t113_0_0_0,
	&GenInst_MeshFilter_t114_0_0_0,
	&GenInst_ComponentFactoryStarterBehaviour_t39_0_0_0,
	&GenInst_VuforiaBehaviour_t72_0_0_0,
	&GenInst_Camera_t91_0_0_0,
	&GenInst_WireframeBehaviour_t76_0_0_0,
	&GenInst_BaseInputModule_t136_0_0_0,
	&GenInst_RaycastResult_t169_0_0_0,
	&GenInst_IDeselectHandler_t370_0_0_0,
	&GenInst_ISelectHandler_t369_0_0_0,
	&GenInst_BaseEventData_t137_0_0_0,
	&GenInst_Entry_t141_0_0_0,
	&GenInst_IPointerEnterHandler_t357_0_0_0,
	&GenInst_IPointerExitHandler_t358_0_0_0,
	&GenInst_IPointerDownHandler_t359_0_0_0,
	&GenInst_IPointerUpHandler_t360_0_0_0,
	&GenInst_IPointerClickHandler_t361_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t362_0_0_0,
	&GenInst_IBeginDragHandler_t363_0_0_0,
	&GenInst_IDragHandler_t364_0_0_0,
	&GenInst_IEndDragHandler_t365_0_0_0,
	&GenInst_IDropHandler_t366_0_0_0,
	&GenInst_IScrollHandler_t367_0_0_0,
	&GenInst_IUpdateSelectedHandler_t368_0_0_0,
	&GenInst_IMoveHandler_t371_0_0_0,
	&GenInst_ISubmitHandler_t372_0_0_0,
	&GenInst_ICancelHandler_t373_0_0_0,
	&GenInst_List_1_t356_0_0_0,
	&GenInst_IEventSystemHandler_t2215_0_0_0,
	&GenInst_Transform_t84_0_0_0,
	&GenInst_PointerEventData_t175_0_0_0,
	&GenInst_AxisEventData_t172_0_0_0,
	&GenInst_BaseRaycaster_t170_0_0_0,
	&GenInst_GameObject_t9_0_0_0,
	&GenInst_EventSystem_t133_0_0_0,
	&GenInst_ButtonState_t178_0_0_0,
	&GenInst_Int32_t392_0_0_0_PointerEventData_t175_0_0_0,
	&GenInst_SpriteRenderer_t400_0_0_0,
	&GenInst_RaycastHit_t89_0_0_0,
	&GenInst_Color_t77_0_0_0,
	&GenInst_Single_t117_0_0_0,
	&GenInst_ICanvasElement_t379_0_0_0,
	&GenInst_RectTransform_t212_0_0_0,
	&GenInst_Image_t15_0_0_0,
	&GenInst_Button_t201_0_0_0,
	&GenInst_Text_t16_0_0_0,
	&GenInst_RawImage_t281_0_0_0,
	&GenInst_Slider_t306_0_0_0,
	&GenInst_Scrollbar_t290_0_0_0,
	&GenInst_Toggle_t213_0_0_0,
	&GenInst_InputField_t263_0_0_0,
	&GenInst_ScrollRect_t296_0_0_0,
	&GenInst_Mask_t273_0_0_0,
	&GenInst_Dropdown_t220_0_0_0,
	&GenInst_OptionData_t214_0_0_0,
	&GenInst_Int32_t392_0_0_0,
	&GenInst_DropdownItem_t211_0_0_0,
	&GenInst_FloatTween_t196_0_0_0,
	&GenInst_Canvas_t230_0_0_0,
	&GenInst_GraphicRaycaster_t236_0_0_0,
	&GenInst_CanvasGroup_t408_0_0_0,
	&GenInst_Boolean_t393_0_0_0,
	&GenInst_Font_t225_0_0_0_List_1_t413_0_0_0,
	&GenInst_Font_t225_0_0_0,
	&GenInst_ColorTween_t193_0_0_0,
	&GenInst_CanvasRenderer_t229_0_0_0,
	&GenInst_Component_t130_0_0_0,
	&GenInst_Graphic_t228_0_0_0,
	&GenInst_Canvas_t230_0_0_0_IndexedSet_1_t422_0_0_0,
	&GenInst_Sprite_t209_0_0_0,
	&GenInst_Type_t241_0_0_0,
	&GenInst_FillMethod_t242_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_SubmitEvent_t254_0_0_0,
	&GenInst_OnChangeEvent_t256_0_0_0,
	&GenInst_OnValidateInput_t258_0_0_0,
	&GenInst_ContentType_t250_0_0_0,
	&GenInst_LineType_t253_0_0_0,
	&GenInst_InputType_t251_0_0_0,
	&GenInst_TouchScreenKeyboardType_t425_0_0_0,
	&GenInst_CharacterValidation_t252_0_0_0,
	&GenInst_Char_t424_0_0_0,
	&GenInst_UILineInfo_t428_0_0_0,
	&GenInst_UICharInfo_t430_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t392_0_0_0,
	&GenInst_LayoutElement_t334_0_0_0,
	&GenInst_IClippable_t384_0_0_0,
	&GenInst_RectMask2D_t276_0_0_0,
	&GenInst_Direction_t286_0_0_0,
	&GenInst_Vector2_t171_0_0_0,
	&GenInst_Selectable_t202_0_0_0,
	&GenInst_Navigation_t280_0_0_0,
	&GenInst_Transition_t297_0_0_0,
	&GenInst_ColorBlock_t207_0_0_0,
	&GenInst_SpriteState_t299_0_0_0,
	&GenInst_AnimationTriggers_t197_0_0_0,
	&GenInst_Animator_t5_0_0_0,
	&GenInst_Direction_t303_0_0_0,
	&GenInst_MatEntry_t307_0_0_0,
	&GenInst_UIVertex_t272_0_0_0,
	&GenInst_Toggle_t213_0_0_0_Boolean_t393_0_0_0,
	&GenInst_IClipper_t387_0_0_0,
	&GenInst_AspectMode_t319_0_0_0,
	&GenInst_FitMode_t325_0_0_0,
	&GenInst_Corner_t327_0_0_0,
	&GenInst_Axis_t328_0_0_0,
	&GenInst_Constraint_t329_0_0_0,
	&GenInst_RectOffset_t335_0_0_0,
	&GenInst_TextAnchor_t444_0_0_0,
	&GenInst_LayoutRebuilder_t337_0_0_0,
	&GenInst_ILayoutElement_t389_0_0_0_Single_t117_0_0_0,
	&GenInst_Vector3_t12_0_0_0,
	&GenInst_Color32_t382_0_0_0,
	&GenInst_Vector4_t350_0_0_0,
	&GenInst_GcLeaderboard_t483_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t692_0_0_0,
	&GenInst_IAchievementU5BU5D_t694_0_0_0,
	&GenInst_IScoreU5BU5D_t630_0_0_0,
	&GenInst_IUserProfileU5BU5D_t626_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t393_0_0_0,
	&GenInst_Rigidbody2D_t540_0_0_0,
	&GenInst_Int32_t392_0_0_0_LayoutCache_t580_0_0_0,
	&GenInst_GUILayoutEntry_t585_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t584_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_GUILayer_t489_0_0_0,
	&GenInst_PersistentCall_t658_0_0_0,
	&GenInst_BaseInvokableCall_t655_0_0_0,
	&GenInst_Camera_t91_0_0_0_VideoBackgroundAbstractBehaviour_t67_0_0_0,
	&GenInst_BackgroundPlaneAbstractBehaviour_t19_0_0_0,
	&GenInst_VideoBackgroundAbstractBehaviour_t67_0_0_0,
	&GenInst_ITrackableEventHandler_t922_0_0_0,
	&GenInst_SmartTerrainTracker_t842_0_0_0,
	&GenInst_ReconstructionAbstractBehaviour_t53_0_0_0,
	&GenInst_ICloudRecoEventHandler_t923_0_0_0,
	&GenInst_TargetSearchResult_t884_0_0_0,
	&GenInst_ObjectTracker_t744_0_0_0,
	&GenInst_HideExcessAreaAbstractBehaviour_t35_0_0_0,
	&GenInst_ReconstructionFromTarget_t755_0_0_0,
	&GenInst_PIXEL_FORMAT_t783_0_0_0_Image_t784_0_0_0,
	&GenInst_PIXEL_FORMAT_t783_0_0_0,
	&GenInst_Int32_t392_0_0_0_Trackable_t737_0_0_0,
	&GenInst_Trackable_t737_0_0_0,
	&GenInst_Int32_t392_0_0_0_VirtualButton_t895_0_0_0,
	&GenInst_DataSet_t764_0_0_0,
	&GenInst_DataSetImpl_t751_0_0_0,
	&GenInst_Int32_t392_0_0_0_Marker_t903_0_0_0,
	&GenInst_Marker_t903_0_0_0,
	&GenInst_TrackableResultData_t810_0_0_0,
	&GenInst_SmartTerrainTrackable_t762_0_0_0,
	&GenInst_SmartTerrainTrackableBehaviour_t761_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t1430_0_0_0,
	&GenInst_WordAbstractBehaviour_t80_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t858_0_0_0,
	&GenInst_Int32_t392_0_0_0_WordAbstractBehaviour_t80_0_0_0,
	&GenInst_Int32_t392_0_0_0_WordResult_t860_0_0_0,
	&GenInst_WordData_t815_0_0_0,
	&GenInst_WordResultData_t814_0_0_0,
	&GenInst_Word_t862_0_0_0,
	&GenInst_WordResult_t860_0_0_0,
	&GenInst_ILoadLevelEventHandler_t936_0_0_0,
	&GenInst_SmartTerrainInitializationInfo_t760_0_0_0,
	&GenInst_Int32_t392_0_0_0_Prop_t81_0_0_0,
	&GenInst_Int32_t392_0_0_0_Surface_t82_0_0_0,
	&GenInst_ISmartTerrainEventHandler_t939_0_0_0,
	&GenInst_Int32_t392_0_0_0_PropAbstractBehaviour_t52_0_0_0,
	&GenInst_Int32_t392_0_0_0_SurfaceAbstractBehaviour_t58_0_0_0,
	&GenInst_PropAbstractBehaviour_t52_0_0_0,
	&GenInst_SurfaceAbstractBehaviour_t58_0_0_0,
	&GenInst_Int32_t392_0_0_0_TrackableBehaviour_t32_0_0_0,
	&GenInst_MarkerTracker_t797_0_0_0,
	&GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0,
	&GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0,
	&GenInst_VirtualButtonAbstractBehaviour_t71_0_0_0,
	&GenInst_Int32_t392_0_0_0_ImageTarget_t897_0_0_0,
	&GenInst_ImageTargetAbstractBehaviour_t37_0_0_0,
	&GenInst_Int32_t392_0_0_0_VirtualButtonAbstractBehaviour_t71_0_0_0,
	&GenInst_VirtualButton_t895_0_0_0,
	&GenInst_ITrackerEventHandler_t946_0_0_0,
	&GenInst_TextTracker_t844_0_0_0,
	&GenInst_WebCamAbstractBehaviour_t75_0_0_0,
	&GenInst_IVideoBackgroundEventHandler_t947_0_0_0,
	&GenInst_ITextRecoEventHandler_t948_0_0_0,
	&GenInst_IUserDefinedTargetEventHandler_t949_0_0_0,
	&GenInst_VuforiaAbstractBehaviour_t73_0_0_0,
	&GenInst_IVirtualButtonEventHandler_t950_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1692_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t1691_0_0_0,
	&GenInst_StrongName_t1939_0_0_0,
	&GenInst_DateTime_t577_0_0_0,
	&GenInst_DateTimeOffset_t2014_0_0_0,
	&GenInst_TimeSpan_t975_0_0_0,
	&GenInst_Guid_t2036_0_0_0,
	&GenInst_CustomAttributeData_t1688_0_0_0,
	&GenInst_MonoBehaviour_t2_0_0_0,
	&GenInst_Behaviour_t450_0_0_0,
	&GenInst_Object_t86_0_0_0,
	&GenInst_IReflect_t3455_0_0_0,
	&GenInst__Type_t3453_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t2122_0_0_0,
	&GenInst__MemberInfo_t3454_0_0_0,
	&GenInst_IFormattable_t2124_0_0_0,
	&GenInst_IConvertible_t2127_0_0_0,
	&GenInst_IComparable_t2126_0_0_0,
	&GenInst_IComparable_1_t3690_0_0_0,
	&GenInst_IEquatable_1_t3695_0_0_0,
	&GenInst_ValueType_t1437_0_0_0,
	&GenInst_Double_t710_0_0_0,
	&GenInst_IComparable_1_t3707_0_0_0,
	&GenInst_IEquatable_1_t3712_0_0_0,
	&GenInst_IComparable_1_t3720_0_0_0,
	&GenInst_IEquatable_1_t3725_0_0_0,
	&GenInst_IEnumerable_t988_0_0_0,
	&GenInst_ICloneable_t2137_0_0_0,
	&GenInst_IComparable_1_t3740_0_0_0,
	&GenInst_IEquatable_1_t3745_0_0_0,
	&GenInst_List_1_t128_0_0_0,
	&GenInst_List_1_t388_0_0_0,
	&GenInst_Int32_t392_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2280_0_0_0,
	&GenInst_Link_t1547_0_0_0,
	&GenInst_Int32_t392_0_0_0_Object_t_0_0_0_Int32_t392_0_0_0,
	&GenInst_Int32_t392_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Int32_t392_0_0_0_Object_t_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_DictionaryEntry_t1423_0_0_0,
	&GenInst_Int32_t392_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2280_0_0_0,
	&GenInst_Int32_t392_0_0_0_PointerEventData_t175_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t397_0_0_0,
	&GenInst_RaycastHit2D_t402_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t392_0_0_0,
	&GenInst_KeyValuePair_2_t2312_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t392_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t392_0_0_0_Int32_t392_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t392_0_0_0_KeyValuePair_2_t2312_0_0_0,
	&GenInst_ICanvasElement_t379_0_0_0_Int32_t392_0_0_0,
	&GenInst_ICanvasElement_t379_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_List_1_t409_0_0_0,
	&GenInst_KeyValuePair_2_t2365_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2365_0_0_0,
	&GenInst_Font_t225_0_0_0_List_1_t413_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2375_0_0_0,
	&GenInst_Graphic_t228_0_0_0_Int32_t392_0_0_0,
	&GenInst_Graphic_t228_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_Canvas_t230_0_0_0_IndexedSet_1_t422_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2399_0_0_0,
	&GenInst_KeyValuePair_2_t2403_0_0_0,
	&GenInst_KeyValuePair_2_t2407_0_0_0,
	&GenInst_Enum_t700_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2415_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t393_0_0_0,
	&GenInst_IClipper_t387_0_0_0_Int32_t392_0_0_0,
	&GenInst_IClipper_t387_0_0_0_Int32_t392_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2467_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t117_0_0_0,
	&GenInst_List_1_t345_0_0_0,
	&GenInst_List_1_t346_0_0_0,
	&GenInst_List_1_t347_0_0_0,
	&GenInst_List_1_t348_0_0_0,
	&GenInst_List_1_t349_0_0_0,
	&GenInst_List_1_t380_0_0_0,
	&GenInst_IAchievementDescription_t3413_0_0_0,
	&GenInst_IAchievement_t677_0_0_0,
	&GenInst_IScore_t632_0_0_0,
	&GenInst_IUserProfile_t3412_0_0_0,
	&GenInst_AchievementDescription_t628_0_0_0,
	&GenInst_UserProfile_t625_0_0_0,
	&GenInst_GcAchievementData_t613_0_0_0,
	&GenInst_Achievement_t627_0_0_0,
	&GenInst_GcScoreData_t614_0_0_0,
	&GenInst_Score_t629_0_0_0,
	&GenInst_IComparable_1_t3924_0_0_0,
	&GenInst_IEquatable_1_t3929_0_0_0,
	&GenInst_Display_t518_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_ISerializable_t2156_0_0_0,
	&GenInst_ContactPoint_t534_0_0_0,
	&GenInst_ContactPoint2D_t541_0_0_0,
	&GenInst_WebCamDevice_t550_0_0_0,
	&GenInst_Keyframe_t557_0_0_0,
	&GenInst_CharacterInfo_t566_0_0_0,
	&GenInst_GUILayoutOption_t589_0_0_0,
	&GenInst_Int32_t392_0_0_0_LayoutCache_t580_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2605_0_0_0,
	&GenInst_GUIStyle_t584_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t584_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2616_0_0_0,
	&GenInst_DisallowMultipleComponent_t603_0_0_0,
	&GenInst_Attribute_t104_0_0_0,
	&GenInst__Attribute_t3443_0_0_0,
	&GenInst_ExecuteInEditMode_t606_0_0_0,
	&GenInst_RequireComponent_t604_0_0_0,
	&GenInst_ParameterModifier_t1710_0_0_0,
	&GenInst_HitInfo_t633_0_0_0,
	&GenInst_ParameterInfo_t716_0_0_0,
	&GenInst__ParameterInfo_t3496_0_0_0,
	&GenInst_Event_t269_0_0_0_TextEditOp_t650_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0,
	&GenInst_KeyValuePair_2_t2636_0_0_0,
	&GenInst_TextEditOp_t650_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_TextEditOp_t650_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t650_0_0_0_KeyValuePair_2_t2636_0_0_0,
	&GenInst_Event_t269_0_0_0,
	&GenInst_Event_t269_0_0_0_TextEditOp_t650_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2650_0_0_0,
	&GenInst_Camera_t91_0_0_0_VideoBackgroundAbstractBehaviour_t67_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2679_0_0_0,
	&GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2708_0_0_0,
	&GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_PIXEL_FORMAT_t783_0_0_0,
	&GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_PIXEL_FORMAT_t783_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2708_0_0_0,
	&GenInst_PIXEL_FORMAT_t783_0_0_0_Image_t784_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2722_0_0_0,
	&GenInst_Int32_t392_0_0_0_Trackable_t737_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2735_0_0_0,
	&GenInst_Byte_t699_0_0_0,
	&GenInst_IComparable_1_t4055_0_0_0,
	&GenInst_IEquatable_1_t4060_0_0_0,
	&GenInst_Int32_t392_0_0_0_VirtualButton_t895_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2745_0_0_0,
	&GenInst_Int32_t392_0_0_0_Marker_t903_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2762_0_0_0,
	&GenInst_SmartTerrainRevisionData_t818_0_0_0,
	&GenInst_SurfaceData_t819_0_0_0,
	&GenInst_PropData_t820_0_0_0,
	&GenInst_IEditorTrackableBehaviour_t981_0_0_0,
	&GenInst_Image_t784_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0,
	&GenInst_KeyValuePair_2_t2792_0_0_0,
	&GenInst_UInt16_t1430_0_0_0,
	&GenInst_IComparable_1_t4111_0_0_0,
	&GenInst_IEquatable_1_t4116_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_UInt16_t1430_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t1430_0_0_0_KeyValuePair_2_t2792_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t1430_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2807_0_0_0,
	&GenInst_Int32_t392_0_0_0_WordResult_t860_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2813_0_0_0,
	&GenInst_Int32_t392_0_0_0_WordAbstractBehaviour_t80_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t1001_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t858_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2833_0_0_0,
	&GenInst_List_1_t858_0_0_0,
	&GenInst_Int32_t392_0_0_0_Surface_t82_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t1014_0_0_0,
	&GenInst_Int32_t392_0_0_0_SurfaceAbstractBehaviour_t58_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2854_0_0_0,
	&GenInst_Int32_t392_0_0_0_Prop_t81_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t1013_0_0_0,
	&GenInst_Int32_t392_0_0_0_PropAbstractBehaviour_t52_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2860_0_0_0,
	&GenInst_Int32_t392_0_0_0_TrackableBehaviour_t32_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2875_0_0_0,
	&GenInst_MarkerAbstractBehaviour_t45_0_0_0,
	&GenInst_IEditorMarkerBehaviour_t1030_0_0_0,
	&GenInst_WorldCenterTrackableBehaviour_t828_0_0_0,
	&GenInst_DataSetTrackableBehaviour_t738_0_0_0,
	&GenInst_IEditorDataSetTrackableBehaviour_t1032_0_0_0,
	&GenInst_IEditorVirtualButtonBehaviour_t1044_0_0_0,
	&GenInst_KeyValuePair_2_t2881_0_0_0,
	&GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_Int32_t392_0_0_0,
	&GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_TrackableResultData_t810_0_0_0,
	&GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_Int32_t392_0_0_0_TrackableResultData_t810_0_0_0_KeyValuePair_2_t2881_0_0_0,
	&GenInst_KeyValuePair_2_t2896_0_0_0,
	&GenInst_VirtualButtonData_t811_0_0_0,
	&GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_Int32_t392_0_0_0,
	&GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_VirtualButtonData_t811_0_0_0,
	&GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_Int32_t392_0_0_0_VirtualButtonData_t811_0_0_0_KeyValuePair_2_t2896_0_0_0,
	&GenInst_Int32_t392_0_0_0_ImageTarget_t897_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2927_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t898_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0,
	&GenInst_KeyValuePair_2_t2935_0_0_0,
	&GenInst_ProfileData_t898_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_ProfileData_t898_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t898_0_0_0_KeyValuePair_2_t2935_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t898_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2949_0_0_0,
	&GenInst_Int32_t392_0_0_0_VirtualButtonAbstractBehaviour_t71_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2954_0_0_0,
	&GenInst_Link_t2983_0_0_0,
	&GenInst_KeySizes_t1087_0_0_0,
	&GenInst_UInt32_t712_0_0_0,
	&GenInst_IComparable_1_t4230_0_0_0,
	&GenInst_IEquatable_1_t4235_0_0_0,
	&GenInst_BigInteger_t1092_0_0_0,
	&GenInst_ByteU5BU5D_t786_0_0_0,
	&GenInst_Array_t_0_0_0,
	&GenInst_ICollection_t1424_0_0_0,
	&GenInst_IList_t1384_0_0_0,
	&GenInst_X509Certificate_t1199_0_0_0,
	&GenInst_IDeserializationCallback_t2159_0_0_0,
	&GenInst_ClientCertificateType_t1203_0_0_0,
	&GenInst_KeyValuePair_2_t3005_0_0_0,
	&GenInst_IComparable_1_t4278_0_0_0,
	&GenInst_IEquatable_1_t4283_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_Boolean_t393_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t393_0_0_0_KeyValuePair_2_t3005_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t393_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t3020_0_0_0,
	&GenInst_X509ChainStatus_t1333_0_0_0,
	&GenInst_Capture_t1352_0_0_0,
	&GenInst_Group_t1271_0_0_0,
	&GenInst_Mark_t1375_0_0_0,
	&GenInst_UriScheme_t1412_0_0_0,
	&GenInst_Int64_t711_0_0_0,
	&GenInst_UInt64_t1442_0_0_0,
	&GenInst_SByte_t1443_0_0_0,
	&GenInst_Int16_t1444_0_0_0,
	&GenInst_Decimal_t1445_0_0_0,
	&GenInst_Delegate_t110_0_0_0,
	&GenInst_IComparable_1_t4308_0_0_0,
	&GenInst_IEquatable_1_t4309_0_0_0,
	&GenInst_IComparable_1_t4312_0_0_0,
	&GenInst_IEquatable_1_t4313_0_0_0,
	&GenInst_IComparable_1_t4310_0_0_0,
	&GenInst_IEquatable_1_t4311_0_0_0,
	&GenInst_IComparable_1_t4306_0_0_0,
	&GenInst_IEquatable_1_t4307_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t3492_0_0_0,
	&GenInst_ConstructorInfo_t723_0_0_0,
	&GenInst__ConstructorInfo_t3490_0_0_0,
	&GenInst_MethodBase_t714_0_0_0,
	&GenInst__MethodBase_t3493_0_0_0,
	&GenInst_TableRange_t1480_0_0_0,
	&GenInst_TailoringInfo_t1483_0_0_0,
	&GenInst_Contraction_t1484_0_0_0,
	&GenInst_Level2Map_t1486_0_0_0,
	&GenInst_BigInteger_t1507_0_0_0,
	&GenInst_Slot_t1557_0_0_0,
	&GenInst_Slot_t1565_0_0_0,
	&GenInst_StackFrame_t713_0_0_0,
	&GenInst_Calendar_t1578_0_0_0,
	&GenInst_ModuleBuilder_t1655_0_0_0,
	&GenInst__ModuleBuilder_t3485_0_0_0,
	&GenInst_Module_t1651_0_0_0,
	&GenInst__Module_t3495_0_0_0,
	&GenInst_ParameterBuilder_t1661_0_0_0,
	&GenInst__ParameterBuilder_t3486_0_0_0,
	&GenInst_TypeU5BU5D_t679_0_0_0,
	&GenInst_ILTokenInfo_t1645_0_0_0,
	&GenInst_LabelData_t1647_0_0_0,
	&GenInst_LabelFixup_t1646_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1643_0_0_0,
	&GenInst_TypeBuilder_t1637_0_0_0,
	&GenInst__TypeBuilder_t3487_0_0_0,
	&GenInst_MethodBuilder_t1644_0_0_0,
	&GenInst__MethodBuilder_t3484_0_0_0,
	&GenInst__MethodInfo_t3494_0_0_0,
	&GenInst_ConstructorBuilder_t1635_0_0_0,
	&GenInst__ConstructorBuilder_t3480_0_0_0,
	&GenInst_FieldBuilder_t1641_0_0_0,
	&GenInst__FieldBuilder_t3482_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t3497_0_0_0,
	&GenInst_ResourceInfo_t1721_0_0_0,
	&GenInst_ResourceCacheItem_t1722_0_0_0,
	&GenInst_IContextProperty_t2116_0_0_0,
	&GenInst_Header_t1802_0_0_0,
	&GenInst_ITrackingHandler_t2151_0_0_0,
	&GenInst_IContextAttribute_t2138_0_0_0,
	&GenInst_IComparable_1_t4538_0_0_0,
	&GenInst_IEquatable_1_t4543_0_0_0,
	&GenInst_IComparable_1_t4314_0_0_0,
	&GenInst_IEquatable_1_t4315_0_0_0,
	&GenInst_IComparable_1_t4562_0_0_0,
	&GenInst_IEquatable_1_t4567_0_0_0,
	&GenInst_TypeTag_t1858_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_Version_t1304_0_0_0,
	&GenInst_RaycastResult_t169_0_0_0_RaycastResult_t169_0_0_0,
	&GenInst_Int32_t392_0_0_0_Int32_t392_0_0_0,
	&GenInst_DictionaryEntry_t1423_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_KeyValuePair_2_t2280_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2280_0_0_0_KeyValuePair_2_t2280_0_0_0,
	&GenInst_KeyValuePair_2_t2312_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2312_0_0_0_KeyValuePair_2_t2312_0_0_0,
	&GenInst_KeyValuePair_2_t2365_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2365_0_0_0_KeyValuePair_2_t2365_0_0_0,
	&GenInst_UIVertex_t272_0_0_0_UIVertex_t272_0_0_0,
	&GenInst_Vector3_t12_0_0_0_Vector3_t12_0_0_0,
	&GenInst_Color32_t382_0_0_0_Color32_t382_0_0_0,
	&GenInst_Vector2_t171_0_0_0_Vector2_t171_0_0_0,
	&GenInst_Vector4_t350_0_0_0_Vector4_t350_0_0_0,
	&GenInst_UICharInfo_t430_0_0_0_UICharInfo_t430_0_0_0,
	&GenInst_UILineInfo_t428_0_0_0_UILineInfo_t428_0_0_0,
	&GenInst_TextEditOp_t650_0_0_0_Object_t_0_0_0,
	&GenInst_TextEditOp_t650_0_0_0_TextEditOp_t650_0_0_0,
	&GenInst_KeyValuePair_2_t2636_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2636_0_0_0_KeyValuePair_2_t2636_0_0_0,
	&GenInst_PIXEL_FORMAT_t783_0_0_0_PIXEL_FORMAT_t783_0_0_0,
	&GenInst_KeyValuePair_2_t2708_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2708_0_0_0_KeyValuePair_2_t2708_0_0_0,
	&GenInst_UInt16_t1430_0_0_0_Object_t_0_0_0,
	&GenInst_UInt16_t1430_0_0_0_UInt16_t1430_0_0_0,
	&GenInst_KeyValuePair_2_t2792_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2792_0_0_0_KeyValuePair_2_t2792_0_0_0,
	&GenInst_TrackableResultData_t810_0_0_0_Object_t_0_0_0,
	&GenInst_TrackableResultData_t810_0_0_0_TrackableResultData_t810_0_0_0,
	&GenInst_KeyValuePair_2_t2881_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2881_0_0_0_KeyValuePair_2_t2881_0_0_0,
	&GenInst_VirtualButtonData_t811_0_0_0_Object_t_0_0_0,
	&GenInst_VirtualButtonData_t811_0_0_0_VirtualButtonData_t811_0_0_0,
	&GenInst_KeyValuePair_2_t2896_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2896_0_0_0_KeyValuePair_2_t2896_0_0_0,
	&GenInst_TargetSearchResult_t884_0_0_0_TargetSearchResult_t884_0_0_0,
	&GenInst_ProfileData_t898_0_0_0_Object_t_0_0_0,
	&GenInst_ProfileData_t898_0_0_0_ProfileData_t898_0_0_0,
	&GenInst_KeyValuePair_2_t2935_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2935_0_0_0_KeyValuePair_2_t2935_0_0_0,
	&GenInst_Boolean_t393_0_0_0_Object_t_0_0_0,
	&GenInst_Boolean_t393_0_0_0_Boolean_t393_0_0_0,
	&GenInst_KeyValuePair_2_t3005_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3005_0_0_0_KeyValuePair_2_t3005_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1692_0_0_0_CustomAttributeTypedArgument_t1692_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t1691_0_0_0_CustomAttributeNamedArgument_t1691_0_0_0,
	&GenInst_ExecuteEvents_Execute_m26752_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m26753_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m26755_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m26756_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m26757_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t3400_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m26784_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t3404_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t3404_gp_0_0_0_0_Int32_t392_0_0_0,
	&GenInst_ListPool_1_t3405_gp_0_0_0_0,
	&GenInst_List_1_t4614_0_0_0,
	&GenInst_ObjectPool_1_t3406_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m26851_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m26855_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m26856_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m26857_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m26858_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m26860_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m26863_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m26865_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m26866_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m26867_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t3414_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3415_gp_0_0_0_0_InvokableCall_2_t3415_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t3415_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3415_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3416_gp_0_0_0_0_InvokableCall_3_t3416_gp_1_0_0_0_InvokableCall_3_t3416_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3416_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3416_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3416_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3417_gp_0_0_0_0_InvokableCall_4_t3417_gp_1_0_0_0_InvokableCall_4_t3417_gp_2_0_0_0_InvokableCall_4_t3417_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t3417_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t3417_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t3417_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3417_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t724_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t3418_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t3419_gp_0_0_0_0_UnityEvent_2_t3419_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t3420_gp_0_0_0_0_UnityEvent_3_t3420_gp_1_0_0_0_UnityEvent_3_t3420_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t3421_gp_0_0_0_0_UnityEvent_4_t3421_gp_1_0_0_0_UnityEvent_4_t3421_gp_2_0_0_0_UnityEvent_4_t3421_gp_3_0_0_0,
	&GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m27239_gp_0_0_0_0,
	&GenInst_HashSet_1_t3430_gp_0_0_0_0,
	&GenInst_Enumerator_t3432_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3433_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m27449_gp_0_0_0_0,
	&GenInst_Enumerable_Cast_m27450_gp_0_0_0_0,
	&GenInst_Enumerable_CreateCastIterator_m27451_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m27452_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m27453_gp_0_0_0_0,
	&GenInst_Enumerable_First_m27454_gp_0_0_0_0,
	&GenInst_Enumerable_ToArray_m27455_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m27456_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m27457_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m27457_gp_0_0_0_0_Boolean_t393_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m27458_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m27458_gp_0_0_0_0_Boolean_t393_0_0_0,
	&GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t3434_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3435_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3435_gp_0_0_0_0_Boolean_t393_0_0_0,
	&GenInst_LinkedList_1_t3437_gp_0_0_0_0,
	&GenInst_Enumerator_t3438_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t3439_gp_0_0_0_0,
	&GenInst_Stack_1_t3440_gp_0_0_0_0,
	&GenInst_Enumerator_t3441_gp_0_0_0_0,
	&GenInst_IEnumerable_1_t3447_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m27624_gp_0_0_0_0,
	&GenInst_Array_Sort_m27636_gp_0_0_0_0_Array_Sort_m27636_gp_0_0_0_0,
	&GenInst_Array_Sort_m27637_gp_0_0_0_0_Array_Sort_m27637_gp_1_0_0_0,
	&GenInst_Array_Sort_m27638_gp_0_0_0_0,
	&GenInst_Array_Sort_m27638_gp_0_0_0_0_Array_Sort_m27638_gp_0_0_0_0,
	&GenInst_Array_Sort_m27639_gp_0_0_0_0,
	&GenInst_Array_Sort_m27639_gp_0_0_0_0_Array_Sort_m27639_gp_1_0_0_0,
	&GenInst_Array_Sort_m27640_gp_0_0_0_0_Array_Sort_m27640_gp_0_0_0_0,
	&GenInst_Array_Sort_m27641_gp_0_0_0_0_Array_Sort_m27641_gp_1_0_0_0,
	&GenInst_Array_Sort_m27642_gp_0_0_0_0,
	&GenInst_Array_Sort_m27642_gp_0_0_0_0_Array_Sort_m27642_gp_0_0_0_0,
	&GenInst_Array_Sort_m27643_gp_0_0_0_0,
	&GenInst_Array_Sort_m27643_gp_1_0_0_0,
	&GenInst_Array_Sort_m27643_gp_0_0_0_0_Array_Sort_m27643_gp_1_0_0_0,
	&GenInst_Array_Sort_m27644_gp_0_0_0_0,
	&GenInst_Array_Sort_m27645_gp_0_0_0_0,
	&GenInst_Array_qsort_m27646_gp_0_0_0_0,
	&GenInst_Array_qsort_m27646_gp_0_0_0_0_Array_qsort_m27646_gp_1_0_0_0,
	&GenInst_Array_compare_m27647_gp_0_0_0_0,
	&GenInst_Array_qsort_m27648_gp_0_0_0_0,
	&GenInst_Array_Resize_m27651_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m27653_gp_0_0_0_0,
	&GenInst_Array_ForEach_m27654_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m27655_gp_0_0_0_0_Array_ConvertAll_m27655_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m27656_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m27657_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m27658_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m27659_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m27660_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m27661_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m27662_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m27663_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m27664_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m27665_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m27666_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m27667_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m27668_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m27669_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m27670_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m27671_gp_0_0_0_0,
	&GenInst_Array_FindAll_m27672_gp_0_0_0_0,
	&GenInst_Array_Exists_m27673_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m27674_gp_0_0_0_0,
	&GenInst_Array_Find_m27675_gp_0_0_0_0,
	&GenInst_Array_FindLast_m27676_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3448_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3449_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3450_gp_0_0_0_0,
	&GenInst_IList_1_t3451_gp_0_0_0_0,
	&GenInst_ICollection_1_t3452_gp_0_0_0_0,
	&GenInst_Nullable_1_t2128_gp_0_0_0_0,
	&GenInst_Comparer_1_t3459_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3460_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t3397_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3461_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4752_0_0_0,
	&GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m27825_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m27830_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m27830_gp_0_0_0_0_Object_t_0_0_0,
	&GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_DictionaryEntry_t1423_0_0_0,
	&GenInst_ShimEnumerator_t3462_gp_0_0_0_0_ShimEnumerator_t3462_gp_1_0_0_0,
	&GenInst_Enumerator_t3463_gp_0_0_0_0_Enumerator_t3463_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4766_0_0_0,
	&GenInst_KeyCollection_t3464_gp_0_0_0_0_KeyCollection_t3464_gp_1_0_0_0,
	&GenInst_KeyCollection_t3464_gp_0_0_0_0,
	&GenInst_Enumerator_t3465_gp_0_0_0_0_Enumerator_t3465_gp_1_0_0_0,
	&GenInst_Enumerator_t3465_gp_0_0_0_0,
	&GenInst_KeyCollection_t3464_gp_0_0_0_0_KeyCollection_t3464_gp_1_0_0_0_KeyCollection_t3464_gp_0_0_0_0,
	&GenInst_KeyCollection_t3464_gp_0_0_0_0_KeyCollection_t3464_gp_0_0_0_0,
	&GenInst_ValueCollection_t3466_gp_0_0_0_0_ValueCollection_t3466_gp_1_0_0_0,
	&GenInst_ValueCollection_t3466_gp_1_0_0_0,
	&GenInst_Enumerator_t3467_gp_0_0_0_0_Enumerator_t3467_gp_1_0_0_0,
	&GenInst_Enumerator_t3467_gp_1_0_0_0,
	&GenInst_ValueCollection_t3466_gp_0_0_0_0_ValueCollection_t3466_gp_1_0_0_0_ValueCollection_t3466_gp_1_0_0_0,
	&GenInst_ValueCollection_t3466_gp_1_0_0_0_ValueCollection_t3466_gp_1_0_0_0,
	&GenInst_Dictionary_2_t3461_gp_0_0_0_0_Dictionary_2_t3461_gp_1_0_0_0_KeyValuePair_2_t4752_0_0_0,
	&GenInst_KeyValuePair_2_t4752_0_0_0_KeyValuePair_2_t4752_0_0_0,
	&GenInst_Dictionary_2_t3461_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t3469_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3470_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t3396_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t4802_0_0_0,
	&GenInst_IDictionary_2_t3472_gp_0_0_0_0_IDictionary_2_t3472_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3474_gp_0_0_0_0_KeyValuePair_2_t3474_gp_1_0_0_0,
	&GenInst_List_1_t3475_gp_0_0_0_0,
	&GenInst_Enumerator_t3476_gp_0_0_0_0,
	&GenInst_Collection_1_t3477_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t3478_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m28108_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m28108_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m28109_gp_0_0_0_0,
};
