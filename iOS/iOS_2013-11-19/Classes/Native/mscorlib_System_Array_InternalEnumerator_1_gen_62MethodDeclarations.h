﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_62.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__5.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m20902_gshared (InternalEnumerator_1_t2770 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m20902(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2770 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m20902_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20903_gshared (InternalEnumerator_1_t2770 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20903(__this, method) (( void (*) (InternalEnumerator_1_t2770 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20903_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20904_gshared (InternalEnumerator_1_t2770 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20904(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2770 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20904_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m20905_gshared (InternalEnumerator_1_t2770 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m20905(__this, method) (( void (*) (InternalEnumerator_1_t2770 *, const MethodInfo*))InternalEnumerator_1_Dispose_m20905_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m20906_gshared (InternalEnumerator_1_t2770 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m20906(__this, method) (( bool (*) (InternalEnumerator_1_t2770 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m20906_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::get_Current()
extern "C" WordData_t815  InternalEnumerator_1_get_Current_m20907_gshared (InternalEnumerator_1_t2770 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m20907(__this, method) (( WordData_t815  (*) (InternalEnumerator_1_t2770 *, const MethodInfo*))InternalEnumerator_1_get_Current_m20907_gshared)(__this, method)
