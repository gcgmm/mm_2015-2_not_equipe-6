﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_23.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15994_gshared (InternalEnumerator_1_t2412 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15994(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2412 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15994_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15995_gshared (InternalEnumerator_1_t2412 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15995(__this, method) (( void (*) (InternalEnumerator_1_t2412 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15995_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15996_gshared (InternalEnumerator_1_t2412 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15996(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2412 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15996_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15997_gshared (InternalEnumerator_1_t2412 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15997(__this, method) (( void (*) (InternalEnumerator_1_t2412 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15997_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15998_gshared (InternalEnumerator_1_t2412 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15998(__this, method) (( bool (*) (InternalEnumerator_1_t2412 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15998_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m15999_gshared (InternalEnumerator_1_t2412 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15999(__this, method) (( int32_t (*) (InternalEnumerator_1_t2412 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15999_gshared)(__this, method)
