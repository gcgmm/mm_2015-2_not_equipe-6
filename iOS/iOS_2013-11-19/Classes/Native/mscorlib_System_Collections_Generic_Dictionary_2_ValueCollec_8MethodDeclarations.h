﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_26MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m20803(__this, ___host, method) (( void (*) (Enumerator_t971 *, Dictionary_2_t799 *, const MethodInfo*))Enumerator__ctor_m14335_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20804(__this, method) (( Object_t * (*) (Enumerator_t971 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14336_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m20805(__this, method) (( void (*) (Enumerator_t971 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14337_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::Dispose()
#define Enumerator_Dispose_m5317(__this, method) (( void (*) (Enumerator_t971 *, const MethodInfo*))Enumerator_Dispose_m14338_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::MoveNext()
#define Enumerator_MoveNext_m5316(__this, method) (( bool (*) (Enumerator_t971 *, const MethodInfo*))Enumerator_MoveNext_m14339_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::get_Current()
#define Enumerator_get_Current_m5315(__this, method) (( Object_t * (*) (Enumerator_t971 *, const MethodInfo*))Enumerator_get_Current_m14340_gshared)(__this, method)
