﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Hashtable/KeyMarker
struct KeyMarker_t1558;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Hashtable/KeyMarker::.ctor()
extern "C" void KeyMarker__ctor_m9179 (KeyMarker_t1558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable/KeyMarker::.cctor()
extern "C" void KeyMarker__cctor_m9180 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
