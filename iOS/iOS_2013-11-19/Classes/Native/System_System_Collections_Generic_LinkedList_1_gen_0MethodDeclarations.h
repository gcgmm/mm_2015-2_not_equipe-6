﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t2999;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t687;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2460;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t3000;
// System.Object[]
struct ObjectU5BU5D_t105;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern "C" void LinkedList_1__ctor_m24445_gshared (LinkedList_1_t2999 * __this, const MethodInfo* method);
#define LinkedList_1__ctor_m24445(__this, method) (( void (*) (LinkedList_1_t2999 *, const MethodInfo*))LinkedList_1__ctor_m24445_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1__ctor_m24446_gshared (LinkedList_1_t2999 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define LinkedList_1__ctor_m24446(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t2999 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))LinkedList_1__ctor_m24446_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24447_gshared (LinkedList_1_t2999 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24447(__this, ___value, method) (( void (*) (LinkedList_1_t2999 *, Object_t *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24447_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m24448_gshared (LinkedList_1_t2999 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m24448(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t2999 *, Array_t *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m24448_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24449_gshared (LinkedList_1_t2999 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24449(__this, method) (( Object_t* (*) (LinkedList_1_t2999 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24449_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m24450_gshared (LinkedList_1_t2999 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m24450(__this, method) (( Object_t * (*) (LinkedList_1_t2999 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m24450_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24451_gshared (LinkedList_1_t2999 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24451(__this, method) (( bool (*) (LinkedList_1_t2999 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24451_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m24452_gshared (LinkedList_1_t2999 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m24452(__this, method) (( bool (*) (LinkedList_1_t2999 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m24452_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m24453_gshared (LinkedList_1_t2999 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m24453(__this, method) (( Object_t * (*) (LinkedList_1_t2999 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m24453_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_VerifyReferencedNode_m24454_gshared (LinkedList_1_t2999 * __this, LinkedListNode_1_t3000 * ___node, const MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m24454(__this, ___node, method) (( void (*) (LinkedList_1_t2999 *, LinkedListNode_1_t3000 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m24454_gshared)(__this, ___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C" LinkedListNode_1_t3000 * LinkedList_1_AddLast_m24455_gshared (LinkedList_1_t2999 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_AddLast_m24455(__this, ___value, method) (( LinkedListNode_1_t3000 * (*) (LinkedList_1_t2999 *, Object_t *, const MethodInfo*))LinkedList_1_AddLast_m24455_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C" void LinkedList_1_Clear_m24456_gshared (LinkedList_1_t2999 * __this, const MethodInfo* method);
#define LinkedList_1_Clear_m24456(__this, method) (( void (*) (LinkedList_1_t2999 *, const MethodInfo*))LinkedList_1_Clear_m24456_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C" bool LinkedList_1_Contains_m24457_gshared (LinkedList_1_t2999 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Contains_m24457(__this, ___value, method) (( bool (*) (LinkedList_1_t2999 *, Object_t *, const MethodInfo*))LinkedList_1_Contains_m24457_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void LinkedList_1_CopyTo_m24458_gshared (LinkedList_1_t2999 * __this, ObjectU5BU5D_t105* ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_CopyTo_m24458(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t2999 *, ObjectU5BU5D_t105*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m24458_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C" LinkedListNode_1_t3000 * LinkedList_1_Find_m24459_gshared (LinkedList_1_t2999 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Find_m24459(__this, ___value, method) (( LinkedListNode_1_t3000 * (*) (LinkedList_1_t2999 *, Object_t *, const MethodInfo*))LinkedList_1_Find_m24459_gshared)(__this, ___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3001  LinkedList_1_GetEnumerator_m24460_gshared (LinkedList_1_t2999 * __this, const MethodInfo* method);
#define LinkedList_1_GetEnumerator_m24460(__this, method) (( Enumerator_t3001  (*) (LinkedList_1_t2999 *, const MethodInfo*))LinkedList_1_GetEnumerator_m24460_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1_GetObjectData_m24461_gshared (LinkedList_1_t2999 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define LinkedList_1_GetObjectData_m24461(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t2999 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))LinkedList_1_GetObjectData_m24461_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern "C" void LinkedList_1_OnDeserialization_m24462_gshared (LinkedList_1_t2999 * __this, Object_t * ___sender, const MethodInfo* method);
#define LinkedList_1_OnDeserialization_m24462(__this, ___sender, method) (( void (*) (LinkedList_1_t2999 *, Object_t *, const MethodInfo*))LinkedList_1_OnDeserialization_m24462_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C" bool LinkedList_1_Remove_m24463_gshared (LinkedList_1_t2999 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Remove_m24463(__this, ___value, method) (( bool (*) (LinkedList_1_t2999 *, Object_t *, const MethodInfo*))LinkedList_1_Remove_m24463_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_Remove_m24464_gshared (LinkedList_1_t2999 * __this, LinkedListNode_1_t3000 * ___node, const MethodInfo* method);
#define LinkedList_1_Remove_m24464(__this, ___node, method) (( void (*) (LinkedList_1_t2999 *, LinkedListNode_1_t3000 *, const MethodInfo*))LinkedList_1_Remove_m24464_gshared)(__this, ___node, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C" void LinkedList_1_RemoveLast_m24465_gshared (LinkedList_1_t2999 * __this, const MethodInfo* method);
#define LinkedList_1_RemoveLast_m24465(__this, method) (( void (*) (LinkedList_1_t2999 *, const MethodInfo*))LinkedList_1_RemoveLast_m24465_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C" int32_t LinkedList_1_get_Count_m24466_gshared (LinkedList_1_t2999 * __this, const MethodInfo* method);
#define LinkedList_1_get_Count_m24466(__this, method) (( int32_t (*) (LinkedList_1_t2999 *, const MethodInfo*))LinkedList_1_get_Count_m24466_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C" LinkedListNode_1_t3000 * LinkedList_1_get_First_m24467_gshared (LinkedList_1_t2999 * __this, const MethodInfo* method);
#define LinkedList_1_get_First_m24467(__this, method) (( LinkedListNode_1_t3000 * (*) (LinkedList_1_t2999 *, const MethodInfo*))LinkedList_1_get_First_m24467_gshared)(__this, method)
