﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>
struct Collection_1_t2918;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t2914;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t958;
// System.Collections.Generic.IList`1<Vuforia.TargetFinder/TargetSearchResult>
struct IList_1_t2917;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void Collection_1__ctor_m23393_gshared (Collection_1_t2918 * __this, const MethodInfo* method);
#define Collection_1__ctor_m23393(__this, method) (( void (*) (Collection_1_t2918 *, const MethodInfo*))Collection_1__ctor_m23393_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23394_gshared (Collection_1_t2918 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23394(__this, method) (( bool (*) (Collection_1_t2918 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23394_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m23395_gshared (Collection_1_t2918 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m23395(__this, ___array, ___index, method) (( void (*) (Collection_1_t2918 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m23395_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m23396_gshared (Collection_1_t2918 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m23396(__this, method) (( Object_t * (*) (Collection_1_t2918 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m23396_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m23397_gshared (Collection_1_t2918 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m23397(__this, ___value, method) (( int32_t (*) (Collection_1_t2918 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m23397_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m23398_gshared (Collection_1_t2918 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m23398(__this, ___value, method) (( bool (*) (Collection_1_t2918 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m23398_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m23399_gshared (Collection_1_t2918 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m23399(__this, ___value, method) (( int32_t (*) (Collection_1_t2918 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m23399_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m23400_gshared (Collection_1_t2918 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m23400(__this, ___index, ___value, method) (( void (*) (Collection_1_t2918 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m23400_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m23401_gshared (Collection_1_t2918 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m23401(__this, ___value, method) (( void (*) (Collection_1_t2918 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m23401_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m23402_gshared (Collection_1_t2918 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m23402(__this, method) (( bool (*) (Collection_1_t2918 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m23402_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m23403_gshared (Collection_1_t2918 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m23403(__this, method) (( Object_t * (*) (Collection_1_t2918 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m23403_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m23404_gshared (Collection_1_t2918 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m23404(__this, method) (( bool (*) (Collection_1_t2918 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m23404_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m23405_gshared (Collection_1_t2918 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m23405(__this, method) (( bool (*) (Collection_1_t2918 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m23405_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m23406_gshared (Collection_1_t2918 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m23406(__this, ___index, method) (( Object_t * (*) (Collection_1_t2918 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m23406_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m23407_gshared (Collection_1_t2918 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m23407(__this, ___index, ___value, method) (( void (*) (Collection_1_t2918 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m23407_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T)
extern "C" void Collection_1_Add_m23408_gshared (Collection_1_t2918 * __this, TargetSearchResult_t884  ___item, const MethodInfo* method);
#define Collection_1_Add_m23408(__this, ___item, method) (( void (*) (Collection_1_t2918 *, TargetSearchResult_t884 , const MethodInfo*))Collection_1_Add_m23408_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Clear()
extern "C" void Collection_1_Clear_m23409_gshared (Collection_1_t2918 * __this, const MethodInfo* method);
#define Collection_1_Clear_m23409(__this, method) (( void (*) (Collection_1_t2918 *, const MethodInfo*))Collection_1_Clear_m23409_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::ClearItems()
extern "C" void Collection_1_ClearItems_m23410_gshared (Collection_1_t2918 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m23410(__this, method) (( void (*) (Collection_1_t2918 *, const MethodInfo*))Collection_1_ClearItems_m23410_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
extern "C" bool Collection_1_Contains_m23411_gshared (Collection_1_t2918 * __this, TargetSearchResult_t884  ___item, const MethodInfo* method);
#define Collection_1_Contains_m23411(__this, ___item, method) (( bool (*) (Collection_1_t2918 *, TargetSearchResult_t884 , const MethodInfo*))Collection_1_Contains_m23411_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m23412_gshared (Collection_1_t2918 * __this, TargetSearchResultU5BU5D_t2914* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m23412(__this, ___array, ___index, method) (( void (*) (Collection_1_t2918 *, TargetSearchResultU5BU5D_t2914*, int32_t, const MethodInfo*))Collection_1_CopyTo_m23412_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m23413_gshared (Collection_1_t2918 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m23413(__this, method) (( Object_t* (*) (Collection_1_t2918 *, const MethodInfo*))Collection_1_GetEnumerator_m23413_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m23414_gshared (Collection_1_t2918 * __this, TargetSearchResult_t884  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m23414(__this, ___item, method) (( int32_t (*) (Collection_1_t2918 *, TargetSearchResult_t884 , const MethodInfo*))Collection_1_IndexOf_m23414_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m23415_gshared (Collection_1_t2918 * __this, int32_t ___index, TargetSearchResult_t884  ___item, const MethodInfo* method);
#define Collection_1_Insert_m23415(__this, ___index, ___item, method) (( void (*) (Collection_1_t2918 *, int32_t, TargetSearchResult_t884 , const MethodInfo*))Collection_1_Insert_m23415_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m23416_gshared (Collection_1_t2918 * __this, int32_t ___index, TargetSearchResult_t884  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m23416(__this, ___index, ___item, method) (( void (*) (Collection_1_t2918 *, int32_t, TargetSearchResult_t884 , const MethodInfo*))Collection_1_InsertItem_m23416_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T)
extern "C" bool Collection_1_Remove_m23417_gshared (Collection_1_t2918 * __this, TargetSearchResult_t884  ___item, const MethodInfo* method);
#define Collection_1_Remove_m23417(__this, ___item, method) (( bool (*) (Collection_1_t2918 *, TargetSearchResult_t884 , const MethodInfo*))Collection_1_Remove_m23417_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m23418_gshared (Collection_1_t2918 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m23418(__this, ___index, method) (( void (*) (Collection_1_t2918 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m23418_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m23419_gshared (Collection_1_t2918 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m23419(__this, ___index, method) (( void (*) (Collection_1_t2918 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m23419_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
extern "C" int32_t Collection_1_get_Count_m23420_gshared (Collection_1_t2918 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m23420(__this, method) (( int32_t (*) (Collection_1_t2918 *, const MethodInfo*))Collection_1_get_Count_m23420_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
extern "C" TargetSearchResult_t884  Collection_1_get_Item_m23421_gshared (Collection_1_t2918 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m23421(__this, ___index, method) (( TargetSearchResult_t884  (*) (Collection_1_t2918 *, int32_t, const MethodInfo*))Collection_1_get_Item_m23421_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m23422_gshared (Collection_1_t2918 * __this, int32_t ___index, TargetSearchResult_t884  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m23422(__this, ___index, ___value, method) (( void (*) (Collection_1_t2918 *, int32_t, TargetSearchResult_t884 , const MethodInfo*))Collection_1_set_Item_m23422_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m23423_gshared (Collection_1_t2918 * __this, int32_t ___index, TargetSearchResult_t884  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m23423(__this, ___index, ___item, method) (( void (*) (Collection_1_t2918 *, int32_t, TargetSearchResult_t884 , const MethodInfo*))Collection_1_SetItem_m23423_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m23424_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m23424(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m23424_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::ConvertItem(System.Object)
extern "C" TargetSearchResult_t884  Collection_1_ConvertItem_m23425_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m23425(__this /* static, unused */, ___item, method) (( TargetSearchResult_t884  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m23425_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m23426_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m23426(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m23426_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m23427_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m23427(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m23427_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m23428_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m23428(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m23428_gshared)(__this /* static, unused */, ___list, method)
