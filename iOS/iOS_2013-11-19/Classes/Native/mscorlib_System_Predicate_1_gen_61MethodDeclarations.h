﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"

// System.Void System.Predicate`1<Vuforia.ITrackerEventHandler>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m23969(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2960 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m12988_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.ITrackerEventHandler>::Invoke(T)
#define Predicate_1_Invoke_m23970(__this, ___obj, method) (( bool (*) (Predicate_1_t2960 *, Object_t *, const MethodInfo*))Predicate_1_Invoke_m12989_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.ITrackerEventHandler>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m23971(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2960 *, Object_t *, AsyncCallback_t261 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m12990_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.ITrackerEventHandler>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m23972(__this, ___result, method) (( bool (*) (Predicate_1_t2960 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m12991_gshared)(__this, ___result, method)
