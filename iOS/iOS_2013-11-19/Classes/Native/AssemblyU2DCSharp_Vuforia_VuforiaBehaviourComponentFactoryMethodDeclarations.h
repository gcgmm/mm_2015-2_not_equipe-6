﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VuforiaBehaviourComponentFactory
struct VuforiaBehaviourComponentFactory_t41;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t47;
// UnityEngine.GameObject
struct GameObject_t9;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t71;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t62;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t37;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t45;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t49;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t23;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t80;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t60;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t51;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C" void VuforiaBehaviourComponentFactory__ctor_m95 (VuforiaBehaviourComponentFactory_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern "C" MaskOutAbstractBehaviour_t47 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m96 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C" VirtualButtonAbstractBehaviour_t71 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m97 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern "C" TurnOffAbstractBehaviour_t62 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m98 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C" ImageTargetAbstractBehaviour_t37 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m99 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern "C" MarkerAbstractBehaviour_t45 * VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m100 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C" MultiTargetAbstractBehaviour_t49 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m101 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C" CylinderTargetAbstractBehaviour_t23 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m102 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C" WordAbstractBehaviour_t80 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m103 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern "C" TextRecoAbstractBehaviour_t60 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m104 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C" ObjectTargetAbstractBehaviour_t51 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m105 (VuforiaBehaviourComponentFactory_t41 * __this, GameObject_t9 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
