﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::.cctor()
extern "C" void PrimeHelper__cctor_m24307_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define PrimeHelper__cctor_m24307(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))PrimeHelper__cctor_m24307_gshared)(__this /* static, unused */, method)
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::TestPrime(System.Int32)
extern "C" bool PrimeHelper_TestPrime_m24308_gshared (Object_t * __this /* static, unused */, int32_t ___x, const MethodInfo* method);
#define PrimeHelper_TestPrime_m24308(__this /* static, unused */, ___x, method) (( bool (*) (Object_t * /* static, unused */, int32_t, const MethodInfo*))PrimeHelper_TestPrime_m24308_gshared)(__this /* static, unused */, ___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::CalcPrime(System.Int32)
extern "C" int32_t PrimeHelper_CalcPrime_m24309_gshared (Object_t * __this /* static, unused */, int32_t ___x, const MethodInfo* method);
#define PrimeHelper_CalcPrime_m24309(__this /* static, unused */, ___x, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, const MethodInfo*))PrimeHelper_CalcPrime_m24309_gshared)(__this /* static, unused */, ___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::ToPrime(System.Int32)
extern "C" int32_t PrimeHelper_ToPrime_m24310_gshared (Object_t * __this /* static, unused */, int32_t ___x, const MethodInfo* method);
#define PrimeHelper_ToPrime_m24310(__this /* static, unused */, ___x, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, const MethodInfo*))PrimeHelper_ToPrime_m24310_gshared)(__this /* static, unused */, ___x, method)
