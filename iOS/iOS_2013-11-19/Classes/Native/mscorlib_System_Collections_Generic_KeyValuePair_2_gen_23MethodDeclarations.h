﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m21363(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2807 *, Type_t *, uint16_t, const MethodInfo*))KeyValuePair_2__ctor_m21259_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::get_Key()
#define KeyValuePair_2_get_Key_m21364(__this, method) (( Type_t * (*) (KeyValuePair_2_t2807 *, const MethodInfo*))KeyValuePair_2_get_Key_m21260_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m21365(__this, ___value, method) (( void (*) (KeyValuePair_2_t2807 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m21261_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::get_Value()
#define KeyValuePair_2_get_Value_m21366(__this, method) (( uint16_t (*) (KeyValuePair_2_t2807 *, const MethodInfo*))KeyValuePair_2_get_Value_m21262_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m21367(__this, ___value, method) (( void (*) (KeyValuePair_2_t2807 *, uint16_t, const MethodInfo*))KeyValuePair_2_set_Value_m21263_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::ToString()
#define KeyValuePair_2_ToString_m21368(__this, method) (( String_t* (*) (KeyValuePair_2_t2807 *, const MethodInfo*))KeyValuePair_2_ToString_m21264_gshared)(__this, method)
