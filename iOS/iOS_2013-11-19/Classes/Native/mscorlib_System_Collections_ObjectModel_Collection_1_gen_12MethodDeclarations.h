﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>
struct Collection_1_t3077;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2130;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t3264;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>
struct IList_1_t1690;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void Collection_1__ctor_m25167_gshared (Collection_1_t3077 * __this, const MethodInfo* method);
#define Collection_1__ctor_m25167(__this, method) (( void (*) (Collection_1_t3077 *, const MethodInfo*))Collection_1__ctor_m25167_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25168_gshared (Collection_1_t3077 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25168(__this, method) (( bool (*) (Collection_1_t3077 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25168_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m25169_gshared (Collection_1_t3077 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m25169(__this, ___array, ___index, method) (( void (*) (Collection_1_t3077 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m25169_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m25170_gshared (Collection_1_t3077 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m25170(__this, method) (( Object_t * (*) (Collection_1_t3077 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m25170_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m25171_gshared (Collection_1_t3077 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m25171(__this, ___value, method) (( int32_t (*) (Collection_1_t3077 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m25171_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m25172_gshared (Collection_1_t3077 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m25172(__this, ___value, method) (( bool (*) (Collection_1_t3077 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m25172_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m25173_gshared (Collection_1_t3077 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m25173(__this, ___value, method) (( int32_t (*) (Collection_1_t3077 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m25173_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m25174_gshared (Collection_1_t3077 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m25174(__this, ___index, ___value, method) (( void (*) (Collection_1_t3077 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m25174_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m25175_gshared (Collection_1_t3077 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m25175(__this, ___value, method) (( void (*) (Collection_1_t3077 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m25175_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m25176_gshared (Collection_1_t3077 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m25176(__this, method) (( bool (*) (Collection_1_t3077 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m25176_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m25177_gshared (Collection_1_t3077 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m25177(__this, method) (( Object_t * (*) (Collection_1_t3077 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m25177_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m25178_gshared (Collection_1_t3077 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m25178(__this, method) (( bool (*) (Collection_1_t3077 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m25178_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m25179_gshared (Collection_1_t3077 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m25179(__this, method) (( bool (*) (Collection_1_t3077 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m25179_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m25180_gshared (Collection_1_t3077 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m25180(__this, ___index, method) (( Object_t * (*) (Collection_1_t3077 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m25180_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m25181_gshared (Collection_1_t3077 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m25181(__this, ___index, ___value, method) (( void (*) (Collection_1_t3077 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m25181_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void Collection_1_Add_m25182_gshared (Collection_1_t3077 * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define Collection_1_Add_m25182(__this, ___item, method) (( void (*) (Collection_1_t3077 *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Collection_1_Add_m25182_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void Collection_1_Clear_m25183_gshared (Collection_1_t3077 * __this, const MethodInfo* method);
#define Collection_1_Clear_m25183(__this, method) (( void (*) (Collection_1_t3077 *, const MethodInfo*))Collection_1_Clear_m25183_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ClearItems()
extern "C" void Collection_1_ClearItems_m25184_gshared (Collection_1_t3077 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m25184(__this, method) (( void (*) (Collection_1_t3077 *, const MethodInfo*))Collection_1_ClearItems_m25184_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool Collection_1_Contains_m25185_gshared (Collection_1_t3077 * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define Collection_1_Contains_m25185(__this, ___item, method) (( bool (*) (Collection_1_t3077 *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Collection_1_Contains_m25185_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m25186_gshared (Collection_1_t3077 * __this, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m25186(__this, ___array, ___index, method) (( void (*) (Collection_1_t3077 *, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, const MethodInfo*))Collection_1_CopyTo_m25186_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m25187_gshared (Collection_1_t3077 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m25187(__this, method) (( Object_t* (*) (Collection_1_t3077 *, const MethodInfo*))Collection_1_GetEnumerator_m25187_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m25188_gshared (Collection_1_t3077 * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m25188(__this, ___item, method) (( int32_t (*) (Collection_1_t3077 *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Collection_1_IndexOf_m25188_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m25189_gshared (Collection_1_t3077 * __this, int32_t ___index, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define Collection_1_Insert_m25189(__this, ___index, ___item, method) (( void (*) (Collection_1_t3077 *, int32_t, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Collection_1_Insert_m25189_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m25190_gshared (Collection_1_t3077 * __this, int32_t ___index, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m25190(__this, ___index, ___item, method) (( void (*) (Collection_1_t3077 *, int32_t, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Collection_1_InsertItem_m25190_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool Collection_1_Remove_m25191_gshared (Collection_1_t3077 * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define Collection_1_Remove_m25191(__this, ___item, method) (( bool (*) (Collection_1_t3077 *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Collection_1_Remove_m25191_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m25192_gshared (Collection_1_t3077 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m25192(__this, ___index, method) (( void (*) (Collection_1_t3077 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m25192_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m25193_gshared (Collection_1_t3077 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m25193(__this, ___index, method) (( void (*) (Collection_1_t3077 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m25193_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t Collection_1_get_Count_m25194_gshared (Collection_1_t3077 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m25194(__this, method) (( int32_t (*) (Collection_1_t3077 *, const MethodInfo*))Collection_1_get_Count_m25194_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1691  Collection_1_get_Item_m25195_gshared (Collection_1_t3077 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m25195(__this, ___index, method) (( CustomAttributeNamedArgument_t1691  (*) (Collection_1_t3077 *, int32_t, const MethodInfo*))Collection_1_get_Item_m25195_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m25196_gshared (Collection_1_t3077 * __this, int32_t ___index, CustomAttributeNamedArgument_t1691  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m25196(__this, ___index, ___value, method) (( void (*) (Collection_1_t3077 *, int32_t, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Collection_1_set_Item_m25196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m25197_gshared (Collection_1_t3077 * __this, int32_t ___index, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m25197(__this, ___index, ___item, method) (( void (*) (Collection_1_t3077 *, int32_t, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Collection_1_SetItem_m25197_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m25198_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m25198(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m25198_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ConvertItem(System.Object)
extern "C" CustomAttributeNamedArgument_t1691  Collection_1_ConvertItem_m25199_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m25199(__this /* static, unused */, ___item, method) (( CustomAttributeNamedArgument_t1691  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m25199_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m25200_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m25200(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m25200_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m25201_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m25201(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m25201_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m25202_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m25202(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m25202_gshared)(__this /* static, unused */, ___list, method)
