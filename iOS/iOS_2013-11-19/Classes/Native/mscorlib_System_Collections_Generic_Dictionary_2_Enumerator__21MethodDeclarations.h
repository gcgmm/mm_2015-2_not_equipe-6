﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m20789(__this, ___dictionary, method) (( void (*) (Enumerator_t2764 *, Dictionary_2_t799 *, const MethodInfo*))Enumerator__ctor_m14303_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20790(__this, method) (( Object_t * (*) (Enumerator_t2764 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14304_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m20791(__this, method) (( void (*) (Enumerator_t2764 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14305_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20792(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t2764 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14306_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20793(__this, method) (( Object_t * (*) (Enumerator_t2764 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14307_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20794(__this, method) (( Object_t * (*) (Enumerator_t2764 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14308_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::MoveNext()
#define Enumerator_MoveNext_m20795(__this, method) (( bool (*) (Enumerator_t2764 *, const MethodInfo*))Enumerator_MoveNext_m14309_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::get_Current()
#define Enumerator_get_Current_m20796(__this, method) (( KeyValuePair_2_t2762  (*) (Enumerator_t2764 *, const MethodInfo*))Enumerator_get_Current_m14310_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m20797(__this, method) (( int32_t (*) (Enumerator_t2764 *, const MethodInfo*))Enumerator_get_CurrentKey_m14311_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m20798(__this, method) (( Object_t * (*) (Enumerator_t2764 *, const MethodInfo*))Enumerator_get_CurrentValue_m14312_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::Reset()
#define Enumerator_Reset_m20799(__this, method) (( void (*) (Enumerator_t2764 *, const MethodInfo*))Enumerator_Reset_m14313_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::VerifyState()
#define Enumerator_VerifyState_m20800(__this, method) (( void (*) (Enumerator_t2764 *, const MethodInfo*))Enumerator_VerifyState_m14314_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m20801(__this, method) (( void (*) (Enumerator_t2764 *, const MethodInfo*))Enumerator_VerifyCurrent_m14315_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Marker>::Dispose()
#define Enumerator_Dispose_m20802(__this, method) (( void (*) (Enumerator_t2764 *, const MethodInfo*))Enumerator_Dispose_m14316_gshared)(__this, method)
