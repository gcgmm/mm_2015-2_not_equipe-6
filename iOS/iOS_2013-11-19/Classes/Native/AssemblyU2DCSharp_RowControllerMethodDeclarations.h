﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RowController
struct RowController_t10;
// UnityEngine.Material
struct Material_t8;
// UnityEngine.Material[]
struct MaterialU5BU5D_t4;

#include "codegen/il2cpp-codegen.h"

// System.Void RowController::.ctor()
extern "C" void RowController__ctor_m24 (RowController_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RowController::Awake()
extern "C" void RowController_Awake_m25 (RowController_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RowController::SetActive(System.Boolean)
extern "C" void RowController_SetActive_m26 (RowController_t10 * __this, bool ___flag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RowController::IsActive()
extern "C" bool RowController_IsActive_m27 (RowController_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RowController::SetColor(UnityEngine.Material,System.Int32)
extern "C" void RowController_SetColor_m28 (RowController_t10 * __this, Material_t8 * ___color, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RowController::IsComplete()
extern "C" bool RowController_IsComplete_m29 (RowController_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RowController::CheckCode(UnityEngine.Material[])
extern "C" bool RowController_CheckCode_m30 (RowController_t10 * __this, MaterialU5BU5D_t4* ___code, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RowController::CheckCodeEasy(UnityEngine.Material[])
extern "C" bool RowController_CheckCodeEasy_m31 (RowController_t10 * __this, MaterialU5BU5D_t4* ___code, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RowController::contains(UnityEngine.Material,UnityEngine.Material[])
extern "C" bool RowController_contains_m32 (RowController_t10 * __this, Material_t8 * ___color, MaterialU5BU5D_t4* ___code, const MethodInfo* method) IL2CPP_METHOD_ATTR;
