﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_65.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__9.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m20952_gshared (InternalEnumerator_1_t2774 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m20952(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2774 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m20952_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20953_gshared (InternalEnumerator_1_t2774 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20953(__this, method) (( void (*) (InternalEnumerator_1_t2774 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20953_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20954_gshared (InternalEnumerator_1_t2774 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20954(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2774 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20954_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m20955_gshared (InternalEnumerator_1_t2774 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m20955(__this, method) (( void (*) (InternalEnumerator_1_t2774 *, const MethodInfo*))InternalEnumerator_1_Dispose_m20955_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m20956_gshared (InternalEnumerator_1_t2774 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m20956(__this, method) (( bool (*) (InternalEnumerator_1_t2774 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m20956_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::get_Current()
extern "C" SurfaceData_t819  InternalEnumerator_1_get_Current_m20957_gshared (InternalEnumerator_1_t2774 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m20957(__this, method) (( SurfaceData_t819  (*) (InternalEnumerator_1_t2774 *, const MethodInfo*))InternalEnumerator_1_get_Current_m20957_gshared)(__this, method)
