﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_123.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24964_gshared (InternalEnumerator_1_t3065 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m24964(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3065 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m24964_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24965_gshared (InternalEnumerator_1_t3065 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24965(__this, method) (( void (*) (InternalEnumerator_1_t3065 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24965_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24966_gshared (InternalEnumerator_1_t3065 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24966(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3065 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24966_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24967_gshared (InternalEnumerator_1_t3065 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24967(__this, method) (( void (*) (InternalEnumerator_1_t3065 *, const MethodInfo*))InternalEnumerator_1_Dispose_m24967_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24968_gshared (InternalEnumerator_1_t3065 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24968(__this, method) (( bool (*) (InternalEnumerator_1_t3065 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m24968_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C" CustomAttributeNamedArgument_t1691  InternalEnumerator_1_get_Current_m24969_gshared (InternalEnumerator_1_t3065 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24969(__this, method) (( CustomAttributeNamedArgument_t1691  (*) (InternalEnumerator_1_t3065 *, const MethodInfo*))InternalEnumerator_1_get_Current_m24969_gshared)(__this, method)
