﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t2292;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t2278;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m14359_gshared (ShimEnumerator_t2292 * __this, Dictionary_2_t2278 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m14359(__this, ___host, method) (( void (*) (ShimEnumerator_t2292 *, Dictionary_2_t2278 *, const MethodInfo*))ShimEnumerator__ctor_m14359_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m14360_gshared (ShimEnumerator_t2292 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m14360(__this, method) (( bool (*) (ShimEnumerator_t2292 *, const MethodInfo*))ShimEnumerator_MoveNext_m14360_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1423  ShimEnumerator_get_Entry_m14361_gshared (ShimEnumerator_t2292 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m14361(__this, method) (( DictionaryEntry_t1423  (*) (ShimEnumerator_t2292 *, const MethodInfo*))ShimEnumerator_get_Entry_m14361_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m14362_gshared (ShimEnumerator_t2292 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m14362(__this, method) (( Object_t * (*) (ShimEnumerator_t2292 *, const MethodInfo*))ShimEnumerator_get_Key_m14362_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m14363_gshared (ShimEnumerator_t2292 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m14363(__this, method) (( Object_t * (*) (ShimEnumerator_t2292 *, const MethodInfo*))ShimEnumerator_get_Value_m14363_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m14364_gshared (ShimEnumerator_t2292 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m14364(__this, method) (( Object_t * (*) (ShimEnumerator_t2292 *, const MethodInfo*))ShimEnumerator_get_Current_m14364_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::Reset()
extern "C" void ShimEnumerator_Reset_m14365_gshared (ShimEnumerator_t2292 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m14365(__this, method) (( void (*) (ShimEnumerator_t2292 *, const MethodInfo*))ShimEnumerator_Reset_m14365_gshared)(__this, method)
