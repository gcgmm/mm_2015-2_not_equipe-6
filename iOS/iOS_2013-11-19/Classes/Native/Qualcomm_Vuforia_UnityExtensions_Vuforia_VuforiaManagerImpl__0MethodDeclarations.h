﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void TrackableResultData_t810_marshal(const TrackableResultData_t810& unmarshaled, TrackableResultData_t810_marshaled& marshaled);
extern "C" void TrackableResultData_t810_marshal_back(const TrackableResultData_t810_marshaled& marshaled, TrackableResultData_t810& unmarshaled);
extern "C" void TrackableResultData_t810_marshal_cleanup(TrackableResultData_t810_marshaled& marshaled);
