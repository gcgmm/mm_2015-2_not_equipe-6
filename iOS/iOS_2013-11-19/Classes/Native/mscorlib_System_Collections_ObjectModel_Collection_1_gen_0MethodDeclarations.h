﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>
struct Collection_1_t2243;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2238;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t3127;
// System.Collections.Generic.IList`1<UnityEngine.EventSystems.RaycastResult>
struct IList_1_t2242;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void Collection_1__ctor_m13681_gshared (Collection_1_t2243 * __this, const MethodInfo* method);
#define Collection_1__ctor_m13681(__this, method) (( void (*) (Collection_1_t2243 *, const MethodInfo*))Collection_1__ctor_m13681_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13682_gshared (Collection_1_t2243 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13682(__this, method) (( bool (*) (Collection_1_t2243 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13682_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m13683_gshared (Collection_1_t2243 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m13683(__this, ___array, ___index, method) (( void (*) (Collection_1_t2243 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m13683_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m13684_gshared (Collection_1_t2243 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m13684(__this, method) (( Object_t * (*) (Collection_1_t2243 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m13684_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m13685_gshared (Collection_1_t2243 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m13685(__this, ___value, method) (( int32_t (*) (Collection_1_t2243 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m13685_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m13686_gshared (Collection_1_t2243 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m13686(__this, ___value, method) (( bool (*) (Collection_1_t2243 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m13686_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m13687_gshared (Collection_1_t2243 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m13687(__this, ___value, method) (( int32_t (*) (Collection_1_t2243 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m13687_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m13688_gshared (Collection_1_t2243 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m13688(__this, ___index, ___value, method) (( void (*) (Collection_1_t2243 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m13688_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m13689_gshared (Collection_1_t2243 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m13689(__this, ___value, method) (( void (*) (Collection_1_t2243 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m13689_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m13690_gshared (Collection_1_t2243 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m13690(__this, method) (( bool (*) (Collection_1_t2243 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m13690_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m13691_gshared (Collection_1_t2243 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m13691(__this, method) (( Object_t * (*) (Collection_1_t2243 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m13691_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m13692_gshared (Collection_1_t2243 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m13692(__this, method) (( bool (*) (Collection_1_t2243 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m13692_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m13693_gshared (Collection_1_t2243 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m13693(__this, method) (( bool (*) (Collection_1_t2243 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m13693_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m13694_gshared (Collection_1_t2243 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m13694(__this, ___index, method) (( Object_t * (*) (Collection_1_t2243 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m13694_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m13695_gshared (Collection_1_t2243 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m13695(__this, ___index, ___value, method) (( void (*) (Collection_1_t2243 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m13695_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void Collection_1_Add_m13696_gshared (Collection_1_t2243 * __this, RaycastResult_t169  ___item, const MethodInfo* method);
#define Collection_1_Add_m13696(__this, ___item, method) (( void (*) (Collection_1_t2243 *, RaycastResult_t169 , const MethodInfo*))Collection_1_Add_m13696_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void Collection_1_Clear_m13697_gshared (Collection_1_t2243 * __this, const MethodInfo* method);
#define Collection_1_Clear_m13697(__this, method) (( void (*) (Collection_1_t2243 *, const MethodInfo*))Collection_1_Clear_m13697_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::ClearItems()
extern "C" void Collection_1_ClearItems_m13698_gshared (Collection_1_t2243 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m13698(__this, method) (( void (*) (Collection_1_t2243 *, const MethodInfo*))Collection_1_ClearItems_m13698_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool Collection_1_Contains_m13699_gshared (Collection_1_t2243 * __this, RaycastResult_t169  ___item, const MethodInfo* method);
#define Collection_1_Contains_m13699(__this, ___item, method) (( bool (*) (Collection_1_t2243 *, RaycastResult_t169 , const MethodInfo*))Collection_1_Contains_m13699_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m13700_gshared (Collection_1_t2243 * __this, RaycastResultU5BU5D_t2238* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m13700(__this, ___array, ___index, method) (( void (*) (Collection_1_t2243 *, RaycastResultU5BU5D_t2238*, int32_t, const MethodInfo*))Collection_1_CopyTo_m13700_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m13701_gshared (Collection_1_t2243 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m13701(__this, method) (( Object_t* (*) (Collection_1_t2243 *, const MethodInfo*))Collection_1_GetEnumerator_m13701_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m13702_gshared (Collection_1_t2243 * __this, RaycastResult_t169  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m13702(__this, ___item, method) (( int32_t (*) (Collection_1_t2243 *, RaycastResult_t169 , const MethodInfo*))Collection_1_IndexOf_m13702_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m13703_gshared (Collection_1_t2243 * __this, int32_t ___index, RaycastResult_t169  ___item, const MethodInfo* method);
#define Collection_1_Insert_m13703(__this, ___index, ___item, method) (( void (*) (Collection_1_t2243 *, int32_t, RaycastResult_t169 , const MethodInfo*))Collection_1_Insert_m13703_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m13704_gshared (Collection_1_t2243 * __this, int32_t ___index, RaycastResult_t169  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m13704(__this, ___index, ___item, method) (( void (*) (Collection_1_t2243 *, int32_t, RaycastResult_t169 , const MethodInfo*))Collection_1_InsertItem_m13704_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool Collection_1_Remove_m13705_gshared (Collection_1_t2243 * __this, RaycastResult_t169  ___item, const MethodInfo* method);
#define Collection_1_Remove_m13705(__this, ___item, method) (( bool (*) (Collection_1_t2243 *, RaycastResult_t169 , const MethodInfo*))Collection_1_Remove_m13705_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m13706_gshared (Collection_1_t2243 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m13706(__this, ___index, method) (( void (*) (Collection_1_t2243 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m13706_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m13707_gshared (Collection_1_t2243 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m13707(__this, ___index, method) (( void (*) (Collection_1_t2243 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m13707_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t Collection_1_get_Count_m13708_gshared (Collection_1_t2243 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m13708(__this, method) (( int32_t (*) (Collection_1_t2243 *, const MethodInfo*))Collection_1_get_Count_m13708_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t169  Collection_1_get_Item_m13709_gshared (Collection_1_t2243 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m13709(__this, ___index, method) (( RaycastResult_t169  (*) (Collection_1_t2243 *, int32_t, const MethodInfo*))Collection_1_get_Item_m13709_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m13710_gshared (Collection_1_t2243 * __this, int32_t ___index, RaycastResult_t169  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m13710(__this, ___index, ___value, method) (( void (*) (Collection_1_t2243 *, int32_t, RaycastResult_t169 , const MethodInfo*))Collection_1_set_Item_m13710_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m13711_gshared (Collection_1_t2243 * __this, int32_t ___index, RaycastResult_t169  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m13711(__this, ___index, ___item, method) (( void (*) (Collection_1_t2243 *, int32_t, RaycastResult_t169 , const MethodInfo*))Collection_1_SetItem_m13711_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m13712_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m13712(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m13712_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::ConvertItem(System.Object)
extern "C" RaycastResult_t169  Collection_1_ConvertItem_m13713_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m13713(__this /* static, unused */, ___item, method) (( RaycastResult_t169  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m13713_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m13714_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m13714(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m13714_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m13715_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m13715(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m13715_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m13716_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m13716(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m13716_gshared)(__this /* static, unused */, ___list, method)
