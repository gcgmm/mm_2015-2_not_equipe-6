﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Stack
struct Stack_t669;

#include "System_System_Text_RegularExpressions_LinkRef.h"

// System.Text.RegularExpressions.LinkStack
struct  LinkStack_t1373  : public LinkRef_t1369
{
	// System.Collections.Stack System.Text.RegularExpressions.LinkStack::stack
	Stack_t669 * ___stack_0;
};
