﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.MeshFilter
struct MeshFilter_t114;
// UnityEngine.Mesh
struct Mesh_t120;

#include "codegen/il2cpp-codegen.h"

// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern "C" Mesh_t120 * MeshFilter_get_mesh_m2427 (MeshFilter_t114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
extern "C" void MeshFilter_set_mesh_m2428 (MeshFilter_t114 * __this, Mesh_t120 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C" Mesh_t120 * MeshFilter_get_sharedMesh_m281 (MeshFilter_t114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
extern "C" void MeshFilter_set_sharedMesh_m2429 (MeshFilter_t114 * __this, Mesh_t120 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
