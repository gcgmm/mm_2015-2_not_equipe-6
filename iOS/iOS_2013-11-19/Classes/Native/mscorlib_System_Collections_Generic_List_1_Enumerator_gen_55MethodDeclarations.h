﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t568;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_55.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m18227_gshared (Enumerator_t2584 * __this, List_1_t568 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m18227(__this, ___l, method) (( void (*) (Enumerator_t2584 *, List_1_t568 *, const MethodInfo*))Enumerator__ctor_m18227_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m18228_gshared (Enumerator_t2584 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m18228(__this, method) (( void (*) (Enumerator_t2584 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m18228_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18229_gshared (Enumerator_t2584 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18229(__this, method) (( Object_t * (*) (Enumerator_t2584 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18229_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C" void Enumerator_Dispose_m18230_gshared (Enumerator_t2584 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m18230(__this, method) (( void (*) (Enumerator_t2584 *, const MethodInfo*))Enumerator_Dispose_m18230_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m18231_gshared (Enumerator_t2584 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m18231(__this, method) (( void (*) (Enumerator_t2584 *, const MethodInfo*))Enumerator_VerifyState_m18231_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m18232_gshared (Enumerator_t2584 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m18232(__this, method) (( bool (*) (Enumerator_t2584 *, const MethodInfo*))Enumerator_MoveNext_m18232_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t430  Enumerator_get_Current_m18233_gshared (Enumerator_t2584 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m18233(__this, method) (( UICharInfo_t430  (*) (Enumerator_t2584 *, const MethodInfo*))Enumerator_get_Current_m18233_gshared)(__this, method)
