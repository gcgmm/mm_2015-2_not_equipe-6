﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.ClientContextTerminatorSink
struct ClientContextTerminatorSink_t1796;
// System.Runtime.Remoting.Contexts.Context
struct Context_t1772;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.ClientContextTerminatorSink::.ctor(System.Runtime.Remoting.Contexts.Context)
extern "C" void ClientContextTerminatorSink__ctor_m10647 (ClientContextTerminatorSink_t1796 * __this, Context_t1772 * ___ctx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
