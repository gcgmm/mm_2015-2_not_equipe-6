﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t345;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t3163;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t3164;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector3>
struct ICollection_1_t3165;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>
struct ReadOnlyCollection_1_t2481;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t121;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t2486;
// System.Comparison`1<UnityEngine.Vector3>
struct Comparison_1_t2489;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_50.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C" void List_1__ctor_m17000_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1__ctor_m17000(__this, method) (( void (*) (List_1_t345 *, const MethodInfo*))List_1__ctor_m17000_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m17001_gshared (List_1_t345 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m17001(__this, ___collection, method) (( void (*) (List_1_t345 *, Object_t*, const MethodInfo*))List_1__ctor_m17001_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Int32)
extern "C" void List_1__ctor_m17002_gshared (List_1_t345 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m17002(__this, ___capacity, method) (( void (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1__ctor_m17002_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.cctor()
extern "C" void List_1__cctor_m17003_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m17003(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m17003_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17004_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17004(__this, method) (( Object_t* (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17004_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17005_gshared (List_1_t345 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m17005(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t345 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m17005_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m17006_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17006(__this, method) (( Object_t * (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m17006_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m17007_gshared (List_1_t345 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m17007(__this, ___item, method) (( int32_t (*) (List_1_t345 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m17007_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m17008_gshared (List_1_t345 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m17008(__this, ___item, method) (( bool (*) (List_1_t345 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m17008_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m17009_gshared (List_1_t345 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m17009(__this, ___item, method) (( int32_t (*) (List_1_t345 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m17009_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m17010_gshared (List_1_t345 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m17010(__this, ___index, ___item, method) (( void (*) (List_1_t345 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m17010_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m17011_gshared (List_1_t345 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m17011(__this, ___item, method) (( void (*) (List_1_t345 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m17011_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17012_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17012(__this, method) (( bool (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17012_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m17013_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17013(__this, method) (( bool (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m17013_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m17014_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m17014(__this, method) (( Object_t * (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m17014_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m17015_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m17015(__this, method) (( bool (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m17015_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m17016_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m17016(__this, method) (( bool (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m17016_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m17017_gshared (List_1_t345 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m17017(__this, ___index, method) (( Object_t * (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m17017_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m17018_gshared (List_1_t345 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m17018(__this, ___index, ___value, method) (( void (*) (List_1_t345 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m17018_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(T)
extern "C" void List_1_Add_m17019_gshared (List_1_t345 * __this, Vector3_t12  ___item, const MethodInfo* method);
#define List_1_Add_m17019(__this, ___item, method) (( void (*) (List_1_t345 *, Vector3_t12 , const MethodInfo*))List_1_Add_m17019_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m17020_gshared (List_1_t345 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m17020(__this, ___newCount, method) (( void (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m17020_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m17021_gshared (List_1_t345 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m17021(__this, ___collection, method) (( void (*) (List_1_t345 *, Object_t*, const MethodInfo*))List_1_AddCollection_m17021_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m17022_gshared (List_1_t345 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m17022(__this, ___enumerable, method) (( void (*) (List_1_t345 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m17022_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m2298_gshared (List_1_t345 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m2298(__this, ___collection, method) (( void (*) (List_1_t345 *, Object_t*, const MethodInfo*))List_1_AddRange_m2298_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2481 * List_1_AsReadOnly_m17023_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m17023(__this, method) (( ReadOnlyCollection_1_t2481 * (*) (List_1_t345 *, const MethodInfo*))List_1_AsReadOnly_m17023_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
extern "C" void List_1_Clear_m17024_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_Clear_m17024(__this, method) (( void (*) (List_1_t345 *, const MethodInfo*))List_1_Clear_m17024_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool List_1_Contains_m17025_gshared (List_1_t345 * __this, Vector3_t12  ___item, const MethodInfo* method);
#define List_1_Contains_m17025(__this, ___item, method) (( bool (*) (List_1_t345 *, Vector3_t12 , const MethodInfo*))List_1_Contains_m17025_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m17026_gshared (List_1_t345 * __this, Vector3U5BU5D_t121* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m17026(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t345 *, Vector3U5BU5D_t121*, int32_t, const MethodInfo*))List_1_CopyTo_m17026_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::Find(System.Predicate`1<T>)
extern "C" Vector3_t12  List_1_Find_m17027_gshared (List_1_t345 * __this, Predicate_1_t2486 * ___match, const MethodInfo* method);
#define List_1_Find_m17027(__this, ___match, method) (( Vector3_t12  (*) (List_1_t345 *, Predicate_1_t2486 *, const MethodInfo*))List_1_Find_m17027_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m17028_gshared (Object_t * __this /* static, unused */, Predicate_1_t2486 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m17028(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2486 *, const MethodInfo*))List_1_CheckMatch_m17028_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m17029_gshared (List_1_t345 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2486 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m17029(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t345 *, int32_t, int32_t, Predicate_1_t2486 *, const MethodInfo*))List_1_GetIndex_m17029_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Enumerator_t2480  List_1_GetEnumerator_m17030_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m17030(__this, method) (( Enumerator_t2480  (*) (List_1_t345 *, const MethodInfo*))List_1_GetEnumerator_m17030_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m17031_gshared (List_1_t345 * __this, Vector3_t12  ___item, const MethodInfo* method);
#define List_1_IndexOf_m17031(__this, ___item, method) (( int32_t (*) (List_1_t345 *, Vector3_t12 , const MethodInfo*))List_1_IndexOf_m17031_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m17032_gshared (List_1_t345 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m17032(__this, ___start, ___delta, method) (( void (*) (List_1_t345 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m17032_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m17033_gshared (List_1_t345 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m17033(__this, ___index, method) (( void (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1_CheckIndex_m17033_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m17034_gshared (List_1_t345 * __this, int32_t ___index, Vector3_t12  ___item, const MethodInfo* method);
#define List_1_Insert_m17034(__this, ___index, ___item, method) (( void (*) (List_1_t345 *, int32_t, Vector3_t12 , const MethodInfo*))List_1_Insert_m17034_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m17035_gshared (List_1_t345 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m17035(__this, ___collection, method) (( void (*) (List_1_t345 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m17035_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool List_1_Remove_m17036_gshared (List_1_t345 * __this, Vector3_t12  ___item, const MethodInfo* method);
#define List_1_Remove_m17036(__this, ___item, method) (( bool (*) (List_1_t345 *, Vector3_t12 , const MethodInfo*))List_1_Remove_m17036_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m17037_gshared (List_1_t345 * __this, Predicate_1_t2486 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m17037(__this, ___match, method) (( int32_t (*) (List_1_t345 *, Predicate_1_t2486 *, const MethodInfo*))List_1_RemoveAll_m17037_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m17038_gshared (List_1_t345 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m17038(__this, ___index, method) (( void (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1_RemoveAt_m17038_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Reverse()
extern "C" void List_1_Reverse_m17039_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_Reverse_m17039(__this, method) (( void (*) (List_1_t345 *, const MethodInfo*))List_1_Reverse_m17039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort()
extern "C" void List_1_Sort_m17040_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_Sort_m17040(__this, method) (( void (*) (List_1_t345 *, const MethodInfo*))List_1_Sort_m17040_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m17041_gshared (List_1_t345 * __this, Comparison_1_t2489 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m17041(__this, ___comparison, method) (( void (*) (List_1_t345 *, Comparison_1_t2489 *, const MethodInfo*))List_1_Sort_m17041_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C" Vector3U5BU5D_t121* List_1_ToArray_m17042_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_ToArray_m17042(__this, method) (( Vector3U5BU5D_t121* (*) (List_1_t345 *, const MethodInfo*))List_1_ToArray_m17042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::TrimExcess()
extern "C" void List_1_TrimExcess_m17043_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m17043(__this, method) (( void (*) (List_1_t345 *, const MethodInfo*))List_1_TrimExcess_m17043_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m17044_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m17044(__this, method) (( int32_t (*) (List_1_t345 *, const MethodInfo*))List_1_get_Capacity_m17044_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m17045_gshared (List_1_t345 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m17045(__this, ___value, method) (( void (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1_set_Capacity_m17045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t List_1_get_Count_m17046_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_get_Count_m17046(__this, method) (( int32_t (*) (List_1_t345 *, const MethodInfo*))List_1_get_Count_m17046_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t12  List_1_get_Item_m17047_gshared (List_1_t345 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m17047(__this, ___index, method) (( Vector3_t12  (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1_get_Item_m17047_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m17048_gshared (List_1_t345 * __this, int32_t ___index, Vector3_t12  ___value, const MethodInfo* method);
#define List_1_set_Item_m17048(__this, ___index, ___value, method) (( void (*) (List_1_t345 *, int32_t, Vector3_t12 , const MethodInfo*))List_1_set_Item_m17048_gshared)(__this, ___index, ___value, method)
