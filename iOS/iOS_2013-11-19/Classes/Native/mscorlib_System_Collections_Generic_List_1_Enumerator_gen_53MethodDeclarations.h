﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t348;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_53.h"
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m17625_gshared (Enumerator_t2520 * __this, List_1_t348 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m17625(__this, ___l, method) (( void (*) (Enumerator_t2520 *, List_1_t348 *, const MethodInfo*))Enumerator__ctor_m17625_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m17626_gshared (Enumerator_t2520 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m17626(__this, method) (( void (*) (Enumerator_t2520 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m17626_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17627_gshared (Enumerator_t2520 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17627(__this, method) (( Object_t * (*) (Enumerator_t2520 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17627_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::Dispose()
extern "C" void Enumerator_Dispose_m17628_gshared (Enumerator_t2520 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17628(__this, method) (( void (*) (Enumerator_t2520 *, const MethodInfo*))Enumerator_Dispose_m17628_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::VerifyState()
extern "C" void Enumerator_VerifyState_m17629_gshared (Enumerator_t2520 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17629(__this, method) (( void (*) (Enumerator_t2520 *, const MethodInfo*))Enumerator_VerifyState_m17629_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17630_gshared (Enumerator_t2520 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17630(__this, method) (( bool (*) (Enumerator_t2520 *, const MethodInfo*))Enumerator_MoveNext_m17630_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::get_Current()
extern "C" Vector4_t350  Enumerator_get_Current_m17631_gshared (Enumerator_t2520 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17631(__this, method) (( Vector4_t350  (*) (Enumerator_t2520 *, const MethodInfo*))Enumerator_get_Current_m17631_gshared)(__this, method)
