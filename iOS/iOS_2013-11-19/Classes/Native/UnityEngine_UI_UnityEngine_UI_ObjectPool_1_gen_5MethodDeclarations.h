﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_1MethodDeclarations.h"

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m17722(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t2530 *, UnityAction_1_t2531 *, UnityAction_1_t2531 *, const MethodInfo*))ObjectPool_1__ctor_m13407_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::get_countAll()
#define ObjectPool_1_get_countAll_m17723(__this, method) (( int32_t (*) (ObjectPool_1_t2530 *, const MethodInfo*))ObjectPool_1_get_countAll_m13409_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m17724(__this, ___value, method) (( void (*) (ObjectPool_1_t2530 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m13411_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::get_countActive()
#define ObjectPool_1_get_countActive_m17725(__this, method) (( int32_t (*) (ObjectPool_1_t2530 *, const MethodInfo*))ObjectPool_1_get_countActive_m13413_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m17726(__this, method) (( int32_t (*) (ObjectPool_1_t2530 *, const MethodInfo*))ObjectPool_1_get_countInactive_m13415_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Get()
#define ObjectPool_1_Get_m17727(__this, method) (( List_1_t345 * (*) (ObjectPool_1_t2530 *, const MethodInfo*))ObjectPool_1_Get_m13417_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Release(T)
#define ObjectPool_1_Release_m17728(__this, ___element, method) (( void (*) (ObjectPool_1_t2530 *, List_1_t345 *, const MethodInfo*))ObjectPool_1_Release_m13419_gshared)(__this, ___element, method)
