﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m22976_gshared (KeyValuePair_2_t2881 * __this, int32_t ___key, TrackableResultData_t810  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m22976(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2881 *, int32_t, TrackableResultData_t810 , const MethodInfo*))KeyValuePair_2__ctor_m22976_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m22977_gshared (KeyValuePair_2_t2881 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m22977(__this, method) (( int32_t (*) (KeyValuePair_2_t2881 *, const MethodInfo*))KeyValuePair_2_get_Key_m22977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m22978_gshared (KeyValuePair_2_t2881 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m22978(__this, ___value, method) (( void (*) (KeyValuePair_2_t2881 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m22978_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Value()
extern "C" TrackableResultData_t810  KeyValuePair_2_get_Value_m22979_gshared (KeyValuePair_2_t2881 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m22979(__this, method) (( TrackableResultData_t810  (*) (KeyValuePair_2_t2881 *, const MethodInfo*))KeyValuePair_2_get_Value_m22979_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m22980_gshared (KeyValuePair_2_t2881 * __this, TrackableResultData_t810  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m22980(__this, ___value, method) (( void (*) (KeyValuePair_2_t2881 *, TrackableResultData_t810 , const MethodInfo*))KeyValuePair_2_set_Value_m22980_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m22981_gshared (KeyValuePair_2_t2881 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m22981(__this, method) (( String_t* (*) (KeyValuePair_2_t2881 *, const MethodInfo*))KeyValuePair_2_ToString_m22981_gshared)(__this, method)
