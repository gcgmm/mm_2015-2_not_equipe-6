﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`1<UnityEngine.Color>
struct InvokableCall_1_t2305;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t376;
// System.Object[]
struct ObjectU5BU5D_t105;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m14543_gshared (InvokableCall_1_t2305 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m14543(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t2305 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m14543_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m14544_gshared (InvokableCall_1_t2305 * __this, UnityAction_1_t376 * ___action, const MethodInfo* method);
#define InvokableCall_1__ctor_m14544(__this, ___action, method) (( void (*) (InvokableCall_1_t2305 *, UnityAction_1_t376 *, const MethodInfo*))InvokableCall_1__ctor_m14544_gshared)(__this, ___action, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m14545_gshared (InvokableCall_1_t2305 * __this, ObjectU5BU5D_t105* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m14545(__this, ___args, method) (( void (*) (InvokableCall_1_t2305 *, ObjectU5BU5D_t105*, const MethodInfo*))InvokableCall_1_Invoke_m14545_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m14546_gshared (InvokableCall_1_t2305 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m14546(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t2305 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m14546_gshared)(__this, ___targetObj, ___method, method)
