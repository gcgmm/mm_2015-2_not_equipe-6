﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.IPremiumObjectFactory
struct IPremiumObjectFactory_t808;

#include "codegen/il2cpp-codegen.h"

// Vuforia.IPremiumObjectFactory Vuforia.PremiumObjectFactory::get_Instance()
extern "C" Object_t * PremiumObjectFactory_get_Instance_m4114 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
