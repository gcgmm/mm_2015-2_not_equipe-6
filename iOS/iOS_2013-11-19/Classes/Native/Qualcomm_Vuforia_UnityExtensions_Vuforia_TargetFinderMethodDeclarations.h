﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.TargetFinder
struct TargetFinder_t795;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.TargetFinder::.ctor()
extern "C" void TargetFinder__ctor_m4907 (TargetFinder_t795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
