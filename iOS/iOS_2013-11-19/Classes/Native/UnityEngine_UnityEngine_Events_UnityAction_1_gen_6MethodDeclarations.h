﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_5MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m13572(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t2233 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m13437_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>::Invoke(T0)
#define UnityAction_1_Invoke_m13573(__this, ___arg0, method) (( void (*) (UnityAction_1_t2233 *, List_1_t128 *, const MethodInfo*))UnityAction_1_Invoke_m13438_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m13574(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t2233 *, List_1_t128 *, AsyncCallback_t261 *, Object_t *, const MethodInfo*))UnityAction_1_BeginInvoke_m13439_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m13575(__this, ___result, method) (( void (*) (UnityAction_1_t2233 *, Object_t *, const MethodInfo*))UnityAction_1_EndInvoke_m13440_gshared)(__this, ___result, method)
