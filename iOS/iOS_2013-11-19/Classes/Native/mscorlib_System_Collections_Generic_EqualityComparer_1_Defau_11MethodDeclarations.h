﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt16>
struct DefaultComparer_t2806;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt16>::.ctor()
extern "C" void DefaultComparer__ctor_m21356_gshared (DefaultComparer_t2806 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m21356(__this, method) (( void (*) (DefaultComparer_t2806 *, const MethodInfo*))DefaultComparer__ctor_m21356_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt16>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m21357_gshared (DefaultComparer_t2806 * __this, uint16_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m21357(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2806 *, uint16_t, const MethodInfo*))DefaultComparer_GetHashCode_m21357_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt16>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m21358_gshared (DefaultComparer_t2806 * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m21358(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2806 *, uint16_t, uint16_t, const MethodInfo*))DefaultComparer_Equals_m21358_gshared)(__this, ___x, ___y, method)
