﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_TimeSpan.h"

// System.TimeSpan
struct  TimeSpan_t975 
{
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;
};
struct TimeSpan_t975_StaticFields{
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t975  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t975  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t975  ___Zero_2;
};
