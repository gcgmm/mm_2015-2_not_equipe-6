﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t2933;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_43.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m23672_gshared (Enumerator_t2939 * __this, Dictionary_2_t2933 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m23672(__this, ___host, method) (( void (*) (Enumerator_t2939 *, Dictionary_2_t2933 *, const MethodInfo*))Enumerator__ctor_m23672_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23673_gshared (Enumerator_t2939 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m23673(__this, method) (( Object_t * (*) (Enumerator_t2939 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23673_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m23674_gshared (Enumerator_t2939 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m23674(__this, method) (( void (*) (Enumerator_t2939 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m23674_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void Enumerator_Dispose_m23675_gshared (Enumerator_t2939 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m23675(__this, method) (( void (*) (Enumerator_t2939 *, const MethodInfo*))Enumerator_Dispose_m23675_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m23676_gshared (Enumerator_t2939 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m23676(__this, method) (( bool (*) (Enumerator_t2939 *, const MethodInfo*))Enumerator_MoveNext_m23676_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m23677_gshared (Enumerator_t2939 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m23677(__this, method) (( Object_t * (*) (Enumerator_t2939 *, const MethodInfo*))Enumerator_get_Current_m23677_gshared)(__this, method)
