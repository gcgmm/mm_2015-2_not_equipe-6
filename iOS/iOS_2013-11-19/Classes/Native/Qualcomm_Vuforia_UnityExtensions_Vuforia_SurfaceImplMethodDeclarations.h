﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.SurfaceImpl
struct SurfaceImpl_t838;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t762;
// UnityEngine.Mesh
struct Mesh_t120;
// System.Int32[]
struct Int32U5BU5D_t122;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void Vuforia.SurfaceImpl::.ctor(System.Int32,Vuforia.SmartTerrainTrackable)
extern "C" void SurfaceImpl__ctor_m4181 (SurfaceImpl_t838 * __this, int32_t ___id, Object_t * ___parent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetMesh(System.Int32,UnityEngine.Mesh,UnityEngine.Mesh,System.Int32[])
extern "C" void SurfaceImpl_SetMesh_m4182 (SurfaceImpl_t838 * __this, int32_t ___meshRev, Mesh_t120 * ___mesh, Mesh_t120 * ___navMesh, Int32U5BU5D_t122* ___meshBoundaries, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetBoundingBox(UnityEngine.Rect)
extern "C" void SurfaceImpl_SetBoundingBox_m4183 (SurfaceImpl_t838 * __this, Rect_t94  ___boundingBox, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh Vuforia.SurfaceImpl::GetNavMesh()
extern "C" Mesh_t120 * SurfaceImpl_GetNavMesh_m4184 (SurfaceImpl_t838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
