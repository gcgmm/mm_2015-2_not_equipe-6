﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t2933;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_53.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m23710_gshared (Enumerator_t2943 * __this, Dictionary_2_t2933 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m23710(__this, ___host, method) (( void (*) (Enumerator_t2943 *, Dictionary_2_t2933 *, const MethodInfo*))Enumerator__ctor_m23710_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23711_gshared (Enumerator_t2943 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m23711(__this, method) (( Object_t * (*) (Enumerator_t2943 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23711_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m23712_gshared (Enumerator_t2943 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m23712(__this, method) (( void (*) (Enumerator_t2943 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m23712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void Enumerator_Dispose_m23713_gshared (Enumerator_t2943 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m23713(__this, method) (( void (*) (Enumerator_t2943 *, const MethodInfo*))Enumerator_Dispose_m23713_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m23714_gshared (Enumerator_t2943 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m23714(__this, method) (( bool (*) (Enumerator_t2943 *, const MethodInfo*))Enumerator_MoveNext_m23714_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" ProfileData_t898  Enumerator_get_Current_m23715_gshared (Enumerator_t2943 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m23715(__this, method) (( ProfileData_t898  (*) (Enumerator_t2943 *, const MethodInfo*))Enumerator_get_Current_m23715_gshared)(__this, method)
