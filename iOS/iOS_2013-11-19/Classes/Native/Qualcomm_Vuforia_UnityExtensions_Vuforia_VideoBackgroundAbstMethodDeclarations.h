﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VideoBackgroundAbstractBehaviour
struct VideoBackgroundAbstractBehaviour_t67;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
extern "C" void VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m5151 (VideoBackgroundAbstractBehaviour_t67 * __this, bool ___disable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::SetStereoDepth(System.Single)
extern "C" void VideoBackgroundAbstractBehaviour_SetStereoDepth_m5152 (VideoBackgroundAbstractBehaviour_t67 * __this, float ___depth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ApplyStereoDepthToMatrices()
extern "C" void VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m5153 (VideoBackgroundAbstractBehaviour_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::RenderOnUpdate()
extern "C" void VideoBackgroundAbstractBehaviour_RenderOnUpdate_m5154 (VideoBackgroundAbstractBehaviour_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Awake()
extern "C" void VideoBackgroundAbstractBehaviour_Awake_m5155 (VideoBackgroundAbstractBehaviour_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Start()
extern "C" void VideoBackgroundAbstractBehaviour_Start_m5156 (VideoBackgroundAbstractBehaviour_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPreRender()
extern "C" void VideoBackgroundAbstractBehaviour_OnPreRender_m5157 (VideoBackgroundAbstractBehaviour_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPostRender()
extern "C" void VideoBackgroundAbstractBehaviour_OnPostRender_m5158 (VideoBackgroundAbstractBehaviour_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnDestroy()
extern "C" void VideoBackgroundAbstractBehaviour_OnDestroy_m5159 (VideoBackgroundAbstractBehaviour_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::.ctor()
extern "C" void VideoBackgroundAbstractBehaviour__ctor_m261 (VideoBackgroundAbstractBehaviour_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
