﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.DatabaseLoadBehaviour
struct DatabaseLoadBehaviour_t24;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.DatabaseLoadBehaviour::.ctor()
extern "C" void DatabaseLoadBehaviour__ctor_m36 (DatabaseLoadBehaviour_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C" void DatabaseLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m37 (DatabaseLoadBehaviour_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
