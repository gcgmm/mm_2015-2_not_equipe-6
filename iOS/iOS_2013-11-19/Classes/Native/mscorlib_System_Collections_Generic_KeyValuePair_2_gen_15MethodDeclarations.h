﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m19105(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2650 *, Event_t269 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m19004_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m19106(__this, method) (( Event_t269 * (*) (KeyValuePair_2_t2650 *, const MethodInfo*))KeyValuePair_2_get_Key_m19005_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m19107(__this, ___value, method) (( void (*) (KeyValuePair_2_t2650 *, Event_t269 *, const MethodInfo*))KeyValuePair_2_set_Key_m19006_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m19108(__this, method) (( int32_t (*) (KeyValuePair_2_t2650 *, const MethodInfo*))KeyValuePair_2_get_Value_m19007_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m19109(__this, ___value, method) (( void (*) (KeyValuePair_2_t2650 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m19008_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m19110(__this, method) (( String_t* (*) (KeyValuePair_2_t2650 *, const MethodInfo*))KeyValuePair_2_ToString_m19009_gshared)(__this, method)
