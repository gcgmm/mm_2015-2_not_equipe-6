﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_63.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__4.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m20908_gshared (InternalEnumerator_1_t2771 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m20908(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2771 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m20908_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20909_gshared (InternalEnumerator_1_t2771 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20909(__this, method) (( void (*) (InternalEnumerator_1_t2771 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m20909_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20910_gshared (InternalEnumerator_1_t2771 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20910(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2771 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20910_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m20911_gshared (InternalEnumerator_1_t2771 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m20911(__this, method) (( void (*) (InternalEnumerator_1_t2771 *, const MethodInfo*))InternalEnumerator_1_Dispose_m20911_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m20912_gshared (InternalEnumerator_1_t2771 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m20912(__this, method) (( bool (*) (InternalEnumerator_1_t2771 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m20912_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::get_Current()
extern "C" WordResultData_t814  InternalEnumerator_1_get_Current_m20913_gshared (InternalEnumerator_1_t2771 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m20913(__this, method) (( WordResultData_t814  (*) (InternalEnumerator_1_t2771 *, const MethodInfo*))InternalEnumerator_1_get_Current_m20913_gshared)(__this, method)
