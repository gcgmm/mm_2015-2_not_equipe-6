﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t3078;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerable_1_t3265;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t3264;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ICollection_1_t2134;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ReadOnlyCollection_1_t2132;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2130;
// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct Predicate_1_t3082;
// System.Comparison`1<System.Reflection.CustomAttributeNamedArgument>
struct Comparison_1_t3085;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_64.h"

// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void List_1__ctor_m25203_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1__ctor_m25203(__this, method) (( void (*) (List_1_t3078 *, const MethodInfo*))List_1__ctor_m25203_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m25204_gshared (List_1_t3078 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m25204(__this, ___collection, method) (( void (*) (List_1_t3078 *, Object_t*, const MethodInfo*))List_1__ctor_m25204_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Int32)
extern "C" void List_1__ctor_m25205_gshared (List_1_t3078 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m25205(__this, ___capacity, method) (( void (*) (List_1_t3078 *, int32_t, const MethodInfo*))List_1__ctor_m25205_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern "C" void List_1__cctor_m25206_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m25206(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m25206_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25207_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25207(__this, method) (( Object_t* (*) (List_1_t3078 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25207_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m25208_gshared (List_1_t3078 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m25208(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3078 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m25208_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m25209_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m25209(__this, method) (( Object_t * (*) (List_1_t3078 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m25209_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m25210_gshared (List_1_t3078 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m25210(__this, ___item, method) (( int32_t (*) (List_1_t3078 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m25210_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m25211_gshared (List_1_t3078 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m25211(__this, ___item, method) (( bool (*) (List_1_t3078 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m25211_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m25212_gshared (List_1_t3078 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m25212(__this, ___item, method) (( int32_t (*) (List_1_t3078 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m25212_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m25213_gshared (List_1_t3078 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m25213(__this, ___index, ___item, method) (( void (*) (List_1_t3078 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m25213_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m25214_gshared (List_1_t3078 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m25214(__this, ___item, method) (( void (*) (List_1_t3078 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m25214_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25215_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25215(__this, method) (( bool (*) (List_1_t3078 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m25216_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m25216(__this, method) (( bool (*) (List_1_t3078 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m25216_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m25217_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m25217(__this, method) (( Object_t * (*) (List_1_t3078 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m25217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m25218_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m25218(__this, method) (( bool (*) (List_1_t3078 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m25218_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m25219_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m25219(__this, method) (( bool (*) (List_1_t3078 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m25219_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m25220_gshared (List_1_t3078 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m25220(__this, ___index, method) (( Object_t * (*) (List_1_t3078 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m25220_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m25221_gshared (List_1_t3078 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m25221(__this, ___index, ___value, method) (( void (*) (List_1_t3078 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m25221_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void List_1_Add_m25222_gshared (List_1_t3078 * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define List_1_Add_m25222(__this, ___item, method) (( void (*) (List_1_t3078 *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))List_1_Add_m25222_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m25223_gshared (List_1_t3078 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m25223(__this, ___newCount, method) (( void (*) (List_1_t3078 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m25223_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m25224_gshared (List_1_t3078 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m25224(__this, ___collection, method) (( void (*) (List_1_t3078 *, Object_t*, const MethodInfo*))List_1_AddCollection_m25224_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m25225_gshared (List_1_t3078 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m25225(__this, ___enumerable, method) (( void (*) (List_1_t3078 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m25225_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m25226_gshared (List_1_t3078 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m25226(__this, ___collection, method) (( void (*) (List_1_t3078 *, Object_t*, const MethodInfo*))List_1_AddRange_m25226_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2132 * List_1_AsReadOnly_m25227_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m25227(__this, method) (( ReadOnlyCollection_1_t2132 * (*) (List_1_t3078 *, const MethodInfo*))List_1_AsReadOnly_m25227_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void List_1_Clear_m25228_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_Clear_m25228(__this, method) (( void (*) (List_1_t3078 *, const MethodInfo*))List_1_Clear_m25228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool List_1_Contains_m25229_gshared (List_1_t3078 * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define List_1_Contains_m25229(__this, ___item, method) (( bool (*) (List_1_t3078 *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))List_1_Contains_m25229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m25230_gshared (List_1_t3078 * __this, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m25230(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3078 *, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, const MethodInfo*))List_1_CopyTo_m25230_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Find(System.Predicate`1<T>)
extern "C" CustomAttributeNamedArgument_t1691  List_1_Find_m25231_gshared (List_1_t3078 * __this, Predicate_1_t3082 * ___match, const MethodInfo* method);
#define List_1_Find_m25231(__this, ___match, method) (( CustomAttributeNamedArgument_t1691  (*) (List_1_t3078 *, Predicate_1_t3082 *, const MethodInfo*))List_1_Find_m25231_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m25232_gshared (Object_t * __this /* static, unused */, Predicate_1_t3082 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m25232(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3082 *, const MethodInfo*))List_1_CheckMatch_m25232_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m25233_gshared (List_1_t3078 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3082 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m25233(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t3078 *, int32_t, int32_t, Predicate_1_t3082 *, const MethodInfo*))List_1_GetIndex_m25233_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Enumerator_t3079  List_1_GetEnumerator_m25234_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m25234(__this, method) (( Enumerator_t3079  (*) (List_1_t3078 *, const MethodInfo*))List_1_GetEnumerator_m25234_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m25235_gshared (List_1_t3078 * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define List_1_IndexOf_m25235(__this, ___item, method) (( int32_t (*) (List_1_t3078 *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))List_1_IndexOf_m25235_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m25236_gshared (List_1_t3078 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m25236(__this, ___start, ___delta, method) (( void (*) (List_1_t3078 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m25236_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m25237_gshared (List_1_t3078 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m25237(__this, ___index, method) (( void (*) (List_1_t3078 *, int32_t, const MethodInfo*))List_1_CheckIndex_m25237_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m25238_gshared (List_1_t3078 * __this, int32_t ___index, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define List_1_Insert_m25238(__this, ___index, ___item, method) (( void (*) (List_1_t3078 *, int32_t, CustomAttributeNamedArgument_t1691 , const MethodInfo*))List_1_Insert_m25238_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m25239_gshared (List_1_t3078 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m25239(__this, ___collection, method) (( void (*) (List_1_t3078 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m25239_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool List_1_Remove_m25240_gshared (List_1_t3078 * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define List_1_Remove_m25240(__this, ___item, method) (( bool (*) (List_1_t3078 *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))List_1_Remove_m25240_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m25241_gshared (List_1_t3078 * __this, Predicate_1_t3082 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m25241(__this, ___match, method) (( int32_t (*) (List_1_t3078 *, Predicate_1_t3082 *, const MethodInfo*))List_1_RemoveAll_m25241_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m25242_gshared (List_1_t3078 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m25242(__this, ___index, method) (( void (*) (List_1_t3078 *, int32_t, const MethodInfo*))List_1_RemoveAt_m25242_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Reverse()
extern "C" void List_1_Reverse_m25243_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_Reverse_m25243(__this, method) (( void (*) (List_1_t3078 *, const MethodInfo*))List_1_Reverse_m25243_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Sort()
extern "C" void List_1_Sort_m25244_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_Sort_m25244(__this, method) (( void (*) (List_1_t3078 *, const MethodInfo*))List_1_Sort_m25244_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m25245_gshared (List_1_t3078 * __this, Comparison_1_t3085 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m25245(__this, ___comparison, method) (( void (*) (List_1_t3078 *, Comparison_1_t3085 *, const MethodInfo*))List_1_Sort_m25245_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::ToArray()
extern "C" CustomAttributeNamedArgumentU5BU5D_t2130* List_1_ToArray_m25246_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_ToArray_m25246(__this, method) (( CustomAttributeNamedArgumentU5BU5D_t2130* (*) (List_1_t3078 *, const MethodInfo*))List_1_ToArray_m25246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::TrimExcess()
extern "C" void List_1_TrimExcess_m25247_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m25247(__this, method) (( void (*) (List_1_t3078 *, const MethodInfo*))List_1_TrimExcess_m25247_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m25248_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m25248(__this, method) (( int32_t (*) (List_1_t3078 *, const MethodInfo*))List_1_get_Capacity_m25248_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m25249_gshared (List_1_t3078 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m25249(__this, ___value, method) (( void (*) (List_1_t3078 *, int32_t, const MethodInfo*))List_1_set_Capacity_m25249_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t List_1_get_Count_m25250_gshared (List_1_t3078 * __this, const MethodInfo* method);
#define List_1_get_Count_m25250(__this, method) (( int32_t (*) (List_1_t3078 *, const MethodInfo*))List_1_get_Count_m25250_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1691  List_1_get_Item_m25251_gshared (List_1_t3078 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m25251(__this, ___index, method) (( CustomAttributeNamedArgument_t1691  (*) (List_1_t3078 *, int32_t, const MethodInfo*))List_1_get_Item_m25251_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m25252_gshared (List_1_t3078 * __this, int32_t ___index, CustomAttributeNamedArgument_t1691  ___value, const MethodInfo* method);
#define List_1_set_Item_m25252(__this, ___index, ___value, method) (( void (*) (List_1_t3078 *, int32_t, CustomAttributeNamedArgument_t1691 , const MethodInfo*))List_1_set_Item_m25252_gshared)(__this, ___index, ___value, method)
