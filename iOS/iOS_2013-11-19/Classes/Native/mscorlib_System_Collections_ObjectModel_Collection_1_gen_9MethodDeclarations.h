﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>
struct Collection_1_t2728;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t2702;
// System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerator_1_t3203;
// System.Collections.Generic.IList`1<Vuforia.Image/PIXEL_FORMAT>
struct IList_1_t2727;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C" void Collection_1__ctor_m20178_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1__ctor_m20178(__this, method) (( void (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1__ctor_m20178_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20179_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20179(__this, method) (( bool (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20179_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m20180_gshared (Collection_1_t2728 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m20180(__this, ___array, ___index, method) (( void (*) (Collection_1_t2728 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m20180_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m20181_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m20181(__this, method) (( Object_t * (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m20181_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m20182_gshared (Collection_1_t2728 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m20182(__this, ___value, method) (( int32_t (*) (Collection_1_t2728 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m20182_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m20183_gshared (Collection_1_t2728 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m20183(__this, ___value, method) (( bool (*) (Collection_1_t2728 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m20183_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m20184_gshared (Collection_1_t2728 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m20184(__this, ___value, method) (( int32_t (*) (Collection_1_t2728 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m20184_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m20185_gshared (Collection_1_t2728 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m20185(__this, ___index, ___value, method) (( void (*) (Collection_1_t2728 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m20185_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m20186_gshared (Collection_1_t2728 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m20186(__this, ___value, method) (( void (*) (Collection_1_t2728 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m20186_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m20187_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m20187(__this, method) (( bool (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m20187_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m20188_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m20188(__this, method) (( Object_t * (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m20188_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m20189_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m20189(__this, method) (( bool (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m20189_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m20190_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m20190(__this, method) (( bool (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m20190_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m20191_gshared (Collection_1_t2728 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m20191(__this, ___index, method) (( Object_t * (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m20191_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m20192_gshared (Collection_1_t2728 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m20192(__this, ___index, ___value, method) (( void (*) (Collection_1_t2728 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m20192_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Add(T)
extern "C" void Collection_1_Add_m20193_gshared (Collection_1_t2728 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m20193(__this, ___item, method) (( void (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_Add_m20193_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Clear()
extern "C" void Collection_1_Clear_m20194_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_Clear_m20194(__this, method) (( void (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_Clear_m20194_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::ClearItems()
extern "C" void Collection_1_ClearItems_m20195_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m20195(__this, method) (( void (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_ClearItems_m20195_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Contains(T)
extern "C" bool Collection_1_Contains_m20196_gshared (Collection_1_t2728 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m20196(__this, ___item, method) (( bool (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_Contains_m20196_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m20197_gshared (Collection_1_t2728 * __this, PIXEL_FORMATU5BU5D_t2702* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m20197(__this, ___array, ___index, method) (( void (*) (Collection_1_t2728 *, PIXEL_FORMATU5BU5D_t2702*, int32_t, const MethodInfo*))Collection_1_CopyTo_m20197_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m20198_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m20198(__this, method) (( Object_t* (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_GetEnumerator_m20198_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m20199_gshared (Collection_1_t2728 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m20199(__this, ___item, method) (( int32_t (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m20199_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m20200_gshared (Collection_1_t2728 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m20200(__this, ___index, ___item, method) (( void (*) (Collection_1_t2728 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m20200_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m20201_gshared (Collection_1_t2728 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m20201(__this, ___index, ___item, method) (( void (*) (Collection_1_t2728 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m20201_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Remove(T)
extern "C" bool Collection_1_Remove_m20202_gshared (Collection_1_t2728 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m20202(__this, ___item, method) (( bool (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_Remove_m20202_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m20203_gshared (Collection_1_t2728 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m20203(__this, ___index, method) (( void (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m20203_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m20204_gshared (Collection_1_t2728 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m20204(__this, ___index, method) (( void (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m20204_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::get_Count()
extern "C" int32_t Collection_1_get_Count_m20205_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m20205(__this, method) (( int32_t (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_get_Count_m20205_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m20206_gshared (Collection_1_t2728 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m20206(__this, ___index, method) (( int32_t (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_get_Item_m20206_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m20207_gshared (Collection_1_t2728 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m20207(__this, ___index, ___value, method) (( void (*) (Collection_1_t2728 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m20207_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m20208_gshared (Collection_1_t2728 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m20208(__this, ___index, ___item, method) (( void (*) (Collection_1_t2728 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m20208_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m20209_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m20209(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m20209_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m20210_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m20210(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m20210_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m20211_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m20211(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m20211_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m20212_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m20212(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m20212_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m20213_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m20213(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m20213_gshared)(__this /* static, unused */, ___list, method)
