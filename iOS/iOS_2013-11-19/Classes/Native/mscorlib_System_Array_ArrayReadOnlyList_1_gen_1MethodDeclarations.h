﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t3086;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2130;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t3264;
// System.Exception
struct Exception_t107;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m25282_gshared (ArrayReadOnlyList_1_t3086 * __this, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m25282(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t3086 *, CustomAttributeNamedArgumentU5BU5D_t2130*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m25282_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m25283_gshared (ArrayReadOnlyList_1_t3086 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m25283(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t3086 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m25283_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1691  ArrayReadOnlyList_1_get_Item_m25284_gshared (ArrayReadOnlyList_1_t3086 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m25284(__this, ___index, method) (( CustomAttributeNamedArgument_t1691  (*) (ArrayReadOnlyList_1_t3086 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m25284_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m25285_gshared (ArrayReadOnlyList_1_t3086 * __this, int32_t ___index, CustomAttributeNamedArgument_t1691  ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m25285(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t3086 *, int32_t, CustomAttributeNamedArgument_t1691 , const MethodInfo*))ArrayReadOnlyList_1_set_Item_m25285_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m25286_gshared (ArrayReadOnlyList_1_t3086 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m25286(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t3086 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m25286_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m25287_gshared (ArrayReadOnlyList_1_t3086 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m25287(__this, method) (( bool (*) (ArrayReadOnlyList_1_t3086 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m25287_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m25288_gshared (ArrayReadOnlyList_1_t3086 * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m25288(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3086 *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))ArrayReadOnlyList_1_Add_m25288_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m25289_gshared (ArrayReadOnlyList_1_t3086 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m25289(__this, method) (( void (*) (ArrayReadOnlyList_1_t3086 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m25289_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m25290_gshared (ArrayReadOnlyList_1_t3086 * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m25290(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3086 *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))ArrayReadOnlyList_1_Contains_m25290_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m25291_gshared (ArrayReadOnlyList_1_t3086 * __this, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m25291(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3086 *, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m25291_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m25292_gshared (ArrayReadOnlyList_1_t3086 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m25292(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t3086 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m25292_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m25293_gshared (ArrayReadOnlyList_1_t3086 * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m25293(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t3086 *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m25293_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m25294_gshared (ArrayReadOnlyList_1_t3086 * __this, int32_t ___index, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m25294(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3086 *, int32_t, CustomAttributeNamedArgument_t1691 , const MethodInfo*))ArrayReadOnlyList_1_Insert_m25294_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m25295_gshared (ArrayReadOnlyList_1_t3086 * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m25295(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3086 *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))ArrayReadOnlyList_1_Remove_m25295_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m25296_gshared (ArrayReadOnlyList_1_t3086 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m25296(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3086 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m25296_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern "C" Exception_t107 * ArrayReadOnlyList_1_ReadOnlyError_m25297_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m25297(__this /* static, unused */, method) (( Exception_t107 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m25297_gshared)(__this /* static, unused */, method)
