﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t2245;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void DefaultComparer__ctor_m13722_gshared (DefaultComparer_t2245 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m13722(__this, method) (( void (*) (DefaultComparer_t2245 *, const MethodInfo*))DefaultComparer__ctor_m13722_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m13723_gshared (DefaultComparer_t2245 * __this, RaycastResult_t169  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m13723(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2245 *, RaycastResult_t169 , const MethodInfo*))DefaultComparer_GetHashCode_m13723_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m13724_gshared (DefaultComparer_t2245 * __this, RaycastResult_t169  ___x, RaycastResult_t169  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m13724(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2245 *, RaycastResult_t169 , RaycastResult_t169 , const MethodInfo*))DefaultComparer_Equals_m13724_gshared)(__this, ___x, ___y, method)
