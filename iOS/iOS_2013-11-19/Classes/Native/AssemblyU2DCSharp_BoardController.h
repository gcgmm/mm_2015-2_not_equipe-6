﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// RowController[]
struct RowControllerU5BU5D_t3;
// UnityEngine.Material[]
struct MaterialU5BU5D_t4;
// UnityEngine.Animator
struct Animator_t5;
// GameController
struct GameController_t6;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// BoardController
struct  BoardController_t1  : public MonoBehaviour_t2
{
	// RowController[] BoardController::rows
	RowControllerU5BU5D_t3* ___rows_2;
	// UnityEngine.Material[] BoardController::materials
	MaterialU5BU5D_t4* ___materials_3;
	// UnityEngine.Animator BoardController::anim
	Animator_t5 * ___anim_4;
	// System.Int32 BoardController::codeLength
	int32_t ___codeLength_5;
	// GameController BoardController::game
	GameController_t6 * ___game_6;
	// System.Int32 BoardController::rowIndex
	int32_t ___rowIndex_7;
	// UnityEngine.Material[] BoardController::code
	MaterialU5BU5D_t4* ___code_8;
};
