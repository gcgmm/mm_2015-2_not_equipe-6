﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_41.h"
#include "UnityEngine_UnityEngine_ContactPoint.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18054_gshared (InternalEnumerator_1_t2573 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18054(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2573 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18054_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18055_gshared (InternalEnumerator_1_t2573 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18055(__this, method) (( void (*) (InternalEnumerator_1_t2573 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18055_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18056_gshared (InternalEnumerator_1_t2573 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18056(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2573 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18056_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18057_gshared (InternalEnumerator_1_t2573 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18057(__this, method) (( void (*) (InternalEnumerator_1_t2573 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18057_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18058_gshared (InternalEnumerator_1_t2573 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18058(__this, method) (( bool (*) (InternalEnumerator_1_t2573 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18058_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::get_Current()
extern "C" ContactPoint_t534  InternalEnumerator_1_get_Current_m18059_gshared (InternalEnumerator_1_t2573 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18059(__this, method) (( ContactPoint_t534  (*) (InternalEnumerator_1_t2573 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18059_gshared)(__this, method)
