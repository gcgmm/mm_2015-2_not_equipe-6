﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>
struct Collection_1_t2483;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t121;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t3164;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t2482;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::.ctor()
extern "C" void Collection_1__ctor_m17086_gshared (Collection_1_t2483 * __this, const MethodInfo* method);
#define Collection_1__ctor_m17086(__this, method) (( void (*) (Collection_1_t2483 *, const MethodInfo*))Collection_1__ctor_m17086_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17087_gshared (Collection_1_t2483 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17087(__this, method) (( bool (*) (Collection_1_t2483 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17087_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m17088_gshared (Collection_1_t2483 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m17088(__this, ___array, ___index, method) (( void (*) (Collection_1_t2483 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m17088_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m17089_gshared (Collection_1_t2483 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m17089(__this, method) (( Object_t * (*) (Collection_1_t2483 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m17089_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m17090_gshared (Collection_1_t2483 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m17090(__this, ___value, method) (( int32_t (*) (Collection_1_t2483 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m17090_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m17091_gshared (Collection_1_t2483 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m17091(__this, ___value, method) (( bool (*) (Collection_1_t2483 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m17091_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m17092_gshared (Collection_1_t2483 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m17092(__this, ___value, method) (( int32_t (*) (Collection_1_t2483 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m17092_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m17093_gshared (Collection_1_t2483 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m17093(__this, ___index, ___value, method) (( void (*) (Collection_1_t2483 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m17093_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m17094_gshared (Collection_1_t2483 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m17094(__this, ___value, method) (( void (*) (Collection_1_t2483 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m17094_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m17095_gshared (Collection_1_t2483 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m17095(__this, method) (( bool (*) (Collection_1_t2483 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m17095_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m17096_gshared (Collection_1_t2483 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m17096(__this, method) (( Object_t * (*) (Collection_1_t2483 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m17096_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m17097_gshared (Collection_1_t2483 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m17097(__this, method) (( bool (*) (Collection_1_t2483 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m17097_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m17098_gshared (Collection_1_t2483 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m17098(__this, method) (( bool (*) (Collection_1_t2483 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m17098_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m17099_gshared (Collection_1_t2483 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m17099(__this, ___index, method) (( Object_t * (*) (Collection_1_t2483 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m17099_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m17100_gshared (Collection_1_t2483 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m17100(__this, ___index, ___value, method) (( void (*) (Collection_1_t2483 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m17100_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Add(T)
extern "C" void Collection_1_Add_m17101_gshared (Collection_1_t2483 * __this, Vector3_t12  ___item, const MethodInfo* method);
#define Collection_1_Add_m17101(__this, ___item, method) (( void (*) (Collection_1_t2483 *, Vector3_t12 , const MethodInfo*))Collection_1_Add_m17101_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Clear()
extern "C" void Collection_1_Clear_m17102_gshared (Collection_1_t2483 * __this, const MethodInfo* method);
#define Collection_1_Clear_m17102(__this, method) (( void (*) (Collection_1_t2483 *, const MethodInfo*))Collection_1_Clear_m17102_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ClearItems()
extern "C" void Collection_1_ClearItems_m17103_gshared (Collection_1_t2483 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m17103(__this, method) (( void (*) (Collection_1_t2483 *, const MethodInfo*))Collection_1_ClearItems_m17103_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool Collection_1_Contains_m17104_gshared (Collection_1_t2483 * __this, Vector3_t12  ___item, const MethodInfo* method);
#define Collection_1_Contains_m17104(__this, ___item, method) (( bool (*) (Collection_1_t2483 *, Vector3_t12 , const MethodInfo*))Collection_1_Contains_m17104_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m17105_gshared (Collection_1_t2483 * __this, Vector3U5BU5D_t121* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m17105(__this, ___array, ___index, method) (( void (*) (Collection_1_t2483 *, Vector3U5BU5D_t121*, int32_t, const MethodInfo*))Collection_1_CopyTo_m17105_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m17106_gshared (Collection_1_t2483 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m17106(__this, method) (( Object_t* (*) (Collection_1_t2483 *, const MethodInfo*))Collection_1_GetEnumerator_m17106_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m17107_gshared (Collection_1_t2483 * __this, Vector3_t12  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m17107(__this, ___item, method) (( int32_t (*) (Collection_1_t2483 *, Vector3_t12 , const MethodInfo*))Collection_1_IndexOf_m17107_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m17108_gshared (Collection_1_t2483 * __this, int32_t ___index, Vector3_t12  ___item, const MethodInfo* method);
#define Collection_1_Insert_m17108(__this, ___index, ___item, method) (( void (*) (Collection_1_t2483 *, int32_t, Vector3_t12 , const MethodInfo*))Collection_1_Insert_m17108_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m17109_gshared (Collection_1_t2483 * __this, int32_t ___index, Vector3_t12  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m17109(__this, ___index, ___item, method) (( void (*) (Collection_1_t2483 *, int32_t, Vector3_t12 , const MethodInfo*))Collection_1_InsertItem_m17109_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool Collection_1_Remove_m17110_gshared (Collection_1_t2483 * __this, Vector3_t12  ___item, const MethodInfo* method);
#define Collection_1_Remove_m17110(__this, ___item, method) (( bool (*) (Collection_1_t2483 *, Vector3_t12 , const MethodInfo*))Collection_1_Remove_m17110_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m17111_gshared (Collection_1_t2483 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m17111(__this, ___index, method) (( void (*) (Collection_1_t2483 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m17111_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m17112_gshared (Collection_1_t2483 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m17112(__this, ___index, method) (( void (*) (Collection_1_t2483 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m17112_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t Collection_1_get_Count_m17113_gshared (Collection_1_t2483 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m17113(__this, method) (( int32_t (*) (Collection_1_t2483 *, const MethodInfo*))Collection_1_get_Count_m17113_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t12  Collection_1_get_Item_m17114_gshared (Collection_1_t2483 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m17114(__this, ___index, method) (( Vector3_t12  (*) (Collection_1_t2483 *, int32_t, const MethodInfo*))Collection_1_get_Item_m17114_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m17115_gshared (Collection_1_t2483 * __this, int32_t ___index, Vector3_t12  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m17115(__this, ___index, ___value, method) (( void (*) (Collection_1_t2483 *, int32_t, Vector3_t12 , const MethodInfo*))Collection_1_set_Item_m17115_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m17116_gshared (Collection_1_t2483 * __this, int32_t ___index, Vector3_t12  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m17116(__this, ___index, ___item, method) (( void (*) (Collection_1_t2483 *, int32_t, Vector3_t12 , const MethodInfo*))Collection_1_SetItem_m17116_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m17117_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m17117(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m17117_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ConvertItem(System.Object)
extern "C" Vector3_t12  Collection_1_ConvertItem_m17118_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m17118(__this /* static, unused */, ___item, method) (( Vector3_t12  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m17118_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m17119_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m17119(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m17119_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m17120_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m17120(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m17120_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m17121_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m17121(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m17121_gshared)(__this /* static, unused */, ___list, method)
