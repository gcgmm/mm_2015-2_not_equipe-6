﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_36.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18017_gshared (InternalEnumerator_1_t2567 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18017(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2567 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18017_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18018_gshared (InternalEnumerator_1_t2567 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18018(__this, method) (( void (*) (InternalEnumerator_1_t2567 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18018_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18019_gshared (InternalEnumerator_1_t2567 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18019(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2567 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18019_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18020_gshared (InternalEnumerator_1_t2567 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18020(__this, method) (( void (*) (InternalEnumerator_1_t2567 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18020_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18021_gshared (InternalEnumerator_1_t2567 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18021(__this, method) (( bool (*) (InternalEnumerator_1_t2567 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18021_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern "C" GcScoreData_t614  InternalEnumerator_1_get_Current_m18022_gshared (InternalEnumerator_1_t2567 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18022(__this, method) (( GcScoreData_t614  (*) (InternalEnumerator_1_t2567 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18022_gshared)(__this, method)
