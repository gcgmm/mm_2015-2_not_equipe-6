﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_t2379;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m15479_gshared (U3CStartU3Ec__Iterator0_t2379 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0__ctor_m15479(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2379 *, const MethodInfo*))U3CStartU3Ec__Iterator0__ctor_m15479_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15480_gshared (U3CStartU3Ec__Iterator0_t2379 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15480(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2379 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15480_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m15481_gshared (U3CStartU3Ec__Iterator0_t2379 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m15481(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2379 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m15481_gshared)(__this, method)
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m15482_gshared (U3CStartU3Ec__Iterator0_t2379 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_MoveNext_m15482(__this, method) (( bool (*) (U3CStartU3Ec__Iterator0_t2379 *, const MethodInfo*))U3CStartU3Ec__Iterator0_MoveNext_m15482_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m15483_gshared (U3CStartU3Ec__Iterator0_t2379 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Dispose_m15483(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2379 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Dispose_m15483_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
extern "C" void U3CStartU3Ec__Iterator0_Reset_m15484_gshared (U3CStartU3Ec__Iterator0_t2379 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Reset_m15484(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2379 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Reset_m15484_gshared)(__this, method)
