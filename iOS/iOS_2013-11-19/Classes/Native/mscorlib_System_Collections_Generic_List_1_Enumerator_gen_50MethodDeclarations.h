﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t345;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_50.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m17049_gshared (Enumerator_t2480 * __this, List_1_t345 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m17049(__this, ___l, method) (( void (*) (Enumerator_t2480 *, List_1_t345 *, const MethodInfo*))Enumerator__ctor_m17049_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m17050_gshared (Enumerator_t2480 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m17050(__this, method) (( void (*) (Enumerator_t2480 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m17050_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17051_gshared (Enumerator_t2480 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17051(__this, method) (( Object_t * (*) (Enumerator_t2480 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C" void Enumerator_Dispose_m17052_gshared (Enumerator_t2480 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17052(__this, method) (( void (*) (Enumerator_t2480 *, const MethodInfo*))Enumerator_Dispose_m17052_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::VerifyState()
extern "C" void Enumerator_VerifyState_m17053_gshared (Enumerator_t2480 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17053(__this, method) (( void (*) (Enumerator_t2480 *, const MethodInfo*))Enumerator_VerifyState_m17053_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17054_gshared (Enumerator_t2480 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17054(__this, method) (( bool (*) (Enumerator_t2480 *, const MethodInfo*))Enumerator_MoveNext_m17054_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C" Vector3_t12  Enumerator_get_Current_m17055_gshared (Enumerator_t2480 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17055(__this, method) (( Vector3_t12  (*) (Enumerator_t2480 *, const MethodInfo*))Enumerator_get_Current_m17055_gshared)(__this, method)
