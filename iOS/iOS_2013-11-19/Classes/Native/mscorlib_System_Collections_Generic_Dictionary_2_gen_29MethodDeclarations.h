﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2309;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2311;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t687;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3134;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerator_1_t3135;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1238;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>
struct KeyCollection_t2314;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int32>
struct ValueCollection_t2318;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m14589_gshared (Dictionary_2_t2309 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m14589(__this, method) (( void (*) (Dictionary_2_t2309 *, const MethodInfo*))Dictionary_2__ctor_m14589_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m14590_gshared (Dictionary_2_t2309 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m14590(__this, ___comparer, method) (( void (*) (Dictionary_2_t2309 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14590_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m14591_gshared (Dictionary_2_t2309 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m14591(__this, ___capacity, method) (( void (*) (Dictionary_2_t2309 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m14591_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m14592_gshared (Dictionary_2_t2309 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m14592(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2309 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2__ctor_m14592_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m14593_gshared (Dictionary_2_t2309 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m14593(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2309 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m14593_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m14594_gshared (Dictionary_2_t2309 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m14594(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2309 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m14594_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m14595_gshared (Dictionary_2_t2309 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m14595(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2309 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m14595_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m14596_gshared (Dictionary_2_t2309 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m14596(__this, ___key, method) (( bool (*) (Dictionary_2_t2309 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m14596_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m14597_gshared (Dictionary_2_t2309 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m14597(__this, ___key, method) (( void (*) (Dictionary_2_t2309 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m14597_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14598_gshared (Dictionary_2_t2309 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14598(__this, method) (( bool (*) (Dictionary_2_t2309 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14598_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14599_gshared (Dictionary_2_t2309 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14599(__this, method) (( Object_t * (*) (Dictionary_2_t2309 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14599_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14600_gshared (Dictionary_2_t2309 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14600(__this, method) (( bool (*) (Dictionary_2_t2309 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14600_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14601_gshared (Dictionary_2_t2309 * __this, KeyValuePair_2_t2312  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14601(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2309 *, KeyValuePair_2_t2312 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14601_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14602_gshared (Dictionary_2_t2309 * __this, KeyValuePair_2_t2312  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14602(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2309 *, KeyValuePair_2_t2312 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14602_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14603_gshared (Dictionary_2_t2309 * __this, KeyValuePair_2U5BU5D_t3134* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14603(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2309 *, KeyValuePair_2U5BU5D_t3134*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14603_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14604_gshared (Dictionary_2_t2309 * __this, KeyValuePair_2_t2312  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14604(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2309 *, KeyValuePair_2_t2312 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14604_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m14605_gshared (Dictionary_2_t2309 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m14605(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2309 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m14605_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14606_gshared (Dictionary_2_t2309 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14606(__this, method) (( Object_t * (*) (Dictionary_2_t2309 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14606_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14607_gshared (Dictionary_2_t2309 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14607(__this, method) (( Object_t* (*) (Dictionary_2_t2309 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14607_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14608_gshared (Dictionary_2_t2309 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14608(__this, method) (( Object_t * (*) (Dictionary_2_t2309 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14608_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m14609_gshared (Dictionary_2_t2309 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m14609(__this, method) (( int32_t (*) (Dictionary_2_t2309 *, const MethodInfo*))Dictionary_2_get_Count_m14609_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m14610_gshared (Dictionary_2_t2309 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m14610(__this, ___key, method) (( int32_t (*) (Dictionary_2_t2309 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m14610_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m14611_gshared (Dictionary_2_t2309 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m14611(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2309 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m14611_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m14612_gshared (Dictionary_2_t2309 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m14612(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2309 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m14612_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m14613_gshared (Dictionary_2_t2309 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m14613(__this, ___size, method) (( void (*) (Dictionary_2_t2309 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m14613_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m14614_gshared (Dictionary_2_t2309 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m14614(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2309 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m14614_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2312  Dictionary_2_make_pair_m14615_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m14615(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2312  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m14615_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m14616_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m14616(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m14616_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m14617_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m14617(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m14617_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m14618_gshared (Dictionary_2_t2309 * __this, KeyValuePair_2U5BU5D_t3134* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m14618(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2309 *, KeyValuePair_2U5BU5D_t3134*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m14618_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m14619_gshared (Dictionary_2_t2309 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m14619(__this, method) (( void (*) (Dictionary_2_t2309 *, const MethodInfo*))Dictionary_2_Resize_m14619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m14620_gshared (Dictionary_2_t2309 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m14620(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2309 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m14620_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m14621_gshared (Dictionary_2_t2309 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m14621(__this, method) (( void (*) (Dictionary_2_t2309 *, const MethodInfo*))Dictionary_2_Clear_m14621_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m14622_gshared (Dictionary_2_t2309 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m14622(__this, ___key, method) (( bool (*) (Dictionary_2_t2309 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m14622_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m14623_gshared (Dictionary_2_t2309 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m14623(__this, ___value, method) (( bool (*) (Dictionary_2_t2309 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m14623_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m14624_gshared (Dictionary_2_t2309 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m14624(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2309 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2_GetObjectData_m14624_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m14625_gshared (Dictionary_2_t2309 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m14625(__this, ___sender, method) (( void (*) (Dictionary_2_t2309 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m14625_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m14626_gshared (Dictionary_2_t2309 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m14626(__this, ___key, method) (( bool (*) (Dictionary_2_t2309 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m14626_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m14627_gshared (Dictionary_2_t2309 * __this, Object_t * ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m14627(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2309 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m14627_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Keys()
extern "C" KeyCollection_t2314 * Dictionary_2_get_Keys_m14628_gshared (Dictionary_2_t2309 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m14628(__this, method) (( KeyCollection_t2314 * (*) (Dictionary_2_t2309 *, const MethodInfo*))Dictionary_2_get_Keys_m14628_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Values()
extern "C" ValueCollection_t2318 * Dictionary_2_get_Values_m14629_gshared (Dictionary_2_t2309 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m14629(__this, method) (( ValueCollection_t2318 * (*) (Dictionary_2_t2309 *, const MethodInfo*))Dictionary_2_get_Values_m14629_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m14630_gshared (Dictionary_2_t2309 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m14630(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2309 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m14630_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m14631_gshared (Dictionary_2_t2309 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m14631(__this, ___value, method) (( int32_t (*) (Dictionary_2_t2309 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m14631_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m14632_gshared (Dictionary_2_t2309 * __this, KeyValuePair_2_t2312  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m14632(__this, ___pair, method) (( bool (*) (Dictionary_2_t2309 *, KeyValuePair_2_t2312 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m14632_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetEnumerator()
extern "C" Enumerator_t2316  Dictionary_2_GetEnumerator_m14633_gshared (Dictionary_2_t2309 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m14633(__this, method) (( Enumerator_t2316  (*) (Dictionary_2_t2309 *, const MethodInfo*))Dictionary_2_GetEnumerator_m14633_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1423  Dictionary_2_U3CCopyToU3Em__0_m14634_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m14634(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1423  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m14634_gshared)(__this /* static, unused */, ___key, ___value, method)
