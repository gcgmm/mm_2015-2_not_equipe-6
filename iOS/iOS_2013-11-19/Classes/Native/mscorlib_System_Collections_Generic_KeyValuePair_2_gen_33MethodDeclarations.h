﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_32MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m23747(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2949 *, String_t*, ProfileData_t898 , const MethodInfo*))KeyValuePair_2__ctor_m23646_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Key()
#define KeyValuePair_2_get_Key_m23748(__this, method) (( String_t* (*) (KeyValuePair_2_t2949 *, const MethodInfo*))KeyValuePair_2_get_Key_m23647_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m23749(__this, ___value, method) (( void (*) (KeyValuePair_2_t2949 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m23648_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Value()
#define KeyValuePair_2_get_Value_m23750(__this, method) (( ProfileData_t898  (*) (KeyValuePair_2_t2949 *, const MethodInfo*))KeyValuePair_2_get_Value_m23649_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m23751(__this, ___value, method) (( void (*) (KeyValuePair_2_t2949 *, ProfileData_t898 , const MethodInfo*))KeyValuePair_2_set_Value_m23650_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::ToString()
#define KeyValuePair_2_ToString_m23752(__this, method) (( String_t* (*) (KeyValuePair_2_t2949 *, const MethodInfo*))KeyValuePair_2_ToString_m23651_gshared)(__this, method)
