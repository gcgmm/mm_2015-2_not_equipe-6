﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t2981;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t687;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2460;
// System.Object[]
struct ObjectU5BU5D_t105;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2311;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0.h"

// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C" void HashSet_1__ctor_m24251_gshared (HashSet_1_t2981 * __this, const MethodInfo* method);
#define HashSet_1__ctor_m24251(__this, method) (( void (*) (HashSet_1_t2981 *, const MethodInfo*))HashSet_1__ctor_m24251_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1__ctor_m24253_gshared (HashSet_1_t2981 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define HashSet_1__ctor_m24253(__this, ___info, ___context, method) (( void (*) (HashSet_1_t2981 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))HashSet_1__ctor_m24253_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24255_gshared (HashSet_1_t2981 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24255(__this, method) (( Object_t* (*) (HashSet_1_t2981 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24255_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24257_gshared (HashSet_1_t2981 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24257(__this, method) (( bool (*) (HashSet_1_t2981 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24257_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m24259_gshared (HashSet_1_t2981 * __this, ObjectU5BU5D_t105* ___array, int32_t ___index, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m24259(__this, ___array, ___index, method) (( void (*) (HashSet_1_t2981 *, ObjectU5BU5D_t105*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m24259_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24261_gshared (HashSet_1_t2981 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24261(__this, ___item, method) (( void (*) (HashSet_1_t2981 *, Object_t *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24261_gshared)(__this, ___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m24263_gshared (HashSet_1_t2981 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m24263(__this, method) (( Object_t * (*) (HashSet_1_t2981 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m24263_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::get_Count()
extern "C" int32_t HashSet_1_get_Count_m24265_gshared (HashSet_1_t2981 * __this, const MethodInfo* method);
#define HashSet_1_get_Count_m24265(__this, method) (( int32_t (*) (HashSet_1_t2981 *, const MethodInfo*))HashSet_1_get_Count_m24265_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C" void HashSet_1_Init_m24267_gshared (HashSet_1_t2981 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define HashSet_1_Init_m24267(__this, ___capacity, ___comparer, method) (( void (*) (HashSet_1_t2981 *, int32_t, Object_t*, const MethodInfo*))HashSet_1_Init_m24267_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::InitArrays(System.Int32)
extern "C" void HashSet_1_InitArrays_m24269_gshared (HashSet_1_t2981 * __this, int32_t ___size, const MethodInfo* method);
#define HashSet_1_InitArrays_m24269(__this, ___size, method) (( void (*) (HashSet_1_t2981 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m24269_gshared)(__this, ___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C" bool HashSet_1_SlotsContainsAt_m24271_gshared (HashSet_1_t2981 * __this, int32_t ___index, int32_t ___hash, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m24271(__this, ___index, ___hash, ___item, method) (( bool (*) (HashSet_1_t2981 *, int32_t, int32_t, Object_t *, const MethodInfo*))HashSet_1_SlotsContainsAt_m24271_gshared)(__this, ___index, ___hash, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void HashSet_1_CopyTo_m24273_gshared (HashSet_1_t2981 * __this, ObjectU5BU5D_t105* ___array, int32_t ___index, const MethodInfo* method);
#define HashSet_1_CopyTo_m24273(__this, ___array, ___index, method) (( void (*) (HashSet_1_t2981 *, ObjectU5BU5D_t105*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m24273_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32,System.Int32)
extern "C" void HashSet_1_CopyTo_m24275_gshared (HashSet_1_t2981 * __this, ObjectU5BU5D_t105* ___array, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define HashSet_1_CopyTo_m24275(__this, ___array, ___index, ___count, method) (( void (*) (HashSet_1_t2981 *, ObjectU5BU5D_t105*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m24275_gshared)(__this, ___array, ___index, ___count, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Resize()
extern "C" void HashSet_1_Resize_m24277_gshared (HashSet_1_t2981 * __this, const MethodInfo* method);
#define HashSet_1_Resize_m24277(__this, method) (( void (*) (HashSet_1_t2981 *, const MethodInfo*))HashSet_1_Resize_m24277_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetLinkHashCode(System.Int32)
extern "C" int32_t HashSet_1_GetLinkHashCode_m24279_gshared (HashSet_1_t2981 * __this, int32_t ___index, const MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m24279(__this, ___index, method) (( int32_t (*) (HashSet_1_t2981 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m24279_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetItemHashCode(T)
extern "C" int32_t HashSet_1_GetItemHashCode_m24281_gshared (HashSet_1_t2981 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_GetItemHashCode_m24281(__this, ___item, method) (( int32_t (*) (HashSet_1_t2981 *, Object_t *, const MethodInfo*))HashSet_1_GetItemHashCode_m24281_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(T)
extern "C" bool HashSet_1_Add_m24282_gshared (HashSet_1_t2981 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Add_m24282(__this, ___item, method) (( bool (*) (HashSet_1_t2981 *, Object_t *, const MethodInfo*))HashSet_1_Add_m24282_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Clear()
extern "C" void HashSet_1_Clear_m24284_gshared (HashSet_1_t2981 * __this, const MethodInfo* method);
#define HashSet_1_Clear_m24284(__this, method) (( void (*) (HashSet_1_t2981 *, const MethodInfo*))HashSet_1_Clear_m24284_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(T)
extern "C" bool HashSet_1_Contains_m24286_gshared (HashSet_1_t2981 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Contains_m24286(__this, ___item, method) (( bool (*) (HashSet_1_t2981 *, Object_t *, const MethodInfo*))HashSet_1_Contains_m24286_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(T)
extern "C" bool HashSet_1_Remove_m24288_gshared (HashSet_1_t2981 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Remove_m24288(__this, ___item, method) (( bool (*) (HashSet_1_t2981 *, Object_t *, const MethodInfo*))HashSet_1_Remove_m24288_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1_GetObjectData_m24290_gshared (HashSet_1_t2981 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define HashSet_1_GetObjectData_m24290(__this, ___info, ___context, method) (( void (*) (HashSet_1_t2981 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))HashSet_1_GetObjectData_m24290_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::OnDeserialization(System.Object)
extern "C" void HashSet_1_OnDeserialization_m24292_gshared (HashSet_1_t2981 * __this, Object_t * ___sender, const MethodInfo* method);
#define HashSet_1_OnDeserialization_m24292(__this, ___sender, method) (( void (*) (HashSet_1_t2981 *, Object_t *, const MethodInfo*))HashSet_1_OnDeserialization_m24292_gshared)(__this, ___sender, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2985  HashSet_1_GetEnumerator_m24293_gshared (HashSet_1_t2981 * __this, const MethodInfo* method);
#define HashSet_1_GetEnumerator_m24293(__this, method) (( Enumerator_t2985  (*) (HashSet_1_t2981 *, const MethodInfo*))HashSet_1_GetEnumerator_m24293_gshared)(__this, method)
