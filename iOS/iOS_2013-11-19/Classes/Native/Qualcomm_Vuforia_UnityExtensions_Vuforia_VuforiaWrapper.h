﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.IVuforiaWrapper
struct IVuforiaWrapper_t868;

#include "mscorlib_System_Object.h"

// Vuforia.VuforiaWrapper
struct  VuforiaWrapper_t867  : public Object_t
{
};
struct VuforiaWrapper_t867_StaticFields{
	// Vuforia.IVuforiaWrapper Vuforia.VuforiaWrapper::sWrapper
	Object_t * ___sWrapper_0;
};
