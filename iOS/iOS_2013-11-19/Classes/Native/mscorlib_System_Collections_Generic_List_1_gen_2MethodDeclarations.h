﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t177;
// System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerable_1_t3126;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t3127;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.RaycastResult>
struct ICollection_1_t3128;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>
struct ReadOnlyCollection_1_t2241;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2238;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t2246;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t138;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_31.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void List_1__ctor_m1762_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1__ctor_m1762(__this, method) (( void (*) (List_1_t177 *, const MethodInfo*))List_1__ctor_m1762_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m13590_gshared (List_1_t177 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m13590(__this, ___collection, method) (( void (*) (List_1_t177 *, Object_t*, const MethodInfo*))List_1__ctor_m13590_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m13591_gshared (List_1_t177 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m13591(__this, ___capacity, method) (( void (*) (List_1_t177 *, int32_t, const MethodInfo*))List_1__ctor_m13591_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern "C" void List_1__cctor_m13592_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m13592(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13592_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13593_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13593(__this, method) (( Object_t* (*) (List_1_t177 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13593_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m13594_gshared (List_1_t177 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m13594(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t177 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13594_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m13595_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m13595(__this, method) (( Object_t * (*) (List_1_t177 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13595_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m13596_gshared (List_1_t177 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m13596(__this, ___item, method) (( int32_t (*) (List_1_t177 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13596_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m13597_gshared (List_1_t177 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m13597(__this, ___item, method) (( bool (*) (List_1_t177 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13597_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m13598_gshared (List_1_t177 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m13598(__this, ___item, method) (( int32_t (*) (List_1_t177 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13598_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m13599_gshared (List_1_t177 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m13599(__this, ___index, ___item, method) (( void (*) (List_1_t177 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13599_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m13600_gshared (List_1_t177 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m13600(__this, ___item, method) (( void (*) (List_1_t177 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13600_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13601_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13601(__this, method) (( bool (*) (List_1_t177 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13601_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m13602_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m13602(__this, method) (( bool (*) (List_1_t177 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13602_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m13603_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m13603(__this, method) (( Object_t * (*) (List_1_t177 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13603_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m13604_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m13604(__this, method) (( bool (*) (List_1_t177 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13604_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m13605_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m13605(__this, method) (( bool (*) (List_1_t177 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13605_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m13606_gshared (List_1_t177 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m13606(__this, ___index, method) (( Object_t * (*) (List_1_t177 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13606_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m13607_gshared (List_1_t177 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m13607(__this, ___index, ___value, method) (( void (*) (List_1_t177 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13607_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void List_1_Add_m13608_gshared (List_1_t177 * __this, RaycastResult_t169  ___item, const MethodInfo* method);
#define List_1_Add_m13608(__this, ___item, method) (( void (*) (List_1_t177 *, RaycastResult_t169 , const MethodInfo*))List_1_Add_m13608_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m13609_gshared (List_1_t177 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m13609(__this, ___newCount, method) (( void (*) (List_1_t177 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13609_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m13610_gshared (List_1_t177 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m13610(__this, ___collection, method) (( void (*) (List_1_t177 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13610_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m13611_gshared (List_1_t177 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m13611(__this, ___enumerable, method) (( void (*) (List_1_t177 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13611_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m13612_gshared (List_1_t177 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m13612(__this, ___collection, method) (( void (*) (List_1_t177 *, Object_t*, const MethodInfo*))List_1_AddRange_m13612_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2241 * List_1_AsReadOnly_m13613_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m13613(__this, method) (( ReadOnlyCollection_1_t2241 * (*) (List_1_t177 *, const MethodInfo*))List_1_AsReadOnly_m13613_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void List_1_Clear_m13614_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_Clear_m13614(__this, method) (( void (*) (List_1_t177 *, const MethodInfo*))List_1_Clear_m13614_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool List_1_Contains_m13615_gshared (List_1_t177 * __this, RaycastResult_t169  ___item, const MethodInfo* method);
#define List_1_Contains_m13615(__this, ___item, method) (( bool (*) (List_1_t177 *, RaycastResult_t169 , const MethodInfo*))List_1_Contains_m13615_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m13616_gshared (List_1_t177 * __this, RaycastResultU5BU5D_t2238* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m13616(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t177 *, RaycastResultU5BU5D_t2238*, int32_t, const MethodInfo*))List_1_CopyTo_m13616_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Find(System.Predicate`1<T>)
extern "C" RaycastResult_t169  List_1_Find_m13617_gshared (List_1_t177 * __this, Predicate_1_t2246 * ___match, const MethodInfo* method);
#define List_1_Find_m13617(__this, ___match, method) (( RaycastResult_t169  (*) (List_1_t177 *, Predicate_1_t2246 *, const MethodInfo*))List_1_Find_m13617_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m13618_gshared (Object_t * __this /* static, unused */, Predicate_1_t2246 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m13618(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2246 *, const MethodInfo*))List_1_CheckMatch_m13618_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m13619_gshared (List_1_t177 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2246 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m13619(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t177 *, int32_t, int32_t, Predicate_1_t2246 *, const MethodInfo*))List_1_GetIndex_m13619_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Enumerator_t2240  List_1_GetEnumerator_m13620_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m13620(__this, method) (( Enumerator_t2240  (*) (List_1_t177 *, const MethodInfo*))List_1_GetEnumerator_m13620_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m13621_gshared (List_1_t177 * __this, RaycastResult_t169  ___item, const MethodInfo* method);
#define List_1_IndexOf_m13621(__this, ___item, method) (( int32_t (*) (List_1_t177 *, RaycastResult_t169 , const MethodInfo*))List_1_IndexOf_m13621_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m13622_gshared (List_1_t177 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m13622(__this, ___start, ___delta, method) (( void (*) (List_1_t177 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13622_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m13623_gshared (List_1_t177 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m13623(__this, ___index, method) (( void (*) (List_1_t177 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13623_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m13624_gshared (List_1_t177 * __this, int32_t ___index, RaycastResult_t169  ___item, const MethodInfo* method);
#define List_1_Insert_m13624(__this, ___index, ___item, method) (( void (*) (List_1_t177 *, int32_t, RaycastResult_t169 , const MethodInfo*))List_1_Insert_m13624_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m13625_gshared (List_1_t177 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m13625(__this, ___collection, method) (( void (*) (List_1_t177 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13625_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool List_1_Remove_m13626_gshared (List_1_t177 * __this, RaycastResult_t169  ___item, const MethodInfo* method);
#define List_1_Remove_m13626(__this, ___item, method) (( bool (*) (List_1_t177 *, RaycastResult_t169 , const MethodInfo*))List_1_Remove_m13626_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m13627_gshared (List_1_t177 * __this, Predicate_1_t2246 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m13627(__this, ___match, method) (( int32_t (*) (List_1_t177 *, Predicate_1_t2246 *, const MethodInfo*))List_1_RemoveAll_m13627_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m13628_gshared (List_1_t177 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m13628(__this, ___index, method) (( void (*) (List_1_t177 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13628_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Reverse()
extern "C" void List_1_Reverse_m13629_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_Reverse_m13629(__this, method) (( void (*) (List_1_t177 *, const MethodInfo*))List_1_Reverse_m13629_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort()
extern "C" void List_1_Sort_m13630_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_Sort_m13630(__this, method) (( void (*) (List_1_t177 *, const MethodInfo*))List_1_Sort_m13630_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1723_gshared (List_1_t177 * __this, Comparison_1_t138 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1723(__this, ___comparison, method) (( void (*) (List_1_t177 *, Comparison_1_t138 *, const MethodInfo*))List_1_Sort_m1723_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::ToArray()
extern "C" RaycastResultU5BU5D_t2238* List_1_ToArray_m13631_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_ToArray_m13631(__this, method) (( RaycastResultU5BU5D_t2238* (*) (List_1_t177 *, const MethodInfo*))List_1_ToArray_m13631_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m13632_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m13632(__this, method) (( void (*) (List_1_t177 *, const MethodInfo*))List_1_TrimExcess_m13632_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m13633_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m13633(__this, method) (( int32_t (*) (List_1_t177 *, const MethodInfo*))List_1_get_Capacity_m13633_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m13634_gshared (List_1_t177 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m13634(__this, ___value, method) (( void (*) (List_1_t177 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13634_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t List_1_get_Count_m13635_gshared (List_1_t177 * __this, const MethodInfo* method);
#define List_1_get_Count_m13635(__this, method) (( int32_t (*) (List_1_t177 *, const MethodInfo*))List_1_get_Count_m13635_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t169  List_1_get_Item_m13636_gshared (List_1_t177 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m13636(__this, ___index, method) (( RaycastResult_t169  (*) (List_1_t177 *, int32_t, const MethodInfo*))List_1_get_Item_m13636_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m13637_gshared (List_1_t177 * __this, int32_t ___index, RaycastResult_t169  ___value, const MethodInfo* method);
#define List_1_set_Item_m13637(__this, ___index, ___value, method) (( void (*) (List_1_t177 *, int32_t, RaycastResult_t169 , const MethodInfo*))List_1_set_Item_m13637_gshared)(__this, ___index, ___value, method)
