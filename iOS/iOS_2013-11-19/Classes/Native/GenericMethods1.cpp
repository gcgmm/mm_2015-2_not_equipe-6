﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array
struct Array_t;
// System.Int64[]
struct Int64U5BU5D_t2103;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t3381;
// System.Object[]
struct ObjectU5BU5D_t105;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t3339;
// System.Comparison`1<System.Object>
struct Comparison_1_t2186;
// System.Predicate`1<System.Object>
struct Predicate_1_t2180;
// System.Action`1<System.Object>
struct Action_1_t2195;
// System.Converter`2<System.Object,System.Object>
struct Converter_2_t3034;
// System.Object
struct Object_t;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2170;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct TableRangeU5BU5D_t1482;
// System.Collections.Generic.IEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
struct IEnumerator_1_t3382;
// System.Collections.Hashtable/Slot[]
struct SlotU5BU5D_t1564;
// System.Collections.Generic.IEnumerator`1<System.Collections.Hashtable/Slot>
struct IEnumerator_1_t3383;
// System.Collections.SortedList/Slot[]
struct SlotU5BU5D_t1568;
// System.Collections.Generic.IEnumerator`1<System.Collections.SortedList/Slot>
struct IEnumerator_1_t3384;
// System.Reflection.Emit.ILTokenInfo[]
struct ILTokenInfoU5BU5D_t1648;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ILTokenInfo>
struct IEnumerator_1_t3385;
// System.Reflection.Emit.ILGenerator/LabelData[]
struct LabelDataU5BU5D_t1649;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>
struct IEnumerator_1_t3386;
// System.Reflection.Emit.ILGenerator/LabelFixup[]
struct LabelFixupU5BU5D_t1650;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>
struct IEnumerator_1_t3387;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2129;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3262;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2130;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t3264;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ReadOnlyCollection_1_t2131;
// System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeTypedArgument>
struct IComparer_1_t3388;
// System.Array/Swapper
struct Swapper_t1448;
// System.Comparison`1<System.Reflection.CustomAttributeTypedArgument>
struct Comparison_1_t3074;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ReadOnlyCollection_1_t2132;
// System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeNamedArgument>
struct IComparer_1_t3389;
// System.Comparison`1<System.Reflection.CustomAttributeNamedArgument>
struct Comparison_1_t3085;
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t3088;
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct StaticGetter_1_t3089;
// System.Resources.ResourceReader/ResourceInfo[]
struct ResourceInfoU5BU5D_t1725;
// System.Collections.Generic.IEnumerator`1<System.Resources.ResourceReader/ResourceInfo>
struct IEnumerator_1_t3390;
// System.Resources.ResourceReader/ResourceCacheItem[]
struct ResourceCacheItemU5BU5D_t1726;
// System.Collections.Generic.IEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>
struct IEnumerator_1_t3391;
// System.DateTime[]
struct DateTimeU5BU5D_t2152;
// System.Collections.Generic.IEnumerator`1<System.DateTime>
struct IEnumerator_1_t3392;
// System.Decimal[]
struct DecimalU5BU5D_t2153;
// System.Collections.Generic.IEnumerator`1<System.Decimal>
struct IEnumerator_1_t3393;
// System.TimeSpan[]
struct TimeSpanU5BU5D_t2154;
// System.Collections.Generic.IEnumerator`1<System.TimeSpan>
struct IEnumerator_1_t3394;
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct TypeTagU5BU5D_t2155;
// System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
struct IEnumerator_1_t3395;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_NotSupportedException.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_ArrayMethodDeclarations.h"
#include "mscorlib_LocaleMethodDeclarations.h"
#include "mscorlib_System_RankExceptionMethodDeclarations.h"
#include "mscorlib_System_Int64MethodDeclarations.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_RankException.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "mscorlib_System_ArgumentException.h"
#include "mscorlib_System_ArgumentOutOfRangeException.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_98.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_98MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen_3.h"
#include "mscorlib_System_Predicate_1_gen_3.h"
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_9.h"
#include "mscorlib_System_Action_1_gen_9MethodDeclarations.h"
#include "mscorlib_System_Converter_2_gen.h"
#include "mscorlib_System_Converter_2_genMethodDeclarations.h"
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_genMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen.h"
#include "mscorlib_System_InvalidOperationException.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_genMethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_2.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_genMethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_2MethodDeclarations.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_101.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_101MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable_Slot.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_106.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_106MethodDeclarations.h"
#include "mscorlib_System_Collections_SortedList_Slot.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_107.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_107MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_113.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_113MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_114.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_114MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_115.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_115MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgumentMethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_122.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_122MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgumentMethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_123.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_123MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeData.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen_0.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen_0MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#include "mscorlib_System_MathMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_17.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_17MethodDeclarations.h"
#include "mscorlib_System_Array_Swapper.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Array_SwapperMethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_Comparison_1_gen_66.h"
#include "mscorlib_System_Comparison_1_gen_66MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_0.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen_1.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen_1MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_0MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_18.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_18MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen_67.h"
#include "mscorlib_System_Comparison_1_gen_67MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoProperty.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_gen.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_genMethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGetter_1_gen.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGetter_1_genMethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceInfo.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_124.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_124MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCacheItem.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_125.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_125MethodDeclarations.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_130.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_130MethodDeclarations.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_DecimalMethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_131.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_131MethodDeclarations.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_132.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_132MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_133.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_133MethodDeclarations.h"

// System.Void System.Array::GetGenericValueImpl<System.Int64>(System.Int32,T&)
extern "C" void Array_GetGenericValueImpl_TisInt64_t711_m26514_gshared (Array_t * __this, int32_t ___pos, int64_t* ___value, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisInt64_t711_m26514(__this, ___pos, ___value, method) (( void (*) (Array_t *, int32_t, int64_t*, const MethodInfo*))Array_GetGenericValueImpl_TisInt64_t711_m26514_gshared)(__this, ___pos, ___value, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Int64>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisInt64_t711_m26515_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisInt64_t711_m26515(__this, ___item, method) (( void (*) (Array_t *, int64_t, const MethodInfo*))Array_InternalArray__ICollection_Add_TisInt64_t711_m26515_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Int64>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisInt64_t711_m26516_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisInt64_t711_m26516(__this, ___item, method) (( bool (*) (Array_t *, int64_t, const MethodInfo*))Array_InternalArray__ICollection_Contains_TisInt64_t711_m26516_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Int64>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt64_t711_m26517_gshared (Array_t * __this, Int64U5BU5D_t2103* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisInt64_t711_m26517(__this, ___array, ___index, method) (( void (*) (Array_t *, Int64U5BU5D_t2103*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisInt64_t711_m26517_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Int64>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisInt64_t711_m26518_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisInt64_t711_m26518(__this, ___item, method) (( bool (*) (Array_t *, int64_t, const MethodInfo*))Array_InternalArray__ICollection_Remove_TisInt64_t711_m26518_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Int64>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisInt64_t711_m26519_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisInt64_t711_m26519(__this, ___item, method) (( int32_t (*) (Array_t *, int64_t, const MethodInfo*))Array_InternalArray__IndexOf_TisInt64_t711_m26519_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Int64>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisInt64_t711_m26520_gshared (Array_t * __this, int32_t ___index, int64_t ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisInt64_t711_m26520(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, int64_t, const MethodInfo*))Array_InternalArray__Insert_TisInt64_t711_m26520_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Int64>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisInt64_t711_m26521_gshared (Array_t * __this, int32_t ___index, int64_t ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisInt64_t711_m26521(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, int64_t, const MethodInfo*))Array_InternalArray__set_Item_TisInt64_t711_m26521_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Int64>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisInt64_t711_m26705_gshared (Array_t * __this, int32_t p0, int64_t* p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisInt64_t711_m26705(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, int64_t*, const MethodInfo*))Array_SetGenericValueImpl_TisInt64_t711_m26705_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Int64>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t711_m26522_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t711_m26522(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t711_m26522_gshared)(__this, method)
// System.Void System.Array::Sort<System.Object>(T[])
extern "C" void Array_Sort_TisObject_t_m26523_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, const MethodInfo* method);
#define Array_Sort_TisObject_t_m26523(__this /* static, unused */, ___array, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, const MethodInfo*))Array_Sort_TisObject_t_m26523_gshared)(__this /* static, unused */, ___array, method)
// System.Void System.Array::Sort<System.Object,System.Object>(!!0[],!!1[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C" void Array_Sort_TisObject_t_TisObject_t_m25592_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* p0, ObjectU5BU5D_t105* p1, int32_t p2, int32_t p3, Object_t* p4, const MethodInfo* method);
#define Array_Sort_TisObject_t_TisObject_t_m25592(__this /* static, unused */, p0, p1, p2, p3, p4, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, ObjectU5BU5D_t105*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_Sort_TisObject_t_TisObject_t_m25592_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[])
extern "C" void Array_Sort_TisObject_t_TisObject_t_m26524_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___keys, ObjectU5BU5D_t105* ___items, const MethodInfo* method);
#define Array_Sort_TisObject_t_TisObject_t_m26524(__this /* static, unused */, ___keys, ___items, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, ObjectU5BU5D_t105*, const MethodInfo*))Array_Sort_TisObject_t_TisObject_t_m26524_gshared)(__this /* static, unused */, ___keys, ___items, method)
// System.Void System.Array::Sort<System.Object>(T[],System.Collections.Generic.IComparer`1<T>)
extern "C" void Array_Sort_TisObject_t_m26525_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t* ___comparer, const MethodInfo* method);
#define Array_Sort_TisObject_t_m26525(__this /* static, unused */, ___array, ___comparer, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Object_t*, const MethodInfo*))Array_Sort_TisObject_t_m26525_gshared)(__this /* static, unused */, ___array, ___comparer, method)
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[],System.Collections.Generic.IComparer`1<TKey>)
extern "C" void Array_Sort_TisObject_t_TisObject_t_m26526_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___keys, ObjectU5BU5D_t105* ___items, Object_t* ___comparer, const MethodInfo* method);
#define Array_Sort_TisObject_t_TisObject_t_m26526(__this /* static, unused */, ___keys, ___items, ___comparer, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, ObjectU5BU5D_t105*, Object_t*, const MethodInfo*))Array_Sort_TisObject_t_TisObject_t_m26526_gshared)(__this /* static, unused */, ___keys, ___items, ___comparer, method)
// System.Void System.Array::Sort<System.Object>(T[],System.Int32,System.Int32)
extern "C" void Array_Sort_TisObject_t_m12763_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, int32_t ___index, int32_t ___length, const MethodInfo* method);
#define Array_Sort_TisObject_t_m12763(__this /* static, unused */, ___array, ___index, ___length, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, int32_t, const MethodInfo*))Array_Sort_TisObject_t_m12763_gshared)(__this /* static, unused */, ___array, ___index, ___length, method)
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[],System.Int32,System.Int32)
extern "C" void Array_Sort_TisObject_t_TisObject_t_m26527_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___keys, ObjectU5BU5D_t105* ___items, int32_t ___index, int32_t ___length, const MethodInfo* method);
#define Array_Sort_TisObject_t_TisObject_t_m26527(__this /* static, unused */, ___keys, ___items, ___index, ___length, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, ObjectU5BU5D_t105*, int32_t, int32_t, const MethodInfo*))Array_Sort_TisObject_t_TisObject_t_m26527_gshared)(__this /* static, unused */, ___keys, ___items, ___index, ___length, method)
// System.Void System.Array::Sort<System.Object>(T[],System.Comparison`1<T>)
extern "C" void Array_Sort_TisObject_t_m26528_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Comparison_1_t2186 * ___comparison, const MethodInfo* method);
#define Array_Sort_TisObject_t_m26528(__this /* static, unused */, ___array, ___comparison, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Comparison_1_t2186 *, const MethodInfo*))Array_Sort_TisObject_t_m26528_gshared)(__this /* static, unused */, ___array, ___comparison, method)
// System.Void System.Array::Sort<System.Object>(!!0[],System.Int32,System.Comparison`1<!!0>)
extern "C" void Array_Sort_TisObject_t_m25624_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* p0, int32_t p1, Comparison_1_t2186 * p2, const MethodInfo* method);
#define Array_Sort_TisObject_t_m25624(__this /* static, unused */, p0, p1, p2, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, Comparison_1_t2186 *, const MethodInfo*))Array_Sort_TisObject_t_m25624_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Boolean System.Array::TrueForAll<System.Object>(T[],System.Predicate`1<T>)
extern "C" bool Array_TrueForAll_TisObject_t_m26529_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define Array_TrueForAll_TisObject_t_m26529(__this /* static, unused */, ___array, ___match, method) (( bool (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Predicate_1_t2180 *, const MethodInfo*))Array_TrueForAll_TisObject_t_m26529_gshared)(__this /* static, unused */, ___array, ___match, method)
// System.Void System.Array::ForEach<System.Object>(T[],System.Action`1<T>)
extern "C" void Array_ForEach_TisObject_t_m26530_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Action_1_t2195 * ___action, const MethodInfo* method);
#define Array_ForEach_TisObject_t_m26530(__this /* static, unused */, ___array, ___action, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Action_1_t2195 *, const MethodInfo*))Array_ForEach_TisObject_t_m26530_gshared)(__this /* static, unused */, ___array, ___action, method)
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern "C" ObjectU5BU5D_t105* Array_ConvertAll_TisObject_t_TisObject_t_m26531_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Converter_2_t3034 * ___converter, const MethodInfo* method);
#define Array_ConvertAll_TisObject_t_TisObject_t_m26531(__this /* static, unused */, ___array, ___converter, method) (( ObjectU5BU5D_t105* (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Converter_2_t3034 *, const MethodInfo*))Array_ConvertAll_TisObject_t_TisObject_t_m26531_gshared)(__this /* static, unused */, ___array, ___converter, method)
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Predicate`1<T>)
extern "C" int32_t Array_FindLastIndex_TisObject_t_m26532_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define Array_FindLastIndex_TisObject_t_m26532(__this /* static, unused */, ___array, ___match, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Predicate_1_t2180 *, const MethodInfo*))Array_FindLastIndex_TisObject_t_m26532_gshared)(__this /* static, unused */, ___array, ___match, method)
// System.Int32 System.Array::FindLastIndex<System.Object>(!!0[],System.Int32,System.Int32,System.Predicate`1<!!0>)
extern "C" int32_t Array_FindLastIndex_TisObject_t_m26533_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* p0, int32_t p1, int32_t p2, Predicate_1_t2180 * p3, const MethodInfo* method);
#define Array_FindLastIndex_TisObject_t_m26533(__this /* static, unused */, p0, p1, p2, p3, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, int32_t, Predicate_1_t2180 *, const MethodInfo*))Array_FindLastIndex_TisObject_t_m26533_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Int32,System.Predicate`1<T>)
extern "C" int32_t Array_FindLastIndex_TisObject_t_m26534_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, int32_t ___startIndex, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define Array_FindLastIndex_TisObject_t_m26534(__this /* static, unused */, ___array, ___startIndex, ___match, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, Predicate_1_t2180 *, const MethodInfo*))Array_FindLastIndex_TisObject_t_m26534_gshared)(__this /* static, unused */, ___array, ___startIndex, ___match, method)
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Predicate`1<T>)
extern "C" int32_t Array_FindIndex_TisObject_t_m26535_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define Array_FindIndex_TisObject_t_m26535(__this /* static, unused */, ___array, ___match, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Predicate_1_t2180 *, const MethodInfo*))Array_FindIndex_TisObject_t_m26535_gshared)(__this /* static, unused */, ___array, ___match, method)
// System.Int32 System.Array::FindIndex<System.Object>(!!0[],System.Int32,System.Int32,System.Predicate`1<!!0>)
extern "C" int32_t Array_FindIndex_TisObject_t_m26536_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* p0, int32_t p1, int32_t p2, Predicate_1_t2180 * p3, const MethodInfo* method);
#define Array_FindIndex_TisObject_t_m26536(__this /* static, unused */, p0, p1, p2, p3, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, int32_t, Predicate_1_t2180 *, const MethodInfo*))Array_FindIndex_TisObject_t_m26536_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Int32,System.Predicate`1<T>)
extern "C" int32_t Array_FindIndex_TisObject_t_m26537_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, int32_t ___startIndex, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define Array_FindIndex_TisObject_t_m26537(__this /* static, unused */, ___array, ___startIndex, ___match, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, Predicate_1_t2180 *, const MethodInfo*))Array_FindIndex_TisObject_t_m26537_gshared)(__this /* static, unused */, ___array, ___startIndex, ___match, method)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],T)
extern "C" int32_t Array_BinarySearch_TisObject_t_m26538_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t * ___value, const MethodInfo* method);
#define Array_BinarySearch_TisObject_t_m26538(__this /* static, unused */, ___array, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Object_t *, const MethodInfo*))Array_BinarySearch_TisObject_t_m26538_gshared)(__this /* static, unused */, ___array, ___value, method)
// System.Int32 System.Array::BinarySearch<System.Object>(!!0[],System.Int32,System.Int32,!!0,System.Collections.Generic.IComparer`1<!!0>)
extern "C" int32_t Array_BinarySearch_TisObject_t_m26539_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* p0, int32_t p1, int32_t p2, Object_t * p3, Object_t* p4, const MethodInfo* method);
#define Array_BinarySearch_TisObject_t_m26539(__this /* static, unused */, p0, p1, p2, p3, p4, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, int32_t, Object_t *, Object_t*, const MethodInfo*))Array_BinarySearch_TisObject_t_m26539_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t Array_BinarySearch_TisObject_t_m26540_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t * ___value, Object_t* ___comparer, const MethodInfo* method);
#define Array_BinarySearch_TisObject_t_m26540(__this /* static, unused */, ___array, ___value, ___comparer, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Object_t *, Object_t*, const MethodInfo*))Array_BinarySearch_TisObject_t_m26540_gshared)(__this /* static, unused */, ___array, ___value, ___comparer, method)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],System.Int32,System.Int32,T)
extern "C" int32_t Array_BinarySearch_TisObject_t_m26541_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, int32_t ___index, int32_t ___length, Object_t * ___value, const MethodInfo* method);
#define Array_BinarySearch_TisObject_t_m26541(__this /* static, unused */, ___array, ___index, ___length, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, int32_t, Object_t *, const MethodInfo*))Array_BinarySearch_TisObject_t_m26541_gshared)(__this /* static, unused */, ___array, ___index, ___length, ___value, method)
// System.Int32 System.Array::IndexOf<System.Object>(T[],T)
extern "C" int32_t Array_IndexOf_TisObject_t_m12769_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t * ___value, const MethodInfo* method);
#define Array_IndexOf_TisObject_t_m12769(__this /* static, unused */, ___array, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Object_t *, const MethodInfo*))Array_IndexOf_TisObject_t_m12769_gshared)(__this /* static, unused */, ___array, ___value, method)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0,System.Int32,System.Int32)
extern "C" int32_t Array_IndexOf_TisObject_t_m12762_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* p0, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisObject_t_m12762(__this /* static, unused */, p0, p1, p2, p3, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Object_t *, int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisObject_t_m12762_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Int32 System.Array::IndexOf<System.Object>(T[],T,System.Int32)
extern "C" int32_t Array_IndexOf_TisObject_t_m26542_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t * ___value, int32_t ___startIndex, const MethodInfo* method);
#define Array_IndexOf_TisObject_t_m26542(__this /* static, unused */, ___array, ___value, ___startIndex, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Object_t *, int32_t, const MethodInfo*))Array_IndexOf_TisObject_t_m26542_gshared)(__this /* static, unused */, ___array, ___value, ___startIndex, method)
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T)
extern "C" int32_t Array_LastIndexOf_TisObject_t_m26543_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t * ___value, const MethodInfo* method);
#define Array_LastIndexOf_TisObject_t_m26543(__this /* static, unused */, ___array, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Object_t *, const MethodInfo*))Array_LastIndexOf_TisObject_t_m26543_gshared)(__this /* static, unused */, ___array, ___value, method)
// System.Int32 System.Array::LastIndexOf<System.Object>(!!0[],!!0,System.Int32)
extern "C" int32_t Array_LastIndexOf_TisObject_t_m26544_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* p0, Object_t * p1, int32_t p2, const MethodInfo* method);
#define Array_LastIndexOf_TisObject_t_m26544(__this /* static, unused */, p0, p1, p2, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Object_t *, int32_t, const MethodInfo*))Array_LastIndexOf_TisObject_t_m26544_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Int32 System.Array::LastIndexOf<System.Object>(!!0[],!!0,System.Int32,System.Int32)
extern "C" int32_t Array_LastIndexOf_TisObject_t_m26545_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* p0, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_LastIndexOf_TisObject_t_m26545(__this /* static, unused */, p0, p1, p2, p3, method) (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Object_t *, int32_t, int32_t, const MethodInfo*))Array_LastIndexOf_TisObject_t_m26545_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern "C" ObjectU5BU5D_t105* Array_FindAll_TisObject_t_m26546_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define Array_FindAll_TisObject_t_m26546(__this /* static, unused */, ___array, ___match, method) (( ObjectU5BU5D_t105* (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Predicate_1_t2180 *, const MethodInfo*))Array_FindAll_TisObject_t_m26546_gshared)(__this /* static, unused */, ___array, ___match, method)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C" void Array_Resize_TisObject_t_m25589_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisObject_t_m25589(__this /* static, unused */, p0, p1, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105**, int32_t, const MethodInfo*))Array_Resize_TisObject_t_m25589_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Array::Exists<System.Object>(T[],System.Predicate`1<T>)
extern "C" bool Array_Exists_TisObject_t_m26547_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define Array_Exists_TisObject_t_m26547(__this /* static, unused */, ___array, ___match, method) (( bool (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Predicate_1_t2180 *, const MethodInfo*))Array_Exists_TisObject_t_m26547_gshared)(__this /* static, unused */, ___array, ___match, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Object>(T[])
extern "C" ReadOnlyCollection_1_t2170 * Array_AsReadOnly_TisObject_t_m12783_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, const MethodInfo* method);
#define Array_AsReadOnly_TisObject_t_m12783(__this /* static, unused */, ___array, method) (( ReadOnlyCollection_1_t2170 * (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, const MethodInfo*))Array_AsReadOnly_TisObject_t_m12783_gshared)(__this /* static, unused */, ___array, method)
// T System.Array::Find<System.Object>(T[],System.Predicate`1<T>)
extern "C" Object_t * Array_Find_TisObject_t_m26548_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define Array_Find_TisObject_t_m26548(__this /* static, unused */, ___array, ___match, method) (( Object_t * (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Predicate_1_t2180 *, const MethodInfo*))Array_Find_TisObject_t_m26548_gshared)(__this /* static, unused */, ___array, ___match, method)
// T System.Array::FindLast<System.Object>(T[],System.Predicate`1<T>)
extern "C" Object_t * Array_FindLast_TisObject_t_m26549_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define Array_FindLast_TisObject_t_m26549(__this /* static, unused */, ___array, ___match, method) (( Object_t * (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Predicate_1_t2180 *, const MethodInfo*))Array_FindLast_TisObject_t_m26549_gshared)(__this /* static, unused */, ___array, ___match, method)
// T System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C" TableRange_t1480  Array_InternalArray__get_Item_TisTableRange_t1480_m26550_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTableRange_t1480_m26550(__this, ___index, method) (( TableRange_t1480  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTableRange_t1480_m26550_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisTableRange_t1480_m26706_gshared (Array_t * __this, int32_t p0, TableRange_t1480 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTableRange_t1480_m26706(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, TableRange_t1480 *, const MethodInfo*))Array_GetGenericValueImpl_TisTableRange_t1480_m26706_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisTableRange_t1480_m26551_gshared (Array_t * __this, TableRange_t1480  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisTableRange_t1480_m26551(__this, ___item, method) (( void (*) (Array_t *, TableRange_t1480 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisTableRange_t1480_m26551_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisTableRange_t1480_m26552_gshared (Array_t * __this, TableRange_t1480  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisTableRange_t1480_m26552(__this, ___item, method) (( bool (*) (Array_t *, TableRange_t1480 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisTableRange_t1480_m26552_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTableRange_t1480_m26553_gshared (Array_t * __this, TableRangeU5BU5D_t1482* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisTableRange_t1480_m26553(__this, ___array, ___index, method) (( void (*) (Array_t *, TableRangeU5BU5D_t1482*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisTableRange_t1480_m26553_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisTableRange_t1480_m26554_gshared (Array_t * __this, TableRange_t1480  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisTableRange_t1480_m26554(__this, ___item, method) (( bool (*) (Array_t *, TableRange_t1480 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisTableRange_t1480_m26554_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisTableRange_t1480_m26555_gshared (Array_t * __this, TableRange_t1480  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisTableRange_t1480_m26555(__this, ___item, method) (( int32_t (*) (Array_t *, TableRange_t1480 , const MethodInfo*))Array_InternalArray__IndexOf_TisTableRange_t1480_m26555_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisTableRange_t1480_m26556_gshared (Array_t * __this, int32_t ___index, TableRange_t1480  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisTableRange_t1480_m26556(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TableRange_t1480 , const MethodInfo*))Array_InternalArray__Insert_TisTableRange_t1480_m26556_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisTableRange_t1480_m26557_gshared (Array_t * __this, int32_t ___index, TableRange_t1480  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisTableRange_t1480_m26557(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TableRange_t1480 , const MethodInfo*))Array_InternalArray__set_Item_TisTableRange_t1480_m26557_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisTableRange_t1480_m26707_gshared (Array_t * __this, int32_t p0, TableRange_t1480 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisTableRange_t1480_m26707(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, TableRange_t1480 *, const MethodInfo*))Array_SetGenericValueImpl_TisTableRange_t1480_m26707_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<Mono.Globalization.Unicode.CodePointIndexer/TableRange>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1480_m26558_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1480_m26558(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1480_m26558_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern "C" Slot_t1557  Array_InternalArray__get_Item_TisSlot_t1557_m26559_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t1557_m26559(__this, ___index, method) (( Slot_t1557  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t1557_m26559_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisSlot_t1557_m26708_gshared (Array_t * __this, int32_t p0, Slot_t1557 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSlot_t1557_m26708(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, Slot_t1557 *, const MethodInfo*))Array_GetGenericValueImpl_TisSlot_t1557_m26708_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.Hashtable/Slot>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1557_m26560_gshared (Array_t * __this, Slot_t1557  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisSlot_t1557_m26560(__this, ___item, method) (( void (*) (Array_t *, Slot_t1557 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisSlot_t1557_m26560_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.Hashtable/Slot>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisSlot_t1557_m26561_gshared (Array_t * __this, Slot_t1557  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisSlot_t1557_m26561(__this, ___item, method) (( bool (*) (Array_t *, Slot_t1557 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisSlot_t1557_m26561_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.Hashtable/Slot>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1557_m26562_gshared (Array_t * __this, SlotU5BU5D_t1564* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisSlot_t1557_m26562(__this, ___array, ___index, method) (( void (*) (Array_t *, SlotU5BU5D_t1564*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisSlot_t1557_m26562_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.Hashtable/Slot>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisSlot_t1557_m26563_gshared (Array_t * __this, Slot_t1557  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisSlot_t1557_m26563(__this, ___item, method) (( bool (*) (Array_t *, Slot_t1557 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisSlot_t1557_m26563_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.Hashtable/Slot>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisSlot_t1557_m26564_gshared (Array_t * __this, Slot_t1557  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisSlot_t1557_m26564(__this, ___item, method) (( int32_t (*) (Array_t *, Slot_t1557 , const MethodInfo*))Array_InternalArray__IndexOf_TisSlot_t1557_m26564_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Collections.Hashtable/Slot>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisSlot_t1557_m26565_gshared (Array_t * __this, int32_t ___index, Slot_t1557  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisSlot_t1557_m26565(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Slot_t1557 , const MethodInfo*))Array_InternalArray__Insert_TisSlot_t1557_m26565_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Collections.Hashtable/Slot>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisSlot_t1557_m26566_gshared (Array_t * __this, int32_t ___index, Slot_t1557  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisSlot_t1557_m26566(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Slot_t1557 , const MethodInfo*))Array_InternalArray__set_Item_TisSlot_t1557_m26566_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisSlot_t1557_m26709_gshared (Array_t * __this, int32_t p0, Slot_t1557 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisSlot_t1557_m26709(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, Slot_t1557 *, const MethodInfo*))Array_SetGenericValueImpl_TisSlot_t1557_m26709_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.Hashtable/Slot>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1557_m26567_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1557_m26567(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1557_m26567_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern "C" Slot_t1565  Array_InternalArray__get_Item_TisSlot_t1565_m26568_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t1565_m26568(__this, ___index, method) (( Slot_t1565  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t1565_m26568_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisSlot_t1565_m26710_gshared (Array_t * __this, int32_t p0, Slot_t1565 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSlot_t1565_m26710(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, Slot_t1565 *, const MethodInfo*))Array_GetGenericValueImpl_TisSlot_t1565_m26710_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.SortedList/Slot>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1565_m26569_gshared (Array_t * __this, Slot_t1565  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisSlot_t1565_m26569(__this, ___item, method) (( void (*) (Array_t *, Slot_t1565 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisSlot_t1565_m26569_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.SortedList/Slot>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisSlot_t1565_m26570_gshared (Array_t * __this, Slot_t1565  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisSlot_t1565_m26570(__this, ___item, method) (( bool (*) (Array_t *, Slot_t1565 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisSlot_t1565_m26570_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.SortedList/Slot>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1565_m26571_gshared (Array_t * __this, SlotU5BU5D_t1568* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisSlot_t1565_m26571(__this, ___array, ___index, method) (( void (*) (Array_t *, SlotU5BU5D_t1568*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisSlot_t1565_m26571_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.SortedList/Slot>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisSlot_t1565_m26572_gshared (Array_t * __this, Slot_t1565  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisSlot_t1565_m26572(__this, ___item, method) (( bool (*) (Array_t *, Slot_t1565 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisSlot_t1565_m26572_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.SortedList/Slot>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisSlot_t1565_m26573_gshared (Array_t * __this, Slot_t1565  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisSlot_t1565_m26573(__this, ___item, method) (( int32_t (*) (Array_t *, Slot_t1565 , const MethodInfo*))Array_InternalArray__IndexOf_TisSlot_t1565_m26573_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Collections.SortedList/Slot>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisSlot_t1565_m26574_gshared (Array_t * __this, int32_t ___index, Slot_t1565  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisSlot_t1565_m26574(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Slot_t1565 , const MethodInfo*))Array_InternalArray__Insert_TisSlot_t1565_m26574_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Collections.SortedList/Slot>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisSlot_t1565_m26575_gshared (Array_t * __this, int32_t ___index, Slot_t1565  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisSlot_t1565_m26575(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Slot_t1565 , const MethodInfo*))Array_InternalArray__set_Item_TisSlot_t1565_m26575_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisSlot_t1565_m26711_gshared (Array_t * __this, int32_t p0, Slot_t1565 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisSlot_t1565_m26711(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, Slot_t1565 *, const MethodInfo*))Array_SetGenericValueImpl_TisSlot_t1565_m26711_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.SortedList/Slot>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1565_m26576_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1565_m26576(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1565_m26576_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern "C" ILTokenInfo_t1645  Array_InternalArray__get_Item_TisILTokenInfo_t1645_m26577_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisILTokenInfo_t1645_m26577(__this, ___index, method) (( ILTokenInfo_t1645  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisILTokenInfo_t1645_m26577_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILTokenInfo>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisILTokenInfo_t1645_m26712_gshared (Array_t * __this, int32_t p0, ILTokenInfo_t1645 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisILTokenInfo_t1645_m26712(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, ILTokenInfo_t1645 *, const MethodInfo*))Array_GetGenericValueImpl_TisILTokenInfo_t1645_m26712_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILTokenInfo>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisILTokenInfo_t1645_m26578_gshared (Array_t * __this, ILTokenInfo_t1645  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisILTokenInfo_t1645_m26578(__this, ___item, method) (( void (*) (Array_t *, ILTokenInfo_t1645 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisILTokenInfo_t1645_m26578_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILTokenInfo>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisILTokenInfo_t1645_m26579_gshared (Array_t * __this, ILTokenInfo_t1645  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisILTokenInfo_t1645_m26579(__this, ___item, method) (( bool (*) (Array_t *, ILTokenInfo_t1645 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisILTokenInfo_t1645_m26579_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILTokenInfo>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisILTokenInfo_t1645_m26580_gshared (Array_t * __this, ILTokenInfoU5BU5D_t1648* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisILTokenInfo_t1645_m26580(__this, ___array, ___index, method) (( void (*) (Array_t *, ILTokenInfoU5BU5D_t1648*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisILTokenInfo_t1645_m26580_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILTokenInfo>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisILTokenInfo_t1645_m26581_gshared (Array_t * __this, ILTokenInfo_t1645  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisILTokenInfo_t1645_m26581(__this, ___item, method) (( bool (*) (Array_t *, ILTokenInfo_t1645 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisILTokenInfo_t1645_m26581_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILTokenInfo>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisILTokenInfo_t1645_m26582_gshared (Array_t * __this, ILTokenInfo_t1645  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisILTokenInfo_t1645_m26582(__this, ___item, method) (( int32_t (*) (Array_t *, ILTokenInfo_t1645 , const MethodInfo*))Array_InternalArray__IndexOf_TisILTokenInfo_t1645_m26582_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILTokenInfo>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisILTokenInfo_t1645_m26583_gshared (Array_t * __this, int32_t ___index, ILTokenInfo_t1645  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisILTokenInfo_t1645_m26583(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, ILTokenInfo_t1645 , const MethodInfo*))Array_InternalArray__Insert_TisILTokenInfo_t1645_m26583_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisILTokenInfo_t1645_m26584_gshared (Array_t * __this, int32_t ___index, ILTokenInfo_t1645  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisILTokenInfo_t1645_m26584(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, ILTokenInfo_t1645 , const MethodInfo*))Array_InternalArray__set_Item_TisILTokenInfo_t1645_m26584_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILTokenInfo>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisILTokenInfo_t1645_m26713_gshared (Array_t * __this, int32_t p0, ILTokenInfo_t1645 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisILTokenInfo_t1645_m26713(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, ILTokenInfo_t1645 *, const MethodInfo*))Array_SetGenericValueImpl_TisILTokenInfo_t1645_m26713_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILTokenInfo>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisILTokenInfo_t1645_m26585_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisILTokenInfo_t1645_m26585(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisILTokenInfo_t1645_m26585_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern "C" LabelData_t1647  Array_InternalArray__get_Item_TisLabelData_t1647_m26586_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelData_t1647_m26586(__this, ___index, method) (( LabelData_t1647  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelData_t1647_m26586_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisLabelData_t1647_m26714_gshared (Array_t * __this, int32_t p0, LabelData_t1647 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLabelData_t1647_m26714(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, LabelData_t1647 *, const MethodInfo*))Array_GetGenericValueImpl_TisLabelData_t1647_m26714_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisLabelData_t1647_m26587_gshared (Array_t * __this, LabelData_t1647  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisLabelData_t1647_m26587(__this, ___item, method) (( void (*) (Array_t *, LabelData_t1647 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisLabelData_t1647_m26587_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisLabelData_t1647_m26588_gshared (Array_t * __this, LabelData_t1647  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisLabelData_t1647_m26588(__this, ___item, method) (( bool (*) (Array_t *, LabelData_t1647 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisLabelData_t1647_m26588_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelData>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelData_t1647_m26589_gshared (Array_t * __this, LabelDataU5BU5D_t1649* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisLabelData_t1647_m26589(__this, ___array, ___index, method) (( void (*) (Array_t *, LabelDataU5BU5D_t1649*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisLabelData_t1647_m26589_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisLabelData_t1647_m26590_gshared (Array_t * __this, LabelData_t1647  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisLabelData_t1647_m26590(__this, ___item, method) (( bool (*) (Array_t *, LabelData_t1647 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisLabelData_t1647_m26590_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisLabelData_t1647_m26591_gshared (Array_t * __this, LabelData_t1647  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisLabelData_t1647_m26591(__this, ___item, method) (( int32_t (*) (Array_t *, LabelData_t1647 , const MethodInfo*))Array_InternalArray__IndexOf_TisLabelData_t1647_m26591_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisLabelData_t1647_m26592_gshared (Array_t * __this, int32_t ___index, LabelData_t1647  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisLabelData_t1647_m26592(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, LabelData_t1647 , const MethodInfo*))Array_InternalArray__Insert_TisLabelData_t1647_m26592_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisLabelData_t1647_m26593_gshared (Array_t * __this, int32_t ___index, LabelData_t1647  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisLabelData_t1647_m26593(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, LabelData_t1647 , const MethodInfo*))Array_InternalArray__set_Item_TisLabelData_t1647_m26593_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisLabelData_t1647_m26715_gshared (Array_t * __this, int32_t p0, LabelData_t1647 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisLabelData_t1647_m26715(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, LabelData_t1647 *, const MethodInfo*))Array_SetGenericValueImpl_TisLabelData_t1647_m26715_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelData>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t1647_m26594_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t1647_m26594(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t1647_m26594_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern "C" LabelFixup_t1646  Array_InternalArray__get_Item_TisLabelFixup_t1646_m26595_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelFixup_t1646_m26595(__this, ___index, method) (( LabelFixup_t1646  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelFixup_t1646_m26595_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisLabelFixup_t1646_m26716_gshared (Array_t * __this, int32_t p0, LabelFixup_t1646 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLabelFixup_t1646_m26716(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, LabelFixup_t1646 *, const MethodInfo*))Array_GetGenericValueImpl_TisLabelFixup_t1646_m26716_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisLabelFixup_t1646_m26596_gshared (Array_t * __this, LabelFixup_t1646  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisLabelFixup_t1646_m26596(__this, ___item, method) (( void (*) (Array_t *, LabelFixup_t1646 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisLabelFixup_t1646_m26596_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisLabelFixup_t1646_m26597_gshared (Array_t * __this, LabelFixup_t1646  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisLabelFixup_t1646_m26597(__this, ___item, method) (( bool (*) (Array_t *, LabelFixup_t1646 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisLabelFixup_t1646_m26597_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelFixup>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t1646_m26598_gshared (Array_t * __this, LabelFixupU5BU5D_t1650* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t1646_m26598(__this, ___array, ___index, method) (( void (*) (Array_t *, LabelFixupU5BU5D_t1650*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t1646_m26598_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisLabelFixup_t1646_m26599_gshared (Array_t * __this, LabelFixup_t1646  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisLabelFixup_t1646_m26599(__this, ___item, method) (( bool (*) (Array_t *, LabelFixup_t1646 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisLabelFixup_t1646_m26599_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisLabelFixup_t1646_m26600_gshared (Array_t * __this, LabelFixup_t1646  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisLabelFixup_t1646_m26600(__this, ___item, method) (( int32_t (*) (Array_t *, LabelFixup_t1646 , const MethodInfo*))Array_InternalArray__IndexOf_TisLabelFixup_t1646_m26600_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisLabelFixup_t1646_m26601_gshared (Array_t * __this, int32_t ___index, LabelFixup_t1646  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisLabelFixup_t1646_m26601(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, LabelFixup_t1646 , const MethodInfo*))Array_InternalArray__Insert_TisLabelFixup_t1646_m26601_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisLabelFixup_t1646_m26602_gshared (Array_t * __this, int32_t ___index, LabelFixup_t1646  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisLabelFixup_t1646_m26602(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, LabelFixup_t1646 , const MethodInfo*))Array_InternalArray__set_Item_TisLabelFixup_t1646_m26602_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisLabelFixup_t1646_m26717_gshared (Array_t * __this, int32_t p0, LabelFixup_t1646 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisLabelFixup_t1646_m26717(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, LabelFixup_t1646 *, const MethodInfo*))Array_SetGenericValueImpl_TisLabelFixup_t1646_m26717_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelFixup>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t1646_m26603_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t1646_m26603(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t1646_m26603_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern "C" CustomAttributeTypedArgument_t1692  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1692_m26604_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1692_m26604(__this, ___index, method) (( CustomAttributeTypedArgument_t1692  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1692_m26604_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeTypedArgument>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t1692_m26718_gshared (Array_t * __this, int32_t p0, CustomAttributeTypedArgument_t1692 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t1692_m26718(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, CustomAttributeTypedArgument_t1692 *, const MethodInfo*))Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t1692_m26718_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.CustomAttributeTypedArgument>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisCustomAttributeTypedArgument_t1692_m26605_gshared (Array_t * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisCustomAttributeTypedArgument_t1692_m26605(__this, ___item, method) (( void (*) (Array_t *, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisCustomAttributeTypedArgument_t1692_m26605_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.CustomAttributeTypedArgument>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisCustomAttributeTypedArgument_t1692_m26606_gshared (Array_t * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisCustomAttributeTypedArgument_t1692_m26606(__this, ___item, method) (( bool (*) (Array_t *, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisCustomAttributeTypedArgument_t1692_m26606_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisCustomAttributeTypedArgument_t1692_m26607_gshared (Array_t * __this, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisCustomAttributeTypedArgument_t1692_m26607(__this, ___array, ___index, method) (( void (*) (Array_t *, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisCustomAttributeTypedArgument_t1692_m26607_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.CustomAttributeTypedArgument>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisCustomAttributeTypedArgument_t1692_m26608_gshared (Array_t * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisCustomAttributeTypedArgument_t1692_m26608(__this, ___item, method) (( bool (*) (Array_t *, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisCustomAttributeTypedArgument_t1692_m26608_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.CustomAttributeTypedArgument>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisCustomAttributeTypedArgument_t1692_m26609_gshared (Array_t * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisCustomAttributeTypedArgument_t1692_m26609(__this, ___item, method) (( int32_t (*) (Array_t *, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Array_InternalArray__IndexOf_TisCustomAttributeTypedArgument_t1692_m26609_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisCustomAttributeTypedArgument_t1692_m26610_gshared (Array_t * __this, int32_t ___index, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisCustomAttributeTypedArgument_t1692_m26610(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Array_InternalArray__Insert_TisCustomAttributeTypedArgument_t1692_m26610_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisCustomAttributeTypedArgument_t1692_m26611_gshared (Array_t * __this, int32_t ___index, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisCustomAttributeTypedArgument_t1692_m26611(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Array_InternalArray__set_Item_TisCustomAttributeTypedArgument_t1692_m26611_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Reflection.CustomAttributeTypedArgument>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisCustomAttributeTypedArgument_t1692_m26719_gshared (Array_t * __this, int32_t p0, CustomAttributeTypedArgument_t1692 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisCustomAttributeTypedArgument_t1692_m26719(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, CustomAttributeTypedArgument_t1692 *, const MethodInfo*))Array_SetGenericValueImpl_TisCustomAttributeTypedArgument_t1692_m26719_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.CustomAttributeTypedArgument>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeTypedArgument_t1692_m26612_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeTypedArgument_t1692_m26612(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeTypedArgument_t1692_m26612_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern "C" CustomAttributeNamedArgument_t1691  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t1691_m26613_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t1691_m26613(__this, ___index, method) (( CustomAttributeNamedArgument_t1691  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t1691_m26613_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeNamedArgument>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t1691_m26720_gshared (Array_t * __this, int32_t p0, CustomAttributeNamedArgument_t1691 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t1691_m26720(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, CustomAttributeNamedArgument_t1691 *, const MethodInfo*))Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t1691_m26720_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.CustomAttributeNamedArgument>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisCustomAttributeNamedArgument_t1691_m26614_gshared (Array_t * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisCustomAttributeNamedArgument_t1691_m26614(__this, ___item, method) (( void (*) (Array_t *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisCustomAttributeNamedArgument_t1691_m26614_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.CustomAttributeNamedArgument>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisCustomAttributeNamedArgument_t1691_m26615_gshared (Array_t * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisCustomAttributeNamedArgument_t1691_m26615(__this, ___item, method) (( bool (*) (Array_t *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisCustomAttributeNamedArgument_t1691_m26615_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisCustomAttributeNamedArgument_t1691_m26616_gshared (Array_t * __this, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisCustomAttributeNamedArgument_t1691_m26616(__this, ___array, ___index, method) (( void (*) (Array_t *, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisCustomAttributeNamedArgument_t1691_m26616_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.CustomAttributeNamedArgument>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisCustomAttributeNamedArgument_t1691_m26617_gshared (Array_t * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisCustomAttributeNamedArgument_t1691_m26617(__this, ___item, method) (( bool (*) (Array_t *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisCustomAttributeNamedArgument_t1691_m26617_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.CustomAttributeNamedArgument>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisCustomAttributeNamedArgument_t1691_m26618_gshared (Array_t * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisCustomAttributeNamedArgument_t1691_m26618(__this, ___item, method) (( int32_t (*) (Array_t *, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Array_InternalArray__IndexOf_TisCustomAttributeNamedArgument_t1691_m26618_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisCustomAttributeNamedArgument_t1691_m26619_gshared (Array_t * __this, int32_t ___index, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisCustomAttributeNamedArgument_t1691_m26619(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Array_InternalArray__Insert_TisCustomAttributeNamedArgument_t1691_m26619_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisCustomAttributeNamedArgument_t1691_m26620_gshared (Array_t * __this, int32_t ___index, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisCustomAttributeNamedArgument_t1691_m26620(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Array_InternalArray__set_Item_TisCustomAttributeNamedArgument_t1691_m26620_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Reflection.CustomAttributeNamedArgument>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisCustomAttributeNamedArgument_t1691_m26721_gshared (Array_t * __this, int32_t p0, CustomAttributeNamedArgument_t1691 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisCustomAttributeNamedArgument_t1691_m26721(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, CustomAttributeNamedArgument_t1691 *, const MethodInfo*))Array_SetGenericValueImpl_TisCustomAttributeNamedArgument_t1691_m26721_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.CustomAttributeNamedArgument>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeNamedArgument_t1691_m26621_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeNamedArgument_t1691_m26621(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeNamedArgument_t1691_m26621_gshared)(__this, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C" CustomAttributeTypedArgumentU5BU5D_t2129* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t1692_m12765_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___values, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t1692_m12765(__this /* static, unused */, ___values, method) (( CustomAttributeTypedArgumentU5BU5D_t2129* (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t1692_m12765_gshared)(__this /* static, unused */, ___values, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Reflection.CustomAttributeTypedArgument>(T[])
extern "C" ReadOnlyCollection_1_t2131 * Array_AsReadOnly_TisCustomAttributeTypedArgument_t1692_m12766_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, const MethodInfo* method);
#define Array_AsReadOnly_TisCustomAttributeTypedArgument_t1692_m12766(__this /* static, unused */, ___array, method) (( ReadOnlyCollection_1_t2131 * (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, const MethodInfo*))Array_AsReadOnly_TisCustomAttributeTypedArgument_t1692_m12766_gshared)(__this /* static, unused */, ___array, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(T[]&,System.Int32)
extern "C" void Array_Resize_TisCustomAttributeTypedArgument_t1692_m26622_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129** ___array, int32_t ___newSize, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeTypedArgument_t1692_m26622(__this /* static, unused */, ___array, ___newSize, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129**, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeTypedArgument_t1692_m26622_gshared)(__this /* static, unused */, ___array, ___newSize, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(!!0[]&,System.Int32,System.Int32)
extern "C" void Array_Resize_TisCustomAttributeTypedArgument_t1692_m26623_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129** p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeTypedArgument_t1692_m26623(__this /* static, unused */, p0, p1, p2, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129**, int32_t, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeTypedArgument_t1692_m26623_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(T[],T,System.Int32,System.Int32)
extern "C" int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t1692_m26624_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, CustomAttributeTypedArgument_t1692  ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t1692_m26624(__this /* static, unused */, ___array, ___value, ___startIndex, ___count, method) (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, CustomAttributeTypedArgument_t1692 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t1692_m26624_gshared)(__this /* static, unused */, ___array, ___value, ___startIndex, ___count, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t1692_m26625_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, int32_t ___index, int32_t ___length, Object_t* ___comparer, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeTypedArgument_t1692_m26625(__this /* static, unused */, ___array, ___index, ___length, ___comparer, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_Sort_TisCustomAttributeTypedArgument_t1692_m26625_gshared)(__this /* static, unused */, ___array, ___index, ___length, ___comparer, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(!!0[],!!1[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t1692_TisCustomAttributeTypedArgument_t1692_m26626_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* p0, CustomAttributeTypedArgumentU5BU5D_t2129* p1, int32_t p2, int32_t p3, Object_t* p4, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeTypedArgument_t1692_TisCustomAttributeTypedArgument_t1692_m26626(__this /* static, unused */, p0, p1, p2, p3, p4, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_Sort_TisCustomAttributeTypedArgument_t1692_TisCustomAttributeTypedArgument_t1692_m26626_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Array/Swapper System.Array::get_swapper<System.Reflection.CustomAttributeTypedArgument>(!!0[])
extern "C" Swapper_t1448 * Array_get_swapper_TisCustomAttributeTypedArgument_t1692_m26627_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* p0, const MethodInfo* method);
#define Array_get_swapper_TisCustomAttributeTypedArgument_t1692_m26627(__this /* static, unused */, p0, method) (( Swapper_t1448 * (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, const MethodInfo*))Array_get_swapper_TisCustomAttributeTypedArgument_t1692_m26627_gshared)(__this /* static, unused */, p0, method)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(!!0[],!!1[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C" void Array_qsort_TisCustomAttributeTypedArgument_t1692_TisCustomAttributeTypedArgument_t1692_m26628_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* p0, CustomAttributeTypedArgumentU5BU5D_t2129* p1, int32_t p2, int32_t p3, Object_t* p4, const MethodInfo* method);
#define Array_qsort_TisCustomAttributeTypedArgument_t1692_TisCustomAttributeTypedArgument_t1692_m26628(__this /* static, unused */, p0, p1, p2, p3, p4, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_qsort_TisCustomAttributeTypedArgument_t1692_TisCustomAttributeTypedArgument_t1692_m26628_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Int32 System.Array::compare<System.Reflection.CustomAttributeTypedArgument>(!!0,!!0,System.Collections.Generic.IComparer`1<!!0>)
extern "C" int32_t Array_compare_TisCustomAttributeTypedArgument_t1692_m26629_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgument_t1692  p0, CustomAttributeTypedArgument_t1692  p1, Object_t* p2, const MethodInfo* method);
#define Array_compare_TisCustomAttributeTypedArgument_t1692_m26629(__this /* static, unused */, p0, p1, p2, method) (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeTypedArgument_t1692 , CustomAttributeTypedArgument_t1692 , Object_t*, const MethodInfo*))Array_compare_TisCustomAttributeTypedArgument_t1692_m26629_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Array::swap<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(!!0[],!!1[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeTypedArgument_t1692_TisCustomAttributeTypedArgument_t1692_m26630_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* p0, CustomAttributeTypedArgumentU5BU5D_t2129* p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_swap_TisCustomAttributeTypedArgument_t1692_TisCustomAttributeTypedArgument_t1692_m26630(__this /* static, unused */, p0, p1, p2, p3, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, const MethodInfo*))Array_swap_TisCustomAttributeTypedArgument_t1692_TisCustomAttributeTypedArgument_t1692_m26630_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Comparison`1<T>)
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t1692_m26631_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, int32_t ___length, Comparison_1_t3074 * ___comparison, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeTypedArgument_t1692_m26631(__this /* static, unused */, ___array, ___length, ___comparison, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, Comparison_1_t3074 *, const MethodInfo*))Array_Sort_TisCustomAttributeTypedArgument_t1692_m26631_gshared)(__this /* static, unused */, ___array, ___length, ___comparison, method)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeTypedArgument>(!!0[],System.Int32,System.Int32,System.Comparison`1<!!0>)
extern "C" void Array_qsort_TisCustomAttributeTypedArgument_t1692_m26632_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* p0, int32_t p1, int32_t p2, Comparison_1_t3074 * p3, const MethodInfo* method);
#define Array_qsort_TisCustomAttributeTypedArgument_t1692_m26632(__this /* static, unused */, p0, p1, p2, p3, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, Comparison_1_t3074 *, const MethodInfo*))Array_qsort_TisCustomAttributeTypedArgument_t1692_m26632_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::swap<System.Reflection.CustomAttributeTypedArgument>(!!0[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeTypedArgument_t1692_m26633_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define Array_swap_TisCustomAttributeTypedArgument_t1692_m26633(__this /* static, unused */, p0, p1, p2, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, const MethodInfo*))Array_swap_TisCustomAttributeTypedArgument_t1692_m26633_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(T[],T)
extern "C" int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t1692_m26634_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, CustomAttributeTypedArgument_t1692  ___value, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t1692_m26634(__this /* static, unused */, ___array, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t1692_m26634_gshared)(__this /* static, unused */, ___array, ___value, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C" CustomAttributeNamedArgumentU5BU5D_t2130* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t1691_m12767_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___values, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t1691_m12767(__this /* static, unused */, ___values, method) (( CustomAttributeNamedArgumentU5BU5D_t2130* (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t1691_m12767_gshared)(__this /* static, unused */, ___values, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Reflection.CustomAttributeNamedArgument>(T[])
extern "C" ReadOnlyCollection_1_t2132 * Array_AsReadOnly_TisCustomAttributeNamedArgument_t1691_m12768_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, const MethodInfo* method);
#define Array_AsReadOnly_TisCustomAttributeNamedArgument_t1691_m12768(__this /* static, unused */, ___array, method) (( ReadOnlyCollection_1_t2132 * (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, const MethodInfo*))Array_AsReadOnly_TisCustomAttributeNamedArgument_t1691_m12768_gshared)(__this /* static, unused */, ___array, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(T[]&,System.Int32)
extern "C" void Array_Resize_TisCustomAttributeNamedArgument_t1691_m26635_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130** ___array, int32_t ___newSize, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeNamedArgument_t1691_m26635(__this /* static, unused */, ___array, ___newSize, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130**, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeNamedArgument_t1691_m26635_gshared)(__this /* static, unused */, ___array, ___newSize, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(!!0[]&,System.Int32,System.Int32)
extern "C" void Array_Resize_TisCustomAttributeNamedArgument_t1691_m26636_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130** p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeNamedArgument_t1691_m26636(__this /* static, unused */, p0, p1, p2, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130**, int32_t, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeNamedArgument_t1691_m26636_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(T[],T,System.Int32,System.Int32)
extern "C" int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t1691_m26637_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, CustomAttributeNamedArgument_t1691  ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t1691_m26637(__this /* static, unused */, ___array, ___value, ___startIndex, ___count, method) (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, CustomAttributeNamedArgument_t1691 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t1691_m26637_gshared)(__this /* static, unused */, ___array, ___value, ___startIndex, ___count, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t1691_m26638_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, int32_t ___index, int32_t ___length, Object_t* ___comparer, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeNamedArgument_t1691_m26638(__this /* static, unused */, ___array, ___index, ___length, ___comparer, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_Sort_TisCustomAttributeNamedArgument_t1691_m26638_gshared)(__this /* static, unused */, ___array, ___index, ___length, ___comparer, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(!!0[],!!1[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t1691_TisCustomAttributeNamedArgument_t1691_m26639_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* p0, CustomAttributeNamedArgumentU5BU5D_t2130* p1, int32_t p2, int32_t p3, Object_t* p4, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeNamedArgument_t1691_TisCustomAttributeNamedArgument_t1691_m26639(__this /* static, unused */, p0, p1, p2, p3, p4, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_Sort_TisCustomAttributeNamedArgument_t1691_TisCustomAttributeNamedArgument_t1691_m26639_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Array/Swapper System.Array::get_swapper<System.Reflection.CustomAttributeNamedArgument>(!!0[])
extern "C" Swapper_t1448 * Array_get_swapper_TisCustomAttributeNamedArgument_t1691_m26640_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* p0, const MethodInfo* method);
#define Array_get_swapper_TisCustomAttributeNamedArgument_t1691_m26640(__this /* static, unused */, p0, method) (( Swapper_t1448 * (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, const MethodInfo*))Array_get_swapper_TisCustomAttributeNamedArgument_t1691_m26640_gshared)(__this /* static, unused */, p0, method)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(!!0[],!!1[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C" void Array_qsort_TisCustomAttributeNamedArgument_t1691_TisCustomAttributeNamedArgument_t1691_m26641_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* p0, CustomAttributeNamedArgumentU5BU5D_t2130* p1, int32_t p2, int32_t p3, Object_t* p4, const MethodInfo* method);
#define Array_qsort_TisCustomAttributeNamedArgument_t1691_TisCustomAttributeNamedArgument_t1691_m26641(__this /* static, unused */, p0, p1, p2, p3, p4, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_qsort_TisCustomAttributeNamedArgument_t1691_TisCustomAttributeNamedArgument_t1691_m26641_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Int32 System.Array::compare<System.Reflection.CustomAttributeNamedArgument>(!!0,!!0,System.Collections.Generic.IComparer`1<!!0>)
extern "C" int32_t Array_compare_TisCustomAttributeNamedArgument_t1691_m26642_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgument_t1691  p0, CustomAttributeNamedArgument_t1691  p1, Object_t* p2, const MethodInfo* method);
#define Array_compare_TisCustomAttributeNamedArgument_t1691_m26642(__this /* static, unused */, p0, p1, p2, method) (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeNamedArgument_t1691 , CustomAttributeNamedArgument_t1691 , Object_t*, const MethodInfo*))Array_compare_TisCustomAttributeNamedArgument_t1691_m26642_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Array::swap<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(!!0[],!!1[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeNamedArgument_t1691_TisCustomAttributeNamedArgument_t1691_m26643_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* p0, CustomAttributeNamedArgumentU5BU5D_t2130* p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_swap_TisCustomAttributeNamedArgument_t1691_TisCustomAttributeNamedArgument_t1691_m26643(__this /* static, unused */, p0, p1, p2, p3, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, const MethodInfo*))Array_swap_TisCustomAttributeNamedArgument_t1691_TisCustomAttributeNamedArgument_t1691_m26643_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Comparison`1<T>)
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t1691_m26644_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, int32_t ___length, Comparison_1_t3085 * ___comparison, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeNamedArgument_t1691_m26644(__this /* static, unused */, ___array, ___length, ___comparison, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, Comparison_1_t3085 *, const MethodInfo*))Array_Sort_TisCustomAttributeNamedArgument_t1691_m26644_gshared)(__this /* static, unused */, ___array, ___length, ___comparison, method)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeNamedArgument>(!!0[],System.Int32,System.Int32,System.Comparison`1<!!0>)
extern "C" void Array_qsort_TisCustomAttributeNamedArgument_t1691_m26645_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* p0, int32_t p1, int32_t p2, Comparison_1_t3085 * p3, const MethodInfo* method);
#define Array_qsort_TisCustomAttributeNamedArgument_t1691_m26645(__this /* static, unused */, p0, p1, p2, p3, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, Comparison_1_t3085 *, const MethodInfo*))Array_qsort_TisCustomAttributeNamedArgument_t1691_m26645_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::swap<System.Reflection.CustomAttributeNamedArgument>(!!0[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeNamedArgument_t1691_m26646_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define Array_swap_TisCustomAttributeNamedArgument_t1691_m26646(__this /* static, unused */, p0, p1, p2, method) (( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, const MethodInfo*))Array_swap_TisCustomAttributeNamedArgument_t1691_m26646_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(T[],T)
extern "C" int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t1691_m26647_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, CustomAttributeNamedArgument_t1691  ___value, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t1691_m26647(__this /* static, unused */, ___array, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, CustomAttributeNamedArgument_t1691 , const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t1691_m26647_gshared)(__this /* static, unused */, ___array, ___value, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C" ObjectU5BU5D_t105* CustomAttributeData_UnboxValues_TisObject_t_m26648_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___values, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisObject_t_m26648(__this /* static, unused */, ___values, method) (( ObjectU5BU5D_t105* (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, const MethodInfo*))CustomAttributeData_UnboxValues_TisObject_t_m26648_gshared)(__this /* static, unused */, ___values, method)
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
extern "C" Object_t * MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m26649_gshared (Object_t * __this /* static, unused */, Getter_2_t3088 * ___getter, Object_t * ___obj, const MethodInfo* method);
#define MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m26649(__this /* static, unused */, ___getter, ___obj, method) (( Object_t * (*) (Object_t * /* static, unused */, Getter_2_t3088 *, Object_t *, const MethodInfo*))MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m26649_gshared)(__this /* static, unused */, ___getter, ___obj, method)
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
extern "C" Object_t * MonoProperty_StaticGetterAdapterFrame_TisObject_t_m26650_gshared (Object_t * __this /* static, unused */, StaticGetter_1_t3089 * ___getter, Object_t * ___obj, const MethodInfo* method);
#define MonoProperty_StaticGetterAdapterFrame_TisObject_t_m26650(__this /* static, unused */, ___getter, ___obj, method) (( Object_t * (*) (Object_t * /* static, unused */, StaticGetter_1_t3089 *, Object_t *, const MethodInfo*))MonoProperty_StaticGetterAdapterFrame_TisObject_t_m26650_gshared)(__this /* static, unused */, ___getter, ___obj, method)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern "C" ResourceInfo_t1721  Array_InternalArray__get_Item_TisResourceInfo_t1721_m26651_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceInfo_t1721_m26651(__this, ___index, method) (( ResourceInfo_t1721  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceInfo_t1721_m26651_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceInfo>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisResourceInfo_t1721_m26722_gshared (Array_t * __this, int32_t p0, ResourceInfo_t1721 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisResourceInfo_t1721_m26722(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, ResourceInfo_t1721 *, const MethodInfo*))Array_GetGenericValueImpl_TisResourceInfo_t1721_m26722_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Resources.ResourceReader/ResourceInfo>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisResourceInfo_t1721_m26652_gshared (Array_t * __this, ResourceInfo_t1721  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisResourceInfo_t1721_m26652(__this, ___item, method) (( void (*) (Array_t *, ResourceInfo_t1721 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisResourceInfo_t1721_m26652_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Resources.ResourceReader/ResourceInfo>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisResourceInfo_t1721_m26653_gshared (Array_t * __this, ResourceInfo_t1721  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisResourceInfo_t1721_m26653(__this, ___item, method) (( bool (*) (Array_t *, ResourceInfo_t1721 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisResourceInfo_t1721_m26653_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Resources.ResourceReader/ResourceInfo>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisResourceInfo_t1721_m26654_gshared (Array_t * __this, ResourceInfoU5BU5D_t1725* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisResourceInfo_t1721_m26654(__this, ___array, ___index, method) (( void (*) (Array_t *, ResourceInfoU5BU5D_t1725*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisResourceInfo_t1721_m26654_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Resources.ResourceReader/ResourceInfo>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisResourceInfo_t1721_m26655_gshared (Array_t * __this, ResourceInfo_t1721  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisResourceInfo_t1721_m26655(__this, ___item, method) (( bool (*) (Array_t *, ResourceInfo_t1721 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisResourceInfo_t1721_m26655_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Resources.ResourceReader/ResourceInfo>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisResourceInfo_t1721_m26656_gshared (Array_t * __this, ResourceInfo_t1721  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisResourceInfo_t1721_m26656(__this, ___item, method) (( int32_t (*) (Array_t *, ResourceInfo_t1721 , const MethodInfo*))Array_InternalArray__IndexOf_TisResourceInfo_t1721_m26656_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisResourceInfo_t1721_m26657_gshared (Array_t * __this, int32_t ___index, ResourceInfo_t1721  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisResourceInfo_t1721_m26657(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, ResourceInfo_t1721 , const MethodInfo*))Array_InternalArray__Insert_TisResourceInfo_t1721_m26657_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisResourceInfo_t1721_m26658_gshared (Array_t * __this, int32_t ___index, ResourceInfo_t1721  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisResourceInfo_t1721_m26658(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, ResourceInfo_t1721 , const MethodInfo*))Array_InternalArray__set_Item_TisResourceInfo_t1721_m26658_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Resources.ResourceReader/ResourceInfo>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisResourceInfo_t1721_m26723_gshared (Array_t * __this, int32_t p0, ResourceInfo_t1721 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisResourceInfo_t1721_m26723(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, ResourceInfo_t1721 *, const MethodInfo*))Array_SetGenericValueImpl_TisResourceInfo_t1721_m26723_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Resources.ResourceReader/ResourceInfo>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisResourceInfo_t1721_m26659_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisResourceInfo_t1721_m26659(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisResourceInfo_t1721_m26659_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern "C" ResourceCacheItem_t1722  Array_InternalArray__get_Item_TisResourceCacheItem_t1722_m26660_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceCacheItem_t1722_m26660(__this, ___index, method) (( ResourceCacheItem_t1722  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceCacheItem_t1722_m26660_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisResourceCacheItem_t1722_m26724_gshared (Array_t * __this, int32_t p0, ResourceCacheItem_t1722 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisResourceCacheItem_t1722_m26724(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, ResourceCacheItem_t1722 *, const MethodInfo*))Array_GetGenericValueImpl_TisResourceCacheItem_t1722_m26724_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisResourceCacheItem_t1722_m26661_gshared (Array_t * __this, ResourceCacheItem_t1722  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisResourceCacheItem_t1722_m26661(__this, ___item, method) (( void (*) (Array_t *, ResourceCacheItem_t1722 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisResourceCacheItem_t1722_m26661_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisResourceCacheItem_t1722_m26662_gshared (Array_t * __this, ResourceCacheItem_t1722  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisResourceCacheItem_t1722_m26662(__this, ___item, method) (( bool (*) (Array_t *, ResourceCacheItem_t1722 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisResourceCacheItem_t1722_m26662_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Resources.ResourceReader/ResourceCacheItem>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisResourceCacheItem_t1722_m26663_gshared (Array_t * __this, ResourceCacheItemU5BU5D_t1726* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisResourceCacheItem_t1722_m26663(__this, ___array, ___index, method) (( void (*) (Array_t *, ResourceCacheItemU5BU5D_t1726*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisResourceCacheItem_t1722_m26663_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisResourceCacheItem_t1722_m26664_gshared (Array_t * __this, ResourceCacheItem_t1722  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisResourceCacheItem_t1722_m26664(__this, ___item, method) (( bool (*) (Array_t *, ResourceCacheItem_t1722 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisResourceCacheItem_t1722_m26664_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisResourceCacheItem_t1722_m26665_gshared (Array_t * __this, ResourceCacheItem_t1722  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisResourceCacheItem_t1722_m26665(__this, ___item, method) (( int32_t (*) (Array_t *, ResourceCacheItem_t1722 , const MethodInfo*))Array_InternalArray__IndexOf_TisResourceCacheItem_t1722_m26665_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisResourceCacheItem_t1722_m26666_gshared (Array_t * __this, int32_t ___index, ResourceCacheItem_t1722  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisResourceCacheItem_t1722_m26666(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, ResourceCacheItem_t1722 , const MethodInfo*))Array_InternalArray__Insert_TisResourceCacheItem_t1722_m26666_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisResourceCacheItem_t1722_m26667_gshared (Array_t * __this, int32_t ___index, ResourceCacheItem_t1722  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisResourceCacheItem_t1722_m26667(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, ResourceCacheItem_t1722 , const MethodInfo*))Array_InternalArray__set_Item_TisResourceCacheItem_t1722_m26667_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisResourceCacheItem_t1722_m26725_gshared (Array_t * __this, int32_t p0, ResourceCacheItem_t1722 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisResourceCacheItem_t1722_m26725(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, ResourceCacheItem_t1722 *, const MethodInfo*))Array_SetGenericValueImpl_TisResourceCacheItem_t1722_m26725_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Resources.ResourceReader/ResourceCacheItem>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisResourceCacheItem_t1722_m26668_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisResourceCacheItem_t1722_m26668(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisResourceCacheItem_t1722_m26668_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern "C" DateTime_t577  Array_InternalArray__get_Item_TisDateTime_t577_m26669_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDateTime_t577_m26669(__this, ___index, method) (( DateTime_t577  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDateTime_t577_m26669_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.DateTime>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisDateTime_t577_m26726_gshared (Array_t * __this, int32_t p0, DateTime_t577 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDateTime_t577_m26726(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, DateTime_t577 *, const MethodInfo*))Array_GetGenericValueImpl_TisDateTime_t577_m26726_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t577_m26670_gshared (Array_t * __this, DateTime_t577  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisDateTime_t577_m26670(__this, ___item, method) (( void (*) (Array_t *, DateTime_t577 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisDateTime_t577_m26670_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisDateTime_t577_m26671_gshared (Array_t * __this, DateTime_t577  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisDateTime_t577_m26671(__this, ___item, method) (( bool (*) (Array_t *, DateTime_t577 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisDateTime_t577_m26671_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t577_m26672_gshared (Array_t * __this, DateTimeU5BU5D_t2152* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisDateTime_t577_m26672(__this, ___array, ___index, method) (( void (*) (Array_t *, DateTimeU5BU5D_t2152*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisDateTime_t577_m26672_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisDateTime_t577_m26673_gshared (Array_t * __this, DateTime_t577  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisDateTime_t577_m26673(__this, ___item, method) (( bool (*) (Array_t *, DateTime_t577 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisDateTime_t577_m26673_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisDateTime_t577_m26674_gshared (Array_t * __this, DateTime_t577  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisDateTime_t577_m26674(__this, ___item, method) (( int32_t (*) (Array_t *, DateTime_t577 , const MethodInfo*))Array_InternalArray__IndexOf_TisDateTime_t577_m26674_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisDateTime_t577_m26675_gshared (Array_t * __this, int32_t ___index, DateTime_t577  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisDateTime_t577_m26675(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, DateTime_t577 , const MethodInfo*))Array_InternalArray__Insert_TisDateTime_t577_m26675_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisDateTime_t577_m26676_gshared (Array_t * __this, int32_t ___index, DateTime_t577  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisDateTime_t577_m26676(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, DateTime_t577 , const MethodInfo*))Array_InternalArray__set_Item_TisDateTime_t577_m26676_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.DateTime>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisDateTime_t577_m26727_gshared (Array_t * __this, int32_t p0, DateTime_t577 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisDateTime_t577_m26727(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, DateTime_t577 *, const MethodInfo*))Array_SetGenericValueImpl_TisDateTime_t577_m26727_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t577_m26677_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t577_m26677(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t577_m26677_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern "C" Decimal_t1445  Array_InternalArray__get_Item_TisDecimal_t1445_m26678_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDecimal_t1445_m26678(__this, ___index, method) (( Decimal_t1445  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDecimal_t1445_m26678_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Decimal>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisDecimal_t1445_m26728_gshared (Array_t * __this, int32_t p0, Decimal_t1445 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDecimal_t1445_m26728(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, Decimal_t1445 *, const MethodInfo*))Array_GetGenericValueImpl_TisDecimal_t1445_m26728_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t1445_m26679_gshared (Array_t * __this, Decimal_t1445  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisDecimal_t1445_m26679(__this, ___item, method) (( void (*) (Array_t *, Decimal_t1445 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisDecimal_t1445_m26679_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisDecimal_t1445_m26680_gshared (Array_t * __this, Decimal_t1445  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisDecimal_t1445_m26680(__this, ___item, method) (( bool (*) (Array_t *, Decimal_t1445 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisDecimal_t1445_m26680_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t1445_m26681_gshared (Array_t * __this, DecimalU5BU5D_t2153* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisDecimal_t1445_m26681(__this, ___array, ___index, method) (( void (*) (Array_t *, DecimalU5BU5D_t2153*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisDecimal_t1445_m26681_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisDecimal_t1445_m26682_gshared (Array_t * __this, Decimal_t1445  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisDecimal_t1445_m26682(__this, ___item, method) (( bool (*) (Array_t *, Decimal_t1445 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisDecimal_t1445_m26682_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisDecimal_t1445_m26683_gshared (Array_t * __this, Decimal_t1445  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisDecimal_t1445_m26683(__this, ___item, method) (( int32_t (*) (Array_t *, Decimal_t1445 , const MethodInfo*))Array_InternalArray__IndexOf_TisDecimal_t1445_m26683_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisDecimal_t1445_m26684_gshared (Array_t * __this, int32_t ___index, Decimal_t1445  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisDecimal_t1445_m26684(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Decimal_t1445 , const MethodInfo*))Array_InternalArray__Insert_TisDecimal_t1445_m26684_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisDecimal_t1445_m26685_gshared (Array_t * __this, int32_t ___index, Decimal_t1445  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisDecimal_t1445_m26685(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Decimal_t1445 , const MethodInfo*))Array_InternalArray__set_Item_TisDecimal_t1445_m26685_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Decimal>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisDecimal_t1445_m26729_gshared (Array_t * __this, int32_t p0, Decimal_t1445 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisDecimal_t1445_m26729(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, Decimal_t1445 *, const MethodInfo*))Array_SetGenericValueImpl_TisDecimal_t1445_m26729_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1445_m26686_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1445_m26686(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1445_m26686_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C" TimeSpan_t975  Array_InternalArray__get_Item_TisTimeSpan_t975_m26687_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t975_m26687(__this, ___index, method) (( TimeSpan_t975  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t975_m26687_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisTimeSpan_t975_m26730_gshared (Array_t * __this, int32_t p0, TimeSpan_t975 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTimeSpan_t975_m26730(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, TimeSpan_t975 *, const MethodInfo*))Array_GetGenericValueImpl_TisTimeSpan_t975_m26730_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t975_m26688_gshared (Array_t * __this, TimeSpan_t975  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisTimeSpan_t975_m26688(__this, ___item, method) (( void (*) (Array_t *, TimeSpan_t975 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisTimeSpan_t975_m26688_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisTimeSpan_t975_m26689_gshared (Array_t * __this, TimeSpan_t975  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisTimeSpan_t975_m26689(__this, ___item, method) (( bool (*) (Array_t *, TimeSpan_t975 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisTimeSpan_t975_m26689_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t975_m26690_gshared (Array_t * __this, TimeSpanU5BU5D_t2154* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t975_m26690(__this, ___array, ___index, method) (( void (*) (Array_t *, TimeSpanU5BU5D_t2154*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t975_m26690_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisTimeSpan_t975_m26691_gshared (Array_t * __this, TimeSpan_t975  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisTimeSpan_t975_m26691(__this, ___item, method) (( bool (*) (Array_t *, TimeSpan_t975 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisTimeSpan_t975_m26691_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisTimeSpan_t975_m26692_gshared (Array_t * __this, TimeSpan_t975  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisTimeSpan_t975_m26692(__this, ___item, method) (( int32_t (*) (Array_t *, TimeSpan_t975 , const MethodInfo*))Array_InternalArray__IndexOf_TisTimeSpan_t975_m26692_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t975_m26693_gshared (Array_t * __this, int32_t ___index, TimeSpan_t975  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisTimeSpan_t975_m26693(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TimeSpan_t975 , const MethodInfo*))Array_InternalArray__Insert_TisTimeSpan_t975_m26693_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t975_m26694_gshared (Array_t * __this, int32_t ___index, TimeSpan_t975  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisTimeSpan_t975_m26694(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TimeSpan_t975 , const MethodInfo*))Array_InternalArray__set_Item_TisTimeSpan_t975_m26694_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.TimeSpan>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisTimeSpan_t975_m26731_gshared (Array_t * __this, int32_t p0, TimeSpan_t975 * p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisTimeSpan_t975_m26731(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, TimeSpan_t975 *, const MethodInfo*))Array_SetGenericValueImpl_TisTimeSpan_t975_m26731_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t975_m26695_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t975_m26695(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t975_m26695_gshared)(__this, method)
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C" uint8_t Array_InternalArray__get_Item_TisTypeTag_t1858_m26696_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeTag_t1858_m26696(__this, ___index, method) (( uint8_t (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeTag_t1858_m26696_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,!!0&)
extern "C" void Array_GetGenericValueImpl_TisTypeTag_t1858_m26732_gshared (Array_t * __this, int32_t p0, uint8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTypeTag_t1858_m26732(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, uint8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisTypeTag_t1858_m26732_gshared)(__this, p0, p1, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisTypeTag_t1858_m26697_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisTypeTag_t1858_m26697(__this, ___item, method) (( void (*) (Array_t *, uint8_t, const MethodInfo*))Array_InternalArray__ICollection_Add_TisTypeTag_t1858_m26697_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisTypeTag_t1858_m26698_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisTypeTag_t1858_m26698(__this, ___item, method) (( bool (*) (Array_t *, uint8_t, const MethodInfo*))Array_InternalArray__ICollection_Contains_TisTypeTag_t1858_m26698_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTypeTag_t1858_m26699_gshared (Array_t * __this, TypeTagU5BU5D_t2155* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisTypeTag_t1858_m26699(__this, ___array, ___index, method) (( void (*) (Array_t *, TypeTagU5BU5D_t2155*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisTypeTag_t1858_m26699_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisTypeTag_t1858_m26700_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisTypeTag_t1858_m26700(__this, ___item, method) (( bool (*) (Array_t *, uint8_t, const MethodInfo*))Array_InternalArray__ICollection_Remove_TisTypeTag_t1858_m26700_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisTypeTag_t1858_m26701_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisTypeTag_t1858_m26701(__this, ___item, method) (( int32_t (*) (Array_t *, uint8_t, const MethodInfo*))Array_InternalArray__IndexOf_TisTypeTag_t1858_m26701_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisTypeTag_t1858_m26702_gshared (Array_t * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisTypeTag_t1858_m26702(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, uint8_t, const MethodInfo*))Array_InternalArray__Insert_TisTypeTag_t1858_m26702_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisTypeTag_t1858_m26703_gshared (Array_t * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisTypeTag_t1858_m26703(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, uint8_t, const MethodInfo*))Array_InternalArray__set_Item_TisTypeTag_t1858_m26703_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,!!0&)
extern "C" void Array_SetGenericValueImpl_TisTypeTag_t1858_m26733_gshared (Array_t * __this, int32_t p0, uint8_t* p1, const MethodInfo* method);
#define Array_SetGenericValueImpl_TisTypeTag_t1858_m26733(__this, p0, p1, method) (( void (*) (Array_t *, int32_t, uint8_t*, const MethodInfo*))Array_SetGenericValueImpl_TisTypeTag_t1858_m26733_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.TypeTag>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t1858_m26704_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t1858_m26704(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t1858_m26704_gshared)(__this, method)
// System.Void System.Array::GetGenericValueImpl<System.Int64>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Int64>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Int64>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisInt64_t711_m26515_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Int64>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Int64>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisInt64_t711_m26516_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int64_t V_2 = 0;
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (int64_t*)(&V_2));
		int64_t L_5 = ___item;
		goto IL_004d;
	}
	{
		int64_t L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		int64_t L_7 = V_2;
		int64_t L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((int64_t*)(&___item));
		bool L_10 = Int64_Equals_m7696((int64_t*)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Int64>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Int64>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt64_t711_m26517_gshared (Array_t * __this, Int64U5BU5D_t2103* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int64U5BU5D_t2103* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Int64U5BU5D_t2103* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		Int64U5BU5D_t2103* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		Int64U5BU5D_t2103* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Int64U5BU5D_t2103* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Int64>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Int64>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisInt64_t711_m26518_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Int64>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Int64>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisInt64_t711_m26519_gshared (Array_t * __this, int64_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int64_t V_2 = 0;
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (int64_t*)(&V_2));
		int64_t L_5 = ___item;
		goto IL_005d;
	}
	{
		int64_t L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		int64_t L_10 = ___item;
		int64_t L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((int64_t*)(&V_2));
		bool L_13 = Int64_Equals_m7696((int64_t*)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Int64>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Int64>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisInt64_t711_m26520_gshared (Array_t * __this, int32_t ___index, int64_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Int64>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Int64>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisInt64_t711_m26521_gshared (Array_t * __this, int32_t ___index, int64_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		int64_t L_6 = ___item;
		int64_t L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (int64_t*)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Int64>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Int64>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Int64>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t711_m26522_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3033  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3033 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3033  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Void System.Array::Sort<System.Object>(T[])
// System.Void System.Array::Sort<System.Object>(T[])
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" void Array_Sort_TisObject_t_m26523_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		ObjectU5BU5D_t105* L_3 = ___array;
		NullCheck(L_3);
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, ObjectU5BU5D_t105*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (ObjectU5BU5D_t105*)(ObjectU5BU5D_t105*)NULL, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[])
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[])
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1354;
extern "C" void Array_Sort_TisObject_t_TisObject_t_m26524_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___keys, ObjectU5BU5D_t105* ___items, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral1354 = il2cpp_codegen_string_literal_from_index(1354);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___keys;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral1354, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___keys;
		ObjectU5BU5D_t105* L_3 = ___items;
		ObjectU5BU5D_t105* L_4 = ___keys;
		NullCheck(L_4);
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, ObjectU5BU5D_t105*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (ObjectU5BU5D_t105*)L_3, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))), (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Object>(T[],System.Collections.Generic.IComparer`1<T>)
// System.Void System.Array::Sort<System.Object>(T[],System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" void Array_Sort_TisObject_t_m26525_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		ObjectU5BU5D_t105* L_3 = ___array;
		NullCheck(L_3);
		Object_t* L_4 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, ObjectU5BU5D_t105*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (ObjectU5BU5D_t105*)(ObjectU5BU5D_t105*)NULL, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Object_t*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[],System.Collections.Generic.IComparer`1<TKey>)
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[],System.Collections.Generic.IComparer`1<TKey>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1354;
extern "C" void Array_Sort_TisObject_t_TisObject_t_m26526_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___keys, ObjectU5BU5D_t105* ___items, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral1354 = il2cpp_codegen_string_literal_from_index(1354);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___keys;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral1354, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___keys;
		ObjectU5BU5D_t105* L_3 = ___items;
		ObjectU5BU5D_t105* L_4 = ___keys;
		NullCheck(L_4);
		Object_t* L_5 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, ObjectU5BU5D_t105*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (ObjectU5BU5D_t105*)L_3, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))), (Object_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Object>(T[],System.Int32,System.Int32)
// System.Void System.Array::Sort<System.Object>(T[],System.Int32,System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" void Array_Sort_TisObject_t_m12763_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, int32_t ___index, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		int32_t L_3 = ___index;
		int32_t L_4 = ___length;
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, ObjectU5BU5D_t105*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (ObjectU5BU5D_t105*)(ObjectU5BU5D_t105*)NULL, (int32_t)L_3, (int32_t)L_4, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[],System.Int32,System.Int32)
// System.Void System.Array::Sort<System.Object,System.Object>(TKey[],TValue[],System.Int32,System.Int32)
extern "C" void Array_Sort_TisObject_t_TisObject_t_m26527_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___keys, ObjectU5BU5D_t105* ___items, int32_t ___index, int32_t ___length, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t105* L_0 = ___keys;
		ObjectU5BU5D_t105* L_1 = ___items;
		int32_t L_2 = ___index;
		int32_t L_3 = ___length;
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, ObjectU5BU5D_t105*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_0, (ObjectU5BU5D_t105*)L_1, (int32_t)L_2, (int32_t)L_3, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Object>(T[],System.Comparison`1<T>)
// System.Void System.Array::Sort<System.Object>(T[],System.Comparison`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" void Array_Sort_TisObject_t_m26528_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Comparison_1_t2186 * ___comparison, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		ObjectU5BU5D_t105* L_3 = ___array;
		NullCheck(L_3);
		Comparison_1_t2186 * L_4 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, Comparison_1_t2186 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Comparison_1_t2186 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Array::TrueForAll<System.Object>(T[],System.Predicate`1<T>)
// System.Boolean System.Array::TrueForAll<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral2077;
extern "C" bool Array_TrueForAll_TisObject_t_m26529_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral2077 = il2cpp_codegen_string_literal_from_index(2077);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ObjectU5BU5D_t105* V_1 = {0};
	int32_t V_2 = 0;
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2180 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t718 * L_3 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_3, (String_t*)_stringLiteral2077, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t105* L_4 = ___array;
		V_1 = (ObjectU5BU5D_t105*)L_4;
		V_2 = (int32_t)0;
		goto IL_0045;
	}

IL_002b:
	{
		ObjectU5BU5D_t105* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_5, L_7, sizeof(Object_t *)));
		Predicate_1_t2180 * L_8 = ___match;
		Object_t * L_9 = V_0;
		NullCheck((Predicate_1_t2180 *)L_8);
		bool L_10 = (( bool (*) (Predicate_1_t2180 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Predicate_1_t2180 *)L_8, (Object_t *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_10)
		{
			goto IL_0041;
		}
	}
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_11 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_12 = V_2;
		ObjectU5BU5D_t105* L_13 = V_1;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_13)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		return 1;
	}
}
// System.Void System.Array::ForEach<System.Object>(T[],System.Action`1<T>)
// System.Void System.Array::ForEach<System.Object>(T[],System.Action`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral2977;
extern "C" void Array_ForEach_TisObject_t_m26530_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Action_1_t2195 * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral2977 = il2cpp_codegen_string_literal_from_index(2977);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ObjectU5BU5D_t105* V_1 = {0};
	int32_t V_2 = 0;
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Action_1_t2195 * L_2 = ___action;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t718 * L_3 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_3, (String_t*)_stringLiteral2977, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t105* L_4 = ___array;
		V_1 = (ObjectU5BU5D_t105*)L_4;
		V_2 = (int32_t)0;
		goto IL_003e;
	}

IL_002b:
	{
		ObjectU5BU5D_t105* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_5, L_7, sizeof(Object_t *)));
		Action_1_t2195 * L_8 = ___action;
		Object_t * L_9 = V_0;
		NullCheck((Action_1_t2195 *)L_8);
		(( void (*) (Action_1_t2195 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Action_1_t2195 *)L_8, (Object_t *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		int32_t L_10 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_11 = V_2;
		ObjectU5BU5D_t105* L_12 = V_1;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_12)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		return;
	}
}
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral2348;
extern "C" ObjectU5BU5D_t105* Array_ConvertAll_TisObject_t_TisObject_t_m26531_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Converter_2_t3034 * ___converter, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral2348 = il2cpp_codegen_string_literal_from_index(2348);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Converter_2_t3034 * L_2 = ___converter;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t718 * L_3 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_3, (String_t*)_stringLiteral2348, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t105* L_4 = ___array;
		NullCheck(L_4);
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (((int32_t)((int32_t)(((Array_t *)L_4)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_004a;
	}

IL_0032:
	{
		ObjectU5BU5D_t105* L_5 = V_0;
		int32_t L_6 = V_1;
		Converter_2_t3034 * L_7 = ___converter;
		ObjectU5BU5D_t105* L_8 = ___array;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		NullCheck((Converter_2_t3034 *)L_7);
		Object_t * L_11 = (( Object_t * (*) (Converter_2_t3034 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((Converter_2_t3034 *)L_7, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_8, L_10, sizeof(Object_t *))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, L_6, sizeof(Object_t *))) = (Object_t *)L_11;
		int32_t L_12 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_13 = V_1;
		ObjectU5BU5D_t105* L_14 = ___array;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_14)->max_length)))))))
		{
			goto IL_0032;
		}
	}
	{
		ObjectU5BU5D_t105* L_15 = V_0;
		return L_15;
	}
}
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Predicate`1<T>)
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_FindLastIndex_TisObject_t_m26532_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		ObjectU5BU5D_t105* L_3 = ___array;
		NullCheck(L_3);
		Predicate_1_t2180 * L_4 = ___match;
		int32_t L_5 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, int32_t, Predicate_1_t2180 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Predicate_1_t2180 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_5;
	}
}
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Int32,System.Int32,System.Predicate`1<T>)
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Int32,System.Int32,System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral2077;
extern "C" int32_t Array_FindLastIndex_TisObject_t_m26533_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, int32_t ___startIndex, int32_t ___count, Predicate_1_t2180 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral2077 = il2cpp_codegen_string_literal_from_index(2077);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2180 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t718 * L_3 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_3, (String_t*)_stringLiteral2077, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___startIndex;
		ObjectU5BU5D_t105* L_5 = ___array;
		NullCheck(L_5);
		if ((((int32_t)L_4) > ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_6 = ___startIndex;
		int32_t L_7 = ___count;
		ObjectU5BU5D_t105* L_8 = ___array;
		NullCheck(L_8);
		if ((((int32_t)((int32_t)((int32_t)L_6+(int32_t)L_7))) <= ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_003c;
		}
	}

IL_0036:
	{
		ArgumentOutOfRangeException_t1083 * L_9 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7596(L_9, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_003c:
	{
		int32_t L_10 = ___startIndex;
		int32_t L_11 = ___count;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10+(int32_t)L_11))-(int32_t)1));
		goto IL_005f;
	}

IL_0047:
	{
		Predicate_1_t2180 * L_12 = ___match;
		ObjectU5BU5D_t105* L_13 = ___array;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		NullCheck((Predicate_1_t2180 *)L_12);
		bool L_16 = (( bool (*) (Predicate_1_t2180 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Predicate_1_t2180 *)L_12, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_13, L_15, sizeof(Object_t *))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_16)
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_17 = V_0;
		return L_17;
	}

IL_005b:
	{
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18-(int32_t)1));
	}

IL_005f:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = ___startIndex;
		if ((((int32_t)L_19) >= ((int32_t)L_20)))
		{
			goto IL_0047;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Int32,System.Predicate`1<T>)
// System.Int32 System.Array::FindLastIndex<System.Object>(T[],System.Int32,System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern "C" int32_t Array_FindLastIndex_TisObject_t_m26534_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, int32_t ___startIndex, Predicate_1_t2180 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m11926(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_000c:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		int32_t L_3 = ___startIndex;
		ObjectU5BU5D_t105* L_4 = ___array;
		NullCheck(L_4);
		int32_t L_5 = ___startIndex;
		Predicate_1_t2180 * L_6 = ___match;
		int32_t L_7 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, int32_t, Predicate_1_t2180 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (int32_t)L_3, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length))))-(int32_t)L_5)), (Predicate_1_t2180 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_7;
	}
}
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Predicate`1<T>)
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_FindIndex_TisObject_t_m26535_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		ObjectU5BU5D_t105* L_3 = ___array;
		NullCheck(L_3);
		Predicate_1_t2180 * L_4 = ___match;
		int32_t L_5 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, int32_t, Predicate_1_t2180 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Predicate_1_t2180 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_5;
	}
}
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Int32,System.Int32,System.Predicate`1<T>)
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Int32,System.Int32,System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral2077;
extern "C" int32_t Array_FindIndex_TisObject_t_m26536_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, int32_t ___startIndex, int32_t ___count, Predicate_1_t2180 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral2077 = il2cpp_codegen_string_literal_from_index(2077);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2180 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t718 * L_3 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_3, (String_t*)_stringLiteral2077, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___startIndex;
		ObjectU5BU5D_t105* L_5 = ___array;
		NullCheck(L_5);
		if ((((int32_t)L_4) > ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_6 = ___startIndex;
		int32_t L_7 = ___count;
		ObjectU5BU5D_t105* L_8 = ___array;
		NullCheck(L_8);
		if ((((int32_t)((int32_t)((int32_t)L_6+(int32_t)L_7))) <= ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_003c;
		}
	}

IL_0036:
	{
		ArgumentOutOfRangeException_t1083 * L_9 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7596(L_9, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_003c:
	{
		int32_t L_10 = ___startIndex;
		V_0 = (int32_t)L_10;
		goto IL_005b;
	}

IL_0043:
	{
		Predicate_1_t2180 * L_11 = ___match;
		ObjectU5BU5D_t105* L_12 = ___array;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		NullCheck((Predicate_1_t2180 *)L_11);
		bool L_15 = (( bool (*) (Predicate_1_t2180 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Predicate_1_t2180 *)L_11, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_12, L_14, sizeof(Object_t *))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_15)
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_16 = V_0;
		return L_16;
	}

IL_0057:
	{
		int32_t L_17 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_18 = V_0;
		int32_t L_19 = ___startIndex;
		int32_t L_20 = ___count;
		if ((((int32_t)L_18) < ((int32_t)((int32_t)((int32_t)L_19+(int32_t)L_20)))))
		{
			goto IL_0043;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Int32,System.Predicate`1<T>)
// System.Int32 System.Array::FindIndex<System.Object>(T[],System.Int32,System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_FindIndex_TisObject_t_m26537_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, int32_t ___startIndex, Predicate_1_t2180 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		int32_t L_3 = ___startIndex;
		ObjectU5BU5D_t105* L_4 = ___array;
		NullCheck(L_4);
		int32_t L_5 = ___startIndex;
		Predicate_1_t2180 * L_6 = ___match;
		int32_t L_7 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, int32_t, Predicate_1_t2180 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (int32_t)L_3, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length))))-(int32_t)L_5)), (Predicate_1_t2180 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_7;
	}
}
// System.Int32 System.Array::BinarySearch<System.Object>(T[],T)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],T)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_BinarySearch_TisObject_t_m26538_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		ObjectU5BU5D_t105* L_3 = ___array;
		NullCheck(L_3);
		Object_t * L_4 = ___value;
		int32_t L_5 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, int32_t, Object_t *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Object_t *)L_4, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_5;
	}
}
// System.Int32 System.Array::BinarySearch<System.Object>(T[],System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t107_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1256_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1341;
extern Il2CppCodeGenString* _stringLiteral1228;
extern Il2CppCodeGenString* _stringLiteral1342;
extern Il2CppCodeGenString* _stringLiteral1343;
extern Il2CppCodeGenString* _stringLiteral1345;
extern "C" int32_t Array_BinarySearch_TisObject_t_m26539_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, int32_t ___index, int32_t ___length, Object_t * ___value, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		Exception_t107_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		InvalidOperationException_t1256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(770);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1341 = il2cpp_codegen_string_literal_from_index(1341);
		_stringLiteral1228 = il2cpp_codegen_string_literal_from_index(1228);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		_stringLiteral1343 = il2cpp_codegen_string_literal_from_index(1343);
		_stringLiteral1345 = il2cpp_codegen_string_literal_from_index(1345);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t107 * V_4 = {0};
	int32_t V_5 = 0;
	Exception_t107 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t107 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1341, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_4 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_4, (String_t*)_stringLiteral863, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___length;
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_0049;
		}
	}
	{
		String_t* L_6 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_7 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_7, (String_t*)_stringLiteral1228, (String_t*)L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_0049:
	{
		int32_t L_8 = ___index;
		ObjectU5BU5D_t105* L_9 = ___array;
		NullCheck(L_9);
		int32_t L_10 = ___length;
		if ((((int32_t)L_8) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length))))-(int32_t)L_10)))))
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1343, /*hidden argument*/NULL);
		ArgumentException_t442 * L_12 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_12, (String_t*)L_11, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_0064:
	{
		Object_t* L_13 = ___comparer;
		if (L_13)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		Comparer_1_t2181 * L_14 = (( Comparer_1_t2181 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		___comparer = (Object_t*)L_14;
	}

IL_0072:
	{
		int32_t L_15 = ___index;
		V_0 = (int32_t)L_15;
		int32_t L_16 = ___index;
		int32_t L_17 = ___length;
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16+(int32_t)L_17))-(int32_t)1));
		V_2 = (int32_t)0;
	}

IL_007c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00bb;
		}

IL_0081:
		{
			int32_t L_18 = V_0;
			int32_t L_19 = V_1;
			int32_t L_20 = V_0;
			V_3 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_19-(int32_t)L_20))/(int32_t)2))));
			Object_t* L_21 = ___comparer;
			Object_t * L_22 = ___value;
			ObjectU5BU5D_t105* L_23 = ___array;
			int32_t L_24 = V_3;
			NullCheck(L_23);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
			int32_t L_25 = L_24;
			NullCheck((Object_t*)L_21);
			int32_t L_26 = (int32_t)InterfaceFuncInvoker2< int32_t, Object_t *, Object_t * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(T,T) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Object_t*)L_21, (Object_t *)L_22, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_23, L_25, sizeof(Object_t *))));
			V_2 = (int32_t)L_26;
			int32_t L_27 = V_2;
			if (L_27)
			{
				goto IL_00a7;
			}
		}

IL_009f:
		{
			int32_t L_28 = V_3;
			V_5 = (int32_t)L_28;
			goto IL_00e3;
		}

IL_00a7:
		{
			int32_t L_29 = V_2;
			if ((((int32_t)L_29) >= ((int32_t)0)))
			{
				goto IL_00b7;
			}
		}

IL_00ae:
		{
			int32_t L_30 = V_3;
			V_1 = (int32_t)((int32_t)((int32_t)L_30-(int32_t)1));
			goto IL_00bb;
		}

IL_00b7:
		{
			int32_t L_31 = V_3;
			V_0 = (int32_t)((int32_t)((int32_t)L_31+(int32_t)1));
		}

IL_00bb:
		{
			int32_t L_32 = V_0;
			int32_t L_33 = V_1;
			if ((((int32_t)L_32) <= ((int32_t)L_33)))
			{
				goto IL_0081;
			}
		}

IL_00c2:
		{
			goto IL_00e0;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t107 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t107_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00c7;
		throw e;
	}

CATCH_00c7:
	{ // begin catch(System.Exception)
		{
			V_4 = (Exception_t107 *)((Exception_t107 *)__exception_local);
			String_t* L_34 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1345, /*hidden argument*/NULL);
			Exception_t107 * L_35 = V_4;
			InvalidOperationException_t1256 * L_36 = (InvalidOperationException_t1256 *)il2cpp_codegen_object_new (InvalidOperationException_t1256_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m12351(L_36, (String_t*)L_34, (Exception_t107 *)L_35, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_36);
		}

IL_00db:
		{
			goto IL_00e0;
		}
	} // end catch (depth: 1)

IL_00e0:
	{
		int32_t L_37 = V_0;
		return ((~L_37));
	}

IL_00e3:
	{
		int32_t L_38 = V_5;
		return L_38;
	}
}
// System.Int32 System.Array::BinarySearch<System.Object>(T[],T,System.Collections.Generic.IComparer`1<T>)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],T,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_BinarySearch_TisObject_t_m26540_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t * ___value, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		ObjectU5BU5D_t105* L_3 = ___array;
		NullCheck(L_3);
		Object_t * L_4 = ___value;
		Object_t* L_5 = ___comparer;
		int32_t L_6 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, int32_t, Object_t *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), (Object_t *)L_4, (Object_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_6;
	}
}
// System.Int32 System.Array::BinarySearch<System.Object>(T[],System.Int32,System.Int32,T)
// System.Int32 System.Array::BinarySearch<System.Object>(T[],System.Int32,System.Int32,T)
extern "C" int32_t Array_BinarySearch_TisObject_t_m26541_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, int32_t ___index, int32_t ___length, Object_t * ___value, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		int32_t L_1 = ___index;
		int32_t L_2 = ___length;
		Object_t * L_3 = ___value;
		int32_t L_4 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, int32_t, int32_t, Object_t *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_0, (int32_t)L_1, (int32_t)L_2, (Object_t *)L_3, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_4;
	}
}
// System.Int32 System.Array::IndexOf<System.Object>(T[],T)
// System.Int32 System.Array::IndexOf<System.Object>(T[],T)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_IndexOf_TisObject_t_m12769_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		Object_t * L_3 = ___value;
		ObjectU5BU5D_t105* L_4 = ___array;
		NullCheck(L_4);
		int32_t L_5 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Object_t *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (Object_t *)L_3, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_5;
	}
}
// System.Int32 System.Array::IndexOf<System.Object>(T[],T,System.Int32)
// System.Int32 System.Array::IndexOf<System.Object>(T[],T,System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_IndexOf_TisObject_t_m26542_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t * ___value, int32_t ___startIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		Object_t * L_3 = ___value;
		int32_t L_4 = ___startIndex;
		ObjectU5BU5D_t105* L_5 = ___array;
		NullCheck(L_5);
		int32_t L_6 = ___startIndex;
		int32_t L_7 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Object_t *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (Object_t *)L_3, (int32_t)L_4, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length))))-(int32_t)L_6)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_7;
	}
}
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T)
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_LastIndexOf_TisObject_t_m26543_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))))
		{
			goto IL_001b;
		}
	}
	{
		return (-1);
	}

IL_001b:
	{
		ObjectU5BU5D_t105* L_3 = ___array;
		Object_t * L_4 = ___value;
		ObjectU5BU5D_t105* L_5 = ___array;
		NullCheck(L_5);
		int32_t L_6 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Object_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_3, (Object_t *)L_4, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length))))-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_6;
	}
}
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T,System.Int32)
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T,System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_LastIndexOf_TisObject_t_m26544_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t * ___value, int32_t ___startIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		Object_t * L_3 = ___value;
		int32_t L_4 = ___startIndex;
		int32_t L_5 = ___startIndex;
		int32_t L_6 = (( int32_t (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105*, Object_t *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105*)L_2, (Object_t *)L_3, (int32_t)L_4, (int32_t)((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_6;
	}
}
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T,System.Int32,System.Int32)
// System.Int32 System.Array::LastIndexOf<System.Object>(T[],T,System.Int32,System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_LastIndexOf_TisObject_t_m26545_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Object_t * ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t2177 * V_0 = {0};
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___count;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_3 = ___startIndex;
		ObjectU5BU5D_t105* L_4 = ___array;
		NullCheck((Array_t *)L_4);
		int32_t L_5 = Array_GetLowerBound_m8295((Array_t *)L_4, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)L_3) < ((int32_t)L_5)))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_6 = ___startIndex;
		ObjectU5BU5D_t105* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetUpperBound_m8307((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)L_6) > ((int32_t)L_8)))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_9 = ___startIndex;
		int32_t L_10 = ___count;
		ObjectU5BU5D_t105* L_11 = ___array;
		NullCheck((Array_t *)L_11);
		int32_t L_12 = Array_GetLowerBound_m8295((Array_t *)L_11, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))+(int32_t)1))) >= ((int32_t)L_12)))
		{
			goto IL_0049;
		}
	}

IL_0043:
	{
		ArgumentOutOfRangeException_t1083 * L_13 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7596(L_13, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_13);
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		EqualityComparer_1_t2177 * L_14 = (( EqualityComparer_1_t2177 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (EqualityComparer_1_t2177 *)L_14;
		int32_t L_15 = ___startIndex;
		V_1 = (int32_t)L_15;
		goto IL_006f;
	}

IL_0056:
	{
		EqualityComparer_1_t2177 * L_16 = V_0;
		ObjectU5BU5D_t105* L_17 = ___array;
		int32_t L_18 = V_1;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		Object_t * L_20 = ___value;
		NullCheck((EqualityComparer_1_t2177 *)L_16);
		bool L_21 = (bool)VirtFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(T,T) */, (EqualityComparer_1_t2177 *)L_16, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_17, L_19, sizeof(Object_t *))), (Object_t *)L_20);
		if (!L_21)
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_22 = V_1;
		return L_22;
	}

IL_006b:
	{
		int32_t L_23 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_23-(int32_t)1));
	}

IL_006f:
	{
		int32_t L_24 = V_1;
		int32_t L_25 = ___startIndex;
		int32_t L_26 = ___count;
		if ((((int32_t)L_24) >= ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_25-(int32_t)L_26))+(int32_t)1)))))
		{
			goto IL_0056;
		}
	}
	{
		return (-1);
	}
}
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral2077;
extern "C" ObjectU5BU5D_t105* Array_FindAll_TisObject_t_m26546_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral2077 = il2cpp_codegen_string_literal_from_index(2077);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ObjectU5BU5D_t105* V_1 = {0};
	Object_t * V_2 = {0};
	ObjectU5BU5D_t105* V_3 = {0};
	int32_t V_4 = 0;
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2180 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t718 * L_3 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_3, (String_t*)_stringLiteral2077, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		V_0 = (int32_t)0;
		ObjectU5BU5D_t105* L_4 = ___array;
		NullCheck(L_4);
		V_1 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (((int32_t)((int32_t)(((Array_t *)L_4)->max_length))))));
		ObjectU5BU5D_t105* L_5 = ___array;
		V_3 = (ObjectU5BU5D_t105*)L_5;
		V_4 = (int32_t)0;
		goto IL_005e;
	}

IL_0037:
	{
		ObjectU5BU5D_t105* L_6 = V_3;
		int32_t L_7 = V_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_2 = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_6, L_8, sizeof(Object_t *)));
		Predicate_1_t2180 * L_9 = ___match;
		Object_t * L_10 = V_2;
		NullCheck((Predicate_1_t2180 *)L_9);
		bool L_11 = (( bool (*) (Predicate_1_t2180 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((Predicate_1_t2180 *)L_9, (Object_t *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		if (!L_11)
		{
			goto IL_0058;
		}
	}
	{
		ObjectU5BU5D_t105* L_12 = V_1;
		int32_t L_13 = V_0;
		int32_t L_14 = (int32_t)L_13;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		Object_t * L_15 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, L_14, sizeof(Object_t *))) = (Object_t *)L_15;
	}

IL_0058:
	{
		int32_t L_16 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_17 = V_4;
		ObjectU5BU5D_t105* L_18 = V_3;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_18)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_19 = V_0;
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t105**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t105**)(&V_1), (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		ObjectU5BU5D_t105* L_20 = V_1;
		return L_20;
	}
}
// System.Boolean System.Array::Exists<System.Object>(T[],System.Predicate`1<T>)
// System.Boolean System.Array::Exists<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral2077;
extern "C" bool Array_Exists_TisObject_t_m26547_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral2077 = il2cpp_codegen_string_literal_from_index(2077);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ObjectU5BU5D_t105* V_1 = {0};
	int32_t V_2 = 0;
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2180 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t718 * L_3 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_3, (String_t*)_stringLiteral2077, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t105* L_4 = ___array;
		V_1 = (ObjectU5BU5D_t105*)L_4;
		V_2 = (int32_t)0;
		goto IL_0045;
	}

IL_002b:
	{
		ObjectU5BU5D_t105* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_5, L_7, sizeof(Object_t *)));
		Predicate_1_t2180 * L_8 = ___match;
		Object_t * L_9 = V_0;
		NullCheck((Predicate_1_t2180 *)L_8);
		bool L_10 = (( bool (*) (Predicate_1_t2180 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Predicate_1_t2180 *)L_8, (Object_t *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_10)
		{
			goto IL_0041;
		}
	}
	{
		return 1;
	}

IL_0041:
	{
		int32_t L_11 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_12 = V_2;
		ObjectU5BU5D_t105* L_13 = V_1;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_13)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		return 0;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Object>(T[])
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Object>(T[])
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" ReadOnlyCollection_1_t2170 * Array_AsReadOnly_TisObject_t_m12783_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t105* L_2 = ___array;
		ArrayReadOnlyList_1_t3035 * L_3 = (ArrayReadOnlyList_1_t3035 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		(( void (*) (ArrayReadOnlyList_1_t3035 *, ObjectU5BU5D_t105*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_3, (ObjectU5BU5D_t105*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		ReadOnlyCollection_1_t2170 * L_4 = (ReadOnlyCollection_1_t2170 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		(( void (*) (ReadOnlyCollection_1_t2170 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_4, (Object_t*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_4;
	}
}
// T System.Array::Find<System.Object>(T[],System.Predicate`1<T>)
// T System.Array::Find<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral2077;
extern "C" Object_t * Array_Find_TisObject_t_m26548_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral2077 = il2cpp_codegen_string_literal_from_index(2077);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ObjectU5BU5D_t105* V_1 = {0};
	int32_t V_2 = 0;
	Object_t * V_3 = {0};
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2180 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t718 * L_3 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_3, (String_t*)_stringLiteral2077, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t105* L_4 = ___array;
		V_1 = (ObjectU5BU5D_t105*)L_4;
		V_2 = (int32_t)0;
		goto IL_0045;
	}

IL_002b:
	{
		ObjectU5BU5D_t105* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_5, L_7, sizeof(Object_t *)));
		Predicate_1_t2180 * L_8 = ___match;
		Object_t * L_9 = V_0;
		NullCheck((Predicate_1_t2180 *)L_8);
		bool L_10 = (( bool (*) (Predicate_1_t2180 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Predicate_1_t2180 *)L_8, (Object_t *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_10)
		{
			goto IL_0041;
		}
	}
	{
		Object_t * L_11 = V_0;
		return L_11;
	}

IL_0041:
	{
		int32_t L_12 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_13 = V_2;
		ObjectU5BU5D_t105* L_14 = V_1;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_14)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_3));
		Object_t * L_15 = V_3;
		return L_15;
	}
}
// T System.Array::FindLast<System.Object>(T[],System.Predicate`1<T>)
// T System.Array::FindLast<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral2077;
extern "C" Object_t * Array_FindLast_TisObject_t_m26549_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___array, Predicate_1_t2180 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral2077 = il2cpp_codegen_string_literal_from_index(2077);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Object_t * V_1 = {0};
	{
		ObjectU5BU5D_t105* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Predicate_1_t2180 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t718 * L_3 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_3, (String_t*)_stringLiteral2077, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t105* L_4 = ___array;
		NullCheck(L_4);
		V_0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length))))-(int32_t)1));
		goto IL_004b;
	}

IL_002d:
	{
		Predicate_1_t2180 * L_5 = ___match;
		ObjectU5BU5D_t105* L_6 = ___array;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck((Predicate_1_t2180 *)L_5);
		bool L_9 = (( bool (*) (Predicate_1_t2180 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Predicate_1_t2180 *)L_5, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_6, L_8, sizeof(Object_t *))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_9)
		{
			goto IL_0047;
		}
	}
	{
		ObjectU5BU5D_t105* L_10 = ___array;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		return (*(Object_t **)(Object_t **)SZArrayLdElema(L_10, L_12, sizeof(Object_t *)));
	}

IL_0047:
	{
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_004b:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_1));
		Object_t * L_15 = V_1;
		return L_15;
	}
}
// T System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
// T System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" TableRange_t1480  Array_InternalArray__get_Item_TisTableRange_t1480_m26550_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	TableRange_t1480  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (TableRange_t1480 *)(&V_0));
		TableRange_t1480  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
// System.Void System.Array::InternalArray__ICollection_Add<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisTableRange_t1480_m26551_gshared (Array_t * __this, TableRange_t1480  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisTableRange_t1480_m26552_gshared (Array_t * __this, TableRange_t1480  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TableRange_t1480  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (TableRange_t1480 *)(&V_2));
		TableRange_t1480  L_5 = ___item;
		goto IL_004d;
	}
	{
		TableRange_t1480  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		TableRange_t1480  L_7 = V_2;
		TableRange_t1480  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTableRange_t1480_m26553_gshared (Array_t * __this, TableRangeU5BU5D_t1482* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		TableRangeU5BU5D_t1482* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TableRangeU5BU5D_t1482* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		TableRangeU5BU5D_t1482* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		TableRangeU5BU5D_t1482* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TableRangeU5BU5D_t1482* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisTableRange_t1480_m26554_gshared (Array_t * __this, TableRange_t1480  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
// System.Int32 System.Array::InternalArray__IndexOf<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisTableRange_t1480_m26555_gshared (Array_t * __this, TableRange_t1480  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TableRange_t1480  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (TableRange_t1480 *)(&V_2));
		TableRange_t1480  L_5 = ___item;
		goto IL_005d;
	}
	{
		TableRange_t1480  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		TableRange_t1480  L_10 = ___item;
		TableRange_t1480  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisTableRange_t1480_m26556_gshared (Array_t * __this, int32_t ___index, TableRange_t1480  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisTableRange_t1480_m26557_gshared (Array_t * __this, int32_t ___index, TableRange_t1480  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		TableRange_t1480  L_6 = ___item;
		TableRange_t1480  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (TableRange_t1480 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<Mono.Globalization.Unicode.CodePointIndexer/TableRange>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<Mono.Globalization.Unicode.CodePointIndexer/TableRange>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1480_m26558_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3039  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3039 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3039  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" Slot_t1557  Array_InternalArray__get_Item_TisSlot_t1557_m26559_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	Slot_t1557  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (Slot_t1557 *)(&V_0));
		Slot_t1557  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.Hashtable/Slot>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.Hashtable/Slot>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1557_m26560_gshared (Array_t * __this, Slot_t1557  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.Hashtable/Slot>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.Hashtable/Slot>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisSlot_t1557_m26561_gshared (Array_t * __this, Slot_t1557  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Slot_t1557  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Slot_t1557 *)(&V_2));
		Slot_t1557  L_5 = ___item;
		goto IL_004d;
	}
	{
		Slot_t1557  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		Slot_t1557  L_7 = V_2;
		Slot_t1557  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.Hashtable/Slot>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.Hashtable/Slot>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1557_m26562_gshared (Array_t * __this, SlotU5BU5D_t1564* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		SlotU5BU5D_t1564* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t1564* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t1564* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		SlotU5BU5D_t1564* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t1564* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.Hashtable/Slot>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.Hashtable/Slot>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisSlot_t1557_m26563_gshared (Array_t * __this, Slot_t1557  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.Hashtable/Slot>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.Hashtable/Slot>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisSlot_t1557_m26564_gshared (Array_t * __this, Slot_t1557  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Slot_t1557  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Slot_t1557 *)(&V_2));
		Slot_t1557  L_5 = ___item;
		goto IL_005d;
	}
	{
		Slot_t1557  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		Slot_t1557  L_10 = ___item;
		Slot_t1557  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Collections.Hashtable/Slot>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Collections.Hashtable/Slot>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisSlot_t1557_m26565_gshared (Array_t * __this, int32_t ___index, Slot_t1557  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Collections.Hashtable/Slot>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Collections.Hashtable/Slot>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisSlot_t1557_m26566_gshared (Array_t * __this, int32_t ___index, Slot_t1557  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		Slot_t1557  L_6 = ___item;
		Slot_t1557  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (Slot_t1557 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.Hashtable/Slot>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.Hashtable/Slot>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1557_m26567_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3048  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3048 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3048  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" Slot_t1565  Array_InternalArray__get_Item_TisSlot_t1565_m26568_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	Slot_t1565  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (Slot_t1565 *)(&V_0));
		Slot_t1565  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.SortedList/Slot>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.SortedList/Slot>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1565_m26569_gshared (Array_t * __this, Slot_t1565  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.SortedList/Slot>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.SortedList/Slot>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisSlot_t1565_m26570_gshared (Array_t * __this, Slot_t1565  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Slot_t1565  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Slot_t1565 *)(&V_2));
		Slot_t1565  L_5 = ___item;
		goto IL_004d;
	}
	{
		Slot_t1565  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		Slot_t1565  L_7 = V_2;
		Slot_t1565  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.SortedList/Slot>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.SortedList/Slot>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1565_m26571_gshared (Array_t * __this, SlotU5BU5D_t1568* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		SlotU5BU5D_t1568* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t1568* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t1568* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		SlotU5BU5D_t1568* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t1568* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.SortedList/Slot>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.SortedList/Slot>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisSlot_t1565_m26572_gshared (Array_t * __this, Slot_t1565  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.SortedList/Slot>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.SortedList/Slot>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisSlot_t1565_m26573_gshared (Array_t * __this, Slot_t1565  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Slot_t1565  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Slot_t1565 *)(&V_2));
		Slot_t1565  L_5 = ___item;
		goto IL_005d;
	}
	{
		Slot_t1565  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		Slot_t1565  L_10 = ___item;
		Slot_t1565  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Collections.SortedList/Slot>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Collections.SortedList/Slot>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisSlot_t1565_m26574_gshared (Array_t * __this, int32_t ___index, Slot_t1565  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Collections.SortedList/Slot>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Collections.SortedList/Slot>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisSlot_t1565_m26575_gshared (Array_t * __this, int32_t ___index, Slot_t1565  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		Slot_t1565  L_6 = ___item;
		Slot_t1565  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (Slot_t1565 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.SortedList/Slot>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.SortedList/Slot>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1565_m26576_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3049  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3049 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3049  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" ILTokenInfo_t1645  Array_InternalArray__get_Item_TisILTokenInfo_t1645_m26577_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ILTokenInfo_t1645  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (ILTokenInfo_t1645 *)(&V_0));
		ILTokenInfo_t1645  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILTokenInfo>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILTokenInfo>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILTokenInfo>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisILTokenInfo_t1645_m26578_gshared (Array_t * __this, ILTokenInfo_t1645  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILTokenInfo>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILTokenInfo>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisILTokenInfo_t1645_m26579_gshared (Array_t * __this, ILTokenInfo_t1645  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ILTokenInfo_t1645  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (ILTokenInfo_t1645 *)(&V_2));
		ILTokenInfo_t1645  L_5 = ___item;
		goto IL_004d;
	}
	{
		ILTokenInfo_t1645  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		ILTokenInfo_t1645  L_7 = V_2;
		ILTokenInfo_t1645  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILTokenInfo>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILTokenInfo>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisILTokenInfo_t1645_m26580_gshared (Array_t * __this, ILTokenInfoU5BU5D_t1648* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		ILTokenInfoU5BU5D_t1648* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		ILTokenInfoU5BU5D_t1648* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		ILTokenInfoU5BU5D_t1648* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		ILTokenInfoU5BU5D_t1648* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		ILTokenInfoU5BU5D_t1648* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILTokenInfo>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILTokenInfo>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisILTokenInfo_t1645_m26581_gshared (Array_t * __this, ILTokenInfo_t1645  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILTokenInfo>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILTokenInfo>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisILTokenInfo_t1645_m26582_gshared (Array_t * __this, ILTokenInfo_t1645  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ILTokenInfo_t1645  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (ILTokenInfo_t1645 *)(&V_2));
		ILTokenInfo_t1645  L_5 = ___item;
		goto IL_005d;
	}
	{
		ILTokenInfo_t1645  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		ILTokenInfo_t1645  L_10 = ___item;
		ILTokenInfo_t1645  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILTokenInfo>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILTokenInfo>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisILTokenInfo_t1645_m26583_gshared (Array_t * __this, int32_t ___index, ILTokenInfo_t1645  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisILTokenInfo_t1645_m26584_gshared (Array_t * __this, int32_t ___index, ILTokenInfo_t1645  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		ILTokenInfo_t1645  L_6 = ___item;
		ILTokenInfo_t1645  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (ILTokenInfo_t1645 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILTokenInfo>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILTokenInfo>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILTokenInfo>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisILTokenInfo_t1645_m26585_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3055  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3055 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3055  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" LabelData_t1647  Array_InternalArray__get_Item_TisLabelData_t1647_m26586_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	LabelData_t1647  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (LabelData_t1647 *)(&V_0));
		LabelData_t1647  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelData>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisLabelData_t1647_m26587_gshared (Array_t * __this, LabelData_t1647  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelData>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisLabelData_t1647_m26588_gshared (Array_t * __this, LabelData_t1647  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	LabelData_t1647  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (LabelData_t1647 *)(&V_2));
		LabelData_t1647  L_5 = ___item;
		goto IL_004d;
	}
	{
		LabelData_t1647  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		LabelData_t1647  L_7 = V_2;
		LabelData_t1647  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelData>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelData>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelData_t1647_m26589_gshared (Array_t * __this, LabelDataU5BU5D_t1649* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		LabelDataU5BU5D_t1649* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		LabelDataU5BU5D_t1649* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		LabelDataU5BU5D_t1649* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		LabelDataU5BU5D_t1649* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		LabelDataU5BU5D_t1649* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelData>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisLabelData_t1647_m26590_gshared (Array_t * __this, LabelData_t1647  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelData>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisLabelData_t1647_m26591_gshared (Array_t * __this, LabelData_t1647  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	LabelData_t1647  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (LabelData_t1647 *)(&V_2));
		LabelData_t1647  L_5 = ___item;
		goto IL_005d;
	}
	{
		LabelData_t1647  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		LabelData_t1647  L_10 = ___item;
		LabelData_t1647  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisLabelData_t1647_m26592_gshared (Array_t * __this, int32_t ___index, LabelData_t1647  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisLabelData_t1647_m26593_gshared (Array_t * __this, int32_t ___index, LabelData_t1647  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		LabelData_t1647  L_6 = ___item;
		LabelData_t1647  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (LabelData_t1647 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelData>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelData>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t1647_m26594_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3056  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3056 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3056  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" LabelFixup_t1646  Array_InternalArray__get_Item_TisLabelFixup_t1646_m26595_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	LabelFixup_t1646  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (LabelFixup_t1646 *)(&V_0));
		LabelFixup_t1646  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisLabelFixup_t1646_m26596_gshared (Array_t * __this, LabelFixup_t1646  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisLabelFixup_t1646_m26597_gshared (Array_t * __this, LabelFixup_t1646  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	LabelFixup_t1646  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (LabelFixup_t1646 *)(&V_2));
		LabelFixup_t1646  L_5 = ___item;
		goto IL_004d;
	}
	{
		LabelFixup_t1646  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		LabelFixup_t1646  L_7 = V_2;
		LabelFixup_t1646  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelFixup>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelFixup>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t1646_m26598_gshared (Array_t * __this, LabelFixupU5BU5D_t1650* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		LabelFixupU5BU5D_t1650* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		LabelFixupU5BU5D_t1650* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		LabelFixupU5BU5D_t1650* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		LabelFixupU5BU5D_t1650* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		LabelFixupU5BU5D_t1650* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisLabelFixup_t1646_m26599_gshared (Array_t * __this, LabelFixup_t1646  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisLabelFixup_t1646_m26600_gshared (Array_t * __this, LabelFixup_t1646  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	LabelFixup_t1646  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (LabelFixup_t1646 *)(&V_2));
		LabelFixup_t1646  L_5 = ___item;
		goto IL_005d;
	}
	{
		LabelFixup_t1646  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		LabelFixup_t1646  L_10 = ___item;
		LabelFixup_t1646  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisLabelFixup_t1646_m26601_gshared (Array_t * __this, int32_t ___index, LabelFixup_t1646  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisLabelFixup_t1646_m26602_gshared (Array_t * __this, int32_t ___index, LabelFixup_t1646  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		LabelFixup_t1646  L_6 = ___item;
		LabelFixup_t1646  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (LabelFixup_t1646 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelFixup>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelFixup>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t1646_m26603_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3057  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3057 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3057  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" CustomAttributeTypedArgument_t1692  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1692_m26604_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeTypedArgument_t1692  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (CustomAttributeTypedArgument_t1692 *)(&V_0));
		CustomAttributeTypedArgument_t1692  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.CustomAttributeTypedArgument>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.CustomAttributeTypedArgument>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisCustomAttributeTypedArgument_t1692_m26605_gshared (Array_t * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.CustomAttributeTypedArgument>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.CustomAttributeTypedArgument>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisCustomAttributeTypedArgument_t1692_m26606_gshared (Array_t * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CustomAttributeTypedArgument_t1692  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (CustomAttributeTypedArgument_t1692 *)(&V_2));
		CustomAttributeTypedArgument_t1692  L_5 = ___item;
		goto IL_004d;
	}
	{
		CustomAttributeTypedArgument_t1692  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		CustomAttributeTypedArgument_t1692  L_7 = V_2;
		CustomAttributeTypedArgument_t1692  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((CustomAttributeTypedArgument_t1692 *)(&___item));
		bool L_10 = CustomAttributeTypedArgument_Equals_m10209((CustomAttributeTypedArgument_t1692 *)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisCustomAttributeTypedArgument_t1692_m26607_gshared (Array_t * __this, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t2129* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t2129* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t2129* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.CustomAttributeTypedArgument>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.CustomAttributeTypedArgument>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisCustomAttributeTypedArgument_t1692_m26608_gshared (Array_t * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.CustomAttributeTypedArgument>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.CustomAttributeTypedArgument>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisCustomAttributeTypedArgument_t1692_m26609_gshared (Array_t * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CustomAttributeTypedArgument_t1692  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (CustomAttributeTypedArgument_t1692 *)(&V_2));
		CustomAttributeTypedArgument_t1692  L_5 = ___item;
		goto IL_005d;
	}
	{
		CustomAttributeTypedArgument_t1692  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		CustomAttributeTypedArgument_t1692  L_10 = ___item;
		CustomAttributeTypedArgument_t1692  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((CustomAttributeTypedArgument_t1692 *)(&V_2));
		bool L_13 = CustomAttributeTypedArgument_Equals_m10209((CustomAttributeTypedArgument_t1692 *)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisCustomAttributeTypedArgument_t1692_m26610_gshared (Array_t * __this, int32_t ___index, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisCustomAttributeTypedArgument_t1692_m26611_gshared (Array_t * __this, int32_t ___index, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		CustomAttributeTypedArgument_t1692  L_6 = ___item;
		CustomAttributeTypedArgument_t1692  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (CustomAttributeTypedArgument_t1692 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Reflection.CustomAttributeTypedArgument>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.CustomAttributeTypedArgument>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.CustomAttributeTypedArgument>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeTypedArgument_t1692_m26612_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3064  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3064 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3064  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" CustomAttributeNamedArgument_t1691  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t1691_m26613_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeNamedArgument_t1691  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (CustomAttributeNamedArgument_t1691 *)(&V_0));
		CustomAttributeNamedArgument_t1691  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.CustomAttributeNamedArgument>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.CustomAttributeNamedArgument>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisCustomAttributeNamedArgument_t1691_m26614_gshared (Array_t * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.CustomAttributeNamedArgument>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.CustomAttributeNamedArgument>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisCustomAttributeNamedArgument_t1691_m26615_gshared (Array_t * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CustomAttributeNamedArgument_t1691  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (CustomAttributeNamedArgument_t1691 *)(&V_2));
		CustomAttributeNamedArgument_t1691  L_5 = ___item;
		goto IL_004d;
	}
	{
		CustomAttributeNamedArgument_t1691  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		CustomAttributeNamedArgument_t1691  L_7 = V_2;
		CustomAttributeNamedArgument_t1691  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((CustomAttributeNamedArgument_t1691 *)(&___item));
		bool L_10 = CustomAttributeNamedArgument_Equals_m10206((CustomAttributeNamedArgument_t1691 *)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisCustomAttributeNamedArgument_t1691_m26616_gshared (Array_t * __this, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t2130* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t2130* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t2130* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.CustomAttributeNamedArgument>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.CustomAttributeNamedArgument>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisCustomAttributeNamedArgument_t1691_m26617_gshared (Array_t * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.CustomAttributeNamedArgument>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.CustomAttributeNamedArgument>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisCustomAttributeNamedArgument_t1691_m26618_gshared (Array_t * __this, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CustomAttributeNamedArgument_t1691  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (CustomAttributeNamedArgument_t1691 *)(&V_2));
		CustomAttributeNamedArgument_t1691  L_5 = ___item;
		goto IL_005d;
	}
	{
		CustomAttributeNamedArgument_t1691  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		CustomAttributeNamedArgument_t1691  L_10 = ___item;
		CustomAttributeNamedArgument_t1691  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((CustomAttributeNamedArgument_t1691 *)(&V_2));
		bool L_13 = CustomAttributeNamedArgument_Equals_m10206((CustomAttributeNamedArgument_t1691 *)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisCustomAttributeNamedArgument_t1691_m26619_gshared (Array_t * __this, int32_t ___index, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisCustomAttributeNamedArgument_t1691_m26620_gshared (Array_t * __this, int32_t ___index, CustomAttributeNamedArgument_t1691  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		CustomAttributeNamedArgument_t1691  L_6 = ___item;
		CustomAttributeNamedArgument_t1691  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (CustomAttributeNamedArgument_t1691 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Reflection.CustomAttributeNamedArgument>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.CustomAttributeNamedArgument>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.CustomAttributeNamedArgument>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisCustomAttributeNamedArgument_t1691_m26621_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3065  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3065 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3065  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C" CustomAttributeTypedArgumentU5BU5D_t2129* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t1692_m12765_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___values, const MethodInfo* method)
{
	CustomAttributeTypedArgumentU5BU5D_t2129* V_0 = {0};
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t105* L_0 = ___values;
		NullCheck(L_0);
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t2129*)((CustomAttributeTypedArgumentU5BU5D_t2129*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t105* L_3 = ___values;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		*((CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_1, L_2, sizeof(CustomAttributeTypedArgument_t1692 ))) = (CustomAttributeTypedArgument_t1692 )((*(CustomAttributeTypedArgument_t1692 *)((CustomAttributeTypedArgument_t1692 *)UnBox ((*(Object_t **)(Object_t **)SZArrayLdElema(L_3, L_5, sizeof(Object_t *))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t105* L_8 = ___values;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_9 = V_0;
		return L_9;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Reflection.CustomAttributeTypedArgument>(T[])
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Reflection.CustomAttributeTypedArgument>(T[])
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" ReadOnlyCollection_1_t2131 * Array_AsReadOnly_TisCustomAttributeTypedArgument_t1692_m12766_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_2 = ___array;
		ArrayReadOnlyList_1_t3075 * L_3 = (ArrayReadOnlyList_1_t3075 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		(( void (*) (ArrayReadOnlyList_1_t3075 *, CustomAttributeTypedArgumentU5BU5D_t2129*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_3, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		ReadOnlyCollection_1_t2131 * L_4 = (ReadOnlyCollection_1_t2131 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		(( void (*) (ReadOnlyCollection_1_t2131 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_4, (Object_t*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_4;
	}
}
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(T[]&,System.Int32)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(T[]&,System.Int32)
extern "C" void Array_Resize_TisCustomAttributeTypedArgument_t1692_m26622_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129** ___array, int32_t ___newSize, const MethodInfo* method)
{
	CustomAttributeTypedArgumentU5BU5D_t2129** G_B2_0 = {0};
	CustomAttributeTypedArgumentU5BU5D_t2129** G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	CustomAttributeTypedArgumentU5BU5D_t2129** G_B3_1 = {0};
	{
		CustomAttributeTypedArgumentU5BU5D_t2129** L_0 = ___array;
		CustomAttributeTypedArgumentU5BU5D_t2129** L_1 = ___array;
		G_B1_0 = L_0;
		if ((*((CustomAttributeTypedArgumentU5BU5D_t2129**)L_1)))
		{
			G_B2_0 = L_0;
			goto IL_000e;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0012;
	}

IL_000e:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129** L_2 = ___array;
		NullCheck((*((CustomAttributeTypedArgumentU5BU5D_t2129**)L_2)));
		G_B3_0 = (((int32_t)((int32_t)(((Array_t *)(*((CustomAttributeTypedArgumentU5BU5D_t2129**)L_2)))->max_length))));
		G_B3_1 = G_B2_0;
	}

IL_0012:
	{
		int32_t L_3 = ___newSize;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129**, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2129**)G_B3_1, (int32_t)G_B3_0, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(T[]&,System.Int32,System.Int32)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(T[]&,System.Int32,System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern "C" void Array_Resize_TisCustomAttributeTypedArgument_t1692_m26623_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129** ___array, int32_t ___length, int32_t ___newSize, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeTypedArgumentU5BU5D_t2129* V_0 = {0};
	{
		int32_t L_0 = ___newSize;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_1 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7596(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_000d:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129** L_2 = ___array;
		if ((*((CustomAttributeTypedArgumentU5BU5D_t2129**)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129** L_3 = ___array;
		int32_t L_4 = ___newSize;
		*((Object_t **)(L_3)) = (Object_t *)((CustomAttributeTypedArgumentU5BU5D_t2129*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), L_4));
		return;
	}

IL_001d:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129** L_5 = ___array;
		NullCheck((*((CustomAttributeTypedArgumentU5BU5D_t2129**)L_5)));
		int32_t L_6 = ___newSize;
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Array_t *)(*((CustomAttributeTypedArgumentU5BU5D_t2129**)L_5)))->max_length))))) == ((uint32_t)L_6))))
		{
			goto IL_0028;
		}
	}
	{
		return;
	}

IL_0028:
	{
		int32_t L_7 = ___newSize;
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t2129*)((CustomAttributeTypedArgumentU5BU5D_t2129*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), L_7));
		CustomAttributeTypedArgumentU5BU5D_t2129** L_8 = ___array;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_9 = V_0;
		int32_t L_10 = ___newSize;
		int32_t L_11 = ___length;
		int32_t L_12 = Math_Min_m5615(NULL /*static, unused*/, (int32_t)L_10, (int32_t)L_11, /*hidden argument*/NULL);
		Array_Copy_m7608(NULL /*static, unused*/, (Array_t *)(Array_t *)(*((CustomAttributeTypedArgumentU5BU5D_t2129**)L_8)), (Array_t *)(Array_t *)L_9, (int32_t)L_12, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t2129** L_13 = ___array;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_14 = V_0;
		*((Object_t **)(L_13)) = (Object_t *)L_14;
		return;
	}
}
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(T[],T,System.Int32,System.Int32)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(T[],T,System.Int32,System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t1692_m26624_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, CustomAttributeTypedArgument_t1692  ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	EqualityComparer_1_t3069 * V_1 = {0};
	int32_t V_2 = 0;
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___count;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = ___startIndex;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_4 = ___array;
		NullCheck((Array_t *)L_4);
		int32_t L_5 = Array_GetLowerBound_m8295((Array_t *)L_4, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)L_3) < ((int32_t)L_5)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_6 = ___startIndex;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetUpperBound_m8307((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		int32_t L_9 = ___count;
		if ((((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))) <= ((int32_t)((int32_t)((int32_t)L_8-(int32_t)L_9)))))
		{
			goto IL_003c;
		}
	}

IL_0036:
	{
		ArgumentOutOfRangeException_t1083 * L_10 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7596(L_10, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_10);
	}

IL_003c:
	{
		int32_t L_11 = ___startIndex;
		int32_t L_12 = ___count;
		V_0 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)L_12));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		EqualityComparer_1_t3069 * L_13 = (( EqualityComparer_1_t3069 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_1 = (EqualityComparer_1_t3069 *)L_13;
		int32_t L_14 = ___startIndex;
		V_2 = (int32_t)L_14;
		goto IL_0066;
	}

IL_004d:
	{
		EqualityComparer_1_t3069 * L_15 = V_1;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_16 = ___array;
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		CustomAttributeTypedArgument_t1692  L_19 = ___value;
		NullCheck((EqualityComparer_1_t3069 *)L_15);
		bool L_20 = (bool)VirtFuncInvoker2< bool, CustomAttributeTypedArgument_t1692 , CustomAttributeTypedArgument_t1692  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeTypedArgument>::Equals(T,T) */, (EqualityComparer_1_t3069 *)L_15, (CustomAttributeTypedArgument_t1692 )(*(CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_16, L_18, sizeof(CustomAttributeTypedArgument_t1692 ))), (CustomAttributeTypedArgument_t1692 )L_19);
		if (!L_20)
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_0062:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_004d;
		}
	}
	{
		return (-1);
	}
}
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t1692_m26625_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, int32_t ___index, int32_t ___length, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_2 = ___array;
		int32_t L_3 = ___index;
		int32_t L_4 = ___length;
		Object_t* L_5 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_2, (CustomAttributeTypedArgumentU5BU5D_t2129*)(CustomAttributeTypedArgumentU5BU5D_t2129*)NULL, (int32_t)L_3, (int32_t)L_4, (Object_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(TKey[],TValue[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<TKey>)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(TKey[],TValue[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<TKey>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* DoubleU5BU5D_t2104_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t122_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t270_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t107_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1256_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1354;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1228;
extern Il2CppCodeGenString* _stringLiteral1355;
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t1692_TisCustomAttributeTypedArgument_t1692_m26626_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___keys, CustomAttributeTypedArgumentU5BU5D_t2129* ___items, int32_t ___index, int32_t ___length, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		DoubleU5BU5D_t2104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		Int32U5BU5D_t122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(437);
		CharU5BU5D_t270_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(221);
		Exception_t107_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		InvalidOperationException_t1256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(770);
		_stringLiteral1354 = il2cpp_codegen_string_literal_from_index(1354);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1228 = il2cpp_codegen_string_literal_from_index(1228);
		_stringLiteral1355 = il2cpp_codegen_string_literal_from_index(1355);
		s_Il2CppMethodIntialized = true;
	}
	Swapper_t1448 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Exception_t107 * V_3 = {0};
	Exception_t107 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t107 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_0 = ___keys;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral1354, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_3 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_3, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___length;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_5 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_5, (String_t*)_stringLiteral1228, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0035:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_6 = ___keys;
		NullCheck(L_6);
		int32_t L_7 = ___index;
		int32_t L_8 = ___length;
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))-(int32_t)L_7))) < ((int32_t)L_8)))
		{
			goto IL_0051;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_9 = ___items;
		if (!L_9)
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_10 = ___index;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_11 = ___items;
		NullCheck(L_11);
		int32_t L_12 = ___length;
		if ((((int32_t)L_10) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length))))-(int32_t)L_12)))))
		{
			goto IL_0057;
		}
	}

IL_0051:
	{
		ArgumentException_t442 * L_13 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m11920(L_13, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_13);
	}

IL_0057:
	{
		int32_t L_14 = ___length;
		if ((((int32_t)L_14) > ((int32_t)1)))
		{
			goto IL_005f;
		}
	}
	{
		return;
	}

IL_005f:
	{
		Object_t* L_15 = ___comparer;
		if (L_15)
		{
			goto IL_00d7;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_16 = ___items;
		if (L_16)
		{
			goto IL_0073;
		}
	}
	{
		V_0 = (Swapper_t1448 *)NULL;
		goto IL_007a;
	}

IL_0073:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_17 = ___items;
		Swapper_t1448 * L_18 = (( Swapper_t1448 * (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Swapper_t1448 *)L_18;
	}

IL_007a:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_19 = ___keys;
		if (!((DoubleU5BU5D_t2104*)IsInst(L_19, DoubleU5BU5D_t2104_il2cpp_TypeInfo_var)))
		{
			goto IL_0099;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_20 = ___keys;
		int32_t L_21 = ___index;
		int32_t L_22 = ___length;
		Swapper_t1448 * L_23 = V_0;
		Array_combsort_m8359(NULL /*static, unused*/, (DoubleU5BU5D_t2104*)((DoubleU5BU5D_t2104*)IsInst(L_20, DoubleU5BU5D_t2104_il2cpp_TypeInfo_var)), (int32_t)L_21, (int32_t)L_22, (Swapper_t1448 *)L_23, /*hidden argument*/NULL);
		return;
	}

IL_0099:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_24 = ___keys;
		if (!((Int32U5BU5D_t122*)IsInst(L_24, Int32U5BU5D_t122_il2cpp_TypeInfo_var)))
		{
			goto IL_00b8;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_25 = ___keys;
		int32_t L_26 = ___index;
		int32_t L_27 = ___length;
		Swapper_t1448 * L_28 = V_0;
		Array_combsort_m8360(NULL /*static, unused*/, (Int32U5BU5D_t122*)((Int32U5BU5D_t122*)IsInst(L_25, Int32U5BU5D_t122_il2cpp_TypeInfo_var)), (int32_t)L_26, (int32_t)L_27, (Swapper_t1448 *)L_28, /*hidden argument*/NULL);
		return;
	}

IL_00b8:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_29 = ___keys;
		if (!((CharU5BU5D_t270*)IsInst(L_29, CharU5BU5D_t270_il2cpp_TypeInfo_var)))
		{
			goto IL_00d7;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_30 = ___keys;
		int32_t L_31 = ___index;
		int32_t L_32 = ___length;
		Swapper_t1448 * L_33 = V_0;
		Array_combsort_m8361(NULL /*static, unused*/, (CharU5BU5D_t270*)((CharU5BU5D_t270*)IsInst(L_30, CharU5BU5D_t270_il2cpp_TypeInfo_var)), (int32_t)L_31, (int32_t)L_32, (Swapper_t1448 *)L_33, /*hidden argument*/NULL);
		return;
	}

IL_00d7:
	try
	{ // begin try (depth: 1)
		int32_t L_34 = ___index;
		V_1 = (int32_t)L_34;
		int32_t L_35 = ___index;
		int32_t L_36 = ___length;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_35+(int32_t)L_36))-(int32_t)1));
		CustomAttributeTypedArgumentU5BU5D_t2129* L_37 = ___keys;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_38 = ___items;
		int32_t L_39 = V_1;
		int32_t L_40 = V_2;
		Object_t* L_41 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_37, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_38, (int32_t)L_39, (int32_t)L_40, (Object_t*)L_41, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		goto IL_0106;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t107 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t107_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00ef;
		throw e;
	}

CATCH_00ef:
	{ // begin catch(System.Exception)
		{
			V_3 = (Exception_t107 *)((Exception_t107 *)__exception_local);
			String_t* L_42 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1355, /*hidden argument*/NULL);
			Exception_t107 * L_43 = V_3;
			InvalidOperationException_t1256 * L_44 = (InvalidOperationException_t1256 *)il2cpp_codegen_object_new (InvalidOperationException_t1256_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m12351(L_44, (String_t*)L_42, (Exception_t107 *)L_43, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_44);
		}

IL_0101:
		{
			goto IL_0106;
		}
	} // end catch (depth: 1)

IL_0106:
	{
		return;
	}
}
// System.Array/Swapper System.Array::get_swapper<System.Reflection.CustomAttributeTypedArgument>(T[])
// System.Array/Swapper System.Array::get_swapper<System.Reflection.CustomAttributeTypedArgument>(T[])
extern TypeInfo* Int32U5BU5D_t122_il2cpp_TypeInfo_var;
extern TypeInfo* Swapper_t1448_il2cpp_TypeInfo_var;
extern TypeInfo* DoubleU5BU5D_t2104_il2cpp_TypeInfo_var;
extern const MethodInfo* Array_int_swapper_m8354_MethodInfo_var;
extern const MethodInfo* Array_double_swapper_m8357_MethodInfo_var;
extern const MethodInfo* Array_slow_swapper_m8356_MethodInfo_var;
extern "C" Swapper_t1448 * Array_get_swapper_TisCustomAttributeTypedArgument_t1692_m26627_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(437);
		Swapper_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		DoubleU5BU5D_t2104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		Array_int_swapper_m8354_MethodInfo_var = il2cpp_codegen_method_info_from_index(682);
		Array_double_swapper_m8357_MethodInfo_var = il2cpp_codegen_method_info_from_index(683);
		Array_slow_swapper_m8356_MethodInfo_var = il2cpp_codegen_method_info_from_index(685);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_0 = ___array;
		if (!((Int32U5BU5D_t122*)IsInst(L_0, Int32U5BU5D_t122_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_1 = ___array;
		IntPtr_t L_2 = { (void*)Array_int_swapper_m8354_MethodInfo_var };
		Swapper_t1448 * L_3 = (Swapper_t1448 *)il2cpp_codegen_object_new (Swapper_t1448_il2cpp_TypeInfo_var);
		Swapper__ctor_m8272(L_3, (Object_t *)(Object_t *)L_1, (IntPtr_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0018:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_4 = ___array;
		if (!((DoubleU5BU5D_t2104*)IsInst(L_4, DoubleU5BU5D_t2104_il2cpp_TypeInfo_var)))
		{
			goto IL_0030;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_5 = ___array;
		IntPtr_t L_6 = { (void*)Array_double_swapper_m8357_MethodInfo_var };
		Swapper_t1448 * L_7 = (Swapper_t1448 *)il2cpp_codegen_object_new (Swapper_t1448_il2cpp_TypeInfo_var);
		Swapper__ctor_m8272(L_7, (Object_t *)(Object_t *)L_5, (IntPtr_t)L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0030:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_8 = ___array;
		IntPtr_t L_9 = { (void*)Array_slow_swapper_m8356_MethodInfo_var };
		Swapper_t1448 * L_10 = (Swapper_t1448 *)il2cpp_codegen_object_new (Swapper_t1448_il2cpp_TypeInfo_var);
		Swapper__ctor_m8272(L_10, (Object_t *)(Object_t *)L_8, (IntPtr_t)L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void System.Array::qsort<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(K[],V[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<K>)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(K[],V[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<K>)
extern "C" void Array_qsort_TisCustomAttributeTypedArgument_t1692_TisCustomAttributeTypedArgument_t1692_m26628_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___keys, CustomAttributeTypedArgumentU5BU5D_t2129* ___items, int32_t ___low0, int32_t ___high0, Object_t* ___comparer, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	CustomAttributeTypedArgument_t1692  V_3 = {0};
	{
		int32_t L_0 = ___low0;
		int32_t L_1 = ___high0;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		int32_t L_2 = ___low0;
		V_0 = (int32_t)L_2;
		int32_t L_3 = ___high0;
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		V_2 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)L_6))/(int32_t)2))));
		CustomAttributeTypedArgumentU5BU5D_t2129* L_7 = ___keys;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_3 = (CustomAttributeTypedArgument_t1692 )(*(CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_7, L_9, sizeof(CustomAttributeTypedArgument_t1692 )));
	}

IL_001c:
	{
		goto IL_0025;
	}

IL_0021:
	{
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = ___high0;
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_0041;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_13 = ___keys;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		CustomAttributeTypedArgument_t1692  L_16 = V_3;
		Object_t* L_17 = ___comparer;
		int32_t L_18 = (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeTypedArgument_t1692 , CustomAttributeTypedArgument_t1692 , Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgument_t1692 )(*(CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_13, L_15, sizeof(CustomAttributeTypedArgument_t1692 ))), (CustomAttributeTypedArgument_t1692 )L_16, (Object_t*)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_18) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}

IL_0041:
	{
		goto IL_004a;
	}

IL_0046:
	{
		int32_t L_19 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_004a:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = ___low0;
		if ((((int32_t)L_20) <= ((int32_t)L_21)))
		{
			goto IL_0066;
		}
	}
	{
		CustomAttributeTypedArgument_t1692  L_22 = V_3;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_23 = ___keys;
		int32_t L_24 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		Object_t* L_26 = ___comparer;
		int32_t L_27 = (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeTypedArgument_t1692 , CustomAttributeTypedArgument_t1692 , Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgument_t1692 )L_22, (CustomAttributeTypedArgument_t1692 )(*(CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_23, L_25, sizeof(CustomAttributeTypedArgument_t1692 ))), (Object_t*)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_27) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}

IL_0066:
	{
		int32_t L_28 = V_0;
		int32_t L_29 = V_1;
		if ((((int32_t)L_28) > ((int32_t)L_29)))
		{
			goto IL_0083;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_30 = ___keys;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_31 = ___items;
		int32_t L_32 = V_0;
		int32_t L_33 = V_1;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_30, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_31, (int32_t)L_32, (int32_t)L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_34 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_34+(int32_t)1));
		int32_t L_35 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_35-(int32_t)1));
		goto IL_0088;
	}

IL_0083:
	{
		goto IL_008d;
	}

IL_0088:
	{
		goto IL_001c;
	}

IL_008d:
	{
		int32_t L_36 = ___low0;
		int32_t L_37 = V_1;
		if ((((int32_t)L_36) >= ((int32_t)L_37)))
		{
			goto IL_009f;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_38 = ___keys;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_39 = ___items;
		int32_t L_40 = ___low0;
		int32_t L_41 = V_1;
		Object_t* L_42 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_38, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_39, (int32_t)L_40, (int32_t)L_41, (Object_t*)L_42, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_009f:
	{
		int32_t L_43 = V_0;
		int32_t L_44 = ___high0;
		if ((((int32_t)L_43) >= ((int32_t)L_44)))
		{
			goto IL_00b1;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_45 = ___keys;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_46 = ___items;
		int32_t L_47 = V_0;
		int32_t L_48 = ___high0;
		Object_t* L_49 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_45, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_46, (int32_t)L_47, (int32_t)L_48, (Object_t*)L_49, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_00b1:
	{
		return;
	}
}
// System.Int32 System.Array::compare<System.Reflection.CustomAttributeTypedArgument>(T,T,System.Collections.Generic.IComparer`1<T>)
// System.Int32 System.Array::compare<System.Reflection.CustomAttributeTypedArgument>(T,T,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* IComparable_t2126_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1256_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2964;
extern "C" int32_t Array_compare_TisCustomAttributeTypedArgument_t1692_m26629_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgument_t1692  ___value1, CustomAttributeTypedArgument_t1692  ___value2, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IComparable_t2126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(987);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		InvalidOperationException_t1256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(770);
		_stringLiteral2964 = il2cpp_codegen_string_literal_from_index(2964);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t G_B6_0 = 0;
	{
		Object_t* L_0 = ___comparer;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Object_t* L_1 = ___comparer;
		CustomAttributeTypedArgument_t1692  L_2 = ___value1;
		CustomAttributeTypedArgument_t1692  L_3 = ___value2;
		NullCheck((Object_t*)L_1);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, CustomAttributeTypedArgument_t1692 , CustomAttributeTypedArgument_t1692  >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeTypedArgument>::Compare(T,T) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Object_t*)L_1, (CustomAttributeTypedArgument_t1692 )L_2, (CustomAttributeTypedArgument_t1692 )L_3);
		return L_4;
	}

IL_000f:
	{
		CustomAttributeTypedArgument_t1692  L_5 = ___value1;
		goto IL_002d;
	}
	{
		CustomAttributeTypedArgument_t1692  L_6 = ___value2;
		goto IL_002b;
	}
	{
		G_B6_0 = 0;
		goto IL_002c;
	}

IL_002b:
	{
		G_B6_0 = (-1);
	}

IL_002c:
	{
		return G_B6_0;
	}

IL_002d:
	{
		CustomAttributeTypedArgument_t1692  L_7 = ___value2;
		goto IL_003a;
	}
	{
		return 1;
	}

IL_003a:
	{
		CustomAttributeTypedArgument_t1692  L_8 = ___value1;
		CustomAttributeTypedArgument_t1692  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_9);
		if (!((Object_t*)IsInst(L_10, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))
		{
			goto IL_005c;
		}
	}
	{
		CustomAttributeTypedArgument_t1692  L_11 = ___value1;
		CustomAttributeTypedArgument_t1692  L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_12);
		CustomAttributeTypedArgument_t1692  L_14 = ___value2;
		NullCheck((Object_t*)((Object_t*)Castclass(L_13, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
		int32_t L_15 = (int32_t)InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t1692  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Reflection.CustomAttributeTypedArgument>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Object_t*)((Object_t*)Castclass(L_13, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))), (CustomAttributeTypedArgument_t1692 )L_14);
		return L_15;
	}

IL_005c:
	{
		CustomAttributeTypedArgument_t1692  L_16 = ___value1;
		CustomAttributeTypedArgument_t1692  L_17 = L_16;
		Object_t * L_18 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_17);
		if (!((Object_t *)IsInst(L_18, IComparable_t2126_il2cpp_TypeInfo_var)))
		{
			goto IL_0083;
		}
	}
	{
		CustomAttributeTypedArgument_t1692  L_19 = ___value1;
		CustomAttributeTypedArgument_t1692  L_20 = L_19;
		Object_t * L_21 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_20);
		CustomAttributeTypedArgument_t1692  L_22 = ___value2;
		CustomAttributeTypedArgument_t1692  L_23 = L_22;
		Object_t * L_24 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_23);
		NullCheck((Object_t *)((Object_t *)Castclass(L_21, IComparable_t2126_il2cpp_TypeInfo_var)));
		int32_t L_25 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t2126_il2cpp_TypeInfo_var, (Object_t *)((Object_t *)Castclass(L_21, IComparable_t2126_il2cpp_TypeInfo_var)), (Object_t *)L_24);
		return L_25;
	}

IL_0083:
	{
		String_t* L_26 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral2964, /*hidden argument*/NULL);
		V_0 = (String_t*)L_26;
		String_t* L_27 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m180(NULL /*static, unused*/, (RuntimeTypeHandle_t1450 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 3)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Format_m1857(NULL /*static, unused*/, (String_t*)L_27, (Object_t *)L_28, /*hidden argument*/NULL);
		InvalidOperationException_t1256 * L_30 = (InvalidOperationException_t1256 *)il2cpp_codegen_object_new (InvalidOperationException_t1256_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m6599(L_30, (String_t*)L_29, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_30);
	}
}
// System.Void System.Array::swap<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(K[],V[],System.Int32,System.Int32)
// System.Void System.Array::swap<System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument>(K[],V[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeTypedArgument_t1692_TisCustomAttributeTypedArgument_t1692_m26630_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___keys, CustomAttributeTypedArgumentU5BU5D_t2129* ___items, int32_t ___i, int32_t ___j, const MethodInfo* method)
{
	CustomAttributeTypedArgument_t1692  V_0 = {0};
	CustomAttributeTypedArgument_t1692  V_1 = {0};
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_0 = ___keys;
		int32_t L_1 = ___i;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (CustomAttributeTypedArgument_t1692 )(*(CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_0, L_2, sizeof(CustomAttributeTypedArgument_t1692 )));
		CustomAttributeTypedArgumentU5BU5D_t2129* L_3 = ___keys;
		int32_t L_4 = ___i;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_5 = ___keys;
		int32_t L_6 = ___j;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		*((CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_3, L_4, sizeof(CustomAttributeTypedArgument_t1692 ))) = (CustomAttributeTypedArgument_t1692 )(*(CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_5, L_7, sizeof(CustomAttributeTypedArgument_t1692 )));
		CustomAttributeTypedArgumentU5BU5D_t2129* L_8 = ___keys;
		int32_t L_9 = ___j;
		CustomAttributeTypedArgument_t1692  L_10 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		*((CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_8, L_9, sizeof(CustomAttributeTypedArgument_t1692 ))) = (CustomAttributeTypedArgument_t1692 )L_10;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_11 = ___items;
		if (!L_11)
		{
			goto IL_0042;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_12 = ___items;
		int32_t L_13 = ___i;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_1 = (CustomAttributeTypedArgument_t1692 )(*(CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_12, L_14, sizeof(CustomAttributeTypedArgument_t1692 )));
		CustomAttributeTypedArgumentU5BU5D_t2129* L_15 = ___items;
		int32_t L_16 = ___i;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_17 = ___items;
		int32_t L_18 = ___j;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		*((CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_15, L_16, sizeof(CustomAttributeTypedArgument_t1692 ))) = (CustomAttributeTypedArgument_t1692 )(*(CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_17, L_19, sizeof(CustomAttributeTypedArgument_t1692 )));
		CustomAttributeTypedArgumentU5BU5D_t2129* L_20 = ___items;
		int32_t L_21 = ___j;
		CustomAttributeTypedArgument_t1692  L_22 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		*((CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_20, L_21, sizeof(CustomAttributeTypedArgument_t1692 ))) = (CustomAttributeTypedArgument_t1692 )L_22;
	}

IL_0042:
	{
		return;
	}
}
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Comparison`1<T>)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Comparison`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t107_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1256_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2965;
extern Il2CppCodeGenString* _stringLiteral2966;
extern "C" void Array_Sort_TisCustomAttributeTypedArgument_t1692_m26631_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, int32_t ___length, Comparison_1_t3074 * ___comparison, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		Exception_t107_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		InvalidOperationException_t1256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(770);
		_stringLiteral2965 = il2cpp_codegen_string_literal_from_index(2965);
		_stringLiteral2966 = il2cpp_codegen_string_literal_from_index(2966);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Exception_t107 * V_2 = {0};
	Exception_t107 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t107 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Comparison_1_t3074 * L_0 = ___comparison;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral2965, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___length;
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_3 = ___array;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length))))) > ((int32_t)1)))
		{
			goto IL_0022;
		}
	}

IL_0021:
	{
		return;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		V_0 = (int32_t)0;
		int32_t L_4 = ___length;
		V_1 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		CustomAttributeTypedArgumentU5BU5D_t2129* L_5 = ___array;
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		Comparison_1_t3074 * L_8 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, Comparison_1_t3074 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_5, (int32_t)L_6, (int32_t)L_7, (Comparison_1_t3074 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		goto IL_004d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t107 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t107_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0036;
		throw e;
	}

CATCH_0036:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t107 *)((Exception_t107 *)__exception_local);
			String_t* L_9 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral2966, /*hidden argument*/NULL);
			Exception_t107 * L_10 = V_2;
			InvalidOperationException_t1256 * L_11 = (InvalidOperationException_t1256 *)il2cpp_codegen_object_new (InvalidOperationException_t1256_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m12351(L_11, (String_t*)L_9, (Exception_t107 *)L_10, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
		}

IL_0048:
		{
			goto IL_004d;
		}
	} // end catch (depth: 1)

IL_004d:
	{
		return;
	}
}
// System.Void System.Array::qsort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32,System.Comparison`1<T>)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32,System.Comparison`1<T>)
extern "C" void Array_qsort_TisCustomAttributeTypedArgument_t1692_m26632_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, int32_t ___low0, int32_t ___high0, Comparison_1_t3074 * ___comparison, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	CustomAttributeTypedArgument_t1692  V_3 = {0};
	{
		int32_t L_0 = ___low0;
		int32_t L_1 = ___high0;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		int32_t L_2 = ___low0;
		V_0 = (int32_t)L_2;
		int32_t L_3 = ___high0;
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		V_2 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)L_6))/(int32_t)2))));
		CustomAttributeTypedArgumentU5BU5D_t2129* L_7 = ___array;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_3 = (CustomAttributeTypedArgument_t1692 )(*(CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_7, L_9, sizeof(CustomAttributeTypedArgument_t1692 )));
	}

IL_001c:
	{
		goto IL_0025;
	}

IL_0021:
	{
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = ___high0;
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_0040;
		}
	}
	{
		Comparison_1_t3074 * L_13 = ___comparison;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_14 = ___array;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		CustomAttributeTypedArgument_t1692  L_17 = V_3;
		NullCheck((Comparison_1_t3074 *)L_13);
		int32_t L_18 = (( int32_t (*) (Comparison_1_t3074 *, CustomAttributeTypedArgument_t1692 , CustomAttributeTypedArgument_t1692 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Comparison_1_t3074 *)L_13, (CustomAttributeTypedArgument_t1692 )(*(CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_14, L_16, sizeof(CustomAttributeTypedArgument_t1692 ))), (CustomAttributeTypedArgument_t1692 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_18) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}

IL_0040:
	{
		goto IL_0049;
	}

IL_0045:
	{
		int32_t L_19 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_0049:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = ___low0;
		if ((((int32_t)L_20) <= ((int32_t)L_21)))
		{
			goto IL_0064;
		}
	}
	{
		Comparison_1_t3074 * L_22 = ___comparison;
		CustomAttributeTypedArgument_t1692  L_23 = V_3;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_24 = ___array;
		int32_t L_25 = V_1;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		NullCheck((Comparison_1_t3074 *)L_22);
		int32_t L_27 = (( int32_t (*) (Comparison_1_t3074 *, CustomAttributeTypedArgument_t1692 , CustomAttributeTypedArgument_t1692 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Comparison_1_t3074 *)L_22, (CustomAttributeTypedArgument_t1692 )L_23, (CustomAttributeTypedArgument_t1692 )(*(CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_24, L_26, sizeof(CustomAttributeTypedArgument_t1692 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_27) < ((int32_t)0)))
		{
			goto IL_0045;
		}
	}

IL_0064:
	{
		int32_t L_28 = V_0;
		int32_t L_29 = V_1;
		if ((((int32_t)L_28) > ((int32_t)L_29)))
		{
			goto IL_0080;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_30 = ___array;
		int32_t L_31 = V_0;
		int32_t L_32 = V_1;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_30, (int32_t)L_31, (int32_t)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_33 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
		int32_t L_34 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_34-(int32_t)1));
		goto IL_0085;
	}

IL_0080:
	{
		goto IL_008a;
	}

IL_0085:
	{
		goto IL_001c;
	}

IL_008a:
	{
		int32_t L_35 = ___low0;
		int32_t L_36 = V_1;
		if ((((int32_t)L_35) >= ((int32_t)L_36)))
		{
			goto IL_009a;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_37 = ___array;
		int32_t L_38 = ___low0;
		int32_t L_39 = V_1;
		Comparison_1_t3074 * L_40 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, Comparison_1_t3074 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_37, (int32_t)L_38, (int32_t)L_39, (Comparison_1_t3074 *)L_40, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_009a:
	{
		int32_t L_41 = V_0;
		int32_t L_42 = ___high0;
		if ((((int32_t)L_41) >= ((int32_t)L_42)))
		{
			goto IL_00aa;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_43 = ___array;
		int32_t L_44 = V_0;
		int32_t L_45 = ___high0;
		Comparison_1_t3074 * L_46 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, int32_t, Comparison_1_t3074 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_43, (int32_t)L_44, (int32_t)L_45, (Comparison_1_t3074 *)L_46, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_00aa:
	{
		return;
	}
}
// System.Void System.Array::swap<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32)
// System.Void System.Array::swap<System.Reflection.CustomAttributeTypedArgument>(T[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeTypedArgument_t1692_m26633_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, int32_t ___i, int32_t ___j, const MethodInfo* method)
{
	CustomAttributeTypedArgument_t1692  V_0 = {0};
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_0 = ___array;
		int32_t L_1 = ___i;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (CustomAttributeTypedArgument_t1692 )(*(CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_0, L_2, sizeof(CustomAttributeTypedArgument_t1692 )));
		CustomAttributeTypedArgumentU5BU5D_t2129* L_3 = ___array;
		int32_t L_4 = ___i;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_5 = ___array;
		int32_t L_6 = ___j;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		*((CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_3, L_4, sizeof(CustomAttributeTypedArgument_t1692 ))) = (CustomAttributeTypedArgument_t1692 )(*(CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_5, L_7, sizeof(CustomAttributeTypedArgument_t1692 )));
		CustomAttributeTypedArgumentU5BU5D_t2129* L_8 = ___array;
		int32_t L_9 = ___j;
		CustomAttributeTypedArgument_t1692  L_10 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		*((CustomAttributeTypedArgument_t1692 *)(CustomAttributeTypedArgument_t1692 *)SZArrayLdElema(L_8, L_9, sizeof(CustomAttributeTypedArgument_t1692 ))) = (CustomAttributeTypedArgument_t1692 )L_10;
		return;
	}
}
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(T[],T)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(T[],T)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t1692_m26634_gshared (Object_t * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, CustomAttributeTypedArgument_t1692  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		CustomAttributeTypedArgumentU5BU5D_t2129* L_2 = ___array;
		CustomAttributeTypedArgument_t1692  L_3 = ___value;
		CustomAttributeTypedArgumentU5BU5D_t2129* L_4 = ___array;
		NullCheck(L_4);
		int32_t L_5 = (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2129*, CustomAttributeTypedArgument_t1692 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2129*)L_2, (CustomAttributeTypedArgument_t1692 )L_3, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_5;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C" CustomAttributeNamedArgumentU5BU5D_t2130* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t1691_m12767_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___values, const MethodInfo* method)
{
	CustomAttributeNamedArgumentU5BU5D_t2130* V_0 = {0};
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t105* L_0 = ___values;
		NullCheck(L_0);
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t2130*)((CustomAttributeNamedArgumentU5BU5D_t2130*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t105* L_3 = ___values;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		*((CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_1, L_2, sizeof(CustomAttributeNamedArgument_t1691 ))) = (CustomAttributeNamedArgument_t1691 )((*(CustomAttributeNamedArgument_t1691 *)((CustomAttributeNamedArgument_t1691 *)UnBox ((*(Object_t **)(Object_t **)SZArrayLdElema(L_3, L_5, sizeof(Object_t *))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)))));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t105* L_8 = ___values;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_9 = V_0;
		return L_9;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Reflection.CustomAttributeNamedArgument>(T[])
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Array::AsReadOnly<System.Reflection.CustomAttributeNamedArgument>(T[])
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" ReadOnlyCollection_1_t2132 * Array_AsReadOnly_TisCustomAttributeNamedArgument_t1691_m12768_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_2 = ___array;
		ArrayReadOnlyList_1_t3086 * L_3 = (ArrayReadOnlyList_1_t3086 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		(( void (*) (ArrayReadOnlyList_1_t3086 *, CustomAttributeNamedArgumentU5BU5D_t2130*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(L_3, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		ReadOnlyCollection_1_t2132 * L_4 = (ReadOnlyCollection_1_t2132 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		(( void (*) (ReadOnlyCollection_1_t2132 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_4, (Object_t*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_4;
	}
}
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(T[]&,System.Int32)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(T[]&,System.Int32)
extern "C" void Array_Resize_TisCustomAttributeNamedArgument_t1691_m26635_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130** ___array, int32_t ___newSize, const MethodInfo* method)
{
	CustomAttributeNamedArgumentU5BU5D_t2130** G_B2_0 = {0};
	CustomAttributeNamedArgumentU5BU5D_t2130** G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	CustomAttributeNamedArgumentU5BU5D_t2130** G_B3_1 = {0};
	{
		CustomAttributeNamedArgumentU5BU5D_t2130** L_0 = ___array;
		CustomAttributeNamedArgumentU5BU5D_t2130** L_1 = ___array;
		G_B1_0 = L_0;
		if ((*((CustomAttributeNamedArgumentU5BU5D_t2130**)L_1)))
		{
			G_B2_0 = L_0;
			goto IL_000e;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0012;
	}

IL_000e:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130** L_2 = ___array;
		NullCheck((*((CustomAttributeNamedArgumentU5BU5D_t2130**)L_2)));
		G_B3_0 = (((int32_t)((int32_t)(((Array_t *)(*((CustomAttributeNamedArgumentU5BU5D_t2130**)L_2)))->max_length))));
		G_B3_1 = G_B2_0;
	}

IL_0012:
	{
		int32_t L_3 = ___newSize;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130**, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2130**)G_B3_1, (int32_t)G_B3_0, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(T[]&,System.Int32,System.Int32)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(T[]&,System.Int32,System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern "C" void Array_Resize_TisCustomAttributeNamedArgument_t1691_m26636_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130** ___array, int32_t ___length, int32_t ___newSize, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeNamedArgumentU5BU5D_t2130* V_0 = {0};
	{
		int32_t L_0 = ___newSize;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_1 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7596(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_000d:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130** L_2 = ___array;
		if ((*((CustomAttributeNamedArgumentU5BU5D_t2130**)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130** L_3 = ___array;
		int32_t L_4 = ___newSize;
		*((Object_t **)(L_3)) = (Object_t *)((CustomAttributeNamedArgumentU5BU5D_t2130*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), L_4));
		return;
	}

IL_001d:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130** L_5 = ___array;
		NullCheck((*((CustomAttributeNamedArgumentU5BU5D_t2130**)L_5)));
		int32_t L_6 = ___newSize;
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Array_t *)(*((CustomAttributeNamedArgumentU5BU5D_t2130**)L_5)))->max_length))))) == ((uint32_t)L_6))))
		{
			goto IL_0028;
		}
	}
	{
		return;
	}

IL_0028:
	{
		int32_t L_7 = ___newSize;
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t2130*)((CustomAttributeNamedArgumentU5BU5D_t2130*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), L_7));
		CustomAttributeNamedArgumentU5BU5D_t2130** L_8 = ___array;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_9 = V_0;
		int32_t L_10 = ___newSize;
		int32_t L_11 = ___length;
		int32_t L_12 = Math_Min_m5615(NULL /*static, unused*/, (int32_t)L_10, (int32_t)L_11, /*hidden argument*/NULL);
		Array_Copy_m7608(NULL /*static, unused*/, (Array_t *)(Array_t *)(*((CustomAttributeNamedArgumentU5BU5D_t2130**)L_8)), (Array_t *)(Array_t *)L_9, (int32_t)L_12, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t2130** L_13 = ___array;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_14 = V_0;
		*((Object_t **)(L_13)) = (Object_t *)L_14;
		return;
	}
}
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(T[],T,System.Int32,System.Int32)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(T[],T,System.Int32,System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t1691_m26637_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, CustomAttributeNamedArgument_t1691  ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	EqualityComparer_1_t3080 * V_1 = {0};
	int32_t V_2 = 0;
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___count;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = ___startIndex;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_4 = ___array;
		NullCheck((Array_t *)L_4);
		int32_t L_5 = Array_GetLowerBound_m8295((Array_t *)L_4, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)L_3) < ((int32_t)L_5)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_6 = ___startIndex;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetUpperBound_m8307((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		int32_t L_9 = ___count;
		if ((((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))) <= ((int32_t)((int32_t)((int32_t)L_8-(int32_t)L_9)))))
		{
			goto IL_003c;
		}
	}

IL_0036:
	{
		ArgumentOutOfRangeException_t1083 * L_10 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7596(L_10, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_10);
	}

IL_003c:
	{
		int32_t L_11 = ___startIndex;
		int32_t L_12 = ___count;
		V_0 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)L_12));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		EqualityComparer_1_t3080 * L_13 = (( EqualityComparer_1_t3080 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_1 = (EqualityComparer_1_t3080 *)L_13;
		int32_t L_14 = ___startIndex;
		V_2 = (int32_t)L_14;
		goto IL_0066;
	}

IL_004d:
	{
		EqualityComparer_1_t3080 * L_15 = V_1;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_16 = ___array;
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		CustomAttributeNamedArgument_t1691  L_19 = ___value;
		NullCheck((EqualityComparer_1_t3080 *)L_15);
		bool L_20 = (bool)VirtFuncInvoker2< bool, CustomAttributeNamedArgument_t1691 , CustomAttributeNamedArgument_t1691  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.CustomAttributeNamedArgument>::Equals(T,T) */, (EqualityComparer_1_t3080 *)L_15, (CustomAttributeNamedArgument_t1691 )(*(CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_16, L_18, sizeof(CustomAttributeNamedArgument_t1691 ))), (CustomAttributeNamedArgument_t1691 )L_19);
		if (!L_20)
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_0062:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_004d;
		}
	}
	{
		return (-1);
	}
}
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t1691_m26638_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, int32_t ___index, int32_t ___length, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_2 = ___array;
		int32_t L_3 = ___index;
		int32_t L_4 = ___length;
		Object_t* L_5 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_2, (CustomAttributeNamedArgumentU5BU5D_t2130*)(CustomAttributeNamedArgumentU5BU5D_t2130*)NULL, (int32_t)L_3, (int32_t)L_4, (Object_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return;
	}
}
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(TKey[],TValue[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<TKey>)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(TKey[],TValue[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<TKey>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* DoubleU5BU5D_t2104_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t122_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t270_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t107_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1256_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1354;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1228;
extern Il2CppCodeGenString* _stringLiteral1355;
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t1691_TisCustomAttributeNamedArgument_t1691_m26639_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___keys, CustomAttributeNamedArgumentU5BU5D_t2130* ___items, int32_t ___index, int32_t ___length, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		DoubleU5BU5D_t2104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		Int32U5BU5D_t122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(437);
		CharU5BU5D_t270_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(221);
		Exception_t107_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		InvalidOperationException_t1256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(770);
		_stringLiteral1354 = il2cpp_codegen_string_literal_from_index(1354);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1228 = il2cpp_codegen_string_literal_from_index(1228);
		_stringLiteral1355 = il2cpp_codegen_string_literal_from_index(1355);
		s_Il2CppMethodIntialized = true;
	}
	Swapper_t1448 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Exception_t107 * V_3 = {0};
	Exception_t107 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t107 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_0 = ___keys;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral1354, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_3 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_3, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___length;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_5 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_5, (String_t*)_stringLiteral1228, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0035:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_6 = ___keys;
		NullCheck(L_6);
		int32_t L_7 = ___index;
		int32_t L_8 = ___length;
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))-(int32_t)L_7))) < ((int32_t)L_8)))
		{
			goto IL_0051;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_9 = ___items;
		if (!L_9)
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_10 = ___index;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_11 = ___items;
		NullCheck(L_11);
		int32_t L_12 = ___length;
		if ((((int32_t)L_10) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length))))-(int32_t)L_12)))))
		{
			goto IL_0057;
		}
	}

IL_0051:
	{
		ArgumentException_t442 * L_13 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m11920(L_13, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_13);
	}

IL_0057:
	{
		int32_t L_14 = ___length;
		if ((((int32_t)L_14) > ((int32_t)1)))
		{
			goto IL_005f;
		}
	}
	{
		return;
	}

IL_005f:
	{
		Object_t* L_15 = ___comparer;
		if (L_15)
		{
			goto IL_00d7;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_16 = ___items;
		if (L_16)
		{
			goto IL_0073;
		}
	}
	{
		V_0 = (Swapper_t1448 *)NULL;
		goto IL_007a;
	}

IL_0073:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_17 = ___items;
		Swapper_t1448 * L_18 = (( Swapper_t1448 * (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Swapper_t1448 *)L_18;
	}

IL_007a:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_19 = ___keys;
		if (!((DoubleU5BU5D_t2104*)IsInst(L_19, DoubleU5BU5D_t2104_il2cpp_TypeInfo_var)))
		{
			goto IL_0099;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_20 = ___keys;
		int32_t L_21 = ___index;
		int32_t L_22 = ___length;
		Swapper_t1448 * L_23 = V_0;
		Array_combsort_m8359(NULL /*static, unused*/, (DoubleU5BU5D_t2104*)((DoubleU5BU5D_t2104*)IsInst(L_20, DoubleU5BU5D_t2104_il2cpp_TypeInfo_var)), (int32_t)L_21, (int32_t)L_22, (Swapper_t1448 *)L_23, /*hidden argument*/NULL);
		return;
	}

IL_0099:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_24 = ___keys;
		if (!((Int32U5BU5D_t122*)IsInst(L_24, Int32U5BU5D_t122_il2cpp_TypeInfo_var)))
		{
			goto IL_00b8;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_25 = ___keys;
		int32_t L_26 = ___index;
		int32_t L_27 = ___length;
		Swapper_t1448 * L_28 = V_0;
		Array_combsort_m8360(NULL /*static, unused*/, (Int32U5BU5D_t122*)((Int32U5BU5D_t122*)IsInst(L_25, Int32U5BU5D_t122_il2cpp_TypeInfo_var)), (int32_t)L_26, (int32_t)L_27, (Swapper_t1448 *)L_28, /*hidden argument*/NULL);
		return;
	}

IL_00b8:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_29 = ___keys;
		if (!((CharU5BU5D_t270*)IsInst(L_29, CharU5BU5D_t270_il2cpp_TypeInfo_var)))
		{
			goto IL_00d7;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_30 = ___keys;
		int32_t L_31 = ___index;
		int32_t L_32 = ___length;
		Swapper_t1448 * L_33 = V_0;
		Array_combsort_m8361(NULL /*static, unused*/, (CharU5BU5D_t270*)((CharU5BU5D_t270*)IsInst(L_30, CharU5BU5D_t270_il2cpp_TypeInfo_var)), (int32_t)L_31, (int32_t)L_32, (Swapper_t1448 *)L_33, /*hidden argument*/NULL);
		return;
	}

IL_00d7:
	try
	{ // begin try (depth: 1)
		int32_t L_34 = ___index;
		V_1 = (int32_t)L_34;
		int32_t L_35 = ___index;
		int32_t L_36 = ___length;
		V_2 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_35+(int32_t)L_36))-(int32_t)1));
		CustomAttributeNamedArgumentU5BU5D_t2130* L_37 = ___keys;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_38 = ___items;
		int32_t L_39 = V_1;
		int32_t L_40 = V_2;
		Object_t* L_41 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_37, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_38, (int32_t)L_39, (int32_t)L_40, (Object_t*)L_41, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		goto IL_0106;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t107 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t107_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00ef;
		throw e;
	}

CATCH_00ef:
	{ // begin catch(System.Exception)
		{
			V_3 = (Exception_t107 *)((Exception_t107 *)__exception_local);
			String_t* L_42 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1355, /*hidden argument*/NULL);
			Exception_t107 * L_43 = V_3;
			InvalidOperationException_t1256 * L_44 = (InvalidOperationException_t1256 *)il2cpp_codegen_object_new (InvalidOperationException_t1256_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m12351(L_44, (String_t*)L_42, (Exception_t107 *)L_43, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_44);
		}

IL_0101:
		{
			goto IL_0106;
		}
	} // end catch (depth: 1)

IL_0106:
	{
		return;
	}
}
// System.Array/Swapper System.Array::get_swapper<System.Reflection.CustomAttributeNamedArgument>(T[])
// System.Array/Swapper System.Array::get_swapper<System.Reflection.CustomAttributeNamedArgument>(T[])
extern TypeInfo* Int32U5BU5D_t122_il2cpp_TypeInfo_var;
extern TypeInfo* Swapper_t1448_il2cpp_TypeInfo_var;
extern TypeInfo* DoubleU5BU5D_t2104_il2cpp_TypeInfo_var;
extern const MethodInfo* Array_int_swapper_m8354_MethodInfo_var;
extern const MethodInfo* Array_double_swapper_m8357_MethodInfo_var;
extern const MethodInfo* Array_slow_swapper_m8356_MethodInfo_var;
extern "C" Swapper_t1448 * Array_get_swapper_TisCustomAttributeNamedArgument_t1691_m26640_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(437);
		Swapper_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(990);
		DoubleU5BU5D_t2104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(991);
		Array_int_swapper_m8354_MethodInfo_var = il2cpp_codegen_method_info_from_index(682);
		Array_double_swapper_m8357_MethodInfo_var = il2cpp_codegen_method_info_from_index(683);
		Array_slow_swapper_m8356_MethodInfo_var = il2cpp_codegen_method_info_from_index(685);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_0 = ___array;
		if (!((Int32U5BU5D_t122*)IsInst(L_0, Int32U5BU5D_t122_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_1 = ___array;
		IntPtr_t L_2 = { (void*)Array_int_swapper_m8354_MethodInfo_var };
		Swapper_t1448 * L_3 = (Swapper_t1448 *)il2cpp_codegen_object_new (Swapper_t1448_il2cpp_TypeInfo_var);
		Swapper__ctor_m8272(L_3, (Object_t *)(Object_t *)L_1, (IntPtr_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0018:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_4 = ___array;
		if (!((DoubleU5BU5D_t2104*)IsInst(L_4, DoubleU5BU5D_t2104_il2cpp_TypeInfo_var)))
		{
			goto IL_0030;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_5 = ___array;
		IntPtr_t L_6 = { (void*)Array_double_swapper_m8357_MethodInfo_var };
		Swapper_t1448 * L_7 = (Swapper_t1448 *)il2cpp_codegen_object_new (Swapper_t1448_il2cpp_TypeInfo_var);
		Swapper__ctor_m8272(L_7, (Object_t *)(Object_t *)L_5, (IntPtr_t)L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0030:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_8 = ___array;
		IntPtr_t L_9 = { (void*)Array_slow_swapper_m8356_MethodInfo_var };
		Swapper_t1448 * L_10 = (Swapper_t1448 *)il2cpp_codegen_object_new (Swapper_t1448_il2cpp_TypeInfo_var);
		Swapper__ctor_m8272(L_10, (Object_t *)(Object_t *)L_8, (IntPtr_t)L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void System.Array::qsort<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(K[],V[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<K>)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(K[],V[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<K>)
extern "C" void Array_qsort_TisCustomAttributeNamedArgument_t1691_TisCustomAttributeNamedArgument_t1691_m26641_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___keys, CustomAttributeNamedArgumentU5BU5D_t2130* ___items, int32_t ___low0, int32_t ___high0, Object_t* ___comparer, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	CustomAttributeNamedArgument_t1691  V_3 = {0};
	{
		int32_t L_0 = ___low0;
		int32_t L_1 = ___high0;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		int32_t L_2 = ___low0;
		V_0 = (int32_t)L_2;
		int32_t L_3 = ___high0;
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		V_2 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)L_6))/(int32_t)2))));
		CustomAttributeNamedArgumentU5BU5D_t2130* L_7 = ___keys;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_3 = (CustomAttributeNamedArgument_t1691 )(*(CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_7, L_9, sizeof(CustomAttributeNamedArgument_t1691 )));
	}

IL_001c:
	{
		goto IL_0025;
	}

IL_0021:
	{
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = ___high0;
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_0041;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_13 = ___keys;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		CustomAttributeNamedArgument_t1691  L_16 = V_3;
		Object_t* L_17 = ___comparer;
		int32_t L_18 = (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeNamedArgument_t1691 , CustomAttributeNamedArgument_t1691 , Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgument_t1691 )(*(CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_13, L_15, sizeof(CustomAttributeNamedArgument_t1691 ))), (CustomAttributeNamedArgument_t1691 )L_16, (Object_t*)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_18) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}

IL_0041:
	{
		goto IL_004a;
	}

IL_0046:
	{
		int32_t L_19 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_004a:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = ___low0;
		if ((((int32_t)L_20) <= ((int32_t)L_21)))
		{
			goto IL_0066;
		}
	}
	{
		CustomAttributeNamedArgument_t1691  L_22 = V_3;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_23 = ___keys;
		int32_t L_24 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		Object_t* L_26 = ___comparer;
		int32_t L_27 = (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeNamedArgument_t1691 , CustomAttributeNamedArgument_t1691 , Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgument_t1691 )L_22, (CustomAttributeNamedArgument_t1691 )(*(CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_23, L_25, sizeof(CustomAttributeNamedArgument_t1691 ))), (Object_t*)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_27) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}

IL_0066:
	{
		int32_t L_28 = V_0;
		int32_t L_29 = V_1;
		if ((((int32_t)L_28) > ((int32_t)L_29)))
		{
			goto IL_0083;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_30 = ___keys;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_31 = ___items;
		int32_t L_32 = V_0;
		int32_t L_33 = V_1;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_30, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_31, (int32_t)L_32, (int32_t)L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_34 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_34+(int32_t)1));
		int32_t L_35 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_35-(int32_t)1));
		goto IL_0088;
	}

IL_0083:
	{
		goto IL_008d;
	}

IL_0088:
	{
		goto IL_001c;
	}

IL_008d:
	{
		int32_t L_36 = ___low0;
		int32_t L_37 = V_1;
		if ((((int32_t)L_36) >= ((int32_t)L_37)))
		{
			goto IL_009f;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_38 = ___keys;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_39 = ___items;
		int32_t L_40 = ___low0;
		int32_t L_41 = V_1;
		Object_t* L_42 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_38, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_39, (int32_t)L_40, (int32_t)L_41, (Object_t*)L_42, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_009f:
	{
		int32_t L_43 = V_0;
		int32_t L_44 = ___high0;
		if ((((int32_t)L_43) >= ((int32_t)L_44)))
		{
			goto IL_00b1;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_45 = ___keys;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_46 = ___items;
		int32_t L_47 = V_0;
		int32_t L_48 = ___high0;
		Object_t* L_49 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_45, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_46, (int32_t)L_47, (int32_t)L_48, (Object_t*)L_49, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_00b1:
	{
		return;
	}
}
// System.Int32 System.Array::compare<System.Reflection.CustomAttributeNamedArgument>(T,T,System.Collections.Generic.IComparer`1<T>)
// System.Int32 System.Array::compare<System.Reflection.CustomAttributeNamedArgument>(T,T,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo* IComparable_t2126_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1256_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2964;
extern "C" int32_t Array_compare_TisCustomAttributeNamedArgument_t1691_m26642_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgument_t1691  ___value1, CustomAttributeNamedArgument_t1691  ___value2, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IComparable_t2126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(987);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		InvalidOperationException_t1256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(770);
		_stringLiteral2964 = il2cpp_codegen_string_literal_from_index(2964);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t G_B6_0 = 0;
	{
		Object_t* L_0 = ___comparer;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Object_t* L_1 = ___comparer;
		CustomAttributeNamedArgument_t1691  L_2 = ___value1;
		CustomAttributeNamedArgument_t1691  L_3 = ___value2;
		NullCheck((Object_t*)L_1);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, CustomAttributeNamedArgument_t1691 , CustomAttributeNamedArgument_t1691  >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeNamedArgument>::Compare(T,T) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Object_t*)L_1, (CustomAttributeNamedArgument_t1691 )L_2, (CustomAttributeNamedArgument_t1691 )L_3);
		return L_4;
	}

IL_000f:
	{
		CustomAttributeNamedArgument_t1691  L_5 = ___value1;
		goto IL_002d;
	}
	{
		CustomAttributeNamedArgument_t1691  L_6 = ___value2;
		goto IL_002b;
	}
	{
		G_B6_0 = 0;
		goto IL_002c;
	}

IL_002b:
	{
		G_B6_0 = (-1);
	}

IL_002c:
	{
		return G_B6_0;
	}

IL_002d:
	{
		CustomAttributeNamedArgument_t1691  L_7 = ___value2;
		goto IL_003a;
	}
	{
		return 1;
	}

IL_003a:
	{
		CustomAttributeNamedArgument_t1691  L_8 = ___value1;
		CustomAttributeNamedArgument_t1691  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_9);
		if (!((Object_t*)IsInst(L_10, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))))
		{
			goto IL_005c;
		}
	}
	{
		CustomAttributeNamedArgument_t1691  L_11 = ___value1;
		CustomAttributeNamedArgument_t1691  L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_12);
		CustomAttributeNamedArgument_t1691  L_14 = ___value2;
		NullCheck((Object_t*)((Object_t*)Castclass(L_13, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))));
		int32_t L_15 = (int32_t)InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t1691  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Reflection.CustomAttributeNamedArgument>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Object_t*)((Object_t*)Castclass(L_13, IL2CPP_RGCTX_DATA(method->rgctx_data, 2))), (CustomAttributeNamedArgument_t1691 )L_14);
		return L_15;
	}

IL_005c:
	{
		CustomAttributeNamedArgument_t1691  L_16 = ___value1;
		CustomAttributeNamedArgument_t1691  L_17 = L_16;
		Object_t * L_18 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_17);
		if (!((Object_t *)IsInst(L_18, IComparable_t2126_il2cpp_TypeInfo_var)))
		{
			goto IL_0083;
		}
	}
	{
		CustomAttributeNamedArgument_t1691  L_19 = ___value1;
		CustomAttributeNamedArgument_t1691  L_20 = L_19;
		Object_t * L_21 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_20);
		CustomAttributeNamedArgument_t1691  L_22 = ___value2;
		CustomAttributeNamedArgument_t1691  L_23 = L_22;
		Object_t * L_24 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), &L_23);
		NullCheck((Object_t *)((Object_t *)Castclass(L_21, IComparable_t2126_il2cpp_TypeInfo_var)));
		int32_t L_25 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t2126_il2cpp_TypeInfo_var, (Object_t *)((Object_t *)Castclass(L_21, IComparable_t2126_il2cpp_TypeInfo_var)), (Object_t *)L_24);
		return L_25;
	}

IL_0083:
	{
		String_t* L_26 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral2964, /*hidden argument*/NULL);
		V_0 = (String_t*)L_26;
		String_t* L_27 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m180(NULL /*static, unused*/, (RuntimeTypeHandle_t1450 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 3)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Format_m1857(NULL /*static, unused*/, (String_t*)L_27, (Object_t *)L_28, /*hidden argument*/NULL);
		InvalidOperationException_t1256 * L_30 = (InvalidOperationException_t1256 *)il2cpp_codegen_object_new (InvalidOperationException_t1256_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m6599(L_30, (String_t*)L_29, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_30);
	}
}
// System.Void System.Array::swap<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(K[],V[],System.Int32,System.Int32)
// System.Void System.Array::swap<System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument>(K[],V[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeNamedArgument_t1691_TisCustomAttributeNamedArgument_t1691_m26643_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___keys, CustomAttributeNamedArgumentU5BU5D_t2130* ___items, int32_t ___i, int32_t ___j, const MethodInfo* method)
{
	CustomAttributeNamedArgument_t1691  V_0 = {0};
	CustomAttributeNamedArgument_t1691  V_1 = {0};
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_0 = ___keys;
		int32_t L_1 = ___i;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (CustomAttributeNamedArgument_t1691 )(*(CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_0, L_2, sizeof(CustomAttributeNamedArgument_t1691 )));
		CustomAttributeNamedArgumentU5BU5D_t2130* L_3 = ___keys;
		int32_t L_4 = ___i;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_5 = ___keys;
		int32_t L_6 = ___j;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		*((CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_3, L_4, sizeof(CustomAttributeNamedArgument_t1691 ))) = (CustomAttributeNamedArgument_t1691 )(*(CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_5, L_7, sizeof(CustomAttributeNamedArgument_t1691 )));
		CustomAttributeNamedArgumentU5BU5D_t2130* L_8 = ___keys;
		int32_t L_9 = ___j;
		CustomAttributeNamedArgument_t1691  L_10 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		*((CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_8, L_9, sizeof(CustomAttributeNamedArgument_t1691 ))) = (CustomAttributeNamedArgument_t1691 )L_10;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_11 = ___items;
		if (!L_11)
		{
			goto IL_0042;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_12 = ___items;
		int32_t L_13 = ___i;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_1 = (CustomAttributeNamedArgument_t1691 )(*(CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_12, L_14, sizeof(CustomAttributeNamedArgument_t1691 )));
		CustomAttributeNamedArgumentU5BU5D_t2130* L_15 = ___items;
		int32_t L_16 = ___i;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_17 = ___items;
		int32_t L_18 = ___j;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		*((CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_15, L_16, sizeof(CustomAttributeNamedArgument_t1691 ))) = (CustomAttributeNamedArgument_t1691 )(*(CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_17, L_19, sizeof(CustomAttributeNamedArgument_t1691 )));
		CustomAttributeNamedArgumentU5BU5D_t2130* L_20 = ___items;
		int32_t L_21 = ___j;
		CustomAttributeNamedArgument_t1691  L_22 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		*((CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_20, L_21, sizeof(CustomAttributeNamedArgument_t1691 ))) = (CustomAttributeNamedArgument_t1691 )L_22;
	}

IL_0042:
	{
		return;
	}
}
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Comparison`1<T>)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Comparison`1<T>)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t107_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1256_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2965;
extern Il2CppCodeGenString* _stringLiteral2966;
extern "C" void Array_Sort_TisCustomAttributeNamedArgument_t1691_m26644_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, int32_t ___length, Comparison_1_t3085 * ___comparison, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		Exception_t107_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		InvalidOperationException_t1256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(770);
		_stringLiteral2965 = il2cpp_codegen_string_literal_from_index(2965);
		_stringLiteral2966 = il2cpp_codegen_string_literal_from_index(2966);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Exception_t107 * V_2 = {0};
	Exception_t107 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t107 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Comparison_1_t3085 * L_0 = ___comparison;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral2965, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___length;
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_3 = ___array;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length))))) > ((int32_t)1)))
		{
			goto IL_0022;
		}
	}

IL_0021:
	{
		return;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		V_0 = (int32_t)0;
		int32_t L_4 = ___length;
		V_1 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		CustomAttributeNamedArgumentU5BU5D_t2130* L_5 = ___array;
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		Comparison_1_t3085 * L_8 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, Comparison_1_t3085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_5, (int32_t)L_6, (int32_t)L_7, (Comparison_1_t3085 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		goto IL_004d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t107 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t107_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0036;
		throw e;
	}

CATCH_0036:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t107 *)((Exception_t107 *)__exception_local);
			String_t* L_9 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral2966, /*hidden argument*/NULL);
			Exception_t107 * L_10 = V_2;
			InvalidOperationException_t1256 * L_11 = (InvalidOperationException_t1256 *)il2cpp_codegen_object_new (InvalidOperationException_t1256_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m12351(L_11, (String_t*)L_9, (Exception_t107 *)L_10, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
		}

IL_0048:
		{
			goto IL_004d;
		}
	} // end catch (depth: 1)

IL_004d:
	{
		return;
	}
}
// System.Void System.Array::qsort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32,System.Comparison`1<T>)
// System.Void System.Array::qsort<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32,System.Comparison`1<T>)
extern "C" void Array_qsort_TisCustomAttributeNamedArgument_t1691_m26645_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, int32_t ___low0, int32_t ___high0, Comparison_1_t3085 * ___comparison, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	CustomAttributeNamedArgument_t1691  V_3 = {0};
	{
		int32_t L_0 = ___low0;
		int32_t L_1 = ___high0;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		int32_t L_2 = ___low0;
		V_0 = (int32_t)L_2;
		int32_t L_3 = ___high0;
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		V_2 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)L_6))/(int32_t)2))));
		CustomAttributeNamedArgumentU5BU5D_t2130* L_7 = ___array;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_3 = (CustomAttributeNamedArgument_t1691 )(*(CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_7, L_9, sizeof(CustomAttributeNamedArgument_t1691 )));
	}

IL_001c:
	{
		goto IL_0025;
	}

IL_0021:
	{
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = ___high0;
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_0040;
		}
	}
	{
		Comparison_1_t3085 * L_13 = ___comparison;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_14 = ___array;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		CustomAttributeNamedArgument_t1691  L_17 = V_3;
		NullCheck((Comparison_1_t3085 *)L_13);
		int32_t L_18 = (( int32_t (*) (Comparison_1_t3085 *, CustomAttributeNamedArgument_t1691 , CustomAttributeNamedArgument_t1691 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Comparison_1_t3085 *)L_13, (CustomAttributeNamedArgument_t1691 )(*(CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_14, L_16, sizeof(CustomAttributeNamedArgument_t1691 ))), (CustomAttributeNamedArgument_t1691 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_18) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}

IL_0040:
	{
		goto IL_0049;
	}

IL_0045:
	{
		int32_t L_19 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_0049:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = ___low0;
		if ((((int32_t)L_20) <= ((int32_t)L_21)))
		{
			goto IL_0064;
		}
	}
	{
		Comparison_1_t3085 * L_22 = ___comparison;
		CustomAttributeNamedArgument_t1691  L_23 = V_3;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_24 = ___array;
		int32_t L_25 = V_1;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		NullCheck((Comparison_1_t3085 *)L_22);
		int32_t L_27 = (( int32_t (*) (Comparison_1_t3085 *, CustomAttributeNamedArgument_t1691 , CustomAttributeNamedArgument_t1691 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Comparison_1_t3085 *)L_22, (CustomAttributeNamedArgument_t1691 )L_23, (CustomAttributeNamedArgument_t1691 )(*(CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_24, L_26, sizeof(CustomAttributeNamedArgument_t1691 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if ((((int32_t)L_27) < ((int32_t)0)))
		{
			goto IL_0045;
		}
	}

IL_0064:
	{
		int32_t L_28 = V_0;
		int32_t L_29 = V_1;
		if ((((int32_t)L_28) > ((int32_t)L_29)))
		{
			goto IL_0080;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_30 = ___array;
		int32_t L_31 = V_0;
		int32_t L_32 = V_1;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_30, (int32_t)L_31, (int32_t)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_33 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
		int32_t L_34 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_34-(int32_t)1));
		goto IL_0085;
	}

IL_0080:
	{
		goto IL_008a;
	}

IL_0085:
	{
		goto IL_001c;
	}

IL_008a:
	{
		int32_t L_35 = ___low0;
		int32_t L_36 = V_1;
		if ((((int32_t)L_35) >= ((int32_t)L_36)))
		{
			goto IL_009a;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_37 = ___array;
		int32_t L_38 = ___low0;
		int32_t L_39 = V_1;
		Comparison_1_t3085 * L_40 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, Comparison_1_t3085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_37, (int32_t)L_38, (int32_t)L_39, (Comparison_1_t3085 *)L_40, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_009a:
	{
		int32_t L_41 = V_0;
		int32_t L_42 = ___high0;
		if ((((int32_t)L_41) >= ((int32_t)L_42)))
		{
			goto IL_00aa;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_43 = ___array;
		int32_t L_44 = V_0;
		int32_t L_45 = ___high0;
		Comparison_1_t3085 * L_46 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, int32_t, int32_t, Comparison_1_t3085 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_43, (int32_t)L_44, (int32_t)L_45, (Comparison_1_t3085 *)L_46, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
	}

IL_00aa:
	{
		return;
	}
}
// System.Void System.Array::swap<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32)
// System.Void System.Array::swap<System.Reflection.CustomAttributeNamedArgument>(T[],System.Int32,System.Int32)
extern "C" void Array_swap_TisCustomAttributeNamedArgument_t1691_m26646_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, int32_t ___i, int32_t ___j, const MethodInfo* method)
{
	CustomAttributeNamedArgument_t1691  V_0 = {0};
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_0 = ___array;
		int32_t L_1 = ___i;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (CustomAttributeNamedArgument_t1691 )(*(CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_0, L_2, sizeof(CustomAttributeNamedArgument_t1691 )));
		CustomAttributeNamedArgumentU5BU5D_t2130* L_3 = ___array;
		int32_t L_4 = ___i;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_5 = ___array;
		int32_t L_6 = ___j;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		*((CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_3, L_4, sizeof(CustomAttributeNamedArgument_t1691 ))) = (CustomAttributeNamedArgument_t1691 )(*(CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_5, L_7, sizeof(CustomAttributeNamedArgument_t1691 )));
		CustomAttributeNamedArgumentU5BU5D_t2130* L_8 = ___array;
		int32_t L_9 = ___j;
		CustomAttributeNamedArgument_t1691  L_10 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		*((CustomAttributeNamedArgument_t1691 *)(CustomAttributeNamedArgument_t1691 *)SZArrayLdElema(L_8, L_9, sizeof(CustomAttributeNamedArgument_t1691 ))) = (CustomAttributeNamedArgument_t1691 )L_10;
		return;
	}
}
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(T[],T)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(T[],T)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern "C" int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t1691_m26647_gshared (Object_t * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130* ___array, CustomAttributeNamedArgument_t1691  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		s_Il2CppMethodIntialized = true;
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		CustomAttributeNamedArgumentU5BU5D_t2130* L_2 = ___array;
		CustomAttributeNamedArgument_t1691  L_3 = ___value;
		CustomAttributeNamedArgumentU5BU5D_t2130* L_4 = ___array;
		NullCheck(L_4);
		int32_t L_5 = (( int32_t (*) (Object_t * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t2130*, CustomAttributeNamedArgument_t1691 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t2130*)L_2, (CustomAttributeNamedArgument_t1691 )L_3, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_5;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C" ObjectU5BU5D_t105* CustomAttributeData_UnboxValues_TisObject_t_m26648_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t105* ___values, const MethodInfo* method)
{
	ObjectU5BU5D_t105* V_0 = {0};
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t105* L_0 = ___values;
		NullCheck(L_0);
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		ObjectU5BU5D_t105* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t105* L_3 = ___values;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, L_2, sizeof(Object_t *))) = (Object_t *)((Object_t *)Castclass((*(Object_t **)(Object_t **)SZArrayLdElema(L_3, L_5, sizeof(Object_t *))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t105* L_8 = ___values;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		ObjectU5BU5D_t105* L_9 = V_0;
		return L_9;
	}
}
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
extern "C" Object_t * MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m26649_gshared (Object_t * __this /* static, unused */, Getter_2_t3088 * ___getter, Object_t * ___obj, const MethodInfo* method)
{
	{
		Getter_2_t3088 * L_0 = ___getter;
		Object_t * L_1 = ___obj;
		NullCheck((Getter_2_t3088 *)L_0);
		Object_t * L_2 = (( Object_t * (*) (Getter_2_t3088 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((Getter_2_t3088 *)L_0, (Object_t *)((Object_t *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
extern "C" Object_t * MonoProperty_StaticGetterAdapterFrame_TisObject_t_m26650_gshared (Object_t * __this /* static, unused */, StaticGetter_1_t3089 * ___getter, Object_t * ___obj, const MethodInfo* method)
{
	{
		StaticGetter_1_t3089 * L_0 = ___getter;
		NullCheck((StaticGetter_1_t3089 *)L_0);
		Object_t * L_1 = (( Object_t * (*) (StaticGetter_1_t3089 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((StaticGetter_1_t3089 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" ResourceInfo_t1721  Array_InternalArray__get_Item_TisResourceInfo_t1721_m26651_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ResourceInfo_t1721  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (ResourceInfo_t1721 *)(&V_0));
		ResourceInfo_t1721  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Resources.ResourceReader/ResourceInfo>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Resources.ResourceReader/ResourceInfo>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisResourceInfo_t1721_m26652_gshared (Array_t * __this, ResourceInfo_t1721  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Resources.ResourceReader/ResourceInfo>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Resources.ResourceReader/ResourceInfo>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisResourceInfo_t1721_m26653_gshared (Array_t * __this, ResourceInfo_t1721  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ResourceInfo_t1721  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (ResourceInfo_t1721 *)(&V_2));
		ResourceInfo_t1721  L_5 = ___item;
		goto IL_004d;
	}
	{
		ResourceInfo_t1721  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		ResourceInfo_t1721  L_7 = V_2;
		ResourceInfo_t1721  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Resources.ResourceReader/ResourceInfo>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Resources.ResourceReader/ResourceInfo>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisResourceInfo_t1721_m26654_gshared (Array_t * __this, ResourceInfoU5BU5D_t1725* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		ResourceInfoU5BU5D_t1725* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		ResourceInfoU5BU5D_t1725* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		ResourceInfoU5BU5D_t1725* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		ResourceInfoU5BU5D_t1725* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		ResourceInfoU5BU5D_t1725* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Resources.ResourceReader/ResourceInfo>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Resources.ResourceReader/ResourceInfo>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisResourceInfo_t1721_m26655_gshared (Array_t * __this, ResourceInfo_t1721  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Resources.ResourceReader/ResourceInfo>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Resources.ResourceReader/ResourceInfo>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisResourceInfo_t1721_m26656_gshared (Array_t * __this, ResourceInfo_t1721  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ResourceInfo_t1721  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (ResourceInfo_t1721 *)(&V_2));
		ResourceInfo_t1721  L_5 = ___item;
		goto IL_005d;
	}
	{
		ResourceInfo_t1721  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		ResourceInfo_t1721  L_10 = ___item;
		ResourceInfo_t1721  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisResourceInfo_t1721_m26657_gshared (Array_t * __this, int32_t ___index, ResourceInfo_t1721  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisResourceInfo_t1721_m26658_gshared (Array_t * __this, int32_t ___index, ResourceInfo_t1721  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		ResourceInfo_t1721  L_6 = ___item;
		ResourceInfo_t1721  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (ResourceInfo_t1721 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Resources.ResourceReader/ResourceInfo>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Resources.ResourceReader/ResourceInfo>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Resources.ResourceReader/ResourceInfo>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisResourceInfo_t1721_m26659_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3090  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3090 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3090  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" ResourceCacheItem_t1722  Array_InternalArray__get_Item_TisResourceCacheItem_t1722_m26660_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ResourceCacheItem_t1722  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (ResourceCacheItem_t1722 *)(&V_0));
		ResourceCacheItem_t1722  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Resources.ResourceReader/ResourceCacheItem>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisResourceCacheItem_t1722_m26661_gshared (Array_t * __this, ResourceCacheItem_t1722  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Resources.ResourceReader/ResourceCacheItem>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisResourceCacheItem_t1722_m26662_gshared (Array_t * __this, ResourceCacheItem_t1722  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ResourceCacheItem_t1722  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (ResourceCacheItem_t1722 *)(&V_2));
		ResourceCacheItem_t1722  L_5 = ___item;
		goto IL_004d;
	}
	{
		ResourceCacheItem_t1722  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		ResourceCacheItem_t1722  L_7 = V_2;
		ResourceCacheItem_t1722  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Resources.ResourceReader/ResourceCacheItem>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Resources.ResourceReader/ResourceCacheItem>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisResourceCacheItem_t1722_m26663_gshared (Array_t * __this, ResourceCacheItemU5BU5D_t1726* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		ResourceCacheItemU5BU5D_t1726* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		ResourceCacheItemU5BU5D_t1726* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		ResourceCacheItemU5BU5D_t1726* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		ResourceCacheItemU5BU5D_t1726* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		ResourceCacheItemU5BU5D_t1726* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Resources.ResourceReader/ResourceCacheItem>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisResourceCacheItem_t1722_m26664_gshared (Array_t * __this, ResourceCacheItem_t1722  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Resources.ResourceReader/ResourceCacheItem>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Resources.ResourceReader/ResourceCacheItem>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisResourceCacheItem_t1722_m26665_gshared (Array_t * __this, ResourceCacheItem_t1722  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ResourceCacheItem_t1722  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (ResourceCacheItem_t1722 *)(&V_2));
		ResourceCacheItem_t1722  L_5 = ___item;
		goto IL_005d;
	}
	{
		ResourceCacheItem_t1722  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		ResourceCacheItem_t1722  L_10 = ___item;
		ResourceCacheItem_t1722  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisResourceCacheItem_t1722_m26666_gshared (Array_t * __this, int32_t ___index, ResourceCacheItem_t1722  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisResourceCacheItem_t1722_m26667_gshared (Array_t * __this, int32_t ___index, ResourceCacheItem_t1722  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		ResourceCacheItem_t1722  L_6 = ___item;
		ResourceCacheItem_t1722  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (ResourceCacheItem_t1722 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Resources.ResourceReader/ResourceCacheItem>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Resources.ResourceReader/ResourceCacheItem>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisResourceCacheItem_t1722_m26668_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3091  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3091 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3091  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" DateTime_t577  Array_InternalArray__get_Item_TisDateTime_t577_m26669_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t577  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (DateTime_t577 *)(&V_0));
		DateTime_t577  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.DateTime>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t577_m26670_gshared (Array_t * __this, DateTime_t577  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisDateTime_t577_m26671_gshared (Array_t * __this, DateTime_t577  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	DateTime_t577  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (DateTime_t577 *)(&V_2));
		DateTime_t577  L_5 = ___item;
		goto IL_004d;
	}
	{
		DateTime_t577  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		DateTime_t577  L_7 = V_2;
		DateTime_t577  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((DateTime_t577 *)(&___item));
		bool L_10 = DateTime_Equals_m12225((DateTime_t577 *)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t577_m26672_gshared (Array_t * __this, DateTimeU5BU5D_t2152* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTimeU5BU5D_t2152* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DateTimeU5BU5D_t2152* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		DateTimeU5BU5D_t2152* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		DateTimeU5BU5D_t2152* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DateTimeU5BU5D_t2152* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisDateTime_t577_m26673_gshared (Array_t * __this, DateTime_t577  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisDateTime_t577_m26674_gshared (Array_t * __this, DateTime_t577  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	DateTime_t577  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (DateTime_t577 *)(&V_2));
		DateTime_t577  L_5 = ___item;
		goto IL_005d;
	}
	{
		DateTime_t577  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		DateTime_t577  L_10 = ___item;
		DateTime_t577  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((DateTime_t577 *)(&V_2));
		bool L_13 = DateTime_Equals_m12225((DateTime_t577 *)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisDateTime_t577_m26675_gshared (Array_t * __this, int32_t ___index, DateTime_t577  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisDateTime_t577_m26676_gshared (Array_t * __this, int32_t ___index, DateTime_t577  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		DateTime_t577  L_6 = ___item;
		DateTime_t577  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (DateTime_t577 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.DateTime>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t577_m26677_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3096  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3096 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3096  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" Decimal_t1445  Array_InternalArray__get_Item_TisDecimal_t1445_m26678_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	Decimal_t1445  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (Decimal_t1445 *)(&V_0));
		Decimal_t1445  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Decimal>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t1445_m26679_gshared (Array_t * __this, Decimal_t1445  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisDecimal_t1445_m26680_gshared (Array_t * __this, Decimal_t1445  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Decimal_t1445  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Decimal_t1445 *)(&V_2));
		Decimal_t1445  L_5 = ___item;
		goto IL_004d;
	}
	{
		Decimal_t1445  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		Decimal_t1445  L_7 = V_2;
		Decimal_t1445  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Decimal_t1445 *)(&___item));
		bool L_10 = Decimal_Equals_m8079((Decimal_t1445 *)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t1445_m26681_gshared (Array_t * __this, DecimalU5BU5D_t2153* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		DecimalU5BU5D_t2153* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DecimalU5BU5D_t2153* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		DecimalU5BU5D_t2153* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		DecimalU5BU5D_t2153* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DecimalU5BU5D_t2153* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisDecimal_t1445_m26682_gshared (Array_t * __this, Decimal_t1445  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisDecimal_t1445_m26683_gshared (Array_t * __this, Decimal_t1445  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Decimal_t1445  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Decimal_t1445 *)(&V_2));
		Decimal_t1445  L_5 = ___item;
		goto IL_005d;
	}
	{
		Decimal_t1445  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		Decimal_t1445  L_10 = ___item;
		Decimal_t1445  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Decimal_t1445 *)(&V_2));
		bool L_13 = Decimal_Equals_m8079((Decimal_t1445 *)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisDecimal_t1445_m26684_gshared (Array_t * __this, int32_t ___index, Decimal_t1445  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisDecimal_t1445_m26685_gshared (Array_t * __this, int32_t ___index, Decimal_t1445  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		Decimal_t1445  L_6 = ___item;
		Decimal_t1445  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (Decimal_t1445 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Decimal>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1445_m26686_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3097  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3097 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3097  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" TimeSpan_t975  Array_InternalArray__get_Item_TisTimeSpan_t975_m26687_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t975  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (TimeSpan_t975 *)(&V_0));
		TimeSpan_t975  L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t975_m26688_gshared (Array_t * __this, TimeSpan_t975  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisTimeSpan_t975_m26689_gshared (Array_t * __this, TimeSpan_t975  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TimeSpan_t975  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (TimeSpan_t975 *)(&V_2));
		TimeSpan_t975  L_5 = ___item;
		goto IL_004d;
	}
	{
		TimeSpan_t975  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		TimeSpan_t975  L_7 = V_2;
		TimeSpan_t975  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((TimeSpan_t975 *)(&___item));
		bool L_10 = TimeSpan_Equals_m12629((TimeSpan_t975 *)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t975_m26690_gshared (Array_t * __this, TimeSpanU5BU5D_t2154* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		TimeSpanU5BU5D_t2154* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TimeSpanU5BU5D_t2154* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		TimeSpanU5BU5D_t2154* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		TimeSpanU5BU5D_t2154* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TimeSpanU5BU5D_t2154* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisTimeSpan_t975_m26691_gshared (Array_t * __this, TimeSpan_t975  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisTimeSpan_t975_m26692_gshared (Array_t * __this, TimeSpan_t975  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TimeSpan_t975  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (TimeSpan_t975 *)(&V_2));
		TimeSpan_t975  L_5 = ___item;
		goto IL_005d;
	}
	{
		TimeSpan_t975  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		TimeSpan_t975  L_10 = ___item;
		TimeSpan_t975  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((TimeSpan_t975 *)(&V_2));
		bool L_13 = TimeSpan_Equals_m12629((TimeSpan_t975 *)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t975_m26693_gshared (Array_t * __this, int32_t ___index, TimeSpan_t975  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t975_m26694_gshared (Array_t * __this, int32_t ___index, TimeSpan_t975  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		TimeSpan_t975  L_6 = ___item;
		TimeSpan_t975  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (TimeSpan_t975 *)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.TimeSpan>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t975_m26695_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3098  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3098 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3098  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" uint8_t Array_InternalArray__get_Item_TisTypeTag_t1858_m26696_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (uint8_t*)(&V_0));
		uint8_t L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T&)
// System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__ICollection_Add_TisTypeTag_t1858_m26697_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" bool Array_InternalArray__ICollection_Contains_TisTypeTag_t1858_m26698_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (uint8_t*)(&V_2));
		uint8_t L_5 = ___item;
		goto IL_004d;
	}
	{
		uint8_t L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		uint8_t L_7 = V_2;
		uint8_t L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t718_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral861;
extern Il2CppCodeGenString* _stringLiteral1319;
extern Il2CppCodeGenString* _stringLiteral1356;
extern Il2CppCodeGenString* _stringLiteral863;
extern Il2CppCodeGenString* _stringLiteral1342;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTypeTag_t1858_m26699_gshared (Array_t * __this, TypeTagU5BU5D_t2155* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(418);
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral861 = il2cpp_codegen_string_literal_from_index(861);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		_stringLiteral1356 = il2cpp_codegen_string_literal_from_index(1356);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		_stringLiteral1342 = il2cpp_codegen_string_literal_from_index(1342);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeTagU5BU5D_t2155* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t718 * L_1 = (ArgumentNullException_t718 *)il2cpp_codegen_object_new (ArgumentNullException_t718_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3631(L_1, (String_t*)_stringLiteral861, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_4 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TypeTagU5BU5D_t2155* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		TypeTagU5BU5D_t2155* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m8293((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t442 * L_11 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_11, (String_t*)_stringLiteral1356, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0055:
	{
		TypeTagU5BU5D_t2155* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m7544((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_15 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1342, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1083 * L_18 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m5620(L_18, (String_t*)_stringLiteral863, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TypeTagU5BU5D_t2155* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m8293((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m8335(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" bool Array_InternalArray__ICollection_Remove_TisTypeTag_t1858_m26700_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo* RankException_t2057_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1319;
extern "C" int32_t Array_InternalArray__IndexOf_TisTypeTag_t1858_m26701_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2057_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(983);
		_stringLiteral1319 = il2cpp_codegen_string_literal_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m7544((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m8486(NULL /*static, unused*/, (String_t*)_stringLiteral1319, /*hidden argument*/NULL);
		RankException_t2057 * L_2 = (RankException_t2057 *)il2cpp_codegen_object_new (RankException_t2057_il2cpp_TypeInfo_var);
		RankException__ctor_m12582(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (uint8_t*)(&V_2));
		uint8_t L_5 = ___item;
		goto IL_005d;
	}
	{
		uint8_t L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		uint8_t L_10 = ___item;
		uint8_t L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m8295((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
// System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern TypeInfo* NotSupportedException_t405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void Array_InternalArray__Insert_TisTypeTag_t1858_m26702_gshared (Array_t * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t405 * L_0 = (NotSupportedException_t405 *)il2cpp_codegen_object_new (NotSupportedException_t405_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m6522(L_0, (String_t*)_stringLiteral884, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863;
extern "C" void Array_InternalArray__set_Item_TisTypeTag_t1858_m26703_gshared (Array_t * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral863 = il2cpp_codegen_string_literal_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t105* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m7541((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1083 * L_2 = (ArgumentOutOfRangeException_t1083 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1083_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6521(L_2, (String_t*)_stringLiteral863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t105*)((ObjectU5BU5D_t105*)IsInst(__this, ObjectU5BU5D_t105_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t105* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t105* L_4 = V_0;
		int32_t L_5 = ___index;
		uint8_t L_6 = ___item;
		uint8_t L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (uint8_t*)(&___item));
		return;
	}
}
// System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T&)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.TypeTag>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.TypeTag>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t1858_m26704_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3099  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3099 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3099  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
