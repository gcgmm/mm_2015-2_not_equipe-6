﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.EqualityComparer`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct EqualityComparer_1_t2908;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.EqualityComparer`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct  EqualityComparer_1_t2908  : public Object_t
{
};
struct EqualityComparer_1_t2908_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::_default
	EqualityComparer_1_t2908 * ____default_0;
};
