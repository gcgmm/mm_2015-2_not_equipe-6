﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Math.BigInteger
struct BigInteger_t1507;
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t2105;
// System.UInt32[]
struct UInt32U5BU5D_t1073;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_Mono_Math_BigInteger_Sign.h"

// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::AddSameSign(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1507 * Kernel_AddSameSign_m8621 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___bi1, BigInteger_t1507 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::Subtract(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1507 * Kernel_Subtract_m8622 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___big, BigInteger_t1507 * ___small, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/Kernel::MinusEq(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" void Kernel_MinusEq_m8623 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___big, BigInteger_t1507 * ___small, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/Kernel::PlusEq(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" void Kernel_PlusEq_m8624 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___bi1, BigInteger_t1507 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger/Sign Mono.Math.BigInteger/Kernel::Compare(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" int32_t Kernel_Compare_m8625 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___bi1, BigInteger_t1507 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Math.BigInteger/Kernel::SingleByteDivideInPlace(Mono.Math.BigInteger,System.UInt32)
extern "C" uint32_t Kernel_SingleByteDivideInPlace_m8626 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___n, uint32_t ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Math.BigInteger/Kernel::DwordMod(Mono.Math.BigInteger,System.UInt32)
extern "C" uint32_t Kernel_DwordMod_m8627 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___n, uint32_t ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger[] Mono.Math.BigInteger/Kernel::DwordDivMod(Mono.Math.BigInteger,System.UInt32)
extern "C" BigIntegerU5BU5D_t2105* Kernel_DwordDivMod_m8628 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___n, uint32_t ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger[] Mono.Math.BigInteger/Kernel::multiByteDivide(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigIntegerU5BU5D_t2105* Kernel_multiByteDivide_m8629 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___bi1, BigInteger_t1507 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::LeftShift(Mono.Math.BigInteger,System.Int32)
extern "C" BigInteger_t1507 * Kernel_LeftShift_m8630 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___bi, int32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::RightShift(Mono.Math.BigInteger,System.Int32)
extern "C" BigInteger_t1507 * Kernel_RightShift_m8631 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___bi, int32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::MultiplyByDword(Mono.Math.BigInteger,System.UInt32)
extern "C" BigInteger_t1507 * Kernel_MultiplyByDword_m8632 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___n, uint32_t ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/Kernel::Multiply(System.UInt32[],System.UInt32,System.UInt32,System.UInt32[],System.UInt32,System.UInt32,System.UInt32[],System.UInt32)
extern "C" void Kernel_Multiply_m8633 (Object_t * __this /* static, unused */, UInt32U5BU5D_t1073* ___x, uint32_t ___xOffset, uint32_t ___xLen, UInt32U5BU5D_t1073* ___y, uint32_t ___yOffset, uint32_t ___yLen, UInt32U5BU5D_t1073* ___d, uint32_t ___dOffset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/Kernel::MultiplyMod2p32pmod(System.UInt32[],System.Int32,System.Int32,System.UInt32[],System.Int32,System.Int32,System.UInt32[],System.Int32,System.Int32)
extern "C" void Kernel_MultiplyMod2p32pmod_m8634 (Object_t * __this /* static, unused */, UInt32U5BU5D_t1073* ___x, int32_t ___xOffset, int32_t ___xLen, UInt32U5BU5D_t1073* ___y, int32_t ___yOffest, int32_t ___yLen, UInt32U5BU5D_t1073* ___d, int32_t ___dOffset, int32_t ___mod, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Math.BigInteger/Kernel::modInverse(Mono.Math.BigInteger,System.UInt32)
extern "C" uint32_t Kernel_modInverse_m8635 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___bi, uint32_t ___modulus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::modInverse(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1507 * Kernel_modInverse_m8636 (Object_t * __this /* static, unused */, BigInteger_t1507 * ___bi, BigInteger_t1507 * ___modulus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
