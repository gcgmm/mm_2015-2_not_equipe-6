﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2634;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_20.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m19030_gshared (Enumerator_t2640 * __this, Dictionary_2_t2634 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m19030(__this, ___host, method) (( void (*) (Enumerator_t2640 *, Dictionary_2_t2634 *, const MethodInfo*))Enumerator__ctor_m19030_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m19031_gshared (Enumerator_t2640 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m19031(__this, method) (( Object_t * (*) (Enumerator_t2640 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19031_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m19032_gshared (Enumerator_t2640 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m19032(__this, method) (( void (*) (Enumerator_t2640 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m19032_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m19033_gshared (Enumerator_t2640 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m19033(__this, method) (( void (*) (Enumerator_t2640 *, const MethodInfo*))Enumerator_Dispose_m19033_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m19034_gshared (Enumerator_t2640 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m19034(__this, method) (( bool (*) (Enumerator_t2640 *, const MethodInfo*))Enumerator_MoveNext_m19034_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m19035_gshared (Enumerator_t2640 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m19035(__this, method) (( Object_t * (*) (Enumerator_t2640 *, const MethodInfo*))Enumerator_get_Current_m19035_gshared)(__this, method)
