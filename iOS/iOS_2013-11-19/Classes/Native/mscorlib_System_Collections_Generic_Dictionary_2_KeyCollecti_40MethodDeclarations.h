﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t1038;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_40.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m23144_gshared (Enumerator_t2900 * __this, Dictionary_2_t1038 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m23144(__this, ___host, method) (( void (*) (Enumerator_t2900 *, Dictionary_2_t1038 *, const MethodInfo*))Enumerator__ctor_m23144_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23145_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m23145(__this, method) (( Object_t * (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23145_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m23146_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m23146(__this, method) (( void (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m23146_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
extern "C" void Enumerator_Dispose_m23147_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m23147(__this, method) (( void (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_Dispose_m23147_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m23148_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m23148(__this, method) (( bool (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_MoveNext_m23148_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern "C" int32_t Enumerator_get_Current_m23149_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m23149(__this, method) (( int32_t (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_get_Current_m23149_gshared)(__this, method)
