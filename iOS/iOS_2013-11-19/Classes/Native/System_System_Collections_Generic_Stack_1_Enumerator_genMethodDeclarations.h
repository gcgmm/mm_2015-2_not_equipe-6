﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t2220;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C" void Enumerator__ctor_m13431_gshared (Enumerator_t2222 * __this, Stack_1_t2220 * ___t, const MethodInfo* method);
#define Enumerator__ctor_m13431(__this, ___t, method) (( void (*) (Enumerator_t2222 *, Stack_1_t2220 *, const MethodInfo*))Enumerator__ctor_m13431_gshared)(__this, ___t, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m13432_gshared (Enumerator_t2222 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m13432(__this, method) (( void (*) (Enumerator_t2222 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m13432_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m13433_gshared (Enumerator_t2222 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m13433(__this, method) (( Object_t * (*) (Enumerator_t2222 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13433_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m13434_gshared (Enumerator_t2222 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m13434(__this, method) (( void (*) (Enumerator_t2222 *, const MethodInfo*))Enumerator_Dispose_m13434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m13435_gshared (Enumerator_t2222 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m13435(__this, method) (( bool (*) (Enumerator_t2222 *, const MethodInfo*))Enumerator_MoveNext_m13435_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m13436_gshared (Enumerator_t2222 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m13436(__this, method) (( Object_t * (*) (Enumerator_t2222 *, const MethodInfo*))Enumerator_get_Current_m13436_gshared)(__this, method)
