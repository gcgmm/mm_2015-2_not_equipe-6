﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.TurnOffWordBehaviour
struct TurnOffWordBehaviour_t63;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
extern "C" void TurnOffWordBehaviour__ctor_m120 (TurnOffWordBehaviour_t63 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
extern "C" void TurnOffWordBehaviour_Awake_m121 (TurnOffWordBehaviour_t63 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
