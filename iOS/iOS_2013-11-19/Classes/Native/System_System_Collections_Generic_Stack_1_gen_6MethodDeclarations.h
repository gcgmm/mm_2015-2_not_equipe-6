﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::.ctor()
#define Stack_1__ctor_m17729(__this, method) (( void (*) (Stack_1_t2532 *, const MethodInfo*))Stack_1__ctor_m13420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m17730(__this, method) (( bool (*) (Stack_1_t2532 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m13421_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m17731(__this, method) (( Object_t * (*) (Stack_1_t2532 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m13422_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m17732(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t2532 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m13423_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17733(__this, method) (( Object_t* (*) (Stack_1_t2532 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13424_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m17734(__this, method) (( Object_t * (*) (Stack_1_t2532 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m13425_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Peek()
#define Stack_1_Peek_m17735(__this, method) (( List_1_t345 * (*) (Stack_1_t2532 *, const MethodInfo*))Stack_1_Peek_m13426_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Pop()
#define Stack_1_Pop_m17736(__this, method) (( List_1_t345 * (*) (Stack_1_t2532 *, const MethodInfo*))Stack_1_Pop_m13427_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Push(T)
#define Stack_1_Push_m17737(__this, ___t, method) (( void (*) (Stack_1_t2532 *, List_1_t345 *, const MethodInfo*))Stack_1_Push_m13428_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::get_Count()
#define Stack_1_get_Count_m17738(__this, method) (( int32_t (*) (Stack_1_t2532 *, const MethodInfo*))Stack_1_get_Count_m13429_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::GetEnumerator()
#define Stack_1_GetEnumerator_m17739(__this, method) (( Enumerator_t3176  (*) (Stack_1_t2532 *, const MethodInfo*))Stack_1_GetEnumerator_m13430_gshared)(__this, method)
