﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__35MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m24715(__this, ___dictionary, method) (( void (*) (Enumerator_t3023 *, Dictionary_2_t1310 *, const MethodInfo*))Enumerator__ctor_m24609_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24716(__this, method) (( Object_t * (*) (Enumerator_t3023 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24610_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m24717(__this, method) (( void (*) (Enumerator_t3023 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m24611_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24718(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t3023 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24612_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24719(__this, method) (( Object_t * (*) (Enumerator_t3023 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24613_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24720(__this, method) (( Object_t * (*) (Enumerator_t3023 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24614_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m24721(__this, method) (( bool (*) (Enumerator_t3023 *, const MethodInfo*))Enumerator_MoveNext_m24615_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m24722(__this, method) (( KeyValuePair_2_t3020  (*) (Enumerator_t3023 *, const MethodInfo*))Enumerator_get_Current_m24616_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m24723(__this, method) (( String_t* (*) (Enumerator_t3023 *, const MethodInfo*))Enumerator_get_CurrentKey_m24617_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m24724(__this, method) (( bool (*) (Enumerator_t3023 *, const MethodInfo*))Enumerator_get_CurrentValue_m24618_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Reset()
#define Enumerator_Reset_m24725(__this, method) (( void (*) (Enumerator_t3023 *, const MethodInfo*))Enumerator_Reset_m24619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m24726(__this, method) (( void (*) (Enumerator_t3023 *, const MethodInfo*))Enumerator_VerifyState_m24620_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m24727(__this, method) (( void (*) (Enumerator_t3023 *, const MethodInfo*))Enumerator_VerifyCurrent_m24621_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m24728(__this, method) (( void (*) (Enumerator_t3023 *, const MethodInfo*))Enumerator_Dispose_m24622_gshared)(__this, method)
