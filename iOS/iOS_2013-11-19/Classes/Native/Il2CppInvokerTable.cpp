﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Object
struct Object_t;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t175;
// UnityEngine.UI.ILayoutElement
struct ILayoutElement_t389;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t480;
// System.String
struct String_t;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t71;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t52;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t58;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t827;
// System.Byte[]
struct ByteU5BU5D_t786;
// System.Collections.Specialized.ListDictionary/DictionaryNode
struct DictionaryNode_t1277;
// System.Net.IPAddress
struct IPAddress_t1305;
// System.Net.IPv6Address
struct IPv6Address_t1307;
// System.UriFormatException
struct UriFormatException_t1414;
// System.Object[]
struct ObjectU5BU5D_t105;
// System.Exception
struct Exception_t107;
// System.MulticastDelegate
struct MulticastDelegate_t259;
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t1497;
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t1498;
// Mono.Globalization.Unicode.CodePointIndexer
struct CodePointIndexer_t1481;
// Mono.Globalization.Unicode.Contraction
struct Contraction_t1484;
// System.Reflection.MethodBase
struct MethodBase_t714;
// System.Reflection.Module
struct Module_t1651;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1812;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t2075;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t687;
// System.Text.StringBuilder
struct StringBuilder_t390;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t1966;
// System.Char[]
struct CharU5BU5D_t270;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t1957;
// System.Int64[]
struct Int64U5BU5D_t2103;
// System.String[]
struct StringU5BU5D_t87;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2238;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t266;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t121;
// System.Int32[]
struct Int32U5BU5D_t122;
// UnityEngine.Color32[]
struct Color32U5BU5D_t456;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t249;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t457;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t683;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t684;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t2702;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t2914;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2129;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2130;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_SByte.h"
#include "mscorlib_System_Boolean.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirection.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Fra.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul.h"
#include "UnityEngine_UnityEngine_LayerMask.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls_Resources.h"
#include "UnityEngine_UnityEngine_FontStyle.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjec.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidation.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Int16.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementType.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollbarVisibility.h"
#include "UnityEngine_UnityEngine_Bounds.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_Int64.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#include "UnityEngine_UnityEngine_BoneWeight.h"
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_TextureFormat.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_RenderBuffer.h"
#include "UnityEngine_UnityEngine_TouchPhase.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
#include "UnityEngine_UnityEngine_CharacterInfo.h"
#include "UnityEngine_UnityEngine_RenderMode.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "UnityEngine_UnityEngine_EventModifiers.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "mscorlib_System_DateTime.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid_0.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetType.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterMode.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordPrefabCreationM.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton_Sensi.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi_0.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeha_0.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__11.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid_1.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox_0.h"
#include "mscorlib_System_UInt16.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainInitial.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavio.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeha.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__7.h"
#include "System_System_Collections_Generic_LinkedList_1_gen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_InitSt.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Update.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
#include "mscorlib_System_UInt32.h"
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
#include "mscorlib_System_UInt64.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "System_System_ComponentModel_EditorBrowsableState.h"
#include "System_System_Net_IPAddress.h"
#include "System_System_Net_Sockets_AddressFamily.h"
#include "System_System_Net_IPv6Address.h"
#include "System_System_Net_SecurityProtocolType.h"
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
#include "System_System_Text_RegularExpressions_RegexOptions.h"
#include "System_System_Text_RegularExpressions_Category.h"
#include "System_System_Text_RegularExpressions_OpFlags.h"
#include "System_System_Text_RegularExpressions_Interval.h"
#include "System_System_Text_RegularExpressions_Position.h"
#include "System_System_UriHostNameType.h"
#include "System_System_UriFormatException.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_TypeCode.h"
#include "mscorlib_System_Globalization_UnicodeCategory.h"
#include "mscorlib_System_UIntPtr.h"
#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_MemberTypes.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_IO_MonoIOError.h"
#include "mscorlib_System_IO_FileAttributes.h"
#include "mscorlib_System_IO_MonoFileType.h"
#include "mscorlib_System_IO_MonoIOStat.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
#include "mscorlib_System_Reflection_FieldAttributes.h"
#include "mscorlib_System_Reflection_Emit_OpCode.h"
#include "mscorlib_System_Reflection_Emit_StackBehaviour.h"
#include "mscorlib_System_Reflection_Module.h"
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
#include "mscorlib_System_Reflection_EventAttributes.h"
#include "mscorlib_System_Reflection_MonoEventInfo.h"
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
#include "mscorlib_System_Reflection_PropertyAttributes.h"
#include "mscorlib_System_Reflection_ParameterAttributes.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceInfo.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
#include "mscorlib_System_DateTimeKind.h"
#include "mscorlib_System_DateTimeOffset.h"
#include "mscorlib_System_Nullable_1_gen.h"
#include "mscorlib_System_MonoEnumInfo.h"
#include "mscorlib_System_PlatformID.h"
#include "mscorlib_System_Guid.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_10.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_30.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_26.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatTween.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_10.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_42.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_Link.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
#include "UnityEngine_UnityEngine_ContactPoint.h"
#include "UnityEngine_UnityEngine_ContactPoint2D.h"
#include "UnityEngine_UnityEngine_WebCamDevice.h"
#include "UnityEngine_UnityEngine_Keyframe.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_17.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__5.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__4.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__8.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__9.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__10.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_30.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_32.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_35.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
#include "System_System_Text_RegularExpressions_Mark.h"
#include "System_System_Uri_UriScheme.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
#include "mscorlib_System_Collections_Hashtable_Slot.h"
#include "mscorlib_System_Collections_SortedList_Slot.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCacheItem.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_31.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_5.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_8.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_41.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_50.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_51.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_52.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_53.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_55.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_56.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_20.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_39.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_24.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_58.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_30.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_44.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__29.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_38.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_48.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__30.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_40.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_50.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_62.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__32.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_43.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_53.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__35.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_47.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_56.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_63.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_64.h"

void* RuntimeInvoker_Void_t1449 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, const MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_InitError_t905_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_RaycastResult_t169_RaycastResult_t169 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t169  p1, RaycastResult_t169  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t169 *)args[0]), *((RaycastResult_t169 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t171  (*Func)(void* obj, const MethodInfo* method);
	Vector2_t171  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t171  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t171 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_MoveDirection_t166 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastResult_t169 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t169  (*Func)(void* obj, const MethodInfo* method);
	RaycastResult_t169  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_RaycastResult_t169 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResult_t169  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastResult_t169 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_InputButton_t173 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastResult_t169_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t169  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RaycastResult_t169  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MoveDirection_t166_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MoveDirection_t166_Single_t117_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Single_t117_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_PointerEventDataU26_t3514_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, PointerEventData_t175 ** p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (PointerEventData_t175 **)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Touch_t375_BooleanU26_t3515_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Touch_t375  p1, bool* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Touch_t375 *)args[0]), (bool*)args[1], (bool*)args[2], method);
	return ret;
}

void* RuntimeInvoker_FramePressState_t174_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t393_Vector2_t171_Vector2_t171_Single_t117_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t171  p1, Vector2_t171  p2, float p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Vector2_t171 *)args[1]), *((float*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_InputMode_t184 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_LayerMask_t13 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t13  (*Func)(void* obj, const MethodInfo* method);
	LayerMask_t13  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_LayerMask_t13 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LayerMask_t13  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LayerMask_t13 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_RaycastHit_t89_RaycastHit_t89 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t89  p1, RaycastHit_t89  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t89 *)args[0]), *((RaycastHit_t89 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t77 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t77  (*Func)(void* obj, const MethodInfo* method);
	Color_t77  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Color_t77 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t77  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t77 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_ColorTweenMode_t190 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ColorBlock_t207 (const MethodInfo* method, void* obj, void** args)
{
	typedef ColorBlock_t207  (*Func)(void* obj, const MethodInfo* method);
	ColorBlock_t207  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector2_t171  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t171 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Resources_t208 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Resources_t208  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Resources_t208 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1443_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Single_t117_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_FontStyle_t595 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextAnchor_t444 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HorizontalWrapMode_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VerticalWrapMode_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Vector2_t171_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t171  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t171_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t171  (*Func)(void* obj, Vector2_t171  p1, const MethodInfo* method);
	Vector2_t171  ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t94  (*Func)(void* obj, const MethodInfo* method);
	Rect_t94  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Color_t77_Single_t117_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t77  p1, float p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t77 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Color_t77_Single_t117_SByte_t1443_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t77  p1, float p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t77 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t77_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t77  (*Func)(void* obj, float p1, const MethodInfo* method);
	Color_t77  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Single_t117_Single_t117_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_BlockingObjects_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Vector2_t171_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector2_t171  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector2_t171 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Type_t241 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FillMethod_t242 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t350_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t350  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Vector4_t350  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Color32_t382_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Color32_t382  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color32_t382 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Vector2_t171_Vector2_t171_Color32_t382_Vector2_t171_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t171  p2, Vector2_t171  p3, Color32_t382  p4, Vector2_t171  p5, Vector2_t171  p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t171 *)args[1]), *((Vector2_t171 *)args[2]), *((Color32_t382 *)args[3]), *((Vector2_t171 *)args[4]), *((Vector2_t171 *)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Vector4_t350_Vector4_t350_Rect_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t350  (*Func)(void* obj, Vector4_t350  p1, Rect_t94  p2, const MethodInfo* method);
	Vector4_t350  ret = ((Func)method->method)(obj, *((Vector4_t350 *)args[0]), *((Rect_t94 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Single_t117_SByte_t1443_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Single_t117_Single_t117_SByte_t1443_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t171_Vector2_t171_Rect_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t171  (*Func)(void* obj, Vector2_t171  p1, Rect_t94  p2, const MethodInfo* method);
	Vector2_t171  ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Rect_t94 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContentType_t250 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LineType_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_InputType_t251 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TouchScreenKeyboardType_t425 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CharacterValidation_t252 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t424 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Vector2_t171_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t171  p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t171  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EditState_t257_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t171  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t171 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t117_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t424_Object_t_Int32_t392_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Int16_t1444_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Char_t424_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Rect_t94_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t94  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t94 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Mode_t279 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Navigation_t280 (const MethodInfo* method, void* obj, void** args)
{
	typedef Navigation_t280  (*Func)(void* obj, const MethodInfo* method);
	Navigation_t280  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Rect_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t94  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t94 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Direction_t286 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Single_t117_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Axis_t288 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MovementType_t292 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScrollbarVisibility_t293 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Single_t117_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t117_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Bounds_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef Bounds_t90  (*Func)(void* obj, const MethodInfo* method);
	Bounds_t90  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Navigation_t280 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Navigation_t280  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Navigation_t280 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Transition_t297 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_ColorBlock_t207 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorBlock_t207  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorBlock_t207 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_SpriteState_t299 (const MethodInfo* method, void* obj, void** args)
{
	typedef SpriteState_t299  (*Func)(void* obj, const MethodInfo* method);
	SpriteState_t299  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_SpriteState_t299 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SpriteState_t299  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SpriteState_t299 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_SelectionState_t298 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Vector3_t12_Object_t_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Object_t * p1, Vector2_t171  p2, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t171 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Color_t77_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t77  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t77 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_ColorU26_t3517_Color_t77 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t77 * p1, Color_t77  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Color_t77 *)args[0], *((Color_t77 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Direction_t303 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Axis_t305 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return ret;
}

void* RuntimeInvoker_TextGenerationSettings_t385_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t385  (*Func)(void* obj, Vector2_t171  p1, const MethodInfo* method);
	TextGenerationSettings_t385  ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t171_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t171  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector2_t171  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t94_Object_t_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t94  (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	Rect_t94  ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t94_Rect_t94_Rect_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t94  (*Func)(void* obj, Rect_t94  p1, Rect_t94  p2, const MethodInfo* method);
	Rect_t94  ret = ((Func)method->method)(obj, *((Rect_t94 *)args[0]), *((Rect_t94 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t94_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t94  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Rect_t94  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AspectMode_t319 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Single_t117_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScaleMode_t321 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScreenMatchMode_t322 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Unit_t323 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FitMode_t325 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Corner_t327 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Axis_t328 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Constraint_t329 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Int32_t392_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Single_t117_Single_t117_Single_t117_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t117_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Object_t_Object_t_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Object_t_Object_t_Single_t117_ILayoutElementU26_t3518 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, Object_t ** p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), (Object_t **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_UIVertexU26_t3519_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t272 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertex_t272 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_UIVertex_t272_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t272  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t272 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector3_t12_Color32_t382_Vector2_t171_Vector2_t171_Vector3_t12_Vector4_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Color32_t382  p2, Vector2_t171  p3, Vector2_t171  p4, Vector3_t12  p5, Vector4_t350  p6, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Color32_t382 *)args[1]), *((Vector2_t171 *)args[2]), *((Vector2_t171 *)args[3]), *((Vector3_t12 *)args[4]), *((Vector4_t350 *)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector3_t12_Color32_t382_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Color32_t382  p2, Vector2_t171  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Color32_t382 *)args[1]), *((Vector2_t171 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_UIVertex_t272 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t272  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t272 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Color32_t382_Int32_t392_Int32_t392_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color32_t382  p2, int32_t p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t382 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int64_t711_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_GcAchievementDescriptionData_t612_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t612  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementDescriptionData_t612 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_GcUserProfileData_t611_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t611  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcUserProfileData_t611 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Double_t710_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int64_t711_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_UserProfileU5BU5DU26_t3520_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t480** p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t480**)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_UserProfileU5BU5DU26_t3520_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t480** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t480**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_GcScoreData_t614 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t614  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcScoreData_t614 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_BoundsU26_t3521 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t90 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Bounds_t90 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_BoneWeight_t485_BoneWeight_t485 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t485  p1, BoneWeight_t485  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t485 *)args[0]), *((BoneWeight_t485 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScreenOrientation_t618 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Vector3U26_t3522 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t12 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Matrix4x4_t124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t124  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Matrix4x4_t124 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Matrix4x4U26_t3523 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t124 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Matrix4x4_t124 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_Color_t77 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t77  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((Color_t77 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_Color_t77_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t77  p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((Color_t77 *)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_ColorU26_t3517_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t77 * p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Color_t77 *)args[2], *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t3522 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t12 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t12 *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, IntPtr_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((IntPtr_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_TextureFormat_t621 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t77_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t77  (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	Color_t77  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_Int32_t392_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Rect_t94_Int32_t392_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t94  p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t94 *)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_RectU26_t3524_Int32_t392_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Rect_t94 * p2, int32_t p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Rect_t94 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_IntPtr_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_CullingGroupEvent_t492 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CullingGroupEvent_t492  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CullingGroupEvent_t492 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_CullingGroupEvent_t492_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CullingGroupEvent_t492  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CullingGroupEvent_t492 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Color_t77_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t77  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t77 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t3525_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TouchScreenKeyboard_InternalConstructorHelperArguments_t498 * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (TouchScreenKeyboard_InternalConstructorHelperArguments_t498 *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_SByte_t1443_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Vector3_t12_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector3U26_t3522_Vector3U26_t3522 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12 * p1, Vector3_t12 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t12 *)args[0], (Vector3_t12 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_ColorU26_t3517 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t77 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t77 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_LayerMask_t13 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LayerMask_t13  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LayerMask_t13 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LayerMask_t13_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t13  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LayerMask_t13  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t171_Vector2_t171_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t171  (*Func)(void* obj, Vector2_t171  p1, Vector2_t171  p2, const MethodInfo* method);
	Vector2_t171  ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Vector2_t171 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Vector2_t171_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t171  p1, Vector2_t171  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Vector2_t171 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t171  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t171_Vector2_t171_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t171  (*Func)(void* obj, Vector2_t171  p1, float p2, const MethodInfo* method);
	Vector2_t171  ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Vector2_t171_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t171  p1, Vector2_t171  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Vector2_t171 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t171_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t171  (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	Vector2_t171  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Vector2_t171  p1, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Vector3_t12_Vector3_t12_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, float p3, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Vector3_t12_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Vector3_t12_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Vector3_t12_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Vector3_t12  p1, float p2, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Vector3_t12_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Single_t117_Single_t117_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t77_Color_t77_Color_t77_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t77  (*Func)(void* obj, Color_t77  p1, Color_t77  p2, float p3, const MethodInfo* method);
	Color_t77  ret = ((Func)method->method)(obj, *((Color_t77 *)args[0]), *((Color_t77 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t77_Color_t77_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t77  (*Func)(void* obj, Color_t77  p1, float p2, const MethodInfo* method);
	Color_t77  ret = ((Func)method->method)(obj, *((Color_t77 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t350_Color_t77 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t350  (*Func)(void* obj, Color_t77  p1, const MethodInfo* method);
	Vector4_t350  ret = ((Func)method->method)(obj, *((Color_t77 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Color32_t382_Color_t77 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t382  (*Func)(void* obj, Color_t77  p1, const MethodInfo* method);
	Color32_t382  ret = ((Func)method->method)(obj, *((Color_t77 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t77_Color32_t382 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t77  (*Func)(void* obj, Color32_t382  p1, const MethodInfo* method);
	Color_t77  ret = ((Func)method->method)(obj, *((Color32_t382 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t125  (*Func)(void* obj, const MethodInfo* method);
	Quaternion_t125  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Quaternion_t125_Quaternion_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Quaternion_t125  p1, Quaternion_t125  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Quaternion_t125 *)args[0]), *((Quaternion_t125 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t125_Single_t117_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t125  (*Func)(void* obj, float p1, Vector3_t12  p2, const MethodInfo* method);
	Quaternion_t125  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t125_Single_t117_Vector3U26_t3522 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t125  (*Func)(void* obj, float p1, Vector3_t12 * p2, const MethodInfo* method);
	Quaternion_t125  ret = ((Func)method->method)(obj, *((float*)args[0]), (Vector3_t12 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_SingleU26_t3526_Vector3U26_t3522 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, Vector3_t12 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], (Vector3_t12 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Quaternion_t125_Quaternion_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t125  (*Func)(void* obj, Quaternion_t125  p1, const MethodInfo* method);
	Quaternion_t125  ret = ((Func)method->method)(obj, *((Quaternion_t125 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t125_QuaternionU26_t3527 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t125  (*Func)(void* obj, Quaternion_t125 * p1, const MethodInfo* method);
	Quaternion_t125  ret = ((Func)method->method)(obj, (Quaternion_t125 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Quaternion_t125_Vector3U26_t3522_SingleU26_t3526 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t125  p1, Vector3_t12 * p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t125 *)args[0]), (Vector3_t12 *)args[1], (float*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_QuaternionU26_t3527_Vector3U26_t3522_SingleU26_t3526 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t125 * p1, Vector3_t12 * p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t125 *)args[0], (Vector3_t12 *)args[1], (float*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Quaternion_t125_Quaternion_t125_Quaternion_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t125  (*Func)(void* obj, Quaternion_t125  p1, Quaternion_t125  p2, const MethodInfo* method);
	Quaternion_t125  ret = ((Func)method->method)(obj, *((Quaternion_t125 *)args[0]), *((Quaternion_t125 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Quaternion_t125_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Quaternion_t125  p1, Vector3_t12  p2, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Quaternion_t125 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Quaternion_t125_Quaternion_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t125  p1, Quaternion_t125  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t125 *)args[0]), *((Quaternion_t125 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Rect_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t94  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t94 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Rect_t94_Rect_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t94  p1, Rect_t94  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t94 *)args[0]), *((Rect_t94 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t124_Matrix4x4_t124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t124  (*Func)(void* obj, Matrix4x4_t124  p1, const MethodInfo* method);
	Matrix4x4_t124  ret = ((Func)method->method)(obj, *((Matrix4x4_t124 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t124_Matrix4x4U26_t3523 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t124  (*Func)(void* obj, Matrix4x4_t124 * p1, const MethodInfo* method);
	Matrix4x4_t124  ret = ((Func)method->method)(obj, (Matrix4x4_t124 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Matrix4x4_t124_Matrix4x4U26_t3523 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t124  p1, Matrix4x4_t124 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t124 *)args[0]), (Matrix4x4_t124 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Matrix4x4U26_t3523_Matrix4x4U26_t3523 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t124 * p1, Matrix4x4_t124 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Matrix4x4_t124 *)args[0], (Matrix4x4_t124 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t124  (*Func)(void* obj, const MethodInfo* method);
	Matrix4x4_t124  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t350_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t350  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector4_t350  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Vector4_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector4_t350  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector4_t350 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t124_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t124  (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	Matrix4x4_t124  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Vector3_t12_Quaternion_t125_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Quaternion_t125  p2, Vector3_t12  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Quaternion_t125 *)args[1]), *((Vector3_t12 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t124_Vector3_t12_Quaternion_t125_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t124  (*Func)(void* obj, Vector3_t12  p1, Quaternion_t125  p2, Vector3_t12  p3, const MethodInfo* method);
	Matrix4x4_t124  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Quaternion_t125 *)args[1]), *((Vector3_t12 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t124_Vector3U26_t3522_QuaternionU26_t3527_Vector3U26_t3522 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t124  (*Func)(void* obj, Vector3_t12 * p1, Quaternion_t125 * p2, Vector3_t12 * p3, const MethodInfo* method);
	Matrix4x4_t124  ret = ((Func)method->method)(obj, (Vector3_t12 *)args[0], (Quaternion_t125 *)args[1], (Vector3_t12 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t124_Single_t117_Single_t117_Single_t117_Single_t117_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t124  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, const MethodInfo* method);
	Matrix4x4_t124  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t124_Single_t117_Single_t117_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t124  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Matrix4x4_t124  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t124_Matrix4x4_t124_Matrix4x4_t124 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t124  (*Func)(void* obj, Matrix4x4_t124  p1, Matrix4x4_t124  p2, const MethodInfo* method);
	Matrix4x4_t124  ret = ((Func)method->method)(obj, *((Matrix4x4_t124 *)args[0]), *((Matrix4x4_t124 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t350_Matrix4x4_t124_Vector4_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t350  (*Func)(void* obj, Matrix4x4_t124  p1, Vector4_t350  p2, const MethodInfo* method);
	Vector4_t350  ret = ((Func)method->method)(obj, *((Matrix4x4_t124 *)args[0]), *((Vector4_t350 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Matrix4x4_t124_Matrix4x4_t124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t124  p1, Matrix4x4_t124  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t124 *)args[0]), *((Matrix4x4_t124 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Bounds_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t90  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Bounds_t90 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Bounds_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t90  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t90 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Bounds_t90_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t90  p1, Vector3_t12  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t90 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_BoundsU26_t3521_Vector3U26_t3522 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t90 * p1, Vector3_t12 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Bounds_t90 *)args[0], (Vector3_t12 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Bounds_t90_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t90  p1, Vector3_t12  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Bounds_t90 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_BoundsU26_t3521_Vector3U26_t3522 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t90 * p1, Vector3_t12 * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Bounds_t90 *)args[0], (Vector3_t12 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_RayU26_t3528_BoundsU26_t3521_SingleU26_t3526 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t88 * p1, Bounds_t90 * p2, float* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Ray_t88 *)args[0], (Bounds_t90 *)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Ray_t88 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t88  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t88 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Ray_t88_SingleU26_t3526 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t88  p1, float* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t88 *)args[0]), (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_BoundsU26_t3521_Vector3U26_t3522 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Bounds_t90 * p1, Vector3_t12 * p2, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, (Bounds_t90 *)args[0], (Vector3_t12 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Bounds_t90_Bounds_t90 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t90  p1, Bounds_t90  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t90 *)args[0]), *((Bounds_t90 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Vector4_t350_Vector4_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t350  p1, Vector4_t350  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t350 *)args[0]), *((Vector4_t350 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Vector4_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t350  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t350 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t350  (*Func)(void* obj, const MethodInfo* method);
	Vector4_t350  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t350_Vector4_t350_Vector4_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t350  (*Func)(void* obj, Vector4_t350  p1, Vector4_t350  p2, const MethodInfo* method);
	Vector4_t350  ret = ((Func)method->method)(obj, *((Vector4_t350 *)args[0]), *((Vector4_t350 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t350_Vector4_t350_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t350  (*Func)(void* obj, Vector4_t350  p1, float p2, const MethodInfo* method);
	Vector4_t350  ret = ((Func)method->method)(obj, *((Vector4_t350 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Vector4_t350_Vector4_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t350  p1, Vector4_t350  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t350 *)args[0]), *((Vector4_t350 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, float p1, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Single_t117_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Single_t117_Single_t117_SingleU26_t3526_Single_t117_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, float p5, float p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_RectU26_t3524 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t94 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t94 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector2U26_t3529 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t171 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t171 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_SphericalHarmonicsL2U26_t3530 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SphericalHarmonicsL2_t509 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (SphericalHarmonicsL2_t509 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Color_t77_SphericalHarmonicsL2U26_t3530 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t77  p1, SphericalHarmonicsL2_t509 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t77 *)args[0]), (SphericalHarmonicsL2_t509 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_ColorU26_t3517_SphericalHarmonicsL2U26_t3530 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t77 * p1, SphericalHarmonicsL2_t509 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t77 *)args[0], (SphericalHarmonicsL2_t509 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector3_t12_Color_t77_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Color_t77  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Color_t77 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector3_t12_Color_t77_SphericalHarmonicsL2U26_t3530 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Color_t77  p2, SphericalHarmonicsL2_t509 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Color_t77 *)args[1]), (SphericalHarmonicsL2_t509 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector3U26_t3522_ColorU26_t3517_SphericalHarmonicsL2U26_t3530 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12 * p1, Color_t77 * p2, SphericalHarmonicsL2_t509 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t12 *)args[0], (Color_t77 *)args[1], (SphericalHarmonicsL2_t509 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_SphericalHarmonicsL2_t509_SphericalHarmonicsL2_t509_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t509  (*Func)(void* obj, SphericalHarmonicsL2_t509  p1, float p2, const MethodInfo* method);
	SphericalHarmonicsL2_t509  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t509 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SphericalHarmonicsL2_t509_Single_t117_SphericalHarmonicsL2_t509 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t509  (*Func)(void* obj, float p1, SphericalHarmonicsL2_t509  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t509  ret = ((Func)method->method)(obj, *((float*)args[0]), *((SphericalHarmonicsL2_t509 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SphericalHarmonicsL2_t509_SphericalHarmonicsL2_t509_SphericalHarmonicsL2_t509 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t509  (*Func)(void* obj, SphericalHarmonicsL2_t509  p1, SphericalHarmonicsL2_t509  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t509  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t509 *)args[0]), *((SphericalHarmonicsL2_t509 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_SphericalHarmonicsL2_t509_SphericalHarmonicsL2_t509 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SphericalHarmonicsL2_t509  p1, SphericalHarmonicsL2_t509  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t509 *)args[0]), *((SphericalHarmonicsL2_t509 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Vector4U26_t3531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4_t350 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4_t350 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Vector4_t350_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t350  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector4_t350  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t171_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t171  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector2_t171  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Vector2U26_t3529 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t171 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t171 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_RuntimePlatform_t467 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_CameraClearFlags_t617 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Object_t_Vector3U26_t3522 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Object_t * p1, Vector3_t12 * p2, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t12 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t88_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t88  (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	Ray_t88  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t88_Object_t_Vector3U26_t3522 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t88  (*Func)(void* obj, Object_t * p1, Vector3_t12 * p2, const MethodInfo* method);
	Ray_t88  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t12 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Ray_t88_Single_t117_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t88  p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t88 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_RayU26_t3528_Single_t117_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t88 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t88 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_RayU26_t3528_Single_t117_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t88 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t88 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_RenderBuffer_t616 (const MethodInfo* method, void* obj, void** args)
{
	typedef RenderBuffer_t616  (*Func)(void* obj, const MethodInfo* method);
	RenderBuffer_t616  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_Int32U26_t3516_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_RenderBufferU26_t3532_RenderBufferU26_t3532 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, RenderBuffer_t616 * p2, RenderBuffer_t616 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (RenderBuffer_t616 *)args[1], (RenderBuffer_t616 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Int32_t392_Int32_t392_Int32U26_t3516_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TouchPhase_t520 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Touch_t375_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t375  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Touch_t375  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Quaternion_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t125  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t125 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_QuaternionU26_t3527 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t125 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t125 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_SByte_t1443_SByte_t1443_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Vector3_t12_Vector3_t12_RaycastHitU26_t3533_Single_t117_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, RaycastHit_t89 * p3, float p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), (RaycastHit_t89 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Ray_t88_RaycastHitU26_t3533_Single_t117_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t88  p1, RaycastHit_t89 * p2, float p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t88 *)args[0]), (RaycastHit_t89 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Ray_t88_RaycastHitU26_t3533_Single_t117_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t88  p1, RaycastHit_t89 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t88 *)args[0]), (RaycastHit_t89 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Ray_t88_Single_t117_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t88  p1, float p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t88 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector3_t12_Vector3_t12_Single_t117_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector3U26_t3522_Vector3U26_t3522_Single_t117_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t12 * p1, Vector3_t12 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t12 *)args[0], (Vector3_t12 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t393_Vector3U26_t3522_Vector3U26_t3522_RaycastHitU26_t3533_Single_t117_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t12 * p1, Vector3_t12 * p2, RaycastHit_t89 * p3, float p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector3_t12 *)args[0], (Vector3_t12 *)args[1], (RaycastHit_t89 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Vector2_t171_Vector2_t171_Single_t117_Int32_t392_Single_t117_Single_t117_RaycastHit2DU26_t3534 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t171  p1, Vector2_t171  p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t402 * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Vector2_t171 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t402 *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector2U26_t3529_Vector2U26_t3529_Single_t117_Int32_t392_Single_t117_Single_t117_RaycastHit2DU26_t3534 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t171 * p1, Vector2_t171 * p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t402 * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t171 *)args[0], (Vector2_t171 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t402 *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit2D_t402_Vector2_t171_Vector2_t171_Single_t117_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t402  (*Func)(void* obj, Vector2_t171  p1, Vector2_t171  p2, float p3, int32_t p4, const MethodInfo* method);
	RaycastHit2D_t402  ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Vector2_t171 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastHit2D_t402_Vector2_t171_Vector2_t171_Single_t117_Int32_t392_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t402  (*Func)(void* obj, Vector2_t171  p1, Vector2_t171  p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	RaycastHit2D_t402  ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Vector2_t171 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector2_t171_Vector2_t171_Single_t117_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t171  p1, Vector2_t171  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Vector2_t171 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector2U26_t3529_Vector2U26_t3529_Single_t117_Int32_t392_Single_t117_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t171 * p1, Vector2_t171 * p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector2_t171 *)args[0], (Vector2_t171 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_SByte_t1443_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_SendMessageOptions_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AnimatorStateInfo_t555 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorStateInfo_t555  (*Func)(void* obj, const MethodInfo* method);
	AnimatorStateInfo_t555  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AnimatorClipInfo_t556 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorClipInfo_t556  (*Func)(void* obj, const MethodInfo* method);
	AnimatorClipInfo_t556  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int16_t1444_CharacterInfoU26_t3535_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t566 * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t566 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int16_t1444_CharacterInfoU26_t3535_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t566 * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t566 *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int16_t1444_CharacterInfoU26_t3535 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t566 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t566 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Color_t77_Int32_t392_Single_t117_Single_t117_Int32_t392_SByte_t1443_SByte_t1443_Int32_t392_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_Int32_t392_Vector2_t171_Vector2_t171_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t77  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, Vector2_t171  p16, Vector2_t171  p17, int8_t p18, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t77 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((Vector2_t171 *)args[15]), *((Vector2_t171 *)args[16]), *((int8_t*)args[17]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Color_t77_Int32_t392_Single_t117_Single_t117_Int32_t392_SByte_t1443_SByte_t1443_Int32_t392_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_Int32_t392_Single_t117_Single_t117_Single_t117_Single_t117_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t77  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, float p16, float p17, float p18, float p19, int8_t p20, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t77 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((int8_t*)args[19]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t_ColorU26_t3517_Int32_t392_Single_t117_Single_t117_Int32_t392_SByte_t1443_SByte_t1443_Int32_t392_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_Int32_t392_Single_t117_Single_t117_Single_t117_Single_t117_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Color_t77 * p4, int32_t p5, float p6, float p7, int32_t p8, int8_t p9, int8_t p10, int32_t p11, int32_t p12, int32_t p13, int32_t p14, int8_t p15, int32_t p16, float p17, float p18, float p19, float p20, int8_t p21, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Color_t77 *)args[3], *((int32_t*)args[4]), *((float*)args[5]), *((float*)args[6]), *((int32_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int32_t*)args[13]), *((int8_t*)args[14]), *((int32_t*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((float*)args[19]), *((int8_t*)args[20]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextGenerationSettings_t385_TextGenerationSettings_t385 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t385  (*Func)(void* obj, TextGenerationSettings_t385  p1, const MethodInfo* method);
	TextGenerationSettings_t385  ret = ((Func)method->method)(obj, *((TextGenerationSettings_t385 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Object_t_TextGenerationSettings_t385 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t385  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t385 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_TextGenerationSettings_t385 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t385  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t385 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RenderMode_t570 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_ColorU26_t3517 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t77 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Color_t77 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_RectU26_t3524 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Rect_t94 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Rect_t94 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Object_t_Vector2_t171_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t171  p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t171 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Vector2U26_t3529_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t171 * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t171 *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t171_Vector2_t171_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t171  (*Func)(void* obj, Vector2_t171  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Vector2_t171  ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Vector2_t171_Object_t_Object_t_Vector2U26_t3529 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t171  p1, Object_t * p2, Object_t * p3, Vector2_t171 * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t171 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Vector2_t171 *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector2U26_t3529_Object_t_Object_t_Vector2U26_t3529 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t171 * p1, Object_t * p2, Object_t * p3, Vector2_t171 * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t171 *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Vector2_t171 *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Object_t_Vector2_t171_Object_t_Vector3U26_t3522 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t171  p2, Object_t * p3, Vector3_t12 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t171 *)args[1]), (Object_t *)args[2], (Vector3_t12 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Vector2_t171_Object_t_Vector2U26_t3529 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t171  p2, Object_t * p3, Vector2_t171 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t171 *)args[1]), (Object_t *)args[2], (Vector2_t171 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t88_Object_t_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t88  (*Func)(void* obj, Object_t * p1, Vector2_t171  p2, const MethodInfo* method);
	Ray_t88  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t171 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Ray_t88 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t88  (*Func)(void* obj, const MethodInfo* method);
	Ray_t88  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Ray_t88 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Ray_t88  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Ray_t88 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_EventType_t572 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EventType_t572_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EventModifiers_t573 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyCode_t571 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_DateTime_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t577  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t577 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Rect_t94_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t94  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t94 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Rect_t94_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t94  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t94 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Rect_t94_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t94  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t94 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t94_Int32_t392_Rect_t94_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t94  (*Func)(void* obj, int32_t p1, Rect_t94  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Rect_t94  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t94 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Object_t_Int32_t392_Single_t117_Single_t117_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, float p5, float p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Rect_t94_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t94  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t94 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_RectU26_t3524_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t94 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t94 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Rect_t94_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t94  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t94 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_RectU26_t3524_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t94 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t94 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t94_Int32_t392_Rect_t94_Object_t_Object_t_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t94  (*Func)(void* obj, int32_t p1, Rect_t94  p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	Rect_t94  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t94 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t94_Int32_t392_RectU26_t3524_Object_t_Object_t_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t94  (*Func)(void* obj, int32_t p1, Rect_t94 * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	Rect_t94  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t94 *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Rect_t94_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t94  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Rect_t94  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Rect_t94 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t94  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t94 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_RectU26_t3524 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t94 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t94 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Single_t117_Single_t117_Single_t117_Single_t117_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_SByte_t1443_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_UserState_t636 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Double_t710_SByte_t1443_SByte_t1443_DateTime_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, int8_t p4, DateTime_t577  p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t577 *)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_DateTime_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1443_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int64_t711_Object_t_DateTime_t577_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, DateTime_t577  p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((DateTime_t577 *)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_UserScope_t637 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Range_t631 (const MethodInfo* method, void* obj, void** args)
{
	typedef Range_t631  (*Func)(void* obj, const MethodInfo* method);
	Range_t631  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Range_t631 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t631  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Range_t631 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_TimeScope_t638 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_HitInfo_t633 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t633  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((HitInfo_t633 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_HitInfo_t633_HitInfo_t633 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t633  p1, HitInfo_t633  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t633 *)args[0]), *((HitInfo_t633 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_HitInfo_t633 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t633  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t633 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_StringU26_t3536_StringU26_t3536 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_StreamingContext_t688 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t688  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t688 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Color_t77_Color_t77 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t77  p1, Color_t77  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t77 *)args[0]), *((Color_t77 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_TextGenerationSettings_t385 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TextGenerationSettings_t385  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TextGenerationSettings_t385 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PersistentListenerMode_t653 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_CameraDirection_t741 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VideoBackgroundReflection_t829 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Matrix4x4_t124_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t124  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Matrix4x4_t124 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Matrix4x4_t124 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Matrix4x4_t124  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Matrix4x4_t124 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_SByte_t1443_Single_t117_Int32_t392_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, float p4, int32_t p5, Object_t * p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((float*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Matrix4x4_t124_SingleU26_t3526_SingleU26_t3526 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t124  p1, float* p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Matrix4x4_t124 *)args[0]), (float*)args[1], (float*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Single_t117_Matrix4x4_t124 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Matrix4x4_t124  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Matrix4x4_t124 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Vector4_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Vector4_t350  p1, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Vector4_t350 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Status_t735 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VideoModeData_t742 (const MethodInfo* method, void* obj, void** args)
{
	typedef VideoModeData_t742  (*Func)(void* obj, const MethodInfo* method);
	VideoModeData_t742  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VideoModeData_t742_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef VideoModeData_t742  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	VideoModeData_t742  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_CameraDeviceModeU26_t3537 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t12_Quaternion_t125_Vector3_t12_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector3_t12  p3, Quaternion_t125  p4, Vector3_t12  p5, int32_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector3_t12 *)args[2]), *((Quaternion_t125 *)args[3]), *((Vector3_t12 *)args[4]), *((int32_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t393_Object_t_Vector3_t12_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector3_t12  p2, Vector3_t12  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t12 *)args[1]), *((Vector3_t12 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Vector3_t12_Vector3_t12_Vector3_t12_Quaternion_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector3_t12  p2, Vector3_t12  p3, Vector3_t12  p4, Quaternion_t125  p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t12 *)args[1]), *((Vector3_t12 *)args[2]), *((Vector3_t12 *)args[3]), *((Quaternion_t125 *)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3U26_t3522_Vector3U26_t3522 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t12 * p1, Vector3_t12 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t12 *)args[0], (Vector3_t12 *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t393_IntPtr_t_Object_t_Vector3_t12_Vector3_t12_Vector3_t12_Quaternion_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Vector3_t12  p3, Vector3_t12  p4, Vector3_t12  p5, Quaternion_t125  p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((Vector3_t12 *)args[2]), *((Vector3_t12 *)args[3]), *((Vector3_t12 *)args[4]), *((Quaternion_t125 *)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t124_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t124  (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Matrix4x4_t124  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Vector2_t171_Vector2_t171_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t171  p1, Vector2_t171  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Vector2_t171 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector3_t12_Vector3_t12_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_TargetSearchResult_t884 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TargetSearchResult_t884  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TargetSearchResult_t884 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_ImageTargetType_t773 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_RectangleData_t766 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, RectangleData_t766  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((RectangleData_t766 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t393_Object_t_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FrameQuality_t775 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_VirtualButtonAbstractBehaviourU26_t3538 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, VirtualButtonAbstractBehaviour_t71 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (VirtualButtonAbstractBehaviour_t71 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WordFilterMode_t919 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WordPrefabCreationMode_t851 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sensitivity_t894 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Matrix4x4_t124 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t124  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t124 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WordTemplateMode_t782 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PIXEL_FORMAT_t783 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Single_t117_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t392_Object_t_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_Int32_t392_Object_t_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Vec2I_t831 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vec2I_t831  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vec2I_t831 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_ProfileCollection_t899_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileCollection_t899  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	ProfileCollection_t899  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WorldCenterMode_t909 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_FrameState_t821 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, FrameState_t821  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((FrameState_t821 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_FrameState_t821_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, FrameState_t821  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((FrameState_t821 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_TrackableResultData_t810 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TrackableResultData_t810  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TrackableResultData_t810 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VideoBGCfgData_t830 (const MethodInfo* method, void* obj, void** args)
{
	typedef VideoBGCfgData_t830  (*Func)(void* obj, const MethodInfo* method);
	VideoBGCfgData_t830  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VideoTextureInfo_t728 (const MethodInfo* method, void* obj, void** args)
{
	typedef VideoTextureInfo_t728  (*Func)(void* obj, const MethodInfo* method);
	VideoTextureInfo_t728  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_VideoBGCfgData_t830 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, VideoBGCfgData_t830  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((VideoBGCfgData_t830 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t124_Single_t117_Single_t117_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t124  (*Func)(void* obj, float p1, float p2, int32_t p3, const MethodInfo* method);
	Matrix4x4_t124  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_PoseData_t809 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PoseData_t809  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((PoseData_t809 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_OrientedBoundingBox3D_t768 (const MethodInfo* method, void* obj, void** args)
{
	typedef OrientedBoundingBox3D_t768  (*Func)(void* obj, const MethodInfo* method);
	OrientedBoundingBox3D_t768  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_OrientedBoundingBox3D_t768 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OrientedBoundingBox3D_t768  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OrientedBoundingBox3D_t768 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_UInt16_t1430_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Vec2I_t831 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Vec2I_t831  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((Vec2I_t831 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Vector2_t171  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((Vector2_t171 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector3_t12_Quaternion_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Quaternion_t125  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Quaternion_t125 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_OrientedBoundingBox_t767 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OrientedBoundingBox_t767  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OrientedBoundingBox_t767 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, IntPtr_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int32_t392_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int32_t392_IntPtr_t_Int32_t392_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, IntPtr_t p2, int32_t p3, IntPtr_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), *((int32_t*)args[2]), *((IntPtr_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_IntPtr_t_Int32_t392_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_IntPtr_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Object_t * p3, IntPtr_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((IntPtr_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_IntPtr_t_IntPtr_t_Int32_t392_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, int32_t p3, IntPtr_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), *((int32_t*)args[2]), *((IntPtr_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Int32_t392_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_IntPtr_t_Int32_t392_IntPtr_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, IntPtr_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Single_t117_Single_t117_IntPtr_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, IntPtr_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((IntPtr_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_IntPtr_t_IntPtr_t_Int32_t392_IntPtr_t_IntPtr_t_IntPtr_t_IntPtr_t_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, int32_t p3, IntPtr_t p4, IntPtr_t p5, IntPtr_t p6, IntPtr_t p7, float p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), *((int32_t*)args[2]), *((IntPtr_t*)args[3]), *((IntPtr_t*)args[4]), *((IntPtr_t*)args[5]), *((IntPtr_t*)args[6]), *((float*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_IntPtr_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, IntPtr_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_SmartTerrainInitializationInfo_t760 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SmartTerrainInitializationInfo_t760  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SmartTerrainInitializationInfo_t760 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Object_t_PropAbstractBehaviourU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, PropAbstractBehaviour_t52 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (PropAbstractBehaviour_t52 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_SurfaceAbstractBehaviourU26_t3540 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, SurfaceAbstractBehaviour_t58 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (SurfaceAbstractBehaviour_t58 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_MeshData_t817_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, MeshData_t817  p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((MeshData_t817 *)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t392_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_LinkedList_1U26_t3541 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LinkedList_1_t827 ** p1, const MethodInfo* method);
	((Func)method->method)(obj, (LinkedList_1_t827 **)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_PoseData_t809 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, PoseData_t809  p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((PoseData_t809 *)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_PoseData_t809 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, PoseData_t809  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((PoseData_t809 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_InitState_t881 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UpdateState_t882 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UpdateState_t882_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RectangleData_t766 (const MethodInfo* method, void* obj, void** args)
{
	typedef RectangleData_t766  (*Func)(void* obj, const MethodInfo* method);
	RectangleData_t766  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_RectangleData_t766 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RectangleData_t766  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RectangleData_t766 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_RectangleData_t766_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, RectangleData_t766  p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((RectangleData_t766 *)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Vec2I_t831 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vec2I_t831  (*Func)(void* obj, const MethodInfo* method);
	Vec2I_t831  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_ProfileData_t898 (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileData_t898  (*Func)(void* obj, const MethodInfo* method);
	ProfileData_t898  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ProfileData_t898_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileData_t898  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	ProfileData_t898  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Vector2_t171_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector2_t171  p2, Vector2_t171  p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t171 *)args[1]), *((Vector2_t171 *)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Vector2_t171_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector2_t171  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t171 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_CameraDeviceMode_t740 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t171_Vector2_t171_Rect_t94_SByte_t1443_VideoModeData_t742 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t171  (*Func)(void* obj, Vector2_t171  p1, Rect_t94  p2, int8_t p3, VideoModeData_t742  p4, const MethodInfo* method);
	Vector2_t171  ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Rect_t94 *)args[1]), *((int8_t*)args[2]), *((VideoModeData_t742 *)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_OrientedBoundingBox_t767_OrientedBoundingBox_t767_Rect_t94_SByte_t1443_VideoModeData_t742 (const MethodInfo* method, void* obj, void** args)
{
	typedef OrientedBoundingBox_t767  (*Func)(void* obj, OrientedBoundingBox_t767  p1, Rect_t94  p2, int8_t p3, VideoModeData_t742  p4, const MethodInfo* method);
	OrientedBoundingBox_t767  ret = ((Func)method->method)(obj, *((OrientedBoundingBox_t767 *)args[0]), *((Rect_t94 *)args[1]), *((int8_t*)args[2]), *((VideoModeData_t742 *)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_SingleU26_t3526_SingleU26_t3526_SingleU26_t3526_SingleU26_t3526_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, float* p2, float* p3, float* p4, float* p5, bool* p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (float*)args[1], (float*)args[2], (float*)args[3], (float*)args[4], (bool*)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Vector2U26_t3529_Vector2U26_t3529 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t171 * p1, Vector2_t171 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector2_t171 *)args[0], (Vector2_t171 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Vector2_t171_Vector2_t171_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t171  p1, Vector2_t171  p2, float p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Vector2_t171 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_SByte_t1443_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_SByte_t1443_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_UInt32_t712_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_UInt32_t712_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sign_t1090_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

void* RuntimeInvoker_ConfidenceFactor_t1094 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Byte_t699 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32U26_t3516_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32U26_t3516_ByteU26_t3542_Int32U26_t3516_ByteU5BU5DU26_t3543 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t786** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t786**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_DateTime_t577_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_DSAParameters_t1229 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DSAParameters_t1229  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((DSAParameters_t1229 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_RSAParameters_t1201_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1201  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	RSAParameters_t1201  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_RSAParameters_t1201 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t1201  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RSAParameters_t1201 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_DSAParameters_t1229_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1229  (*Func)(void* obj, bool* p1, const MethodInfo* method);
	DSAParameters_t1229  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1443_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_DateTime_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t577  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Byte_t699 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Byte_t699_Byte_t699 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_AlertLevel_t1150 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AlertDescription_t1151 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Byte_t699 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Int16_t1444_Object_t_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_Int16_t1444_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_CipherAlgorithmType_t1153 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HashAlgorithmType_t1172 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExchangeAlgorithmType_t1170 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CipherMode_t1081 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_ByteU5BU5DU26_t3543_ByteU5BU5DU26_t3543 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ByteU5BU5D_t786** p2, ByteU5BU5D_t786** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ByteU5BU5D_t786**)args[1], (ByteU5BU5D_t786**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t699_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t1444_Object_t_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_Int16_t1444_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_SecurityProtocolType_t1187 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityCompressionType_t1186 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeType_t1204 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeState_t1171 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t1187_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t699_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t699_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Byte_t699_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t699_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_SByte_t1443_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t711_Int64_t711_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Byte_t699_Byte_t699_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_RSAParameters_t1201 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1201  (*Func)(void* obj, const MethodInfo* method);
	RSAParameters_t1201  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Byte_t699 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Byte_t699_Byte_t699 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, uint8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Byte_t699_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_ContentType_t1165 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t3544 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DictionaryNode_t1277 ** p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (DictionaryNode_t1277 **)args[1], method);
	return ret;
}

void* RuntimeInvoker_DictionaryEntry_t1423 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1423  (*Func)(void* obj, const MethodInfo* method);
	DictionaryEntry_t1423  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EditorBrowsableState_t1288 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1444_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_IPAddressU26_t3545 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPAddress_t1305 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPAddress_t1305 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AddressFamily_t1293 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_IPv6AddressU26_t3546 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPv6Address_t1307 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPv6Address_t1307 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1430_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t1308 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_AsnDecodeStatus_t1348_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t392_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_X509ChainStatusFlags_t1336_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t1336_Object_t_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t1336_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t1336 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32U26_t3516_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_X509RevocationFlag_t1343 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509RevocationMode_t1344 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509VerificationFlags_t1347 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t1341 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t1341_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Byte_t699_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t699_Int16_t1444_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t392_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_RegexOptions_t1360 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Category_t1367_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_UInt16_t1430_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int16_t1444_SByte_t1443_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_UInt16_t1430_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int16_t1444_Int16_t1444_SByte_t1443_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int16_t1444_Object_t_SByte_t1443_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_UInt16_t1430 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_SByte_t1443_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_UInt16_t1430_UInt16_t1430_UInt16_t1430 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_OpFlags_t1362_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_UInt16_t1430_UInt16_t1430 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_Int32U26_t3516_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_Int32U26_t3516_Int32U26_t3516_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32U26_t3516_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_UInt16_t1430_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_Int32_t392_SByte_t1443_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32U26_t3516_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_SByte_t1443_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t1382 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1382  (*Func)(void* obj, const MethodInfo* method);
	Interval_t1382  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Interval_t1382 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t1382  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Interval_t1382 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Interval_t1382 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t1382  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Interval_t1382 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t1382_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1382  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Interval_t1382  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Double_t710_Interval_t1382 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t1382  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Interval_t1382 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Interval_t1382_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Interval_t1382  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Interval_t1382 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Double_t710_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32U26_t3516_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32U26_t3516_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_RegexOptionsU26_t3547 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_RegexOptionsU26_t3547_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Int32U26_t3516_Int32U26_t3516_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Category_t1367 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int16_t1444_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t424_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32U26_t3516_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32U26_t3516_Int32U26_t3516_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_UInt16_t1430_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int16_t1444_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_UInt16_t1430 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Position_t1363 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriHostNameType_t1415_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_StringU26_t3536 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1443_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Char_t424_Object_t_Int32U26_t3516_CharU26_t3548 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t* p2, uint16_t* p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_UriFormatExceptionU26_t3549 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UriFormatException_t1414 ** p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (UriFormatException_t1414 **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_ObjectU5BU5DU26_t3550 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, ObjectU5BU5D_t105** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ObjectU5BU5D_t105**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_ObjectU5BU5DU26_t3550 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t105** p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t105**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t699_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1445_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1445  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Decimal_t1445  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1444_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t711_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1443_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t712_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_SByte_t1443_Object_t_Int32_t392_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Exception_t107 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Exception_t107 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_SByte_t1443_Int32U26_t3516_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t* p3, Exception_t107 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t107 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_SByte_t1443_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t107 ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t107 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32U26_t3516_Object_t_SByte_t1443_SByte_t1443_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int8_t p3, int8_t p4, Exception_t107 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t107 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32U26_t3516_Object_t_Object_t_BooleanU26_t3515_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, bool* p5, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32U26_t3516_Object_t_Object_t_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Int32U26_t3516_Object_t_Int32U26_t3516_SByte_t1443_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, int8_t p4, Exception_t107 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t107 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32U26_t3516_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int16_t1444_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_SByte_t1443_Int32U26_t3516_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, Exception_t107 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t107 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeCode_t2067 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_SByte_t1443_Int64U26_t3552_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int64_t* p3, Exception_t107 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t107 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t711_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_SByte_t1443_Int64U26_t3552_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int64_t* p5, Exception_t107 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t107 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t711_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int64U26_t3552 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_Int64U26_t3552 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int64_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int64_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_SByte_t1443_UInt32U26_t3553_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint32_t* p3, Exception_t107 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t107 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_SByte_t1443_UInt32U26_t3553_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint32_t* p5, Exception_t107 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t107 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t712_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t712_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_UInt32U26_t3553 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_UInt32U26_t3553 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_SByte_t1443_UInt64U26_t3554_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint64_t* p5, Exception_t107 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t107 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_UInt64U26_t3554 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t699_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t699_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_ByteU26_t3542 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_ByteU26_t3542 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint8_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint8_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_SByte_t1443_SByteU26_t3555_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t* p3, Exception_t107 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t107 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1443_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1443_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_SByteU26_t3555 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_SByte_t1443_Int16U26_t3556_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int16_t* p3, Exception_t107 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t107 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1444_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1444_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int16U26_t3556 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1430_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1430_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_UInt16U26_t3557 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_UInt16U26_t3557 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint16_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint16_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_ByteU2AU26_t3558_ByteU2AU26_t3558_DoubleU2AU26_t3559_UInt16U2AU26_t3560_UInt16U2AU26_t3560_UInt16U2AU26_t3560_UInt16U2AU26_t3560 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

void* RuntimeInvoker_UnicodeCategory_t1593_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t424_Int16_t1444_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int16_t1444_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Char_t424_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Object_t_SByte_t1443_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t_Int32_t392_Int32_t392_SByte_t1443_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int16_t1444_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t392_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1444_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32U26_t3516_Int32U26_t3516_Int32U26_t3516_BooleanU26_t3515_StringU26_t3536 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1444_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t117_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t710_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t710_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_SByte_t1443_DoubleU26_t3561_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, double* p5, Exception_t107 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t107 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_DoubleU26_t3561 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, double* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (double*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t1445_Decimal_t1445_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1445  (*Func)(void* obj, Decimal_t1445  p1, Decimal_t1445  p2, const MethodInfo* method);
	Decimal_t1445  ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), *((Decimal_t1445 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t711_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Decimal_t1445_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t1445  p1, Decimal_t1445  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), *((Decimal_t1445 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1445_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1445  (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	Decimal_t1445  ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Decimal_t1445_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1445  p1, Decimal_t1445  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), *((Decimal_t1445 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1445_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1445  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Decimal_t1445  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t_Int32U26_t3516_BooleanU26_t3515_BooleanU26_t3515_Int32U26_t3516_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t1445_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1445  (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Decimal_t1445  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_DecimalU26_t3562_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Decimal_t1445 * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Decimal_t1445 *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_DecimalU26_t3562_UInt64U26_t3554 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1445 * p1, uint64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1445 *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_DecimalU26_t3562_Int64U26_t3552 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1445 * p1, int64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1445 *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_DecimalU26_t3562_DecimalU26_t3562 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1445 * p1, Decimal_t1445 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1445 *)args[0], (Decimal_t1445 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_DecimalU26_t3562_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1445 * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1445 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_DecimalU26_t3562_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1445 * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1445 *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t710_DecimalU26_t3562 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t1445 * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Decimal_t1445 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_DecimalU26_t3562_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t1445 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Decimal_t1445 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_DecimalU26_t3562_DecimalU26_t3562_DecimalU26_t3562 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1445 * p1, Decimal_t1445 * p2, Decimal_t1445 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1445 *)args[0], (Decimal_t1445 *)args[1], (Decimal_t1445 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t699_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1443_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1444_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1430_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t712_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1445_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1445  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Decimal_t1445  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1445_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1445  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Decimal_t1445  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1445_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1445  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Decimal_t1445  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1445_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1445  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Decimal_t1445  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1445_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1445  (*Func)(void* obj, float p1, const MethodInfo* method);
	Decimal_t1445  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1445_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1445  (*Func)(void* obj, double p1, const MethodInfo* method);
	Decimal_t1445  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t710_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_UInt32_t712 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t712_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t3563 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MulticastDelegate_t259 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MulticastDelegate_t259 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t392_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_Object_t_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int64_t711_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int64_t711_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int64_t711_Int64_t711_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int64_t711_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int64_t711_Int64_t711_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, int64_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int64_t711_Object_t_Int64_t711_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, int64_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t392_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_TypeAttributes_t1717 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MemberTypes_t1697 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeTypeHandle_t1450 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1450  (*Func)(void* obj, const MethodInfo* method);
	RuntimeTypeHandle_t1450  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_TypeCode_t2067_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RuntimeTypeHandle_t1450 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeTypeHandle_t1450  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeTypeHandle_t1450 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_RuntimeTypeHandle_t1450_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1450  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RuntimeTypeHandle_t1450  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t392_Object_t_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t392_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_RuntimeFieldHandle_t1452 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, RuntimeFieldHandle_t1452  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((RuntimeFieldHandle_t1452 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_ContractionU5BU5DU26_t3564_Level2MapU5BU5DU26_t3565 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, ContractionU5BU5D_t1497** p3, Level2MapU5BU5D_t1498** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ContractionU5BU5D_t1497**)args[2], (Level2MapU5BU5D_t1498**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_CodePointIndexerU26_t3566_ByteU2AU26_t3558_ByteU2AU26_t3558_CodePointIndexerU26_t3566_ByteU2AU26_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, CodePointIndexer_t1481 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t1481 ** p5, uint8_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (CodePointIndexer_t1481 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t1481 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Byte_t699_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t699_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExtenderType_t1494_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392_BooleanU26_t3515_BooleanU26_t3515_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392_BooleanU26_t3515_BooleanU26_t3515_SByte_t1443_SByte_t1443_ContextU26_t3567 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t1491 * p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t1491 *)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Int32_t392_Int32_t392_SByte_t1443_ContextU26_t3567 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, Context_t1491 * p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t1491 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Object_t_Int32_t392_Int32_t392_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, bool* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int16_t1444_Int32_t392_SByte_t1443_ContextU26_t3567 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int16_t p5, int32_t p6, int8_t p7, Context_t1491 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1491 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Object_t_Int32_t392_Int32_t392_Object_t_ContextU26_t3567 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Context_t1491 * p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Context_t1491 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392_Object_t_Int32_t392_SByte_t1443_ContextU26_t3567 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, Context_t1491 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1491 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32U26_t3516_Int32_t392_Int32_t392_Object_t_SByte_t1443_ContextU26_t3567 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, Context_t1491 * p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), (Context_t1491 *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32U26_t3516_Int32_t392_Int32_t392_Object_t_SByte_t1443_Int32_t392_ContractionU26_t3568_ContextU26_t3567 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, int32_t p7, Contraction_t1484 ** p8, Context_t1491 * p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t1484 **)args[7], (Context_t1491 *)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32U26_t3516_Int32_t392_Int32_t392_Int32_t392_Object_t_SByte_t1443_ContextU26_t3567 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, Context_t1491 * p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), (Context_t1491 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32U26_t3516_Int32_t392_Int32_t392_Int32_t392_Object_t_SByte_t1443_Int32_t392_ContractionU26_t3568_ContextU26_t3567 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, int32_t p8, Contraction_t1484 ** p9, Context_t1491 * p10, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t1484 **)args[8], (Context_t1491 *)args[9], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_ByteU5BU5DU26_t3543_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t786** p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t786**)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ConfidenceFactor_t1503 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sign_t1505_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DSAParameters_t1229_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1229  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DSAParameters_t1229  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_DSAParameters_t1229 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t1229  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DSAParameters_t1229 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int16_t1444_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t710_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t1444_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Single_t117_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Single_t117_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Single_t117_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_SByte_t1443_MethodBaseU26_t3569_Int32U26_t3516_Int32U26_t3516_StringU26_t3536_Int32U26_t3516_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t714 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t714 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_DateTime_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t577  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t2016_DateTime_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t577  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int32U26_t3516_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t2016_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32U26_t3516_Int32U26_t3516_Int32U26_t3516_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_DateTime_t577_DateTime_t577_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t577  p1, DateTime_t577  p2, TimeSpan_t975  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t577 *)args[0]), *((DateTime_t577 *)args[1]), *((TimeSpan_t975 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t975  (*Func)(void* obj, const MethodInfo* method);
	TimeSpan_t975  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1445  (*Func)(void* obj, const MethodInfo* method);
	Decimal_t1445  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1430 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_SByte_t1443_Int32_t392_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Object_t_MonoIOErrorU26_t3570 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t392_Int32_t392_MonoIOErrorU26_t3570 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_MonoIOErrorU26_t3570 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_FileAttributes_t1603_Object_t_MonoIOErrorU26_t3570 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MonoFileType_t1612_IntPtr_t_MonoIOErrorU26_t3570 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_MonoIOStatU26_t3571_MonoIOErrorU26_t3570 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MonoIOStat_t1611 * p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (MonoIOStat_t1611 *)args[1], (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_MonoIOErrorU26_t3570 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_IntPtr_t_MonoIOErrorU26_t3570 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Int32_t392_Int32_t392_MonoIOErrorU26_t3570 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t711_IntPtr_t_Int64_t711_Int32_t392_MonoIOErrorU26_t3570 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t711_IntPtr_t_MonoIOErrorU26_t3570 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_IntPtr_t_Int64_t711_MonoIOErrorU26_t3570 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_CallingConventions_t1687 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeMethodHandle_t2059 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t2059  (*Func)(void* obj, const MethodInfo* method);
	RuntimeMethodHandle_t2059  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t1698 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodToken_t1654 (const MethodInfo* method, void* obj, void** args)
{
	typedef MethodToken_t1654  (*Func)(void* obj, const MethodInfo* method);
	MethodToken_t1654  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FieldAttributes_t1695 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeFieldHandle_t1452 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t1452  (*Func)(void* obj, const MethodInfo* method);
	RuntimeFieldHandle_t1452  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_OpCode_t1658 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1658  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1658 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_OpCode_t1658_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1658  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1658 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_StackBehaviour_t1662 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t392_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t392_Int32_t392_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_SByte_t1443_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t3516_ModuleU26_t3572 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t* p2, Module_t1651 ** p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (Module_t1651 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_AssemblyNameFlags_t1681 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t392_Object_t_ObjectU5BU5DU26_t3550_Object_t_Object_t_Object_t_ObjectU26_t3573 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, ObjectU5BU5D_t105** p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t ** p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (ObjectU5BU5D_t105**)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t **)args[6], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_ObjectU5BU5DU26_t3550_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t105** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t105**)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t392_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_ObjectU5BU5DU26_t3550_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t105** p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t105**)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t392_Object_t_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_EventAttributes_t1693 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t1452 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeFieldHandle_t1452  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeFieldHandle_t1452 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t688 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t688  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t688 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t2059 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeMethodHandle_t2059  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeMethodHandle_t2059 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_MonoEventInfoU26_t3574 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEventInfo_t1702 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEventInfo_t1702 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoEventInfo_t1702_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoEventInfo_t1702  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	MonoEventInfo_t1702  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_MonoMethodInfoU26_t3575 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MonoMethodInfo_t1705 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (MonoMethodInfo_t1705 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoMethodInfo_t1705_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoMethodInfo_t1705  (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	MonoMethodInfo_t1705  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t1698_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CallingConventions_t1687_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Exception_t107 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Exception_t107 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_MonoPropertyInfoU26_t3576_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoPropertyInfo_t1706 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoPropertyInfo_t1706 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_PropertyAttributes_t1713 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_ParameterAttributes_t1709 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int64_t711_ResourceInfoU26_t3577 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, ResourceInfo_t1721 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (ResourceInfo_t1721 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int64_t711_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_GCHandle_t965_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t965  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	GCHandle_t965  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Int32_t392_IntPtr_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, IntPtr_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((IntPtr_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_Object_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Byte_t699_IntPtr_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_IntPtr_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t3536 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, String_t** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (String_t**)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t3536 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, String_t** p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (String_t**)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_Object_t_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t975  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TimeSpan_t975 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_SByte_t1443_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t688_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t688  p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t688 *)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t688_ISurrogateSelectorU26_t3578 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t688  p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t688 *)args[1]), (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t975_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t975  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TimeSpan_t975  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_StringU26_t3536 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (String_t**)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t3573 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t393_Object_t_StringU26_t3536_StringU26_t3536 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WellKnownObjectMode_t1854 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContext_t688 (const MethodInfo* method, void* obj, void** args)
{
	typedef StreamingContext_t688  (*Func)(void* obj, const MethodInfo* method);
	StreamingContext_t688  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeFilterLevel_t1870 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t699_Object_t_SByte_t1443_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t699_Object_t_SByte_t1443_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_SByte_t1443_ObjectU26_t3573_HeaderU5BU5DU26_t3579 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t ** p3, HeaderU5BU5D_t2075** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t **)args[2], (HeaderU5BU5D_t2075**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Byte_t699_Object_t_SByte_t1443_ObjectU26_t3573_HeaderU5BU5DU26_t3579 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t ** p4, HeaderU5BU5D_t2075** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t **)args[3], (HeaderU5BU5D_t2075**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Byte_t699_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Byte_t699_Object_t_Int64U26_t3552_ObjectU26_t3573_SerializationInfoU26_t3580 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int64_t* p3, Object_t ** p4, SerializationInfo_t687 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], (SerializationInfo_t687 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_SByte_t1443_SByte_t1443_Int64U26_t3552_ObjectU26_t3573_SerializationInfoU26_t3580 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int64_t* p4, Object_t ** p5, SerializationInfo_t687 ** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (Object_t **)args[4], (SerializationInfo_t687 **)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int64U26_t3552_ObjectU26_t3573_SerializationInfoU26_t3580 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, SerializationInfo_t687 ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], (SerializationInfo_t687 **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Int64_t711_ObjectU26_t3573_SerializationInfoU26_t3580 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t ** p4, SerializationInfo_t687 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t **)args[3], (SerializationInfo_t687 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int64_t711_Object_t_Object_t_Int64_t711_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int64U26_t3552_ObjectU26_t3573 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Int64U26_t3552_ObjectU26_t3573 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, Object_t ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Int64_t711_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int64_t711_Int64_t711_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int64_t711_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Byte_t699 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Int64_t711_Int32_t392_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int64_t711_Object_t_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int64_t711_Object_t_Int64_t711_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_SByte_t1443_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_StreamingContext_t688 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t688  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t688 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_StreamingContext_t688 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t688  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t688 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_StreamingContext_t688 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t688  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((StreamingContext_t688 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t688_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t688  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t688 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_DateTime_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DateTime_t577  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((DateTime_t577 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_SerializationEntry_t1887 (const MethodInfo* method, void* obj, void** args)
{
	typedef SerializationEntry_t1887  (*Func)(void* obj, const MethodInfo* method);
	SerializationEntry_t1887  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContextStates_t1890 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CspProviderFlags_t1894 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t712_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int64_t711_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_UInt32_t712_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_UInt32U26_t3553_Int32_t392_UInt32U26_t3553_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int64_t711_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UInt64_t1442_Int64_t711_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442_Int64_t711_Int64_t711_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PaddingMode_t1085 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_StringBuilderU26_t3581_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t390 ** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (StringBuilder_t390 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_EncoderFallbackBufferU26_t3582_CharU5BU5DU26_t3583 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, EncoderFallbackBuffer_t1966 ** p6, CharU5BU5D_t270** p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t1966 **)args[5], (CharU5BU5D_t270**)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_DecoderFallbackBufferU26_t3584 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, DecoderFallbackBuffer_t1957 ** p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t1957 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int16_t1444_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int16_t1444_Int16_t1444_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int16_t1444_Int16_t1444_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t392_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_SByte_t1443_Int32_t392_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_SByte_t1443_Int32U26_t3516_BooleanU26_t3515_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_CharU26_t3548_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_CharU26_t3548_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, uint16_t* p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_CharU26_t3548_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint16_t* p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t_Int32_t392_CharU26_t3548_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint16_t* p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Object_t_DecoderFallbackBufferU26_t3584_ByteU5BU5DU26_t3543_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, DecoderFallbackBuffer_t1957 ** p7, ByteU5BU5D_t786** p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (DecoderFallbackBuffer_t1957 **)args[6], (ByteU5BU5D_t786**)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392_Object_t_DecoderFallbackBufferU26_t3584_ByteU5BU5DU26_t3543_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, DecoderFallbackBuffer_t1957 ** p6, ByteU5BU5D_t786** p7, int8_t p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (DecoderFallbackBuffer_t1957 **)args[5], (ByteU5BU5D_t786**)args[6], *((int8_t*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_DecoderFallbackBufferU26_t3584_ByteU5BU5DU26_t3543_Object_t_Int64_t711_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1957 ** p2, ByteU5BU5D_t786** p3, Object_t * p4, int64_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1957 **)args[1], (ByteU5BU5D_t786**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Object_t_DecoderFallbackBufferU26_t3584_ByteU5BU5DU26_t3543_Object_t_Int64_t711_Int32_t392_Object_t_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1957 ** p2, ByteU5BU5D_t786** p3, Object_t * p4, int64_t p5, int32_t p6, Object_t * p7, int32_t* p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1957 **)args[1], (ByteU5BU5D_t786**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], (int32_t*)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_UInt32U26_t3553_UInt32U26_t3553_Object_t_DecoderFallbackBufferU26_t3584_ByteU5BU5DU26_t3543_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint32_t* p6, uint32_t* p7, Object_t * p8, DecoderFallbackBuffer_t1957 ** p9, ByteU5BU5D_t786** p10, int8_t p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (Object_t *)args[7], (DecoderFallbackBuffer_t1957 **)args[8], (ByteU5BU5D_t786**)args[9], *((int8_t*)args[10]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t_Int32_t392_UInt32U26_t3553_UInt32U26_t3553_Object_t_DecoderFallbackBufferU26_t3584_ByteU5BU5DU26_t3543_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint32_t* p5, uint32_t* p6, Object_t * p7, DecoderFallbackBuffer_t1957 ** p8, ByteU5BU5D_t786** p9, int8_t p10, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (Object_t *)args[6], (DecoderFallbackBuffer_t1957 **)args[7], (ByteU5BU5D_t786**)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_SByte_t1443_Object_t_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, Object_t * p2, bool* p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (bool*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_SByte_t1443_SByte_t1443_Object_t_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, bool* p4, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (bool*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_TimeSpan_t975_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t975  p1, TimeSpan_t975  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t975 *)args[0]), *((TimeSpan_t975 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int64_t711_Int64_t711_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_IntPtr_t_Int32_t392_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t711_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int64_t711_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1430_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Byte_t699_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t699_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t699_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t699_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t424_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t424_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t424_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t424_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t577_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t577_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t577_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t577_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t577_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, float p1, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t577_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t710_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t710_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t710_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t710_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t710_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t710_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1444_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1444_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1444_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1444_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1444_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t711_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t711_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t711_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t711_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1443_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1443_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1443_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1443_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1443_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t117_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1430_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1430_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1430_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1430_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1430_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t712_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t712_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t712_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t712_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t712_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1442_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_SByte_t1443_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t975  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((TimeSpan_t975 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int64_t711_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DayOfWeek_t2016 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTimeKind_t2013 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t577_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, TimeSpan_t975  p1, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, *((TimeSpan_t975 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t577_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, double p1, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_DateTime_t577_DateTime_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t577  p1, DateTime_t577  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), *((DateTime_t577 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t577_DateTime_t577_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, DateTime_t577  p1, int32_t p2, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t577_Object_t_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Int32_t392_DateTimeU26_t3585_DateTimeOffsetU26_t3586_SByte_t1443_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, DateTime_t577 * p4, DateTimeOffset_t2014 * p5, int8_t p6, Exception_t107 ** p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (DateTime_t577 *)args[3], (DateTimeOffset_t2014 *)args[4], *((int8_t*)args[5]), (Exception_t107 **)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1443_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Exception_t107 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Exception_t107 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t_Object_t_SByte_t1443_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Int32_t392_Object_t_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Int32_t392_Object_t_SByte_t1443_Int32U26_t3516_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, int32_t* p6, int32_t* p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_SByte_t1443_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t_SByte_t1443_DateTimeU26_t3585_DateTimeOffsetU26_t3586_Object_t_Int32_t392_SByte_t1443_BooleanU26_t3515_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, DateTime_t577 * p5, DateTimeOffset_t2014 * p6, Object_t * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (DateTime_t577 *)args[4], (DateTimeOffset_t2014 *)args[5], (Object_t *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t577_Object_t_Object_t_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t_Int32_t392_DateTimeU26_t3585_SByte_t1443_BooleanU26_t3515_SByte_t1443_ExceptionU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, DateTime_t577 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t107 ** p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (DateTime_t577 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t107 **)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t577_DateTime_t577_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, DateTime_t577  p1, TimeSpan_t975  p2, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), *((TimeSpan_t975 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_DateTime_t577_DateTime_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t577  p1, DateTime_t577  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), *((DateTime_t577 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t975_DateTime_t577_DateTime_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t975  (*Func)(void* obj, DateTime_t577  p1, DateTime_t577  p2, const MethodInfo* method);
	TimeSpan_t975  ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), *((DateTime_t577 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_DateTime_t577_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t577  p1, TimeSpan_t975  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t577 *)args[0]), *((TimeSpan_t975 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int64_t711_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t975  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((TimeSpan_t975 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_DateTimeOffset_t2014 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t2014  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t2014 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_DateTimeOffset_t2014 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t2014  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t2014 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t1444_Object_t_BooleanU26_t3515_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1444_Object_t_BooleanU26_t3515_BooleanU26_t3515_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t577_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t577  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t577_Nullable_1_t2120_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t577  p1, Nullable_1_t2120  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), *((Nullable_1_t2120 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_MonoEnumInfo_t2027 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t2027  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((MonoEnumInfo_t2027 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_MonoEnumInfoU26_t3587 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEnumInfo_t2027 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEnumInfo_t2027 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Int16_t1444_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int64_t711_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PlatformID_t2056 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int16_t1444_Int16_t1444_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Guid_t2036 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t2036  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t2036 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Guid_t2036 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t2036  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t2036 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Guid_t2036 (const MethodInfo* method, void* obj, void** args)
{
	typedef Guid_t2036  (*Func)(void* obj, const MethodInfo* method);
	Guid_t2036  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1443_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Double_t710_Double_t710_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeAttributes_t1717_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1443_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_UInt64U2AU26_t3588_Int32U2AU26_t3589_CharU2AU26_t3590_CharU2AU26_t3590_Int64U2AU26_t3591_Int32U2AU26_t3589 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Double_t710_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Decimal_t1445  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t1445 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1443_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t1444_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t711_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Single_t117_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Double_t710_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Decimal_t1445_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Decimal_t1445  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t1445 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Single_t117_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Double_t710_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_BooleanU26_t3515_SByte_t1443_Int32U26_t3516_Int32U26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392_Object_t_SByte_t1443_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t711_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t975_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t975  (*Func)(void* obj, TimeSpan_t975  p1, const MethodInfo* method);
	TimeSpan_t975  ret = ((Func)method->method)(obj, *((TimeSpan_t975 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_TimeSpan_t975_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t975  p1, TimeSpan_t975  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t975 *)args[0]), *((TimeSpan_t975 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t975  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t975 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t975  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t975 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t975_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t975  (*Func)(void* obj, double p1, const MethodInfo* method);
	TimeSpan_t975  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t975_Double_t710_Int64_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t975  (*Func)(void* obj, double p1, int64_t p2, const MethodInfo* method);
	TimeSpan_t975  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t975_TimeSpan_t975_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t975  (*Func)(void* obj, TimeSpan_t975  p1, TimeSpan_t975  p2, const MethodInfo* method);
	TimeSpan_t975  ret = ((Func)method->method)(obj, *((TimeSpan_t975 *)args[0]), *((TimeSpan_t975 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t975_DateTime_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t975  (*Func)(void* obj, DateTime_t577  p1, const MethodInfo* method);
	TimeSpan_t975  ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_DateTime_t577_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t577  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t577_DateTime_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t577  (*Func)(void* obj, DateTime_t577  p1, const MethodInfo* method);
	DateTime_t577  ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t975_DateTime_t577_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t975  (*Func)(void* obj, DateTime_t577  p1, TimeSpan_t975  p2, const MethodInfo* method);
	TimeSpan_t975  ret = ((Func)method->method)(obj, *((DateTime_t577 *)args[0]), *((TimeSpan_t975 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_Int64U5BU5DU26_t3592_StringU5BU5DU26_t3593 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t2103** p2, StringU5BU5D_t87** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t2103**)args[1], (StringU5BU5D_t87**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_ObjectU26_t3573_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_ObjectU26_t3573_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2985 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2985  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2985  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3001 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3001  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3001  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2222 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2222  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2222  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_ObjectU26_t3573 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_ObjectU5BU5DU26_t3550_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t105** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t105**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_ObjectU5BU5DU26_t3550_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t105** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t105**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_KeyValuePair_2_t2365 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2365  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2365 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2365 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2365  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2365 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2365_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2365  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2365  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_ObjectU26_t3573 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2369 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2369  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2369  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1423_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1423  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1423  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2365 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2365  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2365  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2368 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2368  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2368  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2372  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2372  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int32_t392_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2174 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2174  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2174  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2288 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2288  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2288  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2285 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2285  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2285  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2280 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2280  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2280  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_FloatTween_t196 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, FloatTween_t196  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((FloatTween_t196 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_ColorTween_t193 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorTween_t193  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorTween_t193 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_TypeU26_t3594_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_BooleanU26_t3515_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_FillMethodU26_t3595_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_SingleU26_t3526_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_ContentTypeU26_t3596_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_LineTypeU26_t3597_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_InputTypeU26_t3598_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_TouchScreenKeyboardTypeU26_t3599_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_CharacterValidationU26_t3600_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_CharU26_t3548_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t* p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint16_t*)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_DirectionU26_t3601_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_NavigationU26_t3602_Navigation_t280 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Navigation_t280 * p1, Navigation_t280  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Navigation_t280 *)args[0], *((Navigation_t280 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_TransitionU26_t3603_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_ColorBlockU26_t3604_ColorBlock_t207 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ColorBlock_t207 * p1, ColorBlock_t207  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (ColorBlock_t207 *)args[0], *((ColorBlock_t207 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_SpriteStateU26_t3605_SpriteState_t299 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SpriteState_t299 * p1, SpriteState_t299  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (SpriteState_t299 *)args[0], *((SpriteState_t299 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_DirectionU26_t3606_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_AspectModeU26_t3607_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_FitModeU26_t3608_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_CornerU26_t3609_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_AxisU26_t3610_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector2U26_t3529_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t171 * p1, Vector2_t171  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t171 *)args[0], *((Vector2_t171 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_ConstraintU26_t3611_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32U26_t3516_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_SingleU26_t3526_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_BooleanU26_t3515_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_TextAnchorU26_t3612_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Object_t_Object_t_Single_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Enumerator_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t977  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t977  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2716 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2716  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2716  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Double_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t12_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector3_t12  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector3_t12 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RaycastResult_t169_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t169  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastResult_t169  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_RaycastResult_t169 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t169  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t169 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_RaycastResult_t169 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t169  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t169 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_RaycastResult_t169 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastResult_t169  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastResult_t169 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_RaycastResultU5BU5DU26_t3613_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t2238** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t2238**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_RaycastResultU5BU5DU26_t3613_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t2238** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t2238**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_RaycastResult_t169_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, RaycastResult_t169  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((RaycastResult_t169 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_RaycastResult_t169_RaycastResult_t169_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t169  p1, RaycastResult_t169  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t169 *)args[0]), *((RaycastResult_t169 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2280_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2280  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2280  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_KeyValuePair_2_t2280 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2280  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2280 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2280 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2280  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2280 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_KeyValuePair_2_t2280 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2280  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2280 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2280 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2280  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2280 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Link_t1547_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1547  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t1547  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Link_t1547 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t1547  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t1547 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Link_t1547 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t1547  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t1547 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Link_t1547 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t1547  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t1547 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Link_t1547 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t1547  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t1547 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DictionaryEntry_t1423_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1423  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DictionaryEntry_t1423  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_DictionaryEntry_t1423 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t1423  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DictionaryEntry_t1423 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_DictionaryEntry_t1423 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t1423  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DictionaryEntry_t1423 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_DictionaryEntry_t1423 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t1423  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DictionaryEntry_t1423 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_DictionaryEntry_t1423 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t1423  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t1423 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit2D_t402_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t402  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit2D_t402  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_RaycastHit2D_t402 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit2D_t402  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit2D_t402 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_RaycastHit2D_t402 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit2D_t402  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit2D_t402 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_RaycastHit2D_t402 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit2D_t402  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit2D_t402 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_RaycastHit2D_t402 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit2D_t402  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit2D_t402 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit_t89_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t89  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit_t89  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_RaycastHit_t89 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit_t89  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit_t89 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_RaycastHit_t89 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit_t89  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit_t89 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_RaycastHit_t89 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t89  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t89 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_RaycastHit_t89 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit_t89  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit_t89 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2312_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2312  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2312  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_KeyValuePair_2_t2312 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2312  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2312 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2312 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2312  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2312 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_KeyValuePair_2_t2312 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2312  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2312 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2312 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2312  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2312 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2365_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2365  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2365  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_KeyValuePair_2_t2365 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2365  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2365 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2365 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2365  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2365 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UIVertex_t272_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t272  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIVertex_t272  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_UIVertex_t272 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t272  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t272 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_UIVertex_t272 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t272  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t272 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_UIVertex_t272 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UIVertex_t272  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UIVertex_t272 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_UIVertexU5BU5DU26_t3614_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t266** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t266**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_UIVertexU5BU5DU26_t3614_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t266** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t266**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_UIVertex_t272_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UIVertex_t272  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t272 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_UIVertex_t272_UIVertex_t272_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t272  p1, UIVertex_t272  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t272 *)args[0]), *((UIVertex_t272 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t171  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector2_t171  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector2_t171 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ContentType_t250_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UILineInfo_t428_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t428  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UILineInfo_t428  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_UILineInfo_t428 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfo_t428  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UILineInfo_t428 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_UILineInfo_t428 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t428  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t428 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_UILineInfo_t428 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t428  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t428 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_UILineInfo_t428 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UILineInfo_t428  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UILineInfo_t428 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UICharInfo_t430_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t430  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UICharInfo_t430  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_UICharInfo_t430 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfo_t430  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UICharInfo_t430 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_UICharInfo_t430 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t430  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t430 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_UICharInfo_t430 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t430  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t430 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_UICharInfo_t430 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UICharInfo_t430  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UICharInfo_t430 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector3U5BU5DU26_t3615_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t121** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t121**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector3U5BU5DU26_t3615_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t121** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t121**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_Vector3_t12_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector3_t12  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t12 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Vector3_t12_Vector3_t12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32U5BU5DU26_t3616_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t122** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t122**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32U5BU5DU26_t3616_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t122** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t122**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Color32_t382_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t382  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Color32_t382  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Color32_t382 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color32_t382  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color32_t382 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Color32_t382 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color32_t382  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color32_t382 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Color32_t382 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color32_t382  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color32_t382 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Color32_t382 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color32_t382  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Color32_t382 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Color32U5BU5DU26_t3617_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color32U5BU5D_t456** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color32U5BU5D_t456**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Color32U5BU5DU26_t3617_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color32U5BU5D_t456** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Color32U5BU5D_t456**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_Color32_t382_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Color32_t382  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t382 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Color32_t382_Color32_t382_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color32_t382  p1, Color32_t382  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color32_t382 *)args[0]), *((Color32_t382 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Vector2U5BU5DU26_t3618_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2U5BU5D_t249** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2U5BU5D_t249**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector2U5BU5DU26_t3618_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2U5BU5D_t249** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2U5BU5D_t249**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_Vector2_t171_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector2_t171  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t171 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Vector2_t171_Vector2_t171_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t171  p1, Vector2_t171  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Vector2_t171 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Vector4_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4_t350  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector4_t350 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Vector4_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t350  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t350 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Vector4_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector4_t350  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector4_t350 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Vector4U5BU5DU26_t3619_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4U5BU5D_t457** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4U5BU5D_t457**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Vector4U5BU5DU26_t3619_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4U5BU5D_t457** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4U5BU5D_t457**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_Vector4_t350_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector4_t350  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector4_t350 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Vector4_t350_Vector4_t350_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector4_t350  p1, Vector4_t350  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector4_t350 *)args[0]), *((Vector4_t350 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcAchievementData_t613_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t613  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcAchievementData_t613  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_GcAchievementData_t613 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t613  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementData_t613 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_GcAchievementData_t613 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t613  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcAchievementData_t613 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_GcAchievementData_t613 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t613  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcAchievementData_t613 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_GcAchievementData_t613 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t613  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcAchievementData_t613 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_GcScoreData_t614_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t614  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcScoreData_t614  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_GcScoreData_t614 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t614  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcScoreData_t614 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_GcScoreData_t614 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t614  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcScoreData_t614 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_GcScoreData_t614 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t614  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcScoreData_t614 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ContactPoint_t534_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t534  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ContactPoint_t534  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_ContactPoint_t534 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ContactPoint_t534  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ContactPoint_t534 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_ContactPoint_t534 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ContactPoint_t534  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ContactPoint_t534 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_ContactPoint_t534 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ContactPoint_t534  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ContactPoint_t534 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_ContactPoint_t534 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ContactPoint_t534  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ContactPoint_t534 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ContactPoint2D_t541_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint2D_t541  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ContactPoint2D_t541  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_ContactPoint2D_t541 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ContactPoint2D_t541  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ContactPoint2D_t541 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_ContactPoint2D_t541 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ContactPoint2D_t541  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ContactPoint2D_t541 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_ContactPoint2D_t541 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ContactPoint2D_t541  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ContactPoint2D_t541 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_ContactPoint2D_t541 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ContactPoint2D_t541  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ContactPoint2D_t541 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_WebCamDevice_t550_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef WebCamDevice_t550  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	WebCamDevice_t550  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_WebCamDevice_t550 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, WebCamDevice_t550  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((WebCamDevice_t550 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_WebCamDevice_t550 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, WebCamDevice_t550  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((WebCamDevice_t550 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_WebCamDevice_t550 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, WebCamDevice_t550  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((WebCamDevice_t550 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_WebCamDevice_t550 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, WebCamDevice_t550  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((WebCamDevice_t550 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Keyframe_t557_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t557  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Keyframe_t557  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Keyframe_t557 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t557  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Keyframe_t557 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Keyframe_t557 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t557  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Keyframe_t557 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Keyframe_t557 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t557  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Keyframe_t557 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Keyframe_t557 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t557  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Keyframe_t557 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CharacterInfo_t566_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef CharacterInfo_t566  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CharacterInfo_t566  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_CharacterInfo_t566 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CharacterInfo_t566  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CharacterInfo_t566 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_CharacterInfo_t566 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CharacterInfo_t566  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CharacterInfo_t566 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_CharacterInfo_t566 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CharacterInfo_t566  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CharacterInfo_t566 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_CharacterInfo_t566 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CharacterInfo_t566  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CharacterInfo_t566 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_UICharInfoU5BU5DU26_t3620_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t683** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t683**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_UICharInfoU5BU5DU26_t3620_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t683** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t683**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_UICharInfo_t430_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UICharInfo_t430  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UICharInfo_t430 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_UICharInfo_t430_UICharInfo_t430_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t430  p1, UICharInfo_t430  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t430 *)args[0]), *((UICharInfo_t430 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_UILineInfoU5BU5DU26_t3621_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t684** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t684**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_UILineInfoU5BU5DU26_t3621_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t684** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t684**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_UILineInfo_t428_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UILineInfo_t428  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UILineInfo_t428 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_UILineInfo_t428_UILineInfo_t428_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t428  p1, UILineInfo_t428  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t428 *)args[0]), *((UILineInfo_t428 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ParameterModifier_t1710_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1710  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ParameterModifier_t1710  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_ParameterModifier_t1710 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t1710  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ParameterModifier_t1710 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_ParameterModifier_t1710 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t1710  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParameterModifier_t1710 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_ParameterModifier_t1710 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t1710  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParameterModifier_t1710 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_ParameterModifier_t1710 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t1710  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParameterModifier_t1710 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_HitInfo_t633_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t633  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	HitInfo_t633  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_HitInfo_t633 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t633  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((HitInfo_t633 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_HitInfo_t633 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t633  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HitInfo_t633 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2636_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2636  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2636  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_KeyValuePair_2_t2636 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2636  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2636 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2636 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2636  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2636 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_KeyValuePair_2_t2636 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2636  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2636 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2636 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2636  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2636 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TextEditOp_t650_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TargetSearchResult_t884_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef TargetSearchResult_t884  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TargetSearchResult_t884  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_TargetSearchResult_t884 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TargetSearchResult_t884  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TargetSearchResult_t884 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_TargetSearchResult_t884 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TargetSearchResult_t884  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TargetSearchResult_t884 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_TargetSearchResult_t884 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TargetSearchResult_t884  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TargetSearchResult_t884 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2708_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2708  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2708  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_KeyValuePair_2_t2708 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2708  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2708 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2708 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2708  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2708 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_KeyValuePair_2_t2708 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2708  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2708 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2708 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2708  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2708 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_PIXEL_FORMAT_t783_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_PIXEL_FORMATU5BU5DU26_t3622_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PIXEL_FORMATU5BU5D_t2702** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (PIXEL_FORMATU5BU5D_t2702**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_PIXEL_FORMATU5BU5DU26_t3622_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PIXEL_FORMATU5BU5D_t2702** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (PIXEL_FORMATU5BU5D_t2702**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_TrackableResultData_t810_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef TrackableResultData_t810  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TrackableResultData_t810  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_TrackableResultData_t810 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TrackableResultData_t810  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TrackableResultData_t810 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_TrackableResultData_t810 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TrackableResultData_t810  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TrackableResultData_t810 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_TrackableResultData_t810 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TrackableResultData_t810  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t810 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_WordData_t815_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef WordData_t815  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	WordData_t815  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_WordData_t815 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, WordData_t815  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((WordData_t815 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_WordData_t815 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, WordData_t815  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((WordData_t815 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_WordData_t815 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, WordData_t815  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((WordData_t815 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_WordData_t815 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, WordData_t815  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((WordData_t815 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_WordResultData_t814_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef WordResultData_t814  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	WordResultData_t814  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_WordResultData_t814 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, WordResultData_t814  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((WordResultData_t814 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_WordResultData_t814 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, WordResultData_t814  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((WordResultData_t814 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_WordResultData_t814 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, WordResultData_t814  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((WordResultData_t814 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_WordResultData_t814 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, WordResultData_t814  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((WordResultData_t814 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_SmartTerrainRevisionData_t818_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef SmartTerrainRevisionData_t818  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	SmartTerrainRevisionData_t818  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_SmartTerrainRevisionData_t818 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SmartTerrainRevisionData_t818  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SmartTerrainRevisionData_t818 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_SmartTerrainRevisionData_t818 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SmartTerrainRevisionData_t818  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SmartTerrainRevisionData_t818 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_SmartTerrainRevisionData_t818 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, SmartTerrainRevisionData_t818  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((SmartTerrainRevisionData_t818 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_SmartTerrainRevisionData_t818 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, SmartTerrainRevisionData_t818  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((SmartTerrainRevisionData_t818 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_SurfaceData_t819_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef SurfaceData_t819  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	SurfaceData_t819  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_SurfaceData_t819 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SurfaceData_t819  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SurfaceData_t819 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_SurfaceData_t819 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SurfaceData_t819  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SurfaceData_t819 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_SurfaceData_t819 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, SurfaceData_t819  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((SurfaceData_t819 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_SurfaceData_t819 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, SurfaceData_t819  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((SurfaceData_t819 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_PropData_t820_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef PropData_t820  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	PropData_t820  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_PropData_t820 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PropData_t820  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((PropData_t820 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_PropData_t820 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, PropData_t820  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((PropData_t820 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_PropData_t820 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, PropData_t820  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((PropData_t820 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_PropData_t820 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, PropData_t820  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((PropData_t820 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2792_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2792  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2792  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_KeyValuePair_2_t2792 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2792  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2792 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2792 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2792  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2792 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_KeyValuePair_2_t2792 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2792  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2792 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2792 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2792  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2792 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2881_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2881  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2881  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_KeyValuePair_2_t2881 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2881  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2881 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2881 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2881  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2881 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_KeyValuePair_2_t2881 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2881  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2881 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2881 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2881  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2881 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2896_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2896  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2896  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_KeyValuePair_2_t2896 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2896  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2896 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2896 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2896  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2896 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_KeyValuePair_2_t2896 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2896  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2896 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2896 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2896  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2896 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_VirtualButtonData_t811_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef VirtualButtonData_t811  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	VirtualButtonData_t811  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_VirtualButtonData_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, VirtualButtonData_t811  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((VirtualButtonData_t811 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_VirtualButtonData_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, VirtualButtonData_t811  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((VirtualButtonData_t811 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_VirtualButtonData_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, VirtualButtonData_t811  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((VirtualButtonData_t811 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_VirtualButtonData_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, VirtualButtonData_t811  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t811 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_TargetSearchResultU5BU5DU26_t3623_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TargetSearchResultU5BU5D_t2914** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (TargetSearchResultU5BU5D_t2914**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_TargetSearchResultU5BU5DU26_t3623_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TargetSearchResultU5BU5D_t2914** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (TargetSearchResultU5BU5D_t2914**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_TargetSearchResult_t884_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, TargetSearchResult_t884  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TargetSearchResult_t884 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_TargetSearchResult_t884_TargetSearchResult_t884_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TargetSearchResult_t884  p1, TargetSearchResult_t884  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TargetSearchResult_t884 *)args[0]), *((TargetSearchResult_t884 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2935_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2935  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2935  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_KeyValuePair_2_t2935 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2935  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2935 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2935 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2935  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2935 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_KeyValuePair_2_t2935 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2935  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2935 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2935 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2935  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2935 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ProfileData_t898_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileData_t898  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ProfileData_t898  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_ProfileData_t898 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ProfileData_t898  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ProfileData_t898 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_ProfileData_t898 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ProfileData_t898  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ProfileData_t898 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_ProfileData_t898 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ProfileData_t898  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ProfileData_t898 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_ProfileData_t898 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ProfileData_t898  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ProfileData_t898 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Link_t2983_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t2983  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t2983  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Link_t2983 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t2983  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t2983 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Link_t2983 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t2983  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t2983 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Link_t2983 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t2983  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t2983 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Link_t2983 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t2983  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t2983 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ClientCertificateType_t1203_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3005_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3005  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3005  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_KeyValuePair_2_t3005 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3005  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3005 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_KeyValuePair_2_t3005 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3005  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3005 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_KeyValuePair_2_t3005 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3005  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3005 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t3005 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3005  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3005 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_X509ChainStatus_t1333_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t1333  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	X509ChainStatus_t1333  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_X509ChainStatus_t1333 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t1333  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((X509ChainStatus_t1333 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_X509ChainStatus_t1333 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t1333  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((X509ChainStatus_t1333 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_X509ChainStatus_t1333 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t1333  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((X509ChainStatus_t1333 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_X509ChainStatus_t1333 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t1333  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t1333 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t1375_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1375  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Mark_t1375  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Mark_t1375 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t1375  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Mark_t1375 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Mark_t1375 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t1375  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Mark_t1375 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Mark_t1375 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t1375  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Mark_t1375 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Mark_t1375 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t1375  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Mark_t1375 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UriScheme_t1412_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1412  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UriScheme_t1412  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_UriScheme_t1412 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t1412  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UriScheme_t1412 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_UriScheme_t1412 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t1412  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UriScheme_t1412 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_UriScheme_t1412 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t1412  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UriScheme_t1412 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_UriScheme_t1412 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t1412  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UriScheme_t1412 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TableRange_t1480_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1480  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TableRange_t1480  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_TableRange_t1480 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t1480  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TableRange_t1480 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_TableRange_t1480 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t1480  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TableRange_t1480 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_TableRange_t1480 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t1480  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TableRange_t1480 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_TableRange_t1480 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t1480  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TableRange_t1480 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t1557_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1557  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1557  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Slot_t1557 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1557  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1557 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Slot_t1557 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1557  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1557 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Slot_t1557 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1557  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1557 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Slot_t1557 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1557  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1557 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t1565_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1565  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1565  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Slot_t1565 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1565  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1565 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_Slot_t1565 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1565  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1565 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Slot_t1565 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1565  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1565 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Slot_t1565 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1565  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1565 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ILTokenInfo_t1645_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1645  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ILTokenInfo_t1645  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_ILTokenInfo_t1645 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ILTokenInfo_t1645  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ILTokenInfo_t1645 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_ILTokenInfo_t1645 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ILTokenInfo_t1645  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ILTokenInfo_t1645 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_ILTokenInfo_t1645 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ILTokenInfo_t1645  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ILTokenInfo_t1645 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_ILTokenInfo_t1645 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ILTokenInfo_t1645  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ILTokenInfo_t1645 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelData_t1647_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1647  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelData_t1647  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_LabelData_t1647 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelData_t1647  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelData_t1647 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_LabelData_t1647 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelData_t1647  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelData_t1647 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_LabelData_t1647 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelData_t1647  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelData_t1647 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_LabelData_t1647 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelData_t1647  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelData_t1647 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelFixup_t1646_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1646  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelFixup_t1646  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_LabelFixup_t1646 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelFixup_t1646  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelFixup_t1646 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_LabelFixup_t1646 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelFixup_t1646  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelFixup_t1646 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_LabelFixup_t1646 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelFixup_t1646  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelFixup_t1646 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_LabelFixup_t1646 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelFixup_t1646  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelFixup_t1646 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t1692_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t1692  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CustomAttributeTypedArgument_t1692  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_CustomAttributeTypedArgument_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgument_t1692  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1692 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_CustomAttributeTypedArgument_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t1692  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1692 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_CustomAttributeTypedArgument_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t1692  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1692 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_CustomAttributeTypedArgument_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeTypedArgument_t1692  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CustomAttributeTypedArgument_t1692 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t1691_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t1691  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CustomAttributeNamedArgument_t1691  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_CustomAttributeNamedArgument_t1691 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgument_t1691  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1691 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_CustomAttributeNamedArgument_t1691 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t1691  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1691 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_CustomAttributeNamedArgument_t1691 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t1691  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1691 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_CustomAttributeNamedArgument_t1691 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeNamedArgument_t1691  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CustomAttributeNamedArgument_t1691 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_CustomAttributeTypedArgumentU5BU5DU26_t3624_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t2129** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeTypedArgumentU5BU5D_t2129**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_CustomAttributeTypedArgumentU5BU5DU26_t3624_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t2129** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeTypedArgumentU5BU5D_t2129**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_CustomAttributeTypedArgument_t1692_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeTypedArgument_t1692  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeTypedArgument_t1692 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_CustomAttributeTypedArgument_t1692_CustomAttributeTypedArgument_t1692_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t1692  p1, CustomAttributeTypedArgument_t1692  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1692 *)args[0]), *((CustomAttributeTypedArgument_t1692 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_CustomAttributeTypedArgument_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeTypedArgument_t1692  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeTypedArgument_t1692 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_CustomAttributeNamedArgumentU5BU5DU26_t3625_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t2130** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeNamedArgumentU5BU5D_t2130**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_CustomAttributeNamedArgumentU5BU5DU26_t3625_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t2130** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeNamedArgumentU5BU5D_t2130**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t392_Object_t_CustomAttributeNamedArgument_t1691_Int32_t392_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeNamedArgument_t1691  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeNamedArgument_t1691 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_CustomAttributeNamedArgument_t1691_CustomAttributeNamedArgument_t1691_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t1691  p1, CustomAttributeNamedArgument_t1691  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1691 *)args[0]), *((CustomAttributeNamedArgument_t1691 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Object_t_CustomAttributeNamedArgument_t1691 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeNamedArgument_t1691  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeNamedArgument_t1691 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ResourceInfo_t1721_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceInfo_t1721  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ResourceInfo_t1721  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_ResourceInfo_t1721 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceInfo_t1721  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ResourceInfo_t1721 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_ResourceInfo_t1721 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceInfo_t1721  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ResourceInfo_t1721 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_ResourceInfo_t1721 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceInfo_t1721  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ResourceInfo_t1721 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_ResourceInfo_t1721 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceInfo_t1721  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ResourceInfo_t1721 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ResourceCacheItem_t1722_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceCacheItem_t1722  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ResourceCacheItem_t1722  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_ResourceCacheItem_t1722 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceCacheItem_t1722  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ResourceCacheItem_t1722 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t393_ResourceCacheItem_t1722 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceCacheItem_t1722  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ResourceCacheItem_t1722 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_ResourceCacheItem_t1722 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceCacheItem_t1722  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ResourceCacheItem_t1722 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_ResourceCacheItem_t1722 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceCacheItem_t1722  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ResourceCacheItem_t1722 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_DateTime_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t577  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DateTime_t577 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t1445  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Decimal_t1445 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Decimal_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t1445  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Decimal_t1445 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t975_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t975  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TimeSpan_t975  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_TimeSpan_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t975  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TimeSpan_t975 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TypeTag_t1858_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Byte_t699 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Byte_t699 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1449_Int32_t392_Byte_t699 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_RaycastResult_t169_RaycastResult_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t169  p1, RaycastResult_t169  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t169 *)args[0]), *((RaycastResult_t169 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2240 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2240  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2240  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_RaycastResult_t169_RaycastResult_t169 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t169  p1, RaycastResult_t169  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t169 *)args[0]), *((RaycastResult_t169 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RaycastResult_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t169  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t169 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t2280_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2280  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2280  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_ObjectU26_t3573 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1423_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1423  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1423  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Link_t1547 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1547  (*Func)(void* obj, const MethodInfo* method);
	Link_t1547  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2284 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2284  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2284  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1423_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1423  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DictionaryEntry_t1423  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2280_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2280  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2280  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastHit2D_t402 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t402  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit2D_t402  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RaycastHit_t89_RaycastHit_t89_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastHit_t89  p1, RaycastHit_t89  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastHit_t89 *)args[0]), *((RaycastHit_t89 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_RaycastHit_t89 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t89  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit_t89  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Color_t77_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t77  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t77 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Single_t117_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t2312_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2312  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2312  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2316 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2316  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2316  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1423_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1423  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t1423  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2312 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2312  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2312  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2315 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2315  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2315  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2319 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2319  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2319  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2312_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2312  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2312  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_FloatTween_t196 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, FloatTween_t196  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((FloatTween_t196 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t2365_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2365  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2365  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_ColorTween_t193 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, ColorTween_t193  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((ColorTween_t193 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_UIVertex_t272_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t272  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIVertex_t272  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2381 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2381  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2381  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIVertex_t272 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t272  (*Func)(void* obj, const MethodInfo* method);
	UIVertex_t272  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_UIVertex_t272_UIVertex_t272 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t272  p1, UIVertex_t272  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t272 *)args[0]), *((UIVertex_t272 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UIVertex_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t272  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t272 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_UIVertex_t272_UIVertex_t272 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t272  p1, UIVertex_t272  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t272 *)args[0]), *((UIVertex_t272 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UIVertex_t272_UIVertex_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t272  p1, UIVertex_t272  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t272 *)args[0]), *((UIVertex_t272 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_UILineInfo_t428 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t428  (*Func)(void* obj, const MethodInfo* method);
	UILineInfo_t428  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UICharInfo_t430 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t430  (*Func)(void* obj, const MethodInfo* method);
	UICharInfo_t430  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector2_t171_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t171  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Vector3_t12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2480 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2480  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2480  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t12_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t12  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_Vector3_t12_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t12_Vector3_t12_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Color32_t382_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t382  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Color32_t382  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2499 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2499  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2499  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color32_t382 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t382  (*Func)(void* obj, const MethodInfo* method);
	Color32_t382  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Color32_t382_Color32_t382 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color32_t382  p1, Color32_t382  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color32_t382 *)args[0]), *((Color32_t382 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Color32_t382_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color32_t382  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color32_t382 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_Color32_t382_Color32_t382 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color32_t382  p1, Color32_t382  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color32_t382 *)args[0]), *((Color32_t382 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Color32_t382_Color32_t382_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color32_t382  p1, Color32_t382  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color32_t382 *)args[0]), *((Color32_t382 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2509 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2509  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2509  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Vector2_t171_Vector2_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t171  p1, Vector2_t171  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Vector2_t171 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector2_t171_Vector2_t171_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t171  p1, Vector2_t171  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t171 *)args[0]), *((Vector2_t171 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2520 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2520  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2520  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector4_t350_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector4_t350  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector4_t350 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_Vector4_t350_Vector4_t350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector4_t350  p1, Vector4_t350  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector4_t350 *)args[0]), *((Vector4_t350 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector4_t350_Vector4_t350_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector4_t350  p1, Vector4_t350  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector4_t350 *)args[0]), *((Vector4_t350 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_GcAchievementData_t613 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t613  (*Func)(void* obj, const MethodInfo* method);
	GcAchievementData_t613  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcScoreData_t614 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t614  (*Func)(void* obj, const MethodInfo* method);
	GcScoreData_t614  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContactPoint_t534 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t534  (*Func)(void* obj, const MethodInfo* method);
	ContactPoint_t534  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContactPoint2D_t541 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint2D_t541  (*Func)(void* obj, const MethodInfo* method);
	ContactPoint2D_t541  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WebCamDevice_t550 (const MethodInfo* method, void* obj, void** args)
{
	typedef WebCamDevice_t550  (*Func)(void* obj, const MethodInfo* method);
	WebCamDevice_t550  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Keyframe_t557 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t557  (*Func)(void* obj, const MethodInfo* method);
	Keyframe_t557  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CharacterInfo_t566 (const MethodInfo* method, void* obj, void** args)
{
	typedef CharacterInfo_t566  (*Func)(void* obj, const MethodInfo* method);
	CharacterInfo_t566  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UICharInfo_t430_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t430  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UICharInfo_t430  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2584 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2584  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2584  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_UICharInfo_t430_UICharInfo_t430 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t430  p1, UICharInfo_t430  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t430 *)args[0]), *((UICharInfo_t430 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UICharInfo_t430_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t430  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t430 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_UICharInfo_t430_UICharInfo_t430 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t430  p1, UICharInfo_t430  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t430 *)args[0]), *((UICharInfo_t430 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UICharInfo_t430_UICharInfo_t430_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t430  p1, UICharInfo_t430  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t430 *)args[0]), *((UICharInfo_t430 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_UILineInfo_t428_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t428  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UILineInfo_t428  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2593 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2593  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2593  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_UILineInfo_t428_UILineInfo_t428 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t428  p1, UILineInfo_t428  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t428 *)args[0]), *((UILineInfo_t428 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UILineInfo_t428_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t428  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t428 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_UILineInfo_t428_UILineInfo_t428 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t428  p1, UILineInfo_t428  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t428 *)args[0]), *((UILineInfo_t428 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UILineInfo_t428_UILineInfo_t428_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t428  p1, UILineInfo_t428  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t428 *)args[0]), *((UILineInfo_t428 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_ParameterModifier_t1710 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1710  (*Func)(void* obj, const MethodInfo* method);
	ParameterModifier_t1710  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HitInfo_t633 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t633  (*Func)(void* obj, const MethodInfo* method);
	HitInfo_t633  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t650_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2636_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2636  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2636  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t650_Object_t_Int32_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_TextEditOpU26_t3626 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2641 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2641  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2641  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2636 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2636  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2636  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t650 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2640 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2640  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2640  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2644 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2644  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2644  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2636_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2636  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2636  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TargetSearchResult_t884 (const MethodInfo* method, void* obj, void** args)
{
	typedef TargetSearchResult_t884  (*Func)(void* obj, const MethodInfo* method);
	TargetSearchResult_t884  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2708_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2708  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2708  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PIXEL_FORMAT_t783_Int32_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PIXEL_FORMAT_t783_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2713 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2713  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2713  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2708 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2708  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2708  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2712 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2712  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2712  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2708_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2708  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2708  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2725  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2725  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TrackableResultData_t810 (const MethodInfo* method, void* obj, void** args)
{
	typedef TrackableResultData_t810  (*Func)(void* obj, const MethodInfo* method);
	TrackableResultData_t810  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WordData_t815 (const MethodInfo* method, void* obj, void** args)
{
	typedef WordData_t815  (*Func)(void* obj, const MethodInfo* method);
	WordData_t815  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WordResultData_t814 (const MethodInfo* method, void* obj, void** args)
{
	typedef WordResultData_t814  (*Func)(void* obj, const MethodInfo* method);
	WordResultData_t814  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2772 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2772  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2772  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_TrackableResultData_t810_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, TrackableResultData_t810  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((TrackableResultData_t810 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_SmartTerrainRevisionData_t818 (const MethodInfo* method, void* obj, void** args)
{
	typedef SmartTerrainRevisionData_t818  (*Func)(void* obj, const MethodInfo* method);
	SmartTerrainRevisionData_t818  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SurfaceData_t819 (const MethodInfo* method, void* obj, void** args)
{
	typedef SurfaceData_t819  (*Func)(void* obj, const MethodInfo* method);
	SurfaceData_t819  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PropData_t820 (const MethodInfo* method, void* obj, void** args)
{
	typedef PropData_t820  (*Func)(void* obj, const MethodInfo* method);
	PropData_t820  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2792_Object_t_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2792  (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	KeyValuePair_2_t2792  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1430_Object_t_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2797 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2797  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2797  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1423_Object_t_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1423  (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	DictionaryEntry_t1423  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2792 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2792  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2792  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2796 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2796  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2796  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t1444_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2800 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2800  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2800  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2792_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2792  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2792  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int16_t1444_Int16_t1444 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SmartTerrainInitializationInfo_t760_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, SmartTerrainInitializationInfo_t760  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((SmartTerrainInitializationInfo_t760 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t2881_Int32_t392_TrackableResultData_t810 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2881  (*Func)(void* obj, int32_t p1, TrackableResultData_t810  p2, const MethodInfo* method);
	KeyValuePair_2_t2881  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t810 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int32_t392_TrackableResultData_t810 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, TrackableResultData_t810  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t810 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TrackableResultData_t810_Int32_t392_TrackableResultData_t810 (const MethodInfo* method, void* obj, void** args)
{
	typedef TrackableResultData_t810  (*Func)(void* obj, int32_t p1, TrackableResultData_t810  p2, const MethodInfo* method);
	TrackableResultData_t810  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t810 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_TrackableResultDataU26_t3627 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, TrackableResultData_t810 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (TrackableResultData_t810 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TrackableResultData_t810_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TrackableResultData_t810  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TrackableResultData_t810  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2885 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2885  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2885  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1423_Int32_t392_TrackableResultData_t810 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1423  (*Func)(void* obj, int32_t p1, TrackableResultData_t810  p2, const MethodInfo* method);
	DictionaryEntry_t1423  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t810 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2881 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2881  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2881  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2884 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2884  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2884  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t392_TrackableResultData_t810_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, TrackableResultData_t810  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t810 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2888 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2888  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2888  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2881_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2881  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2881  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_TrackableResultData_t810_TrackableResultData_t810 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TrackableResultData_t810  p1, TrackableResultData_t810  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TrackableResultData_t810 *)args[0]), *((TrackableResultData_t810 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2896_Int32_t392_VirtualButtonData_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2896  (*Func)(void* obj, int32_t p1, VirtualButtonData_t811  p2, const MethodInfo* method);
	KeyValuePair_2_t2896  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t811 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Int32_t392_VirtualButtonData_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, VirtualButtonData_t811  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t811 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VirtualButtonData_t811_Int32_t392_VirtualButtonData_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef VirtualButtonData_t811  (*Func)(void* obj, int32_t p1, VirtualButtonData_t811  p2, const MethodInfo* method);
	VirtualButtonData_t811  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t811 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Int32_t392_VirtualButtonDataU26_t3628 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, VirtualButtonData_t811 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (VirtualButtonData_t811 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VirtualButtonData_t811_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef VirtualButtonData_t811  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	VirtualButtonData_t811  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2901 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2901  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2901  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1423_Int32_t392_VirtualButtonData_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1423  (*Func)(void* obj, int32_t p1, VirtualButtonData_t811  p2, const MethodInfo* method);
	DictionaryEntry_t1423  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t811 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2896 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2896  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2896  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VirtualButtonData_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef VirtualButtonData_t811  (*Func)(void* obj, const MethodInfo* method);
	VirtualButtonData_t811  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2900 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2900  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2900  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t392_VirtualButtonData_t811_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, VirtualButtonData_t811  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t811 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2904 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2904  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2904  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2896_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2896  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2896  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_VirtualButtonData_t811_VirtualButtonData_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, VirtualButtonData_t811  p1, VirtualButtonData_t811  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((VirtualButtonData_t811 *)args[0]), *((VirtualButtonData_t811 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TargetSearchResult_t884_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TargetSearchResult_t884  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TargetSearchResult_t884  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2915 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2915  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2915  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_TargetSearchResult_t884_TargetSearchResult_t884 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TargetSearchResult_t884  p1, TargetSearchResult_t884  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TargetSearchResult_t884 *)args[0]), *((TargetSearchResult_t884 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_TargetSearchResult_t884_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, TargetSearchResult_t884  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((TargetSearchResult_t884 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_TargetSearchResult_t884_TargetSearchResult_t884 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TargetSearchResult_t884  p1, TargetSearchResult_t884  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TargetSearchResult_t884 *)args[0]), *((TargetSearchResult_t884 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_TargetSearchResult_t884_TargetSearchResult_t884_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, TargetSearchResult_t884  p1, TargetSearchResult_t884  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((TargetSearchResult_t884 *)args[0]), *((TargetSearchResult_t884 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1449_Object_t_ProfileData_t898 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ProfileData_t898  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t898 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2935_Object_t_ProfileData_t898 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2935  (*Func)(void* obj, Object_t * p1, ProfileData_t898  p2, const MethodInfo* method);
	KeyValuePair_2_t2935  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t898 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_ProfileData_t898 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, ProfileData_t898  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t898 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_ProfileData_t898_Object_t_ProfileData_t898 (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileData_t898  (*Func)(void* obj, Object_t * p1, ProfileData_t898  p2, const MethodInfo* method);
	ProfileData_t898  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t898 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_ProfileDataU26_t3629 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, ProfileData_t898 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (ProfileData_t898 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2940 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2940  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2940  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1423_Object_t_ProfileData_t898 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1423  (*Func)(void* obj, Object_t * p1, ProfileData_t898  p2, const MethodInfo* method);
	DictionaryEntry_t1423  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t898 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2935 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2935  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2935  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2939 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2939  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2939  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_ProfileData_t898_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, ProfileData_t898  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t898 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2943 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2943  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2943  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2935_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2935  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2935  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_ProfileData_t898_ProfileData_t898 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ProfileData_t898  p1, ProfileData_t898  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ProfileData_t898 *)args[0]), *((ProfileData_t898 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Link_t2983 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t2983  (*Func)(void* obj, const MethodInfo* method);
	Link_t2983  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ClientCertificateType_t1203 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3005_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3005  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	KeyValuePair_2_t3005  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Object_t_BooleanU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3010 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3010  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3010  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1423_Object_t_SByte_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1423  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	DictionaryEntry_t1423  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3005 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3005  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3005  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3009 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3009  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3009  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3013 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3013  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3013  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t3005_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3005  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3005  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatus_t1333 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t1333  (*Func)(void* obj, const MethodInfo* method);
	X509ChainStatus_t1333  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t1375 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1375  (*Func)(void* obj, const MethodInfo* method);
	Mark_t1375  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriScheme_t1412 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1412  (*Func)(void* obj, const MethodInfo* method);
	UriScheme_t1412  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TableRange_t1480 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1480  (*Func)(void* obj, const MethodInfo* method);
	TableRange_t1480  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t1557 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1557  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1557  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t1565 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1565  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1565  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ILTokenInfo_t1645 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1645  (*Func)(void* obj, const MethodInfo* method);
	ILTokenInfo_t1645  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelData_t1647 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1647  (*Func)(void* obj, const MethodInfo* method);
	LabelData_t1647  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelFixup_t1646 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1646  (*Func)(void* obj, const MethodInfo* method);
	LabelFixup_t1646  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t1692  (*Func)(void* obj, const MethodInfo* method);
	CustomAttributeTypedArgument_t1692  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t1691 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t1691  (*Func)(void* obj, const MethodInfo* method);
	CustomAttributeNamedArgument_t1691  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t1692_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t1692  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	CustomAttributeTypedArgument_t1692  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3068 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3068  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3068  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_CustomAttributeTypedArgument_t1692_CustomAttributeTypedArgument_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t1692  p1, CustomAttributeTypedArgument_t1692  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1692 *)args[0]), *((CustomAttributeTypedArgument_t1692 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1692_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeTypedArgument_t1692  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1692 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_CustomAttributeTypedArgument_t1692_CustomAttributeTypedArgument_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t1692  p1, CustomAttributeTypedArgument_t1692  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1692 *)args[0]), *((CustomAttributeTypedArgument_t1692 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1692_CustomAttributeTypedArgument_t1692_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeTypedArgument_t1692  p1, CustomAttributeTypedArgument_t1692  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1692 *)args[0]), *((CustomAttributeTypedArgument_t1692 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t1691_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t1691  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	CustomAttributeNamedArgument_t1691  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3079 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3079  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3079  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_CustomAttributeNamedArgument_t1691_CustomAttributeNamedArgument_t1691 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t1691  p1, CustomAttributeNamedArgument_t1691  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1691 *)args[0]), *((CustomAttributeNamedArgument_t1691 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1691_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeNamedArgument_t1691  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1691 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t392_CustomAttributeNamedArgument_t1691_CustomAttributeNamedArgument_t1691 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t1691  p1, CustomAttributeNamedArgument_t1691  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1691 *)args[0]), *((CustomAttributeNamedArgument_t1691 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1691_CustomAttributeNamedArgument_t1691_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeNamedArgument_t1691  p1, CustomAttributeNamedArgument_t1691  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1691 *)args[0]), *((CustomAttributeNamedArgument_t1691 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_ResourceInfo_t1721 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceInfo_t1721  (*Func)(void* obj, const MethodInfo* method);
	ResourceInfo_t1721  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ResourceCacheItem_t1722 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceCacheItem_t1722  (*Func)(void* obj, const MethodInfo* method);
	ResourceCacheItem_t1722  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeTag_t1858 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_DateTimeOffset_t2014_DateTimeOffset_t2014 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t2014  p1, DateTimeOffset_t2014  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t2014 *)args[0]), *((DateTimeOffset_t2014 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_DateTimeOffset_t2014_DateTimeOffset_t2014 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t2014  p1, DateTimeOffset_t2014  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t2014 *)args[0]), *((DateTimeOffset_t2014 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Nullable_1_t2120 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t2120  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t2120 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t392_Guid_t2036_Guid_t2036 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t2036  p1, Guid_t2036  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t2036 *)args[0]), *((Guid_t2036 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t393_Guid_t2036_Guid_t2036 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t2036  p1, Guid_t2036  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t2036 *)args[0]), *((Guid_t2036 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

extern const InvokerMethod g_Il2CppInvokerPointers[1794] = 
{
	RuntimeInvoker_Void_t1449,
	RuntimeInvoker_Void_t1449_Int32_t392,
	RuntimeInvoker_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t,
	RuntimeInvoker_Void_t1449_SByte_t1443,
	RuntimeInvoker_Boolean_t393,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392,
	RuntimeInvoker_Boolean_t393_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392,
	RuntimeInvoker_InitError_t905_Object_t,
	RuntimeInvoker_Int32_t392_Object_t,
	RuntimeInvoker_Int32_t392_Int32_t392_Object_t,
	RuntimeInvoker_Object_t_Object_t,
	RuntimeInvoker_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_RaycastResult_t169_RaycastResult_t169,
	RuntimeInvoker_Boolean_t393_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Object_t,
	RuntimeInvoker_Vector2_t171,
	RuntimeInvoker_Void_t1449_Vector2_t171,
	RuntimeInvoker_MoveDirection_t166,
	RuntimeInvoker_RaycastResult_t169,
	RuntimeInvoker_Void_t1449_RaycastResult_t169,
	RuntimeInvoker_Vector3_t12,
	RuntimeInvoker_Void_t1449_Vector3_t12,
	RuntimeInvoker_Single_t117,
	RuntimeInvoker_Void_t1449_Single_t117,
	RuntimeInvoker_InputButton_t173,
	RuntimeInvoker_RaycastResult_t169_Object_t,
	RuntimeInvoker_MoveDirection_t166_Single_t117_Single_t117,
	RuntimeInvoker_MoveDirection_t166_Single_t117_Single_t117_Single_t117,
	RuntimeInvoker_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Single_t117_Single_t117_Single_t117,
	RuntimeInvoker_Boolean_t393_Int32_t392_PointerEventDataU26_t3514_SByte_t1443,
	RuntimeInvoker_Object_t_Touch_t375_BooleanU26_t3515_BooleanU26_t3515,
	RuntimeInvoker_FramePressState_t174_Int32_t392,
	RuntimeInvoker_Object_t_Int32_t392,
	RuntimeInvoker_Boolean_t393_Vector2_t171_Vector2_t171_Single_t117_SByte_t1443,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Object_t,
	RuntimeInvoker_InputMode_t184,
	RuntimeInvoker_Void_t1449_Object_t_SByte_t1443_SByte_t1443,
	RuntimeInvoker_LayerMask_t13,
	RuntimeInvoker_Void_t1449_LayerMask_t13,
	RuntimeInvoker_Int32_t392_RaycastHit_t89_RaycastHit_t89,
	RuntimeInvoker_Color_t77,
	RuntimeInvoker_Void_t1449_Color_t77,
	RuntimeInvoker_ColorTweenMode_t190,
	RuntimeInvoker_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_ColorBlock_t207,
	RuntimeInvoker_Object_t_Object_t_Vector2_t171,
	RuntimeInvoker_Object_t_Resources_t208,
	RuntimeInvoker_Object_t_Object_t_SByte_t1443_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Single_t117_Single_t117,
	RuntimeInvoker_Void_t1449_Single_t117_Single_t117_Single_t117,
	RuntimeInvoker_Object_t_Single_t117,
	RuntimeInvoker_FontStyle_t595,
	RuntimeInvoker_TextAnchor_t444,
	RuntimeInvoker_HorizontalWrapMode_t564,
	RuntimeInvoker_VerticalWrapMode_t565,
	RuntimeInvoker_Boolean_t393_Vector2_t171_Object_t,
	RuntimeInvoker_Vector2_t171_Vector2_t171,
	RuntimeInvoker_Rect_t94,
	RuntimeInvoker_Void_t1449_Color_t77_Single_t117_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_Color_t77_Single_t117_SByte_t1443_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Color_t77_Single_t117,
	RuntimeInvoker_Void_t1449_Single_t117_Single_t117_SByte_t1443,
	RuntimeInvoker_BlockingObjects_t235,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Vector2_t171_Object_t,
	RuntimeInvoker_Type_t241,
	RuntimeInvoker_FillMethod_t242,
	RuntimeInvoker_Vector4_t350_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Color32_t382_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Vector2_t171_Vector2_t171_Color32_t382_Vector2_t171_Vector2_t171,
	RuntimeInvoker_Vector4_t350_Vector4_t350_Rect_t94,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Single_t117_SByte_t1443_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Single_t117_Single_t117_SByte_t1443_Int32_t392,
	RuntimeInvoker_Vector2_t171_Vector2_t171_Rect_t94,
	RuntimeInvoker_ContentType_t250,
	RuntimeInvoker_LineType_t253,
	RuntimeInvoker_InputType_t251,
	RuntimeInvoker_TouchScreenKeyboardType_t425,
	RuntimeInvoker_CharacterValidation_t252,
	RuntimeInvoker_Char_t424,
	RuntimeInvoker_Void_t1449_Int16_t1444,
	RuntimeInvoker_Void_t1449_Int32U26_t3516,
	RuntimeInvoker_Int32_t392_Vector2_t171_Object_t,
	RuntimeInvoker_Int32_t392_Vector2_t171,
	RuntimeInvoker_EditState_t257_Object_t,
	RuntimeInvoker_Boolean_t393_Int16_t1444,
	RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Int32_t392_Int32_t392_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Vector2_t171,
	RuntimeInvoker_Single_t117_Int32_t392_Object_t,
	RuntimeInvoker_Char_t424_Object_t_Int32_t392_Int16_t1444,
	RuntimeInvoker_Void_t1449_Int32_t392_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_IntPtr_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Int16_t1444_Object_t_Object_t,
	RuntimeInvoker_Char_t424_Object_t,
	RuntimeInvoker_Void_t1449_Rect_t94_SByte_t1443,
	RuntimeInvoker_Mode_t279,
	RuntimeInvoker_Navigation_t280,
	RuntimeInvoker_Void_t1449_Rect_t94,
	RuntimeInvoker_Direction_t286,
	RuntimeInvoker_Void_t1449_Single_t117_SByte_t1443,
	RuntimeInvoker_Axis_t288,
	RuntimeInvoker_MovementType_t292,
	RuntimeInvoker_ScrollbarVisibility_t293,
	RuntimeInvoker_Void_t1449_Single_t117_Int32_t392,
	RuntimeInvoker_Single_t117_Single_t117_Single_t117,
	RuntimeInvoker_Bounds_t90,
	RuntimeInvoker_Void_t1449_Navigation_t280,
	RuntimeInvoker_Transition_t297,
	RuntimeInvoker_Void_t1449_ColorBlock_t207,
	RuntimeInvoker_SpriteState_t299,
	RuntimeInvoker_Void_t1449_SpriteState_t299,
	RuntimeInvoker_SelectionState_t298,
	RuntimeInvoker_Object_t_Vector3_t12,
	RuntimeInvoker_Vector3_t12_Object_t_Vector2_t171,
	RuntimeInvoker_Void_t1449_Color_t77_SByte_t1443,
	RuntimeInvoker_Boolean_t393_ColorU26_t3517_Color_t77,
	RuntimeInvoker_Direction_t303,
	RuntimeInvoker_Single_t117_Single_t117,
	RuntimeInvoker_Axis_t305,
	RuntimeInvoker_Object_t_Object_t_Int32_t392,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_TextGenerationSettings_t385_Vector2_t171,
	RuntimeInvoker_Vector2_t171_Int32_t392,
	RuntimeInvoker_Rect_t94_Object_t_BooleanU26_t3515,
	RuntimeInvoker_Rect_t94_Rect_t94_Rect_t94,
	RuntimeInvoker_Rect_t94_Object_t_Object_t,
	RuntimeInvoker_AspectMode_t319,
	RuntimeInvoker_Single_t117_Single_t117_Int32_t392,
	RuntimeInvoker_ScaleMode_t321,
	RuntimeInvoker_ScreenMatchMode_t322,
	RuntimeInvoker_Unit_t323,
	RuntimeInvoker_FitMode_t325,
	RuntimeInvoker_Corner_t327,
	RuntimeInvoker_Axis_t328,
	RuntimeInvoker_Constraint_t329,
	RuntimeInvoker_Single_t117_Int32_t392,
	RuntimeInvoker_Single_t117_Int32_t392_Single_t117,
	RuntimeInvoker_Void_t1449_Single_t117_Single_t117_Single_t117_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Single_t117_Single_t117,
	RuntimeInvoker_Single_t117_Object_t_Int32_t392,
	RuntimeInvoker_Single_t117_Object_t,
	RuntimeInvoker_Single_t117_Object_t_Object_t_Single_t117,
	RuntimeInvoker_Single_t117_Object_t_Object_t_Single_t117_ILayoutElementU26_t3518,
	RuntimeInvoker_Void_t1449_UIVertexU26_t3519_Int32_t392,
	RuntimeInvoker_Void_t1449_UIVertex_t272_Int32_t392,
	RuntimeInvoker_Void_t1449_Vector3_t12_Color32_t382_Vector2_t171_Vector2_t171_Vector3_t12_Vector4_t350,
	RuntimeInvoker_Void_t1449_Vector3_t12_Color32_t382_Vector2_t171,
	RuntimeInvoker_Void_t1449_UIVertex_t272,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Color32_t382_Int32_t392_Int32_t392_Single_t117_Single_t117,
	RuntimeInvoker_Void_t1449_Object_t_Double_t710,
	RuntimeInvoker_Void_t1449_Int64_t711_Object_t,
	RuntimeInvoker_Void_t1449_GcAchievementDescriptionData_t612_Int32_t392,
	RuntimeInvoker_Void_t1449_GcUserProfileData_t611_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Double_t710_Object_t,
	RuntimeInvoker_Void_t1449_Int64_t711_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_UserProfileU5BU5DU26_t3520_Object_t_Int32_t392,
	RuntimeInvoker_Void_t1449_UserProfileU5BU5DU26_t3520_Int32_t392,
	RuntimeInvoker_Void_t1449_GcScoreData_t614,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_BoundsU26_t3521,
	RuntimeInvoker_Boolean_t393_BoneWeight_t485_BoneWeight_t485,
	RuntimeInvoker_ScreenOrientation_t618,
	RuntimeInvoker_Void_t1449_Vector3U26_t3522,
	RuntimeInvoker_Void_t1449_Matrix4x4_t124,
	RuntimeInvoker_Void_t1449_Matrix4x4U26_t3523,
	RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_Color_t77,
	RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_Color_t77_Single_t117,
	RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_ColorU26_t3517_Single_t117,
	RuntimeInvoker_Object_t_Object_t_Vector3U26_t3522,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Int32_t392_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443_IntPtr_t,
	RuntimeInvoker_TextureFormat_t621,
	RuntimeInvoker_Color_t77_Single_t117_Single_t117,
	RuntimeInvoker_Boolean_t393_Int32_t392_Int32_t392_Int32_t392_SByte_t1443,
	RuntimeInvoker_Void_t1449_Rect_t94_Int32_t392_Int32_t392_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_RectU26_t3524_Int32_t392_Int32_t392_SByte_t1443,
	RuntimeInvoker_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Object_t_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_IntPtr_t_Int32_t392,
	RuntimeInvoker_Void_t1449_CullingGroupEvent_t492,
	RuntimeInvoker_Object_t_CullingGroupEvent_t492_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Color_t77_Single_t117,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_Object_t,
	RuntimeInvoker_Void_t1449_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t3525_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_SByte_t1443_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_Object_t,
	RuntimeInvoker_Void_t1449_Vector3_t12_Vector3_t12,
	RuntimeInvoker_Void_t1449_Vector3U26_t3522_Vector3U26_t3522,
	RuntimeInvoker_Void_t1449_ColorU26_t3517,
	RuntimeInvoker_Int32_t392_LayerMask_t13,
	RuntimeInvoker_LayerMask_t13_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Single_t117,
	RuntimeInvoker_Vector2_t171_Vector2_t171_Vector2_t171,
	RuntimeInvoker_Single_t117_Vector2_t171_Vector2_t171,
	RuntimeInvoker_Single_t117_Vector2_t171,
	RuntimeInvoker_Vector2_t171_Vector2_t171_Single_t117,
	RuntimeInvoker_Boolean_t393_Vector2_t171_Vector2_t171,
	RuntimeInvoker_Vector2_t171_Vector3_t12,
	RuntimeInvoker_Vector3_t12_Vector2_t171,
	RuntimeInvoker_Vector3_t12_Vector3_t12_Vector3_t12_Single_t117,
	RuntimeInvoker_Vector3_t12_Vector3_t12,
	RuntimeInvoker_Single_t117_Vector3_t12_Vector3_t12,
	RuntimeInvoker_Single_t117_Vector3_t12,
	RuntimeInvoker_Vector3_t12_Vector3_t12_Vector3_t12,
	RuntimeInvoker_Vector3_t12_Vector3_t12_Single_t117,
	RuntimeInvoker_Boolean_t393_Vector3_t12_Vector3_t12,
	RuntimeInvoker_Void_t1449_Single_t117_Single_t117_Single_t117_Single_t117,
	RuntimeInvoker_Color_t77_Color_t77_Color_t77_Single_t117,
	RuntimeInvoker_Color_t77_Color_t77_Single_t117,
	RuntimeInvoker_Vector4_t350_Color_t77,
	RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Color32_t382_Color_t77,
	RuntimeInvoker_Color_t77_Color32_t382,
	RuntimeInvoker_Quaternion_t125,
	RuntimeInvoker_Single_t117_Quaternion_t125_Quaternion_t125,
	RuntimeInvoker_Quaternion_t125_Single_t117_Vector3_t12,
	RuntimeInvoker_Quaternion_t125_Single_t117_Vector3U26_t3522,
	RuntimeInvoker_Void_t1449_SingleU26_t3526_Vector3U26_t3522,
	RuntimeInvoker_Quaternion_t125_Quaternion_t125,
	RuntimeInvoker_Quaternion_t125_QuaternionU26_t3527,
	RuntimeInvoker_Void_t1449_Quaternion_t125_Vector3U26_t3522_SingleU26_t3526,
	RuntimeInvoker_Void_t1449_QuaternionU26_t3527_Vector3U26_t3522_SingleU26_t3526,
	RuntimeInvoker_Quaternion_t125_Quaternion_t125_Quaternion_t125,
	RuntimeInvoker_Vector3_t12_Quaternion_t125_Vector3_t12,
	RuntimeInvoker_Boolean_t393_Quaternion_t125_Quaternion_t125,
	RuntimeInvoker_Boolean_t393_Vector3_t12,
	RuntimeInvoker_Boolean_t393_Rect_t94,
	RuntimeInvoker_Boolean_t393_Rect_t94_Rect_t94,
	RuntimeInvoker_Single_t117_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Single_t117,
	RuntimeInvoker_Matrix4x4_t124_Matrix4x4_t124,
	RuntimeInvoker_Matrix4x4_t124_Matrix4x4U26_t3523,
	RuntimeInvoker_Boolean_t393_Matrix4x4_t124_Matrix4x4U26_t3523,
	RuntimeInvoker_Boolean_t393_Matrix4x4U26_t3523_Matrix4x4U26_t3523,
	RuntimeInvoker_Matrix4x4_t124,
	RuntimeInvoker_Vector4_t350_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Vector4_t350,
	RuntimeInvoker_Matrix4x4_t124_Vector3_t12,
	RuntimeInvoker_Void_t1449_Vector3_t12_Quaternion_t125_Vector3_t12,
	RuntimeInvoker_Matrix4x4_t124_Vector3_t12_Quaternion_t125_Vector3_t12,
	RuntimeInvoker_Matrix4x4_t124_Vector3U26_t3522_QuaternionU26_t3527_Vector3U26_t3522,
	RuntimeInvoker_Matrix4x4_t124_Single_t117_Single_t117_Single_t117_Single_t117_Single_t117_Single_t117,
	RuntimeInvoker_Matrix4x4_t124_Single_t117_Single_t117_Single_t117_Single_t117,
	RuntimeInvoker_Matrix4x4_t124_Matrix4x4_t124_Matrix4x4_t124,
	RuntimeInvoker_Vector4_t350_Matrix4x4_t124_Vector4_t350,
	RuntimeInvoker_Boolean_t393_Matrix4x4_t124_Matrix4x4_t124,
	RuntimeInvoker_Void_t1449_Bounds_t90,
	RuntimeInvoker_Boolean_t393_Bounds_t90,
	RuntimeInvoker_Boolean_t393_Bounds_t90_Vector3_t12,
	RuntimeInvoker_Boolean_t393_BoundsU26_t3521_Vector3U26_t3522,
	RuntimeInvoker_Single_t117_Bounds_t90_Vector3_t12,
	RuntimeInvoker_Single_t117_BoundsU26_t3521_Vector3U26_t3522,
	RuntimeInvoker_Boolean_t393_RayU26_t3528_BoundsU26_t3521_SingleU26_t3526,
	RuntimeInvoker_Boolean_t393_Ray_t88,
	RuntimeInvoker_Boolean_t393_Ray_t88_SingleU26_t3526,
	RuntimeInvoker_Vector3_t12_BoundsU26_t3521_Vector3U26_t3522,
	RuntimeInvoker_Boolean_t393_Bounds_t90_Bounds_t90,
	RuntimeInvoker_Single_t117_Vector4_t350_Vector4_t350,
	RuntimeInvoker_Single_t117_Vector4_t350,
	RuntimeInvoker_Vector4_t350,
	RuntimeInvoker_Vector4_t350_Vector4_t350_Vector4_t350,
	RuntimeInvoker_Vector4_t350_Vector4_t350_Single_t117,
	RuntimeInvoker_Boolean_t393_Vector4_t350_Vector4_t350,
	RuntimeInvoker_Vector3_t12_Single_t117,
	RuntimeInvoker_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Single_t117,
	RuntimeInvoker_Single_t117_Single_t117_Single_t117_Single_t117,
	RuntimeInvoker_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Boolean_t393_Single_t117_Single_t117,
	RuntimeInvoker_Single_t117_Single_t117_Single_t117_SingleU26_t3526_Single_t117_Single_t117_Single_t117,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392,
	RuntimeInvoker_Void_t1449_RectU26_t3524,
	RuntimeInvoker_Void_t1449_Vector2U26_t3529,
	RuntimeInvoker_Void_t1449_Int32_t392_Single_t117_Single_t117,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Single_t117,
	RuntimeInvoker_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_SphericalHarmonicsL2U26_t3530,
	RuntimeInvoker_Void_t1449_Color_t77_SphericalHarmonicsL2U26_t3530,
	RuntimeInvoker_Void_t1449_ColorU26_t3517_SphericalHarmonicsL2U26_t3530,
	RuntimeInvoker_Void_t1449_Vector3_t12_Color_t77_Single_t117,
	RuntimeInvoker_Void_t1449_Vector3_t12_Color_t77_SphericalHarmonicsL2U26_t3530,
	RuntimeInvoker_Void_t1449_Vector3U26_t3522_ColorU26_t3517_SphericalHarmonicsL2U26_t3530,
	RuntimeInvoker_SphericalHarmonicsL2_t509_SphericalHarmonicsL2_t509_Single_t117,
	RuntimeInvoker_SphericalHarmonicsL2_t509_Single_t117_SphericalHarmonicsL2_t509,
	RuntimeInvoker_SphericalHarmonicsL2_t509_SphericalHarmonicsL2_t509_SphericalHarmonicsL2_t509,
	RuntimeInvoker_Boolean_t393_SphericalHarmonicsL2_t509_SphericalHarmonicsL2_t509,
	RuntimeInvoker_Void_t1449_Vector4U26_t3531,
	RuntimeInvoker_Vector4_t350_Object_t,
	RuntimeInvoker_Vector2_t171_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Vector2U26_t3529,
	RuntimeInvoker_RuntimePlatform_t467,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392_SByte_t1443,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_CameraClearFlags_t617,
	RuntimeInvoker_Vector3_t12_Object_t_Vector3U26_t3522,
	RuntimeInvoker_Ray_t88_Vector3_t12,
	RuntimeInvoker_Ray_t88_Object_t_Vector3U26_t3522,
	RuntimeInvoker_Object_t_Ray_t88_Single_t117_Int32_t392,
	RuntimeInvoker_Object_t_Object_t_RayU26_t3528_Single_t117_Int32_t392_Int32_t392,
	RuntimeInvoker_Object_t_Object_t_RayU26_t3528_Single_t117_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_IntPtr_t,
	RuntimeInvoker_RenderBuffer_t616,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_IntPtr_t_Int32U26_t3516_Int32U26_t3516,
	RuntimeInvoker_Void_t1449_IntPtr_t_RenderBufferU26_t3532_RenderBufferU26_t3532,
	RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Int32_t392_Int32_t392_Int32U26_t3516_Int32U26_t3516,
	RuntimeInvoker_TouchPhase_t520,
	RuntimeInvoker_Touch_t375_Int32_t392,
	RuntimeInvoker_IntPtr_t,
	RuntimeInvoker_Object_t_Object_t_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_Object_t,
	RuntimeInvoker_Void_t1449_Quaternion_t125,
	RuntimeInvoker_Void_t1449_QuaternionU26_t3527,
	RuntimeInvoker_Void_t1449_Object_t_SByte_t1443_SByte_t1443_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t393_Vector3_t12_Vector3_t12_RaycastHitU26_t3533_Single_t117_Int32_t392_Int32_t392,
	RuntimeInvoker_Boolean_t393_Ray_t88_RaycastHitU26_t3533_Single_t117_Int32_t392,
	RuntimeInvoker_Boolean_t393_Ray_t88_RaycastHitU26_t3533_Single_t117_Int32_t392_Int32_t392,
	RuntimeInvoker_Object_t_Ray_t88_Single_t117_Int32_t392_Int32_t392,
	RuntimeInvoker_Object_t_Vector3_t12_Vector3_t12_Single_t117_Int32_t392_Int32_t392,
	RuntimeInvoker_Object_t_Vector3U26_t3522_Vector3U26_t3522_Single_t117_Int32_t392_Int32_t392,
	RuntimeInvoker_Boolean_t393_Vector3U26_t3522_Vector3U26_t3522_RaycastHitU26_t3533_Single_t117_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Vector2_t171_Vector2_t171_Single_t117_Int32_t392_Single_t117_Single_t117_RaycastHit2DU26_t3534,
	RuntimeInvoker_Void_t1449_Vector2U26_t3529_Vector2U26_t3529_Single_t117_Int32_t392_Single_t117_Single_t117_RaycastHit2DU26_t3534,
	RuntimeInvoker_RaycastHit2D_t402_Vector2_t171_Vector2_t171_Single_t117_Int32_t392,
	RuntimeInvoker_RaycastHit2D_t402_Vector2_t171_Vector2_t171_Single_t117_Int32_t392_Single_t117_Single_t117,
	RuntimeInvoker_Object_t_Vector2_t171_Vector2_t171_Single_t117_Int32_t392,
	RuntimeInvoker_Object_t_Vector2U26_t3529_Vector2U26_t3529_Single_t117_Int32_t392_Single_t117_Single_t117,
	RuntimeInvoker_Object_t_SByte_t1443_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_SendMessageOptions_t465,
	RuntimeInvoker_AnimatorStateInfo_t555,
	RuntimeInvoker_AnimatorClipInfo_t556,
	RuntimeInvoker_Boolean_t393_Int16_t1444_CharacterInfoU26_t3535_Int32_t392_Int32_t392,
	RuntimeInvoker_Boolean_t393_Int16_t1444_CharacterInfoU26_t3535_Int32_t392,
	RuntimeInvoker_Boolean_t393_Int16_t1444_CharacterInfoU26_t3535,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Color_t77_Int32_t392_Single_t117_Single_t117_Int32_t392_SByte_t1443_SByte_t1443_Int32_t392_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_Int32_t392_Vector2_t171_Vector2_t171_SByte_t1443,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Color_t77_Int32_t392_Single_t117_Single_t117_Int32_t392_SByte_t1443_SByte_t1443_Int32_t392_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_Int32_t392_Single_t117_Single_t117_Single_t117_Single_t117_SByte_t1443,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t_ColorU26_t3517_Int32_t392_Single_t117_Single_t117_Int32_t392_SByte_t1443_SByte_t1443_Int32_t392_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_Int32_t392_Single_t117_Single_t117_Single_t117_Single_t117_SByte_t1443,
	RuntimeInvoker_TextGenerationSettings_t385_TextGenerationSettings_t385,
	RuntimeInvoker_Single_t117_Object_t_TextGenerationSettings_t385,
	RuntimeInvoker_Boolean_t393_Object_t_TextGenerationSettings_t385,
	RuntimeInvoker_RenderMode_t570,
	RuntimeInvoker_Void_t1449_Object_t_ColorU26_t3517,
	RuntimeInvoker_Void_t1449_Object_t_RectU26_t3524,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Vector2_t171_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Vector2U26_t3529_Object_t,
	RuntimeInvoker_Vector2_t171_Vector2_t171_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Vector2_t171_Object_t_Object_t_Vector2U26_t3529,
	RuntimeInvoker_Void_t1449_Vector2U26_t3529_Object_t_Object_t_Vector2U26_t3529,
	RuntimeInvoker_Boolean_t393_Object_t_Vector2_t171_Object_t_Vector3U26_t3522,
	RuntimeInvoker_Boolean_t393_Object_t_Vector2_t171_Object_t_Vector2U26_t3529,
	RuntimeInvoker_Ray_t88_Object_t_Vector2_t171,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Ray_t88,
	RuntimeInvoker_Void_t1449_Ray_t88,
	RuntimeInvoker_EventType_t572,
	RuntimeInvoker_EventType_t572_Int32_t392,
	RuntimeInvoker_EventModifiers_t573,
	RuntimeInvoker_KeyCode_t571,
	RuntimeInvoker_Void_t1449_DateTime_t577,
	RuntimeInvoker_Void_t1449_Rect_t94_Object_t,
	RuntimeInvoker_Void_t1449_Rect_t94_Object_t_Object_t,
	RuntimeInvoker_Boolean_t393_Rect_t94_Object_t,
	RuntimeInvoker_Rect_t94_Int32_t392_Rect_t94_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Object_t_Int32_t392_Single_t117_Single_t117_Object_t,
	RuntimeInvoker_Void_t1449_Rect_t94_Object_t_IntPtr_t,
	RuntimeInvoker_Void_t1449_RectU26_t3524_Object_t_IntPtr_t,
	RuntimeInvoker_Boolean_t393_Rect_t94_Object_t_IntPtr_t,
	RuntimeInvoker_Boolean_t393_RectU26_t3524_Object_t_IntPtr_t,
	RuntimeInvoker_Rect_t94_Int32_t392_Rect_t94_Object_t_Object_t_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_Rect_t94_Int32_t392_RectU26_t3524_Object_t_Object_t_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_Object_t_Int32_t392_SByte_t1443,
	RuntimeInvoker_Rect_t94_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Rect_t94,
	RuntimeInvoker_Void_t1449_Int32_t392_RectU26_t3524,
	RuntimeInvoker_Void_t1449_Single_t117_Single_t117_Single_t117_Single_t117_Object_t,
	RuntimeInvoker_IntPtr_t_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_SByte_t1443_Int32_t392_Object_t,
	RuntimeInvoker_UserState_t636,
	RuntimeInvoker_Void_t1449_Object_t_Double_t710_SByte_t1443_SByte_t1443_DateTime_t577,
	RuntimeInvoker_Double_t710,
	RuntimeInvoker_Void_t1449_Double_t710,
	RuntimeInvoker_DateTime_t577,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1443_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Int64_t711,
	RuntimeInvoker_Void_t1449_Object_t_Int64_t711_Object_t_DateTime_t577_Object_t_Int32_t392,
	RuntimeInvoker_Int64_t711,
	RuntimeInvoker_Void_t1449_Int64_t711,
	RuntimeInvoker_UserScope_t637,
	RuntimeInvoker_Range_t631,
	RuntimeInvoker_Void_t1449_Range_t631,
	RuntimeInvoker_TimeScope_t638,
	RuntimeInvoker_Void_t1449_Int32_t392_HitInfo_t633,
	RuntimeInvoker_Boolean_t393_HitInfo_t633_HitInfo_t633,
	RuntimeInvoker_Boolean_t393_HitInfo_t633,
	RuntimeInvoker_Void_t1449_Object_t_StringU26_t3536_StringU26_t3536,
	RuntimeInvoker_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_StreamingContext_t688,
	RuntimeInvoker_Boolean_t393_Color_t77_Color_t77,
	RuntimeInvoker_Boolean_t393_TextGenerationSettings_t385,
	RuntimeInvoker_PersistentListenerMode_t653,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_CameraDirection_t741,
	RuntimeInvoker_VideoBackgroundReflection_t829,
	RuntimeInvoker_Boolean_t393_SByte_t1443,
	RuntimeInvoker_Void_t1449_Matrix4x4_t124_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Matrix4x4_t124,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_SByte_t1443_Single_t117_Int32_t392_Object_t_Int32_t392,
	RuntimeInvoker_Void_t1449_Matrix4x4_t124_SingleU26_t3526_SingleU26_t3526,
	RuntimeInvoker_Single_t117_Matrix4x4_t124,
	RuntimeInvoker_Vector3_t12_Vector4_t350,
	RuntimeInvoker_Status_t735,
	RuntimeInvoker_VideoModeData_t742,
	RuntimeInvoker_VideoModeData_t742_Int32_t392,
	RuntimeInvoker_Boolean_t393_CameraDeviceModeU26_t3537,
	RuntimeInvoker_Boolean_t393_Int32_t392_SByte_t1443,
	RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t12_Quaternion_t125_Vector3_t12_Int32_t392,
	RuntimeInvoker_Boolean_t393_Object_t_Vector3_t12_Vector3_t12,
	RuntimeInvoker_Boolean_t393_Object_t_Vector3_t12_Vector3_t12_Vector3_t12_Quaternion_t125,
	RuntimeInvoker_Object_t_Vector3U26_t3522_Vector3U26_t3522,
	RuntimeInvoker_Boolean_t393_IntPtr_t_Object_t_Vector3_t12_Vector3_t12_Vector3_t12_Quaternion_t125,
	RuntimeInvoker_Matrix4x4_t124_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Boolean_t393_Single_t117,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392,
	RuntimeInvoker_Boolean_t393_Object_t_SByte_t1443,
	RuntimeInvoker_Void_t1449_Vector2_t171_Vector2_t171_Single_t117,
	RuntimeInvoker_Void_t1449_Vector3_t12_Vector3_t12_Single_t117,
	RuntimeInvoker_Void_t1449_TargetSearchResult_t884,
	RuntimeInvoker_ImageTargetType_t773,
	RuntimeInvoker_Object_t_Object_t_RectangleData_t766,
	RuntimeInvoker_Boolean_t393_Object_t_Single_t117,
	RuntimeInvoker_FrameQuality_t775,
	RuntimeInvoker_Boolean_t393_Int32_t392_VirtualButtonAbstractBehaviourU26_t3538,
	RuntimeInvoker_WordFilterMode_t919,
	RuntimeInvoker_WordPrefabCreationMode_t851,
	RuntimeInvoker_Sensitivity_t894,
	RuntimeInvoker_Boolean_t393_Matrix4x4_t124,
	RuntimeInvoker_WordTemplateMode_t782,
	RuntimeInvoker_PIXEL_FORMAT_t783,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Single_t117_Int32_t392,
	RuntimeInvoker_Object_t_Int32_t392_Object_t_Single_t117,
	RuntimeInvoker_Int32_t392_Int32_t392_Object_t_Single_t117,
	RuntimeInvoker_Void_t1449_Int32_t392_Vec2I_t831,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t,
	RuntimeInvoker_ProfileCollection_t899_Object_t,
	RuntimeInvoker_WorldCenterMode_t909,
	RuntimeInvoker_Void_t1449_FrameState_t821,
	RuntimeInvoker_Void_t1449_FrameState_t821_Object_t,
	RuntimeInvoker_Boolean_t393_TrackableResultData_t810,
	RuntimeInvoker_VideoBGCfgData_t830,
	RuntimeInvoker_VideoTextureInfo_t728,
	RuntimeInvoker_Void_t1449_VideoBGCfgData_t830,
	RuntimeInvoker_Boolean_t393_Int32_t392_Int32_t392,
	RuntimeInvoker_Matrix4x4_t124_Single_t117_Single_t117_Int32_t392,
	RuntimeInvoker_Void_t1449_PoseData_t809,
	RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Object_t_Object_t,
	RuntimeInvoker_OrientedBoundingBox3D_t768,
	RuntimeInvoker_Void_t1449_OrientedBoundingBox3D_t768,
	RuntimeInvoker_UInt16_t1430_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Vec2I_t831,
	RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Vector2_t171,
	RuntimeInvoker_Void_t1449_Vector3_t12_Quaternion_t125,
	RuntimeInvoker_Void_t1449_OrientedBoundingBox_t767,
	RuntimeInvoker_Void_t1449_Int32_t392_IntPtr_t,
	RuntimeInvoker_Boolean_t393_IntPtr_t,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_IntPtr_t,
	RuntimeInvoker_Int32_t392_Int32_t392_IntPtr_t,
	RuntimeInvoker_Int32_t392_Int32_t392_IntPtr_t_Int32_t392_IntPtr_t,
	RuntimeInvoker_Int32_t392_IntPtr_t_Int32_t392_Object_t_Int32_t392,
	RuntimeInvoker_Int32_t392_IntPtr_t_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Single_t117,
	RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Object_t_IntPtr_t,
	RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_IntPtr_t_Object_t,
	RuntimeInvoker_Int32_t392_IntPtr_t_IntPtr_t_Int32_t392_IntPtr_t_Object_t,
	RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Int32_t392_Object_t_Int32_t392,
	RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_IntPtr_t,
	RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Single_t117,
	RuntimeInvoker_Int32_t392_IntPtr_t,
	RuntimeInvoker_Int32_t392_IntPtr_t_Int32_t392_IntPtr_t_Int32_t392,
	RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Single_t117_Single_t117_IntPtr_t_Int32_t392,
	RuntimeInvoker_Boolean_t393_IntPtr_t_IntPtr_t_Int32_t392_IntPtr_t_IntPtr_t_IntPtr_t_IntPtr_t_Single_t117,
	RuntimeInvoker_Boolean_t393_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Void_t1449_IntPtr_t_Single_t117,
	RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392,
	RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Object_t_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_IntPtr_t_Int32_t392,
	RuntimeInvoker_Int32_t392_SByte_t1443,
	RuntimeInvoker_Void_t1449_SmartTerrainInitializationInfo_t760,
	RuntimeInvoker_Boolean_t393_Object_t_PropAbstractBehaviourU26_t3539,
	RuntimeInvoker_Boolean_t393_Object_t_SurfaceAbstractBehaviourU26_t3540,
	RuntimeInvoker_Object_t_MeshData_t817_Object_t_SByte_t1443,
	RuntimeInvoker_Object_t_Int32_t392_IntPtr_t,
	RuntimeInvoker_Void_t1449_LinkedList_1U26_t3541,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_PoseData_t809,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_PoseData_t809,
	RuntimeInvoker_InitState_t881,
	RuntimeInvoker_UpdateState_t882,
	RuntimeInvoker_UpdateState_t882_Int32_t392,
	RuntimeInvoker_RectangleData_t766,
	RuntimeInvoker_Boolean_t393_RectangleData_t766,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_RectangleData_t766_Object_t_Object_t,
	RuntimeInvoker_Vec2I_t831,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Object_t_SByte_t1443,
	RuntimeInvoker_ProfileData_t898,
	RuntimeInvoker_ProfileData_t898_Object_t,
	RuntimeInvoker_Object_t_Object_t_Vector2_t171_Vector2_t171,
	RuntimeInvoker_Object_t_Object_t_Vector2_t171_Object_t,
	RuntimeInvoker_CameraDeviceMode_t740,
	RuntimeInvoker_Boolean_t393_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Vector2_t171_Vector2_t171_Rect_t94_SByte_t1443_VideoModeData_t742,
	RuntimeInvoker_OrientedBoundingBox_t767_OrientedBoundingBox_t767_Rect_t94_SByte_t1443_VideoModeData_t742,
	RuntimeInvoker_Void_t1449_SByte_t1443_SingleU26_t3526_SingleU26_t3526_SingleU26_t3526_SingleU26_t3526_BooleanU26_t3515,
	RuntimeInvoker_Boolean_t393_Vector2U26_t3529_Vector2U26_t3529,
	RuntimeInvoker_Boolean_t393_Vector2_t171_Vector2_t171_Single_t117,
	RuntimeInvoker_Void_t1449_Object_t_SByte_t1443_Object_t,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_SByte_t1443_Object_t_Object_t,
	RuntimeInvoker_UInt32_t712_Int32_t392,
	RuntimeInvoker_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_UInt32_t712_Object_t_Int32_t392,
	RuntimeInvoker_Sign_t1090_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_ConfidenceFactor_t1094,
	RuntimeInvoker_Void_t1449_SByte_t1443_Object_t,
	RuntimeInvoker_Byte_t699,
	RuntimeInvoker_Void_t1449_Object_t_Int32U26_t3516_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Int32U26_t3516_ByteU26_t3542_Int32U26_t3516_ByteU5BU5DU26_t3543,
	RuntimeInvoker_DateTime_t577_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t392,
	RuntimeInvoker_Object_t_Object_t_DSAParameters_t1229,
	RuntimeInvoker_RSAParameters_t1201_SByte_t1443,
	RuntimeInvoker_Void_t1449_RSAParameters_t1201,
	RuntimeInvoker_Object_t_SByte_t1443,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_DSAParameters_t1229_BooleanU26_t3515,
	RuntimeInvoker_Object_t_Object_t_SByte_t1443_Object_t_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_Boolean_t393_DateTime_t577,
	RuntimeInvoker_X509ChainStatusFlags_t1131,
	RuntimeInvoker_Void_t1449_Byte_t699,
	RuntimeInvoker_Void_t1449_Byte_t699_Byte_t699,
	RuntimeInvoker_AlertLevel_t1150,
	RuntimeInvoker_AlertDescription_t1151,
	RuntimeInvoker_Object_t_Byte_t699,
	RuntimeInvoker_Void_t1449_Int16_t1444_Object_t_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_Int16_t1444_SByte_t1443_SByte_t1443,
	RuntimeInvoker_CipherAlgorithmType_t1153,
	RuntimeInvoker_HashAlgorithmType_t1172,
	RuntimeInvoker_ExchangeAlgorithmType_t1170,
	RuntimeInvoker_CipherMode_t1081,
	RuntimeInvoker_Int16_t1444,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int16_t1444,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int64_t711,
	RuntimeInvoker_Void_t1449_Object_t_ByteU5BU5DU26_t3543_ByteU5BU5DU26_t3543,
	RuntimeInvoker_Object_t_Byte_t699_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t392,
	RuntimeInvoker_Object_t_Int16_t1444,
	RuntimeInvoker_Int32_t392_Int16_t1444,
	RuntimeInvoker_Object_t_Int16_t1444_Object_t_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_Int16_t1444_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_SecurityProtocolType_t1187,
	RuntimeInvoker_SecurityCompressionType_t1186,
	RuntimeInvoker_HandshakeType_t1204,
	RuntimeInvoker_HandshakeState_t1171,
	RuntimeInvoker_UInt64_t1442,
	RuntimeInvoker_SecurityProtocolType_t1187_Int16_t1444,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t699_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t699_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Byte_t699_Object_t,
	RuntimeInvoker_Object_t_Byte_t699_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_SByte_t1443_Int32_t392,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Int64_t711_Int64_t711_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_Byte_t699_Byte_t699_Object_t,
	RuntimeInvoker_RSAParameters_t1201,
	RuntimeInvoker_Void_t1449_Object_t_Byte_t699,
	RuntimeInvoker_Void_t1449_Object_t_Byte_t699_Byte_t699,
	RuntimeInvoker_Void_t1449_Object_t_Byte_t699_Object_t,
	RuntimeInvoker_ContentType_t1165,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t3544,
	RuntimeInvoker_DictionaryEntry_t1423,
	RuntimeInvoker_EditorBrowsableState_t1288,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t_Int32_t392,
	RuntimeInvoker_Int16_t1444_Int16_t1444,
	RuntimeInvoker_Boolean_t393_Object_t_IPAddressU26_t3545,
	RuntimeInvoker_AddressFamily_t1293,
	RuntimeInvoker_Object_t_Int64_t711,
	RuntimeInvoker_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Boolean_t393_Object_t_Int32U26_t3516,
	RuntimeInvoker_Boolean_t393_Object_t_IPv6AddressU26_t3546,
	RuntimeInvoker_UInt16_t1430_Int16_t1444,
	RuntimeInvoker_SecurityProtocolType_t1308,
	RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_Int32_t392_SByte_t1443,
	RuntimeInvoker_AsnDecodeStatus_t1348_Object_t,
	RuntimeInvoker_Object_t_Int32_t392_Object_t_SByte_t1443,
	RuntimeInvoker_X509ChainStatusFlags_t1336_Object_t,
	RuntimeInvoker_X509ChainStatusFlags_t1336_Object_t_Int32_t392_SByte_t1443,
	RuntimeInvoker_X509ChainStatusFlags_t1336_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_X509ChainStatusFlags_t1336,
	RuntimeInvoker_Void_t1449_Object_t_Int32U26_t3516_Int32_t392_Int32_t392,
	RuntimeInvoker_X509RevocationFlag_t1343,
	RuntimeInvoker_X509RevocationMode_t1344,
	RuntimeInvoker_X509VerificationFlags_t1347,
	RuntimeInvoker_X509KeyUsageFlags_t1341,
	RuntimeInvoker_X509KeyUsageFlags_t1341_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_SByte_t1443,
	RuntimeInvoker_Byte_t699_Int16_t1444,
	RuntimeInvoker_Byte_t699_Int16_t1444_Int16_t1444,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t392_Int32_t392_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_RegexOptions_t1360,
	RuntimeInvoker_Category_t1367_Object_t,
	RuntimeInvoker_Boolean_t393_UInt16_t1430_Int16_t1444,
	RuntimeInvoker_Boolean_t393_Int32_t392_Int16_t1444,
	RuntimeInvoker_Void_t1449_Int16_t1444_SByte_t1443_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_UInt16_t1430_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_Int16_t1444_Int16_t1444_SByte_t1443_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_Int16_t1444_Object_t_SByte_t1443_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_UInt16_t1430,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_SByte_t1443_Object_t,
	RuntimeInvoker_Void_t1449_Int32_t392_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_SByte_t1443_Int32_t392_Object_t,
	RuntimeInvoker_UInt16_t1430_UInt16_t1430_UInt16_t1430,
	RuntimeInvoker_OpFlags_t1362_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_UInt16_t1430_UInt16_t1430,
	RuntimeInvoker_Boolean_t393_Int32_t392_Int32U26_t3516_Int32_t392,
	RuntimeInvoker_Boolean_t393_Int32_t392_Int32U26_t3516_Int32U26_t3516_SByte_t1443,
	RuntimeInvoker_Boolean_t393_Int32U26_t3516_Int32_t392,
	RuntimeInvoker_Boolean_t393_UInt16_t1430_Int32_t392,
	RuntimeInvoker_Boolean_t393_Int32_t392_Int32_t392_SByte_t1443_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32U26_t3516_Int32U26_t3516,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_SByte_t1443_Int32_t392,
	RuntimeInvoker_Interval_t1382,
	RuntimeInvoker_Boolean_t393_Interval_t1382,
	RuntimeInvoker_Void_t1449_Interval_t1382,
	RuntimeInvoker_Interval_t1382_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_Double_t710_Interval_t1382,
	RuntimeInvoker_Object_t_Interval_t1382_Object_t_Object_t,
	RuntimeInvoker_Double_t710_Object_t,
	RuntimeInvoker_Int32_t392_Object_t_Int32U26_t3516,
	RuntimeInvoker_Int32_t392_Object_t_Int32U26_t3516_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Int32U26_t3516_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Object_t_Object_t_Int32U26_t3516,
	RuntimeInvoker_Object_t_RegexOptionsU26_t3547,
	RuntimeInvoker_Void_t1449_RegexOptionsU26_t3547_SByte_t1443,
	RuntimeInvoker_Boolean_t393_Int32U26_t3516_Int32U26_t3516_Int32_t392,
	RuntimeInvoker_Category_t1367,
	RuntimeInvoker_Int32_t392_Int16_t1444_Int32_t392_Int32_t392,
	RuntimeInvoker_Char_t424_Int16_t1444,
	RuntimeInvoker_Int32_t392_Int32U26_t3516,
	RuntimeInvoker_Void_t1449_Int32U26_t3516_Int32U26_t3516,
	RuntimeInvoker_Void_t1449_Int32U26_t3516_Int32U26_t3516_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_UInt16_t1430_SByte_t1443,
	RuntimeInvoker_Void_t1449_Int16_t1444_Int16_t1444,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Object_t_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_UInt16_t1430,
	RuntimeInvoker_Position_t1363,
	RuntimeInvoker_UriHostNameType_t1415_Object_t,
	RuntimeInvoker_Void_t1449_StringU26_t3536,
	RuntimeInvoker_Object_t_Object_t_SByte_t1443_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Char_t424_Object_t_Int32U26_t3516_CharU26_t3548,
	RuntimeInvoker_Void_t1449_Object_t_UriFormatExceptionU26_t3549,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_ObjectU5BU5DU26_t3550,
	RuntimeInvoker_Int32_t392_Object_t_ObjectU5BU5DU26_t3550,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_Byte_t699_Object_t,
	RuntimeInvoker_Decimal_t1445_Object_t,
	RuntimeInvoker_Int16_t1444_Object_t,
	RuntimeInvoker_Int64_t711_Object_t,
	RuntimeInvoker_SByte_t1443_Object_t,
	RuntimeInvoker_UInt32_t712_Object_t,
	RuntimeInvoker_UInt64_t1442_Object_t,
	RuntimeInvoker_Boolean_t393_SByte_t1443_Object_t_Int32_t392_ExceptionU26_t3551,
	RuntimeInvoker_Boolean_t393_Object_t_SByte_t1443_Int32U26_t3516_ExceptionU26_t3551,
	RuntimeInvoker_Boolean_t393_Int32_t392_SByte_t1443_ExceptionU26_t3551,
	RuntimeInvoker_Boolean_t393_Int32U26_t3516_Object_t_SByte_t1443_SByte_t1443_ExceptionU26_t3551,
	RuntimeInvoker_Void_t1449_Int32U26_t3516_Object_t_Object_t_BooleanU26_t3515_BooleanU26_t3515,
	RuntimeInvoker_Void_t1449_Int32U26_t3516_Object_t_Object_t_BooleanU26_t3515,
	RuntimeInvoker_Boolean_t393_Int32U26_t3516_Object_t_Int32U26_t3516_SByte_t1443_ExceptionU26_t3551,
	RuntimeInvoker_Boolean_t393_Int32U26_t3516_Object_t_Object_t,
	RuntimeInvoker_Boolean_t393_Int16_t1444_SByte_t1443,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_SByte_t1443_Int32U26_t3516_ExceptionU26_t3551,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_Int32U26_t3516,
	RuntimeInvoker_TypeCode_t2067,
	RuntimeInvoker_Int32_t392_Int64_t711,
	RuntimeInvoker_Boolean_t393_Int64_t711,
	RuntimeInvoker_Boolean_t393_Object_t_SByte_t1443_Int64U26_t3552_ExceptionU26_t3551,
	RuntimeInvoker_Int64_t711_Object_t_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_SByte_t1443_Int64U26_t3552_ExceptionU26_t3551,
	RuntimeInvoker_Int64_t711_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Int64U26_t3552,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_Int64U26_t3552,
	RuntimeInvoker_Boolean_t393_Object_t_SByte_t1443_UInt32U26_t3553_ExceptionU26_t3551,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_SByte_t1443_UInt32U26_t3553_ExceptionU26_t3551,
	RuntimeInvoker_UInt32_t712_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_UInt32_t712_Object_t_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_UInt32U26_t3553,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_UInt32U26_t3553,
	RuntimeInvoker_UInt64_t1442_Object_t_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_SByte_t1443_UInt64U26_t3554_ExceptionU26_t3551,
	RuntimeInvoker_UInt64_t1442_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_UInt64U26_t3554,
	RuntimeInvoker_Byte_t699_Object_t_Object_t,
	RuntimeInvoker_Byte_t699_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_ByteU26_t3542,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_ByteU26_t3542,
	RuntimeInvoker_Boolean_t393_Object_t_SByte_t1443_SByteU26_t3555_ExceptionU26_t3551,
	RuntimeInvoker_SByte_t1443_Object_t_Object_t,
	RuntimeInvoker_SByte_t1443_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_SByteU26_t3555,
	RuntimeInvoker_Boolean_t393_Object_t_SByte_t1443_Int16U26_t3556_ExceptionU26_t3551,
	RuntimeInvoker_Int16_t1444_Object_t_Object_t,
	RuntimeInvoker_Int16_t1444_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Int16U26_t3556,
	RuntimeInvoker_UInt16_t1430_Object_t_Object_t,
	RuntimeInvoker_UInt16_t1430_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_UInt16U26_t3557,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_UInt16U26_t3557,
	RuntimeInvoker_Void_t1449_ByteU2AU26_t3558_ByteU2AU26_t3558_DoubleU2AU26_t3559_UInt16U2AU26_t3560_UInt16U2AU26_t3560_UInt16U2AU26_t3560_UInt16U2AU26_t3560,
	RuntimeInvoker_UnicodeCategory_t1593_Int16_t1444,
	RuntimeInvoker_Char_t424_Int16_t1444_Object_t,
	RuntimeInvoker_Void_t1449_Int16_t1444_Int32_t392,
	RuntimeInvoker_Char_t424_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Object_t,
	RuntimeInvoker_Int32_t392_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_Object_t_SByte_t1443_Object_t,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t_Int32_t392_Int32_t392_SByte_t1443_Object_t,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Int16_t1444_Int32_t392,
	RuntimeInvoker_Object_t_Int32_t392_Int16_t1444,
	RuntimeInvoker_Object_t_Int16_t1444_Int16_t1444,
	RuntimeInvoker_Void_t1449_Object_t_Int32U26_t3516_Int32U26_t3516_Int32U26_t3516_BooleanU26_t3515_StringU26_t3536,
	RuntimeInvoker_Void_t1449_Int32_t392_Int16_t1444,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392_Object_t,
	RuntimeInvoker_Object_t_Int16_t1444_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Single_t117_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_Double_t710,
	RuntimeInvoker_Boolean_t393_Double_t710,
	RuntimeInvoker_Double_t710_Object_t_Object_t,
	RuntimeInvoker_Double_t710_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_SByte_t1443_DoubleU26_t3561_ExceptionU26_t3551,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Boolean_t393_Object_t_DoubleU26_t3561,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Object_t_Decimal_t1445,
	RuntimeInvoker_Decimal_t1445_Decimal_t1445_Decimal_t1445,
	RuntimeInvoker_UInt64_t1442_Decimal_t1445,
	RuntimeInvoker_Int64_t711_Decimal_t1445,
	RuntimeInvoker_Boolean_t393_Decimal_t1445_Decimal_t1445,
	RuntimeInvoker_Decimal_t1445_Decimal_t1445,
	RuntimeInvoker_Int32_t392_Decimal_t1445_Decimal_t1445,
	RuntimeInvoker_Int32_t392_Decimal_t1445,
	RuntimeInvoker_Boolean_t393_Decimal_t1445,
	RuntimeInvoker_Decimal_t1445_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t_Int32U26_t3516_BooleanU26_t3515_BooleanU26_t3515_Int32U26_t3516_SByte_t1443,
	RuntimeInvoker_Decimal_t1445_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_DecimalU26_t3562_SByte_t1443,
	RuntimeInvoker_Int32_t392_DecimalU26_t3562_UInt64U26_t3554,
	RuntimeInvoker_Int32_t392_DecimalU26_t3562_Int64U26_t3552,
	RuntimeInvoker_Int32_t392_DecimalU26_t3562_DecimalU26_t3562,
	RuntimeInvoker_Int32_t392_DecimalU26_t3562_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_DecimalU26_t3562_Int32_t392,
	RuntimeInvoker_Double_t710_DecimalU26_t3562,
	RuntimeInvoker_Void_t1449_DecimalU26_t3562_Int32_t392,
	RuntimeInvoker_Int32_t392_DecimalU26_t3562_DecimalU26_t3562_DecimalU26_t3562,
	RuntimeInvoker_Byte_t699_Decimal_t1445,
	RuntimeInvoker_SByte_t1443_Decimal_t1445,
	RuntimeInvoker_Int16_t1444_Decimal_t1445,
	RuntimeInvoker_UInt16_t1430_Decimal_t1445,
	RuntimeInvoker_UInt32_t712_Decimal_t1445,
	RuntimeInvoker_Decimal_t1445_SByte_t1443,
	RuntimeInvoker_Decimal_t1445_Int16_t1444,
	RuntimeInvoker_Decimal_t1445_Int32_t392,
	RuntimeInvoker_Decimal_t1445_Int64_t711,
	RuntimeInvoker_Decimal_t1445_Single_t117,
	RuntimeInvoker_Decimal_t1445_Double_t710,
	RuntimeInvoker_Single_t117_Decimal_t1445,
	RuntimeInvoker_Double_t710_Decimal_t1445,
	RuntimeInvoker_IntPtr_t_Int64_t711,
	RuntimeInvoker_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_IntPtr_t,
	RuntimeInvoker_UInt32_t712,
	RuntimeInvoker_UInt64_t1442_IntPtr_t,
	RuntimeInvoker_UInt32_t712_IntPtr_t,
	RuntimeInvoker_UIntPtr_t_Int64_t711,
	RuntimeInvoker_UIntPtr_t_Object_t,
	RuntimeInvoker_UIntPtr_t_Int32_t392,
	RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t3563,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t392_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_UInt64_t1442_Object_t_Int32_t392,
	RuntimeInvoker_Object_t_Object_t_Int16_t1444,
	RuntimeInvoker_Object_t_Object_t_Int64_t711,
	RuntimeInvoker_Int64_t711_Int32_t392,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Object_t_Int64_t711_Int64_t711,
	RuntimeInvoker_Object_t_Int64_t711_Int64_t711_Int64_t711,
	RuntimeInvoker_Void_t1449_Object_t_Int64_t711_Int64_t711,
	RuntimeInvoker_Void_t1449_Object_t_Int64_t711_Int64_t711_Int64_t711,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Int64_t711_Object_t_Int64_t711_Int64_t711,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Int64_t711,
	RuntimeInvoker_Int32_t392_Object_t_Object_t_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392_Int32_t392_Object_t,
	RuntimeInvoker_Object_t_Int32_t392_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_TypeAttributes_t1717,
	RuntimeInvoker_MemberTypes_t1697,
	RuntimeInvoker_RuntimeTypeHandle_t1450,
	RuntimeInvoker_Object_t_Object_t_SByte_t1443_SByte_t1443,
	RuntimeInvoker_TypeCode_t2067_Object_t,
	RuntimeInvoker_Object_t_RuntimeTypeHandle_t1450,
	RuntimeInvoker_RuntimeTypeHandle_t1450_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t392_Object_t_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t392_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_RuntimeFieldHandle_t1452,
	RuntimeInvoker_Void_t1449_IntPtr_t_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_ContractionU5BU5DU26_t3564_Level2MapU5BU5DU26_t3565,
	RuntimeInvoker_Void_t1449_Object_t_CodePointIndexerU26_t3566_ByteU2AU26_t3558_ByteU2AU26_t3558_CodePointIndexerU26_t3566_ByteU2AU26_t3558,
	RuntimeInvoker_Byte_t699_Int32_t392,
	RuntimeInvoker_Byte_t699_Int32_t392_Int32_t392,
	RuntimeInvoker_ExtenderType_t1494_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Object_t_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392_BooleanU26_t3515_BooleanU26_t3515_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32_t392_BooleanU26_t3515_BooleanU26_t3515_SByte_t1443_SByte_t1443_ContextU26_t3567,
	RuntimeInvoker_Int32_t392_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Int32_t392,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Int32_t392_Int32_t392_SByte_t1443_ContextU26_t3567,
	RuntimeInvoker_Int32_t392_Object_t_Object_t_Int32_t392_Int32_t392_BooleanU26_t3515,
	RuntimeInvoker_Int32_t392_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int16_t1444_Int32_t392_SByte_t1443_ContextU26_t3567,
	RuntimeInvoker_Int32_t392_Object_t_Object_t_Int32_t392_Int32_t392_Object_t_ContextU26_t3567,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392_Object_t_Int32_t392_SByte_t1443_ContextU26_t3567,
	RuntimeInvoker_Boolean_t393_Object_t_Int32U26_t3516_Int32_t392_Int32_t392_Object_t_SByte_t1443_ContextU26_t3567,
	RuntimeInvoker_Boolean_t393_Object_t_Int32U26_t3516_Int32_t392_Int32_t392_Object_t_SByte_t1443_Int32_t392_ContractionU26_t3568_ContextU26_t3567,
	RuntimeInvoker_Boolean_t393_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_SByte_t1443,
	RuntimeInvoker_Boolean_t393_Object_t_Int32U26_t3516_Int32_t392_Int32_t392_Int32_t392_Object_t_SByte_t1443_ContextU26_t3567,
	RuntimeInvoker_Boolean_t393_Object_t_Int32U26_t3516_Int32_t392_Int32_t392_Int32_t392_Object_t_SByte_t1443_Int32_t392_ContractionU26_t3568_ContextU26_t3567,
	RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Object_t_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Object_t_SByte_t1443,
	RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_SByte_t1443_ByteU5BU5DU26_t3543_Int32U26_t3516,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_SByte_t1443,
	RuntimeInvoker_ConfidenceFactor_t1503,
	RuntimeInvoker_Sign_t1505_Object_t_Object_t,
	RuntimeInvoker_DSAParameters_t1229_SByte_t1443,
	RuntimeInvoker_Void_t1449_DSAParameters_t1229,
	RuntimeInvoker_Int16_t1444_Object_t_Int32_t392,
	RuntimeInvoker_Double_t710_Object_t_Int32_t392,
	RuntimeInvoker_Object_t_Int16_t1444_SByte_t1443,
	RuntimeInvoker_Void_t1449_Int32_t392_Single_t117_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Single_t117_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Int32_t392_Single_t117_Object_t,
	RuntimeInvoker_Boolean_t393_Int32_t392_SByte_t1443_MethodBaseU26_t3569_Int32U26_t3516_Int32U26_t3516_StringU26_t3536_Int32U26_t3516_Int32U26_t3516,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_SByte_t1443,
	RuntimeInvoker_Int32_t392_DateTime_t577,
	RuntimeInvoker_DayOfWeek_t2016_DateTime_t577,
	RuntimeInvoker_Int32_t392_Int32U26_t3516_Int32_t392_Int32_t392,
	RuntimeInvoker_DayOfWeek_t2016_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32U26_t3516_Int32U26_t3516_Int32U26_t3516_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_SByte_t1443,
	RuntimeInvoker_Void_t1449_DateTime_t577_DateTime_t577_TimeSpan_t975,
	RuntimeInvoker_TimeSpan_t975,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32U26_t3516,
	RuntimeInvoker_Decimal_t1445,
	RuntimeInvoker_SByte_t1443,
	RuntimeInvoker_UInt16_t1430,
	RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_SByte_t1443_Int32_t392_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_Int32_t392,
	RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Boolean_t393_Object_t_MonoIOErrorU26_t3570,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t392_Int32_t392_MonoIOErrorU26_t3570,
	RuntimeInvoker_Object_t_MonoIOErrorU26_t3570,
	RuntimeInvoker_FileAttributes_t1603_Object_t_MonoIOErrorU26_t3570,
	RuntimeInvoker_MonoFileType_t1612_IntPtr_t_MonoIOErrorU26_t3570,
	RuntimeInvoker_Boolean_t393_Object_t_MonoIOStatU26_t3571_MonoIOErrorU26_t3570,
	RuntimeInvoker_IntPtr_t_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_MonoIOErrorU26_t3570,
	RuntimeInvoker_Boolean_t393_IntPtr_t_MonoIOErrorU26_t3570,
	RuntimeInvoker_Int32_t392_IntPtr_t_Object_t_Int32_t392_Int32_t392_MonoIOErrorU26_t3570,
	RuntimeInvoker_Int64_t711_IntPtr_t_Int64_t711_Int32_t392_MonoIOErrorU26_t3570,
	RuntimeInvoker_Int64_t711_IntPtr_t_MonoIOErrorU26_t3570,
	RuntimeInvoker_Boolean_t393_IntPtr_t_Int64_t711_MonoIOErrorU26_t3570,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_Int32_t392_Object_t_Object_t_Object_t,
	RuntimeInvoker_CallingConventions_t1687,
	RuntimeInvoker_RuntimeMethodHandle_t2059,
	RuntimeInvoker_MethodAttributes_t1698,
	RuntimeInvoker_MethodToken_t1654,
	RuntimeInvoker_FieldAttributes_t1695,
	RuntimeInvoker_RuntimeFieldHandle_t1452,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_OpCode_t1658,
	RuntimeInvoker_Void_t1449_OpCode_t1658_Object_t,
	RuntimeInvoker_StackBehaviour_t1662,
	RuntimeInvoker_Object_t_Int32_t392_Int32_t392_Object_t,
	RuntimeInvoker_Object_t_Int32_t392_Int32_t392_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_SByte_t1443_Object_t,
	RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t3516_ModuleU26_t3572,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1443_SByte_t1443,
	RuntimeInvoker_AssemblyNameFlags_t1681,
	RuntimeInvoker_Object_t_Int32_t392_Object_t_ObjectU5BU5DU26_t3550_Object_t_Object_t_Object_t_ObjectU26_t3573,
	RuntimeInvoker_Void_t1449_ObjectU5BU5DU26_t3550_Object_t,
	RuntimeInvoker_Object_t_Int32_t392_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_ObjectU5BU5DU26_t3550_Object_t,
	RuntimeInvoker_Object_t_Int32_t392_Object_t_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_EventAttributes_t1693,
	RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Object_t_RuntimeFieldHandle_t1452,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Object_t_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_Object_t_StreamingContext_t688,
	RuntimeInvoker_Object_t_RuntimeMethodHandle_t2059,
	RuntimeInvoker_Void_t1449_Object_t_MonoEventInfoU26_t3574,
	RuntimeInvoker_MonoEventInfo_t1702_Object_t,
	RuntimeInvoker_Void_t1449_IntPtr_t_MonoMethodInfoU26_t3575,
	RuntimeInvoker_MonoMethodInfo_t1705_IntPtr_t,
	RuntimeInvoker_MethodAttributes_t1698_IntPtr_t,
	RuntimeInvoker_CallingConventions_t1687_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t3551,
	RuntimeInvoker_Void_t1449_Object_t_MonoPropertyInfoU26_t3576_Int32_t392,
	RuntimeInvoker_PropertyAttributes_t1713,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Int32_t392_Object_t_Object_t_Object_t,
	RuntimeInvoker_ParameterAttributes_t1709,
	RuntimeInvoker_Void_t1449_Int64_t711_ResourceInfoU26_t3577,
	RuntimeInvoker_Void_t1449_Object_t_Int64_t711_Int32_t392,
	RuntimeInvoker_GCHandle_t965_Object_t_Int32_t392,
	RuntimeInvoker_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Void_t1449_Object_t_Int32_t392_IntPtr_t_Int32_t392,
	RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_Object_t_Int32_t392,
	RuntimeInvoker_Void_t1449_IntPtr_t_Object_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Byte_t699_IntPtr_t_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_IntPtr_t_SByte_t1443,
	RuntimeInvoker_Void_t1449_IntPtr_t_Int32_t392_SByte_t1443,
	RuntimeInvoker_Void_t1449_BooleanU26_t3515,
	RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t3536,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t3536,
	RuntimeInvoker_Void_t1449_SByte_t1443_Object_t_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_TimeSpan_t975,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_SByte_t1443_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t688_Object_t,
	RuntimeInvoker_Object_t_Object_t_StreamingContext_t688_ISurrogateSelectorU26_t3578,
	RuntimeInvoker_Void_t1449_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_TimeSpan_t975_Object_t,
	RuntimeInvoker_Object_t_StringU26_t3536,
	RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t3573,
	RuntimeInvoker_Boolean_t393_Object_t_StringU26_t3536_StringU26_t3536,
	RuntimeInvoker_WellKnownObjectMode_t1854,
	RuntimeInvoker_StreamingContext_t688,
	RuntimeInvoker_TypeFilterLevel_t1870,
	RuntimeInvoker_Void_t1449_Object_t_BooleanU26_t3515,
	RuntimeInvoker_Object_t_Byte_t699_Object_t_SByte_t1443_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t699_Object_t_SByte_t1443_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_SByte_t1443_ObjectU26_t3573_HeaderU5BU5DU26_t3579,
	RuntimeInvoker_Void_t1449_Byte_t699_Object_t_SByte_t1443_ObjectU26_t3573_HeaderU5BU5DU26_t3579,
	RuntimeInvoker_Boolean_t393_Byte_t699_Object_t,
	RuntimeInvoker_Void_t1449_Byte_t699_Object_t_Int64U26_t3552_ObjectU26_t3573_SerializationInfoU26_t3580,
	RuntimeInvoker_Void_t1449_Object_t_SByte_t1443_SByte_t1443_Int64U26_t3552_ObjectU26_t3573_SerializationInfoU26_t3580,
	RuntimeInvoker_Void_t1449_Object_t_Int64U26_t3552_ObjectU26_t3573_SerializationInfoU26_t3580,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Int64_t711_ObjectU26_t3573_SerializationInfoU26_t3580,
	RuntimeInvoker_Void_t1449_Int64_t711_Object_t_Object_t_Int64_t711_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Int64U26_t3552_ObjectU26_t3573,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Int64U26_t3552_ObjectU26_t3573,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Int64_t711_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Int64_t711_Int64_t711_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int64_t711_Object_t,
	RuntimeInvoker_Object_t_Object_t_Byte_t699,
	RuntimeInvoker_Void_t1449_Int64_t711_Int32_t392_Int64_t711,
	RuntimeInvoker_Void_t1449_Int64_t711_Object_t_Int64_t711,
	RuntimeInvoker_Void_t1449_Object_t_Int64_t711_Object_t_Int64_t711_Object_t_Object_t,
	RuntimeInvoker_Boolean_t393_SByte_t1443_Object_t_SByte_t1443,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_StreamingContext_t688,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_StreamingContext_t688,
	RuntimeInvoker_Void_t1449_StreamingContext_t688,
	RuntimeInvoker_Object_t_StreamingContext_t688_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_Int16_t1444,
	RuntimeInvoker_Void_t1449_Object_t_DateTime_t577,
	RuntimeInvoker_SerializationEntry_t1887,
	RuntimeInvoker_StreamingContextStates_t1890,
	RuntimeInvoker_CspProviderFlags_t1894,
	RuntimeInvoker_UInt32_t712_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Object_t_SByte_t1443,
	RuntimeInvoker_Void_t1449_Int64_t711_Object_t_Int32_t392,
	RuntimeInvoker_UInt32_t712_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_UInt32U26_t3553_Int32_t392_UInt32U26_t3553_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t,
	RuntimeInvoker_Void_t1449_Int64_t711_Int64_t711,
	RuntimeInvoker_UInt64_t1442_Int64_t711_Int32_t392,
	RuntimeInvoker_UInt64_t1442_Int64_t711_Int64_t711_Int64_t711,
	RuntimeInvoker_UInt64_t1442_Int64_t711,
	RuntimeInvoker_PaddingMode_t1085,
	RuntimeInvoker_Void_t1449_StringBuilderU26_t3581_Int32_t392,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_EncoderFallbackBufferU26_t3582_CharU5BU5DU26_t3583,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_DecoderFallbackBufferU26_t3584,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t_Int32_t392,
	RuntimeInvoker_Boolean_t393_Int16_t1444_Int32_t392,
	RuntimeInvoker_Boolean_t393_Int16_t1444_Int16_t1444_Int32_t392,
	RuntimeInvoker_Void_t1449_Int16_t1444_Int16_t1444_Int32_t392,
	RuntimeInvoker_Object_t_Int32U26_t3516,
	RuntimeInvoker_Object_t_Int32_t392_Object_t_Int32_t392,
	RuntimeInvoker_Void_t1449_SByte_t1443_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_SByte_t1443_Int32_t392_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_SByte_t1443_Int32U26_t3516_BooleanU26_t3515_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_Int32U26_t3516,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_CharU26_t3548_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_CharU26_t3548_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_CharU26_t3548_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t_Int32_t392_CharU26_t3548_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Object_t_DecoderFallbackBufferU26_t3584_ByteU5BU5DU26_t3543_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392_Object_t_DecoderFallbackBufferU26_t3584_ByteU5BU5DU26_t3543_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_DecoderFallbackBufferU26_t3584_ByteU5BU5DU26_t3543_Object_t_Int64_t711_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_DecoderFallbackBufferU26_t3584_ByteU5BU5DU26_t3543_Object_t_Int64_t711_Int32_t392_Object_t_Int32U26_t3516,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Object_t_Int32_t392_UInt32U26_t3553_UInt32U26_t3553_Object_t_DecoderFallbackBufferU26_t3584_ByteU5BU5DU26_t3543_SByte_t1443,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t_Int32_t392_UInt32U26_t3553_UInt32U26_t3553_Object_t_DecoderFallbackBufferU26_t3584_ByteU5BU5DU26_t3543_SByte_t1443,
	RuntimeInvoker_Void_t1449_SByte_t1443_Int32_t392,
	RuntimeInvoker_IntPtr_t_SByte_t1443_Object_t_BooleanU26_t3515,
	RuntimeInvoker_IntPtr_t_SByte_t1443_SByte_t1443_Object_t_BooleanU26_t3515,
	RuntimeInvoker_Boolean_t393_TimeSpan_t975_TimeSpan_t975,
	RuntimeInvoker_Boolean_t393_Int64_t711_Int64_t711_SByte_t1443,
	RuntimeInvoker_Boolean_t393_IntPtr_t_Int32_t392_SByte_t1443,
	RuntimeInvoker_Int64_t711_Double_t710,
	RuntimeInvoker_Object_t_Double_t710,
	RuntimeInvoker_Int64_t711_Object_t_Int32_t392,
	RuntimeInvoker_UInt16_t1430_Object_t_Int32_t392,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t392_Int32_t392,
	RuntimeInvoker_Byte_t699_SByte_t1443,
	RuntimeInvoker_Byte_t699_Double_t710,
	RuntimeInvoker_Byte_t699_Single_t117,
	RuntimeInvoker_Byte_t699_Int64_t711,
	RuntimeInvoker_Char_t424_SByte_t1443,
	RuntimeInvoker_Char_t424_Int64_t711,
	RuntimeInvoker_Char_t424_Single_t117,
	RuntimeInvoker_Char_t424_Object_t_Object_t,
	RuntimeInvoker_DateTime_t577_Object_t_Object_t,
	RuntimeInvoker_DateTime_t577_Int16_t1444,
	RuntimeInvoker_DateTime_t577_Int32_t392,
	RuntimeInvoker_DateTime_t577_Int64_t711,
	RuntimeInvoker_DateTime_t577_Single_t117,
	RuntimeInvoker_DateTime_t577_SByte_t1443,
	RuntimeInvoker_Double_t710_SByte_t1443,
	RuntimeInvoker_Double_t710_Double_t710,
	RuntimeInvoker_Double_t710_Single_t117,
	RuntimeInvoker_Double_t710_Int32_t392,
	RuntimeInvoker_Double_t710_Int64_t711,
	RuntimeInvoker_Double_t710_Int16_t1444,
	RuntimeInvoker_Int16_t1444_SByte_t1443,
	RuntimeInvoker_Int16_t1444_Double_t710,
	RuntimeInvoker_Int16_t1444_Single_t117,
	RuntimeInvoker_Int16_t1444_Int32_t392,
	RuntimeInvoker_Int16_t1444_Int64_t711,
	RuntimeInvoker_Int64_t711_SByte_t1443,
	RuntimeInvoker_Int64_t711_Int16_t1444,
	RuntimeInvoker_Int64_t711_Single_t117,
	RuntimeInvoker_Int64_t711_Int64_t711,
	RuntimeInvoker_SByte_t1443_SByte_t1443,
	RuntimeInvoker_SByte_t1443_Int16_t1444,
	RuntimeInvoker_SByte_t1443_Double_t710,
	RuntimeInvoker_SByte_t1443_Single_t117,
	RuntimeInvoker_SByte_t1443_Int32_t392,
	RuntimeInvoker_SByte_t1443_Int64_t711,
	RuntimeInvoker_Single_t117_SByte_t1443,
	RuntimeInvoker_Single_t117_Double_t710,
	RuntimeInvoker_Single_t117_Int64_t711,
	RuntimeInvoker_Single_t117_Int16_t1444,
	RuntimeInvoker_UInt16_t1430_SByte_t1443,
	RuntimeInvoker_UInt16_t1430_Double_t710,
	RuntimeInvoker_UInt16_t1430_Single_t117,
	RuntimeInvoker_UInt16_t1430_Int32_t392,
	RuntimeInvoker_UInt16_t1430_Int64_t711,
	RuntimeInvoker_UInt32_t712_SByte_t1443,
	RuntimeInvoker_UInt32_t712_Int16_t1444,
	RuntimeInvoker_UInt32_t712_Double_t710,
	RuntimeInvoker_UInt32_t712_Single_t117,
	RuntimeInvoker_UInt32_t712_Int64_t711,
	RuntimeInvoker_UInt64_t1442_SByte_t1443,
	RuntimeInvoker_UInt64_t1442_Int16_t1444,
	RuntimeInvoker_UInt64_t1442_Double_t710,
	RuntimeInvoker_UInt64_t1442_Single_t117,
	RuntimeInvoker_UInt64_t1442_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_SByte_t1443_TimeSpan_t975,
	RuntimeInvoker_Void_t1449_Int64_t711_Int32_t392,
	RuntimeInvoker_DayOfWeek_t2016,
	RuntimeInvoker_DateTimeKind_t2013,
	RuntimeInvoker_DateTime_t577_TimeSpan_t975,
	RuntimeInvoker_DateTime_t577_Double_t710,
	RuntimeInvoker_Int32_t392_DateTime_t577_DateTime_t577,
	RuntimeInvoker_DateTime_t577_DateTime_t577_Int32_t392,
	RuntimeInvoker_DateTime_t577_Object_t_Object_t_Int32_t392,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Int32_t392_DateTimeU26_t3585_DateTimeOffsetU26_t3586_SByte_t1443_ExceptionU26_t3551,
	RuntimeInvoker_Object_t_Object_t_SByte_t1443_ExceptionU26_t3551,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392_SByte_t1443_SByte_t1443_Int32U26_t3516,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Object_t_Object_t_SByte_t1443_Int32U26_t3516,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Int32_t392_Object_t_Int32U26_t3516,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Int32_t392_Object_t_SByte_t1443_Int32U26_t3516_Int32U26_t3516,
	RuntimeInvoker_Boolean_t393_Object_t_Int32_t392_Object_t_SByte_t1443_Int32U26_t3516,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t_SByte_t1443_DateTimeU26_t3585_DateTimeOffsetU26_t3586_Object_t_Int32_t392_SByte_t1443_BooleanU26_t3515_BooleanU26_t3515,
	RuntimeInvoker_DateTime_t577_Object_t_Object_t_Object_t_Int32_t392,
	RuntimeInvoker_Boolean_t393_Object_t_Object_t_Object_t_Int32_t392_DateTimeU26_t3585_SByte_t1443_BooleanU26_t3515_SByte_t1443_ExceptionU26_t3551,
	RuntimeInvoker_DateTime_t577_DateTime_t577_TimeSpan_t975,
	RuntimeInvoker_Boolean_t393_DateTime_t577_DateTime_t577,
	RuntimeInvoker_TimeSpan_t975_DateTime_t577_DateTime_t577,
	RuntimeInvoker_Void_t1449_DateTime_t577_TimeSpan_t975,
	RuntimeInvoker_Void_t1449_Int64_t711_TimeSpan_t975,
	RuntimeInvoker_Int32_t392_DateTimeOffset_t2014,
	RuntimeInvoker_Boolean_t393_DateTimeOffset_t2014,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int16_t1444,
	RuntimeInvoker_Object_t_Int16_t1444_Object_t_BooleanU26_t3515_BooleanU26_t3515,
	RuntimeInvoker_Object_t_Int16_t1444_Object_t_BooleanU26_t3515_BooleanU26_t3515_SByte_t1443,
	RuntimeInvoker_Object_t_DateTime_t577_Object_t_Object_t,
	RuntimeInvoker_Object_t_DateTime_t577_Nullable_1_t2120_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_MonoEnumInfo_t2027,
	RuntimeInvoker_Void_t1449_Object_t_MonoEnumInfoU26_t3587,
	RuntimeInvoker_Int32_t392_Int16_t1444_Int16_t1444,
	RuntimeInvoker_Int32_t392_Int64_t711_Int64_t711,
	RuntimeInvoker_PlatformID_t2056,
	RuntimeInvoker_Void_t1449_Int32_t392_Int16_t1444_Int16_t1444_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Int32_t392_Guid_t2036,
	RuntimeInvoker_Boolean_t393_Guid_t2036,
	RuntimeInvoker_Guid_t2036,
	RuntimeInvoker_Object_t_SByte_t1443_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Double_t710_Double_t710_Double_t710,
	RuntimeInvoker_TypeAttributes_t1717_Object_t,
	RuntimeInvoker_Object_t_SByte_t1443_SByte_t1443,
	RuntimeInvoker_Void_t1449_UInt64U2AU26_t3588_Int32U2AU26_t3589_CharU2AU26_t3590_CharU2AU26_t3590_Int64U2AU26_t3591_Int32U2AU26_t3589,
	RuntimeInvoker_Void_t1449_Int32_t392_Int64_t711,
	RuntimeInvoker_Void_t1449_Object_t_Double_t710_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Decimal_t1445,
	RuntimeInvoker_Object_t_Object_t_SByte_t1443_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int16_t1444_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int64_t711_Object_t,
	RuntimeInvoker_Object_t_Object_t_Single_t117_Object_t,
	RuntimeInvoker_Object_t_Object_t_Double_t710_Object_t,
	RuntimeInvoker_Object_t_Object_t_Decimal_t1445_Object_t,
	RuntimeInvoker_Object_t_Single_t117_Object_t,
	RuntimeInvoker_Object_t_Double_t710_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_BooleanU26_t3515_SByte_t1443_Int32U26_t3516_Int32U26_t3516,
	RuntimeInvoker_Object_t_Object_t_Int32_t392_Int32_t392_Object_t_SByte_t1443_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_Int64_t711_Int32_t392_Int32_t392_Int32_t392_Int32_t392_Int32_t392,
	RuntimeInvoker_TimeSpan_t975_TimeSpan_t975,
	RuntimeInvoker_Int32_t392_TimeSpan_t975_TimeSpan_t975,
	RuntimeInvoker_Int32_t392_TimeSpan_t975,
	RuntimeInvoker_Boolean_t393_TimeSpan_t975,
	RuntimeInvoker_TimeSpan_t975_Double_t710,
	RuntimeInvoker_TimeSpan_t975_Double_t710_Int64_t711,
	RuntimeInvoker_TimeSpan_t975_TimeSpan_t975_TimeSpan_t975,
	RuntimeInvoker_TimeSpan_t975_DateTime_t577,
	RuntimeInvoker_Boolean_t393_DateTime_t577_Object_t,
	RuntimeInvoker_DateTime_t577_DateTime_t577,
	RuntimeInvoker_TimeSpan_t975_DateTime_t577_TimeSpan_t975,
	RuntimeInvoker_Boolean_t393_Int32_t392_Int64U5BU5DU26_t3592_StringU5BU5DU26_t3593,
	RuntimeInvoker_Boolean_t393_ObjectU26_t3573_Object_t,
	RuntimeInvoker_Void_t1449_ObjectU26_t3573_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t393_Int32_t392_Int32_t392_Object_t,
	RuntimeInvoker_Enumerator_t2985,
	RuntimeInvoker_Enumerator_t3001,
	RuntimeInvoker_Enumerator_t2222,
	RuntimeInvoker_Void_t1449_Int32_t392_ObjectU26_t3573,
	RuntimeInvoker_Void_t1449_ObjectU5BU5DU26_t3550_Int32_t392,
	RuntimeInvoker_Void_t1449_ObjectU5BU5DU26_t3550_Int32_t392_Int32_t392,
	RuntimeInvoker_Void_t1449_KeyValuePair_2_t2365,
	RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2365,
	RuntimeInvoker_KeyValuePair_2_t2365_Object_t_Object_t,
	RuntimeInvoker_Boolean_t393_Object_t_ObjectU26_t3573,
	RuntimeInvoker_Enumerator_t2369,
	RuntimeInvoker_DictionaryEntry_t1423_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2365,
	RuntimeInvoker_Enumerator_t2368,
	RuntimeInvoker_Enumerator_t2372,
	RuntimeInvoker_Int32_t392_Int32_t392_Int32_t392_Object_t,
	RuntimeInvoker_Enumerator_t2174,
	RuntimeInvoker_Enumerator_t2288,
	RuntimeInvoker_Enumerator_t2285,
	RuntimeInvoker_KeyValuePair_2_t2280,
	RuntimeInvoker_Void_t1449_FloatTween_t196,
	RuntimeInvoker_Void_t1449_ColorTween_t193,
	RuntimeInvoker_Boolean_t393_TypeU26_t3594_Int32_t392,
	RuntimeInvoker_Boolean_t393_BooleanU26_t3515_SByte_t1443,
	RuntimeInvoker_Boolean_t393_FillMethodU26_t3595_Int32_t392,
	RuntimeInvoker_Boolean_t393_SingleU26_t3526_Single_t117,
	RuntimeInvoker_Boolean_t393_ContentTypeU26_t3596_Int32_t392,
	RuntimeInvoker_Boolean_t393_LineTypeU26_t3597_Int32_t392,
	RuntimeInvoker_Boolean_t393_InputTypeU26_t3598_Int32_t392,
	RuntimeInvoker_Boolean_t393_TouchScreenKeyboardTypeU26_t3599_Int32_t392,
	RuntimeInvoker_Boolean_t393_CharacterValidationU26_t3600_Int32_t392,
	RuntimeInvoker_Boolean_t393_CharU26_t3548_Int16_t1444,
	RuntimeInvoker_Boolean_t393_DirectionU26_t3601_Int32_t392,
	RuntimeInvoker_Boolean_t393_NavigationU26_t3602_Navigation_t280,
	RuntimeInvoker_Boolean_t393_TransitionU26_t3603_Int32_t392,
	RuntimeInvoker_Boolean_t393_ColorBlockU26_t3604_ColorBlock_t207,
	RuntimeInvoker_Boolean_t393_SpriteStateU26_t3605_SpriteState_t299,
	RuntimeInvoker_Boolean_t393_DirectionU26_t3606_Int32_t392,
	RuntimeInvoker_Boolean_t393_AspectModeU26_t3607_Int32_t392,
	RuntimeInvoker_Boolean_t393_FitModeU26_t3608_Int32_t392,
	RuntimeInvoker_Void_t1449_CornerU26_t3609_Int32_t392,
	RuntimeInvoker_Void_t1449_AxisU26_t3610_Int32_t392,
	RuntimeInvoker_Void_t1449_Vector2U26_t3529_Vector2_t171,
	RuntimeInvoker_Void_t1449_ConstraintU26_t3611_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32U26_t3516_Int32_t392,
	RuntimeInvoker_Void_t1449_SingleU26_t3526_Single_t117,
	RuntimeInvoker_Void_t1449_BooleanU26_t3515_SByte_t1443,
	RuntimeInvoker_Void_t1449_TextAnchorU26_t3612_Int32_t392,
	RuntimeInvoker_Void_t1449_Object_t_Object_t_Single_t117,
	RuntimeInvoker_Enumerator_t977,
	RuntimeInvoker_Enumerator_t2716,
	RuntimeInvoker_Void_t1449_Int32_t392_Double_t710,
	RuntimeInvoker_Vector3_t12_Int32_t392,
	RuntimeInvoker_Int32_t392_Vector3_t12,
	RuntimeInvoker_Void_t1449_Int32_t392_Vector3_t12,
	RuntimeInvoker_RaycastResult_t169_Int32_t392,
	RuntimeInvoker_Boolean_t393_RaycastResult_t169,
	RuntimeInvoker_Int32_t392_RaycastResult_t169,
	RuntimeInvoker_Void_t1449_Int32_t392_RaycastResult_t169,
	RuntimeInvoker_Void_t1449_RaycastResultU5BU5DU26_t3613_Int32_t392,
	RuntimeInvoker_Void_t1449_RaycastResultU5BU5DU26_t3613_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_RaycastResult_t169_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_RaycastResult_t169_RaycastResult_t169_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2280_Int32_t392,
	RuntimeInvoker_Void_t1449_KeyValuePair_2_t2280,
	RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2280,
	RuntimeInvoker_Int32_t392_KeyValuePair_2_t2280,
	RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2280,
	RuntimeInvoker_Link_t1547_Int32_t392,
	RuntimeInvoker_Void_t1449_Link_t1547,
	RuntimeInvoker_Boolean_t393_Link_t1547,
	RuntimeInvoker_Int32_t392_Link_t1547,
	RuntimeInvoker_Void_t1449_Int32_t392_Link_t1547,
	RuntimeInvoker_DictionaryEntry_t1423_Int32_t392,
	RuntimeInvoker_Void_t1449_DictionaryEntry_t1423,
	RuntimeInvoker_Boolean_t393_DictionaryEntry_t1423,
	RuntimeInvoker_Int32_t392_DictionaryEntry_t1423,
	RuntimeInvoker_Void_t1449_Int32_t392_DictionaryEntry_t1423,
	RuntimeInvoker_RaycastHit2D_t402_Int32_t392,
	RuntimeInvoker_Void_t1449_RaycastHit2D_t402,
	RuntimeInvoker_Boolean_t393_RaycastHit2D_t402,
	RuntimeInvoker_Int32_t392_RaycastHit2D_t402,
	RuntimeInvoker_Void_t1449_Int32_t392_RaycastHit2D_t402,
	RuntimeInvoker_RaycastHit_t89_Int32_t392,
	RuntimeInvoker_Void_t1449_RaycastHit_t89,
	RuntimeInvoker_Boolean_t393_RaycastHit_t89,
	RuntimeInvoker_Int32_t392_RaycastHit_t89,
	RuntimeInvoker_Void_t1449_Int32_t392_RaycastHit_t89,
	RuntimeInvoker_KeyValuePair_2_t2312_Int32_t392,
	RuntimeInvoker_Void_t1449_KeyValuePair_2_t2312,
	RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2312,
	RuntimeInvoker_Int32_t392_KeyValuePair_2_t2312,
	RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2312,
	RuntimeInvoker_KeyValuePair_2_t2365_Int32_t392,
	RuntimeInvoker_Int32_t392_KeyValuePair_2_t2365,
	RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2365,
	RuntimeInvoker_UIVertex_t272_Int32_t392,
	RuntimeInvoker_Boolean_t393_UIVertex_t272,
	RuntimeInvoker_Int32_t392_UIVertex_t272,
	RuntimeInvoker_Void_t1449_Int32_t392_UIVertex_t272,
	RuntimeInvoker_Void_t1449_UIVertexU5BU5DU26_t3614_Int32_t392,
	RuntimeInvoker_Void_t1449_UIVertexU5BU5DU26_t3614_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_UIVertex_t272_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_UIVertex_t272_UIVertex_t272_Object_t,
	RuntimeInvoker_Boolean_t393_Vector2_t171,
	RuntimeInvoker_Void_t1449_Int32_t392_Vector2_t171,
	RuntimeInvoker_ContentType_t250_Int32_t392,
	RuntimeInvoker_UILineInfo_t428_Int32_t392,
	RuntimeInvoker_Void_t1449_UILineInfo_t428,
	RuntimeInvoker_Boolean_t393_UILineInfo_t428,
	RuntimeInvoker_Int32_t392_UILineInfo_t428,
	RuntimeInvoker_Void_t1449_Int32_t392_UILineInfo_t428,
	RuntimeInvoker_UICharInfo_t430_Int32_t392,
	RuntimeInvoker_Void_t1449_UICharInfo_t430,
	RuntimeInvoker_Boolean_t393_UICharInfo_t430,
	RuntimeInvoker_Int32_t392_UICharInfo_t430,
	RuntimeInvoker_Void_t1449_Int32_t392_UICharInfo_t430,
	RuntimeInvoker_Void_t1449_Vector3U5BU5DU26_t3615_Int32_t392,
	RuntimeInvoker_Void_t1449_Vector3U5BU5DU26_t3615_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Vector3_t12_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Vector3_t12_Vector3_t12_Object_t,
	RuntimeInvoker_Void_t1449_Int32U5BU5DU26_t3616_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32U5BU5DU26_t3616_Int32_t392_Int32_t392,
	RuntimeInvoker_Color32_t382_Int32_t392,
	RuntimeInvoker_Void_t1449_Color32_t382,
	RuntimeInvoker_Boolean_t393_Color32_t382,
	RuntimeInvoker_Int32_t392_Color32_t382,
	RuntimeInvoker_Void_t1449_Int32_t392_Color32_t382,
	RuntimeInvoker_Void_t1449_Color32U5BU5DU26_t3617_Int32_t392,
	RuntimeInvoker_Void_t1449_Color32U5BU5DU26_t3617_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Color32_t382_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Color32_t382_Color32_t382_Object_t,
	RuntimeInvoker_Void_t1449_Vector2U5BU5DU26_t3618_Int32_t392,
	RuntimeInvoker_Void_t1449_Vector2U5BU5DU26_t3618_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Vector2_t171_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Vector2_t171_Vector2_t171_Object_t,
	RuntimeInvoker_Void_t1449_Vector4_t350,
	RuntimeInvoker_Boolean_t393_Vector4_t350,
	RuntimeInvoker_Int32_t392_Vector4_t350,
	RuntimeInvoker_Void_t1449_Vector4U5BU5DU26_t3619_Int32_t392,
	RuntimeInvoker_Void_t1449_Vector4U5BU5DU26_t3619_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_Vector4_t350_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Vector4_t350_Vector4_t350_Object_t,
	RuntimeInvoker_GcAchievementData_t613_Int32_t392,
	RuntimeInvoker_Void_t1449_GcAchievementData_t613,
	RuntimeInvoker_Boolean_t393_GcAchievementData_t613,
	RuntimeInvoker_Int32_t392_GcAchievementData_t613,
	RuntimeInvoker_Void_t1449_Int32_t392_GcAchievementData_t613,
	RuntimeInvoker_GcScoreData_t614_Int32_t392,
	RuntimeInvoker_Boolean_t393_GcScoreData_t614,
	RuntimeInvoker_Int32_t392_GcScoreData_t614,
	RuntimeInvoker_Void_t1449_Int32_t392_GcScoreData_t614,
	RuntimeInvoker_ContactPoint_t534_Int32_t392,
	RuntimeInvoker_Void_t1449_ContactPoint_t534,
	RuntimeInvoker_Boolean_t393_ContactPoint_t534,
	RuntimeInvoker_Int32_t392_ContactPoint_t534,
	RuntimeInvoker_Void_t1449_Int32_t392_ContactPoint_t534,
	RuntimeInvoker_ContactPoint2D_t541_Int32_t392,
	RuntimeInvoker_Void_t1449_ContactPoint2D_t541,
	RuntimeInvoker_Boolean_t393_ContactPoint2D_t541,
	RuntimeInvoker_Int32_t392_ContactPoint2D_t541,
	RuntimeInvoker_Void_t1449_Int32_t392_ContactPoint2D_t541,
	RuntimeInvoker_WebCamDevice_t550_Int32_t392,
	RuntimeInvoker_Void_t1449_WebCamDevice_t550,
	RuntimeInvoker_Boolean_t393_WebCamDevice_t550,
	RuntimeInvoker_Int32_t392_WebCamDevice_t550,
	RuntimeInvoker_Void_t1449_Int32_t392_WebCamDevice_t550,
	RuntimeInvoker_Keyframe_t557_Int32_t392,
	RuntimeInvoker_Void_t1449_Keyframe_t557,
	RuntimeInvoker_Boolean_t393_Keyframe_t557,
	RuntimeInvoker_Int32_t392_Keyframe_t557,
	RuntimeInvoker_Void_t1449_Int32_t392_Keyframe_t557,
	RuntimeInvoker_CharacterInfo_t566_Int32_t392,
	RuntimeInvoker_Void_t1449_CharacterInfo_t566,
	RuntimeInvoker_Boolean_t393_CharacterInfo_t566,
	RuntimeInvoker_Int32_t392_CharacterInfo_t566,
	RuntimeInvoker_Void_t1449_Int32_t392_CharacterInfo_t566,
	RuntimeInvoker_Void_t1449_UICharInfoU5BU5DU26_t3620_Int32_t392,
	RuntimeInvoker_Void_t1449_UICharInfoU5BU5DU26_t3620_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_UICharInfo_t430_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_UICharInfo_t430_UICharInfo_t430_Object_t,
	RuntimeInvoker_Void_t1449_UILineInfoU5BU5DU26_t3621_Int32_t392,
	RuntimeInvoker_Void_t1449_UILineInfoU5BU5DU26_t3621_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_UILineInfo_t428_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_UILineInfo_t428_UILineInfo_t428_Object_t,
	RuntimeInvoker_ParameterModifier_t1710_Int32_t392,
	RuntimeInvoker_Void_t1449_ParameterModifier_t1710,
	RuntimeInvoker_Boolean_t393_ParameterModifier_t1710,
	RuntimeInvoker_Int32_t392_ParameterModifier_t1710,
	RuntimeInvoker_Void_t1449_Int32_t392_ParameterModifier_t1710,
	RuntimeInvoker_HitInfo_t633_Int32_t392,
	RuntimeInvoker_Void_t1449_HitInfo_t633,
	RuntimeInvoker_Int32_t392_HitInfo_t633,
	RuntimeInvoker_KeyValuePair_2_t2636_Int32_t392,
	RuntimeInvoker_Void_t1449_KeyValuePair_2_t2636,
	RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2636,
	RuntimeInvoker_Int32_t392_KeyValuePair_2_t2636,
	RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2636,
	RuntimeInvoker_TextEditOp_t650_Int32_t392,
	RuntimeInvoker_TargetSearchResult_t884_Int32_t392,
	RuntimeInvoker_Boolean_t393_TargetSearchResult_t884,
	RuntimeInvoker_Int32_t392_TargetSearchResult_t884,
	RuntimeInvoker_Void_t1449_Int32_t392_TargetSearchResult_t884,
	RuntimeInvoker_KeyValuePair_2_t2708_Int32_t392,
	RuntimeInvoker_Void_t1449_KeyValuePair_2_t2708,
	RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2708,
	RuntimeInvoker_Int32_t392_KeyValuePair_2_t2708,
	RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2708,
	RuntimeInvoker_PIXEL_FORMAT_t783_Int32_t392,
	RuntimeInvoker_Void_t1449_PIXEL_FORMATU5BU5DU26_t3622_Int32_t392,
	RuntimeInvoker_Void_t1449_PIXEL_FORMATU5BU5DU26_t3622_Int32_t392_Int32_t392,
	RuntimeInvoker_TrackableResultData_t810_Int32_t392,
	RuntimeInvoker_Void_t1449_TrackableResultData_t810,
	RuntimeInvoker_Int32_t392_TrackableResultData_t810,
	RuntimeInvoker_Void_t1449_Int32_t392_TrackableResultData_t810,
	RuntimeInvoker_WordData_t815_Int32_t392,
	RuntimeInvoker_Void_t1449_WordData_t815,
	RuntimeInvoker_Boolean_t393_WordData_t815,
	RuntimeInvoker_Int32_t392_WordData_t815,
	RuntimeInvoker_Void_t1449_Int32_t392_WordData_t815,
	RuntimeInvoker_WordResultData_t814_Int32_t392,
	RuntimeInvoker_Void_t1449_WordResultData_t814,
	RuntimeInvoker_Boolean_t393_WordResultData_t814,
	RuntimeInvoker_Int32_t392_WordResultData_t814,
	RuntimeInvoker_Void_t1449_Int32_t392_WordResultData_t814,
	RuntimeInvoker_SmartTerrainRevisionData_t818_Int32_t392,
	RuntimeInvoker_Void_t1449_SmartTerrainRevisionData_t818,
	RuntimeInvoker_Boolean_t393_SmartTerrainRevisionData_t818,
	RuntimeInvoker_Int32_t392_SmartTerrainRevisionData_t818,
	RuntimeInvoker_Void_t1449_Int32_t392_SmartTerrainRevisionData_t818,
	RuntimeInvoker_SurfaceData_t819_Int32_t392,
	RuntimeInvoker_Void_t1449_SurfaceData_t819,
	RuntimeInvoker_Boolean_t393_SurfaceData_t819,
	RuntimeInvoker_Int32_t392_SurfaceData_t819,
	RuntimeInvoker_Void_t1449_Int32_t392_SurfaceData_t819,
	RuntimeInvoker_PropData_t820_Int32_t392,
	RuntimeInvoker_Void_t1449_PropData_t820,
	RuntimeInvoker_Boolean_t393_PropData_t820,
	RuntimeInvoker_Int32_t392_PropData_t820,
	RuntimeInvoker_Void_t1449_Int32_t392_PropData_t820,
	RuntimeInvoker_KeyValuePair_2_t2792_Int32_t392,
	RuntimeInvoker_Void_t1449_KeyValuePair_2_t2792,
	RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2792,
	RuntimeInvoker_Int32_t392_KeyValuePair_2_t2792,
	RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2792,
	RuntimeInvoker_KeyValuePair_2_t2881_Int32_t392,
	RuntimeInvoker_Void_t1449_KeyValuePair_2_t2881,
	RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2881,
	RuntimeInvoker_Int32_t392_KeyValuePair_2_t2881,
	RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2881,
	RuntimeInvoker_KeyValuePair_2_t2896_Int32_t392,
	RuntimeInvoker_Void_t1449_KeyValuePair_2_t2896,
	RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2896,
	RuntimeInvoker_Int32_t392_KeyValuePair_2_t2896,
	RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2896,
	RuntimeInvoker_VirtualButtonData_t811_Int32_t392,
	RuntimeInvoker_Void_t1449_VirtualButtonData_t811,
	RuntimeInvoker_Boolean_t393_VirtualButtonData_t811,
	RuntimeInvoker_Int32_t392_VirtualButtonData_t811,
	RuntimeInvoker_Void_t1449_Int32_t392_VirtualButtonData_t811,
	RuntimeInvoker_Void_t1449_TargetSearchResultU5BU5DU26_t3623_Int32_t392,
	RuntimeInvoker_Void_t1449_TargetSearchResultU5BU5DU26_t3623_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_TargetSearchResult_t884_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_TargetSearchResult_t884_TargetSearchResult_t884_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2935_Int32_t392,
	RuntimeInvoker_Void_t1449_KeyValuePair_2_t2935,
	RuntimeInvoker_Boolean_t393_KeyValuePair_2_t2935,
	RuntimeInvoker_Int32_t392_KeyValuePair_2_t2935,
	RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t2935,
	RuntimeInvoker_ProfileData_t898_Int32_t392,
	RuntimeInvoker_Void_t1449_ProfileData_t898,
	RuntimeInvoker_Boolean_t393_ProfileData_t898,
	RuntimeInvoker_Int32_t392_ProfileData_t898,
	RuntimeInvoker_Void_t1449_Int32_t392_ProfileData_t898,
	RuntimeInvoker_Link_t2983_Int32_t392,
	RuntimeInvoker_Void_t1449_Link_t2983,
	RuntimeInvoker_Boolean_t393_Link_t2983,
	RuntimeInvoker_Int32_t392_Link_t2983,
	RuntimeInvoker_Void_t1449_Int32_t392_Link_t2983,
	RuntimeInvoker_ClientCertificateType_t1203_Int32_t392,
	RuntimeInvoker_KeyValuePair_2_t3005_Int32_t392,
	RuntimeInvoker_Void_t1449_KeyValuePair_2_t3005,
	RuntimeInvoker_Boolean_t393_KeyValuePair_2_t3005,
	RuntimeInvoker_Int32_t392_KeyValuePair_2_t3005,
	RuntimeInvoker_Void_t1449_Int32_t392_KeyValuePair_2_t3005,
	RuntimeInvoker_X509ChainStatus_t1333_Int32_t392,
	RuntimeInvoker_Void_t1449_X509ChainStatus_t1333,
	RuntimeInvoker_Boolean_t393_X509ChainStatus_t1333,
	RuntimeInvoker_Int32_t392_X509ChainStatus_t1333,
	RuntimeInvoker_Void_t1449_Int32_t392_X509ChainStatus_t1333,
	RuntimeInvoker_Int32_t392_Object_t_Int32_t392_Int32_t392_Int32_t392_Object_t,
	RuntimeInvoker_Mark_t1375_Int32_t392,
	RuntimeInvoker_Void_t1449_Mark_t1375,
	RuntimeInvoker_Boolean_t393_Mark_t1375,
	RuntimeInvoker_Int32_t392_Mark_t1375,
	RuntimeInvoker_Void_t1449_Int32_t392_Mark_t1375,
	RuntimeInvoker_UriScheme_t1412_Int32_t392,
	RuntimeInvoker_Void_t1449_UriScheme_t1412,
	RuntimeInvoker_Boolean_t393_UriScheme_t1412,
	RuntimeInvoker_Int32_t392_UriScheme_t1412,
	RuntimeInvoker_Void_t1449_Int32_t392_UriScheme_t1412,
	RuntimeInvoker_TableRange_t1480_Int32_t392,
	RuntimeInvoker_Void_t1449_TableRange_t1480,
	RuntimeInvoker_Boolean_t393_TableRange_t1480,
	RuntimeInvoker_Int32_t392_TableRange_t1480,
	RuntimeInvoker_Void_t1449_Int32_t392_TableRange_t1480,
	RuntimeInvoker_Slot_t1557_Int32_t392,
	RuntimeInvoker_Void_t1449_Slot_t1557,
	RuntimeInvoker_Boolean_t393_Slot_t1557,
	RuntimeInvoker_Int32_t392_Slot_t1557,
	RuntimeInvoker_Void_t1449_Int32_t392_Slot_t1557,
	RuntimeInvoker_Slot_t1565_Int32_t392,
	RuntimeInvoker_Void_t1449_Slot_t1565,
	RuntimeInvoker_Boolean_t393_Slot_t1565,
	RuntimeInvoker_Int32_t392_Slot_t1565,
	RuntimeInvoker_Void_t1449_Int32_t392_Slot_t1565,
	RuntimeInvoker_ILTokenInfo_t1645_Int32_t392,
	RuntimeInvoker_Void_t1449_ILTokenInfo_t1645,
	RuntimeInvoker_Boolean_t393_ILTokenInfo_t1645,
	RuntimeInvoker_Int32_t392_ILTokenInfo_t1645,
	RuntimeInvoker_Void_t1449_Int32_t392_ILTokenInfo_t1645,
	RuntimeInvoker_LabelData_t1647_Int32_t392,
	RuntimeInvoker_Void_t1449_LabelData_t1647,
	RuntimeInvoker_Boolean_t393_LabelData_t1647,
	RuntimeInvoker_Int32_t392_LabelData_t1647,
	RuntimeInvoker_Void_t1449_Int32_t392_LabelData_t1647,
	RuntimeInvoker_LabelFixup_t1646_Int32_t392,
	RuntimeInvoker_Void_t1449_LabelFixup_t1646,
	RuntimeInvoker_Boolean_t393_LabelFixup_t1646,
	RuntimeInvoker_Int32_t392_LabelFixup_t1646,
	RuntimeInvoker_Void_t1449_Int32_t392_LabelFixup_t1646,
	RuntimeInvoker_CustomAttributeTypedArgument_t1692_Int32_t392,
	RuntimeInvoker_Void_t1449_CustomAttributeTypedArgument_t1692,
	RuntimeInvoker_Boolean_t393_CustomAttributeTypedArgument_t1692,
	RuntimeInvoker_Int32_t392_CustomAttributeTypedArgument_t1692,
	RuntimeInvoker_Void_t1449_Int32_t392_CustomAttributeTypedArgument_t1692,
	RuntimeInvoker_CustomAttributeNamedArgument_t1691_Int32_t392,
	RuntimeInvoker_Void_t1449_CustomAttributeNamedArgument_t1691,
	RuntimeInvoker_Boolean_t393_CustomAttributeNamedArgument_t1691,
	RuntimeInvoker_Int32_t392_CustomAttributeNamedArgument_t1691,
	RuntimeInvoker_Void_t1449_Int32_t392_CustomAttributeNamedArgument_t1691,
	RuntimeInvoker_Void_t1449_CustomAttributeTypedArgumentU5BU5DU26_t3624_Int32_t392,
	RuntimeInvoker_Void_t1449_CustomAttributeTypedArgumentU5BU5DU26_t3624_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_CustomAttributeTypedArgument_t1692_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_CustomAttributeTypedArgument_t1692_CustomAttributeTypedArgument_t1692_Object_t,
	RuntimeInvoker_Int32_t392_Object_t_CustomAttributeTypedArgument_t1692,
	RuntimeInvoker_Void_t1449_CustomAttributeNamedArgumentU5BU5DU26_t3625_Int32_t392,
	RuntimeInvoker_Void_t1449_CustomAttributeNamedArgumentU5BU5DU26_t3625_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_Object_t_CustomAttributeNamedArgument_t1691_Int32_t392_Int32_t392,
	RuntimeInvoker_Int32_t392_CustomAttributeNamedArgument_t1691_CustomAttributeNamedArgument_t1691_Object_t,
	RuntimeInvoker_Int32_t392_Object_t_CustomAttributeNamedArgument_t1691,
	RuntimeInvoker_ResourceInfo_t1721_Int32_t392,
	RuntimeInvoker_Void_t1449_ResourceInfo_t1721,
	RuntimeInvoker_Boolean_t393_ResourceInfo_t1721,
	RuntimeInvoker_Int32_t392_ResourceInfo_t1721,
	RuntimeInvoker_Void_t1449_Int32_t392_ResourceInfo_t1721,
	RuntimeInvoker_ResourceCacheItem_t1722_Int32_t392,
	RuntimeInvoker_Void_t1449_ResourceCacheItem_t1722,
	RuntimeInvoker_Boolean_t393_ResourceCacheItem_t1722,
	RuntimeInvoker_Int32_t392_ResourceCacheItem_t1722,
	RuntimeInvoker_Void_t1449_Int32_t392_ResourceCacheItem_t1722,
	RuntimeInvoker_Void_t1449_Int32_t392_DateTime_t577,
	RuntimeInvoker_Void_t1449_Decimal_t1445,
	RuntimeInvoker_Void_t1449_Int32_t392_Decimal_t1445,
	RuntimeInvoker_TimeSpan_t975_Int32_t392,
	RuntimeInvoker_Void_t1449_Int32_t392_TimeSpan_t975,
	RuntimeInvoker_TypeTag_t1858_Int32_t392,
	RuntimeInvoker_Boolean_t393_Byte_t699,
	RuntimeInvoker_Int32_t392_Byte_t699,
	RuntimeInvoker_Void_t1449_Int32_t392_Byte_t699,
	RuntimeInvoker_Object_t_RaycastResult_t169_RaycastResult_t169_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2240,
	RuntimeInvoker_Boolean_t393_RaycastResult_t169_RaycastResult_t169,
	RuntimeInvoker_Object_t_RaycastResult_t169_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2280_Int32_t392_Object_t,
	RuntimeInvoker_Boolean_t393_Int32_t392_ObjectU26_t3573,
	RuntimeInvoker_DictionaryEntry_t1423_Int32_t392_Object_t,
	RuntimeInvoker_Link_t1547,
	RuntimeInvoker_Enumerator_t2284,
	RuntimeInvoker_DictionaryEntry_t1423_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2280_Object_t,
	RuntimeInvoker_RaycastHit2D_t402,
	RuntimeInvoker_Object_t_RaycastHit_t89_RaycastHit_t89_Object_t_Object_t,
	RuntimeInvoker_RaycastHit_t89,
	RuntimeInvoker_Object_t_Color_t77_Object_t_Object_t,
	RuntimeInvoker_Object_t_Single_t117_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2312_Object_t_Int32_t392,
	RuntimeInvoker_Enumerator_t2316,
	RuntimeInvoker_DictionaryEntry_t1423_Object_t_Int32_t392,
	RuntimeInvoker_KeyValuePair_2_t2312,
	RuntimeInvoker_Enumerator_t2315,
	RuntimeInvoker_Enumerator_t2319,
	RuntimeInvoker_KeyValuePair_2_t2312_Object_t,
	RuntimeInvoker_Object_t_FloatTween_t196,
	RuntimeInvoker_KeyValuePair_2_t2365_Object_t,
	RuntimeInvoker_Object_t_ColorTween_t193,
	RuntimeInvoker_UIVertex_t272_Object_t,
	RuntimeInvoker_Enumerator_t2381,
	RuntimeInvoker_UIVertex_t272,
	RuntimeInvoker_Boolean_t393_UIVertex_t272_UIVertex_t272,
	RuntimeInvoker_Object_t_UIVertex_t272_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_UIVertex_t272_UIVertex_t272,
	RuntimeInvoker_Object_t_UIVertex_t272_UIVertex_t272_Object_t_Object_t,
	RuntimeInvoker_UILineInfo_t428,
	RuntimeInvoker_UICharInfo_t430,
	RuntimeInvoker_Object_t_Vector2_t171_Object_t_Object_t,
	RuntimeInvoker_Vector3_t12_Object_t,
	RuntimeInvoker_Enumerator_t2480,
	RuntimeInvoker_Object_t_Vector3_t12_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_Vector3_t12_Vector3_t12,
	RuntimeInvoker_Object_t_Vector3_t12_Vector3_t12_Object_t_Object_t,
	RuntimeInvoker_Color32_t382_Object_t,
	RuntimeInvoker_Enumerator_t2499,
	RuntimeInvoker_Color32_t382,
	RuntimeInvoker_Boolean_t393_Color32_t382_Color32_t382,
	RuntimeInvoker_Object_t_Color32_t382_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_Color32_t382_Color32_t382,
	RuntimeInvoker_Object_t_Color32_t382_Color32_t382_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2509,
	RuntimeInvoker_Int32_t392_Vector2_t171_Vector2_t171,
	RuntimeInvoker_Object_t_Vector2_t171_Vector2_t171_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2520,
	RuntimeInvoker_Object_t_Vector4_t350_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_Vector4_t350_Vector4_t350,
	RuntimeInvoker_Object_t_Vector4_t350_Vector4_t350_Object_t_Object_t,
	RuntimeInvoker_GcAchievementData_t613,
	RuntimeInvoker_GcScoreData_t614,
	RuntimeInvoker_ContactPoint_t534,
	RuntimeInvoker_ContactPoint2D_t541,
	RuntimeInvoker_WebCamDevice_t550,
	RuntimeInvoker_Keyframe_t557,
	RuntimeInvoker_CharacterInfo_t566,
	RuntimeInvoker_UICharInfo_t430_Object_t,
	RuntimeInvoker_Enumerator_t2584,
	RuntimeInvoker_Boolean_t393_UICharInfo_t430_UICharInfo_t430,
	RuntimeInvoker_Object_t_UICharInfo_t430_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_UICharInfo_t430_UICharInfo_t430,
	RuntimeInvoker_Object_t_UICharInfo_t430_UICharInfo_t430_Object_t_Object_t,
	RuntimeInvoker_UILineInfo_t428_Object_t,
	RuntimeInvoker_Enumerator_t2593,
	RuntimeInvoker_Boolean_t393_UILineInfo_t428_UILineInfo_t428,
	RuntimeInvoker_Object_t_UILineInfo_t428_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_UILineInfo_t428_UILineInfo_t428,
	RuntimeInvoker_Object_t_UILineInfo_t428_UILineInfo_t428_Object_t_Object_t,
	RuntimeInvoker_ParameterModifier_t1710,
	RuntimeInvoker_HitInfo_t633,
	RuntimeInvoker_TextEditOp_t650_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2636_Object_t_Int32_t392,
	RuntimeInvoker_TextEditOp_t650_Object_t_Int32_t392,
	RuntimeInvoker_Boolean_t393_Object_t_TextEditOpU26_t3626,
	RuntimeInvoker_Enumerator_t2641,
	RuntimeInvoker_KeyValuePair_2_t2636,
	RuntimeInvoker_TextEditOp_t650,
	RuntimeInvoker_Enumerator_t2640,
	RuntimeInvoker_Enumerator_t2644,
	RuntimeInvoker_KeyValuePair_2_t2636_Object_t,
	RuntimeInvoker_TargetSearchResult_t884,
	RuntimeInvoker_KeyValuePair_2_t2708_Int32_t392_Object_t,
	RuntimeInvoker_PIXEL_FORMAT_t783_Int32_t392_Object_t,
	RuntimeInvoker_PIXEL_FORMAT_t783_Object_t,
	RuntimeInvoker_Enumerator_t2713,
	RuntimeInvoker_KeyValuePair_2_t2708,
	RuntimeInvoker_Enumerator_t2712,
	RuntimeInvoker_KeyValuePair_2_t2708_Object_t,
	RuntimeInvoker_Enumerator_t2725,
	RuntimeInvoker_TrackableResultData_t810,
	RuntimeInvoker_WordData_t815,
	RuntimeInvoker_WordResultData_t814,
	RuntimeInvoker_Enumerator_t2772,
	RuntimeInvoker_Object_t_TrackableResultData_t810_Object_t_Object_t,
	RuntimeInvoker_SmartTerrainRevisionData_t818,
	RuntimeInvoker_SurfaceData_t819,
	RuntimeInvoker_PropData_t820,
	RuntimeInvoker_KeyValuePair_2_t2792_Object_t_Int16_t1444,
	RuntimeInvoker_UInt16_t1430_Object_t_Int16_t1444,
	RuntimeInvoker_Enumerator_t2797,
	RuntimeInvoker_DictionaryEntry_t1423_Object_t_Int16_t1444,
	RuntimeInvoker_KeyValuePair_2_t2792,
	RuntimeInvoker_Enumerator_t2796,
	RuntimeInvoker_Object_t_Object_t_Int16_t1444_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2800,
	RuntimeInvoker_KeyValuePair_2_t2792_Object_t,
	RuntimeInvoker_Boolean_t393_Int16_t1444_Int16_t1444,
	RuntimeInvoker_Object_t_SmartTerrainInitializationInfo_t760_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2881_Int32_t392_TrackableResultData_t810,
	RuntimeInvoker_Int32_t392_Int32_t392_TrackableResultData_t810,
	RuntimeInvoker_TrackableResultData_t810_Int32_t392_TrackableResultData_t810,
	RuntimeInvoker_Boolean_t393_Int32_t392_TrackableResultDataU26_t3627,
	RuntimeInvoker_TrackableResultData_t810_Object_t,
	RuntimeInvoker_Enumerator_t2885,
	RuntimeInvoker_DictionaryEntry_t1423_Int32_t392_TrackableResultData_t810,
	RuntimeInvoker_KeyValuePair_2_t2881,
	RuntimeInvoker_Enumerator_t2884,
	RuntimeInvoker_Object_t_Int32_t392_TrackableResultData_t810_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2888,
	RuntimeInvoker_KeyValuePair_2_t2881_Object_t,
	RuntimeInvoker_Boolean_t393_TrackableResultData_t810_TrackableResultData_t810,
	RuntimeInvoker_KeyValuePair_2_t2896_Int32_t392_VirtualButtonData_t811,
	RuntimeInvoker_Int32_t392_Int32_t392_VirtualButtonData_t811,
	RuntimeInvoker_VirtualButtonData_t811_Int32_t392_VirtualButtonData_t811,
	RuntimeInvoker_Boolean_t393_Int32_t392_VirtualButtonDataU26_t3628,
	RuntimeInvoker_VirtualButtonData_t811_Object_t,
	RuntimeInvoker_Enumerator_t2901,
	RuntimeInvoker_DictionaryEntry_t1423_Int32_t392_VirtualButtonData_t811,
	RuntimeInvoker_KeyValuePair_2_t2896,
	RuntimeInvoker_VirtualButtonData_t811,
	RuntimeInvoker_Enumerator_t2900,
	RuntimeInvoker_Object_t_Int32_t392_VirtualButtonData_t811_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2904,
	RuntimeInvoker_KeyValuePair_2_t2896_Object_t,
	RuntimeInvoker_Boolean_t393_VirtualButtonData_t811_VirtualButtonData_t811,
	RuntimeInvoker_TargetSearchResult_t884_Object_t,
	RuntimeInvoker_Enumerator_t2915,
	RuntimeInvoker_Boolean_t393_TargetSearchResult_t884_TargetSearchResult_t884,
	RuntimeInvoker_Object_t_TargetSearchResult_t884_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_TargetSearchResult_t884_TargetSearchResult_t884,
	RuntimeInvoker_Object_t_TargetSearchResult_t884_TargetSearchResult_t884_Object_t_Object_t,
	RuntimeInvoker_Void_t1449_Object_t_ProfileData_t898,
	RuntimeInvoker_KeyValuePair_2_t2935_Object_t_ProfileData_t898,
	RuntimeInvoker_Object_t_Object_t_ProfileData_t898,
	RuntimeInvoker_ProfileData_t898_Object_t_ProfileData_t898,
	RuntimeInvoker_Boolean_t393_Object_t_ProfileDataU26_t3629,
	RuntimeInvoker_Enumerator_t2940,
	RuntimeInvoker_DictionaryEntry_t1423_Object_t_ProfileData_t898,
	RuntimeInvoker_KeyValuePair_2_t2935,
	RuntimeInvoker_Enumerator_t2939,
	RuntimeInvoker_Object_t_Object_t_ProfileData_t898_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2943,
	RuntimeInvoker_KeyValuePair_2_t2935_Object_t,
	RuntimeInvoker_Boolean_t393_ProfileData_t898_ProfileData_t898,
	RuntimeInvoker_Link_t2983,
	RuntimeInvoker_ClientCertificateType_t1203,
	RuntimeInvoker_KeyValuePair_2_t3005_Object_t_SByte_t1443,
	RuntimeInvoker_Boolean_t393_Object_t_BooleanU26_t3515,
	RuntimeInvoker_Enumerator_t3010,
	RuntimeInvoker_DictionaryEntry_t1423_Object_t_SByte_t1443,
	RuntimeInvoker_KeyValuePair_2_t3005,
	RuntimeInvoker_Enumerator_t3009,
	RuntimeInvoker_Enumerator_t3013,
	RuntimeInvoker_KeyValuePair_2_t3005_Object_t,
	RuntimeInvoker_X509ChainStatus_t1333,
	RuntimeInvoker_Mark_t1375,
	RuntimeInvoker_UriScheme_t1412,
	RuntimeInvoker_TableRange_t1480,
	RuntimeInvoker_Slot_t1557,
	RuntimeInvoker_Slot_t1565,
	RuntimeInvoker_ILTokenInfo_t1645,
	RuntimeInvoker_LabelData_t1647,
	RuntimeInvoker_LabelFixup_t1646,
	RuntimeInvoker_CustomAttributeTypedArgument_t1692,
	RuntimeInvoker_CustomAttributeNamedArgument_t1691,
	RuntimeInvoker_CustomAttributeTypedArgument_t1692_Object_t,
	RuntimeInvoker_Enumerator_t3068,
	RuntimeInvoker_Boolean_t393_CustomAttributeTypedArgument_t1692_CustomAttributeTypedArgument_t1692,
	RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1692_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_CustomAttributeTypedArgument_t1692_CustomAttributeTypedArgument_t1692,
	RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1692_CustomAttributeTypedArgument_t1692_Object_t_Object_t,
	RuntimeInvoker_CustomAttributeNamedArgument_t1691_Object_t,
	RuntimeInvoker_Enumerator_t3079,
	RuntimeInvoker_Boolean_t393_CustomAttributeNamedArgument_t1691_CustomAttributeNamedArgument_t1691,
	RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1691_Object_t_Object_t,
	RuntimeInvoker_Int32_t392_CustomAttributeNamedArgument_t1691_CustomAttributeNamedArgument_t1691,
	RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1691_CustomAttributeNamedArgument_t1691_Object_t_Object_t,
	RuntimeInvoker_ResourceInfo_t1721,
	RuntimeInvoker_ResourceCacheItem_t1722,
	RuntimeInvoker_TypeTag_t1858,
	RuntimeInvoker_Int32_t392_DateTimeOffset_t2014_DateTimeOffset_t2014,
	RuntimeInvoker_Boolean_t393_DateTimeOffset_t2014_DateTimeOffset_t2014,
	RuntimeInvoker_Boolean_t393_Nullable_1_t2120,
	RuntimeInvoker_Int32_t392_Guid_t2036_Guid_t2036,
	RuntimeInvoker_Boolean_t393_Guid_t2036_Guid_t2036,
};
