﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Contexts.ContextCallbackObject
struct ContextCallbackObject_t1774;
// System.Runtime.Remoting.Contexts.CrossContextDelegate
struct CrossContextDelegate_t2073;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Contexts.ContextCallbackObject::.ctor()
extern "C" void ContextCallbackObject__ctor_m10597 (ContextCallbackObject_t1774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Contexts.ContextCallbackObject::DoCallBack(System.Runtime.Remoting.Contexts.CrossContextDelegate)
extern "C" void ContextCallbackObject_DoCallBack_m10598 (ContextCallbackObject_t1774 * __this, CrossContextDelegate_t2073 * ___deleg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
