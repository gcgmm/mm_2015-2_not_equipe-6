﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t1038;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2277;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t687;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>[]
struct KeyValuePair_2U5BU5D_t3240;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>
struct IEnumerator_1_t3241;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1238;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct KeyCollection_t2899;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct ValueCollection_t2903;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_30.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__30.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor()
extern "C" void Dictionary_2__ctor_m5486_gshared (Dictionary_2_t1038 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m5486(__this, method) (( void (*) (Dictionary_2_t1038 *, const MethodInfo*))Dictionary_2__ctor_m5486_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m23067_gshared (Dictionary_2_t1038 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m23067(__this, ___comparer, method) (( void (*) (Dictionary_2_t1038 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m23067_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m23068_gshared (Dictionary_2_t1038 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m23068(__this, ___capacity, method) (( void (*) (Dictionary_2_t1038 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m23068_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m23069_gshared (Dictionary_2_t1038 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m23069(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1038 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2__ctor_m23069_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m23070_gshared (Dictionary_2_t1038 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m23070(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1038 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m23070_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m23071_gshared (Dictionary_2_t1038 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m23071(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1038 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m23071_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m23072_gshared (Dictionary_2_t1038 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m23072(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1038 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m23072_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m23073_gshared (Dictionary_2_t1038 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m23073(__this, ___key, method) (( bool (*) (Dictionary_2_t1038 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m23073_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m23074_gshared (Dictionary_2_t1038 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m23074(__this, ___key, method) (( void (*) (Dictionary_2_t1038 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m23074_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m23075_gshared (Dictionary_2_t1038 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m23075(__this, method) (( bool (*) (Dictionary_2_t1038 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m23075_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m23076_gshared (Dictionary_2_t1038 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m23076(__this, method) (( Object_t * (*) (Dictionary_2_t1038 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m23076_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m23077_gshared (Dictionary_2_t1038 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m23077(__this, method) (( bool (*) (Dictionary_2_t1038 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m23077_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m23078_gshared (Dictionary_2_t1038 * __this, KeyValuePair_2_t2896  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m23078(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1038 *, KeyValuePair_2_t2896 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m23078_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m23079_gshared (Dictionary_2_t1038 * __this, KeyValuePair_2_t2896  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m23079(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1038 *, KeyValuePair_2_t2896 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m23079_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m23080_gshared (Dictionary_2_t1038 * __this, KeyValuePair_2U5BU5D_t3240* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m23080(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1038 *, KeyValuePair_2U5BU5D_t3240*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m23080_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m23081_gshared (Dictionary_2_t1038 * __this, KeyValuePair_2_t2896  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m23081(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1038 *, KeyValuePair_2_t2896 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m23081_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m23082_gshared (Dictionary_2_t1038 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m23082(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1038 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m23082_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m23083_gshared (Dictionary_2_t1038 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m23083(__this, method) (( Object_t * (*) (Dictionary_2_t1038 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m23083_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m23084_gshared (Dictionary_2_t1038 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m23084(__this, method) (( Object_t* (*) (Dictionary_2_t1038 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m23084_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m23085_gshared (Dictionary_2_t1038 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m23085(__this, method) (( Object_t * (*) (Dictionary_2_t1038 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m23085_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m23086_gshared (Dictionary_2_t1038 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m23086(__this, method) (( int32_t (*) (Dictionary_2_t1038 *, const MethodInfo*))Dictionary_2_get_Count_m23086_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Item(TKey)
extern "C" VirtualButtonData_t811  Dictionary_2_get_Item_m23087_gshared (Dictionary_2_t1038 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m23087(__this, ___key, method) (( VirtualButtonData_t811  (*) (Dictionary_2_t1038 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m23087_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m23088_gshared (Dictionary_2_t1038 * __this, int32_t ___key, VirtualButtonData_t811  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m23088(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1038 *, int32_t, VirtualButtonData_t811 , const MethodInfo*))Dictionary_2_set_Item_m23088_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m23089_gshared (Dictionary_2_t1038 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m23089(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1038 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m23089_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m23090_gshared (Dictionary_2_t1038 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m23090(__this, ___size, method) (( void (*) (Dictionary_2_t1038 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m23090_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m23091_gshared (Dictionary_2_t1038 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m23091(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1038 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m23091_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2896  Dictionary_2_make_pair_m23092_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t811  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m23092(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2896  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t811 , const MethodInfo*))Dictionary_2_make_pair_m23092_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m23093_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t811  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m23093(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t811 , const MethodInfo*))Dictionary_2_pick_key_m23093_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::pick_value(TKey,TValue)
extern "C" VirtualButtonData_t811  Dictionary_2_pick_value_m23094_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t811  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m23094(__this /* static, unused */, ___key, ___value, method) (( VirtualButtonData_t811  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t811 , const MethodInfo*))Dictionary_2_pick_value_m23094_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m23095_gshared (Dictionary_2_t1038 * __this, KeyValuePair_2U5BU5D_t3240* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m23095(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1038 *, KeyValuePair_2U5BU5D_t3240*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m23095_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Resize()
extern "C" void Dictionary_2_Resize_m23096_gshared (Dictionary_2_t1038 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m23096(__this, method) (( void (*) (Dictionary_2_t1038 *, const MethodInfo*))Dictionary_2_Resize_m23096_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m23097_gshared (Dictionary_2_t1038 * __this, int32_t ___key, VirtualButtonData_t811  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m23097(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1038 *, int32_t, VirtualButtonData_t811 , const MethodInfo*))Dictionary_2_Add_m23097_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Clear()
extern "C" void Dictionary_2_Clear_m23098_gshared (Dictionary_2_t1038 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m23098(__this, method) (( void (*) (Dictionary_2_t1038 *, const MethodInfo*))Dictionary_2_Clear_m23098_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m23099_gshared (Dictionary_2_t1038 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m23099(__this, ___key, method) (( bool (*) (Dictionary_2_t1038 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m23099_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m23100_gshared (Dictionary_2_t1038 * __this, VirtualButtonData_t811  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m23100(__this, ___value, method) (( bool (*) (Dictionary_2_t1038 *, VirtualButtonData_t811 , const MethodInfo*))Dictionary_2_ContainsValue_m23100_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m23101_gshared (Dictionary_2_t1038 * __this, SerializationInfo_t687 * ___info, StreamingContext_t688  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m23101(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1038 *, SerializationInfo_t687 *, StreamingContext_t688 , const MethodInfo*))Dictionary_2_GetObjectData_m23101_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m23102_gshared (Dictionary_2_t1038 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m23102(__this, ___sender, method) (( void (*) (Dictionary_2_t1038 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m23102_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m23103_gshared (Dictionary_2_t1038 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m23103(__this, ___key, method) (( bool (*) (Dictionary_2_t1038 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m23103_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m23104_gshared (Dictionary_2_t1038 * __this, int32_t ___key, VirtualButtonData_t811 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m23104(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1038 *, int32_t, VirtualButtonData_t811 *, const MethodInfo*))Dictionary_2_TryGetValue_m23104_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Keys()
extern "C" KeyCollection_t2899 * Dictionary_2_get_Keys_m23105_gshared (Dictionary_2_t1038 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m23105(__this, method) (( KeyCollection_t2899 * (*) (Dictionary_2_t1038 *, const MethodInfo*))Dictionary_2_get_Keys_m23105_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Values()
extern "C" ValueCollection_t2903 * Dictionary_2_get_Values_m23106_gshared (Dictionary_2_t1038 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m23106(__this, method) (( ValueCollection_t2903 * (*) (Dictionary_2_t1038 *, const MethodInfo*))Dictionary_2_get_Values_m23106_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m23107_gshared (Dictionary_2_t1038 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m23107(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1038 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m23107_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToTValue(System.Object)
extern "C" VirtualButtonData_t811  Dictionary_2_ToTValue_m23108_gshared (Dictionary_2_t1038 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m23108(__this, ___value, method) (( VirtualButtonData_t811  (*) (Dictionary_2_t1038 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m23108_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m23109_gshared (Dictionary_2_t1038 * __this, KeyValuePair_2_t2896  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m23109(__this, ___pair, method) (( bool (*) (Dictionary_2_t1038 *, KeyValuePair_2_t2896 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m23109_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::GetEnumerator()
extern "C" Enumerator_t2901  Dictionary_2_GetEnumerator_m23110_gshared (Dictionary_2_t1038 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m23110(__this, method) (( Enumerator_t2901  (*) (Dictionary_2_t1038 *, const MethodInfo*))Dictionary_2_GetEnumerator_m23110_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1423  Dictionary_2_U3CCopyToU3Em__0_m23111_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t811  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m23111(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1423  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t811 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m23111_gshared)(__this /* static, unused */, ___key, ___value, method)
