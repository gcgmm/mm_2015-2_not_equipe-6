﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
struct Collection_1_t2586;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t683;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t3183;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t431;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Collection_1__ctor_m18264_gshared (Collection_1_t2586 * __this, const MethodInfo* method);
#define Collection_1__ctor_m18264(__this, method) (( void (*) (Collection_1_t2586 *, const MethodInfo*))Collection_1__ctor_m18264_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18265_gshared (Collection_1_t2586 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18265(__this, method) (( bool (*) (Collection_1_t2586 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18265_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m18266_gshared (Collection_1_t2586 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m18266(__this, ___array, ___index, method) (( void (*) (Collection_1_t2586 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m18266_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m18267_gshared (Collection_1_t2586 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m18267(__this, method) (( Object_t * (*) (Collection_1_t2586 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m18267_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m18268_gshared (Collection_1_t2586 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m18268(__this, ___value, method) (( int32_t (*) (Collection_1_t2586 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m18268_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m18269_gshared (Collection_1_t2586 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m18269(__this, ___value, method) (( bool (*) (Collection_1_t2586 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m18269_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m18270_gshared (Collection_1_t2586 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m18270(__this, ___value, method) (( int32_t (*) (Collection_1_t2586 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m18270_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m18271_gshared (Collection_1_t2586 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m18271(__this, ___index, ___value, method) (( void (*) (Collection_1_t2586 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m18271_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m18272_gshared (Collection_1_t2586 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m18272(__this, ___value, method) (( void (*) (Collection_1_t2586 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m18272_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m18273_gshared (Collection_1_t2586 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m18273(__this, method) (( bool (*) (Collection_1_t2586 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m18273_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m18274_gshared (Collection_1_t2586 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m18274(__this, method) (( Object_t * (*) (Collection_1_t2586 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m18274_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m18275_gshared (Collection_1_t2586 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m18275(__this, method) (( bool (*) (Collection_1_t2586 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m18275_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m18276_gshared (Collection_1_t2586 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m18276(__this, method) (( bool (*) (Collection_1_t2586 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m18276_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m18277_gshared (Collection_1_t2586 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m18277(__this, ___index, method) (( Object_t * (*) (Collection_1_t2586 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m18277_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m18278_gshared (Collection_1_t2586 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m18278(__this, ___index, ___value, method) (( void (*) (Collection_1_t2586 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m18278_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void Collection_1_Add_m18279_gshared (Collection_1_t2586 * __this, UICharInfo_t430  ___item, const MethodInfo* method);
#define Collection_1_Add_m18279(__this, ___item, method) (( void (*) (Collection_1_t2586 *, UICharInfo_t430 , const MethodInfo*))Collection_1_Add_m18279_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Clear()
extern "C" void Collection_1_Clear_m18280_gshared (Collection_1_t2586 * __this, const MethodInfo* method);
#define Collection_1_Clear_m18280(__this, method) (( void (*) (Collection_1_t2586 *, const MethodInfo*))Collection_1_Clear_m18280_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m18281_gshared (Collection_1_t2586 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m18281(__this, method) (( void (*) (Collection_1_t2586 *, const MethodInfo*))Collection_1_ClearItems_m18281_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m18282_gshared (Collection_1_t2586 * __this, UICharInfo_t430  ___item, const MethodInfo* method);
#define Collection_1_Contains_m18282(__this, ___item, method) (( bool (*) (Collection_1_t2586 *, UICharInfo_t430 , const MethodInfo*))Collection_1_Contains_m18282_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m18283_gshared (Collection_1_t2586 * __this, UICharInfoU5BU5D_t683* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m18283(__this, ___array, ___index, method) (( void (*) (Collection_1_t2586 *, UICharInfoU5BU5D_t683*, int32_t, const MethodInfo*))Collection_1_CopyTo_m18283_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m18284_gshared (Collection_1_t2586 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m18284(__this, method) (( Object_t* (*) (Collection_1_t2586 *, const MethodInfo*))Collection_1_GetEnumerator_m18284_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m18285_gshared (Collection_1_t2586 * __this, UICharInfo_t430  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m18285(__this, ___item, method) (( int32_t (*) (Collection_1_t2586 *, UICharInfo_t430 , const MethodInfo*))Collection_1_IndexOf_m18285_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m18286_gshared (Collection_1_t2586 * __this, int32_t ___index, UICharInfo_t430  ___item, const MethodInfo* method);
#define Collection_1_Insert_m18286(__this, ___index, ___item, method) (( void (*) (Collection_1_t2586 *, int32_t, UICharInfo_t430 , const MethodInfo*))Collection_1_Insert_m18286_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m18287_gshared (Collection_1_t2586 * __this, int32_t ___index, UICharInfo_t430  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m18287(__this, ___index, ___item, method) (( void (*) (Collection_1_t2586 *, int32_t, UICharInfo_t430 , const MethodInfo*))Collection_1_InsertItem_m18287_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m18288_gshared (Collection_1_t2586 * __this, UICharInfo_t430  ___item, const MethodInfo* method);
#define Collection_1_Remove_m18288(__this, ___item, method) (( bool (*) (Collection_1_t2586 *, UICharInfo_t430 , const MethodInfo*))Collection_1_Remove_m18288_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m18289_gshared (Collection_1_t2586 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m18289(__this, ___index, method) (( void (*) (Collection_1_t2586 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m18289_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m18290_gshared (Collection_1_t2586 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m18290(__this, ___index, method) (( void (*) (Collection_1_t2586 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m18290_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m18291_gshared (Collection_1_t2586 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m18291(__this, method) (( int32_t (*) (Collection_1_t2586 *, const MethodInfo*))Collection_1_get_Count_m18291_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t430  Collection_1_get_Item_m18292_gshared (Collection_1_t2586 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m18292(__this, ___index, method) (( UICharInfo_t430  (*) (Collection_1_t2586 *, int32_t, const MethodInfo*))Collection_1_get_Item_m18292_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m18293_gshared (Collection_1_t2586 * __this, int32_t ___index, UICharInfo_t430  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m18293(__this, ___index, ___value, method) (( void (*) (Collection_1_t2586 *, int32_t, UICharInfo_t430 , const MethodInfo*))Collection_1_set_Item_m18293_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m18294_gshared (Collection_1_t2586 * __this, int32_t ___index, UICharInfo_t430  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m18294(__this, ___index, ___item, method) (( void (*) (Collection_1_t2586 *, int32_t, UICharInfo_t430 , const MethodInfo*))Collection_1_SetItem_m18294_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m18295_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m18295(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m18295_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ConvertItem(System.Object)
extern "C" UICharInfo_t430  Collection_1_ConvertItem_m18296_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m18296(__this /* static, unused */, ___item, method) (( UICharInfo_t430  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m18296_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m18297_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m18297(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m18297_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m18298_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m18298(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m18298_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m18299_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m18299(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m18299_gshared)(__this /* static, unused */, ___list, method)
