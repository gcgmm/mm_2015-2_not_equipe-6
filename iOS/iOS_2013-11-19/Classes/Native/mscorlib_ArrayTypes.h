﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// System.Object[]
// System.Object[]
struct ObjectU5BU5D_t105  : public Array_t { };
// System.Type[]
// System.Type[]
struct TypeU5BU5D_t679  : public Array_t { };
// System.Reflection.IReflect[]
// System.Reflection.IReflect[]
struct IReflectU5BU5D_t3266  : public Array_t { };
// System.Runtime.InteropServices._Type[]
// System.Runtime.InteropServices._Type[]
struct _TypeU5BU5D_t3267  : public Array_t { };
// System.Reflection.MemberInfo[]
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1864  : public Array_t { };
// System.Reflection.ICustomAttributeProvider[]
// System.Reflection.ICustomAttributeProvider[]
struct ICustomAttributeProviderU5BU5D_t3268  : public Array_t { };
// System.Runtime.InteropServices._MemberInfo[]
// System.Runtime.InteropServices._MemberInfo[]
struct _MemberInfoU5BU5D_t3269  : public Array_t { };
// System.Int32[]
// System.Int32[]
struct Int32U5BU5D_t122  : public Array_t { };
// System.IFormattable[]
// System.IFormattable[]
struct IFormattableU5BU5D_t3270  : public Array_t { };
// System.IConvertible[]
// System.IConvertible[]
struct IConvertibleU5BU5D_t3271  : public Array_t { };
// System.IComparable[]
// System.IComparable[]
struct IComparableU5BU5D_t3272  : public Array_t { };
// System.IComparable`1<System.Int32>[]
// System.IComparable`1<System.Int32>[]
struct IComparable_1U5BU5D_t3273  : public Array_t { };
// System.IEquatable`1<System.Int32>[]
// System.IEquatable`1<System.Int32>[]
struct IEquatable_1U5BU5D_t3274  : public Array_t { };
// System.ValueType[]
// System.ValueType[]
struct ValueTypeU5BU5D_t3275  : public Array_t { };
// System.Double[]
// System.Double[]
struct DoubleU5BU5D_t2104  : public Array_t { };
// System.IComparable`1<System.Double>[]
// System.IComparable`1<System.Double>[]
struct IComparable_1U5BU5D_t3276  : public Array_t { };
// System.IEquatable`1<System.Double>[]
// System.IEquatable`1<System.Double>[]
struct IEquatable_1U5BU5D_t3277  : public Array_t { };
// System.Char[]
// System.Char[]
struct CharU5BU5D_t270  : public Array_t { };
// System.IComparable`1<System.Char>[]
// System.IComparable`1<System.Char>[]
struct IComparable_1U5BU5D_t3278  : public Array_t { };
// System.IEquatable`1<System.Char>[]
// System.IEquatable`1<System.Char>[]
struct IEquatable_1U5BU5D_t3279  : public Array_t { };
// System.String[]
// System.String[]
struct StringU5BU5D_t87  : public Array_t { };
// System.Collections.IEnumerable[]
// System.Collections.IEnumerable[]
struct IEnumerableU5BU5D_t3280  : public Array_t { };
// System.ICloneable[]
// System.ICloneable[]
struct ICloneableU5BU5D_t3281  : public Array_t { };
// System.IComparable`1<System.String>[]
// System.IComparable`1<System.String>[]
struct IComparable_1U5BU5D_t3282  : public Array_t { };
// System.IEquatable`1<System.String>[]
// System.IEquatable`1<System.String>[]
struct IEquatable_1U5BU5D_t3283  : public Array_t { };
// System.Reflection.MethodInfo[]
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t108  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
struct List_1U5BU5D_t2223  : public Array_t { };
// System.Collections.Generic.List`1<System.Object>[]
// System.Collections.Generic.List`1<System.Object>[]
struct List_1U5BU5D_t2235  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Component>[]
// System.Collections.Generic.List`1<UnityEngine.Component>[]
struct List_1U5BU5D_t2237  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t3130  : public Array_t { };
// System.Collections.Generic.Link[]
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t2274  : public Array_t { };
// System.Collections.DictionaryEntry[]
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t3284  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
struct KeyValuePair_2U5BU5D_t3129  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3134  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
struct List_1U5BU5D_t2351  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t3140  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
struct List_1U5BU5D_t2354  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
struct KeyValuePair_2U5BU5D_t3139  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
struct KeyValuePair_2U5BU5D_t3146  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3147  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3138  : public Array_t { };
// System.Enum[]
// System.Enum[]
struct EnumU5BU5D_t3285  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3154  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3159  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Vector3>[]
// System.Collections.Generic.List`1<UnityEngine.Vector3>[]
struct List_1U5BU5D_t2533  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Color32>[]
// System.Collections.Generic.List`1<UnityEngine.Color32>[]
struct List_1U5BU5D_t2537  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Vector2>[]
// System.Collections.Generic.List`1<UnityEngine.Vector2>[]
struct List_1U5BU5D_t2541  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Vector4>[]
// System.Collections.Generic.List`1<UnityEngine.Vector4>[]
struct List_1U5BU5D_t2545  : public Array_t { };
// System.Collections.Generic.List`1<System.Int32>[]
// System.Collections.Generic.List`1<System.Int32>[]
struct List_1U5BU5D_t2549  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
struct List_1U5BU5D_t2553  : public Array_t { };
// System.Single[]
// System.Single[]
struct SingleU5BU5D_t547  : public Array_t { };
// System.IComparable`1<System.Single>[]
// System.IComparable`1<System.Single>[]
struct IComparable_1U5BU5D_t3286  : public Array_t { };
// System.IEquatable`1<System.Single>[]
// System.IEquatable`1<System.Single>[]
struct IEquatable_1U5BU5D_t3287  : public Array_t { };
// System.IntPtr[]
// System.IntPtr[]
struct IntPtrU5BU5D_t678  : public Array_t { };
// System.Runtime.Serialization.ISerializable[]
// System.Runtime.Serialization.ISerializable[]
struct ISerializableU5BU5D_t3288  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
struct KeyValuePair_2U5BU5D_t3187  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
struct KeyValuePair_2U5BU5D_t3190  : public Array_t { };
// System.Attribute[]
// System.Attribute[]
struct AttributeU5BU5D_t3289  : public Array_t { };
// System.Runtime.InteropServices._Attribute[]
// System.Runtime.InteropServices._Attribute[]
struct _AttributeU5BU5D_t3290  : public Array_t { };
// System.Reflection.ParameterModifier[]
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t708  : public Array_t { };
// System.Reflection.ParameterInfo[]
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t715  : public Array_t { };
// System.Runtime.InteropServices._ParameterInfo[]
// System.Runtime.InteropServices._ParameterInfo[]
struct _ParameterInfoU5BU5D_t3291  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t3193  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t3192  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t3198  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>[]
struct KeyValuePair_2U5BU5D_t3201  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
struct KeyValuePair_2U5BU5D_t3200  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>[]
struct KeyValuePair_2U5BU5D_t3207  : public Array_t { };
// System.Byte[]
// System.Byte[]
struct ByteU5BU5D_t786  : public Array_t { };
// System.IComparable`1<System.Byte>[]
// System.IComparable`1<System.Byte>[]
struct IComparable_1U5BU5D_t3292  : public Array_t { };
// System.IEquatable`1<System.Byte>[]
// System.IEquatable`1<System.Byte>[]
struct IEquatable_1U5BU5D_t3293  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
struct KeyValuePair_2U5BU5D_t3210  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
struct KeyValuePair_2U5BU5D_t3212  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t3215  : public Array_t { };
// System.UInt16[]
// System.UInt16[]
struct UInt16U5BU5D_t1306  : public Array_t { };
// System.IComparable`1<System.UInt16>[]
// System.IComparable`1<System.UInt16>[]
struct IComparable_1U5BU5D_t3294  : public Array_t { };
// System.IEquatable`1<System.UInt16>[]
// System.IEquatable`1<System.UInt16>[]
struct IEquatable_1U5BU5D_t3295  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t3214  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
struct KeyValuePair_2U5BU5D_t3220  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t3223  : public Array_t { };
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
struct List_1U5BU5D_t2831  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
struct KeyValuePair_2U5BU5D_t3225  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
struct KeyValuePair_2U5BU5D_t3227  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t3229  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
struct KeyValuePair_2U5BU5D_t3231  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t3233  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
struct KeyValuePair_2U5BU5D_t3235  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>[]
struct KeyValuePair_2U5BU5D_t3237  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>[]
struct KeyValuePair_2U5BU5D_t3240  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
struct KeyValuePair_2U5BU5D_t3244  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t3248  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t3247  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t3253  : public Array_t { };
// System.Security.Cryptography.KeySizes[]
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t1086  : public Array_t { };
// System.UInt32[]
// System.UInt32[]
struct UInt32U5BU5D_t1073  : public Array_t { };
// System.IComparable`1<System.UInt32>[]
// System.IComparable`1<System.UInt32>[]
struct IComparable_1U5BU5D_t3296  : public Array_t { };
// System.IEquatable`1<System.UInt32>[]
// System.IEquatable`1<System.UInt32>[]
struct IEquatable_1U5BU5D_t3297  : public Array_t { };
// System.Byte[][]
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t1250  : public Array_t { };
// System.Array[]
// System.Array[]
struct ArrayU5BU5D_t3298  : public Array_t { };
// System.Collections.ICollection[]
// System.Collections.ICollection[]
struct ICollectionU5BU5D_t3299  : public Array_t { };
// System.Collections.IList[]
// System.Collections.IList[]
struct IListU5BU5D_t3300  : public Array_t { };
// System.Security.Cryptography.X509Certificates.X509Certificate[]
// System.Security.Cryptography.X509Certificates.X509Certificate[]
struct X509CertificateU5BU5D_t1264  : public Array_t { };
// System.Runtime.Serialization.IDeserializationCallback[]
// System.Runtime.Serialization.IDeserializationCallback[]
struct IDeserializationCallbackU5BU5D_t3301  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t3257  : public Array_t { };
// System.Boolean[]
// System.Boolean[]
struct BooleanU5BU5D_t1311  : public Array_t { };
// System.IComparable`1<System.Boolean>[]
// System.IComparable`1<System.Boolean>[]
struct IComparable_1U5BU5D_t3302  : public Array_t { };
// System.IEquatable`1<System.Boolean>[]
// System.IEquatable`1<System.Boolean>[]
struct IEquatable_1U5BU5D_t3303  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t3256  : public Array_t { };
// System.Single[,]
// System.Single[,]
struct SingleU5BU2CU5D_t3304  : public Array_t { };
// System.Delegate[]
// System.Delegate[]
struct DelegateU5BU5D_t2102  : public Array_t { };
// System.UInt64[]
// System.UInt64[]
struct UInt64U5BU5D_t1921  : public Array_t { };
// System.IComparable`1<System.UInt64>[]
// System.IComparable`1<System.UInt64>[]
struct IComparable_1U5BU5D_t3305  : public Array_t { };
// System.IEquatable`1<System.UInt64>[]
// System.IEquatable`1<System.UInt64>[]
struct IEquatable_1U5BU5D_t3306  : public Array_t { };
// System.Int16[]
// System.Int16[]
struct Int16U5BU5D_t2125  : public Array_t { };
// System.IComparable`1<System.Int16>[]
// System.IComparable`1<System.Int16>[]
struct IComparable_1U5BU5D_t3307  : public Array_t { };
// System.IEquatable`1<System.Int16>[]
// System.IEquatable`1<System.Int16>[]
struct IEquatable_1U5BU5D_t3308  : public Array_t { };
// System.SByte[]
// System.SByte[]
struct SByteU5BU5D_t1976  : public Array_t { };
// System.IComparable`1<System.SByte>[]
// System.IComparable`1<System.SByte>[]
struct IComparable_1U5BU5D_t3309  : public Array_t { };
// System.IEquatable`1<System.SByte>[]
// System.IEquatable`1<System.SByte>[]
struct IEquatable_1U5BU5D_t3310  : public Array_t { };
// System.Int64[]
// System.Int64[]
struct Int64U5BU5D_t2103  : public Array_t { };
// System.IComparable`1<System.Int64>[]
// System.IComparable`1<System.Int64>[]
struct IComparable_1U5BU5D_t3311  : public Array_t { };
// System.IEquatable`1<System.Int64>[]
// System.IEquatable`1<System.Int64>[]
struct IEquatable_1U5BU5D_t3312  : public Array_t { };
// System.Reflection.FieldInfo[]
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t2108  : public Array_t { };
// System.Runtime.InteropServices._FieldInfo[]
// System.Runtime.InteropServices._FieldInfo[]
struct _FieldInfoU5BU5D_t3313  : public Array_t { };
// System.Reflection.ConstructorInfo[]
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t2107  : public Array_t { };
// System.Runtime.InteropServices._ConstructorInfo[]
// System.Runtime.InteropServices._ConstructorInfo[]
struct _ConstructorInfoU5BU5D_t3314  : public Array_t { };
// System.Reflection.MethodBase[]
// System.Reflection.MethodBase[]
struct MethodBaseU5BU5D_t2109  : public Array_t { };
// System.Runtime.InteropServices._MethodBase[]
// System.Runtime.InteropServices._MethodBase[]
struct _MethodBaseU5BU5D_t3315  : public Array_t { };
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct TableRangeU5BU5D_t1482  : public Array_t { };
// Mono.Globalization.Unicode.TailoringInfo[]
// Mono.Globalization.Unicode.TailoringInfo[]
struct TailoringInfoU5BU5D_t1489  : public Array_t { };
// Mono.Globalization.Unicode.Contraction[]
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t1497  : public Array_t { };
// Mono.Globalization.Unicode.Level2Map[]
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t1498  : public Array_t { };
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t2105  : public Array_t { };
// System.Collections.Hashtable/Slot[]
// System.Collections.Hashtable/Slot[]
struct SlotU5BU5D_t1564  : public Array_t { };
// System.Collections.SortedList/Slot[]
// System.Collections.SortedList/Slot[]
struct SlotU5BU5D_t1568  : public Array_t { };
// System.Diagnostics.StackFrame[]
// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t1577  : public Array_t { };
// System.Globalization.Calendar[]
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t1585  : public Array_t { };
// System.Reflection.Emit.ModuleBuilder[]
// System.Reflection.Emit.ModuleBuilder[]
struct ModuleBuilderU5BU5D_t1633  : public Array_t { };
// System.Runtime.InteropServices._ModuleBuilder[]
// System.Runtime.InteropServices._ModuleBuilder[]
struct _ModuleBuilderU5BU5D_t3316  : public Array_t { };
// System.Reflection.Module[]
// System.Reflection.Module[]
struct ModuleU5BU5D_t1634  : public Array_t { };
// System.Runtime.InteropServices._Module[]
// System.Runtime.InteropServices._Module[]
struct _ModuleU5BU5D_t3317  : public Array_t { };
// System.Reflection.Emit.ParameterBuilder[]
// System.Reflection.Emit.ParameterBuilder[]
struct ParameterBuilderU5BU5D_t1638  : public Array_t { };
// System.Runtime.InteropServices._ParameterBuilder[]
// System.Runtime.InteropServices._ParameterBuilder[]
struct _ParameterBuilderU5BU5D_t3318  : public Array_t { };
// System.Type[][]
// System.Type[][]
struct TypeU5BU5DU5BU5D_t1639  : public Array_t { };
// System.Reflection.Emit.ILTokenInfo[]
// System.Reflection.Emit.ILTokenInfo[]
struct ILTokenInfoU5BU5D_t1648  : public Array_t { };
// System.Reflection.Emit.ILGenerator/LabelData[]
// System.Reflection.Emit.ILGenerator/LabelData[]
struct LabelDataU5BU5D_t1649  : public Array_t { };
// System.Reflection.Emit.ILGenerator/LabelFixup[]
// System.Reflection.Emit.ILGenerator/LabelFixup[]
struct LabelFixupU5BU5D_t1650  : public Array_t { };
// System.Reflection.Emit.GenericTypeParameterBuilder[]
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct GenericTypeParameterBuilderU5BU5D_t1653  : public Array_t { };
// System.Reflection.Emit.TypeBuilder[]
// System.Reflection.Emit.TypeBuilder[]
struct TypeBuilderU5BU5D_t1656  : public Array_t { };
// System.Runtime.InteropServices._TypeBuilder[]
// System.Runtime.InteropServices._TypeBuilder[]
struct _TypeBuilderU5BU5D_t3319  : public Array_t { };
// System.Reflection.Emit.MethodBuilder[]
// System.Reflection.Emit.MethodBuilder[]
struct MethodBuilderU5BU5D_t1663  : public Array_t { };
// System.Runtime.InteropServices._MethodBuilder[]
// System.Runtime.InteropServices._MethodBuilder[]
struct _MethodBuilderU5BU5D_t3320  : public Array_t { };
// System.Runtime.InteropServices._MethodInfo[]
// System.Runtime.InteropServices._MethodInfo[]
struct _MethodInfoU5BU5D_t3321  : public Array_t { };
// System.Reflection.Emit.ConstructorBuilder[]
// System.Reflection.Emit.ConstructorBuilder[]
struct ConstructorBuilderU5BU5D_t1664  : public Array_t { };
// System.Runtime.InteropServices._ConstructorBuilder[]
// System.Runtime.InteropServices._ConstructorBuilder[]
struct _ConstructorBuilderU5BU5D_t3322  : public Array_t { };
// System.Reflection.Emit.FieldBuilder[]
// System.Reflection.Emit.FieldBuilder[]
struct FieldBuilderU5BU5D_t1665  : public Array_t { };
// System.Runtime.InteropServices._FieldBuilder[]
// System.Runtime.InteropServices._FieldBuilder[]
struct _FieldBuilderU5BU5D_t3323  : public Array_t { };
// System.Reflection.PropertyInfo[]
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t2110  : public Array_t { };
// System.Runtime.InteropServices._PropertyInfo[]
// System.Runtime.InteropServices._PropertyInfo[]
struct _PropertyInfoU5BU5D_t3324  : public Array_t { };
// System.Reflection.CustomAttributeTypedArgument[]
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2129  : public Array_t { };
// System.Reflection.CustomAttributeNamedArgument[]
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2130  : public Array_t { };
// System.Resources.ResourceReader/ResourceInfo[]
// System.Resources.ResourceReader/ResourceInfo[]
struct ResourceInfoU5BU5D_t1725  : public Array_t { };
// System.Resources.ResourceReader/ResourceCacheItem[]
// System.Resources.ResourceReader/ResourceCacheItem[]
struct ResourceCacheItemU5BU5D_t1726  : public Array_t { };
// System.Runtime.Remoting.Contexts.IContextProperty[]
// System.Runtime.Remoting.Contexts.IContextProperty[]
struct IContextPropertyU5BU5D_t2115  : public Array_t { };
// System.Runtime.Remoting.Messaging.Header[]
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t2075  : public Array_t { };
// System.Runtime.Remoting.Services.ITrackingHandler[]
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct ITrackingHandlerU5BU5D_t2150  : public Array_t { };
// System.Runtime.Remoting.Contexts.IContextAttribute[]
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct IContextAttributeU5BU5D_t2119  : public Array_t { };
// System.DateTime[]
// System.DateTime[]
struct DateTimeU5BU5D_t2152  : public Array_t { };
// System.IComparable`1<System.DateTime>[]
// System.IComparable`1<System.DateTime>[]
struct IComparable_1U5BU5D_t3325  : public Array_t { };
// System.IEquatable`1<System.DateTime>[]
// System.IEquatable`1<System.DateTime>[]
struct IEquatable_1U5BU5D_t3326  : public Array_t { };
// System.Decimal[]
// System.Decimal[]
struct DecimalU5BU5D_t2153  : public Array_t { };
// System.IComparable`1<System.Decimal>[]
// System.IComparable`1<System.Decimal>[]
struct IComparable_1U5BU5D_t3327  : public Array_t { };
// System.IEquatable`1<System.Decimal>[]
// System.IEquatable`1<System.Decimal>[]
struct IEquatable_1U5BU5D_t3328  : public Array_t { };
// System.TimeSpan[]
// System.TimeSpan[]
struct TimeSpanU5BU5D_t2154  : public Array_t { };
// System.IComparable`1<System.TimeSpan>[]
// System.IComparable`1<System.TimeSpan>[]
struct IComparable_1U5BU5D_t3329  : public Array_t { };
// System.IEquatable`1<System.TimeSpan>[]
// System.IEquatable`1<System.TimeSpan>[]
struct IEquatable_1U5BU5D_t3330  : public Array_t { };
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct TypeTagU5BU5D_t2155  : public Array_t { };
// System.MonoType[]
// System.MonoType[]
struct MonoTypeU5BU5D_t2158  : public Array_t { };
// System.Byte[,]
// System.Byte[,]
struct ByteU5BU2CU5D_t1895  : public Array_t { };
// System.Security.Policy.StrongName[]
// System.Security.Policy.StrongName[]
struct StrongNameU5BU5D_t3101  : public Array_t { };
// System.Reflection.CustomAttributeData[]
// System.Reflection.CustomAttributeData[]
struct CustomAttributeDataU5BU5D_t2121  : public Array_t { };
