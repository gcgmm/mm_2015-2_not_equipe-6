﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BoardController
struct BoardController_t1;

#include "codegen/il2cpp-codegen.h"

// System.Void BoardController::.ctor()
extern "C" void BoardController__ctor_m0 (BoardController_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BoardController::Awake()
extern "C" void BoardController_Awake_m1 (BoardController_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BoardController::StartNewGame()
extern "C" void BoardController_StartNewGame_m2 (BoardController_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BoardController::Check(System.Int32)
extern "C" void BoardController_Check_m3 (BoardController_t1 * __this, int32_t ___difficulty, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BoardController::GetIndex()
extern "C" int32_t BoardController_GetIndex_m4 (BoardController_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
