﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t35;
// UnityEngine.GameObject
struct GameObject_t9;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::CreateQuad(UnityEngine.GameObject,System.String,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Int32)
extern "C" GameObject_t9 * HideExcessAreaAbstractBehaviour_CreateQuad_m3829 (HideExcessAreaAbstractBehaviour_t35 * __this, GameObject_t9 * ___parent, String_t* ___name, Vector3_t12  ___position, Quaternion_t125  ___rotation, Vector3_t12  ___scale, int32_t ___layer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesActive(System.Boolean)
extern "C" void HideExcessAreaAbstractBehaviour_SetPlanesActive_m3830 (HideExcessAreaAbstractBehaviour_t35 * __this, bool ___activeflag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesRenderingActive(System.Boolean)
extern "C" void HideExcessAreaAbstractBehaviour_SetPlanesRenderingActive_m3831 (HideExcessAreaAbstractBehaviour_t35 * __this, bool ___activeflag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::isPlanesRenderingActive()
extern "C" bool HideExcessAreaAbstractBehaviour_isPlanesRenderingActive_m3832 (HideExcessAreaAbstractBehaviour_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::ForceUpdateHideBehaviours()
extern "C" void HideExcessAreaAbstractBehaviour_ForceUpdateHideBehaviours_m3833 (HideExcessAreaAbstractBehaviour_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnVuforiaStarted()
extern "C" void HideExcessAreaAbstractBehaviour_OnVuforiaStarted_m3834 (HideExcessAreaAbstractBehaviour_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::HasCalculationDataChanged()
extern "C" bool HideExcessAreaAbstractBehaviour_HasCalculationDataChanged_m3835 (HideExcessAreaAbstractBehaviour_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnPreCull()
extern "C" void HideExcessAreaAbstractBehaviour_OnPreCull_m3836 (HideExcessAreaAbstractBehaviour_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnPostRender()
extern "C" void HideExcessAreaAbstractBehaviour_OnPostRender_m3837 (HideExcessAreaAbstractBehaviour_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Start()
extern "C" void HideExcessAreaAbstractBehaviour_Start_m3838 (HideExcessAreaAbstractBehaviour_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDisable()
extern "C" void HideExcessAreaAbstractBehaviour_OnDisable_m3839 (HideExcessAreaAbstractBehaviour_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDestroy()
extern "C" void HideExcessAreaAbstractBehaviour_OnDestroy_m3840 (HideExcessAreaAbstractBehaviour_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Update()
extern "C" void HideExcessAreaAbstractBehaviour_Update_m3841 (HideExcessAreaAbstractBehaviour_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::.ctor()
extern "C" void HideExcessAreaAbstractBehaviour__ctor_m213 (HideExcessAreaAbstractBehaviour_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
