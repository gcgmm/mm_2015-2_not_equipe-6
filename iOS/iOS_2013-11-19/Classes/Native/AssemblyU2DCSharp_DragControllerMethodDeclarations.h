﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DragController
struct DragController_t11;
// UnityEngine.Collider
struct Collider_t14;

#include "codegen/il2cpp-codegen.h"

// System.Void DragController::.ctor()
extern "C" void DragController__ctor_m9 (DragController_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragController::Awake()
extern "C" void DragController_Awake_m10 (DragController_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragController::OnMouseUp()
extern "C" void DragController_OnMouseUp_m11 (DragController_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragController::OnMouseDrag()
extern "C" void DragController_OnMouseDrag_m12 (DragController_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragController::OnTriggerEnter(UnityEngine.Collider)
extern "C" void DragController_OnTriggerEnter_m13 (DragController_t11 * __this, Collider_t14 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragController::OnTriggerExit(UnityEngine.Collider)
extern "C" void DragController_OnTriggerExit_m14 (DragController_t11 * __this, Collider_t14 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
