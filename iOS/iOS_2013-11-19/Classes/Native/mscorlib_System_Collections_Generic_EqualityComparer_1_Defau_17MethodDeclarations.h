﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>
struct DefaultComparer_t3070;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void DefaultComparer__ctor_m25098_gshared (DefaultComparer_t3070 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m25098(__this, method) (( void (*) (DefaultComparer_t3070 *, const MethodInfo*))DefaultComparer__ctor_m25098_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m25099_gshared (DefaultComparer_t3070 * __this, CustomAttributeTypedArgument_t1692  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m25099(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3070 *, CustomAttributeTypedArgument_t1692 , const MethodInfo*))DefaultComparer_GetHashCode_m25099_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m25100_gshared (DefaultComparer_t3070 * __this, CustomAttributeTypedArgument_t1692  ___x, CustomAttributeTypedArgument_t1692  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m25100(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3070 *, CustomAttributeTypedArgument_t1692 , CustomAttributeTypedArgument_t1692 , const MethodInfo*))DefaultComparer_Equals_m25100_gshared)(__this, ___x, ___y, method)
