﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
struct ShimEnumerator_t2946;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t2933;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m23728_gshared (ShimEnumerator_t2946 * __this, Dictionary_2_t2933 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m23728(__this, ___host, method) (( void (*) (ShimEnumerator_t2946 *, Dictionary_2_t2933 *, const MethodInfo*))ShimEnumerator__ctor_m23728_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m23729_gshared (ShimEnumerator_t2946 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m23729(__this, method) (( bool (*) (ShimEnumerator_t2946 *, const MethodInfo*))ShimEnumerator_MoveNext_m23729_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Entry()
extern "C" DictionaryEntry_t1423  ShimEnumerator_get_Entry_m23730_gshared (ShimEnumerator_t2946 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m23730(__this, method) (( DictionaryEntry_t1423  (*) (ShimEnumerator_t2946 *, const MethodInfo*))ShimEnumerator_get_Entry_m23730_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m23731_gshared (ShimEnumerator_t2946 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m23731(__this, method) (( Object_t * (*) (ShimEnumerator_t2946 *, const MethodInfo*))ShimEnumerator_get_Key_m23731_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m23732_gshared (ShimEnumerator_t2946 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m23732(__this, method) (( Object_t * (*) (ShimEnumerator_t2946 *, const MethodInfo*))ShimEnumerator_get_Value_m23732_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m23733_gshared (ShimEnumerator_t2946 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m23733(__this, method) (( Object_t * (*) (ShimEnumerator_t2946 *, const MethodInfo*))ShimEnumerator_get_Current_m23733_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Reset()
extern "C" void ShimEnumerator_Reset_m23734_gshared (ShimEnumerator_t2946 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m23734(__this, method) (( void (*) (ShimEnumerator_t2946 *, const MethodInfo*))ShimEnumerator_Reset_m23734_gshared)(__this, method)
