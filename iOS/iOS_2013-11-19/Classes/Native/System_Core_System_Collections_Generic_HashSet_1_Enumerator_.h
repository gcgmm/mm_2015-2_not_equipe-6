﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
struct HashSet_1_t917;
// UnityEngine.MeshRenderer
struct MeshRenderer_t113;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>
struct  Enumerator_t1055 
{
	// System.Collections.Generic.HashSet`1<T> System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::hashset
	HashSet_1_t917 * ___hashset_0;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::stamp
	int32_t ___stamp_2;
	// T System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::current
	MeshRenderer_t113 * ___current_3;
};
