﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_18.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m14530_gshared (InternalEnumerator_1_t2304 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m14530(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2304 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14530_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m14531_gshared (InternalEnumerator_1_t2304 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m14531(__this, method) (( void (*) (InternalEnumerator_1_t2304 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m14531_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14532_gshared (InternalEnumerator_1_t2304 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14532(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2304 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14532_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m14533_gshared (InternalEnumerator_1_t2304 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m14533(__this, method) (( void (*) (InternalEnumerator_1_t2304 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14533_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m14534_gshared (InternalEnumerator_1_t2304 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m14534(__this, method) (( bool (*) (InternalEnumerator_1_t2304 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14534_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::get_Current()
extern "C" RaycastHit_t89  InternalEnumerator_1_get_Current_m14535_gshared (InternalEnumerator_1_t2304 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m14535(__this, method) (( RaycastHit_t89  (*) (InternalEnumerator_1_t2304 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14535_gshared)(__this, method)
