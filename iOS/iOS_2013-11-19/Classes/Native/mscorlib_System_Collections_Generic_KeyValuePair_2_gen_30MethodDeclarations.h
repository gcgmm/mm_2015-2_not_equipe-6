﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_30.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m23118_gshared (KeyValuePair_2_t2896 * __this, int32_t ___key, VirtualButtonData_t811  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m23118(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2896 *, int32_t, VirtualButtonData_t811 , const MethodInfo*))KeyValuePair_2__ctor_m23118_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m23119_gshared (KeyValuePair_2_t2896 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m23119(__this, method) (( int32_t (*) (KeyValuePair_2_t2896 *, const MethodInfo*))KeyValuePair_2_get_Key_m23119_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m23120_gshared (KeyValuePair_2_t2896 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m23120(__this, ___value, method) (( void (*) (KeyValuePair_2_t2896 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m23120_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
extern "C" VirtualButtonData_t811  KeyValuePair_2_get_Value_m23121_gshared (KeyValuePair_2_t2896 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m23121(__this, method) (( VirtualButtonData_t811  (*) (KeyValuePair_2_t2896 *, const MethodInfo*))KeyValuePair_2_get_Value_m23121_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m23122_gshared (KeyValuePair_2_t2896 * __this, VirtualButtonData_t811  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m23122(__this, ___value, method) (( void (*) (KeyValuePair_2_t2896 *, VirtualButtonData_t811 , const MethodInfo*))KeyValuePair_2_set_Value_m23122_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m23123_gshared (KeyValuePair_2_t2896 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m23123(__this, method) (( String_t* (*) (KeyValuePair_2_t2896 *, const MethodInfo*))KeyValuePair_2_ToString_m23123_gshared)(__this, method)
