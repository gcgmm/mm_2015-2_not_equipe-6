﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.CameraDevice
struct CameraDevice_t743;

#include "mscorlib_System_Object.h"

// Vuforia.CameraDevice
struct  CameraDevice_t743  : public Object_t
{
};
struct CameraDevice_t743_StaticFields{
	// Vuforia.CameraDevice Vuforia.CameraDevice::mInstance
	CameraDevice_t743 * ___mInstance_0;
};
