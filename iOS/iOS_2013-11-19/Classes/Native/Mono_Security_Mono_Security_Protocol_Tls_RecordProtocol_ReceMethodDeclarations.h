﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
struct ReceiveRecordAsyncResult_t1177;
// System.AsyncCallback
struct AsyncCallback_t261;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t786;
// System.IO.Stream
struct Stream_t1179;
// System.Exception
struct Exception_t107;
// System.Threading.WaitHandle
struct WaitHandle_t1231;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::.ctor(System.AsyncCallback,System.Object,System.Byte[],System.IO.Stream)
extern "C" void ReceiveRecordAsyncResult__ctor_m6190 (ReceiveRecordAsyncResult_t1177 * __this, AsyncCallback_t261 * ___userCallback, Object_t * ___userState, ByteU5BU5D_t786* ___initialBuffer, Stream_t1179 * ___record, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_Record()
extern "C" Stream_t1179 * ReceiveRecordAsyncResult_get_Record_m6191 (ReceiveRecordAsyncResult_t1177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_ResultingBuffer()
extern "C" ByteU5BU5D_t786* ReceiveRecordAsyncResult_get_ResultingBuffer_m6192 (ReceiveRecordAsyncResult_t1177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_InitialBuffer()
extern "C" ByteU5BU5D_t786* ReceiveRecordAsyncResult_get_InitialBuffer_m6193 (ReceiveRecordAsyncResult_t1177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncState()
extern "C" Object_t * ReceiveRecordAsyncResult_get_AsyncState_m6194 (ReceiveRecordAsyncResult_t1177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncException()
extern "C" Exception_t107 * ReceiveRecordAsyncResult_get_AsyncException_m6195 (ReceiveRecordAsyncResult_t1177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_CompletedWithError()
extern "C" bool ReceiveRecordAsyncResult_get_CompletedWithError_m6196 (ReceiveRecordAsyncResult_t1177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncWaitHandle()
extern "C" WaitHandle_t1231 * ReceiveRecordAsyncResult_get_AsyncWaitHandle_m6197 (ReceiveRecordAsyncResult_t1177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_IsCompleted()
extern "C" bool ReceiveRecordAsyncResult_get_IsCompleted_m6198 (ReceiveRecordAsyncResult_t1177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception,System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m6199 (ReceiveRecordAsyncResult_t1177 * __this, Exception_t107 * ___ex, ByteU5BU5D_t786* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception)
extern "C" void ReceiveRecordAsyncResult_SetComplete_m6200 (ReceiveRecordAsyncResult_t1177 * __this, Exception_t107 * ___ex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m6201 (ReceiveRecordAsyncResult_t1177 * __this, ByteU5BU5D_t786* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
