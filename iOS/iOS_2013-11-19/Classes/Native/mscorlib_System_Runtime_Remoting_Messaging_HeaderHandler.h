﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t2075;
// System.IAsyncResult
struct IAsyncResult_t260;
// System.AsyncCallback
struct AsyncCallback_t261;

#include "mscorlib_System_MulticastDelegate.h"

// System.Runtime.Remoting.Messaging.HeaderHandler
struct  HeaderHandler_t2074  : public MulticastDelegate_t259
{
};
