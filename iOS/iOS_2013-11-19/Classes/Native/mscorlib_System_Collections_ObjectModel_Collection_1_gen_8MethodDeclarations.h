﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
struct Collection_1_t2595;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t684;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t3186;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t429;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Collection_1__ctor_m18408_gshared (Collection_1_t2595 * __this, const MethodInfo* method);
#define Collection_1__ctor_m18408(__this, method) (( void (*) (Collection_1_t2595 *, const MethodInfo*))Collection_1__ctor_m18408_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18409_gshared (Collection_1_t2595 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18409(__this, method) (( bool (*) (Collection_1_t2595 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18409_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m18410_gshared (Collection_1_t2595 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m18410(__this, ___array, ___index, method) (( void (*) (Collection_1_t2595 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m18410_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m18411_gshared (Collection_1_t2595 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m18411(__this, method) (( Object_t * (*) (Collection_1_t2595 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m18411_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m18412_gshared (Collection_1_t2595 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m18412(__this, ___value, method) (( int32_t (*) (Collection_1_t2595 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m18412_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m18413_gshared (Collection_1_t2595 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m18413(__this, ___value, method) (( bool (*) (Collection_1_t2595 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m18413_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m18414_gshared (Collection_1_t2595 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m18414(__this, ___value, method) (( int32_t (*) (Collection_1_t2595 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m18414_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m18415_gshared (Collection_1_t2595 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m18415(__this, ___index, ___value, method) (( void (*) (Collection_1_t2595 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m18415_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m18416_gshared (Collection_1_t2595 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m18416(__this, ___value, method) (( void (*) (Collection_1_t2595 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m18416_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m18417_gshared (Collection_1_t2595 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m18417(__this, method) (( bool (*) (Collection_1_t2595 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m18417_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m18418_gshared (Collection_1_t2595 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m18418(__this, method) (( Object_t * (*) (Collection_1_t2595 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m18418_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m18419_gshared (Collection_1_t2595 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m18419(__this, method) (( bool (*) (Collection_1_t2595 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m18419_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m18420_gshared (Collection_1_t2595 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m18420(__this, method) (( bool (*) (Collection_1_t2595 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m18420_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m18421_gshared (Collection_1_t2595 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m18421(__this, ___index, method) (( Object_t * (*) (Collection_1_t2595 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m18421_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m18422_gshared (Collection_1_t2595 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m18422(__this, ___index, ___value, method) (( void (*) (Collection_1_t2595 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m18422_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void Collection_1_Add_m18423_gshared (Collection_1_t2595 * __this, UILineInfo_t428  ___item, const MethodInfo* method);
#define Collection_1_Add_m18423(__this, ___item, method) (( void (*) (Collection_1_t2595 *, UILineInfo_t428 , const MethodInfo*))Collection_1_Add_m18423_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Clear()
extern "C" void Collection_1_Clear_m18424_gshared (Collection_1_t2595 * __this, const MethodInfo* method);
#define Collection_1_Clear_m18424(__this, method) (( void (*) (Collection_1_t2595 *, const MethodInfo*))Collection_1_Clear_m18424_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m18425_gshared (Collection_1_t2595 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m18425(__this, method) (( void (*) (Collection_1_t2595 *, const MethodInfo*))Collection_1_ClearItems_m18425_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m18426_gshared (Collection_1_t2595 * __this, UILineInfo_t428  ___item, const MethodInfo* method);
#define Collection_1_Contains_m18426(__this, ___item, method) (( bool (*) (Collection_1_t2595 *, UILineInfo_t428 , const MethodInfo*))Collection_1_Contains_m18426_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m18427_gshared (Collection_1_t2595 * __this, UILineInfoU5BU5D_t684* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m18427(__this, ___array, ___index, method) (( void (*) (Collection_1_t2595 *, UILineInfoU5BU5D_t684*, int32_t, const MethodInfo*))Collection_1_CopyTo_m18427_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m18428_gshared (Collection_1_t2595 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m18428(__this, method) (( Object_t* (*) (Collection_1_t2595 *, const MethodInfo*))Collection_1_GetEnumerator_m18428_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m18429_gshared (Collection_1_t2595 * __this, UILineInfo_t428  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m18429(__this, ___item, method) (( int32_t (*) (Collection_1_t2595 *, UILineInfo_t428 , const MethodInfo*))Collection_1_IndexOf_m18429_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m18430_gshared (Collection_1_t2595 * __this, int32_t ___index, UILineInfo_t428  ___item, const MethodInfo* method);
#define Collection_1_Insert_m18430(__this, ___index, ___item, method) (( void (*) (Collection_1_t2595 *, int32_t, UILineInfo_t428 , const MethodInfo*))Collection_1_Insert_m18430_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m18431_gshared (Collection_1_t2595 * __this, int32_t ___index, UILineInfo_t428  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m18431(__this, ___index, ___item, method) (( void (*) (Collection_1_t2595 *, int32_t, UILineInfo_t428 , const MethodInfo*))Collection_1_InsertItem_m18431_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m18432_gshared (Collection_1_t2595 * __this, UILineInfo_t428  ___item, const MethodInfo* method);
#define Collection_1_Remove_m18432(__this, ___item, method) (( bool (*) (Collection_1_t2595 *, UILineInfo_t428 , const MethodInfo*))Collection_1_Remove_m18432_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m18433_gshared (Collection_1_t2595 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m18433(__this, ___index, method) (( void (*) (Collection_1_t2595 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m18433_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m18434_gshared (Collection_1_t2595 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m18434(__this, ___index, method) (( void (*) (Collection_1_t2595 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m18434_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m18435_gshared (Collection_1_t2595 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m18435(__this, method) (( int32_t (*) (Collection_1_t2595 *, const MethodInfo*))Collection_1_get_Count_m18435_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t428  Collection_1_get_Item_m18436_gshared (Collection_1_t2595 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m18436(__this, ___index, method) (( UILineInfo_t428  (*) (Collection_1_t2595 *, int32_t, const MethodInfo*))Collection_1_get_Item_m18436_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m18437_gshared (Collection_1_t2595 * __this, int32_t ___index, UILineInfo_t428  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m18437(__this, ___index, ___value, method) (( void (*) (Collection_1_t2595 *, int32_t, UILineInfo_t428 , const MethodInfo*))Collection_1_set_Item_m18437_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m18438_gshared (Collection_1_t2595 * __this, int32_t ___index, UILineInfo_t428  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m18438(__this, ___index, ___item, method) (( void (*) (Collection_1_t2595 *, int32_t, UILineInfo_t428 , const MethodInfo*))Collection_1_SetItem_m18438_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m18439_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m18439(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m18439_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ConvertItem(System.Object)
extern "C" UILineInfo_t428  Collection_1_ConvertItem_m18440_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m18440(__this /* static, unused */, ___item, method) (( UILineInfo_t428  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m18440_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m18441_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m18441(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m18441_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m18442_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m18442(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m18442_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m18443_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m18443(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m18443_gshared)(__this /* static, unused */, ___list, method)
