﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.ITextRecoEventHandler[]
struct ITextRecoEventHandlerU5BU5D_t2967;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
struct  List_1_t914  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::_items
	ITextRecoEventHandlerU5BU5D_t2967* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t914_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::EmptyArray
	ITextRecoEventHandlerU5BU5D_t2967* ___EmptyArray_4;
};
