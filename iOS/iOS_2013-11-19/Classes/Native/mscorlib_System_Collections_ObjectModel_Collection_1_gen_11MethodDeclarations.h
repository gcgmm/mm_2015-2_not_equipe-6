﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>
struct Collection_1_t3066;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2129;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3262;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>
struct IList_1_t1689;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void Collection_1__ctor_m25000_gshared (Collection_1_t3066 * __this, const MethodInfo* method);
#define Collection_1__ctor_m25000(__this, method) (( void (*) (Collection_1_t3066 *, const MethodInfo*))Collection_1__ctor_m25000_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25001_gshared (Collection_1_t3066 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25001(__this, method) (( bool (*) (Collection_1_t3066 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25001_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m25002_gshared (Collection_1_t3066 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m25002(__this, ___array, ___index, method) (( void (*) (Collection_1_t3066 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m25002_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m25003_gshared (Collection_1_t3066 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m25003(__this, method) (( Object_t * (*) (Collection_1_t3066 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m25003_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m25004_gshared (Collection_1_t3066 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m25004(__this, ___value, method) (( int32_t (*) (Collection_1_t3066 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m25004_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m25005_gshared (Collection_1_t3066 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m25005(__this, ___value, method) (( bool (*) (Collection_1_t3066 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m25005_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m25006_gshared (Collection_1_t3066 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m25006(__this, ___value, method) (( int32_t (*) (Collection_1_t3066 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m25006_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m25007_gshared (Collection_1_t3066 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m25007(__this, ___index, ___value, method) (( void (*) (Collection_1_t3066 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m25007_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m25008_gshared (Collection_1_t3066 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m25008(__this, ___value, method) (( void (*) (Collection_1_t3066 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m25008_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m25009_gshared (Collection_1_t3066 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m25009(__this, method) (( bool (*) (Collection_1_t3066 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m25009_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m25010_gshared (Collection_1_t3066 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m25010(__this, method) (( Object_t * (*) (Collection_1_t3066 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m25010_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m25011_gshared (Collection_1_t3066 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m25011(__this, method) (( bool (*) (Collection_1_t3066 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m25011_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m25012_gshared (Collection_1_t3066 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m25012(__this, method) (( bool (*) (Collection_1_t3066 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m25012_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m25013_gshared (Collection_1_t3066 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m25013(__this, ___index, method) (( Object_t * (*) (Collection_1_t3066 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m25013_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m25014_gshared (Collection_1_t3066 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m25014(__this, ___index, ___value, method) (( void (*) (Collection_1_t3066 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m25014_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void Collection_1_Add_m25015_gshared (Collection_1_t3066 * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define Collection_1_Add_m25015(__this, ___item, method) (( void (*) (Collection_1_t3066 *, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Collection_1_Add_m25015_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void Collection_1_Clear_m25016_gshared (Collection_1_t3066 * __this, const MethodInfo* method);
#define Collection_1_Clear_m25016(__this, method) (( void (*) (Collection_1_t3066 *, const MethodInfo*))Collection_1_Clear_m25016_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ClearItems()
extern "C" void Collection_1_ClearItems_m25017_gshared (Collection_1_t3066 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m25017(__this, method) (( void (*) (Collection_1_t3066 *, const MethodInfo*))Collection_1_ClearItems_m25017_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool Collection_1_Contains_m25018_gshared (Collection_1_t3066 * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define Collection_1_Contains_m25018(__this, ___item, method) (( bool (*) (Collection_1_t3066 *, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Collection_1_Contains_m25018_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m25019_gshared (Collection_1_t3066 * __this, CustomAttributeTypedArgumentU5BU5D_t2129* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m25019(__this, ___array, ___index, method) (( void (*) (Collection_1_t3066 *, CustomAttributeTypedArgumentU5BU5D_t2129*, int32_t, const MethodInfo*))Collection_1_CopyTo_m25019_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m25020_gshared (Collection_1_t3066 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m25020(__this, method) (( Object_t* (*) (Collection_1_t3066 *, const MethodInfo*))Collection_1_GetEnumerator_m25020_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m25021_gshared (Collection_1_t3066 * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m25021(__this, ___item, method) (( int32_t (*) (Collection_1_t3066 *, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Collection_1_IndexOf_m25021_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m25022_gshared (Collection_1_t3066 * __this, int32_t ___index, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define Collection_1_Insert_m25022(__this, ___index, ___item, method) (( void (*) (Collection_1_t3066 *, int32_t, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Collection_1_Insert_m25022_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m25023_gshared (Collection_1_t3066 * __this, int32_t ___index, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m25023(__this, ___index, ___item, method) (( void (*) (Collection_1_t3066 *, int32_t, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Collection_1_InsertItem_m25023_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool Collection_1_Remove_m25024_gshared (Collection_1_t3066 * __this, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define Collection_1_Remove_m25024(__this, ___item, method) (( bool (*) (Collection_1_t3066 *, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Collection_1_Remove_m25024_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m25025_gshared (Collection_1_t3066 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m25025(__this, ___index, method) (( void (*) (Collection_1_t3066 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m25025_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m25026_gshared (Collection_1_t3066 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m25026(__this, ___index, method) (( void (*) (Collection_1_t3066 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m25026_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t Collection_1_get_Count_m25027_gshared (Collection_1_t3066 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m25027(__this, method) (( int32_t (*) (Collection_1_t3066 *, const MethodInfo*))Collection_1_get_Count_m25027_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1692  Collection_1_get_Item_m25028_gshared (Collection_1_t3066 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m25028(__this, ___index, method) (( CustomAttributeTypedArgument_t1692  (*) (Collection_1_t3066 *, int32_t, const MethodInfo*))Collection_1_get_Item_m25028_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m25029_gshared (Collection_1_t3066 * __this, int32_t ___index, CustomAttributeTypedArgument_t1692  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m25029(__this, ___index, ___value, method) (( void (*) (Collection_1_t3066 *, int32_t, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Collection_1_set_Item_m25029_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m25030_gshared (Collection_1_t3066 * __this, int32_t ___index, CustomAttributeTypedArgument_t1692  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m25030(__this, ___index, ___item, method) (( void (*) (Collection_1_t3066 *, int32_t, CustomAttributeTypedArgument_t1692 , const MethodInfo*))Collection_1_SetItem_m25030_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m25031_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m25031(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m25031_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ConvertItem(System.Object)
extern "C" CustomAttributeTypedArgument_t1692  Collection_1_ConvertItem_m25032_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m25032(__this /* static, unused */, ___item, method) (( CustomAttributeTypedArgument_t1692  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m25032_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m25033_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m25033(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m25033_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m25034_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m25034(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m25034_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m25035_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m25035(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m25035_gshared)(__this /* static, unused */, ___list, method)
