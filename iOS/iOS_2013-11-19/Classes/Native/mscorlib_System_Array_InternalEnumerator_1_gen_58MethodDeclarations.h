﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_58.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_17.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19938_gshared (InternalEnumerator_1_t2709 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19938(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2709 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19938_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19939_gshared (InternalEnumerator_1_t2709 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19939(__this, method) (( void (*) (InternalEnumerator_1_t2709 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m19939_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19940_gshared (InternalEnumerator_1_t2709 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19940(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2709 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19940_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19941_gshared (InternalEnumerator_1_t2709 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19941(__this, method) (( void (*) (InternalEnumerator_1_t2709 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19941_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19942_gshared (InternalEnumerator_1_t2709 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19942(__this, method) (( bool (*) (InternalEnumerator_1_t2709 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19942_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t2708  InternalEnumerator_1_get_Current_m19943_gshared (InternalEnumerator_1_t2709 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19943(__this, method) (( KeyValuePair_2_t2708  (*) (InternalEnumerator_1_t2709 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19943_gshared)(__this, method)
