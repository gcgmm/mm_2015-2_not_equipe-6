﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"

// System.Void System.Predicate`1<UnityEngine.Canvas>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m15115(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2345 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m12988_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Canvas>::Invoke(T)
#define Predicate_1_Invoke_m15116(__this, ___obj, method) (( bool (*) (Predicate_1_t2345 *, Canvas_t230 *, const MethodInfo*))Predicate_1_Invoke_m12989_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Canvas>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m15117(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2345 *, Canvas_t230 *, AsyncCallback_t261 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m12990_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Canvas>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m15118(__this, ___result, method) (( bool (*) (Predicate_1_t2345 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m12991_gshared)(__this, ___result, method)
