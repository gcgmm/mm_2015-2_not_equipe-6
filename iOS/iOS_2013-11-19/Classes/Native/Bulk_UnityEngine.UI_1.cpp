﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.Sprite
struct Sprite_t209;
// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t307;
// UnityEngine.Material
struct Material_t8;
// UnityEngine.UI.Text
struct Text_t16;
// UnityEngine.TextGenerator
struct TextGenerator_t267;
// UnityEngine.Texture
struct Texture_t282;
// UnityEngine.Font
struct Font_t225;
// System.String
struct String_t;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t234;
// UnityEngine.UI.Toggle/ToggleEvent
struct ToggleEvent_t311;
// UnityEngine.UI.Toggle
struct Toggle_t213;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t312;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t175;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t137;
// UnityEngine.Transform
struct Transform_t84;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Toggle>
struct IEnumerable_1_t386;
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
struct Func_2_t315;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t129;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t459;
// UnityEngine.UI.ClipperRegistry
struct ClipperRegistry_t316;
// UnityEngine.UI.IClipper
struct IClipper_t387;
// System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>
struct List_1_t285;
// UnityEngine.UI.RectangularVertexClipper
struct RectangularVertexClipper_t283;
// UnityEngine.RectTransform
struct RectTransform_t212;
// UnityEngine.Canvas
struct Canvas_t230;
// System.Object
struct Object_t;
// UnityEngine.UI.AspectRatioFitter
struct AspectRatioFitter_t320;
// UnityEngine.UI.CanvasScaler
struct CanvasScaler_t324;
// UnityEngine.UI.ContentSizeFitter
struct ContentSizeFitter_t326;
// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t330;
// UnityEngine.UI.HorizontalLayoutGroup
struct HorizontalLayoutGroup_t332;
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct HorizontalOrVerticalLayoutGroup_t333;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t334;
// UnityEngine.UI.LayoutGroup
struct LayoutGroup_t331;
// UnityEngine.RectOffset
struct RectOffset_t335;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t336;
// UnityEngine.UI.LayoutRebuilder
struct LayoutRebuilder_t337;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t388;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_t341;
// UnityEngine.Component
struct Component_t130;
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_t343;
// UnityEngine.UI.ILayoutElement
struct ILayoutElement_t389;
// UnityEngine.UI.VerticalLayoutGroup
struct VerticalLayoutGroup_t344;
// UnityEngine.Mesh
struct Mesh_t120;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t266;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t380;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t349;
// UnityEngine.UI.BaseVertexEffect
struct BaseVertexEffect_t351;
// UnityEngine.UI.BaseMeshEffect
struct BaseMeshEffect_t352;
// UnityEngine.UI.Graphic
struct Graphic_t228;
// UnityEngine.UI.Outline
struct Outline_t353;
// UnityEngine.UI.PositionAsUV1
struct PositionAsUV1_t355;
// UnityEngine.UI.Shadow
struct Shadow_t354;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite.h"
#include "mscorlib_System_Void.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntry.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntryMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction.h"
#include "mscorlib_System_Object.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterialMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_20MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_20.h"
#include "UnityEngine_UnityEngine_Material.h"
#include "mscorlib_System_Int32.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "mscorlib_System_String.h"
#include "UnityEngine_UnityEngine_HideFlags.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UI_UnityEngine_UI_MiscMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_FontDataMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphicMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_FontData.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic.h"
#include "UnityEngine_UnityEngine_TextGenerator.h"
#include "UnityEngine_UnityEngine_TextGeneratorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Font.h"
#include "UnityEngine_UI_UnityEngine_UI_FontUpdateTrackerMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistryMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviour.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_FontStyle.h"
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_RectTransform.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelperMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransition.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransitionMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen_4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen_4.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_SelectableMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate.h"
#include "UnityEngine_UnityEngine_CanvasRendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroupMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventDataMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventData.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_21MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_21.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentException.h"
#include "mscorlib_System_Predicate_1_genMethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen.h"
#include "mscorlib_System_IntPtr.h"
#include "System_Core_System_Func_2_genMethodDeclarations.h"
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
#include "System_Core_System_Func_2_gen.h"
#include "System_Core_System_Linq_Enumerable.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistryMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_5.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping.h"
#include "UnityEngine_UI_UnityEngine_UI_ClippingMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_16.h"
#include "UnityEngine_UI_UnityEngine_UI_RectMask2DMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_16MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_RectMask2D.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexClipper.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexClipperMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectModeMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitterMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtilityMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTrackerMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleModeMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchModeMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_UnitMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScalerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderMode.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitModeMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitterMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtilityMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_CornerMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_AxisMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_ConstraintMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroupMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup.h"
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_22.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_22MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroup.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroupMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrouMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrou.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElementMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_0MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_13.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_13MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_3MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_0MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertieMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_3.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_0.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertie.h"
#include "mscorlib_System_Predicate_1_gen_0MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen_0.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_4.h"
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility.h"
#include "System_Core_System_Func_2_gen_0MethodDeclarations.h"
#include "System_Core_System_Func_2_gen_0.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroupMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_1MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_2MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_3MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_4MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_5MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_23.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_24.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_25.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_26.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_27.h"
#include "UnityEngine_UnityEngine_Mesh.h"
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_23MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_24MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_26MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_27MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_14.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffectMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffectMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline.h"
#include "UnityEngine_UI_UnityEngine_UI_OutlineMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ShadowMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_6MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_14MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "mscorlib_System_Byte.h"

// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C" Object_t* Enumerable_Where_TisObject_t_m2330_gshared (Object_t * __this /* static, unused */, Object_t* p0, Func_2_t459 * p1, const MethodInfo* method);
#define Enumerable_Where_TisObject_t_m2330(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t459 *, const MethodInfo*))Enumerable_Where_TisObject_t_m2330_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityEngine.UI.Toggle>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisToggle_t213_m2250(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t315 *, const MethodInfo*))Enumerable_Where_TisObject_t_m2330_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m304_gshared (Component_t130 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m304(__this, method) (( Object_t * (*) (Component_t130 *, const MethodInfo*))Component_GetComponent_TisObject_t_m304_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
#define Component_GetComponent_TisTransform_t84_m2253(__this, method) (( Transform_t84 * (*) (Component_t130 *, const MethodInfo*))Component_GetComponent_TisObject_t_m304_gshared)(__this, method)
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<UnityEngine.UI.AspectRatioFitter/AspectMode>(!!0&,!!0)
extern "C" bool SetPropertyUtility_SetStruct_TisAspectMode_t319_m2254_gshared (Object_t * __this /* static, unused */, int32_t* p0, int32_t p1, const MethodInfo* method);
#define SetPropertyUtility_SetStruct_TisAspectMode_t319_m2254(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, int32_t*, int32_t, const MethodInfo*))SetPropertyUtility_SetStruct_TisAspectMode_t319_m2254_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<System.Single>(!!0&,!!0)
extern "C" bool SetPropertyUtility_SetStruct_TisSingle_t117_m2017_gshared (Object_t * __this /* static, unused */, float* p0, float p1, const MethodInfo* method);
#define SetPropertyUtility_SetStruct_TisSingle_t117_m2017(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, float*, float, const MethodInfo*))SetPropertyUtility_SetStruct_TisSingle_t117_m2017_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t212_m1961(__this, method) (( RectTransform_t212 * (*) (Component_t130 *, const MethodInfo*))Component_GetComponent_TisObject_t_m304_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Canvas>()
#define Component_GetComponent_TisCanvas_t230_m1997(__this, method) (( Canvas_t230 * (*) (Component_t130 *, const MethodInfo*))Component_GetComponent_TisObject_t_m304_gshared)(__this, method)
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<UnityEngine.UI.ContentSizeFitter/FitMode>(!!0&,!!0)
extern "C" bool SetPropertyUtility_SetStruct_TisFitMode_t325_m2261_gshared (Object_t * __this /* static, unused */, int32_t* p0, int32_t p1, const MethodInfo* method);
#define SetPropertyUtility_SetStruct_TisFitMode_t325_m2261(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, int32_t*, int32_t, const MethodInfo*))SetPropertyUtility_SetStruct_TisFitMode_t325_m2261_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Corner>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisCorner_t327_m2262_gshared (LayoutGroup_t331 * __this, int32_t* p0, int32_t p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisCorner_t327_m2262(__this, p0, p1, method) (( void (*) (LayoutGroup_t331 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisCorner_t327_m2262_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Axis>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisAxis_t328_m2263_gshared (LayoutGroup_t331 * __this, int32_t* p0, int32_t p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisAxis_t328_m2263(__this, p0, p1, method) (( void (*) (LayoutGroup_t331 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisAxis_t328_m2263_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.Vector2>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisVector2_t171_m2264_gshared (LayoutGroup_t331 * __this, Vector2_t171 * p0, Vector2_t171  p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisVector2_t171_m2264(__this, p0, p1, method) (( void (*) (LayoutGroup_t331 *, Vector2_t171 *, Vector2_t171 , const MethodInfo*))LayoutGroup_SetProperty_TisVector2_t171_m2264_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Constraint>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisConstraint_t329_m2265_gshared (LayoutGroup_t331 * __this, int32_t* p0, int32_t p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisConstraint_t329_m2265(__this, p0, p1, method) (( void (*) (LayoutGroup_t331 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisConstraint_t329_m2265_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Int32>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisInt32_t392_m2266_gshared (LayoutGroup_t331 * __this, int32_t* p0, int32_t p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisInt32_t392_m2266(__this, p0, p1, method) (( void (*) (LayoutGroup_t331 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisInt32_t392_m2266_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Single>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisSingle_t117_m2271_gshared (LayoutGroup_t331 * __this, float* p0, float p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisSingle_t117_m2271(__this, p0, p1, method) (( void (*) (LayoutGroup_t331 *, float*, float, const MethodInfo*))LayoutGroup_SetProperty_TisSingle_t117_m2271_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Boolean>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisBoolean_t393_m2272_gshared (LayoutGroup_t331 * __this, bool* p0, bool p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisBoolean_t393_m2272(__this, p0, p1, method) (( void (*) (LayoutGroup_t331 *, bool*, bool, const MethodInfo*))LayoutGroup_SetProperty_TisBoolean_t393_m2272_gshared)(__this, p0, p1, method)
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<System.Boolean>(!!0&,!!0)
extern "C" bool SetPropertyUtility_SetStruct_TisBoolean_t393_m2014_gshared (Object_t * __this /* static, unused */, bool* p0, bool p1, const MethodInfo* method);
#define SetPropertyUtility_SetStruct_TisBoolean_t393_m2014(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, bool*, bool, const MethodInfo*))SetPropertyUtility_SetStruct_TisBoolean_t393_m2014_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Object>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisObject_t_m2331_gshared (LayoutGroup_t331 * __this, Object_t ** p0, Object_t * p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisObject_t_m2331(__this, p0, p1, method) (( void (*) (LayoutGroup_t331 *, Object_t **, Object_t *, const MethodInfo*))LayoutGroup_SetProperty_TisObject_t_m2331_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.RectOffset>(!!0&,!!0)
#define LayoutGroup_SetProperty_TisRectOffset_t335_m2277(__this, p0, p1, method) (( void (*) (LayoutGroup_t331 *, RectOffset_t335 **, RectOffset_t335 *, const MethodInfo*))LayoutGroup_SetProperty_TisObject_t_m2331_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.TextAnchor>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisTextAnchor_t444_m2278_gshared (LayoutGroup_t331 * __this, int32_t* p0, int32_t p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisTextAnchor_t444_m2278(__this, p0, p1, method) (( void (*) (LayoutGroup_t331 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisTextAnchor_t444_m2278_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Graphic>()
#define Component_GetComponent_TisGraphic_t228_m2142(__this, method) (( Graphic_t228 * (*) (Component_t130 *, const MethodInfo*))Component_GetComponent_TisObject_t_m304_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_highlightedSprite()
extern "C" Sprite_t209 * SpriteState_get_highlightedSprite_m1378 (SpriteState_t299 * __this, const MethodInfo* method)
{
	{
		Sprite_t209 * L_0 = (__this->___m_HighlightedSprite_0);
		return L_0;
	}
}
// System.Void UnityEngine.UI.SpriteState::set_highlightedSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_highlightedSprite_m1379 (SpriteState_t299 * __this, Sprite_t209 * ___value, const MethodInfo* method)
{
	{
		Sprite_t209 * L_0 = ___value;
		__this->___m_HighlightedSprite_0 = L_0;
		return;
	}
}
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_pressedSprite()
extern "C" Sprite_t209 * SpriteState_get_pressedSprite_m1380 (SpriteState_t299 * __this, const MethodInfo* method)
{
	{
		Sprite_t209 * L_0 = (__this->___m_PressedSprite_1);
		return L_0;
	}
}
// System.Void UnityEngine.UI.SpriteState::set_pressedSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_pressedSprite_m1381 (SpriteState_t299 * __this, Sprite_t209 * ___value, const MethodInfo* method)
{
	{
		Sprite_t209 * L_0 = ___value;
		__this->___m_PressedSprite_1 = L_0;
		return;
	}
}
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_disabledSprite()
extern "C" Sprite_t209 * SpriteState_get_disabledSprite_m1382 (SpriteState_t299 * __this, const MethodInfo* method)
{
	{
		Sprite_t209 * L_0 = (__this->___m_DisabledSprite_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.SpriteState::set_disabledSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_disabledSprite_m1383 (SpriteState_t299 * __this, Sprite_t209 * ___value, const MethodInfo* method)
{
	{
		Sprite_t209 * L_0 = ___value;
		__this->___m_DisabledSprite_2 = L_0;
		return;
	}
}
// System.Void UnityEngine.UI.StencilMaterial/MatEntry::.ctor()
extern "C" void MatEntry__ctor_m1384 (MatEntry_t307 * __this, const MethodInfo* method)
{
	{
		__this->___compareFunction_5 = 8;
		Object__ctor_m215(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.StencilMaterial::.cctor()
extern TypeInfo* List_1_t309_il2cpp_TypeInfo_var;
extern TypeInfo* StencilMaterial_t308_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2229_MethodInfo_var;
extern "C" void StencilMaterial__cctor_m1385 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t309_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(266);
		StencilMaterial_t308_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(241);
		List_1__ctor_m2229_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483903);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t309 * L_0 = (List_1_t309 *)il2cpp_codegen_object_new (List_1_t309_il2cpp_TypeInfo_var);
		List_1__ctor_m2229(L_0, /*hidden argument*/List_1__ctor_m2229_MethodInfo_var);
		((StencilMaterial_t308_StaticFields*)StencilMaterial_t308_il2cpp_TypeInfo_var->static_fields)->___m_List_0 = L_0;
		return;
	}
}
// UnityEngine.Material UnityEngine.UI.StencilMaterial::Add(UnityEngine.Material,System.Int32)
extern "C" Material_t8 * StencilMaterial_Add_m1386 (Object_t * __this /* static, unused */, Material_t8 * ___baseMat, int32_t ___stencilID, const MethodInfo* method)
{
	{
		return (Material_t8 *)NULL;
	}
}
// UnityEngine.Material UnityEngine.UI.StencilMaterial::Add(UnityEngine.Material,System.Int32,UnityEngine.Rendering.StencilOp,UnityEngine.Rendering.CompareFunction,UnityEngine.Rendering.ColorWriteMask)
extern TypeInfo* StencilMaterial_t308_il2cpp_TypeInfo_var;
extern "C" Material_t8 * StencilMaterial_Add_m1387 (Object_t * __this /* static, unused */, Material_t8 * ___baseMat, int32_t ___stencilID, int32_t ___operation, int32_t ___compareFunction, int32_t ___colorWriteMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StencilMaterial_t308_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(241);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t8 * L_0 = ___baseMat;
		int32_t L_1 = ___stencilID;
		int32_t L_2 = ___operation;
		int32_t L_3 = ___compareFunction;
		int32_t L_4 = ___colorWriteMask;
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t308_il2cpp_TypeInfo_var);
		Material_t8 * L_5 = StencilMaterial_Add_m1388(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, ((int32_t)255), ((int32_t)255), /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Material UnityEngine.UI.StencilMaterial::Add(UnityEngine.Material,System.Int32,UnityEngine.Rendering.StencilOp,UnityEngine.Rendering.CompareFunction,UnityEngine.Rendering.ColorWriteMask,System.Int32,System.Int32)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StencilMaterial_t308_il2cpp_TypeInfo_var;
extern TypeInfo* MatEntry_t307_il2cpp_TypeInfo_var;
extern TypeInfo* Material_t8_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t392_il2cpp_TypeInfo_var;
extern TypeInfo* StencilOp_t437_il2cpp_TypeInfo_var;
extern TypeInfo* CompareFunction_t438_il2cpp_TypeInfo_var;
extern TypeInfo* ColorWriteMask_t439_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t393_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral127;
extern Il2CppCodeGenString* _stringLiteral128;
extern Il2CppCodeGenString* _stringLiteral129;
extern Il2CppCodeGenString* _stringLiteral130;
extern Il2CppCodeGenString* _stringLiteral131;
extern Il2CppCodeGenString* _stringLiteral132;
extern Il2CppCodeGenString* _stringLiteral133;
extern Il2CppCodeGenString* _stringLiteral134;
extern Il2CppCodeGenString* _stringLiteral135;
extern Il2CppCodeGenString* _stringLiteral136;
extern Il2CppCodeGenString* _stringLiteral137;
extern Il2CppCodeGenString* _stringLiteral138;
extern Il2CppCodeGenString* _stringLiteral139;
extern Il2CppCodeGenString* _stringLiteral140;
extern Il2CppCodeGenString* _stringLiteral141;
extern "C" Material_t8 * StencilMaterial_Add_m1388 (Object_t * __this /* static, unused */, Material_t8 * ___baseMat, int32_t ___stencilID, int32_t ___operation, int32_t ___compareFunction, int32_t ___colorWriteMask, int32_t ___readMask, int32_t ___writeMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		StencilMaterial_t308_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(241);
		MatEntry_t307_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(265);
		Material_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Int32_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(117);
		StencilOp_t437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(267);
		CompareFunction_t438_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(268);
		ColorWriteMask_t439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(269);
		Boolean_t393_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(121);
		_stringLiteral127 = il2cpp_codegen_string_literal_from_index(127);
		_stringLiteral128 = il2cpp_codegen_string_literal_from_index(128);
		_stringLiteral129 = il2cpp_codegen_string_literal_from_index(129);
		_stringLiteral130 = il2cpp_codegen_string_literal_from_index(130);
		_stringLiteral131 = il2cpp_codegen_string_literal_from_index(131);
		_stringLiteral132 = il2cpp_codegen_string_literal_from_index(132);
		_stringLiteral133 = il2cpp_codegen_string_literal_from_index(133);
		_stringLiteral134 = il2cpp_codegen_string_literal_from_index(134);
		_stringLiteral135 = il2cpp_codegen_string_literal_from_index(135);
		_stringLiteral136 = il2cpp_codegen_string_literal_from_index(136);
		_stringLiteral137 = il2cpp_codegen_string_literal_from_index(137);
		_stringLiteral138 = il2cpp_codegen_string_literal_from_index(138);
		_stringLiteral139 = il2cpp_codegen_string_literal_from_index(139);
		_stringLiteral140 = il2cpp_codegen_string_literal_from_index(140);
		_stringLiteral141 = il2cpp_codegen_string_literal_from_index(141);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	MatEntry_t307 * V_1 = {0};
	MatEntry_t307 * V_2 = {0};
	MatEntry_t307 * G_B29_0 = {0};
	MatEntry_t307 * G_B28_0 = {0};
	int32_t G_B30_0 = 0;
	MatEntry_t307 * G_B30_1 = {0};
	String_t* G_B32_0 = {0};
	Material_t8 * G_B32_1 = {0};
	String_t* G_B31_0 = {0};
	Material_t8 * G_B31_1 = {0};
	int32_t G_B33_0 = 0;
	String_t* G_B33_1 = {0};
	Material_t8 * G_B33_2 = {0};
	{
		int32_t L_0 = ___stencilID;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_1 = ___colorWriteMask;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)15))))
		{
			goto IL_001c;
		}
	}

IL_0010:
	{
		Material_t8 * L_2 = ___baseMat;
		bool L_3 = Object_op_Equality_m165(NULL /*static, unused*/, L_2, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}

IL_001c:
	{
		Material_t8 * L_4 = ___baseMat;
		return L_4;
	}

IL_001e:
	{
		Material_t8 * L_5 = ___baseMat;
		NullCheck(L_5);
		bool L_6 = Material_HasProperty_m2230(L_5, _stringLiteral127, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004b;
		}
	}
	{
		Material_t8 * L_7 = ___baseMat;
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m2112(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m211(NULL /*static, unused*/, _stringLiteral128, L_8, _stringLiteral129, /*hidden argument*/NULL);
		Material_t8 * L_10 = ___baseMat;
		Debug_LogWarning_m2231(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Material_t8 * L_11 = ___baseMat;
		return L_11;
	}

IL_004b:
	{
		Material_t8 * L_12 = ___baseMat;
		NullCheck(L_12);
		bool L_13 = Material_HasProperty_m2230(L_12, _stringLiteral130, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0078;
		}
	}
	{
		Material_t8 * L_14 = ___baseMat;
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m2112(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m211(NULL /*static, unused*/, _stringLiteral128, L_15, _stringLiteral131, /*hidden argument*/NULL);
		Material_t8 * L_17 = ___baseMat;
		Debug_LogWarning_m2231(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		Material_t8 * L_18 = ___baseMat;
		return L_18;
	}

IL_0078:
	{
		Material_t8 * L_19 = ___baseMat;
		NullCheck(L_19);
		bool L_20 = Material_HasProperty_m2230(L_19, _stringLiteral132, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00a5;
		}
	}
	{
		Material_t8 * L_21 = ___baseMat;
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m2112(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m211(NULL /*static, unused*/, _stringLiteral128, L_22, _stringLiteral133, /*hidden argument*/NULL);
		Material_t8 * L_24 = ___baseMat;
		Debug_LogWarning_m2231(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		Material_t8 * L_25 = ___baseMat;
		return L_25;
	}

IL_00a5:
	{
		Material_t8 * L_26 = ___baseMat;
		NullCheck(L_26);
		bool L_27 = Material_HasProperty_m2230(L_26, _stringLiteral134, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00d2;
		}
	}
	{
		Material_t8 * L_28 = ___baseMat;
		NullCheck(L_28);
		String_t* L_29 = Object_get_name_m2112(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m211(NULL /*static, unused*/, _stringLiteral128, L_29, _stringLiteral135, /*hidden argument*/NULL);
		Material_t8 * L_31 = ___baseMat;
		Debug_LogWarning_m2231(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		Material_t8 * L_32 = ___baseMat;
		return L_32;
	}

IL_00d2:
	{
		Material_t8 * L_33 = ___baseMat;
		NullCheck(L_33);
		bool L_34 = Material_HasProperty_m2230(L_33, _stringLiteral134, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_00ff;
		}
	}
	{
		Material_t8 * L_35 = ___baseMat;
		NullCheck(L_35);
		String_t* L_36 = Object_get_name_m2112(L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Concat_m211(NULL /*static, unused*/, _stringLiteral128, L_36, _stringLiteral136, /*hidden argument*/NULL);
		Material_t8 * L_38 = ___baseMat;
		Debug_LogWarning_m2231(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		Material_t8 * L_39 = ___baseMat;
		return L_39;
	}

IL_00ff:
	{
		Material_t8 * L_40 = ___baseMat;
		NullCheck(L_40);
		bool L_41 = Material_HasProperty_m2230(L_40, _stringLiteral137, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_012c;
		}
	}
	{
		Material_t8 * L_42 = ___baseMat;
		NullCheck(L_42);
		String_t* L_43 = Object_get_name_m2112(L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m211(NULL /*static, unused*/, _stringLiteral128, L_43, _stringLiteral138, /*hidden argument*/NULL);
		Material_t8 * L_45 = ___baseMat;
		Debug_LogWarning_m2231(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		Material_t8 * L_46 = ___baseMat;
		return L_46;
	}

IL_012c:
	{
		V_0 = 0;
		goto IL_01b4;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t308_il2cpp_TypeInfo_var);
		List_1_t309 * L_47 = ((StencilMaterial_t308_StaticFields*)StencilMaterial_t308_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		int32_t L_48 = V_0;
		NullCheck(L_47);
		MatEntry_t307 * L_49 = (MatEntry_t307 *)VirtFuncInvoker1< MatEntry_t307 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Item(System.Int32) */, L_47, L_48);
		V_1 = L_49;
		MatEntry_t307 * L_50 = V_1;
		NullCheck(L_50);
		Material_t8 * L_51 = (L_50->___baseMat_0);
		Material_t8 * L_52 = ___baseMat;
		bool L_53 = Object_op_Equality_m165(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t307 * L_54 = V_1;
		NullCheck(L_54);
		int32_t L_55 = (L_54->___stencilId_3);
		int32_t L_56 = ___stencilID;
		if ((!(((uint32_t)L_55) == ((uint32_t)L_56))))
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t307 * L_57 = V_1;
		NullCheck(L_57);
		int32_t L_58 = (L_57->___operation_4);
		int32_t L_59 = ___operation;
		if ((!(((uint32_t)L_58) == ((uint32_t)L_59))))
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t307 * L_60 = V_1;
		NullCheck(L_60);
		int32_t L_61 = (L_60->___compareFunction_5);
		int32_t L_62 = ___compareFunction;
		if ((!(((uint32_t)L_61) == ((uint32_t)L_62))))
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t307 * L_63 = V_1;
		NullCheck(L_63);
		int32_t L_64 = (L_63->___readMask_6);
		int32_t L_65 = ___readMask;
		if ((!(((uint32_t)L_64) == ((uint32_t)L_65))))
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t307 * L_66 = V_1;
		NullCheck(L_66);
		int32_t L_67 = (L_66->___writeMask_7);
		int32_t L_68 = ___writeMask;
		if ((!(((uint32_t)L_67) == ((uint32_t)L_68))))
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t307 * L_69 = V_1;
		NullCheck(L_69);
		int32_t L_70 = (L_69->___colorMask_9);
		int32_t L_71 = ___colorWriteMask;
		if ((!(((uint32_t)L_70) == ((uint32_t)L_71))))
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t307 * L_72 = V_1;
		MatEntry_t307 * L_73 = L_72;
		NullCheck(L_73);
		int32_t L_74 = (L_73->___count_2);
		NullCheck(L_73);
		L_73->___count_2 = ((int32_t)((int32_t)L_74+(int32_t)1));
		MatEntry_t307 * L_75 = V_1;
		NullCheck(L_75);
		Material_t8 * L_76 = (L_75->___customMat_1);
		return L_76;
	}

IL_01b0:
	{
		int32_t L_77 = V_0;
		V_0 = ((int32_t)((int32_t)L_77+(int32_t)1));
	}

IL_01b4:
	{
		int32_t L_78 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t308_il2cpp_TypeInfo_var);
		List_1_t309 * L_79 = ((StencilMaterial_t308_StaticFields*)StencilMaterial_t308_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		NullCheck(L_79);
		int32_t L_80 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Count() */, L_79);
		if ((((int32_t)L_78) < ((int32_t)L_80)))
		{
			goto IL_0133;
		}
	}
	{
		MatEntry_t307 * L_81 = (MatEntry_t307 *)il2cpp_codegen_object_new (MatEntry_t307_il2cpp_TypeInfo_var);
		MatEntry__ctor_m1384(L_81, /*hidden argument*/NULL);
		V_2 = L_81;
		MatEntry_t307 * L_82 = V_2;
		NullCheck(L_82);
		L_82->___count_2 = 1;
		MatEntry_t307 * L_83 = V_2;
		Material_t8 * L_84 = ___baseMat;
		NullCheck(L_83);
		L_83->___baseMat_0 = L_84;
		MatEntry_t307 * L_85 = V_2;
		Material_t8 * L_86 = ___baseMat;
		Material_t8 * L_87 = (Material_t8 *)il2cpp_codegen_object_new (Material_t8_il2cpp_TypeInfo_var);
		Material__ctor_m2232(L_87, L_86, /*hidden argument*/NULL);
		NullCheck(L_85);
		L_85->___customMat_1 = L_87;
		MatEntry_t307 * L_88 = V_2;
		NullCheck(L_88);
		Material_t8 * L_89 = (L_88->___customMat_1);
		NullCheck(L_89);
		Object_set_hideFlags_m276(L_89, ((int32_t)61), /*hidden argument*/NULL);
		MatEntry_t307 * L_90 = V_2;
		int32_t L_91 = ___stencilID;
		NullCheck(L_90);
		L_90->___stencilId_3 = L_91;
		MatEntry_t307 * L_92 = V_2;
		int32_t L_93 = ___operation;
		NullCheck(L_92);
		L_92->___operation_4 = L_93;
		MatEntry_t307 * L_94 = V_2;
		int32_t L_95 = ___compareFunction;
		NullCheck(L_94);
		L_94->___compareFunction_5 = L_95;
		MatEntry_t307 * L_96 = V_2;
		int32_t L_97 = ___readMask;
		NullCheck(L_96);
		L_96->___readMask_6 = L_97;
		MatEntry_t307 * L_98 = V_2;
		int32_t L_99 = ___writeMask;
		NullCheck(L_98);
		L_98->___writeMask_7 = L_99;
		MatEntry_t307 * L_100 = V_2;
		int32_t L_101 = ___colorWriteMask;
		NullCheck(L_100);
		L_100->___colorMask_9 = L_101;
		MatEntry_t307 * L_102 = V_2;
		int32_t L_103 = ___operation;
		G_B28_0 = L_102;
		if (!L_103)
		{
			G_B29_0 = L_102;
			goto IL_022c;
		}
	}
	{
		int32_t L_104 = ___writeMask;
		G_B30_0 = ((((int32_t)L_104) > ((int32_t)0))? 1 : 0);
		G_B30_1 = G_B28_0;
		goto IL_022d;
	}

IL_022c:
	{
		G_B30_0 = 0;
		G_B30_1 = G_B29_0;
	}

IL_022d:
	{
		NullCheck(G_B30_1);
		G_B30_1->___useAlphaClip_8 = G_B30_0;
		MatEntry_t307 * L_105 = V_2;
		NullCheck(L_105);
		Material_t8 * L_106 = (L_105->___customMat_1);
		ObjectU5BU5D_t105* L_107 = ((ObjectU5BU5D_t105*)SZArrayNew(ObjectU5BU5D_t105_il2cpp_TypeInfo_var, 8));
		int32_t L_108 = ___stencilID;
		int32_t L_109 = L_108;
		Object_t * L_110 = Box(Int32_t392_il2cpp_TypeInfo_var, &L_109);
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, 0);
		ArrayElementTypeCheck (L_107, L_110);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_107, 0, sizeof(Object_t *))) = (Object_t *)L_110;
		ObjectU5BU5D_t105* L_111 = L_107;
		int32_t L_112 = ___operation;
		int32_t L_113 = L_112;
		Object_t * L_114 = Box(StencilOp_t437_il2cpp_TypeInfo_var, &L_113);
		NullCheck(L_111);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_111, 1);
		ArrayElementTypeCheck (L_111, L_114);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_111, 1, sizeof(Object_t *))) = (Object_t *)L_114;
		ObjectU5BU5D_t105* L_115 = L_111;
		int32_t L_116 = ___compareFunction;
		int32_t L_117 = L_116;
		Object_t * L_118 = Box(CompareFunction_t438_il2cpp_TypeInfo_var, &L_117);
		NullCheck(L_115);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_115, 2);
		ArrayElementTypeCheck (L_115, L_118);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_115, 2, sizeof(Object_t *))) = (Object_t *)L_118;
		ObjectU5BU5D_t105* L_119 = L_115;
		int32_t L_120 = ___writeMask;
		int32_t L_121 = L_120;
		Object_t * L_122 = Box(Int32_t392_il2cpp_TypeInfo_var, &L_121);
		NullCheck(L_119);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_119, 3);
		ArrayElementTypeCheck (L_119, L_122);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_119, 3, sizeof(Object_t *))) = (Object_t *)L_122;
		ObjectU5BU5D_t105* L_123 = L_119;
		int32_t L_124 = ___readMask;
		int32_t L_125 = L_124;
		Object_t * L_126 = Box(Int32_t392_il2cpp_TypeInfo_var, &L_125);
		NullCheck(L_123);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_123, 4);
		ArrayElementTypeCheck (L_123, L_126);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_123, 4, sizeof(Object_t *))) = (Object_t *)L_126;
		ObjectU5BU5D_t105* L_127 = L_123;
		int32_t L_128 = ___colorWriteMask;
		int32_t L_129 = L_128;
		Object_t * L_130 = Box(ColorWriteMask_t439_il2cpp_TypeInfo_var, &L_129);
		NullCheck(L_127);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_127, 5);
		ArrayElementTypeCheck (L_127, L_130);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_127, 5, sizeof(Object_t *))) = (Object_t *)L_130;
		ObjectU5BU5D_t105* L_131 = L_127;
		MatEntry_t307 * L_132 = V_2;
		NullCheck(L_132);
		bool L_133 = (L_132->___useAlphaClip_8);
		bool L_134 = L_133;
		Object_t * L_135 = Box(Boolean_t393_il2cpp_TypeInfo_var, &L_134);
		NullCheck(L_131);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_131, 6);
		ArrayElementTypeCheck (L_131, L_135);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_131, 6, sizeof(Object_t *))) = (Object_t *)L_135;
		ObjectU5BU5D_t105* L_136 = L_131;
		Material_t8 * L_137 = ___baseMat;
		NullCheck(L_137);
		String_t* L_138 = Object_get_name_m2112(L_137, /*hidden argument*/NULL);
		NullCheck(L_136);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_136, 7);
		ArrayElementTypeCheck (L_136, L_138);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_136, 7, sizeof(Object_t *))) = (Object_t *)L_138;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_139 = String_Format_m2233(NULL /*static, unused*/, _stringLiteral139, L_136, /*hidden argument*/NULL);
		NullCheck(L_106);
		Object_set_name_m1884(L_106, L_139, /*hidden argument*/NULL);
		MatEntry_t307 * L_140 = V_2;
		NullCheck(L_140);
		Material_t8 * L_141 = (L_140->___customMat_1);
		int32_t L_142 = ___stencilID;
		NullCheck(L_141);
		Material_SetInt_m2234(L_141, _stringLiteral127, L_142, /*hidden argument*/NULL);
		MatEntry_t307 * L_143 = V_2;
		NullCheck(L_143);
		Material_t8 * L_144 = (L_143->___customMat_1);
		int32_t L_145 = ___operation;
		NullCheck(L_144);
		Material_SetInt_m2234(L_144, _stringLiteral130, L_145, /*hidden argument*/NULL);
		MatEntry_t307 * L_146 = V_2;
		NullCheck(L_146);
		Material_t8 * L_147 = (L_146->___customMat_1);
		int32_t L_148 = ___compareFunction;
		NullCheck(L_147);
		Material_SetInt_m2234(L_147, _stringLiteral132, L_148, /*hidden argument*/NULL);
		MatEntry_t307 * L_149 = V_2;
		NullCheck(L_149);
		Material_t8 * L_150 = (L_149->___customMat_1);
		int32_t L_151 = ___readMask;
		NullCheck(L_150);
		Material_SetInt_m2234(L_150, _stringLiteral134, L_151, /*hidden argument*/NULL);
		MatEntry_t307 * L_152 = V_2;
		NullCheck(L_152);
		Material_t8 * L_153 = (L_152->___customMat_1);
		int32_t L_154 = ___writeMask;
		NullCheck(L_153);
		Material_SetInt_m2234(L_153, _stringLiteral140, L_154, /*hidden argument*/NULL);
		MatEntry_t307 * L_155 = V_2;
		NullCheck(L_155);
		Material_t8 * L_156 = (L_155->___customMat_1);
		int32_t L_157 = ___colorWriteMask;
		NullCheck(L_156);
		Material_SetInt_m2234(L_156, _stringLiteral137, L_157, /*hidden argument*/NULL);
		MatEntry_t307 * L_158 = V_2;
		NullCheck(L_158);
		Material_t8 * L_159 = (L_158->___customMat_1);
		MatEntry_t307 * L_160 = V_2;
		NullCheck(L_160);
		bool L_161 = (L_160->___useAlphaClip_8);
		G_B31_0 = _stringLiteral141;
		G_B31_1 = L_159;
		if (!L_161)
		{
			G_B32_0 = _stringLiteral141;
			G_B32_1 = L_159;
			goto IL_0322;
		}
	}
	{
		G_B33_0 = 1;
		G_B33_1 = G_B31_0;
		G_B33_2 = G_B31_1;
		goto IL_0323;
	}

IL_0322:
	{
		G_B33_0 = 0;
		G_B33_1 = G_B32_0;
		G_B33_2 = G_B32_1;
	}

IL_0323:
	{
		NullCheck(G_B33_2);
		Material_SetInt_m2234(G_B33_2, G_B33_1, G_B33_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t308_il2cpp_TypeInfo_var);
		List_1_t309 * L_162 = ((StencilMaterial_t308_StaticFields*)StencilMaterial_t308_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		MatEntry_t307 * L_163 = V_2;
		NullCheck(L_162);
		VirtActionInvoker1< MatEntry_t307 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Add(!0) */, L_162, L_163);
		MatEntry_t307 * L_164 = V_2;
		NullCheck(L_164);
		Material_t8 * L_165 = (L_164->___customMat_1);
		return L_165;
	}
}
// System.Void UnityEngine.UI.StencilMaterial::Remove(UnityEngine.Material)
extern TypeInfo* StencilMaterial_t308_il2cpp_TypeInfo_var;
extern "C" void StencilMaterial_Remove_m1389 (Object_t * __this /* static, unused */, Material_t8 * ___customMat, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StencilMaterial_t308_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(241);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	MatEntry_t307 * V_1 = {0};
	int32_t V_2 = 0;
	{
		Material_t8 * L_0 = ___customMat;
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		V_0 = 0;
		goto IL_006e;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t308_il2cpp_TypeInfo_var);
		List_1_t309 * L_2 = ((StencilMaterial_t308_StaticFields*)StencilMaterial_t308_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		MatEntry_t307 * L_4 = (MatEntry_t307 *)VirtFuncInvoker1< MatEntry_t307 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Item(System.Int32) */, L_2, L_3);
		V_1 = L_4;
		MatEntry_t307 * L_5 = V_1;
		NullCheck(L_5);
		Material_t8 * L_6 = (L_5->___customMat_1);
		Material_t8 * L_7 = ___customMat;
		bool L_8 = Object_op_Inequality_m150(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_006a;
	}

IL_0036:
	{
		MatEntry_t307 * L_9 = V_1;
		MatEntry_t307 * L_10 = L_9;
		NullCheck(L_10);
		int32_t L_11 = (L_10->___count_2);
		int32_t L_12 = ((int32_t)((int32_t)L_11-(int32_t)1));
		V_2 = L_12;
		NullCheck(L_10);
		L_10->___count_2 = L_12;
		int32_t L_13 = V_2;
		if (L_13)
		{
			goto IL_0069;
		}
	}
	{
		MatEntry_t307 * L_14 = V_1;
		NullCheck(L_14);
		Material_t8 * L_15 = (L_14->___customMat_1);
		Misc_DestroyImmediate_m1099(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MatEntry_t307 * L_16 = V_1;
		NullCheck(L_16);
		L_16->___baseMat_0 = (Material_t8 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t308_il2cpp_TypeInfo_var);
		List_1_t309 * L_17 = ((StencilMaterial_t308_StaticFields*)StencilMaterial_t308_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		int32_t L_18 = V_0;
		NullCheck(L_17);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::RemoveAt(System.Int32) */, L_17, L_18);
	}

IL_0069:
	{
		return;
	}

IL_006a:
	{
		int32_t L_19 = V_0;
		V_0 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_006e:
	{
		int32_t L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t308_il2cpp_TypeInfo_var);
		List_1_t309 * L_21 = ((StencilMaterial_t308_StaticFields*)StencilMaterial_t308_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		NullCheck(L_21);
		int32_t L_22 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Count() */, L_21);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.StencilMaterial::ClearAll()
extern TypeInfo* StencilMaterial_t308_il2cpp_TypeInfo_var;
extern "C" void StencilMaterial_ClearAll_m1390 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StencilMaterial_t308_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(241);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	MatEntry_t307 * V_1 = {0};
	{
		V_0 = 0;
		goto IL_0029;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t308_il2cpp_TypeInfo_var);
		List_1_t309 * L_0 = ((StencilMaterial_t308_StaticFields*)StencilMaterial_t308_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		MatEntry_t307 * L_2 = (MatEntry_t307 *)VirtFuncInvoker1< MatEntry_t307 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Item(System.Int32) */, L_0, L_1);
		V_1 = L_2;
		MatEntry_t307 * L_3 = V_1;
		NullCheck(L_3);
		Material_t8 * L_4 = (L_3->___customMat_1);
		Misc_DestroyImmediate_m1099(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		MatEntry_t307 * L_5 = V_1;
		NullCheck(L_5);
		L_5->___baseMat_0 = (Material_t8 *)NULL;
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t308_il2cpp_TypeInfo_var);
		List_1_t309 * L_8 = ((StencilMaterial_t308_StaticFields*)StencilMaterial_t308_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		NullCheck(L_8);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Count() */, L_8);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0007;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t308_il2cpp_TypeInfo_var);
		List_1_t309 * L_10 = ((StencilMaterial_t308_StaticFields*)StencilMaterial_t308_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Clear() */, L_10);
		return;
	}
}
// System.Void UnityEngine.UI.Text::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* UIVertexU5BU5D_t266_il2cpp_TypeInfo_var;
extern "C" void Text__ctor_m1391 (Text_t16 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		UIVertexU5BU5D_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(240);
		s_Il2CppMethodIntialized = true;
	}
	{
		FontData_t224 * L_0 = FontData_get_defaultFontData_m762(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_FontData_28 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_29 = L_1;
		__this->___m_TempVerts_34 = ((UIVertexU5BU5D_t266*)SZArrayNew(UIVertexU5BU5D_t266_il2cpp_TypeInfo_var, 4));
		MaskableGraphic__ctor_m1073(__this, /*hidden argument*/NULL);
		Graphic_set_useLegacyMeshGeneration_m797(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Text::.cctor()
extern "C" void Text__cctor_m1392 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGenerator()
extern TypeInfo* TextGenerator_t267_il2cpp_TypeInfo_var;
extern "C" TextGenerator_t267 * Text_get_cachedTextGenerator_m1393 (Text_t16 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextGenerator_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(222);
		s_Il2CppMethodIntialized = true;
	}
	TextGenerator_t267 * V_0 = {0};
	TextGenerator_t267 * G_B5_0 = {0};
	TextGenerator_t267 * G_B1_0 = {0};
	Text_t16 * G_B3_0 = {0};
	Text_t16 * G_B2_0 = {0};
	TextGenerator_t267 * G_B4_0 = {0};
	Text_t16 * G_B4_1 = {0};
	{
		TextGenerator_t267 * L_0 = (__this->___m_TextCache_30);
		TextGenerator_t267 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B5_0 = L_1;
			goto IL_0040;
		}
	}
	{
		String_t* L_2 = (__this->___m_Text_29);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m2060(L_2, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if (!L_3)
		{
			G_B3_0 = __this;
			goto IL_0033;
		}
	}
	{
		String_t* L_4 = (__this->___m_Text_29);
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m2060(L_4, /*hidden argument*/NULL);
		TextGenerator_t267 * L_6 = (TextGenerator_t267 *)il2cpp_codegen_object_new (TextGenerator_t267_il2cpp_TypeInfo_var);
		TextGenerator__ctor_m2235(L_6, L_5, /*hidden argument*/NULL);
		G_B4_0 = L_6;
		G_B4_1 = G_B2_0;
		goto IL_0038;
	}

IL_0033:
	{
		TextGenerator_t267 * L_7 = (TextGenerator_t267 *)il2cpp_codegen_object_new (TextGenerator_t267_il2cpp_TypeInfo_var);
		TextGenerator__ctor_m2055(L_7, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B3_0;
	}

IL_0038:
	{
		TextGenerator_t267 * L_8 = G_B4_0;
		V_0 = L_8;
		NullCheck(G_B4_1);
		G_B4_1->___m_TextCache_30 = L_8;
		TextGenerator_t267 * L_9 = V_0;
		G_B5_0 = L_9;
	}

IL_0040:
	{
		return G_B5_0;
	}
}
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGeneratorForLayout()
extern TypeInfo* TextGenerator_t267_il2cpp_TypeInfo_var;
extern "C" TextGenerator_t267 * Text_get_cachedTextGeneratorForLayout_m1394 (Text_t16 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextGenerator_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(222);
		s_Il2CppMethodIntialized = true;
	}
	TextGenerator_t267 * V_0 = {0};
	TextGenerator_t267 * G_B2_0 = {0};
	TextGenerator_t267 * G_B1_0 = {0};
	{
		TextGenerator_t267 * L_0 = (__this->___m_TextCacheForLayout_31);
		TextGenerator_t267 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		TextGenerator_t267 * L_2 = (TextGenerator_t267 *)il2cpp_codegen_object_new (TextGenerator_t267_il2cpp_TypeInfo_var);
		TextGenerator__ctor_m2055(L_2, /*hidden argument*/NULL);
		TextGenerator_t267 * L_3 = L_2;
		V_0 = L_3;
		__this->___m_TextCacheForLayout_31 = L_3;
		TextGenerator_t267 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// UnityEngine.Texture UnityEngine.UI.Text::get_mainTexture()
extern "C" Texture_t282 * Text_get_mainTexture_m1395 (Text_t16 * __this, const MethodInfo* method)
{
	{
		Font_t225 * L_0 = Text_get_font_m1397(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m150(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0053;
		}
	}
	{
		Font_t225 * L_2 = Text_get_font_m1397(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_t8 * L_3 = Font_get_material_m2236(L_2, /*hidden argument*/NULL);
		bool L_4 = Object_op_Inequality_m150(NULL /*static, unused*/, L_3, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0053;
		}
	}
	{
		Font_t225 * L_5 = Text_get_font_m1397(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_t8 * L_6 = Font_get_material_m2236(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Texture_t282 * L_7 = Material_get_mainTexture_m2019(L_6, /*hidden argument*/NULL);
		bool L_8 = Object_op_Inequality_m150(NULL /*static, unused*/, L_7, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0053;
		}
	}
	{
		Font_t225 * L_9 = Text_get_font_m1397(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_t8 * L_10 = Font_get_material_m2236(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Texture_t282 * L_11 = Material_get_mainTexture_m2019(L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0053:
	{
		Material_t8 * L_12 = (((Graphic_t228 *)__this)->___m_Material_4);
		bool L_13 = Object_op_Inequality_m150(NULL /*static, unused*/, L_12, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0070;
		}
	}
	{
		Material_t8 * L_14 = (((Graphic_t228 *)__this)->___m_Material_4);
		NullCheck(L_14);
		Texture_t282 * L_15 = Material_get_mainTexture_m2019(L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_0070:
	{
		Texture_t282 * L_16 = Graphic_get_mainTexture_m814(__this, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Void UnityEngine.UI.Text::FontTextureChanged()
extern TypeInfo* FontUpdateTracker_t226_il2cpp_TypeInfo_var;
extern TypeInfo* CanvasUpdateRegistry_t204_il2cpp_TypeInfo_var;
extern "C" void Text_FontTextureChanged_m1396 (Text_t16 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontUpdateTracker_t226_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		CanvasUpdateRegistry_t204_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(147);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Object_op_Implicit_m182(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FontUpdateTracker_t226_il2cpp_TypeInfo_var);
		FontUpdateTracker_UntrackText_m788(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		bool L_1 = (__this->___m_DisableFontTextureRebuiltCallback_33);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		TextGenerator_t267 * L_2 = Text_get_cachedTextGenerator_m1393(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		TextGenerator_Invalidate_m2237(L_2, /*hidden argument*/NULL);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_3)
		{
			goto IL_0035;
		}
	}
	{
		return;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CanvasUpdateRegistry_t204_il2cpp_TypeInfo_var);
		bool L_4 = CanvasUpdateRegistry_IsRebuildingGraphics_m659(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0049;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CanvasUpdateRegistry_t204_il2cpp_TypeInfo_var);
		bool L_5 = CanvasUpdateRegistry_IsRebuildingLayout_m658(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0054;
		}
	}

IL_0049:
	{
		VirtActionInvoker0::Invoke(34 /* System.Void UnityEngine.UI.Text::UpdateGeometry() */, __this);
		goto IL_005a;
	}

IL_0054:
	{
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetAllDirty() */, __this);
	}

IL_005a:
	{
		return;
	}
}
// UnityEngine.Font UnityEngine.UI.Text::get_font()
extern "C" Font_t225 * Text_get_font_m1397 (Text_t16 * __this, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		Font_t225 * L_1 = FontData_get_font_m763(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_font(UnityEngine.Font)
extern TypeInfo* FontUpdateTracker_t226_il2cpp_TypeInfo_var;
extern "C" void Text_set_font_m1398 (Text_t16 * __this, Font_t225 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontUpdateTracker_t226_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		s_Il2CppMethodIntialized = true;
	}
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		Font_t225 * L_1 = FontData_get_font_m763(L_0, /*hidden argument*/NULL);
		Font_t225 * L_2 = ___value;
		bool L_3 = Object_op_Equality_m165(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FontUpdateTracker_t226_il2cpp_TypeInfo_var);
		FontUpdateTracker_UntrackText_m788(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		FontData_t224 * L_4 = (__this->___m_FontData_28);
		Font_t225 * L_5 = ___value;
		NullCheck(L_4);
		FontData_set_font_m764(L_4, L_5, /*hidden argument*/NULL);
		FontUpdateTracker_TrackText_m786(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetAllDirty() */, __this);
		return;
	}
}
// System.String UnityEngine.UI.Text::get_text()
extern "C" String_t* Text_get_text_m1399 (Text_t16 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Text_29);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Text::set_text(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Text_set_text_m1400 (Text_t16 * __this, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2109(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_2 = (__this->___m_Text_29);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2109(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_29 = L_4;
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		goto IL_0056;
	}

IL_0032:
	{
		String_t* L_5 = (__this->___m_Text_29);
		String_t* L_6 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Inequality_m2078(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		String_t* L_8 = ___value;
		__this->___m_Text_29 = L_8;
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(22 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
	}

IL_0056:
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.Text::get_supportRichText()
extern "C" bool Text_get_supportRichText_m1401 (Text_t16 * __this, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		bool L_1 = FontData_get_richText_m777(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_supportRichText(System.Boolean)
extern "C" void Text_set_supportRichText_m1402 (Text_t16 * __this, bool ___value, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		bool L_1 = FontData_get_richText_m777(L_0, /*hidden argument*/NULL);
		bool L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t224 * L_3 = (__this->___m_FontData_28);
		bool L_4 = ___value;
		NullCheck(L_3);
		FontData_set_richText_m778(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(22 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// System.Boolean UnityEngine.UI.Text::get_resizeTextForBestFit()
extern "C" bool Text_get_resizeTextForBestFit_m1403 (Text_t16 * __this, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		bool L_1 = FontData_get_bestFit_m769(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_resizeTextForBestFit(System.Boolean)
extern "C" void Text_set_resizeTextForBestFit_m1404 (Text_t16 * __this, bool ___value, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		bool L_1 = FontData_get_bestFit_m769(L_0, /*hidden argument*/NULL);
		bool L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t224 * L_3 = (__this->___m_FontData_28);
		bool L_4 = ___value;
		NullCheck(L_3);
		FontData_set_bestFit_m770(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(22 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// System.Int32 UnityEngine.UI.Text::get_resizeTextMinSize()
extern "C" int32_t Text_get_resizeTextMinSize_m1405 (Text_t16 * __this, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_minSize_m771(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_resizeTextMinSize(System.Int32)
extern "C" void Text_set_resizeTextMinSize_m1406 (Text_t16 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_minSize_m771(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t224 * L_3 = (__this->___m_FontData_28);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_minSize_m772(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(22 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// System.Int32 UnityEngine.UI.Text::get_resizeTextMaxSize()
extern "C" int32_t Text_get_resizeTextMaxSize_m1407 (Text_t16 * __this, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_maxSize_m773(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_resizeTextMaxSize(System.Int32)
extern "C" void Text_set_resizeTextMaxSize_m1408 (Text_t16 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_maxSize_m773(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t224 * L_3 = (__this->___m_FontData_28);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_maxSize_m774(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(22 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// UnityEngine.TextAnchor UnityEngine.UI.Text::get_alignment()
extern "C" int32_t Text_get_alignment_m1409 (Text_t16 * __this, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_alignment_m775(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_alignment(UnityEngine.TextAnchor)
extern "C" void Text_set_alignment_m1410 (Text_t16 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_alignment_m775(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t224 * L_3 = (__this->___m_FontData_28);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_alignment_m776(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(22 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// System.Int32 UnityEngine.UI.Text::get_fontSize()
extern "C" int32_t Text_get_fontSize_m1411 (Text_t16 * __this, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_fontSize_m765(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_fontSize(System.Int32)
extern "C" void Text_set_fontSize_m1412 (Text_t16 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_fontSize_m765(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t224 * L_3 = (__this->___m_FontData_28);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_fontSize_m766(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(22 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// UnityEngine.HorizontalWrapMode UnityEngine.UI.Text::get_horizontalOverflow()
extern "C" int32_t Text_get_horizontalOverflow_m1413 (Text_t16 * __this, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_horizontalOverflow_m779(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_horizontalOverflow(UnityEngine.HorizontalWrapMode)
extern "C" void Text_set_horizontalOverflow_m1414 (Text_t16 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_horizontalOverflow_m779(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t224 * L_3 = (__this->___m_FontData_28);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_horizontalOverflow_m780(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(22 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// UnityEngine.VerticalWrapMode UnityEngine.UI.Text::get_verticalOverflow()
extern "C" int32_t Text_get_verticalOverflow_m1415 (Text_t16 * __this, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_verticalOverflow_m781(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_verticalOverflow(UnityEngine.VerticalWrapMode)
extern "C" void Text_set_verticalOverflow_m1416 (Text_t16 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_verticalOverflow_m781(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t224 * L_3 = (__this->___m_FontData_28);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_verticalOverflow_m782(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(22 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// System.Single UnityEngine.UI.Text::get_lineSpacing()
extern "C" float Text_get_lineSpacing_m1417 (Text_t16 * __this, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		float L_1 = FontData_get_lineSpacing_m783(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_lineSpacing(System.Single)
extern "C" void Text_set_lineSpacing_m1418 (Text_t16 * __this, float ___value, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		float L_1 = FontData_get_lineSpacing_m783(L_0, /*hidden argument*/NULL);
		float L_2 = ___value;
		if ((!(((float)L_1) == ((float)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t224 * L_3 = (__this->___m_FontData_28);
		float L_4 = ___value;
		NullCheck(L_3);
		FontData_set_lineSpacing_m784(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(22 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// UnityEngine.FontStyle UnityEngine.UI.Text::get_fontStyle()
extern "C" int32_t Text_get_fontStyle_m1419 (Text_t16 * __this, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_fontStyle_m767(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_fontStyle(UnityEngine.FontStyle)
extern "C" void Text_set_fontStyle_m1420 (Text_t16 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t224 * L_0 = (__this->___m_FontData_28);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_fontStyle_m767(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t224 * L_3 = (__this->___m_FontData_28);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_fontStyle_m768(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(22 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// System.Single UnityEngine.UI.Text::get_pixelsPerUnit()
extern "C" float Text_get_pixelsPerUnit_m1421 (Text_t16 * __this, const MethodInfo* method)
{
	Canvas_t230 * V_0 = {0};
	{
		Canvas_t230 * L_0 = Graphic_get_canvas_m807(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Canvas_t230 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m182(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		return (1.0f);
	}

IL_0018:
	{
		Font_t225 * L_3 = Text_get_font_m1397(__this, /*hidden argument*/NULL);
		bool L_4 = Object_op_Implicit_m182(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		Font_t225 * L_5 = Text_get_font_m1397(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Font_get_dynamic_m2238(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}

IL_0038:
	{
		Canvas_t230 * L_7 = V_0;
		NullCheck(L_7);
		float L_8 = Canvas_get_scaleFactor_m2239(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_003f:
	{
		FontData_t224 * L_9 = (__this->___m_FontData_28);
		NullCheck(L_9);
		int32_t L_10 = FontData_get_fontSize_m765(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		Font_t225 * L_11 = Text_get_font_m1397(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = Font_get_fontSize_m2240(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_12) > ((int32_t)0)))
		{
			goto IL_0067;
		}
	}

IL_0061:
	{
		return (1.0f);
	}

IL_0067:
	{
		Font_t225 * L_13 = Text_get_font_m1397(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = Font_get_fontSize_m2240(L_13, /*hidden argument*/NULL);
		FontData_t224 * L_15 = (__this->___m_FontData_28);
		NullCheck(L_15);
		int32_t L_16 = FontData_get_fontSize_m765(L_15, /*hidden argument*/NULL);
		return ((float)((float)(((float)((float)L_14)))/(float)(((float)((float)L_16)))));
	}
}
// System.Void UnityEngine.UI.Text::OnEnable()
extern TypeInfo* FontUpdateTracker_t226_il2cpp_TypeInfo_var;
extern "C" void Text_OnEnable_m1422 (Text_t16 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontUpdateTracker_t226_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		s_Il2CppMethodIntialized = true;
	}
	{
		MaskableGraphic_OnEnable_m1081(__this, /*hidden argument*/NULL);
		TextGenerator_t267 * L_0 = Text_get_cachedTextGenerator_m1393(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		TextGenerator_Invalidate_m2237(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FontUpdateTracker_t226_il2cpp_TypeInfo_var);
		FontUpdateTracker_TrackText_m786(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Text::OnDisable()
extern TypeInfo* FontUpdateTracker_t226_il2cpp_TypeInfo_var;
extern "C" void Text_OnDisable_m1423 (Text_t16 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontUpdateTracker_t226_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FontUpdateTracker_t226_il2cpp_TypeInfo_var);
		FontUpdateTracker_UntrackText_m788(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		MaskableGraphic_OnDisable_m1082(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Text::UpdateGeometry()
extern "C" void Text_UpdateGeometry_m1424 (Text_t16 * __this, const MethodInfo* method)
{
	{
		Font_t225 * L_0 = Text_get_font_m1397(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m150(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		Graphic_UpdateGeometry_m822(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// UnityEngine.TextGenerationSettings UnityEngine.UI.Text::GetGenerationSettings(UnityEngine.Vector2)
extern TypeInfo* TextGenerationSettings_t385_il2cpp_TypeInfo_var;
extern "C" TextGenerationSettings_t385  Text_GetGenerationSettings_m1425 (Text_t16 * __this, Vector2_t171  ___extents, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextGenerationSettings_t385_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(270);
		s_Il2CppMethodIntialized = true;
	}
	TextGenerationSettings_t385  V_0 = {0};
	{
		Initobj (TextGenerationSettings_t385_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t171  L_0 = ___extents;
		(&V_0)->___generationExtents_14 = L_0;
		Font_t225 * L_1 = Text_get_font_m1397(__this, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m150(NULL /*static, unused*/, L_1, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0067;
		}
	}
	{
		Font_t225 * L_3 = Text_get_font_m1397(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = Font_get_dynamic_m2238(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0067;
		}
	}
	{
		FontData_t224 * L_5 = (__this->___m_FontData_28);
		NullCheck(L_5);
		int32_t L_6 = FontData_get_fontSize_m765(L_5, /*hidden argument*/NULL);
		(&V_0)->___fontSize_2 = L_6;
		FontData_t224 * L_7 = (__this->___m_FontData_28);
		NullCheck(L_7);
		int32_t L_8 = FontData_get_minSize_m771(L_7, /*hidden argument*/NULL);
		(&V_0)->___resizeTextMinSize_9 = L_8;
		FontData_t224 * L_9 = (__this->___m_FontData_28);
		NullCheck(L_9);
		int32_t L_10 = FontData_get_maxSize_m773(L_9, /*hidden argument*/NULL);
		(&V_0)->___resizeTextMaxSize_10 = L_10;
	}

IL_0067:
	{
		FontData_t224 * L_11 = (__this->___m_FontData_28);
		NullCheck(L_11);
		int32_t L_12 = FontData_get_alignment_m775(L_11, /*hidden argument*/NULL);
		(&V_0)->___textAnchor_7 = L_12;
		float L_13 = Text_get_pixelsPerUnit_m1421(__this, /*hidden argument*/NULL);
		(&V_0)->___scaleFactor_5 = L_13;
		Color_t77  L_14 = Graphic_get_color_m792(__this, /*hidden argument*/NULL);
		(&V_0)->___color_1 = L_14;
		Font_t225 * L_15 = Text_get_font_m1397(__this, /*hidden argument*/NULL);
		(&V_0)->___font_0 = L_15;
		RectTransform_t212 * L_16 = Graphic_get_rectTransform_m806(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector2_t171  L_17 = RectTransform_get_pivot_m1932(L_16, /*hidden argument*/NULL);
		(&V_0)->___pivot_15 = L_17;
		FontData_t224 * L_18 = (__this->___m_FontData_28);
		NullCheck(L_18);
		bool L_19 = FontData_get_richText_m777(L_18, /*hidden argument*/NULL);
		(&V_0)->___richText_4 = L_19;
		FontData_t224 * L_20 = (__this->___m_FontData_28);
		NullCheck(L_20);
		float L_21 = FontData_get_lineSpacing_m783(L_20, /*hidden argument*/NULL);
		(&V_0)->___lineSpacing_3 = L_21;
		FontData_t224 * L_22 = (__this->___m_FontData_28);
		NullCheck(L_22);
		int32_t L_23 = FontData_get_fontStyle_m767(L_22, /*hidden argument*/NULL);
		(&V_0)->___fontStyle_6 = L_23;
		FontData_t224 * L_24 = (__this->___m_FontData_28);
		NullCheck(L_24);
		bool L_25 = FontData_get_bestFit_m769(L_24, /*hidden argument*/NULL);
		(&V_0)->___resizeTextForBestFit_8 = L_25;
		(&V_0)->___updateBounds_11 = 0;
		FontData_t224 * L_26 = (__this->___m_FontData_28);
		NullCheck(L_26);
		int32_t L_27 = FontData_get_horizontalOverflow_m779(L_26, /*hidden argument*/NULL);
		(&V_0)->___horizontalOverflow_13 = L_27;
		FontData_t224 * L_28 = (__this->___m_FontData_28);
		NullCheck(L_28);
		int32_t L_29 = FontData_get_verticalOverflow_m781(L_28, /*hidden argument*/NULL);
		(&V_0)->___verticalOverflow_12 = L_29;
		TextGenerationSettings_t385  L_30 = V_0;
		return L_30;
	}
}
// UnityEngine.Vector2 UnityEngine.UI.Text::GetTextAnchorPivot(UnityEngine.TextAnchor)
extern "C" Vector2_t171  Text_GetTextAnchorPivot_m1426 (Object_t * __this /* static, unused */, int32_t ___anchor, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___anchor;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0091;
		}
		if (L_1 == 1)
		{
			goto IL_00a1;
		}
		if (L_1 == 2)
		{
			goto IL_00b1;
		}
		if (L_1 == 3)
		{
			goto IL_0061;
		}
		if (L_1 == 4)
		{
			goto IL_0071;
		}
		if (L_1 == 5)
		{
			goto IL_0081;
		}
		if (L_1 == 6)
		{
			goto IL_0031;
		}
		if (L_1 == 7)
		{
			goto IL_0041;
		}
		if (L_1 == 8)
		{
			goto IL_0051;
		}
	}
	{
		goto IL_00c1;
	}

IL_0031:
	{
		Vector2_t171  L_2 = {0};
		Vector2__ctor_m1764(&L_2, (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_2;
	}

IL_0041:
	{
		Vector2_t171  L_3 = {0};
		Vector2__ctor_m1764(&L_3, (0.5f), (0.0f), /*hidden argument*/NULL);
		return L_3;
	}

IL_0051:
	{
		Vector2_t171  L_4 = {0};
		Vector2__ctor_m1764(&L_4, (1.0f), (0.0f), /*hidden argument*/NULL);
		return L_4;
	}

IL_0061:
	{
		Vector2_t171  L_5 = {0};
		Vector2__ctor_m1764(&L_5, (0.0f), (0.5f), /*hidden argument*/NULL);
		return L_5;
	}

IL_0071:
	{
		Vector2_t171  L_6 = {0};
		Vector2__ctor_m1764(&L_6, (0.5f), (0.5f), /*hidden argument*/NULL);
		return L_6;
	}

IL_0081:
	{
		Vector2_t171  L_7 = {0};
		Vector2__ctor_m1764(&L_7, (1.0f), (0.5f), /*hidden argument*/NULL);
		return L_7;
	}

IL_0091:
	{
		Vector2_t171  L_8 = {0};
		Vector2__ctor_m1764(&L_8, (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_8;
	}

IL_00a1:
	{
		Vector2_t171  L_9 = {0};
		Vector2__ctor_m1764(&L_9, (0.5f), (1.0f), /*hidden argument*/NULL);
		return L_9;
	}

IL_00b1:
	{
		Vector2_t171  L_10 = {0};
		Vector2__ctor_m1764(&L_10, (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_10;
	}

IL_00c1:
	{
		Vector2_t171  L_11 = Vector2_get_zero_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void UnityEngine.UI.Text::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern TypeInfo* Text_t16_il2cpp_TypeInfo_var;
extern TypeInfo* ICollection_1_t440_il2cpp_TypeInfo_var;
extern TypeInfo* IList_1_t441_il2cpp_TypeInfo_var;
extern "C" void Text_OnPopulateMesh_m1427 (Text_t16 * __this, VertexHelper_t234 * ___toFill, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Text_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		ICollection_1_t440_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(271);
		IList_1_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(272);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t171  V_0 = {0};
	TextGenerationSettings_t385  V_1 = {0};
	Rect_t94  V_2 = {0};
	Vector2_t171  V_3 = {0};
	Vector2_t171  V_4 = {0};
	Vector2_t171  V_5 = {0};
	Object_t* V_6 = {0};
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	Rect_t94  V_13 = {0};
	Vector2_t171 * G_B4_0 = {0};
	Vector2_t171 * G_B3_0 = {0};
	float G_B5_0 = 0.0f;
	Vector2_t171 * G_B5_1 = {0};
	Vector2_t171 * G_B7_0 = {0};
	Vector2_t171 * G_B6_0 = {0};
	float G_B8_0 = 0.0f;
	Vector2_t171 * G_B8_1 = {0};
	{
		Font_t225 * L_0 = Text_get_font_m1397(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		__this->___m_DisableFontTextureRebuiltCallback_33 = 1;
		RectTransform_t212 * L_2 = Graphic_get_rectTransform_m806(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rect_t94  L_3 = RectTransform_get_rect_m1916(L_2, /*hidden argument*/NULL);
		V_13 = L_3;
		Vector2_t171  L_4 = Rect_get_size_m1921((&V_13), /*hidden argument*/NULL);
		V_0 = L_4;
		Vector2_t171  L_5 = V_0;
		TextGenerationSettings_t385  L_6 = Text_GetGenerationSettings_m1425(__this, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		TextGenerator_t267 * L_7 = Text_get_cachedTextGenerator_m1393(__this, /*hidden argument*/NULL);
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(64 /* System.String UnityEngine.UI.Text::get_text() */, __this);
		TextGenerationSettings_t385  L_9 = V_1;
		NullCheck(L_7);
		TextGenerator_Populate_m2110(L_7, L_8, L_9, /*hidden argument*/NULL);
		RectTransform_t212 * L_10 = Graphic_get_rectTransform_m806(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Rect_t94  L_11 = RectTransform_get_rect_m1916(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		FontData_t224 * L_12 = (__this->___m_FontData_28);
		NullCheck(L_12);
		int32_t L_13 = FontData_get_alignment_m775(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Text_t16_il2cpp_TypeInfo_var);
		Vector2_t171  L_14 = Text_GetTextAnchorPivot_m1426(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		Vector2_t171  L_15 = Vector2_get_zero_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = ((&V_3)->___x_1);
		G_B3_0 = (&V_4);
		if ((!(((float)L_16) == ((float)(1.0f)))))
		{
			G_B4_0 = (&V_4);
			goto IL_008c;
		}
	}
	{
		float L_17 = Rect_get_xMax_m2045((&V_2), /*hidden argument*/NULL);
		G_B5_0 = L_17;
		G_B5_1 = G_B3_0;
		goto IL_0093;
	}

IL_008c:
	{
		float L_18 = Rect_get_xMin_m2053((&V_2), /*hidden argument*/NULL);
		G_B5_0 = L_18;
		G_B5_1 = G_B4_0;
	}

IL_0093:
	{
		G_B5_1->___x_1 = G_B5_0;
		float L_19 = ((&V_3)->___y_2);
		G_B6_0 = (&V_4);
		if ((!(((float)L_19) == ((float)(0.0f)))))
		{
			G_B7_0 = (&V_4);
			goto IL_00b7;
		}
	}
	{
		float L_20 = Rect_get_yMin_m2052((&V_2), /*hidden argument*/NULL);
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		goto IL_00be;
	}

IL_00b7:
	{
		float L_21 = Rect_get_yMax_m2046((&V_2), /*hidden argument*/NULL);
		G_B8_0 = L_21;
		G_B8_1 = G_B7_0;
	}

IL_00be:
	{
		G_B8_1->___y_2 = G_B8_0;
		Vector2_t171  L_22 = V_4;
		Vector2_t171  L_23 = Graphic_PixelAdjustPoint_m832(__this, L_22, /*hidden argument*/NULL);
		Vector2_t171  L_24 = V_4;
		Vector2_t171  L_25 = Vector2_op_Subtraction_m1773(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		V_5 = L_25;
		TextGenerator_t267 * L_26 = Text_get_cachedTextGenerator_m1393(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		Object_t* L_27 = TextGenerator_get_verts_m2241(L_26, /*hidden argument*/NULL);
		V_6 = L_27;
		float L_28 = Text_get_pixelsPerUnit_m1421(__this, /*hidden argument*/NULL);
		V_7 = ((float)((float)(1.0f)/(float)L_28));
		Object_t* L_29 = V_6;
		NullCheck(L_29);
		int32_t L_30 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UIVertex>::get_Count() */, ICollection_1_t440_il2cpp_TypeInfo_var, L_29);
		V_8 = ((int32_t)((int32_t)L_30-(int32_t)4));
		VertexHelper_t234 * L_31 = ___toFill;
		NullCheck(L_31);
		VertexHelper_Clear_m1677(L_31, /*hidden argument*/NULL);
		Vector2_t171  L_32 = V_5;
		Vector2_t171  L_33 = Vector2_get_zero_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_34 = Vector2_op_Inequality_m2122(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_01cb;
		}
	}
	{
		V_9 = 0;
		goto IL_01bd;
	}

IL_0119:
	{
		int32_t L_35 = V_9;
		V_10 = ((int32_t)((int32_t)L_35&(int32_t)3));
		UIVertexU5BU5D_t266* L_36 = (__this->___m_TempVerts_34);
		int32_t L_37 = V_10;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		Object_t* L_38 = V_6;
		int32_t L_39 = V_9;
		NullCheck(L_38);
		UIVertex_t272  L_40 = (UIVertex_t272 )InterfaceFuncInvoker1< UIVertex_t272 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, IList_1_t441_il2cpp_TypeInfo_var, L_38, L_39);
		(*(UIVertex_t272 *)((UIVertex_t272 *)(UIVertex_t272 *)SZArrayLdElema(L_36, L_37, sizeof(UIVertex_t272 )))) = L_40;
		UIVertexU5BU5D_t266* L_41 = (__this->___m_TempVerts_34);
		int32_t L_42 = V_10;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		UIVertex_t272 * L_43 = ((UIVertex_t272 *)(UIVertex_t272 *)SZArrayLdElema(L_41, L_42, sizeof(UIVertex_t272 )));
		Vector3_t12  L_44 = (L_43->___position_0);
		float L_45 = V_7;
		Vector3_t12  L_46 = Vector3_op_Multiply_m2242(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		L_43->___position_0 = L_46;
		UIVertexU5BU5D_t266* L_47 = (__this->___m_TempVerts_34);
		int32_t L_48 = V_10;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		Vector3_t12 * L_49 = &(((UIVertex_t272 *)(UIVertex_t272 *)SZArrayLdElema(L_47, L_48, sizeof(UIVertex_t272 )))->___position_0);
		Vector3_t12 * L_50 = L_49;
		float L_51 = (L_50->___x_1);
		float L_52 = ((&V_5)->___x_1);
		L_50->___x_1 = ((float)((float)L_51+(float)L_52));
		UIVertexU5BU5D_t266* L_53 = (__this->___m_TempVerts_34);
		int32_t L_54 = V_10;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		Vector3_t12 * L_55 = &(((UIVertex_t272 *)(UIVertex_t272 *)SZArrayLdElema(L_53, L_54, sizeof(UIVertex_t272 )))->___position_0);
		Vector3_t12 * L_56 = L_55;
		float L_57 = (L_56->___y_2);
		float L_58 = ((&V_5)->___y_2);
		L_56->___y_2 = ((float)((float)L_57+(float)L_58));
		int32_t L_59 = V_10;
		if ((!(((uint32_t)L_59) == ((uint32_t)3))))
		{
			goto IL_01b7;
		}
	}
	{
		VertexHelper_t234 * L_60 = ___toFill;
		UIVertexU5BU5D_t266* L_61 = (__this->___m_TempVerts_34);
		NullCheck(L_60);
		VertexHelper_AddUIVertexQuad_m1688(L_60, L_61, /*hidden argument*/NULL);
	}

IL_01b7:
	{
		int32_t L_62 = V_9;
		V_9 = ((int32_t)((int32_t)L_62+(int32_t)1));
	}

IL_01bd:
	{
		int32_t L_63 = V_9;
		int32_t L_64 = V_8;
		if ((((int32_t)L_63) < ((int32_t)L_64)))
		{
			goto IL_0119;
		}
	}
	{
		goto IL_0236;
	}

IL_01cb:
	{
		V_11 = 0;
		goto IL_022d;
	}

IL_01d3:
	{
		int32_t L_65 = V_11;
		V_12 = ((int32_t)((int32_t)L_65&(int32_t)3));
		UIVertexU5BU5D_t266* L_66 = (__this->___m_TempVerts_34);
		int32_t L_67 = V_12;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		Object_t* L_68 = V_6;
		int32_t L_69 = V_11;
		NullCheck(L_68);
		UIVertex_t272  L_70 = (UIVertex_t272 )InterfaceFuncInvoker1< UIVertex_t272 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, IList_1_t441_il2cpp_TypeInfo_var, L_68, L_69);
		(*(UIVertex_t272 *)((UIVertex_t272 *)(UIVertex_t272 *)SZArrayLdElema(L_66, L_67, sizeof(UIVertex_t272 )))) = L_70;
		UIVertexU5BU5D_t266* L_71 = (__this->___m_TempVerts_34);
		int32_t L_72 = V_12;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, L_72);
		UIVertex_t272 * L_73 = ((UIVertex_t272 *)(UIVertex_t272 *)SZArrayLdElema(L_71, L_72, sizeof(UIVertex_t272 )));
		Vector3_t12  L_74 = (L_73->___position_0);
		float L_75 = V_7;
		Vector3_t12  L_76 = Vector3_op_Multiply_m2242(NULL /*static, unused*/, L_74, L_75, /*hidden argument*/NULL);
		L_73->___position_0 = L_76;
		int32_t L_77 = V_12;
		if ((!(((uint32_t)L_77) == ((uint32_t)3))))
		{
			goto IL_0227;
		}
	}
	{
		VertexHelper_t234 * L_78 = ___toFill;
		UIVertexU5BU5D_t266* L_79 = (__this->___m_TempVerts_34);
		NullCheck(L_78);
		VertexHelper_AddUIVertexQuad_m1688(L_78, L_79, /*hidden argument*/NULL);
	}

IL_0227:
	{
		int32_t L_80 = V_11;
		V_11 = ((int32_t)((int32_t)L_80+(int32_t)1));
	}

IL_022d:
	{
		int32_t L_81 = V_11;
		int32_t L_82 = V_8;
		if ((((int32_t)L_81) < ((int32_t)L_82)))
		{
			goto IL_01d3;
		}
	}

IL_0236:
	{
		__this->___m_DisableFontTextureRebuiltCallback_33 = 0;
		return;
	}
}
// System.Void UnityEngine.UI.Text::CalculateLayoutInputHorizontal()
extern "C" void Text_CalculateLayoutInputHorizontal_m1428 (Text_t16 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.Text::CalculateLayoutInputVertical()
extern "C" void Text_CalculateLayoutInputVertical_m1429 (Text_t16 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Single UnityEngine.UI.Text::get_minWidth()
extern "C" float Text_get_minWidth_m1430 (Text_t16 * __this, const MethodInfo* method)
{
	{
		return (0.0f);
	}
}
// System.Single UnityEngine.UI.Text::get_preferredWidth()
extern "C" float Text_get_preferredWidth_m1431 (Text_t16 * __this, const MethodInfo* method)
{
	TextGenerationSettings_t385  V_0 = {0};
	{
		Vector2_t171  L_0 = Vector2_get_zero_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		TextGenerationSettings_t385  L_1 = Text_GetGenerationSettings_m1425(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		TextGenerator_t267 * L_2 = Text_get_cachedTextGeneratorForLayout_m1394(__this, /*hidden argument*/NULL);
		String_t* L_3 = (__this->___m_Text_29);
		TextGenerationSettings_t385  L_4 = V_0;
		NullCheck(L_2);
		float L_5 = TextGenerator_GetPreferredWidth_m2243(L_2, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Text_get_pixelsPerUnit_m1421(__this, /*hidden argument*/NULL);
		return ((float)((float)L_5/(float)L_6));
	}
}
// System.Single UnityEngine.UI.Text::get_flexibleWidth()
extern "C" float Text_get_flexibleWidth_m1432 (Text_t16 * __this, const MethodInfo* method)
{
	{
		return (-1.0f);
	}
}
// System.Single UnityEngine.UI.Text::get_minHeight()
extern "C" float Text_get_minHeight_m1433 (Text_t16 * __this, const MethodInfo* method)
{
	{
		return (0.0f);
	}
}
// System.Single UnityEngine.UI.Text::get_preferredHeight()
extern "C" float Text_get_preferredHeight_m1434 (Text_t16 * __this, const MethodInfo* method)
{
	TextGenerationSettings_t385  V_0 = {0};
	Rect_t94  V_1 = {0};
	Vector2_t171  V_2 = {0};
	{
		RectTransform_t212 * L_0 = Graphic_get_rectTransform_m806(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rect_t94  L_1 = RectTransform_get_rect_m1916(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Vector2_t171  L_2 = Rect_get_size_m1921((&V_1), /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = ((&V_2)->___x_1);
		Vector2_t171  L_4 = {0};
		Vector2__ctor_m1764(&L_4, L_3, (0.0f), /*hidden argument*/NULL);
		TextGenerationSettings_t385  L_5 = Text_GetGenerationSettings_m1425(__this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		TextGenerator_t267 * L_6 = Text_get_cachedTextGeneratorForLayout_m1394(__this, /*hidden argument*/NULL);
		String_t* L_7 = (__this->___m_Text_29);
		TextGenerationSettings_t385  L_8 = V_0;
		NullCheck(L_6);
		float L_9 = TextGenerator_GetPreferredHeight_m2244(L_6, L_7, L_8, /*hidden argument*/NULL);
		float L_10 = Text_get_pixelsPerUnit_m1421(__this, /*hidden argument*/NULL);
		return ((float)((float)L_9/(float)L_10));
	}
}
// System.Single UnityEngine.UI.Text::get_flexibleHeight()
extern "C" float Text_get_flexibleHeight_m1435 (Text_t16 * __this, const MethodInfo* method)
{
	{
		return (-1.0f);
	}
}
// System.Int32 UnityEngine.UI.Text::get_layoutPriority()
extern "C" int32_t Text_get_layoutPriority_m1436 (Text_t16 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void UnityEngine.UI.Toggle/ToggleEvent::.ctor()
extern const MethodInfo* UnityEvent_1__ctor_m2146_MethodInfo_var;
extern "C" void ToggleEvent__ctor_m1437 (ToggleEvent_t311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityEvent_1__ctor_m2146_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483879);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityEvent_1__ctor_m2146(__this, /*hidden argument*/UnityEvent_1__ctor_m2146_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::.ctor()
extern TypeInfo* ToggleEvent_t311_il2cpp_TypeInfo_var;
extern TypeInfo* Selectable_t202_il2cpp_TypeInfo_var;
extern "C" void Toggle__ctor_m1438 (Toggle_t213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ToggleEvent_t311_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(273);
		Selectable_t202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(141);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___toggleTransition_16 = 1;
		ToggleEvent_t311 * L_0 = (ToggleEvent_t311 *)il2cpp_codegen_object_new (ToggleEvent_t311_il2cpp_TypeInfo_var);
		ToggleEvent__ctor_m1437(L_0, /*hidden argument*/NULL);
		__this->___onValueChanged_19 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Selectable_t202_il2cpp_TypeInfo_var);
		Selectable__ctor_m1269(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::get_group()
extern "C" ToggleGroup_t312 * Toggle_get_group_m1439 (Toggle_t213 * __this, const MethodInfo* method)
{
	{
		ToggleGroup_t312 * L_0 = (__this->___m_Group_18);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Toggle::set_group(UnityEngine.UI.ToggleGroup)
extern "C" void Toggle_set_group_m1440 (Toggle_t213 * __this, ToggleGroup_t312 * ___value, const MethodInfo* method)
{
	{
		ToggleGroup_t312 * L_0 = ___value;
		__this->___m_Group_18 = L_0;
		ToggleGroup_t312 * L_1 = (__this->___m_Group_18);
		Toggle_SetToggleGroup_m1447(__this, L_1, 1, /*hidden argument*/NULL);
		Toggle_PlayEffect_m1452(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::Rebuild(UnityEngine.UI.CanvasUpdate)
extern "C" void Toggle_Rebuild_m1441 (Toggle_t213 * __this, int32_t ___executing, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::LayoutComplete()
extern "C" void Toggle_LayoutComplete_m1442 (Toggle_t213 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::GraphicUpdateComplete()
extern "C" void Toggle_GraphicUpdateComplete_m1443 (Toggle_t213 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::OnEnable()
extern "C" void Toggle_OnEnable_m1444 (Toggle_t213 * __this, const MethodInfo* method)
{
	{
		Selectable_OnEnable_m1299(__this, /*hidden argument*/NULL);
		ToggleGroup_t312 * L_0 = (__this->___m_Group_18);
		Toggle_SetToggleGroup_m1447(__this, L_0, 0, /*hidden argument*/NULL);
		Toggle_PlayEffect_m1452(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::OnDisable()
extern "C" void Toggle_OnDisable_m1445 (Toggle_t213 * __this, const MethodInfo* method)
{
	{
		Toggle_SetToggleGroup_m1447(__this, (ToggleGroup_t312 *)NULL, 0, /*hidden argument*/NULL);
		Selectable_OnDisable_m1301(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::OnDidApplyAnimationProperties()
extern TypeInfo* Mathf_t394_il2cpp_TypeInfo_var;
extern "C" void Toggle_OnDidApplyAnimationProperties_m1446 (Toggle_t213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Color_t77  V_1 = {0};
	{
		Graphic_t228 * L_0 = (__this->___graphic_17);
		bool L_1 = Object_op_Inequality_m150(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0054;
		}
	}
	{
		Graphic_t228 * L_2 = (__this->___graphic_17);
		NullCheck(L_2);
		CanvasRenderer_t229 * L_3 = Graphic_get_canvasRenderer_m809(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Color_t77  L_4 = CanvasRenderer_GetColor_m1986(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = ((&V_1)->___a_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		bool L_6 = Mathf_Approximately_m1795(NULL /*static, unused*/, L_5, (0.0f), /*hidden argument*/NULL);
		V_0 = ((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		bool L_7 = (__this->___m_IsOn_20);
		bool L_8 = V_0;
		if ((((int32_t)L_7) == ((int32_t)L_8)))
		{
			goto IL_0054;
		}
	}
	{
		bool L_9 = V_0;
		__this->___m_IsOn_20 = L_9;
		bool L_10 = V_0;
		Toggle_Set_m1450(__this, ((((int32_t)L_10) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0054:
	{
		Selectable_OnDidApplyAnimationProperties_m1298(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::SetToggleGroup(UnityEngine.UI.ToggleGroup,System.Boolean)
extern "C" void Toggle_SetToggleGroup_m1447 (Toggle_t213 * __this, ToggleGroup_t312 * ___newGroup, bool ___setMemberValue, const MethodInfo* method)
{
	ToggleGroup_t312 * V_0 = {0};
	{
		ToggleGroup_t312 * L_0 = (__this->___m_Group_18);
		V_0 = L_0;
		ToggleGroup_t312 * L_1 = (__this->___m_Group_18);
		bool L_2 = Object_op_Inequality_m150(NULL /*static, unused*/, L_1, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		ToggleGroup_t312 * L_3 = (__this->___m_Group_18);
		NullCheck(L_3);
		ToggleGroup_UnregisterToggle_m1464(L_3, __this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		bool L_4 = ___setMemberValue;
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		ToggleGroup_t312 * L_5 = ___newGroup;
		__this->___m_Group_18 = L_5;
	}

IL_0031:
	{
		ToggleGroup_t312 * L_6 = (__this->___m_Group_18);
		bool L_7 = Object_op_Inequality_m150(NULL /*static, unused*/, L_6, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0059;
		}
	}
	{
		bool L_8 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (!L_8)
		{
			goto IL_0059;
		}
	}
	{
		ToggleGroup_t312 * L_9 = (__this->___m_Group_18);
		NullCheck(L_9);
		ToggleGroup_RegisterToggle_m1465(L_9, __this, /*hidden argument*/NULL);
	}

IL_0059:
	{
		ToggleGroup_t312 * L_10 = ___newGroup;
		bool L_11 = Object_op_Inequality_m150(NULL /*static, unused*/, L_10, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0093;
		}
	}
	{
		ToggleGroup_t312 * L_12 = ___newGroup;
		ToggleGroup_t312 * L_13 = V_0;
		bool L_14 = Object_op_Inequality_m150(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0093;
		}
	}
	{
		bool L_15 = Toggle_get_isOn_m1448(__this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0093;
		}
	}
	{
		bool L_16 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (!L_16)
		{
			goto IL_0093;
		}
	}
	{
		ToggleGroup_t312 * L_17 = (__this->___m_Group_18);
		NullCheck(L_17);
		ToggleGroup_NotifyToggleOn_m1463(L_17, __this, /*hidden argument*/NULL);
	}

IL_0093:
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.Toggle::get_isOn()
extern "C" bool Toggle_get_isOn_m1448 (Toggle_t213 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_IsOn_20);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Toggle::set_isOn(System.Boolean)
extern "C" void Toggle_set_isOn_m1449 (Toggle_t213 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		Toggle_Set_m1450(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean)
extern "C" void Toggle_Set_m1450 (Toggle_t213 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		Toggle_Set_m1451(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean,System.Boolean)
extern const MethodInfo* UnityEvent_1_Invoke_m2151_MethodInfo_var;
extern "C" void Toggle_Set_m1451 (Toggle_t213 * __this, bool ___value, bool ___sendCallback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityEvent_1_Invoke_m2151_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483881);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_IsOn_20);
		bool L_1 = ___value;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___value;
		__this->___m_IsOn_20 = L_2;
		ToggleGroup_t312 * L_3 = (__this->___m_Group_18);
		bool L_4 = Object_op_Inequality_m150(NULL /*static, unused*/, L_3, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006e;
		}
	}
	{
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (!L_5)
		{
			goto IL_006e;
		}
	}
	{
		bool L_6 = (__this->___m_IsOn_20);
		if (L_6)
		{
			goto IL_005b;
		}
	}
	{
		ToggleGroup_t312 * L_7 = (__this->___m_Group_18);
		NullCheck(L_7);
		bool L_8 = ToggleGroup_AnyTogglesOn_m1466(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_006e;
		}
	}
	{
		ToggleGroup_t312 * L_9 = (__this->___m_Group_18);
		NullCheck(L_9);
		bool L_10 = ToggleGroup_get_allowSwitchOff_m1460(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_006e;
		}
	}

IL_005b:
	{
		__this->___m_IsOn_20 = 1;
		ToggleGroup_t312 * L_11 = (__this->___m_Group_18);
		NullCheck(L_11);
		ToggleGroup_NotifyToggleOn_m1463(L_11, __this, /*hidden argument*/NULL);
	}

IL_006e:
	{
		int32_t L_12 = (__this->___toggleTransition_16);
		Toggle_PlayEffect_m1452(__this, ((((int32_t)L_12) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		bool L_13 = ___sendCallback;
		if (!L_13)
		{
			goto IL_0094;
		}
	}
	{
		ToggleEvent_t311 * L_14 = (__this->___onValueChanged_19);
		bool L_15 = (__this->___m_IsOn_20);
		NullCheck(L_14);
		UnityEvent_1_Invoke_m2151(L_14, L_15, /*hidden argument*/UnityEvent_1_Invoke_m2151_MethodInfo_var);
	}

IL_0094:
	{
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::PlayEffect(System.Boolean)
extern "C" void Toggle_PlayEffect_m1452 (Toggle_t213 * __this, bool ___instant, const MethodInfo* method)
{
	Graphic_t228 * G_B4_0 = {0};
	Graphic_t228 * G_B3_0 = {0};
	float G_B5_0 = 0.0f;
	Graphic_t228 * G_B5_1 = {0};
	float G_B7_0 = 0.0f;
	Graphic_t228 * G_B7_1 = {0};
	float G_B6_0 = 0.0f;
	Graphic_t228 * G_B6_1 = {0};
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	Graphic_t228 * G_B8_2 = {0};
	{
		Graphic_t228 * L_0 = (__this->___graphic_17);
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Graphic_t228 * L_2 = (__this->___graphic_17);
		bool L_3 = (__this->___m_IsOn_20);
		G_B3_0 = L_2;
		if (!L_3)
		{
			G_B4_0 = L_2;
			goto IL_002d;
		}
	}
	{
		G_B5_0 = (1.0f);
		G_B5_1 = G_B3_0;
		goto IL_0032;
	}

IL_002d:
	{
		G_B5_0 = (0.0f);
		G_B5_1 = G_B4_0;
	}

IL_0032:
	{
		bool L_4 = ___instant;
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
		if (!L_4)
		{
			G_B7_0 = G_B5_0;
			G_B7_1 = G_B5_1;
			goto IL_0042;
		}
	}
	{
		G_B8_0 = (0.0f);
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		goto IL_0047;
	}

IL_0042:
	{
		G_B8_0 = (0.1f);
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
	}

IL_0047:
	{
		NullCheck(G_B8_2);
		Graphic_CrossFadeAlpha_m837(G_B8_2, G_B8_1, G_B8_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::Start()
extern "C" void Toggle_Start_m1453 (Toggle_t213 * __this, const MethodInfo* method)
{
	{
		Toggle_PlayEffect_m1452(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::InternalToggle()
extern "C" void Toggle_InternalToggle_m1454 (Toggle_t213 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(23 /* System.Boolean UnityEngine.UI.Selectable::IsInteractable() */, __this);
		if (L_1)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		return;
	}

IL_0017:
	{
		bool L_2 = Toggle_get_isOn_m1448(__this, /*hidden argument*/NULL);
		Toggle_set_isOn_m1449(__this, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C" void Toggle_OnPointerClick_m1455 (Toggle_t213 * __this, PointerEventData_t175 * ___eventData, const MethodInfo* method)
{
	{
		PointerEventData_t175 * L_0 = ___eventData;
		NullCheck(L_0);
		int32_t L_1 = PointerEventData_get_button_m470(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Toggle_InternalToggle_m1454(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern "C" void Toggle_OnSubmit_m1456 (Toggle_t213 * __this, BaseEventData_t137 * ___eventData, const MethodInfo* method)
{
	{
		Toggle_InternalToggle_m1454(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern "C" bool Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1457 (Toggle_t213 * __this, const MethodInfo* method)
{
	{
		bool L_0 = UIBehaviour_IsDestroyed_m420(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.get_transform()
extern "C" Transform_t84 * Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1458 (Toggle_t213 * __this, const MethodInfo* method)
{
	{
		Transform_t84 * L_0 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.UI.ToggleGroup::.ctor()
extern TypeInfo* List_1_t313_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2245_MethodInfo_var;
extern "C" void ToggleGroup__ctor_m1459 (ToggleGroup_t312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t313_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(274);
		List_1__ctor_m2245_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483904);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t313 * L_0 = (List_1_t313 *)il2cpp_codegen_object_new (List_1_t313_il2cpp_TypeInfo_var);
		List_1__ctor_m2245(L_0, /*hidden argument*/List_1__ctor_m2245_MethodInfo_var);
		__this->___m_Toggles_3 = L_0;
		UIBehaviour__ctor_m407(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.ToggleGroup::get_allowSwitchOff()
extern "C" bool ToggleGroup_get_allowSwitchOff_m1460 (ToggleGroup_t312 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_AllowSwitchOff_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.ToggleGroup::set_allowSwitchOff(System.Boolean)
extern "C" void ToggleGroup_set_allowSwitchOff_m1461 (ToggleGroup_t312 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_AllowSwitchOff_2 = L_0;
		return;
	}
}
// System.Void UnityEngine.UI.ToggleGroup::ValidateToggleIsInGroup(UnityEngine.UI.Toggle)
extern TypeInfo* ObjectU5BU5D_t105_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral142;
extern "C" void ToggleGroup_ValidateToggleIsInGroup_m1462 (ToggleGroup_t312 * __this, Toggle_t213 * ___toggle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		_stringLiteral142 = il2cpp_codegen_string_literal_from_index(142);
		s_Il2CppMethodIntialized = true;
	}
	{
		Toggle_t213 * L_0 = ___toggle;
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		List_1_t313 * L_2 = (__this->___m_Toggles_3);
		Toggle_t213 * L_3 = ___toggle;
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker1< bool, Toggle_t213 * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Contains(!0) */, L_2, L_3);
		if (L_4)
		{
			goto IL_003b;
		}
	}

IL_001d:
	{
		ObjectU5BU5D_t105* L_5 = ((ObjectU5BU5D_t105*)SZArrayNew(ObjectU5BU5D_t105_il2cpp_TypeInfo_var, 2));
		Toggle_t213 * L_6 = ___toggle;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 0, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t105* L_7 = L_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		ArrayElementTypeCheck (L_7, __this);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 1, sizeof(Object_t *))) = (Object_t *)__this;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m2233(NULL /*static, unused*/, _stringLiteral142, L_7, /*hidden argument*/NULL);
		ArgumentException_t442 * L_9 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_9, L_8, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_003b:
	{
		return;
	}
}
// System.Void UnityEngine.UI.ToggleGroup::NotifyToggleOn(UnityEngine.UI.Toggle)
extern "C" void ToggleGroup_NotifyToggleOn_m1463 (ToggleGroup_t312 * __this, Toggle_t213 * ___toggle, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Toggle_t213 * L_0 = ___toggle;
		ToggleGroup_ValidateToggleIsInGroup_m1462(__this, L_0, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0040;
	}

IL_000e:
	{
		List_1_t313 * L_1 = (__this->___m_Toggles_3);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Toggle_t213 * L_3 = (Toggle_t213 *)VirtFuncInvoker1< Toggle_t213 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::get_Item(System.Int32) */, L_1, L_2);
		Toggle_t213 * L_4 = ___toggle;
		bool L_5 = Object_op_Equality_m165(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		goto IL_003c;
	}

IL_002a:
	{
		List_1_t313 * L_6 = (__this->___m_Toggles_3);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Toggle_t213 * L_8 = (Toggle_t213 *)VirtFuncInvoker1< Toggle_t213 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::get_Item(System.Int32) */, L_6, L_7);
		NullCheck(L_8);
		Toggle_set_isOn_m1449(L_8, 0, /*hidden argument*/NULL);
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_10 = V_0;
		List_1_t313 * L_11 = (__this->___m_Toggles_3);
		NullCheck(L_11);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.ToggleGroup::UnregisterToggle(UnityEngine.UI.Toggle)
extern "C" void ToggleGroup_UnregisterToggle_m1464 (ToggleGroup_t312 * __this, Toggle_t213 * ___toggle, const MethodInfo* method)
{
	{
		List_1_t313 * L_0 = (__this->___m_Toggles_3);
		Toggle_t213 * L_1 = ___toggle;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Toggle_t213 * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Contains(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t313 * L_3 = (__this->___m_Toggles_3);
		Toggle_t213 * L_4 = ___toggle;
		NullCheck(L_3);
		VirtFuncInvoker1< bool, Toggle_t213 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Remove(!0) */, L_3, L_4);
	}

IL_001e:
	{
		return;
	}
}
// System.Void UnityEngine.UI.ToggleGroup::RegisterToggle(UnityEngine.UI.Toggle)
extern "C" void ToggleGroup_RegisterToggle_m1465 (ToggleGroup_t312 * __this, Toggle_t213 * ___toggle, const MethodInfo* method)
{
	{
		List_1_t313 * L_0 = (__this->___m_Toggles_3);
		Toggle_t213 * L_1 = ___toggle;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Toggle_t213 * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Contains(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		List_1_t313 * L_3 = (__this->___m_Toggles_3);
		Toggle_t213 * L_4 = ___toggle;
		NullCheck(L_3);
		VirtActionInvoker1< Toggle_t213 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Add(!0) */, L_3, L_4);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.ToggleGroup::AnyTogglesOn()
extern TypeInfo* ToggleGroup_t312_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t314_il2cpp_TypeInfo_var;
extern const MethodInfo* ToggleGroup_U3CAnyTogglesOnU3Em__4_m1469_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m2247_MethodInfo_var;
extern const MethodInfo* List_1_Find_m2248_MethodInfo_var;
extern "C" bool ToggleGroup_AnyTogglesOn_m1466 (ToggleGroup_t312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ToggleGroup_t312_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(276);
		Predicate_1_t314_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(277);
		ToggleGroup_U3CAnyTogglesOnU3Em__4_m1469_MethodInfo_var = il2cpp_codegen_method_info_from_index(257);
		Predicate_1__ctor_m2247_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483906);
		List_1_Find_m2248_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483907);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t313 * G_B2_0 = {0};
	List_1_t313 * G_B1_0 = {0};
	{
		List_1_t313 * L_0 = (__this->___m_Toggles_3);
		Predicate_1_t314 * L_1 = ((ToggleGroup_t312_StaticFields*)ToggleGroup_t312_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_4;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2 = { (void*)ToggleGroup_U3CAnyTogglesOnU3Em__4_m1469_MethodInfo_var };
		Predicate_1_t314 * L_3 = (Predicate_1_t314 *)il2cpp_codegen_object_new (Predicate_1_t314_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m2247(L_3, NULL, L_2, /*hidden argument*/Predicate_1__ctor_m2247_MethodInfo_var);
		((ToggleGroup_t312_StaticFields*)ToggleGroup_t312_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_4 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Predicate_1_t314 * L_4 = ((ToggleGroup_t312_StaticFields*)ToggleGroup_t312_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_4;
		NullCheck(G_B2_0);
		Toggle_t213 * L_5 = List_1_Find_m2248(G_B2_0, L_4, /*hidden argument*/List_1_Find_m2248_MethodInfo_var);
		bool L_6 = Object_op_Inequality_m150(NULL /*static, unused*/, L_5, (Object_t86 *)NULL, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::ActiveToggles()
extern TypeInfo* ToggleGroup_t312_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t315_il2cpp_TypeInfo_var;
extern const MethodInfo* ToggleGroup_U3CActiveTogglesU3Em__5_m1470_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2249_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisToggle_t213_m2250_MethodInfo_var;
extern "C" Object_t* ToggleGroup_ActiveToggles_m1467 (ToggleGroup_t312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ToggleGroup_t312_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(276);
		Func_2_t315_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(278);
		ToggleGroup_U3CActiveTogglesU3Em__5_m1470_MethodInfo_var = il2cpp_codegen_method_info_from_index(260);
		Func_2__ctor_m2249_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483909);
		Enumerable_Where_TisToggle_t213_m2250_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483910);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t313 * G_B2_0 = {0};
	List_1_t313 * G_B1_0 = {0};
	{
		List_1_t313 * L_0 = (__this->___m_Toggles_3);
		Func_2_t315 * L_1 = ((ToggleGroup_t312_StaticFields*)ToggleGroup_t312_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_5;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2 = { (void*)ToggleGroup_U3CActiveTogglesU3Em__5_m1470_MethodInfo_var };
		Func_2_t315 * L_3 = (Func_2_t315 *)il2cpp_codegen_object_new (Func_2_t315_il2cpp_TypeInfo_var);
		Func_2__ctor_m2249(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m2249_MethodInfo_var);
		((ToggleGroup_t312_StaticFields*)ToggleGroup_t312_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_5 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Func_2_t315 * L_4 = ((ToggleGroup_t312_StaticFields*)ToggleGroup_t312_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_5;
		Object_t* L_5 = Enumerable_Where_TisToggle_t213_m2250(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Enumerable_Where_TisToggle_t213_m2250_MethodInfo_var);
		return L_5;
	}
}
// System.Void UnityEngine.UI.ToggleGroup::SetAllTogglesOff()
extern "C" void ToggleGroup_SetAllTogglesOff_m1468 (ToggleGroup_t312 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		bool L_0 = (__this->___m_AllowSwitchOff_2);
		V_0 = L_0;
		__this->___m_AllowSwitchOff_2 = 1;
		V_1 = 0;
		goto IL_002b;
	}

IL_0015:
	{
		List_1_t313 * L_1 = (__this->___m_Toggles_3);
		int32_t L_2 = V_1;
		NullCheck(L_1);
		Toggle_t213 * L_3 = (Toggle_t213 *)VirtFuncInvoker1< Toggle_t213 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::get_Item(System.Int32) */, L_1, L_2);
		NullCheck(L_3);
		Toggle_set_isOn_m1449(L_3, 0, /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_5 = V_1;
		List_1_t313 * L_6 = (__this->___m_Toggles_3);
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::get_Count() */, L_6);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0015;
		}
	}
	{
		bool L_8 = V_0;
		__this->___m_AllowSwitchOff_2 = L_8;
		return;
	}
}
// System.Boolean UnityEngine.UI.ToggleGroup::<AnyTogglesOn>m__4(UnityEngine.UI.Toggle)
extern "C" bool ToggleGroup_U3CAnyTogglesOnU3Em__4_m1469 (Object_t * __this /* static, unused */, Toggle_t213 * ___x, const MethodInfo* method)
{
	{
		Toggle_t213 * L_0 = ___x;
		NullCheck(L_0);
		bool L_1 = Toggle_get_isOn_m1448(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.UI.ToggleGroup::<ActiveToggles>m__5(UnityEngine.UI.Toggle)
extern "C" bool ToggleGroup_U3CActiveTogglesU3Em__5_m1470 (Object_t * __this /* static, unused */, Toggle_t213 * ___x, const MethodInfo* method)
{
	{
		Toggle_t213 * L_0 = ___x;
		NullCheck(L_0);
		bool L_1 = Toggle_get_isOn_m1448(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.ClipperRegistry::.ctor()
extern TypeInfo* IndexedSet_1_t317_il2cpp_TypeInfo_var;
extern const MethodInfo* IndexedSet_1__ctor_m2251_MethodInfo_var;
extern "C" void ClipperRegistry__ctor_m1471 (ClipperRegistry_t316 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexedSet_1_t317_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(280);
		IndexedSet_1__ctor_m2251_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483911);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t443 * V_0 = {0};
	{
		IndexedSet_1_t317 * L_0 = (IndexedSet_1_t317 *)il2cpp_codegen_object_new (IndexedSet_1_t317_il2cpp_TypeInfo_var);
		IndexedSet_1__ctor_m2251(L_0, /*hidden argument*/IndexedSet_1__ctor_m2251_MethodInfo_var);
		__this->___m_Clippers_1 = L_0;
		Object__ctor_m215(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.ClipperRegistry UnityEngine.UI.ClipperRegistry::get_instance()
extern TypeInfo* ClipperRegistry_t316_il2cpp_TypeInfo_var;
extern "C" ClipperRegistry_t316 * ClipperRegistry_get_instance_m1472 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ClipperRegistry_t316_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(281);
		s_Il2CppMethodIntialized = true;
	}
	{
		ClipperRegistry_t316 * L_0 = ((ClipperRegistry_t316_StaticFields*)ClipperRegistry_t316_il2cpp_TypeInfo_var->static_fields)->___s_Instance_0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		ClipperRegistry_t316 * L_1 = (ClipperRegistry_t316 *)il2cpp_codegen_object_new (ClipperRegistry_t316_il2cpp_TypeInfo_var);
		ClipperRegistry__ctor_m1471(L_1, /*hidden argument*/NULL);
		((ClipperRegistry_t316_StaticFields*)ClipperRegistry_t316_il2cpp_TypeInfo_var->static_fields)->___s_Instance_0 = L_1;
	}

IL_0014:
	{
		ClipperRegistry_t316 * L_2 = ((ClipperRegistry_t316_StaticFields*)ClipperRegistry_t316_il2cpp_TypeInfo_var->static_fields)->___s_Instance_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ClipperRegistry::Cull()
extern TypeInfo* IClipper_t387_il2cpp_TypeInfo_var;
extern "C" void ClipperRegistry_Cull_m1473 (ClipperRegistry_t316 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IClipper_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(279);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_001c;
	}

IL_0007:
	{
		IndexedSet_1_t317 * L_0 = (__this->___m_Clippers_1);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(7 /* T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::get_Item(System.Int32) */, L_0, L_1);
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.UI.IClipper::PerformClipping() */, IClipper_t387_il2cpp_TypeInfo_var, L_2);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001c:
	{
		int32_t L_4 = V_0;
		IndexedSet_1_t317 * L_5 = (__this->___m_Clippers_1);
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::get_Count() */, L_5);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.ClipperRegistry::Register(UnityEngine.UI.IClipper)
extern "C" void ClipperRegistry_Register_m1474 (Object_t * __this /* static, unused */, Object_t * ___c, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___c;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		ClipperRegistry_t316 * L_1 = ClipperRegistry_get_instance_m1472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		IndexedSet_1_t317 * L_2 = (L_1->___m_Clippers_1);
		Object_t * L_3 = ___c;
		NullCheck(L_2);
		VirtActionInvoker1< Object_t * >::Invoke(12 /* System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Add(T) */, L_2, L_3);
		return;
	}
}
// System.Void UnityEngine.UI.ClipperRegistry::Unregister(UnityEngine.UI.IClipper)
extern "C" void ClipperRegistry_Unregister_m1475 (Object_t * __this /* static, unused */, Object_t * ___c, const MethodInfo* method)
{
	{
		ClipperRegistry_t316 * L_0 = ClipperRegistry_get_instance_m1472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		IndexedSet_1_t317 * L_1 = (L_0->___m_Clippers_1);
		Object_t * L_2 = ___c;
		NullCheck(L_1);
		VirtFuncInvoker1< bool, Object_t * >::Invoke(16 /* System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Remove(T) */, L_1, L_2);
		return;
	}
}
// UnityEngine.Rect UnityEngine.UI.Clipping::FindCullAndClipWorldRect(System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>,System.Boolean&)
extern TypeInfo* Rect_t94_il2cpp_TypeInfo_var;
extern "C" Rect_t94  Clipping_FindCullAndClipWorldRect_m1476 (Object_t * __this /* static, unused */, List_1_t285 * ___rectMaskParents, bool* ___validRect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Rect_t94_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(250);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t94  V_0 = {0};
	int32_t V_1 = 0;
	bool V_2 = false;
	Vector3_t12  V_3 = {0};
	Vector3_t12  V_4 = {0};
	Rect_t94  V_5 = {0};
	Rect_t94  V_6 = {0};
	int32_t G_B8_0 = 0;
	{
		List_1_t285 * L_0 = ___rectMaskParents;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		bool* L_2 = ___validRect;
		*((int8_t*)(L_2)) = (int8_t)0;
		Initobj (Rect_t94_il2cpp_TypeInfo_var, (&V_5));
		Rect_t94  L_3 = V_5;
		return L_3;
	}

IL_0019:
	{
		List_1_t285 * L_4 = ___rectMaskParents;
		NullCheck(L_4);
		RectMask2D_t276 * L_5 = (RectMask2D_t276 *)VirtFuncInvoker1< RectMask2D_t276 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::get_Item(System.Int32) */, L_4, 0);
		NullCheck(L_5);
		Rect_t94  L_6 = RectMask2D_get_canvasRect_m1120(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		V_1 = 0;
		goto IL_0044;
	}

IL_002d:
	{
		Rect_t94  L_7 = V_0;
		List_1_t285 * L_8 = ___rectMaskParents;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		RectMask2D_t276 * L_10 = (RectMask2D_t276 *)VirtFuncInvoker1< RectMask2D_t276 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::get_Item(System.Int32) */, L_8, L_9);
		NullCheck(L_10);
		Rect_t94  L_11 = RectMask2D_get_canvasRect_m1120(L_10, /*hidden argument*/NULL);
		Rect_t94  L_12 = Clipping_RectIntersect_m1477(NULL /*static, unused*/, L_7, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0044:
	{
		int32_t L_14 = V_1;
		List_1_t285 * L_15 = ___rectMaskParents;
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::get_Count() */, L_15);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_002d;
		}
	}
	{
		float L_17 = Rect_get_width_m1972((&V_0), /*hidden argument*/NULL);
		if ((((float)L_17) <= ((float)(0.0f))))
		{
			goto IL_0074;
		}
	}
	{
		float L_18 = Rect_get_height_m1924((&V_0), /*hidden argument*/NULL);
		G_B8_0 = ((((int32_t)((!(((float)L_18) <= ((float)(0.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0075;
	}

IL_0074:
	{
		G_B8_0 = 1;
	}

IL_0075:
	{
		V_2 = G_B8_0;
		bool L_19 = V_2;
		if (!L_19)
		{
			goto IL_008a;
		}
	}
	{
		bool* L_20 = ___validRect;
		*((int8_t*)(L_20)) = (int8_t)0;
		Initobj (Rect_t94_il2cpp_TypeInfo_var, (&V_6));
		Rect_t94  L_21 = V_6;
		return L_21;
	}

IL_008a:
	{
		float L_22 = Rect_get_x_m1976((&V_0), /*hidden argument*/NULL);
		float L_23 = Rect_get_y_m1977((&V_0), /*hidden argument*/NULL);
		Vector3__ctor_m169((&V_3), L_22, L_23, (0.0f), /*hidden argument*/NULL);
		float L_24 = Rect_get_x_m1976((&V_0), /*hidden argument*/NULL);
		float L_25 = Rect_get_width_m1972((&V_0), /*hidden argument*/NULL);
		float L_26 = Rect_get_y_m1977((&V_0), /*hidden argument*/NULL);
		float L_27 = Rect_get_height_m1924((&V_0), /*hidden argument*/NULL);
		Vector3__ctor_m169((&V_4), ((float)((float)L_24+(float)L_25)), ((float)((float)L_26+(float)L_27)), (0.0f), /*hidden argument*/NULL);
		bool* L_28 = ___validRect;
		*((int8_t*)(L_28)) = (int8_t)1;
		float L_29 = ((&V_3)->___x_1);
		float L_30 = ((&V_3)->___y_2);
		float L_31 = ((&V_4)->___x_1);
		float L_32 = ((&V_3)->___x_1);
		float L_33 = ((&V_4)->___y_2);
		float L_34 = ((&V_3)->___y_2);
		Rect_t94  L_35 = {0};
		Rect__ctor_m187(&L_35, L_29, L_30, ((float)((float)L_31-(float)L_32)), ((float)((float)L_33-(float)L_34)), /*hidden argument*/NULL);
		return L_35;
	}
}
// UnityEngine.Rect UnityEngine.UI.Clipping::RectIntersect(UnityEngine.Rect,UnityEngine.Rect)
extern TypeInfo* Mathf_t394_il2cpp_TypeInfo_var;
extern "C" Rect_t94  Clipping_RectIntersect_m1477 (Object_t * __this /* static, unused */, Rect_t94  ___a, Rect_t94  ___b, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = Rect_get_x_m1976((&___a), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m1976((&___b), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m2208(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m1976((&___a), /*hidden argument*/NULL);
		float L_4 = Rect_get_width_m1972((&___a), /*hidden argument*/NULL);
		float L_5 = Rect_get_x_m1976((&___b), /*hidden argument*/NULL);
		float L_6 = Rect_get_width_m1972((&___b), /*hidden argument*/NULL);
		float L_7 = Mathf_Min_m2252(NULL /*static, unused*/, ((float)((float)L_3+(float)L_4)), ((float)((float)L_5+(float)L_6)), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Rect_get_y_m1977((&___a), /*hidden argument*/NULL);
		float L_9 = Rect_get_y_m1977((&___b), /*hidden argument*/NULL);
		float L_10 = Mathf_Max_m2208(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = Rect_get_y_m1977((&___a), /*hidden argument*/NULL);
		float L_12 = Rect_get_height_m1924((&___a), /*hidden argument*/NULL);
		float L_13 = Rect_get_y_m1977((&___b), /*hidden argument*/NULL);
		float L_14 = Rect_get_height_m1924((&___b), /*hidden argument*/NULL);
		float L_15 = Mathf_Min_m2252(NULL /*static, unused*/, ((float)((float)L_11+(float)L_12)), ((float)((float)L_13+(float)L_14)), /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = V_1;
		float L_17 = V_0;
		if ((!(((float)L_16) >= ((float)L_17))))
		{
			goto IL_008c;
		}
	}
	{
		float L_18 = V_3;
		float L_19 = V_2;
		if ((!(((float)L_18) >= ((float)L_19))))
		{
			goto IL_008c;
		}
	}
	{
		float L_20 = V_0;
		float L_21 = V_2;
		float L_22 = V_1;
		float L_23 = V_0;
		float L_24 = V_3;
		float L_25 = V_2;
		Rect_t94  L_26 = {0};
		Rect__ctor_m187(&L_26, L_20, L_21, ((float)((float)L_22-(float)L_23)), ((float)((float)L_24-(float)L_25)), /*hidden argument*/NULL);
		return L_26;
	}

IL_008c:
	{
		Rect_t94  L_27 = {0};
		Rect__ctor_m187(&L_27, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_27;
	}
}
// System.Void UnityEngine.UI.RectangularVertexClipper::.ctor()
extern TypeInfo* Vector3U5BU5D_t121_il2cpp_TypeInfo_var;
extern "C" void RectangularVertexClipper__ctor_m1478 (RectangularVertexClipper_t283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(179);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_WorldCorners_0 = ((Vector3U5BU5D_t121*)SZArrayNew(Vector3U5BU5D_t121_il2cpp_TypeInfo_var, 4));
		__this->___m_CanvasCorners_1 = ((Vector3U5BU5D_t121*)SZArrayNew(Vector3U5BU5D_t121_il2cpp_TypeInfo_var, 4));
		Object__ctor_m215(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect UnityEngine.UI.RectangularVertexClipper::GetCanvasRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern const MethodInfo* Component_GetComponent_TisTransform_t84_m2253_MethodInfo_var;
extern "C" Rect_t94  RectangularVertexClipper_GetCanvasRect_m1479 (RectangularVertexClipper_t283 * __this, RectTransform_t212 * ___t, Canvas_t230 * ___c, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisTransform_t84_m2253_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483912);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t84 * V_0 = {0};
	int32_t V_1 = 0;
	{
		RectTransform_t212 * L_0 = ___t;
		Vector3U5BU5D_t121* L_1 = (__this->___m_WorldCorners_0);
		NullCheck(L_0);
		RectTransform_GetWorldCorners_m1925(L_0, L_1, /*hidden argument*/NULL);
		Canvas_t230 * L_2 = ___c;
		NullCheck(L_2);
		Transform_t84 * L_3 = Component_GetComponent_TisTransform_t84_m2253(L_2, /*hidden argument*/Component_GetComponent_TisTransform_t84_m2253_MethodInfo_var);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0046;
	}

IL_001a:
	{
		Vector3U5BU5D_t121* L_4 = (__this->___m_CanvasCorners_1);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Transform_t84 * L_6 = V_0;
		Vector3U5BU5D_t121* L_7 = (__this->___m_WorldCorners_0);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		NullCheck(L_6);
		Vector3_t12  L_9 = Transform_InverseTransformPoint_m1926(L_6, (*(Vector3_t12 *)((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_7, L_8, sizeof(Vector3_t12 )))), /*hidden argument*/NULL);
		(*(Vector3_t12 *)((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_4, L_5, sizeof(Vector3_t12 )))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0046:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_001a;
		}
	}
	{
		Vector3U5BU5D_t121* L_12 = (__this->___m_CanvasCorners_1);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		float L_13 = (((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_12, 0, sizeof(Vector3_t12 )))->___x_1);
		Vector3U5BU5D_t121* L_14 = (__this->___m_CanvasCorners_1);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		float L_15 = (((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_14, 0, sizeof(Vector3_t12 )))->___y_2);
		Vector3U5BU5D_t121* L_16 = (__this->___m_CanvasCorners_1);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		float L_17 = (((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_16, 2, sizeof(Vector3_t12 )))->___x_1);
		Vector3U5BU5D_t121* L_18 = (__this->___m_CanvasCorners_1);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		float L_19 = (((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_18, 0, sizeof(Vector3_t12 )))->___x_1);
		Vector3U5BU5D_t121* L_20 = (__this->___m_CanvasCorners_1);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		float L_21 = (((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_20, 2, sizeof(Vector3_t12 )))->___y_2);
		Vector3U5BU5D_t121* L_22 = (__this->___m_CanvasCorners_1);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		float L_23 = (((Vector3_t12 *)(Vector3_t12 *)SZArrayLdElema(L_22, 0, sizeof(Vector3_t12 )))->___y_2);
		Rect_t94  L_24 = {0};
		Rect__ctor_m187(&L_24, L_13, L_15, ((float)((float)L_17-(float)L_19)), ((float)((float)L_21-(float)L_23)), /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::.ctor()
extern "C" void AspectRatioFitter__ctor_m1480 (AspectRatioFitter_t320 * __this, const MethodInfo* method)
{
	{
		__this->___m_AspectRatio_3 = (1.0f);
		UIBehaviour__ctor_m407(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::get_aspectMode()
extern "C" int32_t AspectRatioFitter_get_aspectMode_m1481 (AspectRatioFitter_t320 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_AspectMode_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectMode(UnityEngine.UI.AspectRatioFitter/AspectMode)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisAspectMode_t319_m2254_MethodInfo_var;
extern "C" void AspectRatioFitter_set_aspectMode_m1482 (AspectRatioFitter_t320 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisAspectMode_t319_m2254_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483913);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_AspectMode_2);
		int32_t L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisAspectMode_t319_m2254(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisAspectMode_t319_m2254_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		AspectRatioFitter_SetDirty_m1494(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.AspectRatioFitter::get_aspectRatio()
extern "C" float AspectRatioFitter_get_aspectRatio_m1483 (AspectRatioFitter_t320 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_AspectRatio_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectRatio(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var;
extern "C" void AspectRatioFitter_set_aspectRatio_m1484 (AspectRatioFitter_t320 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483858);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_AspectRatio_3);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t117_m2017(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		AspectRatioFitter_SetDirty_m1494(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::get_rectTransform()
extern const MethodInfo* Component_GetComponent_TisRectTransform_t212_m1961_MethodInfo_var;
extern "C" RectTransform_t212 * AspectRatioFitter_get_rectTransform_m1485 (AspectRatioFitter_t320 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRectTransform_t212_m1961_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483839);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t212 * L_0 = (__this->___m_Rect_4);
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RectTransform_t212 * L_2 = Component_GetComponent_TisRectTransform_t212_m1961(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t212_m1961_MethodInfo_var);
		__this->___m_Rect_4 = L_2;
	}

IL_001d:
	{
		RectTransform_t212 * L_3 = (__this->___m_Rect_4);
		return L_3;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::OnEnable()
extern "C" void AspectRatioFitter_OnEnable_m1486 (AspectRatioFitter_t320 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m409(__this, /*hidden argument*/NULL);
		AspectRatioFitter_SetDirty_m1494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::OnDisable()
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern "C" void AspectRatioFitter_OnDisable_m1487 (AspectRatioFitter_t320 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		s_Il2CppMethodIntialized = true;
	}
	{
		DrivenRectTransformTracker_t291 * L_0 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m2166(L_0, /*hidden argument*/NULL);
		RectTransform_t212 * L_1 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m1635(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		UIBehaviour_OnDisable_m411(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::OnRectTransformDimensionsChange()
extern "C" void AspectRatioFitter_OnRectTransformDimensionsChange_m1488 (AspectRatioFitter_t320 * __this, const MethodInfo* method)
{
	{
		AspectRatioFitter_UpdateRect_m1489(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::UpdateRect()
extern "C" void AspectRatioFitter_UpdateRect_m1489 (AspectRatioFitter_t320 * __this, const MethodInfo* method)
{
	Vector2_t171  V_0 = {0};
	Vector2_t171  V_1 = {0};
	int32_t V_2 = {0};
	Rect_t94  V_3 = {0};
	Rect_t94  V_4 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		DrivenRectTransformTracker_t291 * L_1 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m2166(L_1, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_AspectMode_2);
		V_2 = L_2;
		int32_t L_3 = V_2;
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 0)
		{
			goto IL_007d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 1)
		{
			goto IL_003b;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 2)
		{
			goto IL_00c0;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 3)
		{
			goto IL_00c0;
		}
	}
	{
		goto IL_0188;
	}

IL_003b:
	{
		DrivenRectTransformTracker_t291 * L_4 = &(__this->___m_Tracker_5);
		RectTransform_t212 * L_5 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		DrivenRectTransformTracker_Add_m2167(L_4, __this, L_5, ((int32_t)4096), /*hidden argument*/NULL);
		RectTransform_t212 * L_6 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		RectTransform_t212 * L_7 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Rect_t94  L_8 = RectTransform_get_rect_m1916(L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = Rect_get_height_m1924((&V_3), /*hidden argument*/NULL);
		float L_10 = (__this->___m_AspectRatio_3);
		NullCheck(L_6);
		RectTransform_SetSizeWithCurrentAnchors_m2255(L_6, 0, ((float)((float)L_9*(float)L_10)), /*hidden argument*/NULL);
		goto IL_0188;
	}

IL_007d:
	{
		DrivenRectTransformTracker_t291 * L_11 = &(__this->___m_Tracker_5);
		RectTransform_t212 * L_12 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		DrivenRectTransformTracker_Add_m2167(L_11, __this, L_12, ((int32_t)8192), /*hidden argument*/NULL);
		RectTransform_t212 * L_13 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		RectTransform_t212 * L_14 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Rect_t94  L_15 = RectTransform_get_rect_m1916(L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = Rect_get_width_m1972((&V_4), /*hidden argument*/NULL);
		float L_17 = (__this->___m_AspectRatio_3);
		NullCheck(L_13);
		RectTransform_SetSizeWithCurrentAnchors_m2255(L_13, 1, ((float)((float)L_16/(float)L_17)), /*hidden argument*/NULL);
		goto IL_0188;
	}

IL_00c0:
	{
		DrivenRectTransformTracker_t291 * L_18 = &(__this->___m_Tracker_5);
		RectTransform_t212 * L_19 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		DrivenRectTransformTracker_Add_m2167(L_18, __this, L_19, ((int32_t)16134), /*hidden argument*/NULL);
		RectTransform_t212 * L_20 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		Vector2_t171  L_21 = Vector2_get_zero_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		RectTransform_set_anchorMin_m1870(L_20, L_21, /*hidden argument*/NULL);
		RectTransform_t212 * L_22 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		Vector2_t171  L_23 = Vector2_get_one_m1871(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchorMax_m1872(L_22, L_23, /*hidden argument*/NULL);
		RectTransform_t212 * L_24 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		Vector2_t171  L_25 = Vector2_get_zero_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		RectTransform_set_anchoredPosition_m1873(L_24, L_25, /*hidden argument*/NULL);
		Vector2_t171  L_26 = Vector2_get_zero_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_26;
		Vector2_t171  L_27 = AspectRatioFitter_GetParentSize_m1491(__this, /*hidden argument*/NULL);
		V_1 = L_27;
		float L_28 = ((&V_1)->___y_2);
		float L_29 = AspectRatioFitter_get_aspectRatio_m1483(__this, /*hidden argument*/NULL);
		float L_30 = ((&V_1)->___x_1);
		int32_t L_31 = (__this->___m_AspectMode_2);
		if (!((int32_t)((int32_t)((((float)((float)((float)L_28*(float)L_29))) < ((float)L_30))? 1 : 0)^(int32_t)((((int32_t)L_31) == ((int32_t)3))? 1 : 0))))
		{
			goto IL_015b;
		}
	}
	{
		float L_32 = ((&V_1)->___x_1);
		float L_33 = AspectRatioFitter_get_aspectRatio_m1483(__this, /*hidden argument*/NULL);
		float L_34 = AspectRatioFitter_GetSizeDeltaToProduceSize_m1490(__this, ((float)((float)L_32/(float)L_33)), 1, /*hidden argument*/NULL);
		(&V_0)->___y_2 = L_34;
		goto IL_0177;
	}

IL_015b:
	{
		float L_35 = ((&V_1)->___y_2);
		float L_36 = AspectRatioFitter_get_aspectRatio_m1483(__this, /*hidden argument*/NULL);
		float L_37 = AspectRatioFitter_GetSizeDeltaToProduceSize_m1490(__this, ((float)((float)L_35*(float)L_36)), 0, /*hidden argument*/NULL);
		(&V_0)->___x_1 = L_37;
	}

IL_0177:
	{
		RectTransform_t212 * L_38 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		Vector2_t171  L_39 = V_0;
		NullCheck(L_38);
		RectTransform_set_sizeDelta_m1863(L_38, L_39, /*hidden argument*/NULL);
		goto IL_0188;
	}

IL_0188:
	{
		return;
	}
}
// System.Single UnityEngine.UI.AspectRatioFitter::GetSizeDeltaToProduceSize(System.Single,System.Int32)
extern "C" float AspectRatioFitter_GetSizeDeltaToProduceSize_m1490 (AspectRatioFitter_t320 * __this, float ___size, int32_t ___axis, const MethodInfo* method)
{
	Vector2_t171  V_0 = {0};
	Vector2_t171  V_1 = {0};
	Vector2_t171  V_2 = {0};
	{
		float L_0 = ___size;
		Vector2_t171  L_1 = AspectRatioFitter_GetParentSize_m1491(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___axis;
		float L_3 = Vector2_get_Item_m2040((&V_0), L_2, /*hidden argument*/NULL);
		RectTransform_t212 * L_4 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector2_t171  L_5 = RectTransform_get_anchorMax_m1930(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = ___axis;
		float L_7 = Vector2_get_Item_m2040((&V_1), L_6, /*hidden argument*/NULL);
		RectTransform_t212 * L_8 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector2_t171  L_9 = RectTransform_get_anchorMin_m1929(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		int32_t L_10 = ___axis;
		float L_11 = Vector2_get_Item_m2040((&V_2), L_10, /*hidden argument*/NULL);
		return ((float)((float)L_0-(float)((float)((float)L_3*(float)((float)((float)L_7-(float)L_11))))));
	}
}
// UnityEngine.Vector2 UnityEngine.UI.AspectRatioFitter::GetParentSize()
extern TypeInfo* RectTransform_t212_il2cpp_TypeInfo_var;
extern "C" Vector2_t171  AspectRatioFitter_GetParentSize_m1491 (AspectRatioFitter_t320 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t212_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(152);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t212 * V_0 = {0};
	Rect_t94  V_1 = {0};
	{
		RectTransform_t212 * L_0 = AspectRatioFitter_get_rectTransform_m1485(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t84 * L_1 = Transform_get_parent_m148(L_0, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t212 *)IsInstSealed(L_1, RectTransform_t212_il2cpp_TypeInfo_var));
		RectTransform_t212 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m182(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0022;
		}
	}
	{
		Vector2_t171  L_4 = Vector2_get_zero_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_4;
	}

IL_0022:
	{
		RectTransform_t212 * L_5 = V_0;
		NullCheck(L_5);
		Rect_t94  L_6 = RectTransform_get_rect_m1916(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector2_t171  L_7 = Rect_get_size_m1921((&V_1), /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutHorizontal()
extern "C" void AspectRatioFitter_SetLayoutHorizontal_m1492 (AspectRatioFitter_t320 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutVertical()
extern "C" void AspectRatioFitter_SetLayoutVertical_m1493 (AspectRatioFitter_t320 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::SetDirty()
extern "C" void AspectRatioFitter_SetDirty_m1494 (AspectRatioFitter_t320 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		AspectRatioFitter_UpdateRect_m1489(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::.ctor()
extern "C" void CanvasScaler__ctor_m1495 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		__this->___m_ReferencePixelsPerUnit_4 = (100.0f);
		__this->___m_ScaleFactor_5 = (1.0f);
		Vector2_t171  L_0 = {0};
		Vector2__ctor_m1764(&L_0, (800.0f), (600.0f), /*hidden argument*/NULL);
		__this->___m_ReferenceResolution_6 = L_0;
		__this->___m_PhysicalUnit_9 = 3;
		__this->___m_FallbackScreenDPI_10 = (96.0f);
		__this->___m_DefaultSpriteDPI_11 = (96.0f);
		__this->___m_DynamicPixelsPerUnit_12 = (1.0f);
		__this->___m_PrevScaleFactor_14 = (1.0f);
		__this->___m_PrevReferencePixelsPerUnit_15 = (100.0f);
		UIBehaviour__ctor_m407(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.CanvasScaler/ScaleMode UnityEngine.UI.CanvasScaler::get_uiScaleMode()
extern "C" int32_t CanvasScaler_get_uiScaleMode_m1496 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_UiScaleMode_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_uiScaleMode(UnityEngine.UI.CanvasScaler/ScaleMode)
extern "C" void CanvasScaler_set_uiScaleMode_m1497 (CanvasScaler_t324 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_UiScaleMode_3 = L_0;
		return;
	}
}
// System.Single UnityEngine.UI.CanvasScaler::get_referencePixelsPerUnit()
extern "C" float CanvasScaler_get_referencePixelsPerUnit_m1498 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_ReferencePixelsPerUnit_4);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_referencePixelsPerUnit(System.Single)
extern "C" void CanvasScaler_set_referencePixelsPerUnit_m1499 (CanvasScaler_t324 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_ReferencePixelsPerUnit_4 = L_0;
		return;
	}
}
// System.Single UnityEngine.UI.CanvasScaler::get_scaleFactor()
extern "C" float CanvasScaler_get_scaleFactor_m1500 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_ScaleFactor_5);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_scaleFactor(System.Single)
extern "C" void CanvasScaler_set_scaleFactor_m1501 (CanvasScaler_t324 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_ScaleFactor_5 = L_0;
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::get_referenceResolution()
extern "C" Vector2_t171  CanvasScaler_get_referenceResolution_m1502 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		Vector2_t171  L_0 = (__this->___m_ReferenceResolution_6);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_referenceResolution(UnityEngine.Vector2)
extern "C" void CanvasScaler_set_referenceResolution_m1503 (CanvasScaler_t324 * __this, Vector2_t171  ___value, const MethodInfo* method)
{
	{
		Vector2_t171  L_0 = ___value;
		__this->___m_ReferenceResolution_6 = L_0;
		return;
	}
}
// UnityEngine.UI.CanvasScaler/ScreenMatchMode UnityEngine.UI.CanvasScaler::get_screenMatchMode()
extern "C" int32_t CanvasScaler_get_screenMatchMode_m1504 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_ScreenMatchMode_7);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_screenMatchMode(UnityEngine.UI.CanvasScaler/ScreenMatchMode)
extern "C" void CanvasScaler_set_screenMatchMode_m1505 (CanvasScaler_t324 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_ScreenMatchMode_7 = L_0;
		return;
	}
}
// System.Single UnityEngine.UI.CanvasScaler::get_matchWidthOrHeight()
extern "C" float CanvasScaler_get_matchWidthOrHeight_m1506 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_MatchWidthOrHeight_8);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_matchWidthOrHeight(System.Single)
extern "C" void CanvasScaler_set_matchWidthOrHeight_m1507 (CanvasScaler_t324 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_MatchWidthOrHeight_8 = L_0;
		return;
	}
}
// UnityEngine.UI.CanvasScaler/Unit UnityEngine.UI.CanvasScaler::get_physicalUnit()
extern "C" int32_t CanvasScaler_get_physicalUnit_m1508 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_PhysicalUnit_9);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_physicalUnit(UnityEngine.UI.CanvasScaler/Unit)
extern "C" void CanvasScaler_set_physicalUnit_m1509 (CanvasScaler_t324 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_PhysicalUnit_9 = L_0;
		return;
	}
}
// System.Single UnityEngine.UI.CanvasScaler::get_fallbackScreenDPI()
extern "C" float CanvasScaler_get_fallbackScreenDPI_m1510 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FallbackScreenDPI_10);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_fallbackScreenDPI(System.Single)
extern "C" void CanvasScaler_set_fallbackScreenDPI_m1511 (CanvasScaler_t324 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_FallbackScreenDPI_10 = L_0;
		return;
	}
}
// System.Single UnityEngine.UI.CanvasScaler::get_defaultSpriteDPI()
extern "C" float CanvasScaler_get_defaultSpriteDPI_m1512 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_DefaultSpriteDPI_11);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_defaultSpriteDPI(System.Single)
extern "C" void CanvasScaler_set_defaultSpriteDPI_m1513 (CanvasScaler_t324 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_DefaultSpriteDPI_11 = L_0;
		return;
	}
}
// System.Single UnityEngine.UI.CanvasScaler::get_dynamicPixelsPerUnit()
extern "C" float CanvasScaler_get_dynamicPixelsPerUnit_m1514 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_DynamicPixelsPerUnit_12);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_dynamicPixelsPerUnit(System.Single)
extern "C" void CanvasScaler_set_dynamicPixelsPerUnit_m1515 (CanvasScaler_t324 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_DynamicPixelsPerUnit_12 = L_0;
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::OnEnable()
extern const MethodInfo* Component_GetComponent_TisCanvas_t230_m1997_MethodInfo_var;
extern "C" void CanvasScaler_OnEnable_m1516 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCanvas_t230_m1997_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483848);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIBehaviour_OnEnable_m409(__this, /*hidden argument*/NULL);
		Canvas_t230 * L_0 = Component_GetComponent_TisCanvas_t230_m1997(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t230_m1997_MethodInfo_var);
		__this->___m_Canvas_13 = L_0;
		VirtActionInvoker0::Invoke(17 /* System.Void UnityEngine.UI.CanvasScaler::Handle() */, __this);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::OnDisable()
extern "C" void CanvasScaler_OnDisable_m1517 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		CanvasScaler_SetScaleFactor_m1524(__this, (1.0f), /*hidden argument*/NULL);
		CanvasScaler_SetReferencePixelsPerUnit_m1525(__this, (100.0f), /*hidden argument*/NULL);
		UIBehaviour_OnDisable_m411(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::Update()
extern "C" void CanvasScaler_Update_m1518 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(17 /* System.Void UnityEngine.UI.CanvasScaler::Handle() */, __this);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::Handle()
extern "C" void CanvasScaler_Handle_m1519 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		Canvas_t230 * L_0 = (__this->___m_Canvas_13);
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		Canvas_t230 * L_2 = (__this->___m_Canvas_13);
		NullCheck(L_2);
		bool L_3 = Canvas_get_isRootCanvas_m2256(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0022;
		}
	}

IL_0021:
	{
		return;
	}

IL_0022:
	{
		Canvas_t230 * L_4 = (__this->___m_Canvas_13);
		NullCheck(L_4);
		int32_t L_5 = Canvas_get_renderMode_m1995(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_003a;
		}
	}
	{
		VirtActionInvoker0::Invoke(18 /* System.Void UnityEngine.UI.CanvasScaler::HandleWorldCanvas() */, __this);
		return;
	}

IL_003a:
	{
		int32_t L_6 = (__this->___m_UiScaleMode_3);
		V_0 = L_6;
		int32_t L_7 = V_0;
		if (L_7 == 0)
		{
			goto IL_0058;
		}
		if (L_7 == 1)
		{
			goto IL_0063;
		}
		if (L_7 == 2)
		{
			goto IL_006e;
		}
	}
	{
		goto IL_0079;
	}

IL_0058:
	{
		VirtActionInvoker0::Invoke(19 /* System.Void UnityEngine.UI.CanvasScaler::HandleConstantPixelSize() */, __this);
		goto IL_0079;
	}

IL_0063:
	{
		VirtActionInvoker0::Invoke(20 /* System.Void UnityEngine.UI.CanvasScaler::HandleScaleWithScreenSize() */, __this);
		goto IL_0079;
	}

IL_006e:
	{
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.CanvasScaler::HandleConstantPhysicalSize() */, __this);
		goto IL_0079;
	}

IL_0079:
	{
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::HandleWorldCanvas()
extern "C" void CanvasScaler_HandleWorldCanvas_m1520 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_DynamicPixelsPerUnit_12);
		CanvasScaler_SetScaleFactor_m1524(__this, L_0, /*hidden argument*/NULL);
		float L_1 = (__this->___m_ReferencePixelsPerUnit_4);
		CanvasScaler_SetReferencePixelsPerUnit_m1525(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPixelSize()
extern "C" void CanvasScaler_HandleConstantPixelSize_m1521 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_ScaleFactor_5);
		CanvasScaler_SetScaleFactor_m1524(__this, L_0, /*hidden argument*/NULL);
		float L_1 = (__this->___m_ReferencePixelsPerUnit_4);
		CanvasScaler_SetReferencePixelsPerUnit_m1525(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::HandleScaleWithScreenSize()
extern TypeInfo* Mathf_t394_il2cpp_TypeInfo_var;
extern "C" void CanvasScaler_HandleScaleWithScreenSize_m1522 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t171  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = {0};
	{
		int32_t L_0 = Screen_get_width_m185(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m186(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2__ctor_m1764((&V_0), (((float)((float)L_0))), (((float)((float)L_1))), /*hidden argument*/NULL);
		V_1 = (0.0f);
		int32_t L_2 = (__this->___m_ScreenMatchMode_7);
		V_5 = L_2;
		int32_t L_3 = V_5;
		if (L_3 == 0)
		{
			goto IL_0039;
		}
		if (L_3 == 1)
		{
			goto IL_0096;
		}
		if (L_3 == 2)
		{
			goto IL_00c7;
		}
	}
	{
		goto IL_00f8;
	}

IL_0039:
	{
		float L_4 = ((&V_0)->___x_1);
		Vector2_t171 * L_5 = &(__this->___m_ReferenceResolution_6);
		float L_6 = (L_5->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Log_m2257(NULL /*static, unused*/, ((float)((float)L_4/(float)L_6)), (2.0f), /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = ((&V_0)->___y_2);
		Vector2_t171 * L_9 = &(__this->___m_ReferenceResolution_6);
		float L_10 = (L_9->___y_2);
		float L_11 = Mathf_Log_m2257(NULL /*static, unused*/, ((float)((float)L_8/(float)L_10)), (2.0f), /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = V_2;
		float L_13 = V_3;
		float L_14 = (__this->___m_MatchWidthOrHeight_8);
		float L_15 = Mathf_Lerp_m1843(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = V_4;
		float L_17 = powf((2.0f), L_16);
		V_1 = L_17;
		goto IL_00f8;
	}

IL_0096:
	{
		float L_18 = ((&V_0)->___x_1);
		Vector2_t171 * L_19 = &(__this->___m_ReferenceResolution_6);
		float L_20 = (L_19->___x_1);
		float L_21 = ((&V_0)->___y_2);
		Vector2_t171 * L_22 = &(__this->___m_ReferenceResolution_6);
		float L_23 = (L_22->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_24 = Mathf_Min_m2252(NULL /*static, unused*/, ((float)((float)L_18/(float)L_20)), ((float)((float)L_21/(float)L_23)), /*hidden argument*/NULL);
		V_1 = L_24;
		goto IL_00f8;
	}

IL_00c7:
	{
		float L_25 = ((&V_0)->___x_1);
		Vector2_t171 * L_26 = &(__this->___m_ReferenceResolution_6);
		float L_27 = (L_26->___x_1);
		float L_28 = ((&V_0)->___y_2);
		Vector2_t171 * L_29 = &(__this->___m_ReferenceResolution_6);
		float L_30 = (L_29->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_31 = Mathf_Max_m2208(NULL /*static, unused*/, ((float)((float)L_25/(float)L_27)), ((float)((float)L_28/(float)L_30)), /*hidden argument*/NULL);
		V_1 = L_31;
		goto IL_00f8;
	}

IL_00f8:
	{
		float L_32 = V_1;
		CanvasScaler_SetScaleFactor_m1524(__this, L_32, /*hidden argument*/NULL);
		float L_33 = (__this->___m_ReferencePixelsPerUnit_4);
		CanvasScaler_SetReferencePixelsPerUnit_m1525(__this, L_33, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPhysicalSize()
extern "C" void CanvasScaler_HandleConstantPhysicalSize_m1523 (CanvasScaler_t324 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = {0};
	float G_B3_0 = 0.0f;
	{
		float L_0 = Screen_get_dpi_m2258(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_001c;
		}
	}
	{
		float L_2 = (__this->___m_FallbackScreenDPI_10);
		G_B3_0 = L_2;
		goto IL_001d;
	}

IL_001c:
	{
		float L_3 = V_0;
		G_B3_0 = L_3;
	}

IL_001d:
	{
		V_1 = G_B3_0;
		V_2 = (1.0f);
		int32_t L_4 = (__this->___m_PhysicalUnit_9);
		V_3 = L_4;
		int32_t L_5 = V_3;
		if (L_5 == 0)
		{
			goto IL_004a;
		}
		if (L_5 == 1)
		{
			goto IL_0055;
		}
		if (L_5 == 2)
		{
			goto IL_0060;
		}
		if (L_5 == 3)
		{
			goto IL_006b;
		}
		if (L_5 == 4)
		{
			goto IL_0076;
		}
	}
	{
		goto IL_0081;
	}

IL_004a:
	{
		V_2 = (2.54f);
		goto IL_0081;
	}

IL_0055:
	{
		V_2 = (25.4f);
		goto IL_0081;
	}

IL_0060:
	{
		V_2 = (1.0f);
		goto IL_0081;
	}

IL_006b:
	{
		V_2 = (72.0f);
		goto IL_0081;
	}

IL_0076:
	{
		V_2 = (6.0f);
		goto IL_0081;
	}

IL_0081:
	{
		float L_6 = V_1;
		float L_7 = V_2;
		CanvasScaler_SetScaleFactor_m1524(__this, ((float)((float)L_6/(float)L_7)), /*hidden argument*/NULL);
		float L_8 = (__this->___m_ReferencePixelsPerUnit_4);
		float L_9 = V_2;
		float L_10 = (__this->___m_DefaultSpriteDPI_11);
		CanvasScaler_SetReferencePixelsPerUnit_m1525(__this, ((float)((float)((float)((float)L_8*(float)L_9))/(float)L_10)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::SetScaleFactor(System.Single)
extern "C" void CanvasScaler_SetScaleFactor_m1524 (CanvasScaler_t324 * __this, float ___scaleFactor, const MethodInfo* method)
{
	{
		float L_0 = ___scaleFactor;
		float L_1 = (__this->___m_PrevScaleFactor_14);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		Canvas_t230 * L_2 = (__this->___m_Canvas_13);
		float L_3 = ___scaleFactor;
		NullCheck(L_2);
		Canvas_set_scaleFactor_m2259(L_2, L_3, /*hidden argument*/NULL);
		float L_4 = ___scaleFactor;
		__this->___m_PrevScaleFactor_14 = L_4;
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::SetReferencePixelsPerUnit(System.Single)
extern "C" void CanvasScaler_SetReferencePixelsPerUnit_m1525 (CanvasScaler_t324 * __this, float ___referencePixelsPerUnit, const MethodInfo* method)
{
	{
		float L_0 = ___referencePixelsPerUnit;
		float L_1 = (__this->___m_PrevReferencePixelsPerUnit_15);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		Canvas_t230 * L_2 = (__this->___m_Canvas_13);
		float L_3 = ___referencePixelsPerUnit;
		NullCheck(L_2);
		Canvas_set_referencePixelsPerUnit_m2260(L_2, L_3, /*hidden argument*/NULL);
		float L_4 = ___referencePixelsPerUnit;
		__this->___m_PrevReferencePixelsPerUnit_15 = L_4;
		return;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::.ctor()
extern "C" void ContentSizeFitter__ctor_m1526 (ContentSizeFitter_t326 * __this, const MethodInfo* method)
{
	{
		UIBehaviour__ctor_m407(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_horizontalFit()
extern "C" int32_t ContentSizeFitter_get_horizontalFit_m1527 (ContentSizeFitter_t326 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_HorizontalFit_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::set_horizontalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisFitMode_t325_m2261_MethodInfo_var;
extern "C" void ContentSizeFitter_set_horizontalFit_m1528 (ContentSizeFitter_t326 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisFitMode_t325_m2261_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483914);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_HorizontalFit_2);
		int32_t L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisFitMode_t325_m2261(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisFitMode_t325_m2261_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		ContentSizeFitter_SetDirty_m1538(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_verticalFit()
extern "C" int32_t ContentSizeFitter_get_verticalFit_m1529 (ContentSizeFitter_t326 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_VerticalFit_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::set_verticalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisFitMode_t325_m2261_MethodInfo_var;
extern "C" void ContentSizeFitter_set_verticalFit_m1530 (ContentSizeFitter_t326 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisFitMode_t325_m2261_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483914);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_VerticalFit_3);
		int32_t L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisFitMode_t325_m2261(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisFitMode_t325_m2261_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		ContentSizeFitter_SetDirty_m1538(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::get_rectTransform()
extern const MethodInfo* Component_GetComponent_TisRectTransform_t212_m1961_MethodInfo_var;
extern "C" RectTransform_t212 * ContentSizeFitter_get_rectTransform_m1531 (ContentSizeFitter_t326 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRectTransform_t212_m1961_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483839);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t212 * L_0 = (__this->___m_Rect_4);
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RectTransform_t212 * L_2 = Component_GetComponent_TisRectTransform_t212_m1961(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t212_m1961_MethodInfo_var);
		__this->___m_Rect_4 = L_2;
	}

IL_001d:
	{
		RectTransform_t212 * L_3 = (__this->___m_Rect_4);
		return L_3;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::OnEnable()
extern "C" void ContentSizeFitter_OnEnable_m1532 (ContentSizeFitter_t326 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m409(__this, /*hidden argument*/NULL);
		ContentSizeFitter_SetDirty_m1538(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::OnDisable()
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern "C" void ContentSizeFitter_OnDisable_m1533 (ContentSizeFitter_t326 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		s_Il2CppMethodIntialized = true;
	}
	{
		DrivenRectTransformTracker_t291 * L_0 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m2166(L_0, /*hidden argument*/NULL);
		RectTransform_t212 * L_1 = ContentSizeFitter_get_rectTransform_m1531(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m1635(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		UIBehaviour_OnDisable_m411(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::OnRectTransformDimensionsChange()
extern "C" void ContentSizeFitter_OnRectTransformDimensionsChange_m1534 (ContentSizeFitter_t326 * __this, const MethodInfo* method)
{
	{
		ContentSizeFitter_SetDirty_m1538(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::HandleSelfFittingAlongAxis(System.Int32)
extern "C" void ContentSizeFitter_HandleSelfFittingAlongAxis_m1535 (ContentSizeFitter_t326 * __this, int32_t ___axis, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B3_0 = {0};
	RectTransform_t212 * G_B7_0 = {0};
	ContentSizeFitter_t326 * G_B7_1 = {0};
	DrivenRectTransformTracker_t291 * G_B7_2 = {0};
	RectTransform_t212 * G_B6_0 = {0};
	ContentSizeFitter_t326 * G_B6_1 = {0};
	DrivenRectTransformTracker_t291 * G_B6_2 = {0};
	int32_t G_B8_0 = 0;
	RectTransform_t212 * G_B8_1 = {0};
	ContentSizeFitter_t326 * G_B8_2 = {0};
	DrivenRectTransformTracker_t291 * G_B8_3 = {0};
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = ContentSizeFitter_get_horizontalFit_m1527(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0011:
	{
		int32_t L_2 = ContentSizeFitter_get_verticalFit_m1529(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		int32_t L_3 = V_0;
		if (L_3)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		DrivenRectTransformTracker_t291 * L_4 = &(__this->___m_Tracker_5);
		RectTransform_t212 * L_5 = ContentSizeFitter_get_rectTransform_m1531(__this, /*hidden argument*/NULL);
		int32_t L_6 = ___axis;
		G_B6_0 = L_5;
		G_B6_1 = __this;
		G_B6_2 = L_4;
		if (L_6)
		{
			G_B7_0 = L_5;
			G_B7_1 = __this;
			G_B7_2 = L_4;
			goto IL_003c;
		}
	}
	{
		G_B8_0 = ((int32_t)4096);
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		goto IL_0041;
	}

IL_003c:
	{
		G_B8_0 = ((int32_t)8192);
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
	}

IL_0041:
	{
		DrivenRectTransformTracker_Add_m2167(G_B8_3, G_B8_2, G_B8_1, G_B8_0, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_006a;
		}
	}
	{
		RectTransform_t212 * L_8 = ContentSizeFitter_get_rectTransform_m1531(__this, /*hidden argument*/NULL);
		int32_t L_9 = ___axis;
		RectTransform_t212 * L_10 = (__this->___m_Rect_4);
		int32_t L_11 = ___axis;
		float L_12 = LayoutUtility_GetMinSize_m1650(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		RectTransform_SetSizeWithCurrentAnchors_m2255(L_8, L_9, L_12, /*hidden argument*/NULL);
		goto IL_0082;
	}

IL_006a:
	{
		RectTransform_t212 * L_13 = ContentSizeFitter_get_rectTransform_m1531(__this, /*hidden argument*/NULL);
		int32_t L_14 = ___axis;
		RectTransform_t212 * L_15 = (__this->___m_Rect_4);
		int32_t L_16 = ___axis;
		float L_17 = LayoutUtility_GetPreferredSize_m1651(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_SetSizeWithCurrentAnchors_m2255(L_13, L_14, L_17, /*hidden argument*/NULL);
	}

IL_0082:
	{
		return;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutHorizontal()
extern "C" void ContentSizeFitter_SetLayoutHorizontal_m1536 (ContentSizeFitter_t326 * __this, const MethodInfo* method)
{
	{
		DrivenRectTransformTracker_t291 * L_0 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m2166(L_0, /*hidden argument*/NULL);
		ContentSizeFitter_HandleSelfFittingAlongAxis_m1535(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutVertical()
extern "C" void ContentSizeFitter_SetLayoutVertical_m1537 (ContentSizeFitter_t326 * __this, const MethodInfo* method)
{
	{
		ContentSizeFitter_HandleSelfFittingAlongAxis_m1535(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::SetDirty()
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern "C" void ContentSizeFitter_SetDirty_m1538 (ContentSizeFitter_t326 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		RectTransform_t212 * L_1 = ContentSizeFitter_get_rectTransform_m1531(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m1635(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::.ctor()
extern "C" void GridLayoutGroup__ctor_m1539 (GridLayoutGroup_t330 * __this, const MethodInfo* method)
{
	{
		Vector2_t171  L_0 = {0};
		Vector2__ctor_m1764(&L_0, (100.0f), (100.0f), /*hidden argument*/NULL);
		__this->___m_CellSize_12 = L_0;
		Vector2_t171  L_1 = Vector2_get_zero_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Spacing_13 = L_1;
		__this->___m_ConstraintCount_15 = 2;
		LayoutGroup__ctor_m1595(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::get_startCorner()
extern "C" int32_t GridLayoutGroup_get_startCorner_m1540 (GridLayoutGroup_t330 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_StartCorner_10);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_startCorner(UnityEngine.UI.GridLayoutGroup/Corner)
extern const MethodInfo* LayoutGroup_SetProperty_TisCorner_t327_m2262_MethodInfo_var;
extern "C" void GridLayoutGroup_set_startCorner_m1541 (GridLayoutGroup_t330 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisCorner_t327_m2262_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483915);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_StartCorner_10);
		int32_t L_1 = ___value;
		LayoutGroup_SetProperty_TisCorner_t327_m2262(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisCorner_t327_m2262_MethodInfo_var);
		return;
	}
}
// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::get_startAxis()
extern "C" int32_t GridLayoutGroup_get_startAxis_m1542 (GridLayoutGroup_t330 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_StartAxis_11);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_startAxis(UnityEngine.UI.GridLayoutGroup/Axis)
extern const MethodInfo* LayoutGroup_SetProperty_TisAxis_t328_m2263_MethodInfo_var;
extern "C" void GridLayoutGroup_set_startAxis_m1543 (GridLayoutGroup_t330 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisAxis_t328_m2263_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483916);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_StartAxis_11);
		int32_t L_1 = ___value;
		LayoutGroup_SetProperty_TisAxis_t328_m2263(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisAxis_t328_m2263_MethodInfo_var);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_cellSize()
extern "C" Vector2_t171  GridLayoutGroup_get_cellSize_m1544 (GridLayoutGroup_t330 * __this, const MethodInfo* method)
{
	{
		Vector2_t171  L_0 = (__this->___m_CellSize_12);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_cellSize(UnityEngine.Vector2)
extern const MethodInfo* LayoutGroup_SetProperty_TisVector2_t171_m2264_MethodInfo_var;
extern "C" void GridLayoutGroup_set_cellSize_m1545 (GridLayoutGroup_t330 * __this, Vector2_t171  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisVector2_t171_m2264_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483917);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t171 * L_0 = &(__this->___m_CellSize_12);
		Vector2_t171  L_1 = ___value;
		LayoutGroup_SetProperty_TisVector2_t171_m2264(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisVector2_t171_m2264_MethodInfo_var);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_spacing()
extern "C" Vector2_t171  GridLayoutGroup_get_spacing_m1546 (GridLayoutGroup_t330 * __this, const MethodInfo* method)
{
	{
		Vector2_t171  L_0 = (__this->___m_Spacing_13);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_spacing(UnityEngine.Vector2)
extern const MethodInfo* LayoutGroup_SetProperty_TisVector2_t171_m2264_MethodInfo_var;
extern "C" void GridLayoutGroup_set_spacing_m1547 (GridLayoutGroup_t330 * __this, Vector2_t171  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisVector2_t171_m2264_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483917);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t171 * L_0 = &(__this->___m_Spacing_13);
		Vector2_t171  L_1 = ___value;
		LayoutGroup_SetProperty_TisVector2_t171_m2264(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisVector2_t171_m2264_MethodInfo_var);
		return;
	}
}
// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::get_constraint()
extern "C" int32_t GridLayoutGroup_get_constraint_m1548 (GridLayoutGroup_t330 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Constraint_14);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraint(UnityEngine.UI.GridLayoutGroup/Constraint)
extern const MethodInfo* LayoutGroup_SetProperty_TisConstraint_t329_m2265_MethodInfo_var;
extern "C" void GridLayoutGroup_set_constraint_m1549 (GridLayoutGroup_t330 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisConstraint_t329_m2265_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483918);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_Constraint_14);
		int32_t L_1 = ___value;
		LayoutGroup_SetProperty_TisConstraint_t329_m2265(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisConstraint_t329_m2265_MethodInfo_var);
		return;
	}
}
// System.Int32 UnityEngine.UI.GridLayoutGroup::get_constraintCount()
extern "C" int32_t GridLayoutGroup_get_constraintCount_m1550 (GridLayoutGroup_t330 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_ConstraintCount_15);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraintCount(System.Int32)
extern TypeInfo* Mathf_t394_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutGroup_SetProperty_TisInt32_t392_m2266_MethodInfo_var;
extern "C" void GridLayoutGroup_set_constraintCount_m1551 (GridLayoutGroup_t330 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		LayoutGroup_SetProperty_TisInt32_t392_m2266_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483919);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_ConstraintCount_15);
		int32_t L_1 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2100(NULL /*static, unused*/, 1, L_1, /*hidden argument*/NULL);
		LayoutGroup_SetProperty_TisInt32_t392_m2266(__this, L_0, L_2, /*hidden argument*/LayoutGroup_SetProperty_TisInt32_t392_m2266_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputHorizontal()
extern TypeInfo* Mathf_t394_il2cpp_TypeInfo_var;
extern "C" void GridLayoutGroup_CalculateLayoutInputHorizontal_m1552 (GridLayoutGroup_t330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector2_t171  V_2 = {0};
	Vector2_t171  V_3 = {0};
	Vector2_t171  V_4 = {0};
	Vector2_t171  V_5 = {0};
	Vector2_t171  V_6 = {0};
	Vector2_t171  V_7 = {0};
	{
		LayoutGroup_CalculateLayoutInputHorizontal_m1602(__this, /*hidden argument*/NULL);
		V_0 = 0;
		V_1 = 0;
		int32_t L_0 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_1 = (__this->___m_ConstraintCount_15);
		int32_t L_2 = L_1;
		V_1 = L_2;
		V_0 = L_2;
		goto IL_0070;
	}

IL_0024:
	{
		int32_t L_3 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0057;
		}
	}
	{
		List_1_t336 * L_4 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_4);
		int32_t L_6 = (__this->___m_ConstraintCount_15);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_CeilToInt_m2267(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)((float)L_5)))/(float)(((float)((float)L_6)))))-(float)(0.001f))), /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		V_1 = L_8;
		V_0 = L_8;
		goto IL_0070;
	}

IL_0057:
	{
		V_0 = 1;
		List_1_t336 * L_9 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_11 = sqrtf((((float)((float)L_10))));
		int32_t L_12 = Mathf_CeilToInt_m2267(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
	}

IL_0070:
	{
		RectOffset_t335 * L_13 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_horizontal_m2268(L_13, /*hidden argument*/NULL);
		Vector2_t171  L_15 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_2 = L_15;
		float L_16 = ((&V_2)->___x_1);
		Vector2_t171  L_17 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_3 = L_17;
		float L_18 = ((&V_3)->___x_1);
		int32_t L_19 = V_0;
		Vector2_t171  L_20 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_4 = L_20;
		float L_21 = ((&V_4)->___x_1);
		RectOffset_t335 * L_22 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = RectOffset_get_horizontal_m2268(L_22, /*hidden argument*/NULL);
		Vector2_t171  L_24 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_5 = L_24;
		float L_25 = ((&V_5)->___x_1);
		Vector2_t171  L_26 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_6 = L_26;
		float L_27 = ((&V_6)->___x_1);
		int32_t L_28 = V_1;
		Vector2_t171  L_29 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_7 = L_29;
		float L_30 = ((&V_7)->___x_1);
		LayoutGroup_SetLayoutInputForAxis_m1617(__this, ((float)((float)((float)((float)(((float)((float)L_14)))+(float)((float)((float)((float)((float)L_16+(float)L_18))*(float)(((float)((float)L_19)))))))-(float)L_21)), ((float)((float)((float)((float)(((float)((float)L_23)))+(float)((float)((float)((float)((float)L_25+(float)L_27))*(float)(((float)((float)L_28)))))))-(float)L_30)), (-1.0f), 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputVertical()
extern TypeInfo* Mathf_t394_il2cpp_TypeInfo_var;
extern "C" void GridLayoutGroup_CalculateLayoutInputVertical_m1553 (GridLayoutGroup_t330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	Rect_t94  V_4 = {0};
	Vector2_t171  V_5 = {0};
	Vector2_t171  V_6 = {0};
	Vector2_t171  V_7 = {0};
	Vector2_t171  V_8 = {0};
	Vector2_t171  V_9 = {0};
	Vector2_t171  V_10 = {0};
	Vector2_t171  V_11 = {0};
	{
		V_0 = 0;
		int32_t L_0 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0033;
		}
	}
	{
		List_1_t336 * L_1 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_1);
		int32_t L_3 = (__this->___m_ConstraintCount_15);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_CeilToInt_m2267(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)((float)L_2)))/(float)(((float)((float)L_3)))))-(float)(0.001f))), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_00ce;
	}

IL_0033:
	{
		int32_t L_5 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_6 = (__this->___m_ConstraintCount_15);
		V_0 = L_6;
		goto IL_00ce;
	}

IL_004b:
	{
		RectTransform_t212 * L_7 = LayoutGroup_get_rectTransform_m1600(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Rect_t94  L_8 = RectTransform_get_rect_m1916(L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		Vector2_t171  L_9 = Rect_get_size_m1921((&V_4), /*hidden argument*/NULL);
		V_5 = L_9;
		float L_10 = ((&V_5)->___x_1);
		V_1 = L_10;
		float L_11 = V_1;
		RectOffset_t335 * L_12 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_horizontal_m2268(L_12, /*hidden argument*/NULL);
		Vector2_t171  L_14 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_6 = L_14;
		float L_15 = ((&V_6)->___x_1);
		Vector2_t171  L_16 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_7 = L_16;
		float L_17 = ((&V_7)->___x_1);
		Vector2_t171  L_18 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_8 = L_18;
		float L_19 = ((&V_8)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		int32_t L_20 = Mathf_FloorToInt_m2269(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)((float)((float)L_11-(float)(((float)((float)L_13)))))+(float)L_15))+(float)(0.001f)))/(float)((float)((float)L_17+(float)L_19)))), /*hidden argument*/NULL);
		int32_t L_21 = Mathf_Max_m2100(NULL /*static, unused*/, 1, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		List_1_t336 * L_22 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_22);
		int32_t L_24 = V_2;
		int32_t L_25 = Mathf_CeilToInt_m2267(NULL /*static, unused*/, ((float)((float)(((float)((float)L_23)))/(float)(((float)((float)L_24))))), /*hidden argument*/NULL);
		V_0 = L_25;
	}

IL_00ce:
	{
		RectOffset_t335 * L_26 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_27 = RectOffset_get_vertical_m2270(L_26, /*hidden argument*/NULL);
		Vector2_t171  L_28 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_9 = L_28;
		float L_29 = ((&V_9)->___y_2);
		Vector2_t171  L_30 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_10 = L_30;
		float L_31 = ((&V_10)->___y_2);
		int32_t L_32 = V_0;
		Vector2_t171  L_33 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_11 = L_33;
		float L_34 = ((&V_11)->___y_2);
		V_3 = ((float)((float)((float)((float)(((float)((float)L_27)))+(float)((float)((float)((float)((float)L_29+(float)L_31))*(float)(((float)((float)L_32)))))))-(float)L_34));
		float L_35 = V_3;
		float L_36 = V_3;
		LayoutGroup_SetLayoutInputForAxis_m1617(__this, L_35, L_36, (-1.0f), 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutHorizontal()
extern "C" void GridLayoutGroup_SetLayoutHorizontal_m1554 (GridLayoutGroup_t330 * __this, const MethodInfo* method)
{
	{
		GridLayoutGroup_SetCellsAlongAxis_m1556(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutVertical()
extern "C" void GridLayoutGroup_SetLayoutVertical_m1555 (GridLayoutGroup_t330 * __this, const MethodInfo* method)
{
	{
		GridLayoutGroup_SetCellsAlongAxis_m1556(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::SetCellsAlongAxis(System.Int32)
extern TypeInfo* Mathf_t394_il2cpp_TypeInfo_var;
extern "C" void GridLayoutGroup_SetCellsAlongAxis_m1556 (GridLayoutGroup_t330 * __this, int32_t ___axis, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t212 * V_1 = {0};
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	Vector2_t171  V_11 = {0};
	Vector2_t171  V_12 = {0};
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	Rect_t94  V_16 = {0};
	Vector2_t171  V_17 = {0};
	Rect_t94  V_18 = {0};
	Vector2_t171  V_19 = {0};
	Vector2_t171  V_20 = {0};
	Vector2_t171  V_21 = {0};
	Vector2_t171  V_22 = {0};
	Vector2_t171  V_23 = {0};
	Vector2_t171  V_24 = {0};
	Vector2_t171  V_25 = {0};
	Vector2_t171  V_26 = {0};
	Vector2_t171  V_27 = {0};
	Vector2_t171  V_28 = {0};
	Vector2_t171  V_29 = {0};
	Vector2_t171  V_30 = {0};
	Vector2_t171  V_31 = {0};
	Vector2_t171  V_32 = {0};
	Vector2_t171  V_33 = {0};
	Vector2_t171  V_34 = {0};
	Vector2_t171  V_35 = {0};
	Vector2_t171  V_36 = {0};
	Vector2_t171  V_37 = {0};
	Vector2_t171  V_38 = {0};
	Vector2_t171  V_39 = {0};
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_0064;
		}
	}
	{
		V_0 = 0;
		goto IL_0052;
	}

IL_000d:
	{
		List_1_t336 * L_1 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		RectTransform_t212 * L_3 = (RectTransform_t212 *)VirtFuncInvoker1< RectTransform_t212 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_1, L_2);
		V_1 = L_3;
		DrivenRectTransformTracker_t291 * L_4 = &(((LayoutGroup_t331 *)__this)->___m_Tracker_5);
		RectTransform_t212 * L_5 = V_1;
		DrivenRectTransformTracker_Add_m2167(L_4, __this, L_5, ((int32_t)16134), /*hidden argument*/NULL);
		RectTransform_t212 * L_6 = V_1;
		Vector2_t171  L_7 = Vector2_get_up_m1892(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		RectTransform_set_anchorMin_m1870(L_6, L_7, /*hidden argument*/NULL);
		RectTransform_t212 * L_8 = V_1;
		Vector2_t171  L_9 = Vector2_get_up_m1892(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		RectTransform_set_anchorMax_m1872(L_8, L_9, /*hidden argument*/NULL);
		RectTransform_t212 * L_10 = V_1;
		Vector2_t171  L_11 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		RectTransform_set_sizeDelta_m1863(L_10, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0052:
	{
		int32_t L_13 = V_0;
		List_1_t336 * L_14 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_14);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_0064:
	{
		RectTransform_t212 * L_16 = LayoutGroup_get_rectTransform_m1600(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Rect_t94  L_17 = RectTransform_get_rect_m1916(L_16, /*hidden argument*/NULL);
		V_16 = L_17;
		Vector2_t171  L_18 = Rect_get_size_m1921((&V_16), /*hidden argument*/NULL);
		V_17 = L_18;
		float L_19 = ((&V_17)->___x_1);
		V_2 = L_19;
		RectTransform_t212 * L_20 = LayoutGroup_get_rectTransform_m1600(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Rect_t94  L_21 = RectTransform_get_rect_m1916(L_20, /*hidden argument*/NULL);
		V_18 = L_21;
		Vector2_t171  L_22 = Rect_get_size_m1921((&V_18), /*hidden argument*/NULL);
		V_19 = L_22;
		float L_23 = ((&V_19)->___y_2);
		V_3 = L_23;
		V_4 = 1;
		V_5 = 1;
		int32_t L_24 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_24) == ((uint32_t)1))))
		{
			goto IL_00dc;
		}
	}
	{
		int32_t L_25 = (__this->___m_ConstraintCount_15);
		V_4 = L_25;
		List_1_t336 * L_26 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_27 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_26);
		int32_t L_28 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		int32_t L_29 = Mathf_CeilToInt_m2267(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)((float)L_27)))/(float)(((float)((float)L_28)))))-(float)(0.001f))), /*hidden argument*/NULL);
		V_5 = L_29;
		goto IL_021e;
	}

IL_00dc:
	{
		int32_t L_30 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_30) == ((uint32_t)2))))
		{
			goto IL_0112;
		}
	}
	{
		int32_t L_31 = (__this->___m_ConstraintCount_15);
		V_5 = L_31;
		List_1_t336 * L_32 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		int32_t L_33 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_32);
		int32_t L_34 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		int32_t L_35 = Mathf_CeilToInt_m2267(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)((float)L_33)))/(float)(((float)((float)L_34)))))-(float)(0.001f))), /*hidden argument*/NULL);
		V_4 = L_35;
		goto IL_021e;
	}

IL_0112:
	{
		Vector2_t171  L_36 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_20 = L_36;
		float L_37 = ((&V_20)->___x_1);
		Vector2_t171  L_38 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_21 = L_38;
		float L_39 = ((&V_21)->___x_1);
		if ((!(((float)((float)((float)L_37+(float)L_39))) <= ((float)(0.0f)))))
		{
			goto IL_0147;
		}
	}
	{
		V_4 = ((int32_t)2147483647);
		goto IL_0198;
	}

IL_0147:
	{
		float L_40 = V_2;
		RectOffset_t335 * L_41 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		int32_t L_42 = RectOffset_get_horizontal_m2268(L_41, /*hidden argument*/NULL);
		Vector2_t171  L_43 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_22 = L_43;
		float L_44 = ((&V_22)->___x_1);
		Vector2_t171  L_45 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_23 = L_45;
		float L_46 = ((&V_23)->___x_1);
		Vector2_t171  L_47 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_24 = L_47;
		float L_48 = ((&V_24)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		int32_t L_49 = Mathf_FloorToInt_m2269(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)((float)((float)L_40-(float)(((float)((float)L_42)))))+(float)L_44))+(float)(0.001f)))/(float)((float)((float)L_46+(float)L_48)))), /*hidden argument*/NULL);
		int32_t L_50 = Mathf_Max_m2100(NULL /*static, unused*/, 1, L_49, /*hidden argument*/NULL);
		V_4 = L_50;
	}

IL_0198:
	{
		Vector2_t171  L_51 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_25 = L_51;
		float L_52 = ((&V_25)->___y_2);
		Vector2_t171  L_53 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_26 = L_53;
		float L_54 = ((&V_26)->___y_2);
		if ((!(((float)((float)((float)L_52+(float)L_54))) <= ((float)(0.0f)))))
		{
			goto IL_01cd;
		}
	}
	{
		V_5 = ((int32_t)2147483647);
		goto IL_021e;
	}

IL_01cd:
	{
		float L_55 = V_3;
		RectOffset_t335 * L_56 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		int32_t L_57 = RectOffset_get_vertical_m2270(L_56, /*hidden argument*/NULL);
		Vector2_t171  L_58 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_27 = L_58;
		float L_59 = ((&V_27)->___y_2);
		Vector2_t171  L_60 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_28 = L_60;
		float L_61 = ((&V_28)->___y_2);
		Vector2_t171  L_62 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_29 = L_62;
		float L_63 = ((&V_29)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		int32_t L_64 = Mathf_FloorToInt_m2269(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)((float)((float)L_55-(float)(((float)((float)L_57)))))+(float)L_59))+(float)(0.001f)))/(float)((float)((float)L_61+(float)L_63)))), /*hidden argument*/NULL);
		int32_t L_65 = Mathf_Max_m2100(NULL /*static, unused*/, 1, L_64, /*hidden argument*/NULL);
		V_5 = L_65;
	}

IL_021e:
	{
		int32_t L_66 = GridLayoutGroup_get_startCorner_m1540(__this, /*hidden argument*/NULL);
		V_6 = ((int32_t)((int32_t)L_66%(int32_t)2));
		int32_t L_67 = GridLayoutGroup_get_startCorner_m1540(__this, /*hidden argument*/NULL);
		V_7 = ((int32_t)((int32_t)L_67/(int32_t)2));
		int32_t L_68 = GridLayoutGroup_get_startAxis_m1542(__this, /*hidden argument*/NULL);
		if (L_68)
		{
			goto IL_027a;
		}
	}
	{
		int32_t L_69 = V_4;
		V_8 = L_69;
		int32_t L_70 = V_4;
		List_1_t336 * L_71 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_71);
		int32_t L_72 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_71);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		int32_t L_73 = Mathf_Clamp_m1899(NULL /*static, unused*/, L_70, 1, L_72, /*hidden argument*/NULL);
		V_9 = L_73;
		int32_t L_74 = V_5;
		List_1_t336 * L_75 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_75);
		int32_t L_76 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_75);
		int32_t L_77 = V_8;
		int32_t L_78 = Mathf_CeilToInt_m2267(NULL /*static, unused*/, ((float)((float)(((float)((float)L_76)))/(float)(((float)((float)L_77))))), /*hidden argument*/NULL);
		int32_t L_79 = Mathf_Clamp_m1899(NULL /*static, unused*/, L_74, 1, L_78, /*hidden argument*/NULL);
		V_10 = L_79;
		goto IL_02b2;
	}

IL_027a:
	{
		int32_t L_80 = V_5;
		V_8 = L_80;
		int32_t L_81 = V_5;
		List_1_t336 * L_82 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_82);
		int32_t L_83 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_82);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		int32_t L_84 = Mathf_Clamp_m1899(NULL /*static, unused*/, L_81, 1, L_83, /*hidden argument*/NULL);
		V_10 = L_84;
		int32_t L_85 = V_4;
		List_1_t336 * L_86 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_86);
		int32_t L_87 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_86);
		int32_t L_88 = V_8;
		int32_t L_89 = Mathf_CeilToInt_m2267(NULL /*static, unused*/, ((float)((float)(((float)((float)L_87)))/(float)(((float)((float)L_88))))), /*hidden argument*/NULL);
		int32_t L_90 = Mathf_Clamp_m1899(NULL /*static, unused*/, L_85, 1, L_89, /*hidden argument*/NULL);
		V_9 = L_90;
	}

IL_02b2:
	{
		int32_t L_91 = V_9;
		Vector2_t171  L_92 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_30 = L_92;
		float L_93 = ((&V_30)->___x_1);
		int32_t L_94 = V_9;
		Vector2_t171  L_95 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_31 = L_95;
		float L_96 = ((&V_31)->___x_1);
		int32_t L_97 = V_10;
		Vector2_t171  L_98 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_32 = L_98;
		float L_99 = ((&V_32)->___y_2);
		int32_t L_100 = V_10;
		Vector2_t171  L_101 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_33 = L_101;
		float L_102 = ((&V_33)->___y_2);
		Vector2__ctor_m1764((&V_11), ((float)((float)((float)((float)(((float)((float)L_91)))*(float)L_93))+(float)((float)((float)(((float)((float)((int32_t)((int32_t)L_94-(int32_t)1)))))*(float)L_96)))), ((float)((float)((float)((float)(((float)((float)L_97)))*(float)L_99))+(float)((float)((float)(((float)((float)((int32_t)((int32_t)L_100-(int32_t)1)))))*(float)L_102)))), /*hidden argument*/NULL);
		float L_103 = ((&V_11)->___x_1);
		float L_104 = LayoutGroup_GetStartOffset_m1616(__this, 0, L_103, /*hidden argument*/NULL);
		float L_105 = ((&V_11)->___y_2);
		float L_106 = LayoutGroup_GetStartOffset_m1616(__this, 1, L_105, /*hidden argument*/NULL);
		Vector2__ctor_m1764((&V_12), L_104, L_106, /*hidden argument*/NULL);
		V_13 = 0;
		goto IL_042c;
	}

IL_0336:
	{
		int32_t L_107 = GridLayoutGroup_get_startAxis_m1542(__this, /*hidden argument*/NULL);
		if (L_107)
		{
			goto IL_0354;
		}
	}
	{
		int32_t L_108 = V_13;
		int32_t L_109 = V_8;
		V_14 = ((int32_t)((int32_t)L_108%(int32_t)L_109));
		int32_t L_110 = V_13;
		int32_t L_111 = V_8;
		V_15 = ((int32_t)((int32_t)L_110/(int32_t)L_111));
		goto IL_0362;
	}

IL_0354:
	{
		int32_t L_112 = V_13;
		int32_t L_113 = V_8;
		V_14 = ((int32_t)((int32_t)L_112/(int32_t)L_113));
		int32_t L_114 = V_13;
		int32_t L_115 = V_8;
		V_15 = ((int32_t)((int32_t)L_114%(int32_t)L_115));
	}

IL_0362:
	{
		int32_t L_116 = V_6;
		if ((!(((uint32_t)L_116) == ((uint32_t)1))))
		{
			goto IL_0373;
		}
	}
	{
		int32_t L_117 = V_9;
		int32_t L_118 = V_14;
		V_14 = ((int32_t)((int32_t)((int32_t)((int32_t)L_117-(int32_t)1))-(int32_t)L_118));
	}

IL_0373:
	{
		int32_t L_119 = V_7;
		if ((!(((uint32_t)L_119) == ((uint32_t)1))))
		{
			goto IL_0384;
		}
	}
	{
		int32_t L_120 = V_10;
		int32_t L_121 = V_15;
		V_15 = ((int32_t)((int32_t)((int32_t)((int32_t)L_120-(int32_t)1))-(int32_t)L_121));
	}

IL_0384:
	{
		List_1_t336 * L_122 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		int32_t L_123 = V_13;
		NullCheck(L_122);
		RectTransform_t212 * L_124 = (RectTransform_t212 *)VirtFuncInvoker1< RectTransform_t212 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_122, L_123);
		float L_125 = ((&V_12)->___x_1);
		Vector2_t171  L_126 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_34 = L_126;
		float L_127 = Vector2_get_Item_m2040((&V_34), 0, /*hidden argument*/NULL);
		Vector2_t171  L_128 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_35 = L_128;
		float L_129 = Vector2_get_Item_m2040((&V_35), 0, /*hidden argument*/NULL);
		int32_t L_130 = V_14;
		Vector2_t171  L_131 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_36 = L_131;
		float L_132 = Vector2_get_Item_m2040((&V_36), 0, /*hidden argument*/NULL);
		LayoutGroup_SetChildAlongAxis_m1618(__this, L_124, 0, ((float)((float)L_125+(float)((float)((float)((float)((float)L_127+(float)L_129))*(float)(((float)((float)L_130))))))), L_132, /*hidden argument*/NULL);
		List_1_t336 * L_133 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		int32_t L_134 = V_13;
		NullCheck(L_133);
		RectTransform_t212 * L_135 = (RectTransform_t212 *)VirtFuncInvoker1< RectTransform_t212 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_133, L_134);
		float L_136 = ((&V_12)->___y_2);
		Vector2_t171  L_137 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_37 = L_137;
		float L_138 = Vector2_get_Item_m2040((&V_37), 1, /*hidden argument*/NULL);
		Vector2_t171  L_139 = GridLayoutGroup_get_spacing_m1546(__this, /*hidden argument*/NULL);
		V_38 = L_139;
		float L_140 = Vector2_get_Item_m2040((&V_38), 1, /*hidden argument*/NULL);
		int32_t L_141 = V_15;
		Vector2_t171  L_142 = GridLayoutGroup_get_cellSize_m1544(__this, /*hidden argument*/NULL);
		V_39 = L_142;
		float L_143 = Vector2_get_Item_m2040((&V_39), 1, /*hidden argument*/NULL);
		LayoutGroup_SetChildAlongAxis_m1618(__this, L_135, 1, ((float)((float)L_136+(float)((float)((float)((float)((float)L_138+(float)L_140))*(float)(((float)((float)L_141))))))), L_143, /*hidden argument*/NULL);
		int32_t L_144 = V_13;
		V_13 = ((int32_t)((int32_t)L_144+(int32_t)1));
	}

IL_042c:
	{
		int32_t L_145 = V_13;
		List_1_t336 * L_146 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_146);
		int32_t L_147 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_146);
		if ((((int32_t)L_145) < ((int32_t)L_147)))
		{
			goto IL_0336;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalLayoutGroup::.ctor()
extern "C" void HorizontalLayoutGroup__ctor_m1557 (HorizontalLayoutGroup_t332 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup__ctor_m1562(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputHorizontal()
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1558 (HorizontalLayoutGroup_t332 * __this, const MethodInfo* method)
{
	{
		LayoutGroup_CalculateLayoutInputHorizontal_m1602(__this, /*hidden argument*/NULL);
		HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1569(__this, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputVertical()
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputVertical_m1559 (HorizontalLayoutGroup_t332 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1569(__this, 1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutHorizontal()
extern "C" void HorizontalLayoutGroup_SetLayoutHorizontal_m1560 (HorizontalLayoutGroup_t332 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1570(__this, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutVertical()
extern "C" void HorizontalLayoutGroup_SetLayoutVertical_m1561 (HorizontalLayoutGroup_t332 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1570(__this, 1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
extern "C" void HorizontalOrVerticalLayoutGroup__ctor_m1562 (HorizontalOrVerticalLayoutGroup_t333 * __this, const MethodInfo* method)
{
	{
		__this->___m_ChildForceExpandWidth_11 = 1;
		__this->___m_ChildForceExpandHeight_12 = 1;
		LayoutGroup__ctor_m1595(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
extern "C" float HorizontalOrVerticalLayoutGroup_get_spacing_m1563 (HorizontalOrVerticalLayoutGroup_t333 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Spacing_10);
		return L_0;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
extern const MethodInfo* LayoutGroup_SetProperty_TisSingle_t117_m2271_MethodInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_set_spacing_m1564 (HorizontalOrVerticalLayoutGroup_t333 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisSingle_t117_m2271_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483920);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_Spacing_10);
		float L_1 = ___value;
		LayoutGroup_SetProperty_TisSingle_t117_m2271(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisSingle_t117_m2271_MethodInfo_var);
		return;
	}
}
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1565 (HorizontalOrVerticalLayoutGroup_t333 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_ChildForceExpandWidth_11);
		return L_0;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
extern const MethodInfo* LayoutGroup_SetProperty_TisBoolean_t393_m2272_MethodInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1566 (HorizontalOrVerticalLayoutGroup_t333 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisBoolean_t393_m2272_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483921);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool* L_0 = &(__this->___m_ChildForceExpandWidth_11);
		bool L_1 = ___value;
		LayoutGroup_SetProperty_TisBoolean_t393_m2272(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisBoolean_t393_m2272_MethodInfo_var);
		return;
	}
}
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1567 (HorizontalOrVerticalLayoutGroup_t333 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_ChildForceExpandHeight_12);
		return L_0;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
extern const MethodInfo* LayoutGroup_SetProperty_TisBoolean_t393_m2272_MethodInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1568 (HorizontalOrVerticalLayoutGroup_t333 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisBoolean_t393_m2272_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483921);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool* L_0 = &(__this->___m_ChildForceExpandHeight_12);
		bool L_1 = ___value;
		LayoutGroup_SetProperty_TisBoolean_t393_m2272(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisBoolean_t393_m2272_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
extern TypeInfo* Mathf_t394_il2cpp_TypeInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1569 (HorizontalOrVerticalLayoutGroup_t333 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	int32_t V_5 = 0;
	RectTransform_t212 * V_6 = {0};
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	int32_t G_B3_0 = 0;
	bool G_B7_0 = false;
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		RectOffset_t335 * L_1 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = RectOffset_get_horizontal_m2268(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0021;
	}

IL_0016:
	{
		RectOffset_t335 * L_3 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_vertical_m2270(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0021:
	{
		V_0 = (((float)((float)G_B3_0)));
		float L_5 = V_0;
		V_1 = L_5;
		float L_6 = V_0;
		V_2 = L_6;
		V_3 = (0.0f);
		bool L_7 = ___isVertical;
		int32_t L_8 = ___axis;
		V_4 = ((int32_t)((int32_t)L_7^(int32_t)((((int32_t)L_8) == ((int32_t)1))? 1 : 0)));
		V_5 = 0;
		goto IL_00e2;
	}

IL_003d:
	{
		List_1_t336 * L_9 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_5;
		NullCheck(L_9);
		RectTransform_t212 * L_11 = (RectTransform_t212 *)VirtFuncInvoker1< RectTransform_t212 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_9, L_10);
		V_6 = L_11;
		RectTransform_t212 * L_12 = V_6;
		int32_t L_13 = ___axis;
		float L_14 = LayoutUtility_GetMinSize_m1650(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		V_7 = L_14;
		RectTransform_t212 * L_15 = V_6;
		int32_t L_16 = ___axis;
		float L_17 = LayoutUtility_GetPreferredSize_m1651(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		V_8 = L_17;
		RectTransform_t212 * L_18 = V_6;
		int32_t L_19 = ___axis;
		float L_20 = LayoutUtility_GetFlexibleSize_m1652(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_9 = L_20;
		int32_t L_21 = ___axis;
		if (L_21)
		{
			goto IL_007b;
		}
	}
	{
		bool L_22 = HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1565(__this, /*hidden argument*/NULL);
		G_B7_0 = L_22;
		goto IL_0081;
	}

IL_007b:
	{
		bool L_23 = HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1567(__this, /*hidden argument*/NULL);
		G_B7_0 = L_23;
	}

IL_0081:
	{
		if (!G_B7_0)
		{
			goto IL_0094;
		}
	}
	{
		float L_24 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_25 = Mathf_Max_m2208(NULL /*static, unused*/, L_24, (1.0f), /*hidden argument*/NULL);
		V_9 = L_25;
	}

IL_0094:
	{
		bool L_26 = V_4;
		if (!L_26)
		{
			goto IL_00bf;
		}
	}
	{
		float L_27 = V_7;
		float L_28 = V_0;
		float L_29 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_30 = Mathf_Max_m2208(NULL /*static, unused*/, ((float)((float)L_27+(float)L_28)), L_29, /*hidden argument*/NULL);
		V_1 = L_30;
		float L_31 = V_8;
		float L_32 = V_0;
		float L_33 = V_2;
		float L_34 = Mathf_Max_m2208(NULL /*static, unused*/, ((float)((float)L_31+(float)L_32)), L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		float L_35 = V_9;
		float L_36 = V_3;
		float L_37 = Mathf_Max_m2208(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_3 = L_37;
		goto IL_00dc;
	}

IL_00bf:
	{
		float L_38 = V_1;
		float L_39 = V_7;
		float L_40 = HorizontalOrVerticalLayoutGroup_get_spacing_m1563(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_38+(float)((float)((float)L_39+(float)L_40))));
		float L_41 = V_2;
		float L_42 = V_8;
		float L_43 = HorizontalOrVerticalLayoutGroup_get_spacing_m1563(__this, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_41+(float)((float)((float)L_42+(float)L_43))));
		float L_44 = V_3;
		float L_45 = V_9;
		V_3 = ((float)((float)L_44+(float)L_45));
	}

IL_00dc:
	{
		int32_t L_46 = V_5;
		V_5 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_00e2:
	{
		int32_t L_47 = V_5;
		List_1_t336 * L_48 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_49 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_48);
		if ((((int32_t)L_47) < ((int32_t)L_49)))
		{
			goto IL_003d;
		}
	}
	{
		bool L_50 = V_4;
		if (L_50)
		{
			goto IL_011e;
		}
	}
	{
		List_1_t336 * L_51 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_51);
		int32_t L_52 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_51);
		if ((((int32_t)L_52) <= ((int32_t)0)))
		{
			goto IL_011e;
		}
	}
	{
		float L_53 = V_1;
		float L_54 = HorizontalOrVerticalLayoutGroup_get_spacing_m1563(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_53-(float)L_54));
		float L_55 = V_2;
		float L_56 = HorizontalOrVerticalLayoutGroup_get_spacing_m1563(__this, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_55-(float)L_56));
	}

IL_011e:
	{
		float L_57 = V_1;
		float L_58 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_59 = Mathf_Max_m2208(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		V_2 = L_59;
		float L_60 = V_1;
		float L_61 = V_2;
		float L_62 = V_3;
		int32_t L_63 = ___axis;
		LayoutGroup_SetLayoutInputForAxis_m1617(__this, L_60, L_61, L_62, L_63, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
extern TypeInfo* Mathf_t394_il2cpp_TypeInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1570 (HorizontalOrVerticalLayoutGroup_t333 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	RectTransform_t212 * V_4 = {0};
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	int32_t V_13 = 0;
	RectTransform_t212 * V_14 = {0};
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	Rect_t94  V_19 = {0};
	Vector2_t171  V_20 = {0};
	float G_B3_0 = 0.0f;
	float G_B2_0 = 0.0f;
	int32_t G_B4_0 = 0;
	float G_B4_1 = 0.0f;
	bool G_B8_0 = false;
	float G_B12_0 = 0.0f;
	float G_B12_1 = 0.0f;
	float G_B11_0 = 0.0f;
	float G_B11_1 = 0.0f;
	float G_B13_0 = 0.0f;
	float G_B13_1 = 0.0f;
	float G_B13_2 = 0.0f;
	int32_t G_B19_0 = 0;
	float G_B23_0 = 0.0f;
	int32_t G_B23_1 = 0;
	HorizontalOrVerticalLayoutGroup_t333 * G_B23_2 = {0};
	float G_B22_0 = 0.0f;
	int32_t G_B22_1 = 0;
	HorizontalOrVerticalLayoutGroup_t333 * G_B22_2 = {0};
	int32_t G_B24_0 = 0;
	float G_B24_1 = 0.0f;
	int32_t G_B24_2 = 0;
	HorizontalOrVerticalLayoutGroup_t333 * G_B24_3 = {0};
	bool G_B34_0 = false;
	{
		RectTransform_t212 * L_0 = LayoutGroup_get_rectTransform_m1600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rect_t94  L_1 = RectTransform_get_rect_m1916(L_0, /*hidden argument*/NULL);
		V_19 = L_1;
		Vector2_t171  L_2 = Rect_get_size_m1921((&V_19), /*hidden argument*/NULL);
		V_20 = L_2;
		int32_t L_3 = ___axis;
		float L_4 = Vector2_get_Item_m2040((&V_20), L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = ___isVertical;
		int32_t L_6 = ___axis;
		V_1 = ((int32_t)((int32_t)L_5^(int32_t)((((int32_t)L_6) == ((int32_t)1))? 1 : 0)));
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_00fe;
		}
	}
	{
		float L_8 = V_0;
		int32_t L_9 = ___axis;
		G_B2_0 = L_8;
		if (L_9)
		{
			G_B3_0 = L_8;
			goto IL_0043;
		}
	}
	{
		RectOffset_t335 * L_10 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_horizontal_m2268(L_10, /*hidden argument*/NULL);
		G_B4_0 = L_11;
		G_B4_1 = G_B2_0;
		goto IL_004e;
	}

IL_0043:
	{
		RectOffset_t335 * L_12 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_vertical_m2270(L_12, /*hidden argument*/NULL);
		G_B4_0 = L_13;
		G_B4_1 = G_B3_0;
	}

IL_004e:
	{
		V_2 = ((float)((float)G_B4_1-(float)(((float)((float)G_B4_0)))));
		V_3 = 0;
		goto IL_00e8;
	}

IL_0058:
	{
		List_1_t336 * L_14 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		int32_t L_15 = V_3;
		NullCheck(L_14);
		RectTransform_t212 * L_16 = (RectTransform_t212 *)VirtFuncInvoker1< RectTransform_t212 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_14, L_15);
		V_4 = L_16;
		RectTransform_t212 * L_17 = V_4;
		int32_t L_18 = ___axis;
		float L_19 = LayoutUtility_GetMinSize_m1650(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_5 = L_19;
		RectTransform_t212 * L_20 = V_4;
		int32_t L_21 = ___axis;
		float L_22 = LayoutUtility_GetPreferredSize_m1651(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		V_6 = L_22;
		RectTransform_t212 * L_23 = V_4;
		int32_t L_24 = ___axis;
		float L_25 = LayoutUtility_GetFlexibleSize_m1652(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		V_7 = L_25;
		int32_t L_26 = ___axis;
		if (L_26)
		{
			goto IL_0095;
		}
	}
	{
		bool L_27 = HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1565(__this, /*hidden argument*/NULL);
		G_B8_0 = L_27;
		goto IL_009b;
	}

IL_0095:
	{
		bool L_28 = HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1567(__this, /*hidden argument*/NULL);
		G_B8_0 = L_28;
	}

IL_009b:
	{
		if (!G_B8_0)
		{
			goto IL_00ae;
		}
	}
	{
		float L_29 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_30 = Mathf_Max_m2208(NULL /*static, unused*/, L_29, (1.0f), /*hidden argument*/NULL);
		V_7 = L_30;
	}

IL_00ae:
	{
		float L_31 = V_2;
		float L_32 = V_5;
		float L_33 = V_7;
		G_B11_0 = L_32;
		G_B11_1 = L_31;
		if ((!(((float)L_33) > ((float)(0.0f)))))
		{
			G_B12_0 = L_32;
			G_B12_1 = L_31;
			goto IL_00c3;
		}
	}
	{
		float L_34 = V_0;
		G_B13_0 = L_34;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c5;
	}

IL_00c3:
	{
		float L_35 = V_6;
		G_B13_0 = L_35;
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_36 = Mathf_Clamp_m2025(NULL /*static, unused*/, G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		V_8 = L_36;
		int32_t L_37 = ___axis;
		float L_38 = V_8;
		float L_39 = LayoutGroup_GetStartOffset_m1616(__this, L_37, L_38, /*hidden argument*/NULL);
		V_9 = L_39;
		RectTransform_t212 * L_40 = V_4;
		int32_t L_41 = ___axis;
		float L_42 = V_9;
		float L_43 = V_8;
		LayoutGroup_SetChildAlongAxis_m1618(__this, L_40, L_41, L_42, L_43, /*hidden argument*/NULL);
		int32_t L_44 = V_3;
		V_3 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_00e8:
	{
		int32_t L_45 = V_3;
		List_1_t336 * L_46 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		int32_t L_47 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_46);
		if ((((int32_t)L_45) < ((int32_t)L_47)))
		{
			goto IL_0058;
		}
	}
	{
		goto IL_028e;
	}

IL_00fe:
	{
		int32_t L_48 = ___axis;
		if (L_48)
		{
			goto IL_0114;
		}
	}
	{
		RectOffset_t335 * L_49 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_49);
		int32_t L_50 = RectOffset_get_left_m2273(L_49, /*hidden argument*/NULL);
		G_B19_0 = L_50;
		goto IL_011f;
	}

IL_0114:
	{
		RectOffset_t335 * L_51 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_51);
		int32_t L_52 = RectOffset_get_top_m2274(L_51, /*hidden argument*/NULL);
		G_B19_0 = L_52;
	}

IL_011f:
	{
		V_10 = (((float)((float)G_B19_0)));
		int32_t L_53 = ___axis;
		float L_54 = LayoutGroup_GetTotalFlexibleSize_m1615(__this, L_53, /*hidden argument*/NULL);
		if ((!(((float)L_54) == ((float)(0.0f)))))
		{
			goto IL_0173;
		}
	}
	{
		int32_t L_55 = ___axis;
		float L_56 = LayoutGroup_GetTotalPreferredSize_m1614(__this, L_55, /*hidden argument*/NULL);
		float L_57 = V_0;
		if ((!(((float)L_56) < ((float)L_57))))
		{
			goto IL_0173;
		}
	}
	{
		int32_t L_58 = ___axis;
		int32_t L_59 = ___axis;
		float L_60 = LayoutGroup_GetTotalPreferredSize_m1614(__this, L_59, /*hidden argument*/NULL);
		int32_t L_61 = ___axis;
		G_B22_0 = L_60;
		G_B22_1 = L_58;
		G_B22_2 = __this;
		if (L_61)
		{
			G_B23_0 = L_60;
			G_B23_1 = L_58;
			G_B23_2 = __this;
			goto IL_015f;
		}
	}
	{
		RectOffset_t335 * L_62 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = RectOffset_get_horizontal_m2268(L_62, /*hidden argument*/NULL);
		G_B24_0 = L_63;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		G_B24_3 = G_B22_2;
		goto IL_016a;
	}

IL_015f:
	{
		RectOffset_t335 * L_64 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_64);
		int32_t L_65 = RectOffset_get_vertical_m2270(L_64, /*hidden argument*/NULL);
		G_B24_0 = L_65;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
		G_B24_3 = G_B23_2;
	}

IL_016a:
	{
		NullCheck(G_B24_3);
		float L_66 = LayoutGroup_GetStartOffset_m1616(G_B24_3, G_B24_2, ((float)((float)G_B24_1-(float)(((float)((float)G_B24_0))))), /*hidden argument*/NULL);
		V_10 = L_66;
	}

IL_0173:
	{
		V_11 = (0.0f);
		int32_t L_67 = ___axis;
		float L_68 = LayoutGroup_GetTotalMinSize_m1613(__this, L_67, /*hidden argument*/NULL);
		int32_t L_69 = ___axis;
		float L_70 = LayoutGroup_GetTotalPreferredSize_m1614(__this, L_69, /*hidden argument*/NULL);
		if ((((float)L_68) == ((float)L_70)))
		{
			goto IL_01ad;
		}
	}
	{
		float L_71 = V_0;
		int32_t L_72 = ___axis;
		float L_73 = LayoutGroup_GetTotalMinSize_m1613(__this, L_72, /*hidden argument*/NULL);
		int32_t L_74 = ___axis;
		float L_75 = LayoutGroup_GetTotalPreferredSize_m1614(__this, L_74, /*hidden argument*/NULL);
		int32_t L_76 = ___axis;
		float L_77 = LayoutGroup_GetTotalMinSize_m1613(__this, L_76, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_78 = Mathf_Clamp01_m2016(NULL /*static, unused*/, ((float)((float)((float)((float)L_71-(float)L_73))/(float)((float)((float)L_75-(float)L_77)))), /*hidden argument*/NULL);
		V_11 = L_78;
	}

IL_01ad:
	{
		V_12 = (0.0f);
		float L_79 = V_0;
		int32_t L_80 = ___axis;
		float L_81 = LayoutGroup_GetTotalPreferredSize_m1614(__this, L_80, /*hidden argument*/NULL);
		if ((!(((float)L_79) > ((float)L_81))))
		{
			goto IL_01e5;
		}
	}
	{
		int32_t L_82 = ___axis;
		float L_83 = LayoutGroup_GetTotalFlexibleSize_m1615(__this, L_82, /*hidden argument*/NULL);
		if ((!(((float)L_83) > ((float)(0.0f)))))
		{
			goto IL_01e5;
		}
	}
	{
		float L_84 = V_0;
		int32_t L_85 = ___axis;
		float L_86 = LayoutGroup_GetTotalPreferredSize_m1614(__this, L_85, /*hidden argument*/NULL);
		int32_t L_87 = ___axis;
		float L_88 = LayoutGroup_GetTotalFlexibleSize_m1615(__this, L_87, /*hidden argument*/NULL);
		V_12 = ((float)((float)((float)((float)L_84-(float)L_86))/(float)L_88));
	}

IL_01e5:
	{
		V_13 = 0;
		goto IL_027c;
	}

IL_01ed:
	{
		List_1_t336 * L_89 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		int32_t L_90 = V_13;
		NullCheck(L_89);
		RectTransform_t212 * L_91 = (RectTransform_t212 *)VirtFuncInvoker1< RectTransform_t212 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_89, L_90);
		V_14 = L_91;
		RectTransform_t212 * L_92 = V_14;
		int32_t L_93 = ___axis;
		float L_94 = LayoutUtility_GetMinSize_m1650(NULL /*static, unused*/, L_92, L_93, /*hidden argument*/NULL);
		V_15 = L_94;
		RectTransform_t212 * L_95 = V_14;
		int32_t L_96 = ___axis;
		float L_97 = LayoutUtility_GetPreferredSize_m1651(NULL /*static, unused*/, L_95, L_96, /*hidden argument*/NULL);
		V_16 = L_97;
		RectTransform_t212 * L_98 = V_14;
		int32_t L_99 = ___axis;
		float L_100 = LayoutUtility_GetFlexibleSize_m1652(NULL /*static, unused*/, L_98, L_99, /*hidden argument*/NULL);
		V_17 = L_100;
		int32_t L_101 = ___axis;
		if (L_101)
		{
			goto IL_022b;
		}
	}
	{
		bool L_102 = HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1565(__this, /*hidden argument*/NULL);
		G_B34_0 = L_102;
		goto IL_0231;
	}

IL_022b:
	{
		bool L_103 = HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1567(__this, /*hidden argument*/NULL);
		G_B34_0 = L_103;
	}

IL_0231:
	{
		if (!G_B34_0)
		{
			goto IL_0244;
		}
	}
	{
		float L_104 = V_17;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_105 = Mathf_Max_m2208(NULL /*static, unused*/, L_104, (1.0f), /*hidden argument*/NULL);
		V_17 = L_105;
	}

IL_0244:
	{
		float L_106 = V_15;
		float L_107 = V_16;
		float L_108 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_109 = Mathf_Lerp_m1843(NULL /*static, unused*/, L_106, L_107, L_108, /*hidden argument*/NULL);
		V_18 = L_109;
		float L_110 = V_18;
		float L_111 = V_17;
		float L_112 = V_12;
		V_18 = ((float)((float)L_110+(float)((float)((float)L_111*(float)L_112))));
		RectTransform_t212 * L_113 = V_14;
		int32_t L_114 = ___axis;
		float L_115 = V_10;
		float L_116 = V_18;
		LayoutGroup_SetChildAlongAxis_m1618(__this, L_113, L_114, L_115, L_116, /*hidden argument*/NULL);
		float L_117 = V_10;
		float L_118 = V_18;
		float L_119 = HorizontalOrVerticalLayoutGroup_get_spacing_m1563(__this, /*hidden argument*/NULL);
		V_10 = ((float)((float)L_117+(float)((float)((float)L_118+(float)L_119))));
		int32_t L_120 = V_13;
		V_13 = ((int32_t)((int32_t)L_120+(int32_t)1));
	}

IL_027c:
	{
		int32_t L_121 = V_13;
		List_1_t336 * L_122 = LayoutGroup_get_rectChildren_m1601(__this, /*hidden argument*/NULL);
		NullCheck(L_122);
		int32_t L_123 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_122);
		if ((((int32_t)L_121) < ((int32_t)L_123)))
		{
			goto IL_01ed;
		}
	}

IL_028e:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::.ctor()
extern "C" void LayoutElement__ctor_m1571 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		__this->___m_MinWidth_3 = (-1.0f);
		__this->___m_MinHeight_4 = (-1.0f);
		__this->___m_PreferredWidth_5 = (-1.0f);
		__this->___m_PreferredHeight_6 = (-1.0f);
		__this->___m_FlexibleWidth_7 = (-1.0f);
		__this->___m_FlexibleHeight_8 = (-1.0f);
		UIBehaviour__ctor_m407(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutElement::get_ignoreLayout()
extern "C" bool LayoutElement_get_ignoreLayout_m1572 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_IgnoreLayout_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_ignoreLayout(System.Boolean)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisBoolean_t393_m2014_MethodInfo_var;
extern "C" void LayoutElement_set_ignoreLayout_m1573 (LayoutElement_t334 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisBoolean_t393_m2014_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483856);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool* L_0 = &(__this->___m_IgnoreLayout_2);
		bool L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisBoolean_t393_m2014(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisBoolean_t393_m2014_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m1594(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputHorizontal()
extern "C" void LayoutElement_CalculateLayoutInputHorizontal_m1574 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputVertical()
extern "C" void LayoutElement_CalculateLayoutInputVertical_m1575 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_minWidth()
extern "C" float LayoutElement_get_minWidth_m1576 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_MinWidth_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_minWidth(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var;
extern "C" void LayoutElement_set_minWidth_m1577 (LayoutElement_t334 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483858);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_MinWidth_3);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t117_m2017(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m1594(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_minHeight()
extern "C" float LayoutElement_get_minHeight_m1578 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_MinHeight_4);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_minHeight(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var;
extern "C" void LayoutElement_set_minHeight_m1579 (LayoutElement_t334 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483858);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_MinHeight_4);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t117_m2017(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m1594(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_preferredWidth()
extern "C" float LayoutElement_get_preferredWidth_m1580 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_PreferredWidth_5);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_preferredWidth(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var;
extern "C" void LayoutElement_set_preferredWidth_m1581 (LayoutElement_t334 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483858);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_PreferredWidth_5);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t117_m2017(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m1594(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_preferredHeight()
extern "C" float LayoutElement_get_preferredHeight_m1582 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_PreferredHeight_6);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_preferredHeight(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var;
extern "C" void LayoutElement_set_preferredHeight_m1583 (LayoutElement_t334 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483858);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_PreferredHeight_6);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t117_m2017(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m1594(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_flexibleWidth()
extern "C" float LayoutElement_get_flexibleWidth_m1584 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FlexibleWidth_7);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_flexibleWidth(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var;
extern "C" void LayoutElement_set_flexibleWidth_m1585 (LayoutElement_t334 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483858);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_FlexibleWidth_7);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t117_m2017(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m1594(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_flexibleHeight()
extern "C" float LayoutElement_get_flexibleHeight_m1586 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FlexibleHeight_8);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_flexibleHeight(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var;
extern "C" void LayoutElement_set_flexibleHeight_m1587 (LayoutElement_t334 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483858);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_FlexibleHeight_8);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t117_m2017(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t117_m2017_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m1594(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Int32 UnityEngine.UI.LayoutElement::get_layoutPriority()
extern "C" int32_t LayoutElement_get_layoutPriority_m1588 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnEnable()
extern "C" void LayoutElement_OnEnable_m1589 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m409(__this, /*hidden argument*/NULL);
		LayoutElement_SetDirty_m1594(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnTransformParentChanged()
extern "C" void LayoutElement_OnTransformParentChanged_m1590 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		LayoutElement_SetDirty_m1594(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnDisable()
extern "C" void LayoutElement_OnDisable_m1591 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		LayoutElement_SetDirty_m1594(__this, /*hidden argument*/NULL);
		UIBehaviour_OnDisable_m411(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnDidApplyAnimationProperties()
extern "C" void LayoutElement_OnDidApplyAnimationProperties_m1592 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		LayoutElement_SetDirty_m1594(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnBeforeTransformParentChanged()
extern "C" void LayoutElement_OnBeforeTransformParentChanged_m1593 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	{
		LayoutElement_SetDirty_m1594(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::SetDirty()
extern TypeInfo* RectTransform_t212_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern "C" void LayoutElement_SetDirty_m1594 (LayoutElement_t334 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t212_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(152);
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Transform_t84 * L_1 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m1635(NULL /*static, unused*/, ((RectTransform_t212 *)IsInstSealed(L_1, RectTransform_t212_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::.ctor()
extern TypeInfo* RectOffset_t335_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t336_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2276_MethodInfo_var;
extern "C" void LayoutGroup__ctor_m1595 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t335_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(287);
		List_1_t336_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(288);
		List_1__ctor_m2276_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483922);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t335 * L_0 = (RectOffset_t335 *)il2cpp_codegen_object_new (RectOffset_t335_il2cpp_TypeInfo_var);
		RectOffset__ctor_m2275(L_0, /*hidden argument*/NULL);
		__this->___m_Padding_2 = L_0;
		Vector2_t171  L_1 = Vector2_get_zero_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_TotalMinSize_6 = L_1;
		Vector2_t171  L_2 = Vector2_get_zero_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_TotalPreferredSize_7 = L_2;
		Vector2_t171  L_3 = Vector2_get_zero_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_TotalFlexibleSize_8 = L_3;
		List_1_t336 * L_4 = (List_1_t336 *)il2cpp_codegen_object_new (List_1_t336_il2cpp_TypeInfo_var);
		List_1__ctor_m2276(L_4, /*hidden argument*/List_1__ctor_m2276_MethodInfo_var);
		__this->___m_RectChildren_9 = L_4;
		UIBehaviour__ctor_m407(__this, /*hidden argument*/NULL);
		RectOffset_t335 * L_5 = (__this->___m_Padding_2);
		if (L_5)
		{
			goto IL_0053;
		}
	}
	{
		RectOffset_t335 * L_6 = (RectOffset_t335 *)il2cpp_codegen_object_new (RectOffset_t335_il2cpp_TypeInfo_var);
		RectOffset__ctor_m2275(L_6, /*hidden argument*/NULL);
		__this->___m_Padding_2 = L_6;
	}

IL_0053:
	{
		return;
	}
}
// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::get_padding()
extern "C" RectOffset_t335 * LayoutGroup_get_padding_m1596 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		RectOffset_t335 * L_0 = (__this->___m_Padding_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::set_padding(UnityEngine.RectOffset)
extern const MethodInfo* LayoutGroup_SetProperty_TisRectOffset_t335_m2277_MethodInfo_var;
extern "C" void LayoutGroup_set_padding_m1597 (LayoutGroup_t331 * __this, RectOffset_t335 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisRectOffset_t335_m2277_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483923);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t335 ** L_0 = &(__this->___m_Padding_2);
		RectOffset_t335 * L_1 = ___value;
		LayoutGroup_SetProperty_TisRectOffset_t335_m2277(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisRectOffset_t335_m2277_MethodInfo_var);
		return;
	}
}
// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::get_childAlignment()
extern "C" int32_t LayoutGroup_get_childAlignment_m1598 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_ChildAlignment_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::set_childAlignment(UnityEngine.TextAnchor)
extern const MethodInfo* LayoutGroup_SetProperty_TisTextAnchor_t444_m2278_MethodInfo_var;
extern "C" void LayoutGroup_set_childAlignment_m1599 (LayoutGroup_t331 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisTextAnchor_t444_m2278_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483924);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_ChildAlignment_3);
		int32_t L_1 = ___value;
		LayoutGroup_SetProperty_TisTextAnchor_t444_m2278(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisTextAnchor_t444_m2278_MethodInfo_var);
		return;
	}
}
// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::get_rectTransform()
extern const MethodInfo* Component_GetComponent_TisRectTransform_t212_m1961_MethodInfo_var;
extern "C" RectTransform_t212 * LayoutGroup_get_rectTransform_m1600 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRectTransform_t212_m1961_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483839);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t212 * L_0 = (__this->___m_Rect_4);
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RectTransform_t212 * L_2 = Component_GetComponent_TisRectTransform_t212_m1961(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t212_m1961_MethodInfo_var);
		__this->___m_Rect_4 = L_2;
	}

IL_001d:
	{
		RectTransform_t212 * L_3 = (__this->___m_Rect_4);
		return L_3;
	}
}
// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::get_rectChildren()
extern "C" List_1_t336 * LayoutGroup_get_rectChildren_m1601 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		List_1_t336 * L_0 = (__this->___m_RectChildren_9);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputHorizontal()
extern const Il2CppType* ILayoutIgnorer_t445_0_0_0_var;
extern TypeInfo* ListPool_1_t415_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransform_t212_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ILayoutIgnorer_t445_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m1963_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m1965_MethodInfo_var;
extern "C" void LayoutGroup_CalculateLayoutInputHorizontal_m1602 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutIgnorer_t445_0_0_0_var = il2cpp_codegen_type_from_index(290);
		ListPool_1_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(199);
		RectTransform_t212_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(152);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		ILayoutIgnorer_t445_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(290);
		ListPool_1_Get_m1963_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483841);
		ListPool_1_Release_m1965_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483842);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t388 * V_0 = {0};
	int32_t V_1 = 0;
	RectTransform_t212 * V_2 = {0};
	int32_t V_3 = 0;
	Object_t * V_4 = {0};
	{
		List_1_t336 * L_0 = (__this->___m_RectChildren_9);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.RectTransform>::Clear() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t415_il2cpp_TypeInfo_var);
		List_1_t388 * L_1 = ListPool_1_Get_m1963(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m1963_MethodInfo_var);
		V_0 = L_1;
		V_1 = 0;
		goto IL_00be;
	}

IL_0018:
	{
		RectTransform_t212 * L_2 = LayoutGroup_get_rectTransform_m1600(__this, /*hidden argument*/NULL);
		int32_t L_3 = V_1;
		NullCheck(L_2);
		Transform_t84 * L_4 = Transform_GetChild_m146(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((RectTransform_t212 *)IsInstSealed(L_4, RectTransform_t212_il2cpp_TypeInfo_var));
		RectTransform_t212 * L_5 = V_2;
		bool L_6 = Object_op_Equality_m165(NULL /*static, unused*/, L_5, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0046;
		}
	}
	{
		RectTransform_t212 * L_7 = V_2;
		NullCheck(L_7);
		GameObject_t9 * L_8 = Component_get_gameObject_m147(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = GameObject_get_activeInHierarchy_m1767(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_004b;
		}
	}

IL_0046:
	{
		goto IL_00ba;
	}

IL_004b:
	{
		RectTransform_t212 * L_10 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m180(NULL /*static, unused*/, LoadTypeToken(ILayoutIgnorer_t445_0_0_0_var), /*hidden argument*/NULL);
		List_1_t388 * L_12 = V_0;
		NullCheck(L_10);
		Component_GetComponents_m1964(L_10, L_11, L_12, /*hidden argument*/NULL);
		List_1_t388 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_13);
		if (L_14)
		{
			goto IL_0078;
		}
	}
	{
		List_1_t336 * L_15 = (__this->___m_RectChildren_9);
		RectTransform_t212 * L_16 = V_2;
		NullCheck(L_15);
		VirtActionInvoker1< RectTransform_t212 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.RectTransform>::Add(!0) */, L_15, L_16);
		goto IL_00ba;
	}

IL_0078:
	{
		V_3 = 0;
		goto IL_00ae;
	}

IL_007f:
	{
		List_1_t388 * L_17 = V_0;
		int32_t L_18 = V_3;
		NullCheck(L_17);
		Component_t130 * L_19 = (Component_t130 *)VirtFuncInvoker1< Component_t130 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_17, L_18);
		V_4 = ((Object_t *)Castclass(L_19, ILayoutIgnorer_t445_il2cpp_TypeInfo_var));
		Object_t * L_20 = V_4;
		NullCheck(L_20);
		bool L_21 = (bool)InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.UI.ILayoutIgnorer::get_ignoreLayout() */, ILayoutIgnorer_t445_il2cpp_TypeInfo_var, L_20);
		if (L_21)
		{
			goto IL_00aa;
		}
	}
	{
		List_1_t336 * L_22 = (__this->___m_RectChildren_9);
		RectTransform_t212 * L_23 = V_2;
		NullCheck(L_22);
		VirtActionInvoker1< RectTransform_t212 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.RectTransform>::Add(!0) */, L_22, L_23);
		goto IL_00ba;
	}

IL_00aa:
	{
		int32_t L_24 = V_3;
		V_3 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00ae:
	{
		int32_t L_25 = V_3;
		List_1_t388 * L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_26);
		if ((((int32_t)L_25) < ((int32_t)L_27)))
		{
			goto IL_007f;
		}
	}

IL_00ba:
	{
		int32_t L_28 = V_1;
		V_1 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00be:
	{
		int32_t L_29 = V_1;
		RectTransform_t212 * L_30 = LayoutGroup_get_rectTransform_m1600(__this, /*hidden argument*/NULL);
		NullCheck(L_30);
		int32_t L_31 = Transform_get_childCount_m1868(L_30, /*hidden argument*/NULL);
		if ((((int32_t)L_29) < ((int32_t)L_31)))
		{
			goto IL_0018;
		}
	}
	{
		List_1_t388 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t415_il2cpp_TypeInfo_var);
		ListPool_1_Release_m1965(NULL /*static, unused*/, L_32, /*hidden argument*/ListPool_1_Release_m1965_MethodInfo_var);
		DrivenRectTransformTracker_t291 * L_33 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m2166(L_33, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_minWidth()
extern "C" float LayoutGroup_get_minWidth_m1603 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalMinSize_m1613(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_preferredWidth()
extern "C" float LayoutGroup_get_preferredWidth_m1604 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalPreferredSize_m1614(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleWidth()
extern "C" float LayoutGroup_get_flexibleWidth_m1605 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalFlexibleSize_m1615(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_minHeight()
extern "C" float LayoutGroup_get_minHeight_m1606 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalMinSize_m1613(__this, 1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_preferredHeight()
extern "C" float LayoutGroup_get_preferredHeight_m1607 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalPreferredSize_m1614(__this, 1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleHeight()
extern "C" float LayoutGroup_get_flexibleHeight_m1608 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalFlexibleSize_m1615(__this, 1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.UI.LayoutGroup::get_layoutPriority()
extern "C" int32_t LayoutGroup_get_layoutPriority_m1609 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnEnable()
extern "C" void LayoutGroup_OnEnable_m1610 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m409(__this, /*hidden argument*/NULL);
		LayoutGroup_SetDirty_m1622(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnDisable()
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern "C" void LayoutGroup_OnDisable_m1611 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		s_Il2CppMethodIntialized = true;
	}
	{
		DrivenRectTransformTracker_t291 * L_0 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m2166(L_0, /*hidden argument*/NULL);
		RectTransform_t212 * L_1 = LayoutGroup_get_rectTransform_m1600(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m1635(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		UIBehaviour_OnDisable_m411(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnDidApplyAnimationProperties()
extern "C" void LayoutGroup_OnDidApplyAnimationProperties_m1612 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		LayoutGroup_SetDirty_m1622(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::GetTotalMinSize(System.Int32)
extern "C" float LayoutGroup_GetTotalMinSize_m1613 (LayoutGroup_t331 * __this, int32_t ___axis, const MethodInfo* method)
{
	{
		Vector2_t171 * L_0 = &(__this->___m_TotalMinSize_6);
		int32_t L_1 = ___axis;
		float L_2 = Vector2_get_Item_m2040(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::GetTotalPreferredSize(System.Int32)
extern "C" float LayoutGroup_GetTotalPreferredSize_m1614 (LayoutGroup_t331 * __this, int32_t ___axis, const MethodInfo* method)
{
	{
		Vector2_t171 * L_0 = &(__this->___m_TotalPreferredSize_7);
		int32_t L_1 = ___axis;
		float L_2 = Vector2_get_Item_m2040(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::GetTotalFlexibleSize(System.Int32)
extern "C" float LayoutGroup_GetTotalFlexibleSize_m1615 (LayoutGroup_t331 * __this, int32_t ___axis, const MethodInfo* method)
{
	{
		Vector2_t171 * L_0 = &(__this->___m_TotalFlexibleSize_8);
		int32_t L_1 = ___axis;
		float L_2 = Vector2_get_Item_m2040(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::GetStartOffset(System.Int32,System.Single)
extern "C" float LayoutGroup_GetStartOffset_m1616 (LayoutGroup_t331 * __this, int32_t ___axis, float ___requiredSpaceWithoutPadding, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Rect_t94  V_4 = {0};
	Vector2_t171  V_5 = {0};
	float G_B2_0 = 0.0f;
	float G_B1_0 = 0.0f;
	int32_t G_B3_0 = 0;
	float G_B3_1 = 0.0f;
	int32_t G_B9_0 = 0;
	{
		float L_0 = ___requiredSpaceWithoutPadding;
		int32_t L_1 = ___axis;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0017;
		}
	}
	{
		RectOffset_t335 * L_2 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = RectOffset_get_horizontal_m2268(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_0022;
	}

IL_0017:
	{
		RectOffset_t335 * L_4 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_vertical_m2270(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_0022:
	{
		V_0 = ((float)((float)G_B3_1+(float)(((float)((float)G_B3_0)))));
		RectTransform_t212 * L_6 = LayoutGroup_get_rectTransform_m1600(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Rect_t94  L_7 = RectTransform_get_rect_m1916(L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		Vector2_t171  L_8 = Rect_get_size_m1921((&V_4), /*hidden argument*/NULL);
		V_5 = L_8;
		int32_t L_9 = ___axis;
		float L_10 = Vector2_get_Item_m2040((&V_5), L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = V_1;
		float L_12 = V_0;
		V_2 = ((float)((float)L_11-(float)L_12));
		V_3 = (0.0f);
		int32_t L_13 = ___axis;
		if (L_13)
		{
			goto IL_0069;
		}
	}
	{
		int32_t L_14 = LayoutGroup_get_childAlignment_m1598(__this, /*hidden argument*/NULL);
		V_3 = ((float)((float)(((float)((float)((int32_t)((int32_t)L_14%(int32_t)3)))))*(float)(0.5f)));
		goto IL_0079;
	}

IL_0069:
	{
		int32_t L_15 = LayoutGroup_get_childAlignment_m1598(__this, /*hidden argument*/NULL);
		V_3 = ((float)((float)(((float)((float)((int32_t)((int32_t)L_15/(int32_t)3)))))*(float)(0.5f)));
	}

IL_0079:
	{
		int32_t L_16 = ___axis;
		if (L_16)
		{
			goto IL_008f;
		}
	}
	{
		RectOffset_t335 * L_17 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		int32_t L_18 = RectOffset_get_left_m2273(L_17, /*hidden argument*/NULL);
		G_B9_0 = L_18;
		goto IL_009a;
	}

IL_008f:
	{
		RectOffset_t335 * L_19 = LayoutGroup_get_padding_m1596(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_top_m2274(L_19, /*hidden argument*/NULL);
		G_B9_0 = L_20;
	}

IL_009a:
	{
		float L_21 = V_2;
		float L_22 = V_3;
		return ((float)((float)(((float)((float)G_B9_0)))+(float)((float)((float)L_21*(float)L_22))));
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutInputForAxis(System.Single,System.Single,System.Single,System.Int32)
extern "C" void LayoutGroup_SetLayoutInputForAxis_m1617 (LayoutGroup_t331 * __this, float ___totalMin, float ___totalPreferred, float ___totalFlexible, int32_t ___axis, const MethodInfo* method)
{
	{
		Vector2_t171 * L_0 = &(__this->___m_TotalMinSize_6);
		int32_t L_1 = ___axis;
		float L_2 = ___totalMin;
		Vector2_set_Item_m2048(L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector2_t171 * L_3 = &(__this->___m_TotalPreferredSize_7);
		int32_t L_4 = ___axis;
		float L_5 = ___totalPreferred;
		Vector2_set_Item_m2048(L_3, L_4, L_5, /*hidden argument*/NULL);
		Vector2_t171 * L_6 = &(__this->___m_TotalFlexibleSize_8);
		int32_t L_7 = ___axis;
		float L_8 = ___totalFlexible;
		Vector2_set_Item_m2048(L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetChildAlongAxis(UnityEngine.RectTransform,System.Int32,System.Single,System.Single)
extern "C" void LayoutGroup_SetChildAlongAxis_m1618 (LayoutGroup_t331 * __this, RectTransform_t212 * ___rect, int32_t ___axis, float ___pos, float ___size, const MethodInfo* method)
{
	RectTransform_t212 * G_B4_0 = {0};
	RectTransform_t212 * G_B3_0 = {0};
	int32_t G_B5_0 = 0;
	RectTransform_t212 * G_B5_1 = {0};
	{
		RectTransform_t212 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		DrivenRectTransformTracker_t291 * L_2 = &(__this->___m_Tracker_5);
		RectTransform_t212 * L_3 = ___rect;
		DrivenRectTransformTracker_Add_m2167(L_2, __this, L_3, ((int32_t)16134), /*hidden argument*/NULL);
		RectTransform_t212 * L_4 = ___rect;
		int32_t L_5 = ___axis;
		G_B3_0 = L_4;
		if (L_5)
		{
			G_B4_0 = L_4;
			goto IL_002c;
		}
	}
	{
		G_B5_0 = 0;
		G_B5_1 = G_B3_0;
		goto IL_002d;
	}

IL_002c:
	{
		G_B5_0 = 2;
		G_B5_1 = G_B4_0;
	}

IL_002d:
	{
		float L_6 = ___pos;
		float L_7 = ___size;
		NullCheck(G_B5_1);
		RectTransform_SetInsetAndSizeFromParentEdge_m2279(G_B5_1, G_B5_0, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutGroup::get_isRootLayoutGroup()
extern const Il2CppType* ILayoutGroup_t446_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" bool LayoutGroup_get_isRootLayoutGroup_m1619 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutGroup_t446_0_0_0_var = il2cpp_codegen_type_from_index(291);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t84 * V_0 = {0};
	{
		Transform_t84 * L_0 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t84 * L_1 = Transform_get_parent_m148(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Transform_t84 * L_2 = V_0;
		bool L_3 = Object_op_Equality_m165(NULL /*static, unused*/, L_2, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		return 1;
	}

IL_001a:
	{
		Transform_t84 * L_4 = Component_get_transform_m145(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t84 * L_5 = Transform_get_parent_m148(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m180(NULL /*static, unused*/, LoadTypeToken(ILayoutGroup_t446_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_5);
		Component_t130 * L_7 = Component_GetComponent_m2280(L_5, L_6, /*hidden argument*/NULL);
		bool L_8 = Object_op_Equality_m165(NULL /*static, unused*/, L_7, (Object_t86 *)NULL, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnRectTransformDimensionsChange()
extern "C" void LayoutGroup_OnRectTransformDimensionsChange_m1620 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnRectTransformDimensionsChange_m414(__this, /*hidden argument*/NULL);
		bool L_0 = LayoutGroup_get_isRootLayoutGroup_m1619(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		LayoutGroup_SetDirty_m1622(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnTransformChildrenChanged()
extern "C" void LayoutGroup_OnTransformChildrenChanged_m1621 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	{
		LayoutGroup_SetDirty_m1622(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetDirty()
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern "C" void LayoutGroup_SetDirty_m1622 (LayoutGroup_t331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		RectTransform_t212 * L_1 = LayoutGroup_get_rectTransform_m1600(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m1635(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::.ctor()
extern "C" void LayoutRebuilder__ctor_m1623 (LayoutRebuilder_t337 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m215(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::.cctor()
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAction_1_t339_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectPool_1_t338_il2cpp_TypeInfo_var;
extern TypeInfo* ReapplyDrivenProperties_t447_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutRebuilder_U3Cs_RebuildersU3Em__6_m1644_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m2281_MethodInfo_var;
extern const MethodInfo* ObjectPool_1__ctor_m2282_MethodInfo_var;
extern const MethodInfo* LayoutRebuilder_ReapplyDrivenProperties_m1627_MethodInfo_var;
extern "C" void LayoutRebuilder__cctor_m1624 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		UnityAction_1_t339_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(292);
		ObjectPool_1_t338_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(293);
		ReapplyDrivenProperties_t447_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(294);
		LayoutRebuilder_U3Cs_RebuildersU3Em__6_m1644_MethodInfo_var = il2cpp_codegen_method_info_from_index(277);
		UnityAction_1__ctor_m2281_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483926);
		ObjectPool_1__ctor_m2282_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483927);
		LayoutRebuilder_ReapplyDrivenProperties_m1627_MethodInfo_var = il2cpp_codegen_method_info_from_index(280);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	{
		UnityAction_1_t339 * L_0 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)LayoutRebuilder_U3Cs_RebuildersU3Em__6_m1644_MethodInfo_var };
		UnityAction_1_t339 * L_2 = (UnityAction_1_t339 *)il2cpp_codegen_object_new (UnityAction_1_t339_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m2281(L_2, NULL, L_1, /*hidden argument*/UnityAction_1__ctor_m2281_MethodInfo_var);
		((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t339 * L_3 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		ObjectPool_1_t338 * L_4 = (ObjectPool_1_t338 *)il2cpp_codegen_object_new (ObjectPool_1_t338_il2cpp_TypeInfo_var);
		ObjectPool_1__ctor_m2282(L_4, (UnityAction_1_t339 *)G_B2_0, L_3, /*hidden argument*/ObjectPool_1__ctor_m2282_MethodInfo_var);
		((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___s_Rebuilders_2 = L_4;
		IntPtr_t L_5 = { (void*)LayoutRebuilder_ReapplyDrivenProperties_m1627_MethodInfo_var };
		ReapplyDrivenProperties_t447 * L_6 = (ReapplyDrivenProperties_t447 *)il2cpp_codegen_object_new (ReapplyDrivenProperties_t447_il2cpp_TypeInfo_var);
		ReapplyDrivenProperties__ctor_m2283(L_6, NULL, L_5, /*hidden argument*/NULL);
		RectTransform_add_reapplyDrivenProperties_m2284(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::Initialize(UnityEngine.RectTransform)
extern "C" void LayoutRebuilder_Initialize_m1625 (LayoutRebuilder_t337 * __this, RectTransform_t212 * ___controller, const MethodInfo* method)
{
	{
		RectTransform_t212 * L_0 = ___controller;
		__this->___m_ToRebuild_0 = L_0;
		RectTransform_t212 * L_1 = ___controller;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 UnityEngine.Object::GetHashCode() */, L_1);
		__this->___m_CachedHashFromTransform_1 = L_2;
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::Clear()
extern "C" void LayoutRebuilder_Clear_m1626 (LayoutRebuilder_t337 * __this, const MethodInfo* method)
{
	{
		__this->___m_ToRebuild_0 = (RectTransform_t212 *)NULL;
		__this->___m_CachedHashFromTransform_1 = 0;
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::ReapplyDrivenProperties(UnityEngine.RectTransform)
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_ReapplyDrivenProperties_m1627 (Object_t * __this /* static, unused */, RectTransform_t212 * ___driven, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t212 * L_0 = ___driven;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m1635(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.UI.LayoutRebuilder::get_transform()
extern "C" Transform_t84 * LayoutRebuilder_get_transform_m1628 (LayoutRebuilder_t337 * __this, const MethodInfo* method)
{
	{
		RectTransform_t212 * L_0 = (__this->___m_ToRebuild_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::IsDestroyed()
extern "C" bool LayoutRebuilder_IsDestroyed_m1629 (LayoutRebuilder_t337 * __this, const MethodInfo* method)
{
	{
		RectTransform_t212 * L_0 = (__this->___m_ToRebuild_0);
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::StripDisabledBehavioursFromList(System.Collections.Generic.List`1<UnityEngine.Component>)
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t340_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__7_m1645_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m2285_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAll_m2286_MethodInfo_var;
extern "C" void LayoutRebuilder_StripDisabledBehavioursFromList_m1630 (Object_t * __this /* static, unused */, List_1_t388 * ___components, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		Predicate_1_t340_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(295);
		LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__7_m1645_MethodInfo_var = il2cpp_codegen_method_info_from_index(281);
		Predicate_1__ctor_m2285_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483930);
		List_1_RemoveAll_m2286_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483931);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t388 * G_B2_0 = {0};
	List_1_t388 * G_B1_0 = {0};
	{
		List_1_t388 * L_0 = ___components;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		Predicate_1_t340 * L_1 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__7_m1645_MethodInfo_var };
		Predicate_1_t340 * L_3 = (Predicate_1_t340 *)il2cpp_codegen_object_new (Predicate_1_t340_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m2285(L_3, NULL, L_2, /*hidden argument*/Predicate_1__ctor_m2285_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		Predicate_1_t340 * L_4 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		NullCheck(G_B2_0);
		List_1_RemoveAll_m2286(G_B2_0, L_4, /*hidden argument*/List_1_RemoveAll_m2286_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::ForceRebuildLayoutImmediate(UnityEngine.RectTransform)
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern const MethodInfo* ObjectPool_1_Get_m2287_MethodInfo_var;
extern const MethodInfo* ObjectPool_1_Release_m2288_MethodInfo_var;
extern "C" void LayoutRebuilder_ForceRebuildLayoutImmediate_m1631 (Object_t * __this /* static, unused */, RectTransform_t212 * ___layoutRoot, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		ObjectPool_1_Get_m2287_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483932);
		ObjectPool_1_Release_m2288_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483933);
		s_Il2CppMethodIntialized = true;
	}
	LayoutRebuilder_t337 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		ObjectPool_1_t338 * L_0 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___s_Rebuilders_2;
		NullCheck(L_0);
		LayoutRebuilder_t337 * L_1 = ObjectPool_1_Get_m2287(L_0, /*hidden argument*/ObjectPool_1_Get_m2287_MethodInfo_var);
		V_0 = L_1;
		LayoutRebuilder_t337 * L_2 = V_0;
		RectTransform_t212 * L_3 = ___layoutRoot;
		NullCheck(L_2);
		LayoutRebuilder_Initialize_m1625(L_2, L_3, /*hidden argument*/NULL);
		LayoutRebuilder_t337 * L_4 = V_0;
		NullCheck(L_4);
		VirtActionInvoker1< int32_t >::Invoke(4 /* System.Void UnityEngine.UI.LayoutRebuilder::Rebuild(UnityEngine.UI.CanvasUpdate) */, L_4, 1);
		ObjectPool_1_t338 * L_5 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___s_Rebuilders_2;
		LayoutRebuilder_t337 * L_6 = V_0;
		NullCheck(L_5);
		ObjectPool_1_Release_m2288(L_5, L_6, /*hidden argument*/ObjectPool_1_Release_m2288_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::Rebuild(UnityEngine.UI.CanvasUpdate)
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAction_1_t341_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CRebuildU3Em__8_m1646_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m2289_MethodInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CRebuildU3Em__9_m1647_MethodInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CRebuildU3Em__A_m1648_MethodInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CRebuildU3Em__B_m1649_MethodInfo_var;
extern "C" void LayoutRebuilder_Rebuild_m1632 (LayoutRebuilder_t337 * __this, int32_t ___executing, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		UnityAction_1_t341_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(296);
		LayoutRebuilder_U3CRebuildU3Em__8_m1646_MethodInfo_var = il2cpp_codegen_method_info_from_index(286);
		UnityAction_1__ctor_m2289_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483935);
		LayoutRebuilder_U3CRebuildU3Em__9_m1647_MethodInfo_var = il2cpp_codegen_method_info_from_index(288);
		LayoutRebuilder_U3CRebuildU3Em__A_m1648_MethodInfo_var = il2cpp_codegen_method_info_from_index(289);
		LayoutRebuilder_U3CRebuildU3Em__B_m1649_MethodInfo_var = il2cpp_codegen_method_info_from_index(290);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	RectTransform_t212 * G_B4_0 = {0};
	LayoutRebuilder_t337 * G_B4_1 = {0};
	RectTransform_t212 * G_B3_0 = {0};
	LayoutRebuilder_t337 * G_B3_1 = {0};
	RectTransform_t212 * G_B6_0 = {0};
	LayoutRebuilder_t337 * G_B6_1 = {0};
	RectTransform_t212 * G_B5_0 = {0};
	LayoutRebuilder_t337 * G_B5_1 = {0};
	RectTransform_t212 * G_B8_0 = {0};
	LayoutRebuilder_t337 * G_B8_1 = {0};
	RectTransform_t212 * G_B7_0 = {0};
	LayoutRebuilder_t337 * G_B7_1 = {0};
	RectTransform_t212 * G_B10_0 = {0};
	LayoutRebuilder_t337 * G_B10_1 = {0};
	RectTransform_t212 * G_B9_0 = {0};
	LayoutRebuilder_t337 * G_B9_1 = {0};
	{
		int32_t L_0 = ___executing;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_000e;
		}
	}
	{
		goto IL_00b7;
	}

IL_000e:
	{
		RectTransform_t212 * L_2 = (__this->___m_ToRebuild_0);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		UnityAction_1_t341 * L_3 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		G_B3_0 = L_2;
		G_B3_1 = __this;
		if (L_3)
		{
			G_B4_0 = L_2;
			G_B4_1 = __this;
			goto IL_002d;
		}
	}
	{
		IntPtr_t L_4 = { (void*)LayoutRebuilder_U3CRebuildU3Em__8_m1646_MethodInfo_var };
		UnityAction_1_t341 * L_5 = (UnityAction_1_t341 *)il2cpp_codegen_object_new (UnityAction_1_t341_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m2289(L_5, NULL, L_4, /*hidden argument*/UnityAction_1__ctor_m2289_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5 = L_5;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_002d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		UnityAction_1_t341 * L_6 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		NullCheck(G_B4_1);
		LayoutRebuilder_PerformLayoutCalculation_m1634(G_B4_1, G_B4_0, L_6, /*hidden argument*/NULL);
		RectTransform_t212 * L_7 = (__this->___m_ToRebuild_0);
		UnityAction_1_t341 * L_8 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		G_B5_0 = L_7;
		G_B5_1 = __this;
		if (L_8)
		{
			G_B6_0 = L_7;
			G_B6_1 = __this;
			goto IL_0056;
		}
	}
	{
		IntPtr_t L_9 = { (void*)LayoutRebuilder_U3CRebuildU3Em__9_m1647_MethodInfo_var };
		UnityAction_1_t341 * L_10 = (UnityAction_1_t341 *)il2cpp_codegen_object_new (UnityAction_1_t341_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m2289(L_10, NULL, L_9, /*hidden argument*/UnityAction_1__ctor_m2289_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6 = L_10;
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		UnityAction_1_t341 * L_11 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		NullCheck(G_B6_1);
		LayoutRebuilder_PerformLayoutControl_m1633(G_B6_1, G_B6_0, L_11, /*hidden argument*/NULL);
		RectTransform_t212 * L_12 = (__this->___m_ToRebuild_0);
		UnityAction_1_t341 * L_13 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7;
		G_B7_0 = L_12;
		G_B7_1 = __this;
		if (L_13)
		{
			G_B8_0 = L_12;
			G_B8_1 = __this;
			goto IL_007f;
		}
	}
	{
		IntPtr_t L_14 = { (void*)LayoutRebuilder_U3CRebuildU3Em__A_m1648_MethodInfo_var };
		UnityAction_1_t341 * L_15 = (UnityAction_1_t341 *)il2cpp_codegen_object_new (UnityAction_1_t341_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m2289(L_15, NULL, L_14, /*hidden argument*/UnityAction_1__ctor_m2289_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7 = L_15;
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
	}

IL_007f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		UnityAction_1_t341 * L_16 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7;
		NullCheck(G_B8_1);
		LayoutRebuilder_PerformLayoutCalculation_m1634(G_B8_1, G_B8_0, L_16, /*hidden argument*/NULL);
		RectTransform_t212 * L_17 = (__this->___m_ToRebuild_0);
		UnityAction_1_t341 * L_18 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache8_8;
		G_B9_0 = L_17;
		G_B9_1 = __this;
		if (L_18)
		{
			G_B10_0 = L_17;
			G_B10_1 = __this;
			goto IL_00a8;
		}
	}
	{
		IntPtr_t L_19 = { (void*)LayoutRebuilder_U3CRebuildU3Em__B_m1649_MethodInfo_var };
		UnityAction_1_t341 * L_20 = (UnityAction_1_t341 *)il2cpp_codegen_object_new (UnityAction_1_t341_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m2289(L_20, NULL, L_19, /*hidden argument*/UnityAction_1__ctor_m2289_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache8_8 = L_20;
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
	}

IL_00a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		UnityAction_1_t341 * L_21 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache8_8;
		NullCheck(G_B10_1);
		LayoutRebuilder_PerformLayoutControl_m1633(G_B10_1, G_B10_0, L_21, /*hidden argument*/NULL);
		goto IL_00b7;
	}

IL_00b7:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutControl(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
extern const Il2CppType* ILayoutController_t448_0_0_0_var;
extern TypeInfo* ListPool_1_t415_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern TypeInfo* ILayoutSelfController_t449_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransform_t212_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m1963_MethodInfo_var;
extern const MethodInfo* UnityAction_1_Invoke_m2290_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m1965_MethodInfo_var;
extern "C" void LayoutRebuilder_PerformLayoutControl_m1633 (LayoutRebuilder_t337 * __this, RectTransform_t212 * ___rect, UnityAction_1_t341 * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutController_t448_0_0_0_var = il2cpp_codegen_type_from_index(297);
		ListPool_1_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(199);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		ILayoutSelfController_t449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(298);
		RectTransform_t212_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(152);
		ListPool_1_Get_m1963_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483841);
		UnityAction_1_Invoke_m2290_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		ListPool_1_Release_m1965_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483842);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t388 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		RectTransform_t212 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t415_il2cpp_TypeInfo_var);
		List_1_t388 * L_2 = ListPool_1_Get_m1963(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m1963_MethodInfo_var);
		V_0 = L_2;
		RectTransform_t212 * L_3 = ___rect;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m180(NULL /*static, unused*/, LoadTypeToken(ILayoutController_t448_0_0_0_var), /*hidden argument*/NULL);
		List_1_t388 * L_5 = V_0;
		NullCheck(L_3);
		Component_GetComponents_m1964(L_3, L_4, L_5, /*hidden argument*/NULL);
		List_1_t388 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		LayoutRebuilder_StripDisabledBehavioursFromList_m1630(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		List_1_t388 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_7);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_00ca;
		}
	}
	{
		V_1 = 0;
		goto IL_005f;
	}

IL_003d:
	{
		List_1_t388 * L_9 = V_0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Component_t130 * L_11 = (Component_t130 *)VirtFuncInvoker1< Component_t130 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_9, L_10);
		if (!((Object_t *)IsInst(L_11, ILayoutSelfController_t449_il2cpp_TypeInfo_var)))
		{
			goto IL_005b;
		}
	}
	{
		UnityAction_1_t341 * L_12 = ___action;
		List_1_t388 * L_13 = V_0;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		Component_t130 * L_15 = (Component_t130 *)VirtFuncInvoker1< Component_t130 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_13, L_14);
		NullCheck(L_12);
		UnityAction_1_Invoke_m2290(L_12, L_15, /*hidden argument*/UnityAction_1_Invoke_m2290_MethodInfo_var);
	}

IL_005b:
	{
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005f:
	{
		int32_t L_17 = V_1;
		List_1_t388 * L_18 = V_0;
		NullCheck(L_18);
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_18);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_003d;
		}
	}
	{
		V_2 = 0;
		goto IL_0094;
	}

IL_0072:
	{
		List_1_t388 * L_20 = V_0;
		int32_t L_21 = V_2;
		NullCheck(L_20);
		Component_t130 * L_22 = (Component_t130 *)VirtFuncInvoker1< Component_t130 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_20, L_21);
		if (((Object_t *)IsInst(L_22, ILayoutSelfController_t449_il2cpp_TypeInfo_var)))
		{
			goto IL_0090;
		}
	}
	{
		UnityAction_1_t341 * L_23 = ___action;
		List_1_t388 * L_24 = V_0;
		int32_t L_25 = V_2;
		NullCheck(L_24);
		Component_t130 * L_26 = (Component_t130 *)VirtFuncInvoker1< Component_t130 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_24, L_25);
		NullCheck(L_23);
		UnityAction_1_Invoke_m2290(L_23, L_26, /*hidden argument*/UnityAction_1_Invoke_m2290_MethodInfo_var);
	}

IL_0090:
	{
		int32_t L_27 = V_2;
		V_2 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0094:
	{
		int32_t L_28 = V_2;
		List_1_t388 * L_29 = V_0;
		NullCheck(L_29);
		int32_t L_30 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_29);
		if ((((int32_t)L_28) < ((int32_t)L_30)))
		{
			goto IL_0072;
		}
	}
	{
		V_3 = 0;
		goto IL_00be;
	}

IL_00a7:
	{
		RectTransform_t212 * L_31 = ___rect;
		int32_t L_32 = V_3;
		NullCheck(L_31);
		Transform_t84 * L_33 = Transform_GetChild_m146(L_31, L_32, /*hidden argument*/NULL);
		UnityAction_1_t341 * L_34 = ___action;
		LayoutRebuilder_PerformLayoutControl_m1633(__this, ((RectTransform_t212 *)IsInstSealed(L_33, RectTransform_t212_il2cpp_TypeInfo_var)), L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_3;
		V_3 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00be:
	{
		int32_t L_36 = V_3;
		RectTransform_t212 * L_37 = ___rect;
		NullCheck(L_37);
		int32_t L_38 = Transform_get_childCount_m1868(L_37, /*hidden argument*/NULL);
		if ((((int32_t)L_36) < ((int32_t)L_38)))
		{
			goto IL_00a7;
		}
	}

IL_00ca:
	{
		List_1_t388 * L_39 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t415_il2cpp_TypeInfo_var);
		ListPool_1_Release_m1965(NULL /*static, unused*/, L_39, /*hidden argument*/ListPool_1_Release_m1965_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutCalculation(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
extern const Il2CppType* ILayoutElement_t389_0_0_0_var;
extern TypeInfo* ListPool_1_t415_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransform_t212_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m1963_MethodInfo_var;
extern const MethodInfo* UnityAction_1_Invoke_m2290_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m1965_MethodInfo_var;
extern "C" void LayoutRebuilder_PerformLayoutCalculation_m1634 (LayoutRebuilder_t337 * __this, RectTransform_t212 * ___rect, UnityAction_1_t341 * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t389_0_0_0_var = il2cpp_codegen_type_from_index(299);
		ListPool_1_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(199);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		RectTransform_t212_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(152);
		ListPool_1_Get_m1963_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483841);
		UnityAction_1_Invoke_m2290_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		ListPool_1_Release_m1965_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483842);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t388 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		RectTransform_t212 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t415_il2cpp_TypeInfo_var);
		List_1_t388 * L_2 = ListPool_1_Get_m1963(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m1963_MethodInfo_var);
		V_0 = L_2;
		RectTransform_t212 * L_3 = ___rect;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m180(NULL /*static, unused*/, LoadTypeToken(ILayoutElement_t389_0_0_0_var), /*hidden argument*/NULL);
		List_1_t388 * L_5 = V_0;
		NullCheck(L_3);
		Component_GetComponents_m1964(L_3, L_4, L_5, /*hidden argument*/NULL);
		List_1_t388 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		LayoutRebuilder_StripDisabledBehavioursFromList_m1630(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		List_1_t388 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_7);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0084;
		}
	}
	{
		V_1 = 0;
		goto IL_0054;
	}

IL_003d:
	{
		RectTransform_t212 * L_9 = ___rect;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Transform_t84 * L_11 = Transform_GetChild_m146(L_9, L_10, /*hidden argument*/NULL);
		UnityAction_1_t341 * L_12 = ___action;
		LayoutRebuilder_PerformLayoutCalculation_m1634(__this, ((RectTransform_t212 *)IsInstSealed(L_11, RectTransform_t212_il2cpp_TypeInfo_var)), L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_14 = V_1;
		RectTransform_t212 * L_15 = ___rect;
		NullCheck(L_15);
		int32_t L_16 = Transform_get_childCount_m1868(L_15, /*hidden argument*/NULL);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_003d;
		}
	}
	{
		V_2 = 0;
		goto IL_0078;
	}

IL_0067:
	{
		UnityAction_1_t341 * L_17 = ___action;
		List_1_t388 * L_18 = V_0;
		int32_t L_19 = V_2;
		NullCheck(L_18);
		Component_t130 * L_20 = (Component_t130 *)VirtFuncInvoker1< Component_t130 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_18, L_19);
		NullCheck(L_17);
		UnityAction_1_Invoke_m2290(L_17, L_20, /*hidden argument*/UnityAction_1_Invoke_m2290_MethodInfo_var);
		int32_t L_21 = V_2;
		V_2 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_22 = V_2;
		List_1_t388 * L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_23);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_0067;
		}
	}

IL_0084:
	{
		List_1_t388 * L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t415_il2cpp_TypeInfo_var);
		ListPool_1_Release_m1965(NULL /*static, unused*/, L_25, /*hidden argument*/ListPool_1_Release_m1965_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutForRebuild(UnityEngine.RectTransform)
extern TypeInfo* RectTransform_t212_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_MarkLayoutForRebuild_m1635 (Object_t * __this /* static, unused */, RectTransform_t212 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t212_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(152);
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t212 * V_0 = {0};
	RectTransform_t212 * V_1 = {0};
	{
		RectTransform_t212 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		RectTransform_t212 * L_2 = ___rect;
		V_0 = L_2;
	}

IL_000f:
	{
		RectTransform_t212 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t84 * L_4 = Transform_get_parent_m148(L_3, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t212 *)IsInstSealed(L_4, RectTransform_t212_il2cpp_TypeInfo_var));
		RectTransform_t212 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		bool L_6 = LayoutRebuilder_ValidLayoutGroup_m1636(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_002b;
		}
	}
	{
		goto IL_0032;
	}

IL_002b:
	{
		RectTransform_t212 * L_7 = V_1;
		V_0 = L_7;
		goto IL_000f;
	}

IL_0032:
	{
		RectTransform_t212 * L_8 = V_0;
		RectTransform_t212 * L_9 = ___rect;
		bool L_10 = Object_op_Equality_m165(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_004a;
		}
	}
	{
		RectTransform_t212 * L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		bool L_12 = LayoutRebuilder_ValidController_m1637(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_004a;
		}
	}
	{
		return;
	}

IL_004a:
	{
		RectTransform_t212 * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutRootForRebuild_m1638(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidLayoutGroup(UnityEngine.RectTransform)
extern const Il2CppType* ILayoutGroup_t446_0_0_0_var;
extern TypeInfo* ListPool_1_t415_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m1963_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m1965_MethodInfo_var;
extern "C" bool LayoutRebuilder_ValidLayoutGroup_m1636 (Object_t * __this /* static, unused */, RectTransform_t212 * ___parent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutGroup_t446_0_0_0_var = il2cpp_codegen_type_from_index(291);
		ListPool_1_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(199);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		ListPool_1_Get_m1963_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483841);
		ListPool_1_Release_m1965_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483842);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t388 * V_0 = {0};
	bool V_1 = false;
	{
		RectTransform_t212 * L_0 = ___parent;
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t415_il2cpp_TypeInfo_var);
		List_1_t388 * L_2 = ListPool_1_Get_m1963(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m1963_MethodInfo_var);
		V_0 = L_2;
		RectTransform_t212 * L_3 = ___parent;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m180(NULL /*static, unused*/, LoadTypeToken(ILayoutGroup_t446_0_0_0_var), /*hidden argument*/NULL);
		List_1_t388 * L_5 = V_0;
		NullCheck(L_3);
		Component_GetComponents_m1964(L_3, L_4, L_5, /*hidden argument*/NULL);
		List_1_t388 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		LayoutRebuilder_StripDisabledBehavioursFromList_m1630(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		List_1_t388 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_7);
		V_1 = ((((int32_t)L_8) > ((int32_t)0))? 1 : 0);
		List_1_t388 * L_9 = V_0;
		ListPool_1_Release_m1965(NULL /*static, unused*/, L_9, /*hidden argument*/ListPool_1_Release_m1965_MethodInfo_var);
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidController(UnityEngine.RectTransform)
extern const Il2CppType* ILayoutController_t448_0_0_0_var;
extern TypeInfo* ListPool_1_t415_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m1963_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m1965_MethodInfo_var;
extern "C" bool LayoutRebuilder_ValidController_m1637 (Object_t * __this /* static, unused */, RectTransform_t212 * ___layoutRoot, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutController_t448_0_0_0_var = il2cpp_codegen_type_from_index(297);
		ListPool_1_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(199);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		ListPool_1_Get_m1963_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483841);
		ListPool_1_Release_m1965_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483842);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t388 * V_0 = {0};
	bool V_1 = false;
	{
		RectTransform_t212 * L_0 = ___layoutRoot;
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t415_il2cpp_TypeInfo_var);
		List_1_t388 * L_2 = ListPool_1_Get_m1963(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m1963_MethodInfo_var);
		V_0 = L_2;
		RectTransform_t212 * L_3 = ___layoutRoot;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m180(NULL /*static, unused*/, LoadTypeToken(ILayoutController_t448_0_0_0_var), /*hidden argument*/NULL);
		List_1_t388 * L_5 = V_0;
		NullCheck(L_3);
		Component_GetComponents_m1964(L_3, L_4, L_5, /*hidden argument*/NULL);
		List_1_t388 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		LayoutRebuilder_StripDisabledBehavioursFromList_m1630(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		List_1_t388 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_7);
		V_1 = ((((int32_t)L_8) > ((int32_t)0))? 1 : 0);
		List_1_t388 * L_9 = V_0;
		ListPool_1_Release_m1965(NULL /*static, unused*/, L_9, /*hidden argument*/ListPool_1_Release_m1965_MethodInfo_var);
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutRootForRebuild(UnityEngine.RectTransform)
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern TypeInfo* CanvasUpdateRegistry_t204_il2cpp_TypeInfo_var;
extern const MethodInfo* ObjectPool_1_Get_m2287_MethodInfo_var;
extern const MethodInfo* ObjectPool_1_Release_m2288_MethodInfo_var;
extern "C" void LayoutRebuilder_MarkLayoutRootForRebuild_m1638 (Object_t * __this /* static, unused */, RectTransform_t212 * ___controller, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		CanvasUpdateRegistry_t204_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(147);
		ObjectPool_1_Get_m2287_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483932);
		ObjectPool_1_Release_m2288_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483933);
		s_Il2CppMethodIntialized = true;
	}
	LayoutRebuilder_t337 * V_0 = {0};
	{
		RectTransform_t212 * L_0 = ___controller;
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		ObjectPool_1_t338 * L_2 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___s_Rebuilders_2;
		NullCheck(L_2);
		LayoutRebuilder_t337 * L_3 = ObjectPool_1_Get_m2287(L_2, /*hidden argument*/ObjectPool_1_Get_m2287_MethodInfo_var);
		V_0 = L_3;
		LayoutRebuilder_t337 * L_4 = V_0;
		RectTransform_t212 * L_5 = ___controller;
		NullCheck(L_4);
		LayoutRebuilder_Initialize_m1625(L_4, L_5, /*hidden argument*/NULL);
		LayoutRebuilder_t337 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CanvasUpdateRegistry_t204_il2cpp_TypeInfo_var);
		bool L_7 = CanvasUpdateRegistry_TryRegisterCanvasElementForLayoutRebuild_m650(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		ObjectPool_1_t338 * L_8 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___s_Rebuilders_2;
		LayoutRebuilder_t337 * L_9 = V_0;
		NullCheck(L_8);
		ObjectPool_1_Release_m2288(L_8, L_9, /*hidden argument*/ObjectPool_1_Release_m2288_MethodInfo_var);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::LayoutComplete()
extern TypeInfo* LayoutRebuilder_t337_il2cpp_TypeInfo_var;
extern const MethodInfo* ObjectPool_1_Release_m2288_MethodInfo_var;
extern "C" void LayoutRebuilder_LayoutComplete_m1639 (LayoutRebuilder_t337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		ObjectPool_1_Release_m2288_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483933);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t337_il2cpp_TypeInfo_var);
		ObjectPool_1_t338 * L_0 = ((LayoutRebuilder_t337_StaticFields*)LayoutRebuilder_t337_il2cpp_TypeInfo_var->static_fields)->___s_Rebuilders_2;
		NullCheck(L_0);
		ObjectPool_1_Release_m2288(L_0, __this, /*hidden argument*/ObjectPool_1_Release_m2288_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::GraphicUpdateComplete()
extern "C" void LayoutRebuilder_GraphicUpdateComplete_m1640 (LayoutRebuilder_t337 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 UnityEngine.UI.LayoutRebuilder::GetHashCode()
extern "C" int32_t LayoutRebuilder_GetHashCode_m1641 (LayoutRebuilder_t337 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_CachedHashFromTransform_1);
		return L_0;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::Equals(System.Object)
extern "C" bool LayoutRebuilder_Equals_m1642 (LayoutRebuilder_t337 * __this, Object_t * ___obj, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___obj;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 UnityEngine.UI.LayoutRebuilder::GetHashCode() */, __this);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.String UnityEngine.UI.LayoutRebuilder::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral143;
extern "C" String_t* LayoutRebuilder_ToString_m1643 (LayoutRebuilder_t337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		_stringLiteral143 = il2cpp_codegen_string_literal_from_index(143);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t212 * L_0 = (__this->___m_ToRebuild_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1725(NULL /*static, unused*/, _stringLiteral143, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::<s_Rebuilders>m__6(UnityEngine.UI.LayoutRebuilder)
extern "C" void LayoutRebuilder_U3Cs_RebuildersU3Em__6_m1644 (Object_t * __this /* static, unused */, LayoutRebuilder_t337 * ___x, const MethodInfo* method)
{
	{
		LayoutRebuilder_t337 * L_0 = ___x;
		NullCheck(L_0);
		LayoutRebuilder_Clear_m1626(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::<StripDisabledBehavioursFromList>m__7(UnityEngine.Component)
extern TypeInfo* Behaviour_t450_il2cpp_TypeInfo_var;
extern "C" bool LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__7_m1645 (Object_t * __this /* static, unused */, Component_t130 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Behaviour_t450_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(300);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Component_t130 * L_0 = ___e;
		if (!((Behaviour_t450 *)IsInstClass(L_0, Behaviour_t450_il2cpp_TypeInfo_var)))
		{
			goto IL_001b;
		}
	}
	{
		Component_t130 * L_1 = ___e;
		NullCheck(((Behaviour_t450 *)CastclassClass(L_1, Behaviour_t450_il2cpp_TypeInfo_var)));
		bool L_2 = Behaviour_get_isActiveAndEnabled_m1759(((Behaviour_t450 *)CastclassClass(L_1, Behaviour_t450_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__8(UnityEngine.Component)
extern TypeInfo* ILayoutElement_t389_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_U3CRebuildU3Em__8_m1646 (Object_t * __this /* static, unused */, Component_t130 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(299);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_t130 * L_0 = ___e;
		NullCheck(((Object_t *)IsInst(L_0, ILayoutElement_t389_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputHorizontal() */, ILayoutElement_t389_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_0, ILayoutElement_t389_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__9(UnityEngine.Component)
extern TypeInfo* ILayoutController_t448_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_U3CRebuildU3Em__9_m1647 (Object_t * __this /* static, unused */, Component_t130 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutController_t448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(297);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_t130 * L_0 = ___e;
		NullCheck(((Object_t *)IsInst(L_0, ILayoutController_t448_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.UI.ILayoutController::SetLayoutHorizontal() */, ILayoutController_t448_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_0, ILayoutController_t448_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__A(UnityEngine.Component)
extern TypeInfo* ILayoutElement_t389_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_U3CRebuildU3Em__A_m1648 (Object_t * __this /* static, unused */, Component_t130 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(299);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_t130 * L_0 = ___e;
		NullCheck(((Object_t *)IsInst(L_0, ILayoutElement_t389_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker0::Invoke(1 /* System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputVertical() */, ILayoutElement_t389_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_0, ILayoutElement_t389_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__B(UnityEngine.Component)
extern TypeInfo* ILayoutController_t448_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_U3CRebuildU3Em__B_m1649 (Object_t * __this /* static, unused */, Component_t130 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutController_t448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(297);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_t130 * L_0 = ___e;
		NullCheck(((Object_t *)IsInst(L_0, ILayoutController_t448_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker0::Invoke(1 /* System.Void UnityEngine.UI.ILayoutController::SetLayoutVertical() */, ILayoutController_t448_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_0, ILayoutController_t448_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetMinSize(UnityEngine.RectTransform,System.Int32)
extern "C" float LayoutUtility_GetMinSize_m1650 (Object_t * __this /* static, unused */, RectTransform_t212 * ___rect, int32_t ___axis, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		RectTransform_t212 * L_1 = ___rect;
		float L_2 = LayoutUtility_GetMinWidth_m1653(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_000d:
	{
		RectTransform_t212 * L_3 = ___rect;
		float L_4 = LayoutUtility_GetMinHeight_m1656(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredSize(UnityEngine.RectTransform,System.Int32)
extern "C" float LayoutUtility_GetPreferredSize_m1651 (Object_t * __this /* static, unused */, RectTransform_t212 * ___rect, int32_t ___axis, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		RectTransform_t212 * L_1 = ___rect;
		float L_2 = LayoutUtility_GetPreferredWidth_m1654(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_000d:
	{
		RectTransform_t212 * L_3 = ___rect;
		float L_4 = LayoutUtility_GetPreferredHeight_m1657(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleSize(UnityEngine.RectTransform,System.Int32)
extern "C" float LayoutUtility_GetFlexibleSize_m1652 (Object_t * __this /* static, unused */, RectTransform_t212 * ___rect, int32_t ___axis, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		RectTransform_t212 * L_1 = ___rect;
		float L_2 = LayoutUtility_GetFlexibleWidth_m1655(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_000d:
	{
		RectTransform_t212 * L_3 = ___rect;
		float L_4 = LayoutUtility_GetFlexibleHeight_m1658(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetMinWidth(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t342_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t343_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetMinWidthU3Em__C_m1661_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2291_MethodInfo_var;
extern "C" float LayoutUtility_GetMinWidth_m1653 (Object_t * __this /* static, unused */, RectTransform_t212 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(301);
		Func_2_t343_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(302);
		LayoutUtility_U3CGetMinWidthU3Em__C_m1661_MethodInfo_var = il2cpp_codegen_method_info_from_index(292);
		Func_2__ctor_m2291_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t212 * G_B2_0 = {0};
	RectTransform_t212 * G_B1_0 = {0};
	{
		RectTransform_t212 * L_0 = ___rect;
		Func_2_t343 * L_1 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_0;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetMinWidthU3Em__C_m1661_MethodInfo_var };
		Func_2_t343 * L_3 = (Func_2_t343 *)il2cpp_codegen_object_new (Func_2_t343_il2cpp_TypeInfo_var);
		Func_2__ctor_m2291(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m2291_MethodInfo_var);
		((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_0 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t343 * L_4 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_0;
		float L_5 = LayoutUtility_GetLayoutProperty_m1659(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredWidth(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t342_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t343_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t394_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetPreferredWidthU3Em__D_m1662_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2291_MethodInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetPreferredWidthU3Em__E_m1663_MethodInfo_var;
extern "C" float LayoutUtility_GetPreferredWidth_m1654 (Object_t * __this /* static, unused */, RectTransform_t212 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(301);
		Func_2_t343_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(302);
		Mathf_t394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		LayoutUtility_U3CGetPreferredWidthU3Em__D_m1662_MethodInfo_var = il2cpp_codegen_method_info_from_index(294);
		Func_2__ctor_m2291_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		LayoutUtility_U3CGetPreferredWidthU3Em__E_m1663_MethodInfo_var = il2cpp_codegen_method_info_from_index(295);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t212 * G_B2_0 = {0};
	RectTransform_t212 * G_B1_0 = {0};
	RectTransform_t212 * G_B4_0 = {0};
	float G_B4_1 = 0.0f;
	RectTransform_t212 * G_B3_0 = {0};
	float G_B3_1 = 0.0f;
	{
		RectTransform_t212 * L_0 = ___rect;
		Func_2_t343 * L_1 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetPreferredWidthU3Em__D_m1662_MethodInfo_var };
		Func_2_t343 * L_3 = (Func_2_t343 *)il2cpp_codegen_object_new (Func_2_t343_il2cpp_TypeInfo_var);
		Func_2__ctor_m2291(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m2291_MethodInfo_var);
		((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t343 * L_4 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		float L_5 = LayoutUtility_GetLayoutProperty_m1659(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		RectTransform_t212 * L_6 = ___rect;
		Func_2_t343 * L_7 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2;
		G_B3_0 = L_6;
		G_B3_1 = L_5;
		if (L_7)
		{
			G_B4_0 = L_6;
			G_B4_1 = L_5;
			goto IL_0041;
		}
	}
	{
		IntPtr_t L_8 = { (void*)LayoutUtility_U3CGetPreferredWidthU3Em__E_m1663_MethodInfo_var };
		Func_2_t343 * L_9 = (Func_2_t343 *)il2cpp_codegen_object_new (Func_2_t343_il2cpp_TypeInfo_var);
		Func_2__ctor_m2291(L_9, NULL, L_8, /*hidden argument*/Func_2__ctor_m2291_MethodInfo_var);
		((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2 = L_9;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0041:
	{
		Func_2_t343 * L_10 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2;
		float L_11 = LayoutUtility_GetLayoutProperty_m1659(NULL /*static, unused*/, G_B4_0, L_10, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Max_m2208(NULL /*static, unused*/, G_B4_1, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleWidth(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t342_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t343_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetFlexibleWidthU3Em__F_m1664_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2291_MethodInfo_var;
extern "C" float LayoutUtility_GetFlexibleWidth_m1655 (Object_t * __this /* static, unused */, RectTransform_t212 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(301);
		Func_2_t343_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(302);
		LayoutUtility_U3CGetFlexibleWidthU3Em__F_m1664_MethodInfo_var = il2cpp_codegen_method_info_from_index(296);
		Func_2__ctor_m2291_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t212 * G_B2_0 = {0};
	RectTransform_t212 * G_B1_0 = {0};
	{
		RectTransform_t212 * L_0 = ___rect;
		Func_2_t343 * L_1 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetFlexibleWidthU3Em__F_m1664_MethodInfo_var };
		Func_2_t343 * L_3 = (Func_2_t343 *)il2cpp_codegen_object_new (Func_2_t343_il2cpp_TypeInfo_var);
		Func_2__ctor_m2291(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m2291_MethodInfo_var);
		((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t343 * L_4 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		float L_5 = LayoutUtility_GetLayoutProperty_m1659(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetMinHeight(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t342_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t343_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetMinHeightU3Em__10_m1665_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2291_MethodInfo_var;
extern "C" float LayoutUtility_GetMinHeight_m1656 (Object_t * __this /* static, unused */, RectTransform_t212 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(301);
		Func_2_t343_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(302);
		LayoutUtility_U3CGetMinHeightU3Em__10_m1665_MethodInfo_var = il2cpp_codegen_method_info_from_index(297);
		Func_2__ctor_m2291_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t212 * G_B2_0 = {0};
	RectTransform_t212 * G_B1_0 = {0};
	{
		RectTransform_t212 * L_0 = ___rect;
		Func_2_t343 * L_1 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetMinHeightU3Em__10_m1665_MethodInfo_var };
		Func_2_t343 * L_3 = (Func_2_t343 *)il2cpp_codegen_object_new (Func_2_t343_il2cpp_TypeInfo_var);
		Func_2__ctor_m2291(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m2291_MethodInfo_var);
		((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t343 * L_4 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		float L_5 = LayoutUtility_GetLayoutProperty_m1659(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredHeight(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t342_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t343_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t394_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetPreferredHeightU3Em__11_m1666_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2291_MethodInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetPreferredHeightU3Em__12_m1667_MethodInfo_var;
extern "C" float LayoutUtility_GetPreferredHeight_m1657 (Object_t * __this /* static, unused */, RectTransform_t212 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(301);
		Func_2_t343_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(302);
		Mathf_t394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		LayoutUtility_U3CGetPreferredHeightU3Em__11_m1666_MethodInfo_var = il2cpp_codegen_method_info_from_index(298);
		Func_2__ctor_m2291_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		LayoutUtility_U3CGetPreferredHeightU3Em__12_m1667_MethodInfo_var = il2cpp_codegen_method_info_from_index(299);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t212 * G_B2_0 = {0};
	RectTransform_t212 * G_B1_0 = {0};
	RectTransform_t212 * G_B4_0 = {0};
	float G_B4_1 = 0.0f;
	RectTransform_t212 * G_B3_0 = {0};
	float G_B3_1 = 0.0f;
	{
		RectTransform_t212 * L_0 = ___rect;
		Func_2_t343 * L_1 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetPreferredHeightU3Em__11_m1666_MethodInfo_var };
		Func_2_t343 * L_3 = (Func_2_t343 *)il2cpp_codegen_object_new (Func_2_t343_il2cpp_TypeInfo_var);
		Func_2__ctor_m2291(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m2291_MethodInfo_var);
		((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t343 * L_4 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		float L_5 = LayoutUtility_GetLayoutProperty_m1659(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		RectTransform_t212 * L_6 = ___rect;
		Func_2_t343 * L_7 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		G_B3_0 = L_6;
		G_B3_1 = L_5;
		if (L_7)
		{
			G_B4_0 = L_6;
			G_B4_1 = L_5;
			goto IL_0041;
		}
	}
	{
		IntPtr_t L_8 = { (void*)LayoutUtility_U3CGetPreferredHeightU3Em__12_m1667_MethodInfo_var };
		Func_2_t343 * L_9 = (Func_2_t343 *)il2cpp_codegen_object_new (Func_2_t343_il2cpp_TypeInfo_var);
		Func_2__ctor_m2291(L_9, NULL, L_8, /*hidden argument*/Func_2__ctor_m2291_MethodInfo_var);
		((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6 = L_9;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0041:
	{
		Func_2_t343 * L_10 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		float L_11 = LayoutUtility_GetLayoutProperty_m1659(NULL /*static, unused*/, G_B4_0, L_10, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t394_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Max_m2208(NULL /*static, unused*/, G_B4_1, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleHeight(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t342_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t343_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetFlexibleHeightU3Em__13_m1668_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2291_MethodInfo_var;
extern "C" float LayoutUtility_GetFlexibleHeight_m1658 (Object_t * __this /* static, unused */, RectTransform_t212 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(301);
		Func_2_t343_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(302);
		LayoutUtility_U3CGetFlexibleHeightU3Em__13_m1668_MethodInfo_var = il2cpp_codegen_method_info_from_index(300);
		Func_2__ctor_m2291_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t212 * G_B2_0 = {0};
	RectTransform_t212 * G_B1_0 = {0};
	{
		RectTransform_t212 * L_0 = ___rect;
		Func_2_t343 * L_1 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetFlexibleHeightU3Em__13_m1668_MethodInfo_var };
		Func_2_t343 * L_3 = (Func_2_t343 *)il2cpp_codegen_object_new (Func_2_t343_il2cpp_TypeInfo_var);
		Func_2__ctor_m2291(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m2291_MethodInfo_var);
		((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t343 * L_4 = ((LayoutUtility_t342_StaticFields*)LayoutUtility_t342_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7;
		float L_5 = LayoutUtility_GetLayoutProperty_m1659(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single)
extern "C" float LayoutUtility_GetLayoutProperty_m1659 (Object_t * __this /* static, unused */, RectTransform_t212 * ___rect, Func_2_t343 * ___property, float ___defaultValue, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		RectTransform_t212 * L_0 = ___rect;
		Func_2_t343 * L_1 = ___property;
		float L_2 = ___defaultValue;
		float L_3 = LayoutUtility_GetLayoutProperty_m1660(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single,UnityEngine.UI.ILayoutElement&)
extern const Il2CppType* ILayoutElement_t389_0_0_0_var;
extern TypeInfo* ListPool_1_t415_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ILayoutElement_t389_il2cpp_TypeInfo_var;
extern TypeInfo* Behaviour_t450_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m1963_MethodInfo_var;
extern const MethodInfo* Func_2_Invoke_m2292_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m1965_MethodInfo_var;
extern "C" float LayoutUtility_GetLayoutProperty_m1660 (Object_t * __this /* static, unused */, RectTransform_t212 * ___rect, Func_2_t343 * ___property, float ___defaultValue, Object_t ** ___source, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t389_0_0_0_var = il2cpp_codegen_type_from_index(299);
		ListPool_1_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(199);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		ILayoutElement_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(299);
		Behaviour_t450_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(300);
		ListPool_1_Get_m1963_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483841);
		Func_2_Invoke_m2292_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483949);
		ListPool_1_Release_m1965_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483842);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	List_1_t388 * V_2 = {0};
	int32_t V_3 = 0;
	Object_t * V_4 = {0};
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	{
		Object_t ** L_0 = ___source;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		RectTransform_t212 * L_1 = ___rect;
		bool L_2 = Object_op_Equality_m165(NULL /*static, unused*/, L_1, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		return (0.0f);
	}

IL_0015:
	{
		float L_3 = ___defaultValue;
		V_0 = L_3;
		V_1 = ((int32_t)-2147483648);
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t415_il2cpp_TypeInfo_var);
		List_1_t388 * L_4 = ListPool_1_Get_m1963(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m1963_MethodInfo_var);
		V_2 = L_4;
		RectTransform_t212 * L_5 = ___rect;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m180(NULL /*static, unused*/, LoadTypeToken(ILayoutElement_t389_0_0_0_var), /*hidden argument*/NULL);
		List_1_t388 * L_7 = V_2;
		NullCheck(L_5);
		Component_GetComponents_m1964(L_5, L_6, L_7, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_00c6;
	}

IL_003b:
	{
		List_1_t388 * L_8 = V_2;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		Component_t130 * L_10 = (Component_t130 *)VirtFuncInvoker1< Component_t130 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_8, L_9);
		V_4 = ((Object_t *)IsInst(L_10, ILayoutElement_t389_il2cpp_TypeInfo_var));
		Object_t * L_11 = V_4;
		if (!((Behaviour_t450 *)IsInstClass(L_11, Behaviour_t450_il2cpp_TypeInfo_var)))
		{
			goto IL_006b;
		}
	}
	{
		Object_t * L_12 = V_4;
		NullCheck(((Behaviour_t450 *)CastclassClass(L_12, Behaviour_t450_il2cpp_TypeInfo_var)));
		bool L_13 = Behaviour_get_isActiveAndEnabled_m1759(((Behaviour_t450 *)CastclassClass(L_12, Behaviour_t450_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_006b;
		}
	}
	{
		goto IL_00c2;
	}

IL_006b:
	{
		Object_t * L_14 = V_4;
		NullCheck(L_14);
		int32_t L_15 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 UnityEngine.UI.ILayoutElement::get_layoutPriority() */, ILayoutElement_t389_il2cpp_TypeInfo_var, L_14);
		V_5 = L_15;
		int32_t L_16 = V_5;
		int32_t L_17 = V_1;
		if ((((int32_t)L_16) >= ((int32_t)L_17)))
		{
			goto IL_0081;
		}
	}
	{
		goto IL_00c2;
	}

IL_0081:
	{
		Func_2_t343 * L_18 = ___property;
		Object_t * L_19 = V_4;
		NullCheck(L_18);
		float L_20 = Func_2_Invoke_m2292(L_18, L_19, /*hidden argument*/Func_2_Invoke_m2292_MethodInfo_var);
		V_6 = L_20;
		float L_21 = V_6;
		if ((!(((float)L_21) < ((float)(0.0f)))))
		{
			goto IL_009c;
		}
	}
	{
		goto IL_00c2;
	}

IL_009c:
	{
		int32_t L_22 = V_5;
		int32_t L_23 = V_1;
		if ((((int32_t)L_22) <= ((int32_t)L_23)))
		{
			goto IL_00b3;
		}
	}
	{
		float L_24 = V_6;
		V_0 = L_24;
		int32_t L_25 = V_5;
		V_1 = L_25;
		Object_t ** L_26 = ___source;
		Object_t * L_27 = V_4;
		*((Object_t **)(L_26)) = (Object_t *)L_27;
		goto IL_00c2;
	}

IL_00b3:
	{
		float L_28 = V_6;
		float L_29 = V_0;
		if ((!(((float)L_28) > ((float)L_29))))
		{
			goto IL_00c2;
		}
	}
	{
		float L_30 = V_6;
		V_0 = L_30;
		Object_t ** L_31 = ___source;
		Object_t * L_32 = V_4;
		*((Object_t **)(L_31)) = (Object_t *)L_32;
	}

IL_00c2:
	{
		int32_t L_33 = V_3;
		V_3 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00c6:
	{
		int32_t L_34 = V_3;
		List_1_t388 * L_35 = V_2;
		NullCheck(L_35);
		int32_t L_36 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_35);
		if ((((int32_t)L_34) < ((int32_t)L_36)))
		{
			goto IL_003b;
		}
	}
	{
		List_1_t388 * L_37 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t415_il2cpp_TypeInfo_var);
		ListPool_1_Release_m1965(NULL /*static, unused*/, L_37, /*hidden argument*/ListPool_1_Release_m1965_MethodInfo_var);
		float L_38 = V_0;
		return L_38;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetMinWidth>m__C(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t389_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetMinWidthU3Em__C_m1661 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(299);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(2 /* System.Single UnityEngine.UI.ILayoutElement::get_minWidth() */, ILayoutElement_t389_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__D(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t389_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetPreferredWidthU3Em__D_m1662 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(299);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(2 /* System.Single UnityEngine.UI.ILayoutElement::get_minWidth() */, ILayoutElement_t389_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__E(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t389_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetPreferredWidthU3Em__E_m1663 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(299);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(3 /* System.Single UnityEngine.UI.ILayoutElement::get_preferredWidth() */, ILayoutElement_t389_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleWidth>m__F(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t389_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetFlexibleWidthU3Em__F_m1664 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(299);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(4 /* System.Single UnityEngine.UI.ILayoutElement::get_flexibleWidth() */, ILayoutElement_t389_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetMinHeight>m__10(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t389_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetMinHeightU3Em__10_m1665 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(299);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(5 /* System.Single UnityEngine.UI.ILayoutElement::get_minHeight() */, ILayoutElement_t389_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__11(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t389_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetPreferredHeightU3Em__11_m1666 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(299);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(5 /* System.Single UnityEngine.UI.ILayoutElement::get_minHeight() */, ILayoutElement_t389_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__12(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t389_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetPreferredHeightU3Em__12_m1667 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(299);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(6 /* System.Single UnityEngine.UI.ILayoutElement::get_preferredHeight() */, ILayoutElement_t389_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleHeight>m__13(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t389_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetFlexibleHeightU3Em__13_m1668 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(299);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(7 /* System.Single UnityEngine.UI.ILayoutElement::get_flexibleHeight() */, ILayoutElement_t389_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void UnityEngine.UI.VerticalLayoutGroup::.ctor()
extern "C" void VerticalLayoutGroup__ctor_m1669 (VerticalLayoutGroup_t344 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup__ctor_m1562(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputHorizontal()
extern "C" void VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1670 (VerticalLayoutGroup_t344 * __this, const MethodInfo* method)
{
	{
		LayoutGroup_CalculateLayoutInputHorizontal_m1602(__this, /*hidden argument*/NULL);
		HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1569(__this, 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputVertical()
extern "C" void VerticalLayoutGroup_CalculateLayoutInputVertical_m1671 (VerticalLayoutGroup_t344 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1569(__this, 1, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutHorizontal()
extern "C" void VerticalLayoutGroup_SetLayoutHorizontal_m1672 (VerticalLayoutGroup_t344 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1570(__this, 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutVertical()
extern "C" void VerticalLayoutGroup_SetLayoutVertical_m1673 (VerticalLayoutGroup_t344 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1570(__this, 1, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::.ctor()
extern TypeInfo* ListPool_1_t451_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t452_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t453_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t454_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t455_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m2293_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m2294_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m2295_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m2296_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m2297_MethodInfo_var;
extern "C" void VertexHelper__ctor_m1674 (VertexHelper_t234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ListPool_1_t451_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(305);
		ListPool_1_t452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(306);
		ListPool_1_t453_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(307);
		ListPool_1_t454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(308);
		ListPool_1_t455_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(309);
		ListPool_1_Get_m2293_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483950);
		ListPool_1_Get_m2294_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483951);
		ListPool_1_Get_m2295_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483952);
		ListPool_1_Get_m2296_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483953);
		ListPool_1_Get_m2297_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483954);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t451_il2cpp_TypeInfo_var);
		List_1_t345 * L_0 = ListPool_1_Get_m2293(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2293_MethodInfo_var);
		__this->___m_Positions_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t452_il2cpp_TypeInfo_var);
		List_1_t346 * L_1 = ListPool_1_Get_m2294(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2294_MethodInfo_var);
		__this->___m_Colors_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t453_il2cpp_TypeInfo_var);
		List_1_t347 * L_2 = ListPool_1_Get_m2295(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2295_MethodInfo_var);
		__this->___m_Uv0S_2 = L_2;
		List_1_t347 * L_3 = ListPool_1_Get_m2295(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2295_MethodInfo_var);
		__this->___m_Uv1S_3 = L_3;
		List_1_t345 * L_4 = ListPool_1_Get_m2293(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2293_MethodInfo_var);
		__this->___m_Normals_4 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t454_il2cpp_TypeInfo_var);
		List_1_t348 * L_5 = ListPool_1_Get_m2296(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2296_MethodInfo_var);
		__this->___m_Tangents_5 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t455_il2cpp_TypeInfo_var);
		List_1_t349 * L_6 = ListPool_1_Get_m2297(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2297_MethodInfo_var);
		__this->___m_Indicies_6 = L_6;
		Object__ctor_m215(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::.ctor(UnityEngine.Mesh)
extern TypeInfo* ListPool_1_t451_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t452_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t453_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t454_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t455_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m2293_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m2294_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m2295_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m2296_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m2297_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m2298_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m2300_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m2302_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m2306_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m2308_MethodInfo_var;
extern "C" void VertexHelper__ctor_m1675 (VertexHelper_t234 * __this, Mesh_t120 * ___m, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ListPool_1_t451_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(305);
		ListPool_1_t452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(306);
		ListPool_1_t453_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(307);
		ListPool_1_t454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(308);
		ListPool_1_t455_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(309);
		ListPool_1_Get_m2293_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483950);
		ListPool_1_Get_m2294_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483951);
		ListPool_1_Get_m2295_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483952);
		ListPool_1_Get_m2296_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483953);
		ListPool_1_Get_m2297_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483954);
		List_1_AddRange_m2298_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483955);
		List_1_AddRange_m2300_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483956);
		List_1_AddRange_m2302_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483957);
		List_1_AddRange_m2306_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483958);
		List_1_AddRange_m2308_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483959);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t451_il2cpp_TypeInfo_var);
		List_1_t345 * L_0 = ListPool_1_Get_m2293(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2293_MethodInfo_var);
		__this->___m_Positions_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t452_il2cpp_TypeInfo_var);
		List_1_t346 * L_1 = ListPool_1_Get_m2294(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2294_MethodInfo_var);
		__this->___m_Colors_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t453_il2cpp_TypeInfo_var);
		List_1_t347 * L_2 = ListPool_1_Get_m2295(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2295_MethodInfo_var);
		__this->___m_Uv0S_2 = L_2;
		List_1_t347 * L_3 = ListPool_1_Get_m2295(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2295_MethodInfo_var);
		__this->___m_Uv1S_3 = L_3;
		List_1_t345 * L_4 = ListPool_1_Get_m2293(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2293_MethodInfo_var);
		__this->___m_Normals_4 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t454_il2cpp_TypeInfo_var);
		List_1_t348 * L_5 = ListPool_1_Get_m2296(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2296_MethodInfo_var);
		__this->___m_Tangents_5 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t455_il2cpp_TypeInfo_var);
		List_1_t349 * L_6 = ListPool_1_Get_m2297(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2297_MethodInfo_var);
		__this->___m_Indicies_6 = L_6;
		Object__ctor_m215(__this, /*hidden argument*/NULL);
		List_1_t345 * L_7 = (__this->___m_Positions_0);
		Mesh_t120 * L_8 = ___m;
		NullCheck(L_8);
		Vector3U5BU5D_t121* L_9 = Mesh_get_vertices_m282(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		List_1_AddRange_m2298(L_7, (Object_t*)(Object_t*)L_9, /*hidden argument*/List_1_AddRange_m2298_MethodInfo_var);
		List_1_t346 * L_10 = (__this->___m_Colors_1);
		Mesh_t120 * L_11 = ___m;
		NullCheck(L_11);
		Color32U5BU5D_t456* L_12 = Mesh_get_colors32_m2299(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		List_1_AddRange_m2300(L_10, (Object_t*)(Object_t*)L_12, /*hidden argument*/List_1_AddRange_m2300_MethodInfo_var);
		List_1_t347 * L_13 = (__this->___m_Uv0S_2);
		Mesh_t120 * L_14 = ___m;
		NullCheck(L_14);
		Vector2U5BU5D_t249* L_15 = Mesh_get_uv_m2301(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		List_1_AddRange_m2302(L_13, (Object_t*)(Object_t*)L_15, /*hidden argument*/List_1_AddRange_m2302_MethodInfo_var);
		List_1_t347 * L_16 = (__this->___m_Uv1S_3);
		Mesh_t120 * L_17 = ___m;
		NullCheck(L_17);
		Vector2U5BU5D_t249* L_18 = Mesh_get_uv2_m2303(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		List_1_AddRange_m2302(L_16, (Object_t*)(Object_t*)L_18, /*hidden argument*/List_1_AddRange_m2302_MethodInfo_var);
		List_1_t345 * L_19 = (__this->___m_Normals_4);
		Mesh_t120 * L_20 = ___m;
		NullCheck(L_20);
		Vector3U5BU5D_t121* L_21 = Mesh_get_normals_m2304(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		List_1_AddRange_m2298(L_19, (Object_t*)(Object_t*)L_21, /*hidden argument*/List_1_AddRange_m2298_MethodInfo_var);
		List_1_t348 * L_22 = (__this->___m_Tangents_5);
		Mesh_t120 * L_23 = ___m;
		NullCheck(L_23);
		Vector4U5BU5D_t457* L_24 = Mesh_get_tangents_m2305(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		List_1_AddRange_m2306(L_22, (Object_t*)(Object_t*)L_24, /*hidden argument*/List_1_AddRange_m2306_MethodInfo_var);
		List_1_t349 * L_25 = (__this->___m_Indicies_6);
		Mesh_t120 * L_26 = ___m;
		NullCheck(L_26);
		Int32U5BU5D_t122* L_27 = Mesh_GetIndices_m2307(L_26, 0, /*hidden argument*/NULL);
		NullCheck(L_25);
		List_1_AddRange_m2308(L_25, (Object_t*)(Object_t*)L_27, /*hidden argument*/List_1_AddRange_m2308_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::.cctor()
extern TypeInfo* VertexHelper_t234_il2cpp_TypeInfo_var;
extern "C" void VertexHelper__cctor_m1676 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VertexHelper_t234_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector4_t350  L_0 = {0};
		Vector4__ctor_m1978(&L_0, (1.0f), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((VertexHelper_t234_StaticFields*)VertexHelper_t234_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7 = L_0;
		Vector3_t12  L_1 = Vector3_get_back_m2309(NULL /*static, unused*/, /*hidden argument*/NULL);
		((VertexHelper_t234_StaticFields*)VertexHelper_t234_il2cpp_TypeInfo_var->static_fields)->___s_DefaultNormal_8 = L_1;
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::Clear()
extern "C" void VertexHelper_Clear_m1677 (VertexHelper_t234 * __this, const MethodInfo* method)
{
	{
		List_1_t345 * L_0 = (__this->___m_Positions_0);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear() */, L_0);
		List_1_t346 * L_1 = (__this->___m_Colors_1);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Clear() */, L_1);
		List_1_t347 * L_2 = (__this->___m_Uv0S_2);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear() */, L_2);
		List_1_t347 * L_3 = (__this->___m_Uv1S_3);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear() */, L_3);
		List_1_t345 * L_4 = (__this->___m_Normals_4);
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear() */, L_4);
		List_1_t348 * L_5 = (__this->___m_Tangents_5);
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Clear() */, L_5);
		List_1_t349 * L_6 = (__this->___m_Indicies_6);
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.Int32>::Clear() */, L_6);
		return;
	}
}
// System.Int32 UnityEngine.UI.VertexHelper::get_currentVertCount()
extern "C" int32_t VertexHelper_get_currentVertCount_m1678 (VertexHelper_t234 * __this, const MethodInfo* method)
{
	{
		List_1_t345 * L_0 = (__this->___m_Positions_0);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count() */, L_0);
		return L_1;
	}
}
// System.Int32 UnityEngine.UI.VertexHelper::get_currentIndexCount()
extern "C" int32_t VertexHelper_get_currentIndexCount_m1679 (VertexHelper_t234 * __this, const MethodInfo* method)
{
	{
		List_1_t349 * L_0 = (__this->___m_Indicies_6);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count() */, L_0);
		return L_1;
	}
}
// System.Void UnityEngine.UI.VertexHelper::PopulateUIVertex(UnityEngine.UIVertex&,System.Int32)
extern "C" void VertexHelper_PopulateUIVertex_m1680 (VertexHelper_t234 * __this, UIVertex_t272 * ___vertex, int32_t ___i, const MethodInfo* method)
{
	{
		UIVertex_t272 * L_0 = ___vertex;
		List_1_t345 * L_1 = (__this->___m_Positions_0);
		int32_t L_2 = ___i;
		NullCheck(L_1);
		Vector3_t12  L_3 = (Vector3_t12 )VirtFuncInvoker1< Vector3_t12 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_1, L_2);
		L_0->___position_0 = L_3;
		UIVertex_t272 * L_4 = ___vertex;
		List_1_t346 * L_5 = (__this->___m_Colors_1);
		int32_t L_6 = ___i;
		NullCheck(L_5);
		Color32_t382  L_7 = (Color32_t382 )VirtFuncInvoker1< Color32_t382 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Color32>::get_Item(System.Int32) */, L_5, L_6);
		L_4->___color_2 = L_7;
		UIVertex_t272 * L_8 = ___vertex;
		List_1_t347 * L_9 = (__this->___m_Uv0S_2);
		int32_t L_10 = ___i;
		NullCheck(L_9);
		Vector2_t171  L_11 = (Vector2_t171 )VirtFuncInvoker1< Vector2_t171 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32) */, L_9, L_10);
		L_8->___uv0_3 = L_11;
		UIVertex_t272 * L_12 = ___vertex;
		List_1_t347 * L_13 = (__this->___m_Uv1S_3);
		int32_t L_14 = ___i;
		NullCheck(L_13);
		Vector2_t171  L_15 = (Vector2_t171 )VirtFuncInvoker1< Vector2_t171 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32) */, L_13, L_14);
		L_12->___uv1_4 = L_15;
		UIVertex_t272 * L_16 = ___vertex;
		List_1_t345 * L_17 = (__this->___m_Normals_4);
		int32_t L_18 = ___i;
		NullCheck(L_17);
		Vector3_t12  L_19 = (Vector3_t12 )VirtFuncInvoker1< Vector3_t12 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_17, L_18);
		L_16->___normal_1 = L_19;
		UIVertex_t272 * L_20 = ___vertex;
		List_1_t348 * L_21 = (__this->___m_Tangents_5);
		int32_t L_22 = ___i;
		NullCheck(L_21);
		Vector4_t350  L_23 = (Vector4_t350 )VirtFuncInvoker1< Vector4_t350 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Item(System.Int32) */, L_21, L_22);
		L_20->___tangent_5 = L_23;
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::SetUIVertex(UnityEngine.UIVertex,System.Int32)
extern "C" void VertexHelper_SetUIVertex_m1681 (VertexHelper_t234 * __this, UIVertex_t272  ___vertex, int32_t ___i, const MethodInfo* method)
{
	{
		List_1_t345 * L_0 = (__this->___m_Positions_0);
		int32_t L_1 = ___i;
		Vector3_t12  L_2 = ((&___vertex)->___position_0);
		NullCheck(L_0);
		VirtActionInvoker2< int32_t, Vector3_t12  >::Invoke(32 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,!0) */, L_0, L_1, L_2);
		List_1_t346 * L_3 = (__this->___m_Colors_1);
		int32_t L_4 = ___i;
		Color32_t382  L_5 = ((&___vertex)->___color_2);
		NullCheck(L_3);
		VirtActionInvoker2< int32_t, Color32_t382  >::Invoke(32 /* System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::set_Item(System.Int32,!0) */, L_3, L_4, L_5);
		List_1_t347 * L_6 = (__this->___m_Uv0S_2);
		int32_t L_7 = ___i;
		Vector2_t171  L_8 = ((&___vertex)->___uv0_3);
		NullCheck(L_6);
		VirtActionInvoker2< int32_t, Vector2_t171  >::Invoke(32 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Item(System.Int32,!0) */, L_6, L_7, L_8);
		List_1_t347 * L_9 = (__this->___m_Uv1S_3);
		int32_t L_10 = ___i;
		Vector2_t171  L_11 = ((&___vertex)->___uv1_4);
		NullCheck(L_9);
		VirtActionInvoker2< int32_t, Vector2_t171  >::Invoke(32 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Item(System.Int32,!0) */, L_9, L_10, L_11);
		List_1_t345 * L_12 = (__this->___m_Normals_4);
		int32_t L_13 = ___i;
		Vector3_t12  L_14 = ((&___vertex)->___normal_1);
		NullCheck(L_12);
		VirtActionInvoker2< int32_t, Vector3_t12  >::Invoke(32 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,!0) */, L_12, L_13, L_14);
		List_1_t348 * L_15 = (__this->___m_Tangents_5);
		int32_t L_16 = ___i;
		Vector4_t350  L_17 = ((&___vertex)->___tangent_5);
		NullCheck(L_15);
		VirtActionInvoker2< int32_t, Vector4_t350  >::Invoke(32 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::set_Item(System.Int32,!0) */, L_15, L_16, L_17);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::FillMesh(UnityEngine.Mesh)
extern TypeInfo* ArgumentException_t442_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral144;
extern "C" void VertexHelper_FillMesh_m1682 (VertexHelper_t234 * __this, Mesh_t120 * ___mesh, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(275);
		_stringLiteral144 = il2cpp_codegen_string_literal_from_index(144);
		s_Il2CppMethodIntialized = true;
	}
	{
		Mesh_t120 * L_0 = ___mesh;
		NullCheck(L_0);
		Mesh_Clear_m1974(L_0, /*hidden argument*/NULL);
		List_1_t345 * L_1 = (__this->___m_Positions_0);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count() */, L_1);
		if ((((int32_t)L_2) < ((int32_t)((int32_t)65000))))
		{
			goto IL_0026;
		}
	}
	{
		ArgumentException_t442 * L_3 = (ArgumentException_t442 *)il2cpp_codegen_object_new (ArgumentException_t442_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2246(L_3, _stringLiteral144, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0026:
	{
		Mesh_t120 * L_4 = ___mesh;
		List_1_t345 * L_5 = (__this->___m_Positions_0);
		NullCheck(L_4);
		Mesh_SetVertices_m2310(L_4, L_5, /*hidden argument*/NULL);
		Mesh_t120 * L_6 = ___mesh;
		List_1_t346 * L_7 = (__this->___m_Colors_1);
		NullCheck(L_6);
		Mesh_SetColors_m2311(L_6, L_7, /*hidden argument*/NULL);
		Mesh_t120 * L_8 = ___mesh;
		List_1_t347 * L_9 = (__this->___m_Uv0S_2);
		NullCheck(L_8);
		Mesh_SetUVs_m2312(L_8, 0, L_9, /*hidden argument*/NULL);
		Mesh_t120 * L_10 = ___mesh;
		List_1_t347 * L_11 = (__this->___m_Uv1S_3);
		NullCheck(L_10);
		Mesh_SetUVs_m2312(L_10, 1, L_11, /*hidden argument*/NULL);
		Mesh_t120 * L_12 = ___mesh;
		List_1_t345 * L_13 = (__this->___m_Normals_4);
		NullCheck(L_12);
		Mesh_SetNormals_m2313(L_12, L_13, /*hidden argument*/NULL);
		Mesh_t120 * L_14 = ___mesh;
		List_1_t348 * L_15 = (__this->___m_Tangents_5);
		NullCheck(L_14);
		Mesh_SetTangents_m2314(L_14, L_15, /*hidden argument*/NULL);
		Mesh_t120 * L_16 = ___mesh;
		List_1_t349 * L_17 = (__this->___m_Indicies_6);
		NullCheck(L_16);
		Mesh_SetTriangles_m2315(L_16, L_17, 0, /*hidden argument*/NULL);
		Mesh_t120 * L_18 = ___mesh;
		NullCheck(L_18);
		Mesh_RecalculateBounds_m2316(L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::Dispose()
extern TypeInfo* ListPool_1_t451_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t452_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t453_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t454_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t455_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Release_m2317_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m2318_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m2319_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m2320_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m2321_MethodInfo_var;
extern "C" void VertexHelper_Dispose_m1683 (VertexHelper_t234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ListPool_1_t451_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(305);
		ListPool_1_t452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(306);
		ListPool_1_t453_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(307);
		ListPool_1_t454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(308);
		ListPool_1_t455_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(309);
		ListPool_1_Release_m2317_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483960);
		ListPool_1_Release_m2318_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483961);
		ListPool_1_Release_m2319_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483962);
		ListPool_1_Release_m2320_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483963);
		ListPool_1_Release_m2321_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483964);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t345 * L_0 = (__this->___m_Positions_0);
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t451_il2cpp_TypeInfo_var);
		ListPool_1_Release_m2317(NULL /*static, unused*/, L_0, /*hidden argument*/ListPool_1_Release_m2317_MethodInfo_var);
		List_1_t346 * L_1 = (__this->___m_Colors_1);
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t452_il2cpp_TypeInfo_var);
		ListPool_1_Release_m2318(NULL /*static, unused*/, L_1, /*hidden argument*/ListPool_1_Release_m2318_MethodInfo_var);
		List_1_t347 * L_2 = (__this->___m_Uv0S_2);
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t453_il2cpp_TypeInfo_var);
		ListPool_1_Release_m2319(NULL /*static, unused*/, L_2, /*hidden argument*/ListPool_1_Release_m2319_MethodInfo_var);
		List_1_t347 * L_3 = (__this->___m_Uv1S_3);
		ListPool_1_Release_m2319(NULL /*static, unused*/, L_3, /*hidden argument*/ListPool_1_Release_m2319_MethodInfo_var);
		List_1_t345 * L_4 = (__this->___m_Normals_4);
		ListPool_1_Release_m2317(NULL /*static, unused*/, L_4, /*hidden argument*/ListPool_1_Release_m2317_MethodInfo_var);
		List_1_t348 * L_5 = (__this->___m_Tangents_5);
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t454_il2cpp_TypeInfo_var);
		ListPool_1_Release_m2320(NULL /*static, unused*/, L_5, /*hidden argument*/ListPool_1_Release_m2320_MethodInfo_var);
		List_1_t349 * L_6 = (__this->___m_Indicies_6);
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t455_il2cpp_TypeInfo_var);
		ListPool_1_Release_m2321(NULL /*static, unused*/, L_6, /*hidden argument*/ListPool_1_Release_m2321_MethodInfo_var);
		__this->___m_Positions_0 = (List_1_t345 *)NULL;
		__this->___m_Colors_1 = (List_1_t346 *)NULL;
		__this->___m_Uv0S_2 = (List_1_t347 *)NULL;
		__this->___m_Uv1S_3 = (List_1_t347 *)NULL;
		__this->___m_Normals_4 = (List_1_t345 *)NULL;
		__this->___m_Tangents_5 = (List_1_t348 *)NULL;
		__this->___m_Indicies_6 = (List_1_t349 *)NULL;
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::AddVert(UnityEngine.Vector3,UnityEngine.Color32,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector3,UnityEngine.Vector4)
extern "C" void VertexHelper_AddVert_m1684 (VertexHelper_t234 * __this, Vector3_t12  ___position, Color32_t382  ___color, Vector2_t171  ___uv0, Vector2_t171  ___uv1, Vector3_t12  ___normal, Vector4_t350  ___tangent, const MethodInfo* method)
{
	{
		List_1_t345 * L_0 = (__this->___m_Positions_0);
		Vector3_t12  L_1 = ___position;
		NullCheck(L_0);
		VirtActionInvoker1< Vector3_t12  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_0, L_1);
		List_1_t346 * L_2 = (__this->___m_Colors_1);
		Color32_t382  L_3 = ___color;
		NullCheck(L_2);
		VirtActionInvoker1< Color32_t382  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Add(!0) */, L_2, L_3);
		List_1_t347 * L_4 = (__this->___m_Uv0S_2);
		Vector2_t171  L_5 = ___uv0;
		NullCheck(L_4);
		VirtActionInvoker1< Vector2_t171  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0) */, L_4, L_5);
		List_1_t347 * L_6 = (__this->___m_Uv1S_3);
		Vector2_t171  L_7 = ___uv1;
		NullCheck(L_6);
		VirtActionInvoker1< Vector2_t171  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0) */, L_6, L_7);
		List_1_t345 * L_8 = (__this->___m_Normals_4);
		Vector3_t12  L_9 = ___normal;
		NullCheck(L_8);
		VirtActionInvoker1< Vector3_t12  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_8, L_9);
		List_1_t348 * L_10 = (__this->___m_Tangents_5);
		Vector4_t350  L_11 = ___tangent;
		NullCheck(L_10);
		VirtActionInvoker1< Vector4_t350  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Add(!0) */, L_10, L_11);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::AddVert(UnityEngine.Vector3,UnityEngine.Color32,UnityEngine.Vector2)
extern TypeInfo* VertexHelper_t234_il2cpp_TypeInfo_var;
extern "C" void VertexHelper_AddVert_m1685 (VertexHelper_t234 * __this, Vector3_t12  ___position, Color32_t382  ___color, Vector2_t171  ___uv0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VertexHelper_t234_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t12  L_0 = ___position;
		Color32_t382  L_1 = ___color;
		Vector2_t171  L_2 = ___uv0;
		Vector2_t171  L_3 = Vector2_get_zero_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VertexHelper_t234_il2cpp_TypeInfo_var);
		Vector3_t12  L_4 = ((VertexHelper_t234_StaticFields*)VertexHelper_t234_il2cpp_TypeInfo_var->static_fields)->___s_DefaultNormal_8;
		Vector4_t350  L_5 = ((VertexHelper_t234_StaticFields*)VertexHelper_t234_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7;
		VertexHelper_AddVert_m1684(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::AddVert(UnityEngine.UIVertex)
extern "C" void VertexHelper_AddVert_m1686 (VertexHelper_t234 * __this, UIVertex_t272  ___v, const MethodInfo* method)
{
	{
		Vector3_t12  L_0 = ((&___v)->___position_0);
		Color32_t382  L_1 = ((&___v)->___color_2);
		Vector2_t171  L_2 = ((&___v)->___uv0_3);
		Vector2_t171  L_3 = ((&___v)->___uv1_4);
		Vector3_t12  L_4 = ((&___v)->___normal_1);
		Vector4_t350  L_5 = ((&___v)->___tangent_5);
		VertexHelper_AddVert_m1684(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::AddTriangle(System.Int32,System.Int32,System.Int32)
extern "C" void VertexHelper_AddTriangle_m1687 (VertexHelper_t234 * __this, int32_t ___idx0, int32_t ___idx1, int32_t ___idx2, const MethodInfo* method)
{
	{
		List_1_t349 * L_0 = (__this->___m_Indicies_6);
		int32_t L_1 = ___idx0;
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_0, L_1);
		List_1_t349 * L_2 = (__this->___m_Indicies_6);
		int32_t L_3 = ___idx1;
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_2, L_3);
		List_1_t349 * L_4 = (__this->___m_Indicies_6);
		int32_t L_5 = ___idx2;
		NullCheck(L_4);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_4, L_5);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::AddUIVertexQuad(UnityEngine.UIVertex[])
extern "C" void VertexHelper_AddUIVertexQuad_m1688 (VertexHelper_t234 * __this, UIVertexU5BU5D_t266* ___verts, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VertexHelper_get_currentVertCount_m1678(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0060;
	}

IL_000e:
	{
		UIVertexU5BU5D_t266* L_1 = ___verts;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector3_t12  L_3 = (((UIVertex_t272 *)(UIVertex_t272 *)SZArrayLdElema(L_1, L_2, sizeof(UIVertex_t272 )))->___position_0);
		UIVertexU5BU5D_t266* L_4 = ___verts;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Color32_t382  L_6 = (((UIVertex_t272 *)(UIVertex_t272 *)SZArrayLdElema(L_4, L_5, sizeof(UIVertex_t272 )))->___color_2);
		UIVertexU5BU5D_t266* L_7 = ___verts;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Vector2_t171  L_9 = (((UIVertex_t272 *)(UIVertex_t272 *)SZArrayLdElema(L_7, L_8, sizeof(UIVertex_t272 )))->___uv0_3);
		UIVertexU5BU5D_t266* L_10 = ___verts;
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		Vector2_t171  L_12 = (((UIVertex_t272 *)(UIVertex_t272 *)SZArrayLdElema(L_10, L_11, sizeof(UIVertex_t272 )))->___uv1_4);
		UIVertexU5BU5D_t266* L_13 = ___verts;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		Vector3_t12  L_15 = (((UIVertex_t272 *)(UIVertex_t272 *)SZArrayLdElema(L_13, L_14, sizeof(UIVertex_t272 )))->___normal_1);
		UIVertexU5BU5D_t266* L_16 = ___verts;
		int32_t L_17 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		Vector4_t350  L_18 = (((UIVertex_t272 *)(UIVertex_t272 *)SZArrayLdElema(L_16, L_17, sizeof(UIVertex_t272 )))->___tangent_5);
		VertexHelper_AddVert_m1684(__this, L_3, L_6, L_9, L_12, L_15, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_1;
		V_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0060:
	{
		int32_t L_20 = V_1;
		if ((((int32_t)L_20) < ((int32_t)4)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_21 = V_0;
		int32_t L_22 = V_0;
		int32_t L_23 = V_0;
		VertexHelper_AddTriangle_m1687(__this, L_21, ((int32_t)((int32_t)L_22+(int32_t)1)), ((int32_t)((int32_t)L_23+(int32_t)2)), /*hidden argument*/NULL);
		int32_t L_24 = V_0;
		int32_t L_25 = V_0;
		int32_t L_26 = V_0;
		VertexHelper_AddTriangle_m1687(__this, ((int32_t)((int32_t)L_24+(int32_t)2)), ((int32_t)((int32_t)L_25+(int32_t)3)), L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::AddUIVertexStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<System.Int32>)
extern const MethodInfo* List_1_AddRange_m2308_MethodInfo_var;
extern "C" void VertexHelper_AddUIVertexStream_m1689 (VertexHelper_t234 * __this, List_1_t380 * ___verts, List_1_t349 * ___indicies, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_AddRange_m2308_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483959);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t380 * L_0 = ___verts;
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		List_1_t380 * L_1 = ___verts;
		List_1_t345 * L_2 = (__this->___m_Positions_0);
		List_1_t346 * L_3 = (__this->___m_Colors_1);
		List_1_t347 * L_4 = (__this->___m_Uv0S_2);
		List_1_t347 * L_5 = (__this->___m_Uv1S_3);
		List_1_t345 * L_6 = (__this->___m_Normals_4);
		List_1_t348 * L_7 = (__this->___m_Tangents_5);
		CanvasRenderer_AddUIVertexStream_m2322(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0030:
	{
		List_1_t349 * L_8 = ___indicies;
		if (!L_8)
		{
			goto IL_0042;
		}
	}
	{
		List_1_t349 * L_9 = (__this->___m_Indicies_6);
		List_1_t349 * L_10 = ___indicies;
		NullCheck(L_9);
		List_1_AddRange_m2308(L_9, L_10, /*hidden argument*/List_1_AddRange_m2308_MethodInfo_var);
	}

IL_0042:
	{
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::AddUIVertexTriangleStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void VertexHelper_AddUIVertexTriangleStream_m1690 (VertexHelper_t234 * __this, List_1_t380 * ___verts, const MethodInfo* method)
{
	{
		List_1_t380 * L_0 = ___verts;
		List_1_t345 * L_1 = (__this->___m_Positions_0);
		List_1_t346 * L_2 = (__this->___m_Colors_1);
		List_1_t347 * L_3 = (__this->___m_Uv0S_2);
		List_1_t347 * L_4 = (__this->___m_Uv1S_3);
		List_1_t345 * L_5 = (__this->___m_Normals_4);
		List_1_t348 * L_6 = (__this->___m_Tangents_5);
		List_1_t349 * L_7 = (__this->___m_Indicies_6);
		CanvasRenderer_SplitUIVertexStreams_m2323(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::GetUIVertexStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void VertexHelper_GetUIVertexStream_m1691 (VertexHelper_t234 * __this, List_1_t380 * ___stream, const MethodInfo* method)
{
	{
		List_1_t380 * L_0 = ___stream;
		List_1_t345 * L_1 = (__this->___m_Positions_0);
		List_1_t346 * L_2 = (__this->___m_Colors_1);
		List_1_t347 * L_3 = (__this->___m_Uv0S_2);
		List_1_t347 * L_4 = (__this->___m_Uv1S_3);
		List_1_t345 * L_5 = (__this->___m_Normals_4);
		List_1_t348 * L_6 = (__this->___m_Tangents_5);
		List_1_t349 * L_7 = (__this->___m_Indicies_6);
		CanvasRenderer_CreateUIVertexStream_m2324(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BaseVertexEffect::.ctor()
extern "C" void BaseVertexEffect__ctor_m1692 (BaseVertexEffect_t351 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m215(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BaseMeshEffect::.ctor()
extern "C" void BaseMeshEffect__ctor_m1693 (BaseMeshEffect_t352 * __this, const MethodInfo* method)
{
	{
		UIBehaviour__ctor_m407(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::get_graphic()
extern const MethodInfo* Component_GetComponent_TisGraphic_t228_m2142_MethodInfo_var;
extern "C" Graphic_t228 * BaseMeshEffect_get_graphic_m1694 (BaseMeshEffect_t352 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisGraphic_t228_m2142_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483878);
		s_Il2CppMethodIntialized = true;
	}
	{
		Graphic_t228 * L_0 = (__this->___m_Graphic_2);
		bool L_1 = Object_op_Equality_m165(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Graphic_t228 * L_2 = Component_GetComponent_TisGraphic_t228_m2142(__this, /*hidden argument*/Component_GetComponent_TisGraphic_t228_m2142_MethodInfo_var);
		__this->___m_Graphic_2 = L_2;
	}

IL_001d:
	{
		Graphic_t228 * L_3 = (__this->___m_Graphic_2);
		return L_3;
	}
}
// System.Void UnityEngine.UI.BaseMeshEffect::OnEnable()
extern "C" void BaseMeshEffect_OnEnable_m1695 (BaseMeshEffect_t352 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m409(__this, /*hidden argument*/NULL);
		Graphic_t228 * L_0 = BaseMeshEffect_get_graphic_m1694(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m150(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Graphic_t228 * L_2 = BaseMeshEffect_get_graphic_m1694(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_2);
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.UI.BaseMeshEffect::OnDisable()
extern "C" void BaseMeshEffect_OnDisable_m1696 (BaseMeshEffect_t352 * __this, const MethodInfo* method)
{
	{
		Graphic_t228 * L_0 = BaseMeshEffect_get_graphic_m1694(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m150(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Graphic_t228 * L_2 = BaseMeshEffect_get_graphic_m1694(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_2);
	}

IL_001c:
	{
		UIBehaviour_OnDisable_m411(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BaseMeshEffect::OnDidApplyAnimationProperties()
extern "C" void BaseMeshEffect_OnDidApplyAnimationProperties_m1697 (BaseMeshEffect_t352 * __this, const MethodInfo* method)
{
	{
		Graphic_t228 * L_0 = BaseMeshEffect_get_graphic_m1694(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m150(NULL /*static, unused*/, L_0, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Graphic_t228 * L_2 = BaseMeshEffect_get_graphic_m1694(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_2);
	}

IL_001c:
	{
		UIBehaviour_OnDidApplyAnimationProperties_m417(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BaseMeshEffect::ModifyMesh(UnityEngine.Mesh)
extern TypeInfo* VertexHelper_t234_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t111_il2cpp_TypeInfo_var;
extern "C" void BaseMeshEffect_ModifyMesh_m1698 (BaseMeshEffect_t352 * __this, Mesh_t120 * ___mesh, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VertexHelper_t234_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		IDisposable_t111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	VertexHelper_t234 * V_0 = {0};
	Exception_t107 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t107 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Mesh_t120 * L_0 = ___mesh;
		VertexHelper_t234 * L_1 = (VertexHelper_t234 *)il2cpp_codegen_object_new (VertexHelper_t234_il2cpp_TypeInfo_var);
		VertexHelper__ctor_m1675(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		VertexHelper_t234 * L_2 = V_0;
		VirtActionInvoker1< VertexHelper_t234 * >::Invoke(19 /* System.Void UnityEngine.UI.BaseMeshEffect::ModifyMesh(UnityEngine.UI.VertexHelper) */, __this, L_2);
		VertexHelper_t234 * L_3 = V_0;
		Mesh_t120 * L_4 = ___mesh;
		NullCheck(L_3);
		VertexHelper_FillMesh_m1682(L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x27, FINALLY_001a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t107 *)e.ex;
		goto FINALLY_001a;
	}

FINALLY_001a:
	{ // begin finally (depth: 1)
		{
			VertexHelper_t234 * L_5 = V_0;
			if (!L_5)
			{
				goto IL_0026;
			}
		}

IL_0020:
		{
			VertexHelper_t234 * L_6 = V_0;
			NullCheck(L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t111_il2cpp_TypeInfo_var, L_6);
		}

IL_0026:
		{
			IL2CPP_END_FINALLY(26)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(26)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t107 *)
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.UI.Outline::.ctor()
extern "C" void Outline__ctor_m1699 (Outline_t353 * __this, const MethodInfo* method)
{
	{
		Shadow__ctor_m1703(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Outline::ModifyMesh(UnityEngine.UI.VertexHelper)
extern TypeInfo* ListPool_1_t458_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m2325_MethodInfo_var;
extern const MethodInfo* List_1_get_Capacity_m2326_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m2327_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m2328_MethodInfo_var;
extern "C" void Outline_ModifyMesh_m1700 (Outline_t353 * __this, VertexHelper_t234 * ___vh, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ListPool_1_t458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(310);
		ListPool_1_Get_m2325_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483965);
		List_1_get_Capacity_m2326_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483966);
		List_1_set_Capacity_m2327_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483967);
		ListPool_1_Release_m2328_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483968);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t380 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Vector2_t171  V_4 = {0};
	Vector2_t171  V_5 = {0};
	Vector2_t171  V_6 = {0};
	Vector2_t171  V_7 = {0};
	Vector2_t171  V_8 = {0};
	Vector2_t171  V_9 = {0};
	Vector2_t171  V_10 = {0};
	Vector2_t171  V_11 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t458_il2cpp_TypeInfo_var);
		List_1_t380 * L_1 = ListPool_1_Get_m2325(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2325_MethodInfo_var);
		V_0 = L_1;
		VertexHelper_t234 * L_2 = ___vh;
		List_1_t380 * L_3 = V_0;
		NullCheck(L_2);
		VertexHelper_GetUIVertexStream_m1691(L_2, L_3, /*hidden argument*/NULL);
		List_1_t380 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_4);
		V_1 = ((int32_t)((int32_t)L_5*(int32_t)5));
		List_1_t380 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Capacity_m2326(L_6, /*hidden argument*/List_1_get_Capacity_m2326_MethodInfo_var);
		int32_t L_8 = V_1;
		if ((((int32_t)L_7) >= ((int32_t)L_8)))
		{
			goto IL_0035;
		}
	}
	{
		List_1_t380 * L_9 = V_0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		List_1_set_Capacity_m2327(L_9, L_10, /*hidden argument*/List_1_set_Capacity_m2327_MethodInfo_var);
	}

IL_0035:
	{
		V_2 = 0;
		List_1_t380 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_11);
		V_3 = L_12;
		List_1_t380 * L_13 = V_0;
		Color_t77  L_14 = Shadow_get_effectColor_m1704(__this, /*hidden argument*/NULL);
		Color32_t382  L_15 = Color32_op_Implicit_m1979(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_2;
		List_1_t380 * L_17 = V_0;
		NullCheck(L_17);
		int32_t L_18 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_17);
		Vector2_t171  L_19 = Shadow_get_effectDistance_m1706(__this, /*hidden argument*/NULL);
		V_4 = L_19;
		float L_20 = ((&V_4)->___x_1);
		Vector2_t171  L_21 = Shadow_get_effectDistance_m1706(__this, /*hidden argument*/NULL);
		V_5 = L_21;
		float L_22 = ((&V_5)->___y_2);
		Shadow_ApplyShadowZeroAlloc_m1710(__this, L_13, L_15, L_16, L_18, L_20, L_22, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		V_2 = L_23;
		List_1_t380 * L_24 = V_0;
		NullCheck(L_24);
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_24);
		V_3 = L_25;
		List_1_t380 * L_26 = V_0;
		Color_t77  L_27 = Shadow_get_effectColor_m1704(__this, /*hidden argument*/NULL);
		Color32_t382  L_28 = Color32_op_Implicit_m1979(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		int32_t L_29 = V_2;
		List_1_t380 * L_30 = V_0;
		NullCheck(L_30);
		int32_t L_31 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_30);
		Vector2_t171  L_32 = Shadow_get_effectDistance_m1706(__this, /*hidden argument*/NULL);
		V_6 = L_32;
		float L_33 = ((&V_6)->___x_1);
		Vector2_t171  L_34 = Shadow_get_effectDistance_m1706(__this, /*hidden argument*/NULL);
		V_7 = L_34;
		float L_35 = ((&V_7)->___y_2);
		Shadow_ApplyShadowZeroAlloc_m1710(__this, L_26, L_28, L_29, L_31, L_33, ((-L_35)), /*hidden argument*/NULL);
		int32_t L_36 = V_3;
		V_2 = L_36;
		List_1_t380 * L_37 = V_0;
		NullCheck(L_37);
		int32_t L_38 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_37);
		V_3 = L_38;
		List_1_t380 * L_39 = V_0;
		Color_t77  L_40 = Shadow_get_effectColor_m1704(__this, /*hidden argument*/NULL);
		Color32_t382  L_41 = Color32_op_Implicit_m1979(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		int32_t L_42 = V_2;
		List_1_t380 * L_43 = V_0;
		NullCheck(L_43);
		int32_t L_44 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_43);
		Vector2_t171  L_45 = Shadow_get_effectDistance_m1706(__this, /*hidden argument*/NULL);
		V_8 = L_45;
		float L_46 = ((&V_8)->___x_1);
		Vector2_t171  L_47 = Shadow_get_effectDistance_m1706(__this, /*hidden argument*/NULL);
		V_9 = L_47;
		float L_48 = ((&V_9)->___y_2);
		Shadow_ApplyShadowZeroAlloc_m1710(__this, L_39, L_41, L_42, L_44, ((-L_46)), L_48, /*hidden argument*/NULL);
		int32_t L_49 = V_3;
		V_2 = L_49;
		List_1_t380 * L_50 = V_0;
		NullCheck(L_50);
		int32_t L_51 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_50);
		V_3 = L_51;
		List_1_t380 * L_52 = V_0;
		Color_t77  L_53 = Shadow_get_effectColor_m1704(__this, /*hidden argument*/NULL);
		Color32_t382  L_54 = Color32_op_Implicit_m1979(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		int32_t L_55 = V_2;
		List_1_t380 * L_56 = V_0;
		NullCheck(L_56);
		int32_t L_57 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_56);
		Vector2_t171  L_58 = Shadow_get_effectDistance_m1706(__this, /*hidden argument*/NULL);
		V_10 = L_58;
		float L_59 = ((&V_10)->___x_1);
		Vector2_t171  L_60 = Shadow_get_effectDistance_m1706(__this, /*hidden argument*/NULL);
		V_11 = L_60;
		float L_61 = ((&V_11)->___y_2);
		Shadow_ApplyShadowZeroAlloc_m1710(__this, L_52, L_54, L_55, L_57, ((-L_59)), ((-L_61)), /*hidden argument*/NULL);
		VertexHelper_t234 * L_62 = ___vh;
		NullCheck(L_62);
		VertexHelper_Clear_m1677(L_62, /*hidden argument*/NULL);
		VertexHelper_t234 * L_63 = ___vh;
		List_1_t380 * L_64 = V_0;
		NullCheck(L_63);
		VertexHelper_AddUIVertexTriangleStream_m1690(L_63, L_64, /*hidden argument*/NULL);
		List_1_t380 * L_65 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t458_il2cpp_TypeInfo_var);
		ListPool_1_Release_m2328(NULL /*static, unused*/, L_65, /*hidden argument*/ListPool_1_Release_m2328_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.PositionAsUV1::.ctor()
extern "C" void PositionAsUV1__ctor_m1701 (PositionAsUV1_t355 * __this, const MethodInfo* method)
{
	{
		BaseMeshEffect__ctor_m1693(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.PositionAsUV1::ModifyMesh(UnityEngine.UI.VertexHelper)
extern TypeInfo* UIVertex_t272_il2cpp_TypeInfo_var;
extern "C" void PositionAsUV1_ModifyMesh_m1702 (PositionAsUV1_t355 * __this, VertexHelper_t234 * ___vh, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIVertex_t272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(215);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t272  V_0 = {0};
	int32_t V_1 = 0;
	{
		Initobj (UIVertex_t272_il2cpp_TypeInfo_var, (&V_0));
		V_1 = 0;
		goto IL_0048;
	}

IL_000f:
	{
		VertexHelper_t234 * L_0 = ___vh;
		int32_t L_1 = V_1;
		NullCheck(L_0);
		VertexHelper_PopulateUIVertex_m1680(L_0, (&V_0), L_1, /*hidden argument*/NULL);
		Vector3_t12 * L_2 = &((&V_0)->___position_0);
		float L_3 = (L_2->___x_1);
		Vector3_t12 * L_4 = &((&V_0)->___position_0);
		float L_5 = (L_4->___y_2);
		Vector2_t171  L_6 = {0};
		Vector2__ctor_m1764(&L_6, L_3, L_5, /*hidden argument*/NULL);
		(&V_0)->___uv1_4 = L_6;
		VertexHelper_t234 * L_7 = ___vh;
		UIVertex_t272  L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_7);
		VertexHelper_SetUIVertex_m1681(L_7, L_8, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_11 = V_1;
		VertexHelper_t234 * L_12 = ___vh;
		NullCheck(L_12);
		int32_t L_13 = VertexHelper_get_currentVertCount_m1678(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.Shadow::.ctor()
extern "C" void Shadow__ctor_m1703 (Shadow_t354 * __this, const MethodInfo* method)
{
	{
		Color_t77  L_0 = {0};
		Color__ctor_m1860(&L_0, (0.0f), (0.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		__this->___m_EffectColor_3 = L_0;
		Vector2_t171  L_1 = {0};
		Vector2__ctor_m1764(&L_1, (1.0f), (-1.0f), /*hidden argument*/NULL);
		__this->___m_EffectDistance_4 = L_1;
		__this->___m_UseGraphicAlpha_5 = 1;
		BaseMeshEffect__ctor_m1693(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color UnityEngine.UI.Shadow::get_effectColor()
extern "C" Color_t77  Shadow_get_effectColor_m1704 (Shadow_t354 * __this, const MethodInfo* method)
{
	{
		Color_t77  L_0 = (__this->___m_EffectColor_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Shadow::set_effectColor(UnityEngine.Color)
extern "C" void Shadow_set_effectColor_m1705 (Shadow_t354 * __this, Color_t77  ___value, const MethodInfo* method)
{
	{
		Color_t77  L_0 = ___value;
		__this->___m_EffectColor_3 = L_0;
		Graphic_t228 * L_1 = BaseMeshEffect_get_graphic_m1694(__this, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m150(NULL /*static, unused*/, L_1, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Graphic_t228 * L_3 = BaseMeshEffect_get_graphic_m1694(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_3);
	}

IL_0023:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.UI.Shadow::get_effectDistance()
extern "C" Vector2_t171  Shadow_get_effectDistance_m1706 (Shadow_t354 * __this, const MethodInfo* method)
{
	{
		Vector2_t171  L_0 = (__this->___m_EffectDistance_4);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Shadow::set_effectDistance(UnityEngine.Vector2)
extern "C" void Shadow_set_effectDistance_m1707 (Shadow_t354 * __this, Vector2_t171  ___value, const MethodInfo* method)
{
	{
		float L_0 = ((&___value)->___x_1);
		if ((!(((float)L_0) > ((float)(600.0f)))))
		{
			goto IL_001d;
		}
	}
	{
		(&___value)->___x_1 = (600.0f);
	}

IL_001d:
	{
		float L_1 = ((&___value)->___x_1);
		if ((!(((float)L_1) < ((float)(-600.0f)))))
		{
			goto IL_003a;
		}
	}
	{
		(&___value)->___x_1 = (-600.0f);
	}

IL_003a:
	{
		float L_2 = ((&___value)->___y_2);
		if ((!(((float)L_2) > ((float)(600.0f)))))
		{
			goto IL_0057;
		}
	}
	{
		(&___value)->___y_2 = (600.0f);
	}

IL_0057:
	{
		float L_3 = ((&___value)->___y_2);
		if ((!(((float)L_3) < ((float)(-600.0f)))))
		{
			goto IL_0074;
		}
	}
	{
		(&___value)->___y_2 = (-600.0f);
	}

IL_0074:
	{
		Vector2_t171  L_4 = (__this->___m_EffectDistance_4);
		Vector2_t171  L_5 = ___value;
		bool L_6 = Vector2_op_Equality_m2329(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0086;
		}
	}
	{
		return;
	}

IL_0086:
	{
		Vector2_t171  L_7 = ___value;
		__this->___m_EffectDistance_4 = L_7;
		Graphic_t228 * L_8 = BaseMeshEffect_get_graphic_m1694(__this, /*hidden argument*/NULL);
		bool L_9 = Object_op_Inequality_m150(NULL /*static, unused*/, L_8, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00a9;
		}
	}
	{
		Graphic_t228 * L_10 = BaseMeshEffect_get_graphic_m1694(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_10);
	}

IL_00a9:
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.Shadow::get_useGraphicAlpha()
extern "C" bool Shadow_get_useGraphicAlpha_m1708 (Shadow_t354 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_UseGraphicAlpha_5);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Shadow::set_useGraphicAlpha(System.Boolean)
extern "C" void Shadow_set_useGraphicAlpha_m1709 (Shadow_t354 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_UseGraphicAlpha_5 = L_0;
		Graphic_t228 * L_1 = BaseMeshEffect_get_graphic_m1694(__this, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m150(NULL /*static, unused*/, L_1, (Object_t86 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Graphic_t228 * L_3 = BaseMeshEffect_get_graphic_m1694(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(23 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_3);
	}

IL_0023:
	{
		return;
	}
}
// System.Void UnityEngine.UI.Shadow::ApplyShadowZeroAlloc(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color32,System.Int32,System.Int32,System.Single,System.Single)
extern const MethodInfo* List_1_get_Capacity_m2326_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m2327_MethodInfo_var;
extern "C" void Shadow_ApplyShadowZeroAlloc_m1710 (Shadow_t354 * __this, List_1_t380 * ___verts, Color32_t382  ___color, int32_t ___start, int32_t ___end, float ___x, float ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_get_Capacity_m2326_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483966);
		List_1_set_Capacity_m2327_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483967);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t272  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t12  V_3 = {0};
	Color32_t382  V_4 = {0};
	UIVertex_t272  V_5 = {0};
	{
		List_1_t380 * L_0 = ___verts;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_0);
		V_1 = ((int32_t)((int32_t)L_1*(int32_t)2));
		List_1_t380 * L_2 = ___verts;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Capacity_m2326(L_2, /*hidden argument*/List_1_get_Capacity_m2326_MethodInfo_var);
		int32_t L_4 = V_1;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_001c;
		}
	}
	{
		List_1_t380 * L_5 = ___verts;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		List_1_set_Capacity_m2327(L_5, L_6, /*hidden argument*/List_1_set_Capacity_m2327_MethodInfo_var);
	}

IL_001c:
	{
		int32_t L_7 = ___start;
		V_2 = L_7;
		goto IL_00b0;
	}

IL_0023:
	{
		List_1_t380 * L_8 = ___verts;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		UIVertex_t272  L_10 = (UIVertex_t272 )VirtFuncInvoker1< UIVertex_t272 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_8, L_9);
		V_0 = L_10;
		List_1_t380 * L_11 = ___verts;
		UIVertex_t272  L_12 = V_0;
		NullCheck(L_11);
		VirtActionInvoker1< UIVertex_t272  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(!0) */, L_11, L_12);
		Vector3_t12  L_13 = ((&V_0)->___position_0);
		V_3 = L_13;
		Vector3_t12 * L_14 = (&V_3);
		float L_15 = (L_14->___x_1);
		float L_16 = ___x;
		L_14->___x_1 = ((float)((float)L_15+(float)L_16));
		Vector3_t12 * L_17 = (&V_3);
		float L_18 = (L_17->___y_2);
		float L_19 = ___y;
		L_17->___y_2 = ((float)((float)L_18+(float)L_19));
		Vector3_t12  L_20 = V_3;
		(&V_0)->___position_0 = L_20;
		Color32_t382  L_21 = ___color;
		V_4 = L_21;
		bool L_22 = (__this->___m_UseGraphicAlpha_5);
		if (!L_22)
		{
			goto IL_009b;
		}
	}
	{
		uint8_t L_23 = ((&V_4)->___a_3);
		List_1_t380 * L_24 = ___verts;
		int32_t L_25 = V_2;
		NullCheck(L_24);
		UIVertex_t272  L_26 = (UIVertex_t272 )VirtFuncInvoker1< UIVertex_t272 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_24, L_25);
		V_5 = L_26;
		Color32_t382 * L_27 = &((&V_5)->___color_2);
		uint8_t L_28 = (L_27->___a_3);
		(&V_4)->___a_3 = (((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_23*(int32_t)L_28))/(int32_t)((int32_t)255))))));
	}

IL_009b:
	{
		Color32_t382  L_29 = V_4;
		(&V_0)->___color_2 = L_29;
		List_1_t380 * L_30 = ___verts;
		int32_t L_31 = V_2;
		UIVertex_t272  L_32 = V_0;
		NullCheck(L_30);
		VirtActionInvoker2< int32_t, UIVertex_t272  >::Invoke(32 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,!0) */, L_30, L_31, L_32);
		int32_t L_33 = V_2;
		V_2 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b0:
	{
		int32_t L_34 = V_2;
		int32_t L_35 = ___end;
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_0023;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.Shadow::ApplyShadow(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color32,System.Int32,System.Int32,System.Single,System.Single)
extern const MethodInfo* List_1_get_Capacity_m2326_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m2327_MethodInfo_var;
extern "C" void Shadow_ApplyShadow_m1711 (Shadow_t354 * __this, List_1_t380 * ___verts, Color32_t382  ___color, int32_t ___start, int32_t ___end, float ___x, float ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_get_Capacity_m2326_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483966);
		List_1_set_Capacity_m2327_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483967);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t380 * L_0 = ___verts;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_0);
		V_0 = ((int32_t)((int32_t)L_1*(int32_t)2));
		List_1_t380 * L_2 = ___verts;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Capacity_m2326(L_2, /*hidden argument*/List_1_get_Capacity_m2326_MethodInfo_var);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_001c;
		}
	}
	{
		List_1_t380 * L_5 = ___verts;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		List_1_set_Capacity_m2327(L_5, L_6, /*hidden argument*/List_1_set_Capacity_m2327_MethodInfo_var);
	}

IL_001c:
	{
		List_1_t380 * L_7 = ___verts;
		Color32_t382  L_8 = ___color;
		int32_t L_9 = ___start;
		int32_t L_10 = ___end;
		float L_11 = ___x;
		float L_12 = ___y;
		Shadow_ApplyShadowZeroAlloc_m1710(__this, L_7, L_8, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Shadow::ModifyMesh(UnityEngine.UI.VertexHelper)
extern TypeInfo* ListPool_1_t458_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m2325_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m2328_MethodInfo_var;
extern "C" void Shadow_ModifyMesh_m1712 (Shadow_t354 * __this, VertexHelper_t234 * ___vh, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ListPool_1_t458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(310);
		ListPool_1_Get_m2325_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483965);
		ListPool_1_Release_m2328_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483968);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t380 * V_0 = {0};
	Vector2_t171  V_1 = {0};
	Vector2_t171  V_2 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t458_il2cpp_TypeInfo_var);
		List_1_t380 * L_1 = ListPool_1_Get_m2325(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m2325_MethodInfo_var);
		V_0 = L_1;
		VertexHelper_t234 * L_2 = ___vh;
		List_1_t380 * L_3 = V_0;
		NullCheck(L_2);
		VertexHelper_GetUIVertexStream_m1691(L_2, L_3, /*hidden argument*/NULL);
		List_1_t380 * L_4 = V_0;
		Color_t77  L_5 = Shadow_get_effectColor_m1704(__this, /*hidden argument*/NULL);
		Color32_t382  L_6 = Color32_op_Implicit_m1979(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		List_1_t380 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_7);
		Vector2_t171  L_9 = Shadow_get_effectDistance_m1706(__this, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = ((&V_1)->___x_1);
		Vector2_t171  L_11 = Shadow_get_effectDistance_m1706(__this, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = ((&V_2)->___y_2);
		Shadow_ApplyShadow_m1711(__this, L_4, L_6, 0, L_8, L_10, L_12, /*hidden argument*/NULL);
		VertexHelper_t234 * L_13 = ___vh;
		NullCheck(L_13);
		VertexHelper_Clear_m1677(L_13, /*hidden argument*/NULL);
		VertexHelper_t234 * L_14 = ___vh;
		List_1_t380 * L_15 = V_0;
		NullCheck(L_14);
		VertexHelper_AddUIVertexTriangleStream_m1690(L_14, L_15, /*hidden argument*/NULL);
		List_1_t380 * L_16 = V_0;
		ListPool_1_Release_m2328(NULL /*static, unused*/, L_16, /*hidden argument*/ListPool_1_Release_m2328_MethodInfo_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
