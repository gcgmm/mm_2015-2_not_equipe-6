﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.Guid>
struct GenericComparer_1_t2165;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.Guid>::.ctor()
extern "C" void GenericComparer_1__ctor_m12778_gshared (GenericComparer_1_t2165 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m12778(__this, method) (( void (*) (GenericComparer_1_t2165 *, const MethodInfo*))GenericComparer_1__ctor_m12778_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Guid>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m25510_gshared (GenericComparer_1_t2165 * __this, Guid_t2036  ___x, Guid_t2036  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m25510(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2165 *, Guid_t2036 , Guid_t2036 , const MethodInfo*))GenericComparer_1_Compare_m25510_gshared)(__this, ___x, ___y, method)
