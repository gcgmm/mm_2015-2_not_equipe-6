﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_35MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.UInt16,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m21359(__this, ___object, ___method, method) (( void (*) (Transform_1_t2788 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m21333_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.UInt16,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m21360(__this, ___key, ___value, method) (( DictionaryEntry_t1423  (*) (Transform_1_t2788 *, Type_t *, uint16_t, const MethodInfo*))Transform_1_Invoke_m21334_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.UInt16,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m21361(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2788 *, Type_t *, uint16_t, AsyncCallback_t261 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m21335_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.UInt16,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m21362(__this, ___result, method) (( DictionaryEntry_t1423  (*) (Transform_1_t2788 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m21336_gshared)(__this, ___result, method)
