﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Assembly
struct Assembly_t1432;
// System.Object
struct Object_t;
// System.ResolveEventArgs
struct ResolveEventArgs_t2058;
// System.IAsyncResult
struct IAsyncResult_t260;
// System.AsyncCallback
struct AsyncCallback_t261;

#include "mscorlib_System_MulticastDelegate.h"

// System.ResolveEventHandler
struct  ResolveEventHandler_t2002  : public MulticastDelegate_t259
{
};
