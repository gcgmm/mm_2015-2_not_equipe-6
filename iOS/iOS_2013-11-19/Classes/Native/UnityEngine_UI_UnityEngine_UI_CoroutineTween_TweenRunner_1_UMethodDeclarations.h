﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>
struct U3CStartU3Ec__Iterator0_t2341;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m15029_gshared (U3CStartU3Ec__Iterator0_t2341 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0__ctor_m15029(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2341 *, const MethodInfo*))U3CStartU3Ec__Iterator0__ctor_m15029_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15030_gshared (U3CStartU3Ec__Iterator0_t2341 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15030(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2341 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15030_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m15031_gshared (U3CStartU3Ec__Iterator0_t2341 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m15031(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2341 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m15031_gshared)(__this, method)
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m15032_gshared (U3CStartU3Ec__Iterator0_t2341 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_MoveNext_m15032(__this, method) (( bool (*) (U3CStartU3Ec__Iterator0_t2341 *, const MethodInfo*))U3CStartU3Ec__Iterator0_MoveNext_m15032_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m15033_gshared (U3CStartU3Ec__Iterator0_t2341 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Dispose_m15033(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2341 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Dispose_m15033_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Reset()
extern "C" void U3CStartU3Ec__Iterator0_Reset_m15034_gshared (U3CStartU3Ec__Iterator0_t2341 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Reset_m15034(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2341 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Reset_m15034_gshared)(__this, method)
