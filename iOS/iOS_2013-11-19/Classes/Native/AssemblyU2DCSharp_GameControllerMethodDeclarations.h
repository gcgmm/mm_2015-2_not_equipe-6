﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController
struct GameController_t6;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController::.ctor()
extern "C" void GameController__ctor_m15 (GameController_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::startEasy()
extern "C" void GameController_startEasy_m16 (GameController_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::startMild()
extern "C" void GameController_startMild_m17 (GameController_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::startHard()
extern "C" void GameController_startHard_m18 (GameController_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::CheckCode()
extern "C" void GameController_CheckCode_m19 (GameController_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::OnGameWin()
extern "C" void GameController_OnGameWin_m20 (GameController_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::onGameLoose()
extern "C" void GameController_onGameLoose_m21 (GameController_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::Restart()
extern "C" void GameController_Restart_m22 (GameController_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::hideInitMenu()
extern "C" void GameController_hideInitMenu_m23 (GameController_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
