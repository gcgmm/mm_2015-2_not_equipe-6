﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>
struct List_1_t222;
// UnityEngine.UI.Dropdown/DropdownItem
struct DropdownItem_t211;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Dropdown/DropdownItem>
struct  Enumerator_t2339 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Dropdown/DropdownItem>::l
	List_1_t222 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Dropdown/DropdownItem>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Dropdown/DropdownItem>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Dropdown/DropdownItem>::current
	DropdownItem_t211 * ___current_3;
};
