﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.ObjectTargetImpl
struct ObjectTargetImpl_t750;
// System.String
struct String_t;
// Vuforia.DataSet
struct DataSet_t764;
// Vuforia.DataSetImpl
struct DataSetImpl_t751;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.ObjectTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
extern "C" void ObjectTargetImpl__ctor_m3847 (ObjectTargetImpl_t750 * __this, String_t* ___name, int32_t ___id, DataSet_t764 * ___dataSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.ObjectTargetImpl::GetSize()
extern "C" Vector3_t12  ObjectTargetImpl_GetSize_m3848 (ObjectTargetImpl_t750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetImpl::SetSize(UnityEngine.Vector3)
extern "C" void ObjectTargetImpl_SetSize_m3849 (ObjectTargetImpl_t750 * __this, Vector3_t12  ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTargetImpl::StartExtendedTracking()
extern "C" bool ObjectTargetImpl_StartExtendedTracking_m3850 (ObjectTargetImpl_t750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DataSetImpl Vuforia.ObjectTargetImpl::get_DataSet()
extern "C" DataSetImpl_t751 * ObjectTargetImpl_get_DataSet_m3851 (ObjectTargetImpl_t750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
