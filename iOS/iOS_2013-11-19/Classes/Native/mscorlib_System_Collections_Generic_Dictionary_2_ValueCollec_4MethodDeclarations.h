﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_29MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m19447(__this, ___dictionary, method) (( void (*) (ValueCollection_t952 *, Dictionary_2_t731 *, const MethodInfo*))ValueCollection__ctor_m15340_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m19448(__this, ___item, method) (( void (*) (ValueCollection_t952 *, VideoBackgroundAbstractBehaviour_t67 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15341_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m19449(__this, method) (( void (*) (ValueCollection_t952 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15342_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m19450(__this, ___item, method) (( bool (*) (ValueCollection_t952 *, VideoBackgroundAbstractBehaviour_t67 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15343_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m19451(__this, ___item, method) (( bool (*) (ValueCollection_t952 *, VideoBackgroundAbstractBehaviour_t67 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15344_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m19452(__this, method) (( Object_t* (*) (ValueCollection_t952 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15345_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m19453(__this, ___array, ___index, method) (( void (*) (ValueCollection_t952 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m15346_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m19454(__this, method) (( Object_t * (*) (ValueCollection_t952 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19455(__this, method) (( bool (*) (ValueCollection_t952 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15348_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m19456(__this, method) (( bool (*) (ValueCollection_t952 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15349_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m19457(__this, method) (( Object_t * (*) (ValueCollection_t952 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m15350_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m19458(__this, ___array, ___index, method) (( void (*) (ValueCollection_t952 *, VideoBackgroundAbstractBehaviourU5BU5D_t2676*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m15351_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::GetEnumerator()
#define ValueCollection_GetEnumerator_m5234(__this, method) (( Enumerator_t951  (*) (ValueCollection_t952 *, const MethodInfo*))ValueCollection_GetEnumerator_m15352_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Count()
#define ValueCollection_get_Count_m19459(__this, method) (( int32_t (*) (ValueCollection_t952 *, const MethodInfo*))ValueCollection_get_Count_m15353_gshared)(__this, method)
