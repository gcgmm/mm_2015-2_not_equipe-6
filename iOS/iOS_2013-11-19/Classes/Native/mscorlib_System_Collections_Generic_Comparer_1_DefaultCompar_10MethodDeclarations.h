﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>
struct DefaultComparer_t2923;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void DefaultComparer__ctor_m23445_gshared (DefaultComparer_t2923 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m23445(__this, method) (( void (*) (DefaultComparer_t2923 *, const MethodInfo*))DefaultComparer__ctor_m23445_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m23446_gshared (DefaultComparer_t2923 * __this, TargetSearchResult_t884  ___x, TargetSearchResult_t884  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m23446(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2923 *, TargetSearchResult_t884 , TargetSearchResult_t884 , const MethodInfo*))DefaultComparer_Compare_m23446_gshared)(__this, ___x, ___y, method)
