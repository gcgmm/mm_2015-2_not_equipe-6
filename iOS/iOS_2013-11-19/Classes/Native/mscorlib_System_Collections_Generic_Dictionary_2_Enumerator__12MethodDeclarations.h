﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m18555(__this, ___dictionary, method) (( void (*) (Enumerator_t2608 *, Dictionary_2_t583 *, const MethodInfo*))Enumerator__ctor_m14303_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18556(__this, method) (( Object_t * (*) (Enumerator_t2608 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14304_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m18557(__this, method) (( void (*) (Enumerator_t2608 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14305_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18558(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t2608 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14306_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18559(__this, method) (( Object_t * (*) (Enumerator_t2608 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14307_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18560(__this, method) (( Object_t * (*) (Enumerator_t2608 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14308_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::MoveNext()
#define Enumerator_MoveNext_m18561(__this, method) (( bool (*) (Enumerator_t2608 *, const MethodInfo*))Enumerator_MoveNext_m14309_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Current()
#define Enumerator_get_Current_m18562(__this, method) (( KeyValuePair_2_t2605  (*) (Enumerator_t2608 *, const MethodInfo*))Enumerator_get_Current_m14310_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m18563(__this, method) (( int32_t (*) (Enumerator_t2608 *, const MethodInfo*))Enumerator_get_CurrentKey_m14311_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m18564(__this, method) (( LayoutCache_t580 * (*) (Enumerator_t2608 *, const MethodInfo*))Enumerator_get_CurrentValue_m14312_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::Reset()
#define Enumerator_Reset_m18565(__this, method) (( void (*) (Enumerator_t2608 *, const MethodInfo*))Enumerator_Reset_m14313_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyState()
#define Enumerator_VerifyState_m18566(__this, method) (( void (*) (Enumerator_t2608 *, const MethodInfo*))Enumerator_VerifyState_m14314_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m18567(__this, method) (( void (*) (Enumerator_t2608 *, const MethodInfo*))Enumerator_VerifyCurrent_m14315_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::Dispose()
#define Enumerator_Dispose_m18568(__this, method) (( void (*) (Enumerator_t2608 *, const MethodInfo*))Enumerator_Dispose_m14316_gshared)(__this, method)
