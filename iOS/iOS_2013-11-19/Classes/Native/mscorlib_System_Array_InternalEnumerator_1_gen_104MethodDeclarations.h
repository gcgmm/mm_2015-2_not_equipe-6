﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Level2Map>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m24845(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3042 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m12785_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Level2Map>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m24846(__this, method) (( void (*) (InternalEnumerator_1_t3042 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12787_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Level2Map>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24847(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3042 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12789_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Level2Map>::Dispose()
#define InternalEnumerator_1_Dispose_m24848(__this, method) (( void (*) (InternalEnumerator_1_t3042 *, const MethodInfo*))InternalEnumerator_1_Dispose_m12791_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Level2Map>::MoveNext()
#define InternalEnumerator_1_MoveNext_m24849(__this, method) (( bool (*) (InternalEnumerator_1_t3042 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m12793_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Level2Map>::get_Current()
#define InternalEnumerator_1_get_Current_m24850(__this, method) (( Level2Map_t1486 * (*) (InternalEnumerator_1_t3042 *, const MethodInfo*))InternalEnumerator_1_get_Current_m12795_gshared)(__this, method)
