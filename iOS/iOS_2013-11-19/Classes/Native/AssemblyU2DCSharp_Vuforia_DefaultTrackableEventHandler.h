﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t32;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Vuforia.DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t31  : public MonoBehaviour_t2
{
	// Vuforia.TrackableBehaviour Vuforia.DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t32 * ___mTrackableBehaviour_2;
};
