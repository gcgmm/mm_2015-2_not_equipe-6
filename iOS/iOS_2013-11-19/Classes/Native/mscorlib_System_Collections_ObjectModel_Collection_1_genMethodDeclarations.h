﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t2176;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t105;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2460;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t2175;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::.ctor()
extern "C" void Collection_1__ctor_m12938_gshared (Collection_1_t2176 * __this, const MethodInfo* method);
#define Collection_1__ctor_m12938(__this, method) (( void (*) (Collection_1_t2176 *, const MethodInfo*))Collection_1__ctor_m12938_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12939_gshared (Collection_1_t2176 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12939(__this, method) (( bool (*) (Collection_1_t2176 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12939_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m12940_gshared (Collection_1_t2176 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m12940(__this, ___array, ___index, method) (( void (*) (Collection_1_t2176 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m12940_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m12941_gshared (Collection_1_t2176 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m12941(__this, method) (( Object_t * (*) (Collection_1_t2176 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m12941_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m12942_gshared (Collection_1_t2176 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m12942(__this, ___value, method) (( int32_t (*) (Collection_1_t2176 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m12942_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m12943_gshared (Collection_1_t2176 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m12943(__this, ___value, method) (( bool (*) (Collection_1_t2176 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m12943_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m12944_gshared (Collection_1_t2176 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m12944(__this, ___value, method) (( int32_t (*) (Collection_1_t2176 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m12944_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m12945_gshared (Collection_1_t2176 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m12945(__this, ___index, ___value, method) (( void (*) (Collection_1_t2176 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m12945_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m12946_gshared (Collection_1_t2176 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m12946(__this, ___value, method) (( void (*) (Collection_1_t2176 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m12946_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m12947_gshared (Collection_1_t2176 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m12947(__this, method) (( bool (*) (Collection_1_t2176 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m12947_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m12948_gshared (Collection_1_t2176 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m12948(__this, method) (( Object_t * (*) (Collection_1_t2176 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m12948_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m12949_gshared (Collection_1_t2176 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m12949(__this, method) (( bool (*) (Collection_1_t2176 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m12949_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m12950_gshared (Collection_1_t2176 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m12950(__this, method) (( bool (*) (Collection_1_t2176 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m12950_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m12951_gshared (Collection_1_t2176 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m12951(__this, ___index, method) (( Object_t * (*) (Collection_1_t2176 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m12951_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m12952_gshared (Collection_1_t2176 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m12952(__this, ___index, ___value, method) (( void (*) (Collection_1_t2176 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m12952_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(T)
extern "C" void Collection_1_Add_m12953_gshared (Collection_1_t2176 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Add_m12953(__this, ___item, method) (( void (*) (Collection_1_t2176 *, Object_t *, const MethodInfo*))Collection_1_Add_m12953_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Clear()
extern "C" void Collection_1_Clear_m12954_gshared (Collection_1_t2176 * __this, const MethodInfo* method);
#define Collection_1_Clear_m12954(__this, method) (( void (*) (Collection_1_t2176 *, const MethodInfo*))Collection_1_Clear_m12954_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems()
extern "C" void Collection_1_ClearItems_m12955_gshared (Collection_1_t2176 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m12955(__this, method) (( void (*) (Collection_1_t2176 *, const MethodInfo*))Collection_1_ClearItems_m12955_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Contains(T)
extern "C" bool Collection_1_Contains_m12956_gshared (Collection_1_t2176 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Contains_m12956(__this, ___item, method) (( bool (*) (Collection_1_t2176 *, Object_t *, const MethodInfo*))Collection_1_Contains_m12956_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m12957_gshared (Collection_1_t2176 * __this, ObjectU5BU5D_t105* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m12957(__this, ___array, ___index, method) (( void (*) (Collection_1_t2176 *, ObjectU5BU5D_t105*, int32_t, const MethodInfo*))Collection_1_CopyTo_m12957_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m12958_gshared (Collection_1_t2176 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m12958(__this, method) (( Object_t* (*) (Collection_1_t2176 *, const MethodInfo*))Collection_1_GetEnumerator_m12958_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m12959_gshared (Collection_1_t2176 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m12959(__this, ___item, method) (( int32_t (*) (Collection_1_t2176 *, Object_t *, const MethodInfo*))Collection_1_IndexOf_m12959_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m12960_gshared (Collection_1_t2176 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Insert_m12960(__this, ___index, ___item, method) (( void (*) (Collection_1_t2176 *, int32_t, Object_t *, const MethodInfo*))Collection_1_Insert_m12960_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m12961_gshared (Collection_1_t2176 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m12961(__this, ___index, ___item, method) (( void (*) (Collection_1_t2176 *, int32_t, Object_t *, const MethodInfo*))Collection_1_InsertItem_m12961_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(T)
extern "C" bool Collection_1_Remove_m12962_gshared (Collection_1_t2176 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Remove_m12962(__this, ___item, method) (( bool (*) (Collection_1_t2176 *, Object_t *, const MethodInfo*))Collection_1_Remove_m12962_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m12963_gshared (Collection_1_t2176 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m12963(__this, ___index, method) (( void (*) (Collection_1_t2176 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m12963_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m12964_gshared (Collection_1_t2176 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m12964(__this, ___index, method) (( void (*) (Collection_1_t2176 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m12964_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count()
extern "C" int32_t Collection_1_get_Count_m12965_gshared (Collection_1_t2176 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m12965(__this, method) (( int32_t (*) (Collection_1_t2176 *, const MethodInfo*))Collection_1_get_Count_m12965_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * Collection_1_get_Item_m12966_gshared (Collection_1_t2176 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m12966(__this, ___index, method) (( Object_t * (*) (Collection_1_t2176 *, int32_t, const MethodInfo*))Collection_1_get_Item_m12966_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m12967_gshared (Collection_1_t2176 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_set_Item_m12967(__this, ___index, ___value, method) (( void (*) (Collection_1_t2176 *, int32_t, Object_t *, const MethodInfo*))Collection_1_set_Item_m12967_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m12968_gshared (Collection_1_t2176 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_SetItem_m12968(__this, ___index, ___item, method) (( void (*) (Collection_1_t2176 *, int32_t, Object_t *, const MethodInfo*))Collection_1_SetItem_m12968_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m12969_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m12969(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m12969_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::ConvertItem(System.Object)
extern "C" Object_t * Collection_1_ConvertItem_m12970_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m12970(__this /* static, unused */, ___item, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m12970_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m12971_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m12971(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m12971_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m12972_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m12972(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m12972_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m12973_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m12973(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m12973_gshared)(__this /* static, unused */, ___list, method)
