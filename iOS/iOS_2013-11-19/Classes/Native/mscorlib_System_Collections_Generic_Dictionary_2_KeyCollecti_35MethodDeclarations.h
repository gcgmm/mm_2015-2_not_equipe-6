﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_5MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m22795(__this, ___host, method) (( void (*) (Enumerator_t2871 *, Dictionary_2_t872 *, const MethodInfo*))Enumerator__ctor_m14297_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22796(__this, method) (( Object_t * (*) (Enumerator_t2871 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14298_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m22797(__this, method) (( void (*) (Enumerator_t2871 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m14299_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::Dispose()
#define Enumerator_Dispose_m22798(__this, method) (( void (*) (Enumerator_t2871 *, const MethodInfo*))Enumerator_Dispose_m14300_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::MoveNext()
#define Enumerator_MoveNext_m22799(__this, method) (( bool (*) (Enumerator_t2871 *, const MethodInfo*))Enumerator_MoveNext_m14301_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::get_Current()
#define Enumerator_get_Current_m22800(__this, method) (( int32_t (*) (Enumerator_t2871 *, const MethodInfo*))Enumerator_get_Current_m14302_gshared)(__this, method)
