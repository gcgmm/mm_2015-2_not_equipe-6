﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.ITextRecoEventHandler
struct ITextRecoEventHandler_t948;
// System.IAsyncResult
struct IAsyncResult_t260;
// System.AsyncCallback
struct AsyncCallback_t261;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// System.Predicate`1<Vuforia.ITextRecoEventHandler>
struct  Predicate_1_t2970  : public MulticastDelegate_t259
{
};
