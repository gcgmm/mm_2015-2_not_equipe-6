﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Transform_1_t2905;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t260;
// System.AsyncCallback
struct AsyncCallback_t261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m23188_gshared (Transform_1_t2905 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m23188(__this, ___object, ___method, method) (( void (*) (Transform_1_t2905 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m23188_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Invoke(TKey,TValue)
extern "C" VirtualButtonData_t811  Transform_1_Invoke_m23189_gshared (Transform_1_t2905 * __this, int32_t ___key, VirtualButtonData_t811  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m23189(__this, ___key, ___value, method) (( VirtualButtonData_t811  (*) (Transform_1_t2905 *, int32_t, VirtualButtonData_t811 , const MethodInfo*))Transform_1_Invoke_m23189_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m23190_gshared (Transform_1_t2905 * __this, int32_t ___key, VirtualButtonData_t811  ___value, AsyncCallback_t261 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m23190(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2905 *, int32_t, VirtualButtonData_t811 , AsyncCallback_t261 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m23190_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::EndInvoke(System.IAsyncResult)
extern "C" VirtualButtonData_t811  Transform_1_EndInvoke_m23191_gshared (Transform_1_t2905 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m23191(__this, ___result, method) (( VirtualButtonData_t811  (*) (Transform_1_t2905 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m23191_gshared)(__this, ___result, method)
