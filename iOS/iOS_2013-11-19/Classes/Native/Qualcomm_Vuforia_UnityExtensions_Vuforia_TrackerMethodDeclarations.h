﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.Tracker
struct Tracker_t791;

#include "codegen/il2cpp-codegen.h"

// System.Boolean Vuforia.Tracker::get_IsActive()
extern "C" bool Tracker_get_IsActive_m4070 (Tracker_t791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Tracker::set_IsActive(System.Boolean)
extern "C" void Tracker_set_IsActive_m4071 (Tracker_t791 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Tracker::.ctor()
extern "C" void Tracker__ctor_m4072 (Tracker_t791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
