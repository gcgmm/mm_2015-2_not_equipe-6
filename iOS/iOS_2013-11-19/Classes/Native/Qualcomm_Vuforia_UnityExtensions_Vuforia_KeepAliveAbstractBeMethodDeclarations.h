﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.KeepAliveAbstractBehaviour
struct KeepAliveAbstractBehaviour_t43;
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t936;

#include "codegen/il2cpp-codegen.h"

// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepARCameraAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepARCameraAlive_m4796 (KeepAliveAbstractBehaviour_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepARCameraAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepARCameraAlive_m4797 (KeepAliveAbstractBehaviour_t43 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTrackableBehavioursAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepTrackableBehavioursAlive_m4798 (KeepAliveAbstractBehaviour_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTrackableBehavioursAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepTrackableBehavioursAlive_m4799 (KeepAliveAbstractBehaviour_t43 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTextRecoBehaviourAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepTextRecoBehaviourAlive_m4800 (KeepAliveAbstractBehaviour_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTextRecoBehaviourAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepTextRecoBehaviourAlive_m4801 (KeepAliveAbstractBehaviour_t43 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepUDTBuildingBehaviourAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepUDTBuildingBehaviourAlive_m4802 (KeepAliveAbstractBehaviour_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepUDTBuildingBehaviourAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepUDTBuildingBehaviourAlive_m4803 (KeepAliveAbstractBehaviour_t43 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepSmartTerrainAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepSmartTerrainAlive_m4804 (KeepAliveAbstractBehaviour_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepSmartTerrainAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepSmartTerrainAlive_m4805 (KeepAliveAbstractBehaviour_t43 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepCloudRecoBehaviourAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepCloudRecoBehaviourAlive_m4806 (KeepAliveAbstractBehaviour_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepCloudRecoBehaviourAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepCloudRecoBehaviourAlive_m4807 (KeepAliveAbstractBehaviour_t43 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.KeepAliveAbstractBehaviour Vuforia.KeepAliveAbstractBehaviour::get_Instance()
extern "C" KeepAliveAbstractBehaviour_t43 * KeepAliveAbstractBehaviour_get_Instance_m4808 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::RegisterEventHandler(Vuforia.ILoadLevelEventHandler)
extern "C" void KeepAliveAbstractBehaviour_RegisterEventHandler_m4809 (KeepAliveAbstractBehaviour_t43 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::UnregisterEventHandler(Vuforia.ILoadLevelEventHandler)
extern "C" bool KeepAliveAbstractBehaviour_UnregisterEventHandler_m4810 (KeepAliveAbstractBehaviour_t43 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::OnLevelWasLoaded()
extern "C" void KeepAliveAbstractBehaviour_OnLevelWasLoaded_m4811 (KeepAliveAbstractBehaviour_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::.ctor()
extern "C" void KeepAliveAbstractBehaviour__ctor_m242 (KeepAliveAbstractBehaviour_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
