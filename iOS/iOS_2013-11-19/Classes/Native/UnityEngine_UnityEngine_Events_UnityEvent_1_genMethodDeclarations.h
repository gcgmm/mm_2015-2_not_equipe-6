﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen_6MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::.ctor()
#define UnityEvent_1__ctor_m1729(__this, method) (( void (*) (UnityEvent_1_t140 *, const MethodInfo*))UnityEvent_1__ctor_m13923_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_AddListener_m13924(__this, ___call, method) (( void (*) (UnityEvent_1_t140 *, UnityAction_1_t2263 *, const MethodInfo*))UnityEvent_1_AddListener_m13925_gshared)(__this, ___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_RemoveListener_m13926(__this, ___call, method) (( void (*) (UnityEvent_1_t140 *, UnityAction_1_t2263 *, const MethodInfo*))UnityEvent_1_RemoveListener_m13927_gshared)(__this, ___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::FindMethod_Impl(System.String,System.Object)
#define UnityEvent_1_FindMethod_Impl_m13928(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_1_t140 *, String_t*, Object_t *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m13929_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::GetDelegate(System.Object,System.Reflection.MethodInfo)
#define UnityEvent_1_GetDelegate_m13930(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t655 * (*) (UnityEvent_1_t140 *, Object_t *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m13931_gshared)(__this, ___target, ___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_GetDelegate_m13932(__this /* static, unused */, ___action, method) (( BaseInvokableCall_t655 * (*) (Object_t * /* static, unused */, UnityAction_1_t2263 *, const MethodInfo*))UnityEvent_1_GetDelegate_m13933_gshared)(__this /* static, unused */, ___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::Invoke(T0)
#define UnityEvent_1_Invoke_m1731(__this, ___arg0, method) (( void (*) (UnityEvent_1_t140 *, BaseEventData_t137 *, const MethodInfo*))UnityEvent_1_Invoke_m13934_gshared)(__this, ___arg0, method)
