﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m18747(__this, ___dictionary, method) (( void (*) (Enumerator_t2618 *, Dictionary_2_t593 *, const MethodInfo*))Enumerator__ctor_m15322_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18748(__this, method) (( Object_t * (*) (Enumerator_t2618 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m18749(__this, method) (( void (*) (Enumerator_t2618 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15324_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18750(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t2618 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15325_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18751(__this, method) (( Object_t * (*) (Enumerator_t2618 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15326_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18752(__this, method) (( Object_t * (*) (Enumerator_t2618 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15327_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m18753(__this, method) (( bool (*) (Enumerator_t2618 *, const MethodInfo*))Enumerator_MoveNext_m15328_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m18754(__this, method) (( KeyValuePair_2_t2616  (*) (Enumerator_t2618 *, const MethodInfo*))Enumerator_get_Current_m15329_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m18755(__this, method) (( String_t* (*) (Enumerator_t2618 *, const MethodInfo*))Enumerator_get_CurrentKey_m15330_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m18756(__this, method) (( GUIStyle_t584 * (*) (Enumerator_t2618 *, const MethodInfo*))Enumerator_get_CurrentValue_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::Reset()
#define Enumerator_Reset_m18757(__this, method) (( void (*) (Enumerator_t2618 *, const MethodInfo*))Enumerator_Reset_m15332_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyState()
#define Enumerator_VerifyState_m18758(__this, method) (( void (*) (Enumerator_t2618 *, const MethodInfo*))Enumerator_VerifyState_m15333_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m18759(__this, method) (( void (*) (Enumerator_t2618 *, const MethodInfo*))Enumerator_VerifyCurrent_m15334_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m18760(__this, method) (( void (*) (Enumerator_t2618 *, const MethodInfo*))Enumerator_Dispose_m15335_gshared)(__this, method)
