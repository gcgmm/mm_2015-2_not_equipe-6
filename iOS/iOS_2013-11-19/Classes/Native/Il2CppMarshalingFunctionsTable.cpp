﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void WaitForSeconds_t407_marshal ();
extern "C" void WaitForSeconds_t407_marshal_back ();
extern "C" void WaitForSeconds_t407_marshal_cleanup ();
extern "C" void Coroutine_t268_marshal ();
extern "C" void Coroutine_t268_marshal_back ();
extern "C" void Coroutine_t268_marshal_cleanup ();
extern "C" void ScriptableObject_t471_marshal ();
extern "C" void ScriptableObject_t471_marshal_back ();
extern "C" void ScriptableObject_t471_marshal_cleanup ();
extern "C" void Gradient_t497_marshal ();
extern "C" void Gradient_t497_marshal_back ();
extern "C" void Gradient_t497_marshal_cleanup ();
extern "C" void CacheIndex_t511_marshal ();
extern "C" void CacheIndex_t511_marshal_back ();
extern "C" void CacheIndex_t511_marshal_cleanup ();
extern "C" void AsyncOperation_t462_marshal ();
extern "C" void AsyncOperation_t462_marshal_back ();
extern "C" void AsyncOperation_t462_marshal_cleanup ();
extern "C" void Touch_t375_marshal ();
extern "C" void Touch_t375_marshal_back ();
extern "C" void Touch_t375_marshal_cleanup ();
extern "C" void Object_t86_marshal ();
extern "C" void Object_t86_marshal_back ();
extern "C" void Object_t86_marshal_cleanup ();
extern "C" void YieldInstruction_t469_marshal ();
extern "C" void YieldInstruction_t469_marshal_back ();
extern "C" void YieldInstruction_t469_marshal_cleanup ();
extern "C" void WebCamDevice_t550_marshal ();
extern "C" void WebCamDevice_t550_marshal_back ();
extern "C" void WebCamDevice_t550_marshal_cleanup ();
extern "C" void AnimationCurve_t558_marshal ();
extern "C" void AnimationCurve_t558_marshal_back ();
extern "C" void AnimationCurve_t558_marshal_cleanup ();
extern "C" void AnimatorTransitionInfo_t560_marshal ();
extern "C" void AnimatorTransitionInfo_t560_marshal_back ();
extern "C" void AnimatorTransitionInfo_t560_marshal_cleanup ();
extern "C" void SkeletonBone_t561_marshal ();
extern "C" void SkeletonBone_t561_marshal_back ();
extern "C" void SkeletonBone_t561_marshal_cleanup ();
extern "C" void HumanBone_t563_marshal ();
extern "C" void HumanBone_t563_marshal_back ();
extern "C" void HumanBone_t563_marshal_cleanup ();
extern "C" void CharacterInfo_t566_marshal ();
extern "C" void CharacterInfo_t566_marshal_back ();
extern "C" void CharacterInfo_t566_marshal_cleanup ();
extern "C" void Event_t269_marshal ();
extern "C" void Event_t269_marshal_back ();
extern "C" void Event_t269_marshal_cleanup ();
extern "C" void GcAchievementData_t613_marshal ();
extern "C" void GcAchievementData_t613_marshal_back ();
extern "C" void GcAchievementData_t613_marshal_cleanup ();
extern "C" void GcScoreData_t614_marshal ();
extern "C" void GcScoreData_t614_marshal_back ();
extern "C" void GcScoreData_t614_marshal_cleanup ();
extern "C" void TrackedReference_t559_marshal ();
extern "C" void TrackedReference_t559_marshal_back ();
extern "C" void TrackedReference_t559_marshal_cleanup ();
extern "C" void TrackableResultData_t810_marshal ();
extern "C" void TrackableResultData_t810_marshal_back ();
extern "C" void TrackableResultData_t810_marshal_cleanup ();
extern "C" void WordResultData_t814_marshal ();
extern "C" void WordResultData_t814_marshal_back ();
extern "C" void WordResultData_t814_marshal_cleanup ();
extern "C" void VideoBGCfgData_t830_marshal ();
extern "C" void VideoBGCfgData_t830_marshal_back ();
extern "C" void VideoBGCfgData_t830_marshal_cleanup ();
extern "C" void TargetSearchResult_t884_marshal ();
extern "C" void TargetSearchResult_t884_marshal_back ();
extern "C" void TargetSearchResult_t884_marshal_cleanup ();
extern "C" void TargetFinderState_t885_marshal ();
extern "C" void TargetFinderState_t885_marshal_back ();
extern "C" void TargetFinderState_t885_marshal_cleanup ();
extern "C" void X509ChainStatus_t1333_marshal ();
extern "C" void X509ChainStatus_t1333_marshal_back ();
extern "C" void X509ChainStatus_t1333_marshal_cleanup ();
extern "C" void IntStack_t1376_marshal ();
extern "C" void IntStack_t1376_marshal_back ();
extern "C" void IntStack_t1376_marshal_cleanup ();
extern "C" void Interval_t1382_marshal ();
extern "C" void Interval_t1382_marshal_back ();
extern "C" void Interval_t1382_marshal_cleanup ();
extern "C" void UriScheme_t1412_marshal ();
extern "C" void UriScheme_t1412_marshal_back ();
extern "C" void UriScheme_t1412_marshal_cleanup ();
extern "C" void Context_t1491_marshal ();
extern "C" void Context_t1491_marshal_back ();
extern "C" void Context_t1491_marshal_cleanup ();
extern "C" void PreviousInfo_t1492_marshal ();
extern "C" void PreviousInfo_t1492_marshal_back ();
extern "C" void PreviousInfo_t1492_marshal_cleanup ();
extern "C" void Escape_t1493_marshal ();
extern "C" void Escape_t1493_marshal_back ();
extern "C" void Escape_t1493_marshal_cleanup ();
extern "C" void MonoIOStat_t1611_marshal ();
extern "C" void MonoIOStat_t1611_marshal_back ();
extern "C" void MonoIOStat_t1611_marshal_cleanup ();
extern "C" void ParameterModifier_t1710_marshal ();
extern "C" void ParameterModifier_t1710_marshal_back ();
extern "C" void ParameterModifier_t1710_marshal_cleanup ();
extern "C" void ResourceInfo_t1721_marshal ();
extern "C" void ResourceInfo_t1721_marshal_back ();
extern "C" void ResourceInfo_t1721_marshal_cleanup ();
extern "C" void DSAParameters_t1229_marshal ();
extern "C" void DSAParameters_t1229_marshal_back ();
extern "C" void DSAParameters_t1229_marshal_cleanup ();
extern "C" void RSAParameters_t1201_marshal ();
extern "C" void RSAParameters_t1201_marshal_back ();
extern "C" void RSAParameters_t1201_marshal_cleanup ();
extern const Il2CppMarshalingFunctions g_MarshalingFunctions[37] = 
{
	{ WaitForSeconds_t407_marshal, WaitForSeconds_t407_marshal_back, WaitForSeconds_t407_marshal_cleanup },
	{ Coroutine_t268_marshal, Coroutine_t268_marshal_back, Coroutine_t268_marshal_cleanup },
	{ ScriptableObject_t471_marshal, ScriptableObject_t471_marshal_back, ScriptableObject_t471_marshal_cleanup },
	{ Gradient_t497_marshal, Gradient_t497_marshal_back, Gradient_t497_marshal_cleanup },
	{ CacheIndex_t511_marshal, CacheIndex_t511_marshal_back, CacheIndex_t511_marshal_cleanup },
	{ AsyncOperation_t462_marshal, AsyncOperation_t462_marshal_back, AsyncOperation_t462_marshal_cleanup },
	{ Touch_t375_marshal, Touch_t375_marshal_back, Touch_t375_marshal_cleanup },
	{ Object_t86_marshal, Object_t86_marshal_back, Object_t86_marshal_cleanup },
	{ YieldInstruction_t469_marshal, YieldInstruction_t469_marshal_back, YieldInstruction_t469_marshal_cleanup },
	{ WebCamDevice_t550_marshal, WebCamDevice_t550_marshal_back, WebCamDevice_t550_marshal_cleanup },
	{ AnimationCurve_t558_marshal, AnimationCurve_t558_marshal_back, AnimationCurve_t558_marshal_cleanup },
	{ AnimatorTransitionInfo_t560_marshal, AnimatorTransitionInfo_t560_marshal_back, AnimatorTransitionInfo_t560_marshal_cleanup },
	{ SkeletonBone_t561_marshal, SkeletonBone_t561_marshal_back, SkeletonBone_t561_marshal_cleanup },
	{ HumanBone_t563_marshal, HumanBone_t563_marshal_back, HumanBone_t563_marshal_cleanup },
	{ CharacterInfo_t566_marshal, CharacterInfo_t566_marshal_back, CharacterInfo_t566_marshal_cleanup },
	{ Event_t269_marshal, Event_t269_marshal_back, Event_t269_marshal_cleanup },
	{ GcAchievementData_t613_marshal, GcAchievementData_t613_marshal_back, GcAchievementData_t613_marshal_cleanup },
	{ GcScoreData_t614_marshal, GcScoreData_t614_marshal_back, GcScoreData_t614_marshal_cleanup },
	{ TrackedReference_t559_marshal, TrackedReference_t559_marshal_back, TrackedReference_t559_marshal_cleanup },
	{ TrackableResultData_t810_marshal, TrackableResultData_t810_marshal_back, TrackableResultData_t810_marshal_cleanup },
	{ WordResultData_t814_marshal, WordResultData_t814_marshal_back, WordResultData_t814_marshal_cleanup },
	{ VideoBGCfgData_t830_marshal, VideoBGCfgData_t830_marshal_back, VideoBGCfgData_t830_marshal_cleanup },
	{ TargetSearchResult_t884_marshal, TargetSearchResult_t884_marshal_back, TargetSearchResult_t884_marshal_cleanup },
	{ TargetFinderState_t885_marshal, TargetFinderState_t885_marshal_back, TargetFinderState_t885_marshal_cleanup },
	{ X509ChainStatus_t1333_marshal, X509ChainStatus_t1333_marshal_back, X509ChainStatus_t1333_marshal_cleanup },
	{ IntStack_t1376_marshal, IntStack_t1376_marshal_back, IntStack_t1376_marshal_cleanup },
	{ Interval_t1382_marshal, Interval_t1382_marshal_back, Interval_t1382_marshal_cleanup },
	{ UriScheme_t1412_marshal, UriScheme_t1412_marshal_back, UriScheme_t1412_marshal_cleanup },
	{ Context_t1491_marshal, Context_t1491_marshal_back, Context_t1491_marshal_cleanup },
	{ PreviousInfo_t1492_marshal, PreviousInfo_t1492_marshal_back, PreviousInfo_t1492_marshal_cleanup },
	{ Escape_t1493_marshal, Escape_t1493_marshal_back, Escape_t1493_marshal_cleanup },
	{ MonoIOStat_t1611_marshal, MonoIOStat_t1611_marshal_back, MonoIOStat_t1611_marshal_cleanup },
	{ ParameterModifier_t1710_marshal, ParameterModifier_t1710_marshal_back, ParameterModifier_t1710_marshal_cleanup },
	{ ResourceInfo_t1721_marshal, ResourceInfo_t1721_marshal_back, ResourceInfo_t1721_marshal_cleanup },
	{ DSAParameters_t1229_marshal, DSAParameters_t1229_marshal_back, DSAParameters_t1229_marshal_cleanup },
	{ RSAParameters_t1201_marshal, RSAParameters_t1201_marshal_back, RSAParameters_t1201_marshal_cleanup },
	NULL,
};
