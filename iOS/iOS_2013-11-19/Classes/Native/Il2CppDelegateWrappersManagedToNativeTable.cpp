﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void pinvoke_delegate_wrapper_OnValidateInput_t258 ();
extern "C" void pinvoke_delegate_wrapper_StateChanged_t493 ();
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t447 ();
extern "C" void pinvoke_delegate_wrapper_LogCallback_t513 ();
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t515 ();
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t517 ();
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t544 ();
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t546 ();
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t548 ();
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t567 ();
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t406 ();
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t95 ();
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t591 ();
extern "C" void pinvoke_delegate_wrapper_UnityAdsDelegate_t528 ();
extern "C" void pinvoke_delegate_wrapper_UnityAction_t231 ();
extern "C" void pinvoke_delegate_wrapper_Action_t106 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1116 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t1216 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback_t1191 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback2_t1192 ();
extern "C" void pinvoke_delegate_wrapper_CertificateSelectionCallback_t1175 ();
extern "C" void pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t1176 ();
extern "C" void pinvoke_delegate_wrapper_MatchAppendEvaluator_t1350 ();
extern "C" void pinvoke_delegate_wrapper_CostDelegate_t1385 ();
extern "C" void pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t1259 ();
extern "C" void pinvoke_delegate_wrapper_MatchEvaluator_t1419 ();
extern "C" void pinvoke_delegate_wrapper_Swapper_t1448 ();
extern "C" void pinvoke_delegate_wrapper_AsyncCallback_t261 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1512 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1520 ();
extern "C" void pinvoke_delegate_wrapper_ReadDelegate_t1608 ();
extern "C" void pinvoke_delegate_wrapper_WriteDelegate_t1609 ();
extern "C" void pinvoke_delegate_wrapper_AddEventAdapter_t1694 ();
extern "C" void pinvoke_delegate_wrapper_GetterAdapter_t1708 ();
extern "C" void pinvoke_delegate_wrapper_CallbackHandler_t1885 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t2072 ();
extern "C" void pinvoke_delegate_wrapper_MemberFilter_t1451 ();
extern "C" void pinvoke_delegate_wrapper_TypeFilter_t1701 ();
extern "C" void pinvoke_delegate_wrapper_CrossContextDelegate_t2073 ();
extern "C" void pinvoke_delegate_wrapper_HeaderHandler_t2074 ();
extern "C" void pinvoke_delegate_wrapper_ThreadStart_t2076 ();
extern "C" void pinvoke_delegate_wrapper_TimerCallback_t1995 ();
extern "C" void pinvoke_delegate_wrapper_WaitCallback_t2077 ();
extern "C" void pinvoke_delegate_wrapper_AppDomainInitializer_t2004 ();
extern "C" void pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t2001 ();
extern "C" void pinvoke_delegate_wrapper_EventHandler_t1631 ();
extern "C" void pinvoke_delegate_wrapper_ResolveEventHandler_t2002 ();
extern "C" void pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t691 ();
extern const methodPointerType g_DelegateWrappersManagedToNative[48] = 
{
	pinvoke_delegate_wrapper_OnValidateInput_t258,
	pinvoke_delegate_wrapper_StateChanged_t493,
	pinvoke_delegate_wrapper_ReapplyDrivenProperties_t447,
	pinvoke_delegate_wrapper_LogCallback_t513,
	pinvoke_delegate_wrapper_CameraCallback_t515,
	pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t517,
	pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t544,
	pinvoke_delegate_wrapper_PCMReaderCallback_t546,
	pinvoke_delegate_wrapper_PCMSetPositionCallback_t548,
	pinvoke_delegate_wrapper_FontTextureRebuildCallback_t567,
	pinvoke_delegate_wrapper_WillRenderCanvases_t406,
	pinvoke_delegate_wrapper_WindowFunction_t95,
	pinvoke_delegate_wrapper_SkinChangedDelegate_t591,
	pinvoke_delegate_wrapper_UnityAdsDelegate_t528,
	pinvoke_delegate_wrapper_UnityAction_t231,
	pinvoke_delegate_wrapper_Action_t106,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1116,
	pinvoke_delegate_wrapper_PrimalityTest_t1216,
	pinvoke_delegate_wrapper_CertificateValidationCallback_t1191,
	pinvoke_delegate_wrapper_CertificateValidationCallback2_t1192,
	pinvoke_delegate_wrapper_CertificateSelectionCallback_t1175,
	pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t1176,
	pinvoke_delegate_wrapper_MatchAppendEvaluator_t1350,
	pinvoke_delegate_wrapper_CostDelegate_t1385,
	pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t1259,
	pinvoke_delegate_wrapper_MatchEvaluator_t1419,
	pinvoke_delegate_wrapper_Swapper_t1448,
	pinvoke_delegate_wrapper_AsyncCallback_t261,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1512,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1520,
	pinvoke_delegate_wrapper_ReadDelegate_t1608,
	pinvoke_delegate_wrapper_WriteDelegate_t1609,
	pinvoke_delegate_wrapper_AddEventAdapter_t1694,
	pinvoke_delegate_wrapper_GetterAdapter_t1708,
	pinvoke_delegate_wrapper_CallbackHandler_t1885,
	pinvoke_delegate_wrapper_PrimalityTest_t2072,
	pinvoke_delegate_wrapper_MemberFilter_t1451,
	pinvoke_delegate_wrapper_TypeFilter_t1701,
	pinvoke_delegate_wrapper_CrossContextDelegate_t2073,
	pinvoke_delegate_wrapper_HeaderHandler_t2074,
	pinvoke_delegate_wrapper_ThreadStart_t2076,
	pinvoke_delegate_wrapper_TimerCallback_t1995,
	pinvoke_delegate_wrapper_WaitCallback_t2077,
	pinvoke_delegate_wrapper_AppDomainInitializer_t2004,
	pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t2001,
	pinvoke_delegate_wrapper_EventHandler_t1631,
	pinvoke_delegate_wrapper_ResolveEventHandler_t2002,
	pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t691,
};
