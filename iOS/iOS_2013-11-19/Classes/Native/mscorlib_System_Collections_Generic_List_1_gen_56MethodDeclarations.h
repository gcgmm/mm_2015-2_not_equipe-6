﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t888;
// System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerable_1_t944;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t958;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<Vuforia.TargetFinder/TargetSearchResult>
struct ICollection_1_t3243;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>
struct ReadOnlyCollection_1_t2916;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t2914;
// System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>
struct Predicate_1_t2921;
// System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>
struct Comparison_1_t2924;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_62.h"

// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void List_1__ctor_m5498_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1__ctor_m5498(__this, method) (( void (*) (List_1_t888 *, const MethodInfo*))List_1__ctor_m5498_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m23307_gshared (List_1_t888 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m23307(__this, ___collection, method) (( void (*) (List_1_t888 *, Object_t*, const MethodInfo*))List_1__ctor_m23307_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m23308_gshared (List_1_t888 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m23308(__this, ___capacity, method) (( void (*) (List_1_t888 *, int32_t, const MethodInfo*))List_1__ctor_m23308_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.cctor()
extern "C" void List_1__cctor_m23309_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m23309(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m23309_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23310_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23310(__this, method) (( Object_t* (*) (List_1_t888 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23310_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m23311_gshared (List_1_t888 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m23311(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t888 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m23311_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m23312_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23312(__this, method) (( Object_t * (*) (List_1_t888 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m23312_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m23313_gshared (List_1_t888 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m23313(__this, ___item, method) (( int32_t (*) (List_1_t888 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m23313_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m23314_gshared (List_1_t888 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m23314(__this, ___item, method) (( bool (*) (List_1_t888 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m23314_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m23315_gshared (List_1_t888 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m23315(__this, ___item, method) (( int32_t (*) (List_1_t888 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m23315_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m23316_gshared (List_1_t888 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m23316(__this, ___index, ___item, method) (( void (*) (List_1_t888 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m23316_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m23317_gshared (List_1_t888 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m23317(__this, ___item, method) (( void (*) (List_1_t888 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m23317_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23318_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23318(__this, method) (( bool (*) (List_1_t888 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23318_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m23319_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23319(__this, method) (( bool (*) (List_1_t888 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m23319_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m23320_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m23320(__this, method) (( Object_t * (*) (List_1_t888 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m23320_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m23321_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m23321(__this, method) (( bool (*) (List_1_t888 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m23321_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m23322_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m23322(__this, method) (( bool (*) (List_1_t888 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m23322_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m23323_gshared (List_1_t888 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m23323(__this, ___index, method) (( Object_t * (*) (List_1_t888 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m23323_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m23324_gshared (List_1_t888 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m23324(__this, ___index, ___value, method) (( void (*) (List_1_t888 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m23324_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T)
extern "C" void List_1_Add_m23325_gshared (List_1_t888 * __this, TargetSearchResult_t884  ___item, const MethodInfo* method);
#define List_1_Add_m23325(__this, ___item, method) (( void (*) (List_1_t888 *, TargetSearchResult_t884 , const MethodInfo*))List_1_Add_m23325_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m23326_gshared (List_1_t888 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m23326(__this, ___newCount, method) (( void (*) (List_1_t888 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m23326_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m23327_gshared (List_1_t888 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m23327(__this, ___collection, method) (( void (*) (List_1_t888 *, Object_t*, const MethodInfo*))List_1_AddCollection_m23327_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m23328_gshared (List_1_t888 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m23328(__this, ___enumerable, method) (( void (*) (List_1_t888 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m23328_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m23329_gshared (List_1_t888 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m23329(__this, ___collection, method) (( void (*) (List_1_t888 *, Object_t*, const MethodInfo*))List_1_AddRange_m23329_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2916 * List_1_AsReadOnly_m23330_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m23330(__this, method) (( ReadOnlyCollection_1_t2916 * (*) (List_1_t888 *, const MethodInfo*))List_1_AsReadOnly_m23330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Clear()
extern "C" void List_1_Clear_m23331_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_Clear_m23331(__this, method) (( void (*) (List_1_t888 *, const MethodInfo*))List_1_Clear_m23331_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
extern "C" bool List_1_Contains_m23332_gshared (List_1_t888 * __this, TargetSearchResult_t884  ___item, const MethodInfo* method);
#define List_1_Contains_m23332(__this, ___item, method) (( bool (*) (List_1_t888 *, TargetSearchResult_t884 , const MethodInfo*))List_1_Contains_m23332_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m23333_gshared (List_1_t888 * __this, TargetSearchResultU5BU5D_t2914* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m23333(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t888 *, TargetSearchResultU5BU5D_t2914*, int32_t, const MethodInfo*))List_1_CopyTo_m23333_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Find(System.Predicate`1<T>)
extern "C" TargetSearchResult_t884  List_1_Find_m23334_gshared (List_1_t888 * __this, Predicate_1_t2921 * ___match, const MethodInfo* method);
#define List_1_Find_m23334(__this, ___match, method) (( TargetSearchResult_t884  (*) (List_1_t888 *, Predicate_1_t2921 *, const MethodInfo*))List_1_Find_m23334_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m23335_gshared (Object_t * __this /* static, unused */, Predicate_1_t2921 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m23335(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2921 *, const MethodInfo*))List_1_CheckMatch_m23335_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m23336_gshared (List_1_t888 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2921 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m23336(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t888 *, int32_t, int32_t, Predicate_1_t2921 *, const MethodInfo*))List_1_GetIndex_m23336_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
extern "C" Enumerator_t2915  List_1_GetEnumerator_m23337_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m23337(__this, method) (( Enumerator_t2915  (*) (List_1_t888 *, const MethodInfo*))List_1_GetEnumerator_m23337_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m23338_gshared (List_1_t888 * __this, TargetSearchResult_t884  ___item, const MethodInfo* method);
#define List_1_IndexOf_m23338(__this, ___item, method) (( int32_t (*) (List_1_t888 *, TargetSearchResult_t884 , const MethodInfo*))List_1_IndexOf_m23338_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m23339_gshared (List_1_t888 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m23339(__this, ___start, ___delta, method) (( void (*) (List_1_t888 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m23339_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m23340_gshared (List_1_t888 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m23340(__this, ___index, method) (( void (*) (List_1_t888 *, int32_t, const MethodInfo*))List_1_CheckIndex_m23340_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m23341_gshared (List_1_t888 * __this, int32_t ___index, TargetSearchResult_t884  ___item, const MethodInfo* method);
#define List_1_Insert_m23341(__this, ___index, ___item, method) (( void (*) (List_1_t888 *, int32_t, TargetSearchResult_t884 , const MethodInfo*))List_1_Insert_m23341_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m23342_gshared (List_1_t888 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m23342(__this, ___collection, method) (( void (*) (List_1_t888 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m23342_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T)
extern "C" bool List_1_Remove_m23343_gshared (List_1_t888 * __this, TargetSearchResult_t884  ___item, const MethodInfo* method);
#define List_1_Remove_m23343(__this, ___item, method) (( bool (*) (List_1_t888 *, TargetSearchResult_t884 , const MethodInfo*))List_1_Remove_m23343_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m23344_gshared (List_1_t888 * __this, Predicate_1_t2921 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m23344(__this, ___match, method) (( int32_t (*) (List_1_t888 *, Predicate_1_t2921 *, const MethodInfo*))List_1_RemoveAll_m23344_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m23345_gshared (List_1_t888 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m23345(__this, ___index, method) (( void (*) (List_1_t888 *, int32_t, const MethodInfo*))List_1_RemoveAt_m23345_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Reverse()
extern "C" void List_1_Reverse_m23346_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_Reverse_m23346(__this, method) (( void (*) (List_1_t888 *, const MethodInfo*))List_1_Reverse_m23346_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Sort()
extern "C" void List_1_Sort_m23347_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_Sort_m23347(__this, method) (( void (*) (List_1_t888 *, const MethodInfo*))List_1_Sort_m23347_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m23348_gshared (List_1_t888 * __this, Comparison_1_t2924 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m23348(__this, ___comparison, method) (( void (*) (List_1_t888 *, Comparison_1_t2924 *, const MethodInfo*))List_1_Sort_m23348_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::ToArray()
extern "C" TargetSearchResultU5BU5D_t2914* List_1_ToArray_m23349_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_ToArray_m23349(__this, method) (( TargetSearchResultU5BU5D_t2914* (*) (List_1_t888 *, const MethodInfo*))List_1_ToArray_m23349_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m23350_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m23350(__this, method) (( void (*) (List_1_t888 *, const MethodInfo*))List_1_TrimExcess_m23350_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m23351_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m23351(__this, method) (( int32_t (*) (List_1_t888 *, const MethodInfo*))List_1_get_Capacity_m23351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m23352_gshared (List_1_t888 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m23352(__this, ___value, method) (( void (*) (List_1_t888 *, int32_t, const MethodInfo*))List_1_set_Capacity_m23352_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
extern "C" int32_t List_1_get_Count_m23353_gshared (List_1_t888 * __this, const MethodInfo* method);
#define List_1_get_Count_m23353(__this, method) (( int32_t (*) (List_1_t888 *, const MethodInfo*))List_1_get_Count_m23353_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
extern "C" TargetSearchResult_t884  List_1_get_Item_m23354_gshared (List_1_t888 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m23354(__this, ___index, method) (( TargetSearchResult_t884  (*) (List_1_t888 *, int32_t, const MethodInfo*))List_1_get_Item_m23354_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m23355_gshared (List_1_t888 * __this, int32_t ___index, TargetSearchResult_t884  ___value, const MethodInfo* method);
#define List_1_set_Item_m23355(__this, ___index, ___value, method) (( void (*) (List_1_t888 *, int32_t, TargetSearchResult_t884 , const MethodInfo*))List_1_set_Item_m23355_gshared)(__this, ___index, ___value, method)
