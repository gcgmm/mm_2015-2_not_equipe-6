﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct ShimEnumerator_t2719;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t2706;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m20026_gshared (ShimEnumerator_t2719 * __this, Dictionary_2_t2706 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m20026(__this, ___host, method) (( void (*) (ShimEnumerator_t2719 *, Dictionary_2_t2706 *, const MethodInfo*))ShimEnumerator__ctor_m20026_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m20027_gshared (ShimEnumerator_t2719 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m20027(__this, method) (( bool (*) (ShimEnumerator_t2719 *, const MethodInfo*))ShimEnumerator_MoveNext_m20027_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1423  ShimEnumerator_get_Entry_m20028_gshared (ShimEnumerator_t2719 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m20028(__this, method) (( DictionaryEntry_t1423  (*) (ShimEnumerator_t2719 *, const MethodInfo*))ShimEnumerator_get_Entry_m20028_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m20029_gshared (ShimEnumerator_t2719 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m20029(__this, method) (( Object_t * (*) (ShimEnumerator_t2719 *, const MethodInfo*))ShimEnumerator_get_Key_m20029_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m20030_gshared (ShimEnumerator_t2719 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m20030(__this, method) (( Object_t * (*) (ShimEnumerator_t2719 *, const MethodInfo*))ShimEnumerator_get_Value_m20030_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m20031_gshared (ShimEnumerator_t2719 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m20031(__this, method) (( Object_t * (*) (ShimEnumerator_t2719 *, const MethodInfo*))ShimEnumerator_get_Current_m20031_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Reset()
extern "C" void ShimEnumerator_Reset_m20032_gshared (ShimEnumerator_t2719 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m20032(__this, method) (( void (*) (ShimEnumerator_t2719 *, const MethodInfo*))ShimEnumerator_Reset_m20032_gshared)(__this, method)
