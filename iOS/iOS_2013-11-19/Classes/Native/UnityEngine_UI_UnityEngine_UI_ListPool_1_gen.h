﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct ObjectPool_1_t2348;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct UnityAction_1_t2349;

#include "mscorlib_System_Object.h"

// UnityEngine.UI.ListPool`1<UnityEngine.Canvas>
struct  ListPool_1_t410  : public Object_t
{
};
struct ListPool_1_t410_StaticFields{
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1<UnityEngine.Canvas>::s_ListPool
	ObjectPool_1_t2348 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1<UnityEngine.Canvas>::<>f__am$cache1
	UnityAction_1_t2349 * ___U3CU3Ef__amU24cache1_1;
};
