﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2634;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_39.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m19068_gshared (Enumerator_t2644 * __this, Dictionary_2_t2634 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m19068(__this, ___host, method) (( void (*) (Enumerator_t2644 *, Dictionary_2_t2634 *, const MethodInfo*))Enumerator__ctor_m19068_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m19069_gshared (Enumerator_t2644 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m19069(__this, method) (( Object_t * (*) (Enumerator_t2644 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19069_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m19070_gshared (Enumerator_t2644 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m19070(__this, method) (( void (*) (Enumerator_t2644 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m19070_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m19071_gshared (Enumerator_t2644 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m19071(__this, method) (( void (*) (Enumerator_t2644 *, const MethodInfo*))Enumerator_Dispose_m19071_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m19072_gshared (Enumerator_t2644 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m19072(__this, method) (( bool (*) (Enumerator_t2644 *, const MethodInfo*))Enumerator_MoveNext_m19072_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" int32_t Enumerator_get_Current_m19073_gshared (Enumerator_t2644 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m19073(__this, method) (( int32_t (*) (Enumerator_t2644 *, const MethodInfo*))Enumerator_get_Current_m19073_gshared)(__this, method)
