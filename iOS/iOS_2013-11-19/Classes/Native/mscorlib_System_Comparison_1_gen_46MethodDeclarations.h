﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"

// System.Void System.Comparison`1<Vuforia.DataSetImpl>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m20602(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2752 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m13016_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<Vuforia.DataSetImpl>::Invoke(T,T)
#define Comparison_1_Invoke_m20603(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2752 *, DataSetImpl_t751 *, DataSetImpl_t751 *, const MethodInfo*))Comparison_1_Invoke_m13017_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<Vuforia.DataSetImpl>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m20604(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2752 *, DataSetImpl_t751 *, DataSetImpl_t751 *, AsyncCallback_t261 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m13018_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<Vuforia.DataSetImpl>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m20605(__this, ___result, method) (( int32_t (*) (Comparison_1_t2752 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m13019_gshared)(__this, ___result, method)
