﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t779;
// System.Collections.Generic.IEnumerable`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerable_1_t3205;
// System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerator_1_t3203;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ICollection_1_t3206;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ReadOnlyCollection_1_t2726;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t2702;
// System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>
struct Predicate_1_t2729;
// System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparison_1_t2732;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_58.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C" void List_1__ctor_m5283_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1__ctor_m5283(__this, method) (( void (*) (List_1_t779 *, const MethodInfo*))List_1__ctor_m5283_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m20092_gshared (List_1_t779 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m20092(__this, ___collection, method) (( void (*) (List_1_t779 *, Object_t*, const MethodInfo*))List_1__ctor_m20092_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Int32)
extern "C" void List_1__ctor_m20093_gshared (List_1_t779 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m20093(__this, ___capacity, method) (( void (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1__ctor_m20093_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.cctor()
extern "C" void List_1__cctor_m20094_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m20094(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m20094_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20095_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20095(__this, method) (( Object_t* (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20095_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m20096_gshared (List_1_t779 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m20096(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t779 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m20096_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m20097_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20097(__this, method) (( Object_t * (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m20097_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m20098_gshared (List_1_t779 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m20098(__this, ___item, method) (( int32_t (*) (List_1_t779 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m20098_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m20099_gshared (List_1_t779 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m20099(__this, ___item, method) (( bool (*) (List_1_t779 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m20099_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m20100_gshared (List_1_t779 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m20100(__this, ___item, method) (( int32_t (*) (List_1_t779 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m20100_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m20101_gshared (List_1_t779 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m20101(__this, ___index, ___item, method) (( void (*) (List_1_t779 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m20101_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m20102_gshared (List_1_t779 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m20102(__this, ___item, method) (( void (*) (List_1_t779 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m20102_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20103_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20103(__this, method) (( bool (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20103_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m20104_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20104(__this, method) (( bool (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m20104_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m20105_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m20105(__this, method) (( Object_t * (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m20105_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m20106_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m20106(__this, method) (( bool (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m20106_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m20107_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m20107(__this, method) (( bool (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m20107_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m20108_gshared (List_1_t779 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m20108(__this, ___index, method) (( Object_t * (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m20108_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m20109_gshared (List_1_t779 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m20109(__this, ___index, ___value, method) (( void (*) (List_1_t779 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m20109_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Add(T)
extern "C" void List_1_Add_m20110_gshared (List_1_t779 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Add_m20110(__this, ___item, method) (( void (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_Add_m20110_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m20111_gshared (List_1_t779 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m20111(__this, ___newCount, method) (( void (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m20111_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m20112_gshared (List_1_t779 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m20112(__this, ___collection, method) (( void (*) (List_1_t779 *, Object_t*, const MethodInfo*))List_1_AddCollection_m20112_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m20113_gshared (List_1_t779 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m20113(__this, ___enumerable, method) (( void (*) (List_1_t779 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m20113_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m20114_gshared (List_1_t779 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m20114(__this, ___collection, method) (( void (*) (List_1_t779 *, Object_t*, const MethodInfo*))List_1_AddRange_m20114_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2726 * List_1_AsReadOnly_m20115_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m20115(__this, method) (( ReadOnlyCollection_1_t2726 * (*) (List_1_t779 *, const MethodInfo*))List_1_AsReadOnly_m20115_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Clear()
extern "C" void List_1_Clear_m20116_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_Clear_m20116(__this, method) (( void (*) (List_1_t779 *, const MethodInfo*))List_1_Clear_m20116_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(T)
extern "C" bool List_1_Contains_m20117_gshared (List_1_t779 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Contains_m20117(__this, ___item, method) (( bool (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_Contains_m20117_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m20118_gshared (List_1_t779 * __this, PIXEL_FORMATU5BU5D_t2702* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m20118(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t779 *, PIXEL_FORMATU5BU5D_t2702*, int32_t, const MethodInfo*))List_1_CopyTo_m20118_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Find(System.Predicate`1<T>)
extern "C" int32_t List_1_Find_m20119_gshared (List_1_t779 * __this, Predicate_1_t2729 * ___match, const MethodInfo* method);
#define List_1_Find_m20119(__this, ___match, method) (( int32_t (*) (List_1_t779 *, Predicate_1_t2729 *, const MethodInfo*))List_1_Find_m20119_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m20120_gshared (Object_t * __this /* static, unused */, Predicate_1_t2729 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m20120(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2729 *, const MethodInfo*))List_1_CheckMatch_m20120_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m20121_gshared (List_1_t779 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2729 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m20121(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t779 *, int32_t, int32_t, Predicate_1_t2729 *, const MethodInfo*))List_1_GetIndex_m20121_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator()
extern "C" Enumerator_t2725  List_1_GetEnumerator_m20122_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m20122(__this, method) (( Enumerator_t2725  (*) (List_1_t779 *, const MethodInfo*))List_1_GetEnumerator_m20122_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m20123_gshared (List_1_t779 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m20123(__this, ___item, method) (( int32_t (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_IndexOf_m20123_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m20124_gshared (List_1_t779 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m20124(__this, ___start, ___delta, method) (( void (*) (List_1_t779 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m20124_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m20125_gshared (List_1_t779 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m20125(__this, ___index, method) (( void (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_CheckIndex_m20125_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m20126_gshared (List_1_t779 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define List_1_Insert_m20126(__this, ___index, ___item, method) (( void (*) (List_1_t779 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m20126_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m20127_gshared (List_1_t779 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m20127(__this, ___collection, method) (( void (*) (List_1_t779 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m20127_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Remove(T)
extern "C" bool List_1_Remove_m20128_gshared (List_1_t779 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Remove_m20128(__this, ___item, method) (( bool (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_Remove_m20128_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m20129_gshared (List_1_t779 * __this, Predicate_1_t2729 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m20129(__this, ___match, method) (( int32_t (*) (List_1_t779 *, Predicate_1_t2729 *, const MethodInfo*))List_1_RemoveAll_m20129_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m20130_gshared (List_1_t779 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m20130(__this, ___index, method) (( void (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_RemoveAt_m20130_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Reverse()
extern "C" void List_1_Reverse_m20131_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_Reverse_m20131(__this, method) (( void (*) (List_1_t779 *, const MethodInfo*))List_1_Reverse_m20131_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort()
extern "C" void List_1_Sort_m20132_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_Sort_m20132(__this, method) (( void (*) (List_1_t779 *, const MethodInfo*))List_1_Sort_m20132_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m20133_gshared (List_1_t779 * __this, Comparison_1_t2732 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m20133(__this, ___comparison, method) (( void (*) (List_1_t779 *, Comparison_1_t2732 *, const MethodInfo*))List_1_Sort_m20133_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::ToArray()
extern "C" PIXEL_FORMATU5BU5D_t2702* List_1_ToArray_m20134_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_ToArray_m20134(__this, method) (( PIXEL_FORMATU5BU5D_t2702* (*) (List_1_t779 *, const MethodInfo*))List_1_ToArray_m20134_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::TrimExcess()
extern "C" void List_1_TrimExcess_m20135_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m20135(__this, method) (( void (*) (List_1_t779 *, const MethodInfo*))List_1_TrimExcess_m20135_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m20136_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m20136(__this, method) (( int32_t (*) (List_1_t779 *, const MethodInfo*))List_1_get_Capacity_m20136_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m20137_gshared (List_1_t779 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m20137(__this, ___value, method) (( void (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_set_Capacity_m20137_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Count()
extern "C" int32_t List_1_get_Count_m20138_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_get_Count_m20138(__this, method) (( int32_t (*) (List_1_t779 *, const MethodInfo*))List_1_get_Count_m20138_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Item(System.Int32)
extern "C" int32_t List_1_get_Item_m20139_gshared (List_1_t779 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m20139(__this, ___index, method) (( int32_t (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_get_Item_m20139_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m20140_gshared (List_1_t779 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define List_1_set_Item_m20140(__this, ___index, ___value, method) (( void (*) (List_1_t779 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m20140_gshared)(__this, ___index, ___value, method)
