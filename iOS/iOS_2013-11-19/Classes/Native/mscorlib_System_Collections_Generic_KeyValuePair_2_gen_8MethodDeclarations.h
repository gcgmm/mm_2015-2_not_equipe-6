﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m15888(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2403 *, Graphic_t228 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m14641_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m15889(__this, method) (( Graphic_t228 * (*) (KeyValuePair_2_t2403 *, const MethodInfo*))KeyValuePair_2_get_Key_m14642_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15890(__this, ___value, method) (( void (*) (KeyValuePair_2_t2403 *, Graphic_t228 *, const MethodInfo*))KeyValuePair_2_set_Key_m14643_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m15891(__this, method) (( int32_t (*) (KeyValuePair_2_t2403 *, const MethodInfo*))KeyValuePair_2_get_Value_m14644_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15892(__this, ___value, method) (( void (*) (KeyValuePair_2_t2403 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m14645_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m15893(__this, method) (( String_t* (*) (KeyValuePair_2_t2403 *, const MethodInfo*))KeyValuePair_2_ToString_m14646_gshared)(__this, method)
