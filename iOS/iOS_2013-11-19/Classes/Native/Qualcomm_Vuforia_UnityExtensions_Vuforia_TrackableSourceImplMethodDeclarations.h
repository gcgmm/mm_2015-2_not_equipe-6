﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.TrackableSourceImpl
struct TrackableSourceImpl_t890;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void Vuforia.TrackableSourceImpl::set_TrackableSourcePtr(System.IntPtr)
extern "C" void TrackableSourceImpl_set_TrackableSourcePtr_m4921 (TrackableSourceImpl_t890 * __this, IntPtr_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableSourceImpl::.ctor(System.IntPtr)
extern "C" void TrackableSourceImpl__ctor_m4922 (TrackableSourceImpl_t890 * __this, IntPtr_t ___trackableSourcePtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
