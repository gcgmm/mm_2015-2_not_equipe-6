﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::.ctor(System.Collections.Generic.HashSet`1<T>)
#define Enumerator__ctor_m24311(__this, ___hashset, method) (( void (*) (Enumerator_t1055 *, HashSet_1_t917 *, const MethodInfo*))Enumerator__ctor_m24300_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24312(__this, method) (( Object_t * (*) (Enumerator_t1055 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24301_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m24313(__this, method) (( void (*) (Enumerator_t1055 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m24302_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::MoveNext()
#define Enumerator_MoveNext_m5548(__this, method) (( bool (*) (Enumerator_t1055 *, const MethodInfo*))Enumerator_MoveNext_m24303_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::get_Current()
#define Enumerator_get_Current_m5547(__this, method) (( MeshRenderer_t113 * (*) (Enumerator_t1055 *, const MethodInfo*))Enumerator_get_Current_m24304_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::Dispose()
#define Enumerator_Dispose_m5549(__this, method) (( void (*) (Enumerator_t1055 *, const MethodInfo*))Enumerator_Dispose_m24305_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::CheckState()
#define Enumerator_CheckState_m24314(__this, method) (( void (*) (Enumerator_t1055 *, const MethodInfo*))Enumerator_CheckState_m24306_gshared)(__this, method)
