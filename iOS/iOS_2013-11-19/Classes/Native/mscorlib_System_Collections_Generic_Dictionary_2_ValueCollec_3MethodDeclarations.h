﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_30MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m19474(__this, ___host, method) (( void (*) (Enumerator_t951 *, Dictionary_2_t731 *, const MethodInfo*))Enumerator__ctor_m15354_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19475(__this, method) (( Object_t * (*) (Enumerator_t951 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15355_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m19476(__this, method) (( void (*) (Enumerator_t951 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15356_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m5237(__this, method) (( void (*) (Enumerator_t951 *, const MethodInfo*))Enumerator_Dispose_m15357_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m5236(__this, method) (( bool (*) (Enumerator_t951 *, const MethodInfo*))Enumerator_MoveNext_m15358_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m5235(__this, method) (( VideoBackgroundAbstractBehaviour_t67 * (*) (Enumerator_t951 *, const MethodInfo*))Enumerator_get_Current_m15359_gshared)(__this, method)
