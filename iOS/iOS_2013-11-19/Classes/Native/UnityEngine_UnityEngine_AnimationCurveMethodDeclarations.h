﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AnimationCurve
struct AnimationCurve_t558;
struct AnimationCurve_t558_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t681;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m2993 (AnimationCurve_t558 * __this, KeyframeU5BU5D_t681* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m2994 (AnimationCurve_t558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m2995 (AnimationCurve_t558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m2996 (AnimationCurve_t558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m2997 (AnimationCurve_t558 * __this, KeyframeU5BU5D_t681* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void AnimationCurve_t558_marshal(const AnimationCurve_t558& unmarshaled, AnimationCurve_t558_marshaled& marshaled);
extern "C" void AnimationCurve_t558_marshal_back(const AnimationCurve_t558_marshaled& marshaled, AnimationCurve_t558& unmarshaled);
extern "C" void AnimationCurve_t558_marshal_cleanup(AnimationCurve_t558_marshaled& marshaled);
