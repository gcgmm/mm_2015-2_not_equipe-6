﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t8;
// UnityEngine.GameObject
struct GameObject_t9;
// RowController
struct RowController_t10;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// DockController
struct  DockController_t7  : public MonoBehaviour_t2
{
	// System.Int32 DockController::DockIndex
	int32_t ___DockIndex_2;
	// System.Boolean DockController::isOver
	bool ___isOver_3;
	// UnityEngine.Material DockController::dockedColor
	Material_t8 * ___dockedColor_4;
	// UnityEngine.GameObject DockController::dockHolder
	GameObject_t9 * ___dockHolder_5;
	// RowController DockController::row
	RowController_t10 * ___row_6;
};
