﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>
struct ShimEnumerator_t3016;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3003;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m24659_gshared (ShimEnumerator_t3016 * __this, Dictionary_2_t3003 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m24659(__this, ___host, method) (( void (*) (ShimEnumerator_t3016 *, Dictionary_2_t3003 *, const MethodInfo*))ShimEnumerator__ctor_m24659_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m24660_gshared (ShimEnumerator_t3016 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m24660(__this, method) (( bool (*) (ShimEnumerator_t3016 *, const MethodInfo*))ShimEnumerator_MoveNext_m24660_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Entry()
extern "C" DictionaryEntry_t1423  ShimEnumerator_get_Entry_m24661_gshared (ShimEnumerator_t3016 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m24661(__this, method) (( DictionaryEntry_t1423  (*) (ShimEnumerator_t3016 *, const MethodInfo*))ShimEnumerator_get_Entry_m24661_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m24662_gshared (ShimEnumerator_t3016 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m24662(__this, method) (( Object_t * (*) (ShimEnumerator_t3016 *, const MethodInfo*))ShimEnumerator_get_Key_m24662_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m24663_gshared (ShimEnumerator_t3016 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m24663(__this, method) (( Object_t * (*) (ShimEnumerator_t3016 *, const MethodInfo*))ShimEnumerator_get_Value_m24663_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m24664_gshared (ShimEnumerator_t3016 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m24664(__this, method) (( Object_t * (*) (ShimEnumerator_t3016 *, const MethodInfo*))ShimEnumerator_get_Current_m24664_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::Reset()
extern "C" void ShimEnumerator_Reset_m24665_gshared (ShimEnumerator_t3016 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m24665(__this, method) (( void (*) (ShimEnumerator_t3016 *, const MethodInfo*))ShimEnumerator_Reset_m24665_gshared)(__this, method)
