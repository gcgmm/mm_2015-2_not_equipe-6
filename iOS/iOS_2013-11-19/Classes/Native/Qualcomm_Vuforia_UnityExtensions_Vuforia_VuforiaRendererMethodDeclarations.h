﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VuforiaRenderer
struct VuforiaRenderer_t832;
// Vuforia.VuforiaRendererImpl
struct VuforiaRendererImpl_t834;

#include "codegen/il2cpp-codegen.h"

// Vuforia.VuforiaRenderer Vuforia.VuforiaRenderer::get_Instance()
extern "C" VuforiaRenderer_t832 * VuforiaRenderer_get_Instance_m4148 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaRendererImpl Vuforia.VuforiaRenderer::get_InternalInstance()
extern "C" VuforiaRendererImpl_t834 * VuforiaRenderer_get_InternalInstance_m4149 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRenderer::.ctor()
extern "C" void VuforiaRenderer__ctor_m4150 (VuforiaRenderer_t832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRenderer::.cctor()
extern "C" void VuforiaRenderer__cctor_m4151 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
