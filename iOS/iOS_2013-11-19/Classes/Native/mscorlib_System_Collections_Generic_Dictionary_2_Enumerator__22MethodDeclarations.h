﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t2790;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21291_gshared (Enumerator_t2797 * __this, Dictionary_2_t2790 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m21291(__this, ___dictionary, method) (( void (*) (Enumerator_t2797 *, Dictionary_2_t2790 *, const MethodInfo*))Enumerator__ctor_m21291_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21292_gshared (Enumerator_t2797 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21292(__this, method) (( Object_t * (*) (Enumerator_t2797 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21292_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m21293_gshared (Enumerator_t2797 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m21293(__this, method) (( void (*) (Enumerator_t2797 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m21293_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1423  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21294_gshared (Enumerator_t2797 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21294(__this, method) (( DictionaryEntry_t1423  (*) (Enumerator_t2797 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21294_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21295_gshared (Enumerator_t2797 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21295(__this, method) (( Object_t * (*) (Enumerator_t2797 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21295_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21296_gshared (Enumerator_t2797 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21296(__this, method) (( Object_t * (*) (Enumerator_t2797 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21296_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21297_gshared (Enumerator_t2797 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21297(__this, method) (( bool (*) (Enumerator_t2797 *, const MethodInfo*))Enumerator_MoveNext_m21297_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_Current()
extern "C" KeyValuePair_2_t2792  Enumerator_get_Current_m21298_gshared (Enumerator_t2797 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21298(__this, method) (( KeyValuePair_2_t2792  (*) (Enumerator_t2797 *, const MethodInfo*))Enumerator_get_Current_m21298_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m21299_gshared (Enumerator_t2797 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m21299(__this, method) (( Object_t * (*) (Enumerator_t2797 *, const MethodInfo*))Enumerator_get_CurrentKey_m21299_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentValue()
extern "C" uint16_t Enumerator_get_CurrentValue_m21300_gshared (Enumerator_t2797 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m21300(__this, method) (( uint16_t (*) (Enumerator_t2797 *, const MethodInfo*))Enumerator_get_CurrentValue_m21300_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::Reset()
extern "C" void Enumerator_Reset_m21301_gshared (Enumerator_t2797 * __this, const MethodInfo* method);
#define Enumerator_Reset_m21301(__this, method) (( void (*) (Enumerator_t2797 *, const MethodInfo*))Enumerator_Reset_m21301_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::VerifyState()
extern "C" void Enumerator_VerifyState_m21302_gshared (Enumerator_t2797 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m21302(__this, method) (( void (*) (Enumerator_t2797 *, const MethodInfo*))Enumerator_VerifyState_m21302_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m21303_gshared (Enumerator_t2797 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m21303(__this, method) (( void (*) (Enumerator_t2797 *, const MethodInfo*))Enumerator_VerifyCurrent_m21303_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C" void Enumerator_Dispose_m21304_gshared (Enumerator_t2797 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21304(__this, method) (( void (*) (Enumerator_t2797 *, const MethodInfo*))Enumerator_Dispose_m21304_gshared)(__this, method)
