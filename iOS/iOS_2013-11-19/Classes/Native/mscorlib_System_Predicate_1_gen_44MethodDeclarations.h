﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>
struct Predicate_1_t2729;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t260;
// System.AsyncCallback
struct AsyncCallback_t261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m20214_gshared (Predicate_1_t2729 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m20214(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2729 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m20214_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m20215_gshared (Predicate_1_t2729 * __this, int32_t ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m20215(__this, ___obj, method) (( bool (*) (Predicate_1_t2729 *, int32_t, const MethodInfo*))Predicate_1_Invoke_m20215_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m20216_gshared (Predicate_1_t2729 * __this, int32_t ___obj, AsyncCallback_t261 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m20216(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2729 *, int32_t, AsyncCallback_t261 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m20216_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m20217_gshared (Predicate_1_t2729 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m20217(__this, ___result, method) (( bool (*) (Predicate_1_t2729 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m20217_gshared)(__this, ___result, method)
