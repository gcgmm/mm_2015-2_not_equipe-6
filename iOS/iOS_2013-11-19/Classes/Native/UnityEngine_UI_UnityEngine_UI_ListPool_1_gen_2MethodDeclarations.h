﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t346;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::.cctor()
extern "C" void ListPool_1__cctor_m17744_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m17744(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m17744_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Get()
extern "C" List_1_t346 * ListPool_1_Get_m2294_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Get_m2294(__this /* static, unused */, method) (( List_1_t346 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m2294_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m2318_gshared (Object_t * __this /* static, unused */, List_1_t346 * ___toRelease, const MethodInfo* method);
#define ListPool_1_Release_m2318(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t346 *, const MethodInfo*))ListPool_1_Release_m2318_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__14_m17745_gshared (Object_t * __this /* static, unused */, List_1_t346 * ___l, const MethodInfo* method);
#define ListPool_1_U3Cs_ListPoolU3Em__14_m17745(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t346 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__14_m17745_gshared)(__this /* static, unused */, ___l, method)
