﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m23296(__this, ___l, method) (( void (*) (Enumerator_t1041 *, List_1_t1039 *, const MethodInfo*))Enumerator__ctor_m12901_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m23297(__this, method) (( void (*) (Enumerator_t1041 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m12902_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23298(__this, method) (( Object_t * (*) (Enumerator_t1041 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12903_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m5495(__this, method) (( void (*) (Enumerator_t1041 *, const MethodInfo*))Enumerator_Dispose_m12904_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m23299(__this, method) (( void (*) (Enumerator_t1041 *, const MethodInfo*))Enumerator_VerifyState_m12905_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m5494(__this, method) (( bool (*) (Enumerator_t1041 *, const MethodInfo*))Enumerator_MoveNext_m12906_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m5493(__this, method) (( VirtualButtonAbstractBehaviour_t71 * (*) (Enumerator_t1041 *, const MethodInfo*))Enumerator_get_Current_m12907_gshared)(__this, method)
