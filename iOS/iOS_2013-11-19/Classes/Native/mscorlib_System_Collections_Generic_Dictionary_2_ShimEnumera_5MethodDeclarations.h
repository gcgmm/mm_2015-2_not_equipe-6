﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct ShimEnumerator_t2891;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t1037;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m23052_gshared (ShimEnumerator_t2891 * __this, Dictionary_2_t1037 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m23052(__this, ___host, method) (( void (*) (ShimEnumerator_t2891 *, Dictionary_2_t1037 *, const MethodInfo*))ShimEnumerator__ctor_m23052_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m23053_gshared (ShimEnumerator_t2891 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m23053(__this, method) (( bool (*) (ShimEnumerator_t2891 *, const MethodInfo*))ShimEnumerator_MoveNext_m23053_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Entry()
extern "C" DictionaryEntry_t1423  ShimEnumerator_get_Entry_m23054_gshared (ShimEnumerator_t2891 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m23054(__this, method) (( DictionaryEntry_t1423  (*) (ShimEnumerator_t2891 *, const MethodInfo*))ShimEnumerator_get_Entry_m23054_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m23055_gshared (ShimEnumerator_t2891 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m23055(__this, method) (( Object_t * (*) (ShimEnumerator_t2891 *, const MethodInfo*))ShimEnumerator_get_Key_m23055_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m23056_gshared (ShimEnumerator_t2891 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m23056(__this, method) (( Object_t * (*) (ShimEnumerator_t2891 *, const MethodInfo*))ShimEnumerator_get_Value_m23056_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m23057_gshared (ShimEnumerator_t2891 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m23057(__this, method) (( Object_t * (*) (ShimEnumerator_t2891 *, const MethodInfo*))ShimEnumerator_get_Current_m23057_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Reset()
extern "C" void ShimEnumerator_Reset_m23058_gshared (ShimEnumerator_t2891 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m23058(__this, method) (( void (*) (ShimEnumerator_t2891 *, const MethodInfo*))ShimEnumerator_Reset_m23058_gshared)(__this, method)
