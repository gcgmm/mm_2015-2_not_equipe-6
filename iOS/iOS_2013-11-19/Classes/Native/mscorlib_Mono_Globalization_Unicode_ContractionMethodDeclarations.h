﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Globalization.Unicode.Contraction
struct Contraction_t1484;
// System.Char[]
struct CharU5BU5D_t270;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t786;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Globalization.Unicode.Contraction::.ctor(System.Char[],System.String,System.Byte[])
extern "C" void Contraction__ctor_m8499 (Contraction_t1484 * __this, CharU5BU5D_t270* ___source, String_t* ___replacement, ByteU5BU5D_t786* ___sortkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
