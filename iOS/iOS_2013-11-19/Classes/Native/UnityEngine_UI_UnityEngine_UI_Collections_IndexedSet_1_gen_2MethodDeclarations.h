﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t2308;
// System.Collections.IEnumerator
struct IEnumerator_t378;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2460;
// System.Object[]
struct ObjectU5BU5D_t105;
// System.Predicate`1<System.Object>
struct Predicate_1_t2180;
// System.Comparison`1<System.Object>
struct Comparison_1_t2186;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C" void IndexedSet_1__ctor_m14557_gshared (IndexedSet_1_t2308 * __this, const MethodInfo* method);
#define IndexedSet_1__ctor_m14557(__this, method) (( void (*) (IndexedSet_1_t2308 *, const MethodInfo*))IndexedSet_1__ctor_m14557_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m14559_gshared (IndexedSet_1_t2308 * __this, const MethodInfo* method);
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m14559(__this, method) (( Object_t * (*) (IndexedSet_1_t2308 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m14559_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C" void IndexedSet_1_Add_m14561_gshared (IndexedSet_1_t2308 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Add_m14561(__this, ___item, method) (( void (*) (IndexedSet_1_t2308 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m14561_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C" bool IndexedSet_1_Remove_m14563_gshared (IndexedSet_1_t2308 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Remove_m14563(__this, ___item, method) (( bool (*) (IndexedSet_1_t2308 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m14563_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern "C" Object_t* IndexedSet_1_GetEnumerator_m14565_gshared (IndexedSet_1_t2308 * __this, const MethodInfo* method);
#define IndexedSet_1_GetEnumerator_m14565(__this, method) (( Object_t* (*) (IndexedSet_1_t2308 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m14565_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C" void IndexedSet_1_Clear_m14567_gshared (IndexedSet_1_t2308 * __this, const MethodInfo* method);
#define IndexedSet_1_Clear_m14567(__this, method) (( void (*) (IndexedSet_1_t2308 *, const MethodInfo*))IndexedSet_1_Clear_m14567_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C" bool IndexedSet_1_Contains_m14569_gshared (IndexedSet_1_t2308 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Contains_m14569(__this, ___item, method) (( bool (*) (IndexedSet_1_t2308 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m14569_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void IndexedSet_1_CopyTo_m14571_gshared (IndexedSet_1_t2308 * __this, ObjectU5BU5D_t105* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define IndexedSet_1_CopyTo_m14571(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t2308 *, ObjectU5BU5D_t105*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m14571_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C" int32_t IndexedSet_1_get_Count_m14573_gshared (IndexedSet_1_t2308 * __this, const MethodInfo* method);
#define IndexedSet_1_get_Count_m14573(__this, method) (( int32_t (*) (IndexedSet_1_t2308 *, const MethodInfo*))IndexedSet_1_get_Count_m14573_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C" bool IndexedSet_1_get_IsReadOnly_m14575_gshared (IndexedSet_1_t2308 * __this, const MethodInfo* method);
#define IndexedSet_1_get_IsReadOnly_m14575(__this, method) (( bool (*) (IndexedSet_1_t2308 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m14575_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C" int32_t IndexedSet_1_IndexOf_m14577_gshared (IndexedSet_1_t2308 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_IndexOf_m14577(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t2308 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m14577_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern "C" void IndexedSet_1_Insert_m14579_gshared (IndexedSet_1_t2308 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Insert_m14579(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t2308 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m14579_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C" void IndexedSet_1_RemoveAt_m14581_gshared (IndexedSet_1_t2308 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_RemoveAt_m14581(__this, ___index, method) (( void (*) (IndexedSet_1_t2308 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m14581_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * IndexedSet_1_get_Item_m14583_gshared (IndexedSet_1_t2308 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_get_Item_m14583(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t2308 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m14583_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C" void IndexedSet_1_set_Item_m14585_gshared (IndexedSet_1_t2308 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define IndexedSet_1_set_Item_m14585(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t2308 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m14585_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" void IndexedSet_1_RemoveAll_m14587_gshared (IndexedSet_1_t2308 * __this, Predicate_1_t2180 * ___match, const MethodInfo* method);
#define IndexedSet_1_RemoveAll_m14587(__this, ___match, method) (( void (*) (IndexedSet_1_t2308 *, Predicate_1_t2180 *, const MethodInfo*))IndexedSet_1_RemoveAll_m14587_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void IndexedSet_1_Sort_m14588_gshared (IndexedSet_1_t2308 * __this, Comparison_1_t2186 * ___sortLayoutFunction, const MethodInfo* method);
#define IndexedSet_1_Sort_m14588(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t2308 *, Comparison_1_t2186 *, const MethodInfo*))IndexedSet_1_Sort_m14588_gshared)(__this, ___sortLayoutFunction, method)
