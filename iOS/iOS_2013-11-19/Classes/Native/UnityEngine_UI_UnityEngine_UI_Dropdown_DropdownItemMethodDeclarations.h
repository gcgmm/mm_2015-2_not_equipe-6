﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Dropdown/DropdownItem
struct DropdownItem_t211;
// UnityEngine.UI.Text
struct Text_t16;
// UnityEngine.UI.Image
struct Image_t15;
// UnityEngine.RectTransform
struct RectTransform_t212;
// UnityEngine.UI.Toggle
struct Toggle_t213;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t175;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t137;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Dropdown/DropdownItem::.ctor()
extern "C" void DropdownItem__ctor_m691 (DropdownItem_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Text UnityEngine.UI.Dropdown/DropdownItem::get_text()
extern "C" Text_t16 * DropdownItem_get_text_m692 (DropdownItem_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_text(UnityEngine.UI.Text)
extern "C" void DropdownItem_set_text_m693 (DropdownItem_t211 * __this, Text_t16 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Image UnityEngine.UI.Dropdown/DropdownItem::get_image()
extern "C" Image_t15 * DropdownItem_get_image_m694 (DropdownItem_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_image(UnityEngine.UI.Image)
extern "C" void DropdownItem_set_image_m695 (DropdownItem_t211 * __this, Image_t15 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.Dropdown/DropdownItem::get_rectTransform()
extern "C" RectTransform_t212 * DropdownItem_get_rectTransform_m696 (DropdownItem_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_rectTransform(UnityEngine.RectTransform)
extern "C" void DropdownItem_set_rectTransform_m697 (DropdownItem_t211 * __this, RectTransform_t212 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Toggle UnityEngine.UI.Dropdown/DropdownItem::get_toggle()
extern "C" Toggle_t213 * DropdownItem_get_toggle_m698 (DropdownItem_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_toggle(UnityEngine.UI.Toggle)
extern "C" void DropdownItem_set_toggle_m699 (DropdownItem_t211 * __this, Toggle_t213 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C" void DropdownItem_OnPointerEnter_m700 (DropdownItem_t211 * __this, PointerEventData_t175 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::OnCancel(UnityEngine.EventSystems.BaseEventData)
extern "C" void DropdownItem_OnCancel_m701 (DropdownItem_t211 * __this, BaseEventData_t137 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
