﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_1MethodDeclarations.h"

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m17842(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t2550 *, UnityAction_1_t2551 *, UnityAction_1_t2551 *, const MethodInfo*))ObjectPool_1__ctor_m13407_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countAll()
#define ObjectPool_1_get_countAll_m17843(__this, method) (( int32_t (*) (ObjectPool_1_t2550 *, const MethodInfo*))ObjectPool_1_get_countAll_m13409_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m17844(__this, ___value, method) (( void (*) (ObjectPool_1_t2550 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m13411_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countActive()
#define ObjectPool_1_get_countActive_m17845(__this, method) (( int32_t (*) (ObjectPool_1_t2550 *, const MethodInfo*))ObjectPool_1_get_countActive_m13413_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m17846(__this, method) (( int32_t (*) (ObjectPool_1_t2550 *, const MethodInfo*))ObjectPool_1_get_countInactive_m13415_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Get()
#define ObjectPool_1_Get_m17847(__this, method) (( List_1_t380 * (*) (ObjectPool_1_t2550 *, const MethodInfo*))ObjectPool_1_Get_m13417_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Release(T)
#define ObjectPool_1_Release_m17848(__this, ___element, method) (( void (*) (ObjectPool_1_t2550 *, List_1_t380 *, const MethodInfo*))ObjectPool_1_Release_m13419_gshared)(__this, ___element, method)
