﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_63MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Boolean,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m24677(__this, ___object, ___method, method) (( void (*) (Transform_1_t3002 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m24651_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Boolean,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m24678(__this, ___key, ___value, method) (( DictionaryEntry_t1423  (*) (Transform_1_t3002 *, String_t*, bool, const MethodInfo*))Transform_1_Invoke_m24652_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Boolean,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m24679(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3002 *, String_t*, bool, AsyncCallback_t261 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m24653_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Boolean,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m24680(__this, ___result, method) (( DictionaryEntry_t1423  (*) (Transform_1_t3002 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m24654_gshared)(__this, ___result, method)
