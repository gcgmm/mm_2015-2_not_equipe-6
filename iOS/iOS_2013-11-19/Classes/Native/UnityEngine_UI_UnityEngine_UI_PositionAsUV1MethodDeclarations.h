﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.PositionAsUV1
struct PositionAsUV1_t355;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t234;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.PositionAsUV1::.ctor()
extern "C" void PositionAsUV1__ctor_m1701 (PositionAsUV1_t355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.PositionAsUV1::ModifyMesh(UnityEngine.UI.VertexHelper)
extern "C" void PositionAsUV1_ModifyMesh_m1702 (PositionAsUV1_t355 * __this, VertexHelper_t234 * ___vh, const MethodInfo* method) IL2CPP_METHOD_ATTR;
