﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
struct UserDefinedTargetBuildingAbstractBehaviour_t65;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t949;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"

// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::RegisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m5138 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::UnregisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
extern "C" bool UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m5139 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StartScanning()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m5140 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::BuildNewTarget(System.String,System.Single)
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m5141 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, String_t* ___targetName, float ___sceenSizeWidth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopScanning()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m5142 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::SetFrameQuality(Vuforia.ImageTargetBuilder/FrameQuality)
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m5143 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, int32_t ___frameQuality, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Start()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_Start_m5144 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Update()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_Update_m5145 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnEnable()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m5146 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDisable()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m5147 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDestroy()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m5148 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnVuforiaStarted()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_OnVuforiaStarted_m5149 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnPause(System.Boolean)
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_OnPause_m5150 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, bool ___pause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::.ctor()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour__ctor_m260 (UserDefinedTargetBuildingAbstractBehaviour_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
